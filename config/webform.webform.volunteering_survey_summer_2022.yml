uuid: 9f23cbc9-9f52-478e-a9a3-f1e74a6cbf05
langcode: en
status: open
dependencies: {  }
weight: 0
open: null
close: null
uid: 3873
template: false
archive: false
id: volunteering_survey_summer_2022
title: 'Volunteering Survey Summer 2022'
description: ''
category: Volunteering
elements: |-
  introduction:
    '#type': webform_card
    '#title': Introduction
    processed_text:
      '#type': processed_text
      '#text': |
        <h3>Welcome to the Volunteering Survey!</h3>

        <p><span><span><span>Thanks for visiting our survey - it's fairly short and there's a chance to win £300 of vouchers for a store of your choice (with two second prizes of £50 vouchers).</span></span></span></p>

        <p><span><span><span>We'd love to hear what you think,&nbsp;<strong>whether or not you’ve volunteered.</strong><br />
        &nbsp;<br />
        The survey should only take about 10 minutes to complete, and your answers will really help us plan our activities for next academic year and beyond.<br />
        &nbsp;<br />
        The survey is open to all UCL students, including those who graduated this year, plus UCL staff. </span></span></span><br />
        &nbsp;</p>

        <h3><strong>Data Protection</strong></h3>

        <p>This survey is designed and managed by Students’ Union UCL. We will use the information you provide in this survey to measure the impact of the Volunteering Service’s activities and identify improvements to our service.&nbsp;<strong>We will not identify you&nbsp;</strong>in any report nor share your details with anyone else, and will only process your personal data in accordance with University College London’s current data protection policy and the Students’ Union UCL Data Protection and Privacy Policy available at&nbsp;<a href="https://studentsunionucl.org/data-protection-and-privacy-policy">studentsunionucl.org/data-protection-and-privacy-policy</a></p>
      '#format': basic_html
    i_understand_let_s_get_on_with_the_survey_:
      '#type': checkbox
      '#title': 'I understand ... let''s get on with the survey.'
      '#required': true
  card_1:
    '#type': webform_card
    '#title': 'About your volunteering'
    by_community_volunteering_we_mean:
      '#type': processed_text
      '#text': |
        <h3>Important - please read carefully:</h3>

        <p>In this survey when we ask you about volunteering, we mean&nbsp;<strong>community volunteering</strong>. That is:<br />
        * Giving your spare time to a not-for-profit charity or voluntary organisation<br />
        * Unpaid mentoring or tutoring<br />
        * Participating in a student-led project that benefits the community<br />
        * Something that you have done through your club or society that benefits people outside of UCL<br />
        * Something you’ve done via your UCL department that benefits people outside of UCL<br />
        * Helping your own community e.g. through faith groups or community organisations<br />
        * Participating in one-off volunteering events</p>

        <p>We&nbsp;<strong>DO NOT</strong>&nbsp;mean<br />
        * Anything for which you have been paid<br />
        * Anything which ONLY benefits other UCL students<br />
        * Work placements / internships with profit-making organisations<br />
        * Anything from before your time at UCL</p>
      '#format': basic_html
    have_you_taken_part_in_any_community_volunteering_this_academic_:
      '#type': radios
      '#title': 'Have you taken part in any community volunteering this academic year (i.e. since August 2021)'
      '#options':
        'Yes, I''ve volunteeered': 'Yes, I''ve volunteeered'
        'No, I haven''t volunteered': 'No, I haven''t volunteered'
      '#required': true
    about_your_volunteering:
      '#type': fieldset
      '#title': 'About your volunteering'
      '#title_display': invisible
      '#states':
        visible:
          ':input[name="have_you_taken_part_in_any_community_volunteering_this_academic_"]':
            value: 'Yes, I''ve volunteeered'
      how_did_you_find_out_about_your_volunteering_please_tick_all_tha:
        '#type': webform_checkboxes_other
        '#title': 'How did you find out about your volunteering? Please tick all that apply.'
        '#options':
          'The Volunteering Service at Students'' Union UCL': 'The Volunteering Service at Students'' Union UCL'
          'Volunteering Fair at UCL': 'Volunteering Fair at UCL'
          'Students'' Union UCL newsletter or social media': 'Students'' Union UCL newsletter or social media'
          'UCL Volunteering Society': 'UCL Volunteering Society'
          'A friend / family member asked me': 'A friend / family member asked me'
          'A Club / Society at UCL': 'A Club / Society at UCL'
          'My UCL Department': 'My UCL Department'
          'I organised it myself with help from the Volunteering Service': 'I organised it myself with help from the Volunteering Service'
          'I organised it myself with help from my UCL department': 'I organised it myself with help from my UCL department'
          'I organised it myself without help from anyone at UCL': 'I organised it myself without help from anyone at UCL'
          'I was already involved before I came to UCL': 'I was already involved before I came to UCL'
        '#required': true
      processed_text_01:
        '#type': processed_text
        '#text': |
          <p><strong>Could you tell us briefly where you volunteered this year?</strong>&nbsp;Please give the name of each&nbsp;organisation, project or activity on a separate line.</p>
        '#format': basic_html
      project_1:
        '#type': textfield
        '#title': 'Project 1'
        '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
        '#required': true
      project_2:
        '#type': textfield
        '#title': 'Project 2'
        '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
      project_3:
        '#type': textfield
        '#title': 'Project 3'
        '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
      project_4:
        '#type': textfield
        '#title': 'Project 4'
        '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
      project_5:
        '#type': textfield
        '#title': 'Project 5'
        '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
      if_you_ve_volunteered_for_more_than_5_projects_this_year_please_:
        '#type': textarea
        '#title': 'If you''ve volunteered for more than 5 projects this year, please give details here'
        '#states':
          visible:
            ':input[name="project_5"]':
              filled: true
        '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    do_you_agree_with_any_of_the_following:
      '#type': webform_checkboxes_other
      '#title': 'Do you agree with any of the following statements?'
      '#options':
        'I’ve been too busy with my studies to volunteer': 'I’ve been too busy with my studies to volunteer'
        'I didn’t know how to get involved with volunteering': 'I didn’t know how to get involved with volunteering'
        'I’ve been too busy with paid work to volunteer': 'I’ve been too busy with paid work to volunteer'
        'I applied for volunteering but the organisation was too slow to respond': 'I applied for volunteering but the organisation was too slow to respond'
        'I’ve been too busy with other extra-curricular activities (e.g. clubs and societies) to volunteer': 'I’ve been too busy with other extra-curricular activities (e.g. clubs and societies) to volunteer'
        'I haven’t seen much publicity about volunteering': 'I haven’t seen much publicity about volunteering'
        'I applied for volunteering but was turned down': 'I applied for volunteering but was turned down'
        'I don’t feel ready to volunteer': 'I don’t feel ready to volunteer'
        'None of my friends volunteer': 'None of my friends volunteer'
        'I’ve been too busy with caring responsibilities to volunteer': 'I’ve been too busy with caring responsibilities to volunteer'
        'Volunteering isn’t relevant to my interests': 'Volunteering isn’t relevant to my interests'
        'I''ve been too unwell to volunteer': 'I''ve been too unwell to volunteer'
      '#options_randomize': true
      '#states':
        visible:
          ':input[name="have_you_taken_part_in_any_community_volunteering_this_academic_"]':
            value: 'No, I haven''t volunteered'
  card_2:
    '#type': webform_card
    '#title': 'Volunteering and your skills'
    '#states':
      visible:
        ':input[name="have_you_taken_part_in_any_community_volunteering_this_academic_"]':
          value: 'Yes, I''ve volunteeered'
    volunteering_skills_text:
      '#type': processed_text
      '#text': |
        <p>This section covers the skills that you might have developed as a volunteer.</p>
      '#format': basic_html
    because_of_my_volunteering_i_have_become_better_at_:
      '#type': webform_likert
      '#title': 'Because of my volunteering, I have become better at '
      '#questions':
        'Dealing with conflict': 'Dealing with conflict'
        'Knowing how to bring about social change': 'Knowing how to bring about social change'
        'Negotiating or influencing others ': 'Negotiating or influencing others '
        'Dealing with setbacks and challenges ': 'Dealing with setbacks and challenges '
        'Responding to change ': 'Responding to change '
      '#answers':
        'Strongly Agree': 'Strongly Agree'
        Agree: Agree
        'Neither Agree not Disagree': 'Neither Agree not Disagree'
        Disagree: Disagree
        'Strongly Disagree': 'Strongly Disagree'
      '#required': true
    because_of_my_volunteering_i_have_become_better_at_2:
      '#type': webform_likert
      '#title': 'Because of my volunteering, I have become better at '
      '#questions':
        'Time management ': 'Time management '
        'Finding creative solutions to problems ': 'Finding creative solutions to problems '
        'Collaborating with others': 'Collaborating with others'
        'Understanding other people ': 'Understanding other people '
      '#answers':
        'Strongly Agree': 'Strongly Agree'
        Agree: Agree
        'Neither Agree not Disagree': 'Neither Agree not Disagree'
        Disagree: Disagree
        'Strongly Disagree': 'Strongly Disagree'
      '#required': true
  volunteering_and_your_course:
    '#type': webform_card
    '#title': 'Volunteering and your course'
    '#states':
      visible:
        ':input[name="have_you_taken_part_in_any_community_volunteering_this_academic_"]':
          value: 'Yes, I''ve volunteeered'
    course_text:
      '#type': processed_text
      '#text': |
        <p>This section asks you about how your volunteering may have impacted your studies</p>
      '#format': basic_html
    because_of_my_volunteering_:
      '#type': webform_likert
      '#title': 'Because of my volunteering ...'
      '#questions':
        'I’ve acquired skills / experience related to my degree': 'I’ve acquired skills / experience related to my degree'
        'I put my studies into a social context': 'I put my studies into a social context'
        'I passed on knowledge from my subject to other people': 'I passed on knowledge from my subject to other people'
        'I got to apply my learning from my course in a new context': 'I got to apply my learning from my course in a new context'
        'I was better motivated in my degree': 'I was better motivated in my degree'
        'I developed new study skills': 'I developed new study skills'
        'I gained new insights into my academic subject': 'I gained new insights into my academic subject'
      '#answers':
        'Strongly Agree': 'Strongly Agree'
        Agree: Agree
        'Neither Agree not Disagree': 'Neither Agree not Disagree'
        Disagree: Disagree
        'Strongly Disagree': 'Strongly Disagree'
  your_volunteering_experience:
    '#type': webform_card
    '#title': 'Your volunteering experience'
    '#states':
      visible:
        ':input[name="have_you_taken_part_in_any_community_volunteering_this_academic_"]':
          value: 'Yes, I''ve volunteeered'
    looking_at_your_overall_experience_with_the_organisations_projec:
      '#type': webform_likert
      '#title': 'Looking at your overall experience with the organisations/projects you volunteered with in the last year, how much would you agree with the following statements? '
      '#questions':
        'I felt welcomed': 'I felt welcomed'
        'I was satisfied with the application/registration process': 'I was satisfied with the application/registration process'
        'The organisers showed their appreciation of my efforts': 'The organisers showed their appreciation of my efforts'
        'I felt like I was making a difference': 'I felt like I was making a difference'
        'I felt supported by the supervisors/leaders': 'I felt supported by the supervisors/leaders'
        'I received the training and/or information I needed to be an effective volunteer': 'I received the training and/or information I needed to be an effective volunteer'
        'The leaders were open to my ideas': 'The leaders were open to my ideas'
        'I knew what to do in an emergency': 'I knew what to do in an emergency'
        'I knew how to claim travel expenses': 'I knew how to claim travel expenses'
      '#answers':
        'True for ALL of my volunteering': 'True for ALL of my volunteering'
        'True for SOME of my volunteering': 'True for SOME of my volunteering'
        'Not True for ANY of my volunteering': 'Not True for ANY of my volunteering'
  the_volunteering_service:
    '#type': webform_card
    '#title': 'The Volunteering Service'
    about_vs_text:
      '#type': processed_text
      '#text': |
        <p><img alt="Volunteering Service logo" data-entity-type="file" data-entity-uuid="c871b53d-ade5-4000-b114-b23a9560540a" height="109" src="/sites/default/files/inline-images/Volunteering-Logos-wide.png" width="482" /></p>

        <p>Whether or not you've volunteered, we'd like to know what you think about Students' Union UCL Volunteering Service. This will help us identify what works well, and what needs to change.</p>
      '#format': basic_html
    what_do_you_like_about_the_volunteering_service:
      '#type': textarea
      '#title': 'What do you like about the Volunteering Service?'
      '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    what_don_t_you_like_about_the_volunteering_service_:
      '#type': textarea
      '#title': 'What don''t you like about the Volunteering Service?'
      '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    how_could_we_improve_the_volunteering_service_:
      '#type': textarea
      '#title': 'How could we improve the Volunteering Service?'
      '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
  test:
    '#type': webform_card
    '#title': 'A bit about you'
    are_you_:
      '#type': webform_checkboxes_other
      '#title': 'Are you ...'
      '#options':
        Undergraduate: Undergraduate
        'Master''s student': 'Master''s student'
        'PhD student': 'PhD student'
        'UCL staff member': 'UCL staff member'
    do_any_of_the_following_apply_to_you_:
      '#type': checkboxes
      '#title': 'Do any of the following apply to you?'
      '#options':
        'I work during term time': 'I work during term time'
        'I am on the committee of a UCL club or society': 'I am on the committee of a UCL club or society'
        'I have a high academic workload': 'I have a high academic workload'
        'I am an active member of a sports team at UCL ': 'I am an active member of a sports team at UCL '
        'I am an active member of an arts society at UCL ': 'I am an active member of an arts society at UCL '
        'I am an academic representative': 'I am an academic representative'
        'I have another elected role at UCL': 'I have another elected role at UCL'
  survey_prize_draw:
    '#type': webform_card
    '#title': 'Survey Prize Draw'
    processed_text_03:
      '#type': processed_text
      '#text': |
        <p>If you'd like to enter the prize draw to win £300 of vouchers for a store of your choice, please leave your details below. There are two second prizes of £50 too!</p>
      '#format': basic_html
    your_name:
      '#type': textfield
      '#title': 'Your Name'
      '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    your_ucl_email:
      '#type': email
      '#title': 'Your UCL email'
      '#description': '<p>The prize draw is only open to UCL students, staff members and recent graduates - please include a UCL email address</p>'
      '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    your_other_email:
      '#type': email
      '#title': 'Your other email'
      '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
  actions:
    '#type': webform_actions
    '#title': 'Submit button(s)'
    '#submit__label': 'Submit your survey answers'
css: ''
javascript: ''
settings:
  ajax: false
  ajax_scroll_top: form
  ajax_progress_type: ''
  ajax_effect: ''
  ajax_speed: null
  page: true
  page_submit_path: ''
  page_confirm_path: ''
  page_theme_name: ''
  form_title: source_entity_webform
  form_submit_once: false
  form_open_message: ''
  form_close_message: ''
  form_exception_message: ''
  form_previous_submissions: true
  form_confidential: false
  form_confidential_message: ''
  form_disable_remote_addr: false
  form_convert_anonymous: false
  form_prepopulate: false
  form_prepopulate_source_entity: false
  form_prepopulate_source_entity_required: false
  form_prepopulate_source_entity_type: ''
  form_unsaved: false
  form_disable_back: false
  form_submit_back: false
  form_disable_autocomplete: false
  form_novalidate: false
  form_disable_inline_errors: false
  form_required: false
  form_autofocus: false
  form_details_toggle: false
  form_reset: false
  form_access_denied: default
  form_access_denied_title: ''
  form_access_denied_message: ''
  form_access_denied_attributes: {  }
  form_file_limit: ''
  form_attributes: {  }
  form_method: ''
  form_action: ''
  share: false
  share_node: false
  share_theme_name: ''
  share_title: true
  share_page_body_attributes: {  }
  submission_label: ''
  submission_exception_message: ''
  submission_locked_message: ''
  submission_log: false
  submission_excluded_elements: {  }
  submission_exclude_empty: false
  submission_exclude_empty_checkbox: false
  submission_views: {  }
  submission_views_replace: {  }
  submission_user_columns: {  }
  submission_user_duplicate: false
  submission_access_denied: default
  submission_access_denied_title: ''
  submission_access_denied_message: ''
  submission_access_denied_attributes: {  }
  previous_submission_message: ''
  previous_submissions_message: ''
  autofill: false
  autofill_message: ''
  autofill_excluded_elements: {  }
  wizard_progress_bar: false
  wizard_progress_pages: false
  wizard_progress_percentage: false
  wizard_progress_link: false
  wizard_progress_states: false
  wizard_start_label: ''
  wizard_preview_link: false
  wizard_confirmation: true
  wizard_confirmation_label: ''
  wizard_auto_forward: false
  wizard_auto_forward_hide_next_button: false
  wizard_keyboard: true
  wizard_track: ''
  wizard_prev_button_label: ''
  wizard_next_button_label: ''
  wizard_toggle: true
  wizard_toggle_show_label: ''
  wizard_toggle_hide_label: ''
  preview: 0
  preview_label: ''
  preview_title: ''
  preview_message: ''
  preview_attributes: {  }
  preview_excluded_elements: {  }
  preview_exclude_empty: true
  preview_exclude_empty_checkbox: false
  draft: none
  draft_multiple: false
  draft_auto_save: false
  draft_saved_message: ''
  draft_loaded_message: ''
  draft_pending_single_message: ''
  draft_pending_multiple_message: ''
  confirmation_type: page
  confirmation_url: ''
  confirmation_title: 'Thank You'
  confirmation_message: "<p>Thanks for completing our survey - we really appreciate it!</p>\r\n\r\n<p>We've still lots of summer and online volunteering on offer, and projects are starting to recruit for the new academic year. <a href=\"https://studentsunionucl.org/volunteering\">Find out more on our webpages.</a></p>"
  confirmation_attributes: {  }
  confirmation_back: true
  confirmation_back_label: ''
  confirmation_back_attributes: {  }
  confirmation_exclude_query: false
  confirmation_exclude_token: false
  confirmation_update: false
  limit_total: null
  limit_total_interval: null
  limit_total_message: ''
  limit_total_unique: false
  limit_user: null
  limit_user_interval: null
  limit_user_message: ''
  limit_user_unique: false
  entity_limit_total: null
  entity_limit_total_interval: null
  entity_limit_user: null
  entity_limit_user_interval: null
  purge: none
  purge_days: null
  results_disabled: false
  results_disabled_ignore: false
  results_customize: false
  token_view: false
  token_update: false
  token_delete: false
  serial_disabled: false
access:
  create:
    roles:
      - anonymous
      - authenticated
    users: {  }
    permissions: {  }
  view_any:
    roles: {  }
    users: {  }
    permissions: {  }
  update_any:
    roles: {  }
    users: {  }
    permissions: {  }
  delete_any:
    roles: {  }
    users: {  }
    permissions: {  }
  purge_any:
    roles: {  }
    users: {  }
    permissions: {  }
  view_own:
    roles: {  }
    users: {  }
    permissions: {  }
  update_own:
    roles: {  }
    users: {  }
    permissions: {  }
  delete_own:
    roles: {  }
    users: {  }
    permissions: {  }
  administer:
    roles: {  }
    users: {  }
    permissions: {  }
  test:
    roles: {  }
    users: {  }
    permissions: {  }
  configuration:
    roles: {  }
    users: {  }
    permissions: {  }
handlers:
  email:
    id: email
    handler_id: email
    label: Email
    notes: ''
    status: true
    conditions: {  }
    weight: 0
    settings:
      states:
        - completed
      to_mail: j.braime@ucl.ac.uk
      to_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      cc_mail: ''
      cc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: _default
      reply_to: ''
      return_path: ''
      sender_mail: ''
      sender_name: ''
      subject: _default
      body: _default
      excluded_elements: {  }
      ignore_access: false
      exclude_empty: true
      exclude_empty_checkbox: false
      exclude_attachments: false
      html: true
      attachments: false
      twig: false
      theme_name: ''
      parameters: {  }
      debug: false
variants: {  }
