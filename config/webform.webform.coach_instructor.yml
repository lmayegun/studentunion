uuid: 52a24470-e843-46de-bfed-39107a546a78
langcode: en
status: open
dependencies:
  module:
    - webform_workflows_element
weight: 0
open: null
close: null
uid: 298722
template: false
archive: false
id: coach_instructor
title: 'Club/Society Coach & Instructor Registration'
description: ''
category: 'Clubs & Societies'
elements: |-
  workflow:
    '#type': webform_workflows_element
    '#title': Workflow
    '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    '#access_update_roles':
      - administrator
      - clubs_societies
    '#access_view_roles':
      - authenticated
    '#workflow': club_society_coach_registration
    '#log_public_setting': Optional
    '#log_admin_setting': Optional
    '#state_pending_color': Grey
    '#state_passed_to_societies_and_media_manager_color': Purple
    '#state_passed_to_sports_development_manager_color': Blue
    '#state_approved_color': Green
    '#state_rejected_color': Red
  webform_access_group:
    '#type': webform_entity_select
    '#title': Club/Society
    '#prepopulate': true
    '#required': true
    '#multiple__no_items_message': '<p>No items entered. Please add items below.</p>'
    '#target_type': group
    '#selection_handler': 'default:group'
    '#selection_settings':
      target_bundles:
        club_society: club_society
      sort:
        field: label
        direction: ASC
  club_or_society:
    '#type': select
    '#title': 'Is this coach/instructor for a club or society?'
    '#options':
      Club: Club
      Society: Society
    '#required': true
  contact_email_address:
    '#type': email
    '#size': 20
    '#default_value': '[current-user:mail]'
    '#title': 'Contact email address'
    '#description': ''
    '#required': true
  club:
    '#type': fieldset
    '#title': Club
    '#states':
      visible:
        ':input[name="club_or_society"]':
          value: Club
    processed_text:
      '#type': processed_text
      '#text': |
        <p>This webform is for Sports Clubs to register their coaches and instructors. Please be aware that&nbsp;<strong>all forms must be correct&nbsp;</strong>in order for us to approve the registration. We can only accept submissions from&nbsp;<strong>presidents or&nbsp;treasurers!</strong></p>

        <p>Please read through our <a href="https://studentsunionucl.org/how-to/clubs-and-societies/how-to-register-and-pay-coaches-and-instructors">How-To guide for Registering Sports Club Coaches</a> before processing this form. This includes all of the necessary forms that need to be downloaded and completed, as well as a breakdown of the exact documents we require for each type of coach.</p>

        <p>&nbsp;</p>

        <p>If you have any questions regarding&nbsp;<strong>c</strong><strong>oach qualification requirements</strong>&nbsp;please email&nbsp;<a href="mailto:teamucl@ucl.ac.uk">teamucl@ucl.ac.uk</a>.&nbsp;</p>
      '#format': basic_html
    horizontal_rule:
      '#type': webform_horizontal_rule
    are_you_a_returning_coach:
      '#type': radios
      '#options':
        1: 'Yes'
        2: 'No'
      '#title': 'Are you a returning coach?'
      '#description': 'Have you previously been registered as a coach of the TeamUCL Club you are coaching this year?'
      '#required': true
    coachinstructorreg_markup:
      '#type': processed_text
      '#title': 'Coach / Instructor Registration Form'
      '#description': ''
      '#text': |
        <p>Your coach should download and complete the Coach/Instructor Registration Form. All sections of this form must be completed and photocopies of the following documents attached:</p>

        <ul>
        	<li>Coaching Certificates</li>
        	<li>Public Liability Insurance</li>
        	<li>First Aid Certificate</li>
        </ul>

        <p>Download the <a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="5c263263-e19b-4ede-86cc-d136120b44a6" filename="coach_instructor_registration_form_22-23_0.docx" href="/sites/default/files/inline-files/coach_instructor_registration_form_22-23_0.docx">Coach/Instructor Registration form template</a>.</p>
      '#format': basic_html
    test_upload:
      '#type': managed_file
      '#title': 'Coach / Instructor Registration Upload'
      '#description': '<p>Please upload your completed Coach or Instructor Registration form here:</p>'
      '#title_display': before
      '#required': true
      '#max_filesize': '4'
      '#file_extensions': 'pdf doc docx'
    coachingqualification_upload:
      '#type': managed_file
      '#title': 'Coaching Qualification Upload'
      '#description': '<p>Please upload the Coaching Certificate(s) here:</p>'
      '#title_display': before
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
    firstaidcertificate_upload:
      '#type': managed_file
      '#title': 'First Aid Certificate Upload'
      '#description': '<p>Please upload your Coach''s First Aid Certificate here:</p>'
      '#title_display': before
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
    publicliabilityinsurance_upload:
      '#type': managed_file
      '#title': 'Public Liability Insurance Upload'
      '#description': '<p>Please upload the Public Liability Insurance Document here:</p>'
      '#title_display': before
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
    sla_markup:
      '#type': processed_text
      '#title': 'Service Level Agreement (SLA)'
      '#description': ''
      '#text': |
        <p>The Coach/Instructor should read and sign the appropriate Student’s Union UCL Service Level Agreement (SLA).</p>

        <p>Via these links, download either the&nbsp;<a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="6349ff5f-1a00-4081-8b57-16462e83a781" filename="lead_coach_sla_22-23_0.docx" href="/sites/default/files/inline-files/lead_coach_sla_22-23_0.docx">Lead Coach&nbsp;SLA</a>&nbsp;or the <a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="57de15e2-1371-48eb-b0ad-b183e0060829" filename="assistant_coach_sla_22-23_0.docx" href="/sites/default/files/inline-files/assistant_coach_sla_22-23_0.docx">Assistant Coach&nbsp;SLA</a>.</p>

        <p><em>Please ensure that all details are correct.</em></p>
      '#format': basic_html
    sla_upload:
      '#type': managed_file
      '#title': 'Service Level Agreement (SLA)'
      '#description': '<p>Please upload the completed SLA here:</p>'
      '#title_display': before
      '#required': true
      '#max_filesize': '4'
      '#file_extensions': 'pdf doc docx'
    clubcoachagreement_markup:
      '#type': processed_text
      '#title': 'Club-Coach Agreement'
      '#description': ''
      '#text': |
        <p>The Club President, together with the Coach, should complete the Club-Coach Agreement, found here. When both parties are happy, this should be signed (this is an internal document so you have our permission to sign this one!).</p>

        <p>Download the <a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="a844e4e8-5151-4b65-a5d7-1a923972f2b1" filename="sports_club_coach_agreement_22-23_0.docx" href="/sites/default/files/inline-files/sports_club_coach_agreement_22-23_0.docx">Club-Coach Agreement template</a>.</p>
      '#format': basic_html
    clubcoachagreement_upload:
      '#type': managed_file
      '#title': 'Club-Coach Agreement'
      '#description': '<p>Please upload the completed Club-Coach Agreement here:</p>'
      '#title_display': before
      '#required': true
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
  societies:
    '#type': fieldset
    '#title': Societies
    '#states':
      visible:
        ':input[name="club_or_society"]':
          value: Society
    processed_text_societies:
      '#type': processed_text
      '#text': |
        <p>This webform is for&nbsp;<strong>societies&nbsp;</strong>to register their instructors. Please be aware that&nbsp;all forms must be correct&nbsp;in order for us to approve the&nbsp;registration.</p>

        <p>A How-To guide for this process can be found&nbsp;<a href="https://studentsunionucl.org/how-to-guides/how-to-register-and-pay-coaches-and-instructors">here</a>.</p>

        <p>Your instructors need to be re-registered each year, regardless of how long they have been teaching. No teaching may take place until your instructor registration is&nbsp;complete.&nbsp;</p>

        <p>Instructors can be fellow students, members of UCL, Student’s Union UCL staff or any other external&nbsp;individual.&nbsp;</p>

        <p>No matter what they do for a job, the Student’s Union UCL Finance Department consider them as Self-Employed – this is to do with tax&nbsp;status.&nbsp;</p>

        <p>If you have any questions about this process please email&nbsp;<a href="mailto:su.activities@ucl.ac.uk">su.activities@ucl.ac.uk</a>.&nbsp;</p>
      '#format': basic_html
    societycoach_instructor_name:
      '#type': textfield
      '#title': 'Coach/instructor name'
      '#description': 'Please include the full name of your coach/instructor below.'
      '#required': true
    societyfile_uploads:
      '#type': webform_section
      '#title': 'File Uploads'
      '#description': |-
        <p>Your instructor should download and complete the <a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="14e73718-3bb6-44e1-991e-2a676cf9f3c5" filename="Society Instructor Registration Form 22-23.docx" href="/sites/default/files/inline-files/Society%20Instructor%20Registration%20Form%2022-23.docx">Society Instructor Registration Form</a>. All sections of this form must be completed.</p>

        <p>Please attach the following completed forms :</p>

        <ul>
        	<li>Society Instructor Registration Form</li>
        	<li>Public Liability Insurance</li>
        	<li>First Aid Certificate&nbsp;(for any societies that participate in regular instructed physical activity)</li>
        </ul>
    societytest_upload:
      '#type': managed_file
      '#title': 'Instructor Registration Upload'
      '#description': '<p>Please upload your completed Society Instructor Registration Form here:</p>'
      '#title_display': invisible
      '#required': true
      '#max_filesize': '4'
      '#file_extensions': 'pdf doc docx'
    societyfirstaidcertificate_upload:
      '#type': managed_file
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
      '#title_display': invisible
      '#title': 'First Aid Certificate Upload'
      '#description': |+
        Please upload your instructor's First Aid Certificate here:

      '#required': true
    societypublicliabilityinsurance_upload:
      '#type': managed_file
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
      '#title_display': invisible
      '#title': 'Public Liability Insurance Upload'
      '#description': |+
        Please upload the Public Liability Insurance Document here:

      '#required': true
    societysla_markup:
      '#type': processed_text
      '#title': 'Society Instructor Service Level Agreement (SLA)'
      '#description': ''
      '#text': |
        <p>Your instructor should read and sign the Students’ Union UCL Service Level Agreement (SLA). Download the <a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="ef60c4c4-8ef4-4fc1-b689-dc378436e596" filename="Society-Instructor Agreement 22-23_0.docx" href="/sites/default/files/inline-files/Society-Instructor%20Agreement%2022-23_0.docx">Society Instructor SLA here</a>.</p>
      '#format': basic_html
    societysla_upload:
      '#type': managed_file
      '#max_filesize': '4'
      '#file_extensions': 'pdf doc docx'
      '#title_display': invisible
      '#title': 'Service Level Agreement (SLA)'
      '#description': |+
        Please upload the completed SLA below:

      '#required': true
    societyinstructoragreement_markup:
      '#type': processed_text
      '#title': 'Society-Instructor Agreement'
      '#description': ''
      '#text': |
        <p>Your society president, together with the instructor, should complete the <a class="file file--mime-application-vnd-openxmlformats-officedocument-wordprocessingml-document file--x-office-document" data-entity-type="file" data-entity-uuid="fbaee74c-652a-41be-ad5b-75457c260edb" filename="Society-Instructor Agreement 22-23.docx" href="/sites/default/files/inline-files/Society-Instructor%20Agreement%2022-23.docx">Society-Instructor Agreement</a>. When both parties are happy, this should be signed. This is an internal document so you have our permission to sign this one!&nbsp;</p>
      '#format': basic_html
    societyinstructoragreement_upload:
      '#type': managed_file
      '#max_filesize': '4'
      '#file_extensions': 'jpg jpeg png pdf doc docx'
      '#title_display': invisible
      '#title': 'Society-Instructor Agreement'
      '#description': |+
        Please upload the completed Society-Instructor Agreement below:

      '#required': true
css: ''
javascript: ''
settings:
  ajax: false
  ajax_scroll_top: form
  ajax_progress_type: ''
  ajax_effect: ''
  ajax_speed: null
  page: false
  page_submit_path: ''
  page_confirm_path: ''
  page_theme_name: ''
  form_title: source_entity_webform
  form_submit_once: false
  form_open_message: ''
  form_close_message: ''
  form_exception_message: ''
  form_previous_submissions: true
  form_confidential: false
  form_confidential_message: ''
  form_disable_remote_addr: false
  form_convert_anonymous: false
  form_prepopulate: false
  form_prepopulate_source_entity: false
  form_prepopulate_source_entity_required: false
  form_prepopulate_source_entity_type: ''
  form_unsaved: false
  form_disable_back: false
  form_submit_back: false
  form_disable_autocomplete: false
  form_novalidate: false
  form_disable_inline_errors: false
  form_required: false
  form_autofocus: false
  form_details_toggle: false
  form_reset: false
  form_access_denied: login
  form_access_denied_title: ''
  form_access_denied_message: '<p>You must log in to submit this form.</p>'
  form_access_denied_attributes: {  }
  form_file_limit: ''
  form_attributes: {  }
  form_method: ''
  form_action: ''
  share: false
  share_node: false
  share_theme_name: ''
  share_title: true
  share_page_body_attributes: {  }
  submission_label: ''
  submission_exception_message: ''
  submission_locked_message: ''
  submission_log: false
  submission_excluded_elements: {  }
  submission_exclude_empty: false
  submission_exclude_empty_checkbox: false
  submission_views: {  }
  submission_views_replace: {  }
  submission_user_columns: {  }
  submission_user_duplicate: false
  submission_access_denied: default
  submission_access_denied_title: ''
  submission_access_denied_message: ''
  submission_access_denied_attributes: {  }
  previous_submission_message: ''
  previous_submissions_message: ''
  autofill: false
  autofill_message: ''
  autofill_excluded_elements: {  }
  wizard_progress_bar: true
  wizard_progress_pages: false
  wizard_progress_percentage: false
  wizard_progress_link: false
  wizard_progress_states: false
  wizard_start_label: ''
  wizard_preview_link: false
  wizard_confirmation: true
  wizard_confirmation_label: ''
  wizard_auto_forward: true
  wizard_auto_forward_hide_next_button: false
  wizard_keyboard: true
  wizard_track: ''
  wizard_prev_button_label: ''
  wizard_next_button_label: ''
  wizard_toggle: true
  wizard_toggle_show_label: ''
  wizard_toggle_hide_label: ''
  preview: 0
  preview_label: ''
  preview_title: ''
  preview_message: ''
  preview_attributes: {  }
  preview_excluded_elements: {  }
  preview_exclude_empty: true
  preview_exclude_empty_checkbox: false
  draft: '0'
  draft_multiple: false
  draft_auto_save: false
  draft_saved_message: ''
  draft_loaded_message: ''
  draft_pending_single_message: ''
  draft_pending_multiple_message: ''
  confirmation_type: page
  confirmation_url: ''
  confirmation_title: ''
  confirmation_message: "<p>Your forms will be checked by Union staff. Please note that if any of the forms are incorrect, you will need to resubmit all your documents through this webform.&nbsp;</p>\r\n"
  confirmation_attributes: {  }
  confirmation_back: true
  confirmation_back_label: ''
  confirmation_back_attributes: {  }
  confirmation_exclude_query: false
  confirmation_exclude_token: false
  confirmation_update: false
  limit_total: null
  limit_total_interval: null
  limit_total_message: ''
  limit_total_unique: false
  limit_user: null
  limit_user_interval: null
  limit_user_message: ''
  limit_user_unique: false
  entity_limit_total: null
  entity_limit_total_interval: null
  entity_limit_user: null
  entity_limit_user_interval: null
  purge: none
  purge_days: null
  results_disabled: false
  results_disabled_ignore: false
  results_customize: false
  token_view: false
  token_update: false
  token_delete: false
  serial_disabled: false
  form_submit_label: ''
  wizard_complete: 1
access:
  create:
    roles:
      - authenticated
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  view_any:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  update_any:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  delete_any:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  purge_any:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  view_own:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  update_own:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  delete_own:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  administer:
    roles:
      - administrator
      - clubs_societies
    users: {  }
    permissions: {  }
  test:
    roles: {  }
    users: {  }
    permissions: {  }
    group_roles: {  }
    group_permissions: {  }
  configuration:
    roles:
      - clubs_societies
    users: {  }
    permissions: {  }
  administer_submissions:
    group_roles: {  }
    group_permissions: {  }
handlers:
  email_1:
    id: email
    handler_id: email_1
    label: 'Email to submitter'
    notes: ''
    status: true
    conditions: {  }
    weight: 1
    settings:
      states:
        - completed
      to_mail: '[webform_submission:values:contact_email_address:raw]'
      to_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      cc_mail: ''
      cc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: 'Student Activities'
      reply_to: su.activities@ucl.ac.uk
      return_path: ''
      sender_mail: ''
      sender_name: ''
      subject: 'Coach & Instructor Registration Form'
      body: "Thank you for submitting your coach/instructor registration documents. Your request is pending. \r\n\r\nYou can log in to the Students' Union UCL website to view the status of any webforms you have submitted. You can view Coach & Instructor Registration Forms you have previously submitted on the following page: https://studentsunionucl.org/forms/sports-clubs-coach-instructor-registration-webform. For this submission, please visit [webform_submission:url].\r\n\r\nIf you have any questions, please email su.activities@ucl.ac.uk. "
      excluded_elements: {  }
      ignore_access: false
      exclude_empty: true
      exclude_empty_checkbox: false
      exclude_attachments: false
      html: false
      attachments: false
      twig: false
      theme_name: ''
      parameters: {  }
      debug: false
  e_mail_societies_media_manager:
    id: workflows_transition_email
    handler_id: e_mail_societies_media_manager
    label: 'E-mail - Societies & Media Manager'
    notes: ''
    status: true
    conditions: {  }
    weight: 2
    settings:
      states:
        - 'workflow:pass_to_societies_and_media_manager'
      to_mail: c.salton-cox@ucl.ac.uk
      to_options: {  }
      cc_mail: ''
      cc_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: _default
      subject: _default
      body: "<p>Coach/instructor registration documents have been submitted.</p>\r\n\r\n<p>Current status is:&nbsp;[webform_submission:values:workflow:workflow_state_label]</p>\r\n\r\n<p>[webform_submission:values:workflow:log_public]</p>\r\n\r\n<hr />\r\n<p>View submission at <a href=\"[webform_submission:url:canonical]\">[webform_submission:url:canonical]</a></p>\r\n\r\n<p>Submitted on [webform_submission:created]</p>\r\n\r\n<p>Submitted by: [webform_submission:user]</p>\r\n\r\n<p>Submitted values are:</p>\r\n\r\n<p>[webform_submission:values]</p>"
      excluded_elements: {  }
      ignore_access: 0
      exclude_empty: true
      exclude_empty_checkbox: 0
      exclude_attachments: 0
      html: true
      attachments: 0
      twig: false
      debug: false
      reply_to: ''
      return_path: ''
      sender_mail: ''
      sender_name: ''
      theme_name: ''
      parameters: {  }
  e_mail_sports_development_manager:
    id: workflows_transition_email
    handler_id: e_mail_sports_development_manager
    label: 'E-mail - Sports Club Manager'
    notes: ''
    status: true
    conditions: {  }
    weight: 2
    settings:
      states:
        - 'workflow:pass_to_sports_development_manager'
      to_mail: h.warne@ucl.ac.uk
      to_options: {  }
      cc_mail: ''
      cc_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: _default
      subject: _default
      body: "<p>Coach/instructor registration documents have been submitted.</p>\r\n\r\n<p>Current status is:&nbsp;[webform_submission:values:workflow:workflow_state_label]</p>\r\n\r\n<p>[webform_submission:values:workflow:log_public]</p>\r\n\r\n<hr />\r\n<p>View submission at <a href=\"[webform_submission:url:canonical]\">[webform_submission:url:canonical]</a></p>\r\n\r\n<p>Submitted on [webform_submission:created]</p>\r\n\r\n<p>Submitted by: [webform_submission:user]</p>\r\n\r\n<p>Submitted values are:</p>\r\n\r\n<p>[webform_submission:values]</p>"
      excluded_elements: {  }
      ignore_access: 0
      exclude_empty: true
      exclude_empty_checkbox: 0
      exclude_attachments: 0
      html: true
      attachments: 0
      twig: false
      debug: false
      reply_to: ''
      return_path: ''
      sender_mail: ''
      sender_name: ''
      theme_name: ''
      parameters: {  }
  e_mail_approved:
    id: workflows_transition_email
    handler_id: e_mail_approved
    label: 'E-mail - approved'
    notes: ''
    status: true
    conditions: {  }
    weight: 3
    settings:
      states:
        - 'workflow:approve'
      to_mail: '[webform_submission:values:contact_email_address:raw]'
      to_options: {  }
      cc_mail: ''
      cc_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: _default
      subject: _default
      body: "<p>Thank you for submitting your coach/instructor registration documents. Your request is approved.</p>\r\n\r\n<p><em>[webform_submission:values:workflow:log_public]</em></p>\r\n\r\n<p>You can log in to the Students' Union UCL website to view the status of any webforms you have submitted. You can view Coach &amp; Instructor Registration Forms you have previously submitted on the following page: https://studentsunionucl.org/forms/sports-clubs-coach-instructor-registration-webform. For this submission, please visit <a href=\"[webform_submission:url:canonical]\">[webform_submission:url:canonical]</a>.</p>\r\n\r\n<hr />\r\n<p>View submission at <a href=\"[webform_submission:url:canonical]\">[webform_submission:url:canonical]</a></p>\r\n\r\n<p>Submitted on [webform_submission:created]</p>\r\n\r\n<p>Submitted by: [webform_submission:user]</p>\r\n\r\n<p>Submitted values are:</p>\r\n\r\n<p>[webform_submission:values]</p>"
      excluded_elements: {  }
      ignore_access: 0
      exclude_empty: true
      exclude_empty_checkbox: 0
      exclude_attachments: 0
      html: true
      attachments: 0
      twig: false
      debug: false
      reply_to: su.activities@ucl.ac.uk
      return_path: ''
      sender_mail: ''
      sender_name: ''
      theme_name: ''
      parameters: {  }
  e_mail_rejected:
    id: workflows_transition_email
    handler_id: e_mail_rejected
    label: 'E-mail - rejected'
    notes: ''
    status: true
    conditions: {  }
    weight: 4
    settings:
      states:
        - 'workflow:reject_no_resubmit_'
      to_mail: '[webform_submission:values:contact_email_address:raw]'
      to_options: {  }
      cc_mail: ''
      cc_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: _default
      subject: _default
      body: "<p>Thank you for submitting your coach/instructor registration documents. Your request has been rejected.</p>\r\n\r\n<p><em>[webform_submission:values:workflow:log_public]</em></p>\r\n\r\n<p>You can log in to the Students' Union UCL website to view the status of any webforms you have submitted. You can view Coach &amp; Instructor Registration Forms you have previously submitted on the following page: https://studentsunionucl.org/forms/sports-clubs-coach-instructor-registration-webform. For this submission, please visit <a href=\"[webform_submission:url:canonical]\">[webform_submission:url:canonical]</a>.</p>\r\n\r\n<p>If you have any questions, please email su.activites-reception@ucl.ac.uk.&nbsp;</p>\r\n\r\n<hr />\r\n<p>View submission at <a href=\"[webform_submission:url:canonical]\">[webform_submission:url:canonical]</a></p>\r\n\r\n<p>Submitted on [webform_submission:created]</p>\r\n\r\n<p>Submitted by: [webform_submission:user]</p>\r\n\r\n<p>Submitted values are:</p>\r\n\r\n<p>[webform_submission:values]</p>"
      excluded_elements: {  }
      ignore_access: 0
      exclude_empty: true
      exclude_empty_checkbox: 0
      exclude_attachments: 0
      html: true
      attachments: 0
      twig: false
      debug: false
      reply_to: su.activities@ucl.ac.uk
      return_path: ''
      sender_mail: ''
      sender_name: ''
      theme_name: ''
      parameters: {  }
  email:
    id: email
    handler_id: email
    label: Email
    notes: ''
    status: true
    conditions: {  }
    weight: 4
    settings:
      states:
        - completed
      to_mail: scott.dale@ucl.ac.uk
      to_options: {  }
      bcc_mail: ''
      bcc_options: {  }
      cc_mail: ''
      cc_options: {  }
      from_mail: _default
      from_options: {  }
      from_name: _default
      reply_to: ''
      return_path: ''
      sender_mail: ''
      sender_name: ''
      subject: _default
      body: _default
      excluded_elements: {  }
      ignore_access: false
      exclude_empty: true
      exclude_empty_checkbox: false
      exclude_attachments: false
      html: true
      attachments: false
      twig: false
      theme_name: ''
      parameters: {  }
      debug: false
variants: {  }
