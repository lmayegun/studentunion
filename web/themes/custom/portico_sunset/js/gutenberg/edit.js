(function (wp, drupalSettings) {
    
    drupalSettings.gutenberg._listeners.init.push(() => {
        // Disable unwanted styles
        // https://www.drupal.org/project/gutenberg/issues/3166966
        wp.blocks.unregisterBlockStyle( 'core/quote', 'large' );
        wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
        wp.blocks.unregisterBlockStyle( 'core/image', 'rounded' );
        wp.blocks.unregisterBlockStyle( 'core/separator', 'wide' );
        wp.blocks.unregisterBlockStyle( 'core/separator', 'dots' );

    });

    wp.domReady( function() {            
        // Add custom styles
        // // https://www.drupal.org/project/gutenberg/issues/3064924
        // wp.blocks.registerBlockStyle("core/button", {
        //     name: "upper-case",
        //     label: "Upper Case"
        // }); 
      
    });  

})(wp, drupalSettings);