
jQuery(document).ready(function ($) {
    //   console.log('setupTableOfContents');

    // table of contents
    var elementTocContainer = '.container-toc';
    var elementTocRegion = '.region-toc';
    if ($(elementTocRegion).length >= 1) {
        var minimumForTableToShow = 3;
        var h2s = $('article h2');
        var elementAbove = '#header';

        function tocLinkClick(index) {

            console.log('tocLinkClick', '#toc-link-' + index);
            if (index == 0) {
                $('html, body').animate({
                    scrollTop: 0
                }, 200);
            } else {
                topValue = $('#anchor' + index).offset().top - $(elementTocRegion).height() - 50;
                console.log('SCROLL TO', topValue);
                $('html, body').animate({
                    scrollTop: topValue
                }, 200);
            }
        }

        function tocUpdateCurrent() {
            var easement = (0.5 * $(window).height()); // switch to next heading if within 50% of the window of it
            var atBottom = $(window).scrollTop() + $(window).height() >= $(document).height() - easement;
            var atTop = $(window).scrollTop() < jQuery('.container-toc').offset().top;

            if (atTop) {
                var index = 0;
            } else if (atBottom) {
                var index = $('.toc-anchor').length;
            } else {
                // Store the positions of all the headers
                let elements = $('.toc-anchor').toArray();
                console.log('elements', elements);
                let linkCoords = elements.map(link => {
                    // let rect = link.closest(elementAbove).getBoundingClientRect();
                    let rect = link.getBoundingClientRect();
                    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
                        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
                    return [rect.x + scrollLeft, rect.y + scrollTop];
                });
                console.log('linkCoords', linkCoords);

                // Get current position and get closest to it
                var x = '80';
                var y = window.scrollY;
                let distances = [];
                linkCoords.forEach(linkCoord => {
                    if (linkCoord[1] <= parseInt(y) + easement) {
                        let distance = Math.hypot(linkCoord[0] - parseInt(x), linkCoord[1] - parseInt(y));
                        distances.push(parseInt(distance));
                    } else {
                        distances.push(9999);
                    }
                });
                let closestLinkIndex = distances.indexOf(Math.min(...distances));
                var closest = elements[closestLinkIndex].id;
                console.log(closest);

                // Add class to link for closest anchor
                var index = closest.match(/\d+/)[0];
                console.log("Passing index ", index);
            }
            
            $.each($('.toc-anchor'), function (anchor_index, element) {
                loop_index = anchor_index + 1;
                console.log('Classifying ', '#toc-link-' + loop_index, 'anchor', anchor_index);
                var link = $('#toc-link-' + loop_index);
                if (loop_index == index) { // || (index == $('.toc-anchor').length && loop_index == index - 1)) {
                    link.addClass('current');
                    link.removeClass('toc-before');
                    link.removeClass('toc-after');
                } else {
                    link.removeClass('current');
                    if (loop_index < index) {
                        link.addClass('toc-before');
                        link.removeClass('toc-after');
                    } else {
                        link.addClass('toc-after');
                        link.removeClass('toc-before');
                    }
                }
            });

            if (index >= 1) {
                $('.toc-link-top').show();
            } else {
                $('.toc-link-top').hide();
            }
        }

        // console.log(h2s);

        if (h2s.length >= minimumForTableToShow) {
            index = 0;
            // Start 
            $(elementTocRegion).append('<a id="toc-link-0" style="display: none;" class="toc-link toc-before toc-link-top" href="#">Start</a>&nbsp;');
            $('#toc-link-0').on('click', function () {
                tocLinkClick(0);
            });

            // H2s 
            $.each(h2s, function (i, element) {
                let index = i + 1;
                console.log('$.each(h2s', index, '#toc-link-' + index);
                $(elementTocRegion).append('<a id="toc-link-' + index + '" class="toc-link" href="#">' + $(this).text() + '</a>&nbsp;');
                $('#toc-link-' + index).on('click', function () {
                    tocLinkClick(index);
                });
                $(this).prepend('<a class="toc-anchor" id="anchor' + index + '"></a>');
            });

            $(elementTocContainer).slideDown();

            $(window).on('load scroll', function (e) {
                tocUpdateCurrent();
            });
        } else {
            $(elementTocContainer).hide();
        }
    }
});