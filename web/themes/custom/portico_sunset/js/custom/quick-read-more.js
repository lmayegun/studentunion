// Turns anything with a 'data-read-more-label' into a slide down "read more" style thing

function quickReadMoreOpen(id) {
    div = jQuery('[data-read-more-id=' + id + ']');
    div.slideDown().next('a').hide().next('a').show();
}

function quickReadMoreClose(id) {
    div = jQuery('[data-read-more-id=' + id + ']');
    div.slideUp().next('a').show().next('a').hide();
}

jQuery(document).ready(function ($) {
    // console.log('quick-read-more');
    i = 0;
    $('[data-read-more-label]').each(function () {
        $(this).hide().attr('data-read-more-id', i);
        $(this).after('&nbsp;<a href="#z" style="display:none;" onclick="quickReadMoreClose(' + i + ');">Hide</a>');
        $(this).after('&nbsp;<a href="#z" onclick="quickReadMoreOpen(' + i + ');">' + $(this).attr('data-read-more-label') + '</a>');
        i++;
    });
});