/**
 * Main menu
 */

jQuery(function ($) {
  $(window).scroll(function () {
    var topofDiv = $("#menu").offset().top; //gets offset of header
    var height = $("#menu").outerHeight(); //gets height of header
    if ($(window).scrollTop() > (topofDiv + height)) {
      $(".cart-block--contents.is-outside-horizontal").slideUp();
    }
  });
});

jQuery(function ($) {
  // Sort search
  $searchElement = '#block-exposedformsearch-publicpage-1';
  $searchEditElement = '#edit-term--2';
  $menuElementToSlide = '.tb-megamenu-main';

  $formInputContainer = $($searchElement + ' .form-item').eq(0);

  // Search
  toggleSearch = function (open) {
    // Make all the parents of the form flex
    if (open) {
      console.log('toggleSearch true');

      $($menuElementToSlide).css('display', 'inline-flex !important').animate({ width: 'toggle' }, 50);
      $('#block-responsivemenumobileicon').addClass('hidden-for-search').show();

      if (parseInt($(window).width()) < 640) {
        var toHide = ['#block-portico-sunset-branding', '#block-cart', '#block-portico-sunset-account-menu--2'];
      } else {
        var toHide = [];
      }
      console.log(parseInt($(window).width()), toHide);
      toHide.forEach(item => {
        console.log(item, $(item).is(':visible'));
        if ($(item).is(':visible')) {
          $(item).addClass('hidden-for-search').hide();
        }
      });

      $($searchElement).css('display', 'flex').css('flex-grow', 1);
      $formInputContainer.css('flex-grow', 1).animate({ display: 'inline-block', width: '100%' }, 200).css('display', 'inline-flex');
      $($searchEditElement).css('width', '100%');
      $($searchElement + ' input').get(0).focus();
      $('.search-api-autocomplete-search').show();
    } else {
      console.log('toggleSearch false');

      $($menuElementToSlide).css('display', 'flex !important').animate({ width: 'toggle' }, 50);

      $('.hidden-for-search').show();

      $($searchElement).css('display', 'inline-block').css('flex-grow', 'unset');
      $($searchElement + ' .form-item').css('display', 'none');
      $formInputContainer.css('flex-grow', 0).animate({ display: 'inline-block', width: '0px' }, 200);
      $('.search-api-autocomplete-search').hide();

    }
  };

  // Open the search if a term exists
  if ($($searchEditElement).val() && $($searchEditElement).val() != '') {
    console.log('Search query filled');
    toggleSearch(true);
    $('#page-title').html($($searchEditElement).val());
  }

  // Close the search after a bit if it loses focus
  searchTimeout = false;
  $($searchEditElement).on('focusout', function (event) {
    searchTimeout = setTimeout(function () {
      toggleSearch(false);
    }, 600)
  });
  $($searchEditElement).on('focus', function (event) {
    searchTimeout = false;
  });

  $($searchElement + ' .form-submit').on('click', function (event) {
    $formInputContainer = $($searchElement + ' .form-item').eq(0);
    $empty = $($searchElement + ' .form-item input').val() == '';
    if ($formInputContainer && $formInputContainer.is(':visible')) {
      if ($empty) {
        event.preventDefault();
        toggleSearch(false);
      }
      // Do submit as normal
    } else {
      event.preventDefault();
      toggleSearch(true);
    }
  });
});
