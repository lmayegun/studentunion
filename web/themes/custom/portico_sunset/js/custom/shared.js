jQuery(document).ready(function ($) {
  // Election candidates
  // https://jedfoster.com/Readmore.js
  defaultOptions = {
    speed: 75,
    collapsedHeight: 150,
    moreLink: '<a href="#">Read more</a>',
    lessLink: '<a href="#">Read less</a>',
    blockCSS: 'z-index: 5;',
    afterToggle: function (trigger, element, expanded) {
      if (expanded) {
        element.removeClass('bottom-fade-to-white');
      } else {
        element.addClass('bottom-fade-to-white');
      }
    }
  };
  electionOptions = defaultOptions;
  electionOptions.collapsedHeight = 150;
  $('.election_candidate .field--name-statement').readmore(electionOptions).css('overflow-y', 'hidden').addClass('bottom-fade-to-white');

  // Prevent worldnet button being disabled
  $('input, select').on('change', function () {
    $('#submitBtn').prop('disabled', false);
  });
});

// Prevent worldnet button being disabled
jQuery('#worldnetFrame').on("load", function () {
  console.log('#worldnetFrame load');
  $('input, select').on('change', function () {
    $('#submitBtn').prop('disabled', false);
  });
});
