/**
* Add Javascript - Node features JS
*/
jQuery(document).ready(function ($) {
  // Deal with full pages, putting non-full-width elements into a
  $('#page.page-node-type-page_full .field--name-body').addClass('g-2');
  // $('#page.page-node-type-page_full .field--name-body > *:not(.alignfull):not(.container)').wrap("<div class='container'></div>");
  $('.breadcrumb').wrap("<div class='container g-1'></div>");
});

