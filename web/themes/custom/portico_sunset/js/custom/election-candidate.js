(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.candidateReadMore = {
    attach: function (context) {
      if (context !== document) {
        return;
      }
      $('.election_candidate .text_section').each(function () {
        console.log('.election_candidate .read_more');

        if ($(this).height() >= 250) {
          $(this).addClass('text_section__rolled_up');
          var readMore = $(this).next('.read_more_link');
          readMore.addClass('read_more_link__visible');
          readMore.on('click', function () {
            var rolledUp = $(this).prev('.text_section').hasClass('text_section__rolled_up');
            console.log('rolledUp', rolledUp);
            if (rolledUp) {
              $(this).html('Show less');
              $(this).prev('.text_section').removeClass('text_section__rolled_up');
            } else {
              $(this).html('Read more');
              $(this).prev('.text_section').addClass('text_section__rolled_up');
            }
            $(this).parent()[0].scrollIntoView();
          });
        }
      });

    }
  }

})(jQuery, Drupal, drupalSettings);
