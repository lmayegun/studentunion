
jQuery(document).ready(function ($) {
    // Cycle through text headers if there's only one slide
    if ($('.front_page_slider').length == 1) {
        nextHeading = function () {
            array = JSON.parse($('.front_page_slider .page-title').attr('data-array'));
            current = $('.front_page_slider .page-title').html();
            currentIndex = array.indexOf(current);
            nextIndex = (currentIndex == array.length - 1 ? 0 : currentIndex + 1);
            $('.front_page_slider .page-title').fadeOut(200, function () {
                $(this).text(array[nextIndex]).fadeIn(300);
            });
        }
        setInterval(function () {
            nextHeading();
        }, 4000);
    }
});

//   /**
//    * Front page image slider and typewriter effect
//    */
//   jQuery(document).ready(function($) {
//       slideLength = 6000;
//       typewriterSpeed = 8;
//       //   textToKeep = 'Where ';

//       target = document.getElementById('page-title');
//       var typewriter = new Typewriter(target, {
//           'loop': true,
//           'cursor': '',
//           'delay': typewriterSpeed,
//           'deleteSpeed': typewriterSpeed / 2,
//           'skipAddStyles': true
//       });

//       getSlideValues = function(counter) {
//           newSlide = rows.eq(counter);
//           url = newSlide.find('img').attr('src');
//           text = $.trim(newSlide.find('.views-field-name .field-content').text());
//           return { 'url': url, 'text': text };
//       }

//       setSlideValues = function(url, text) {
//           $('#header-background').css('background-image', 'url(' + url + ')');

//           // https://safi.me.uk/typewriterjs/
//           typewriter.typeString(text).start();
//       }

//       deleting = null;
//       writing = null;
//       setDeleteTimer = function(text) {
//           if (document.hasFocus()) {
//               // Start deleting relative to how much text there is.
//               timer = slideLength - ((typewriterSpeed * text.length) * 4);
//               charsToDelete = text.length; // - textToKeep.length
//               deleting = setTimeout(function() {
//                   typewriter.deleteChars(charsToDelete).start();
//               }, timer)
//           }
//       }

//       var rows = $('.front-page-image');
//       if (rows.length > 0) {
//           currentSlideCounter = 0;

//           // Initial slide
//           values = getSlideValues(currentSlideCounter);
//           rows.eq(currentSlideCounter).addClass('no-transition');
//           //   $('#header-background').css('background-image', 'url(' + values.url + ')');
//           //   $('h1.page-title').html(values.text);
//           setSlideValues(values.url, values.text);
//           setDeleteTimer(values.text);
//           rows.eq(currentSlideCounter).removeClass('no-transition');

//           changeSlide = function() {
//               typewriter.deleteAll().start();
//               currentSlideCounter++;
//               if (rows.length == currentSlideCounter) {
//                   currentSlideCounter = 0;
//               }

//               values = getSlideValues(currentSlideCounter);
//               setSlideValues(values.url, values.text);
//               return values;
//           }

//           setSlideInterval = function() {
//               writing = setInterval(function() {
//                   if (document.hasFocus()) {
//                       values = changeSlide();
//                       setDeleteTimer(values.text);
//                   }
//               }, slideLength);
//           }
//           setSlideInterval();
//       }
//   });