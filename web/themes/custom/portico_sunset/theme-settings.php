<?php

function portico_sunset_form_system_theme_settings_alter(&$form, &$form_state)
{

  // $form['#attached']['library'][] = 'portico_sunset/theme-settings';

  $form['portico_sunset_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['portico_sunset_settings']['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'basic_tab',
  );

  $form['portico_sunset_settings']['basic_tab']['basic_settings'] = array(
    '#type' => 'details',
    '#title' => t('Basic Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'tabs',
  );

  $form['portico_sunset_settings']['basic_tab']['basic_settings']['header'] = array(
    '#type' => 'item',
    '#markup' => '<div class="theme-settings-title">' . t("Header positioning") . '</div>',
  );

  $form['portico_sunset_settings']['basic_tab']['basic_settings']['fixed_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed position'),
    '#description'   => t('Use the checkbox to apply fixed position to the header.'),
    '#default_value' => theme_get_setting('fixed_header', 'portico_sunset'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['portico_sunset_settings']['basic_tab']['basic_settings']['scrolltop'] = array(
    '#type' => 'item',
    '#markup' => '<div class="theme-settings-title">' . t("Scroll to top") . '</div>',
  );

  $form['portico_sunset_settings']['basic_tab']['basic_settings']['scrolltop_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show scroll-to-top button'),
    '#description'   => t('Use the checkbox to enable or disable scroll-to-top button.'),
    '#default_value' => theme_get_setting('scrolltop_display', 'portico_sunset'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['portico_sunset_settings']['bootstrap_tab']['bootstrap'] = array(
    '#type' => 'details',
    '#title' => t('Bootstrap'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'tabs',
  );

  $form['portico_sunset_settings']['bootstrap_tab']['bootstrap']['bootstrap_remote_type'] = array(
    '#type' => 'select',
    '#title' => t('Select the remote type'),
    '#description'   => t('From the drop down select box, select how to load the Bootstrap library. If you select "Local" make sure that you download and place Bootstrap folder into the root theme folder (portico_sunset/bootstrap).'),
    '#default_value' => theme_get_setting('bootstrap_remote_type', 'portico_sunset'),
    '#options' => array(
      'local' => t('Local / No remote'),
      'cdn' => t('CDN'),
    ),
  );

  $form['breadcrumbs'] = [
    '#type' => 'details',
    '#title' => t('Breadcrumb'),
    '#open' => TRUE,
  ];

  $form['breadcrumbs']['hide_home_breadcrumb'] = array(
    '#type' => 'radios',
    '#options' => [
      '0' => 'Show <em>Home</em> in breadcrumb trail (Drupalâ€™s default behavior)',
      '1' => 'Remove <em>Home</em> from breadcrumb trail',
      '2' => 'Remove <em>Home</em> when it is the only breadcrumb in trail',
    ],
    '#default_value' => theme_get_setting('hide_home_breadcrumb', 'iu'),
  );
}
