<?php

namespace Drupal\commerce_cart_webform\Plugin\views\field;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("commerce_cart_webform_submission_for_order_item")
 */
class OrderItemWebformSubmission extends FieldPluginBase {

    /**
     * @{inheritdoc}
     */
    public function query() {
        // Leave empty to avoid a query on this field.
    }

    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
        $options = parent::defineOptions();

        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        parent::buildOptionsForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values) {
        $orderItem = $this->getEntity($values);

        $submissions = $orderItem->cart_webform_submission->referencedEntities();
        $li = [];
        foreach($submissions AS $submission) {
            $li[] = $submission->toLink();
        }

        return '<ul><li>'.implode('</li><li>', $li).'</li></ul>';
    }
}
