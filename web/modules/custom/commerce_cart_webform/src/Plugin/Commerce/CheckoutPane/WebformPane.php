<?php

namespace Drupal\commerce_cart_webform\Plugin\Commerce\CheckoutPane;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\InlineFormManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a custom message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_cart_webform_pane",
 *   label = @Translation("Webforms"),
 *   display_label = @Translation("Additional information based on your cart"),
 *   wrapper_element = "fieldset",
 * )
 */
class WebformPane extends CheckoutPaneBase {
  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // Check whether the order has an item that requires the disclaimer message.
    foreach ($this->order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();
      if ($purchased_entity->get('cart_webform')->entity && $purchased_entity->cart_webform_location->value == 'checkout') {
        return TRUE;
      }
    }

    return FALSE;
  }


  /**
   * Constructs a new BillingInformation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->inlineFormManager = $inline_form_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    foreach ($this->order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();
      $count = 0;
      if ($purchased_entity->get('cart_webform')->entity && $purchased_entity->cart_webform_location->value == 'checkout') {
        $webform = $purchased_entity->get('cart_webform')->entity;
        $inline_form = $this->inlineFormManager->createInstance('webform_inline_form', [
          'webform_id' => $webform->id(),
        ], $webform);

        $inline_form_id = $webform->id() . '_' . $count;
        $pane_form[$inline_form_id] = [
          '#parents' => array_merge($pane_form['#parents'], [$inline_form_id]),
        ];
        $pane_form[$inline_form_id] = $inline_form->buildInlineForm($pane_form[$inline_form_id], $form_state);
      }
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    // $this->order->setData('order_comment', $values['comment']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $summary = [];
    foreach ($this->order->getItems() as $order_item) {
      $submissions = $order_item->cart_webform_submission->referencedEntities();
      if (count($submissions) > 0) {
        foreach ($submissions as $submission) {
          $view_builder = \Drupal::service('entity_type.manager')->getViewBuilder('webform_submission');
          $build        = $view_builder->view($submission);
          $summary[] = [
            '#markup' => \Drupal::service('renderer')->render($build),
          ];
        }
      }
    }
    return $summary;
  }
}
