CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module adds fields to all variations to allow requiring a webform be submitted when purchasing a product variation.


REQUIREMENTS
------------

This module requires Webform, Commerce, Commerce Checkout and Commerce Cart.


INSTALLATION
------------

Install the Commerce Cart Webform module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

From there, if you plan to use the checkout pane functionality, go to Commerce > Configuration > Orders > Checkout flows, edit the relevant flows, and drag the "Webforms" pane up from Disabled.

For product variation types you would like to use webforms for, Manage Form Display and add the commerce_cart_webform fields to the form.


CONFIGURATION
-------------




MAINTAINERS
-----------

 * Maxwell Keeble https://drupal.org/u/maxwellkeeble

