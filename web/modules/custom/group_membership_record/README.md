# Group membership record

# What it does

Groups currently allow users to be assigned roles within the group. But what if roles are held by different people over time, and you want to keep track of the full history of who has had what roles within the group?

Or someone can have a single role in terms of group permissions, e.g. "Committee member", but be in that role in various different ways (e.g. different kinds of committee members), and you'd want to keep track of each of those ways separately without having a role for each.

This module allows you to track as many 'instances' of a user having a role as you like, and assign fields to those instances to collect any data that you like. So, group roles become not just controlling current access to the group via permissions, but also a record of any other information you might want, and a history of all previous variations of how a user has had that role.

It also out of the box allows for two key management tools (both optional):

- an instance can be 'current', based on a date range, which allows restricting group access based on whether today's date falls within that date range. This means e.g. you could assign someone a role instance for a specific period, and they'd only get the permissions of that role for as long as the instance is current.

- an instance can be 'enabled', based on either manually checking or unchecking a boolean field, OR via providing a plugin in a custom module defining what it means for an instance to be current.

# Use cases

For an academic setting, this might be:

- Class president - has certain access to the group only for a specified amount of time, but we want to keep a record of them having been president or that they will be President

- Volunteer group representative - has certain access as above, and we record their role based on dates - but they need to complete training to have access to the group, so we have a custom EnabledDeterminer plugin to only enable the group membership record once that training is complete

For Students' Union UCL, the developers, this would be used for:

- Clubs and Societies - standard members and committee officers
- Volunteering organisations - leaders and volunteers
- Volunteering opportunities - expression of interest and placement
- Elected representatives (each constituency is a Group)
- Committees

# How to use

The module expects you have existing group types and roles within those groups.

1. Create "group membership record types" - these are the different sets of fields you might want to associate with given group roles.
   - Name it and provide some fields. admin/group/membership_record/type
   - Often you'll have a group membership record type for each group role, e.g. if you have a 'Committee member' role, you would create a 'Committee member' role instance type and assign it to it.
   - However, you could e.g. have President, Treasurer and Secretary roles in the group, but want to track the same information for all of them. So, you create a more generic 'Society committee officer' group membership record type, and assign that same type to each of the three roles.

2. After creating the instance type, edit the group role and link the role to the "group membership record type" you created. There are some other options available when editing the group role that are explained in the form, e.g. to restrict access based on enabled or current fields.

3. Group membership records are then created in a few ways:
   - Through the "Group membership records" entity list and default form - not recommended as it can be complex
   - Custom modules provide custom forms extending the GroupMembershipRecordForm form class - e.g. create a custom "Add Club President" form. This class allows for: setting the form to default to or limit to, roles, group types, groups etc, including e.g. limiting available roles to a specified group type; autocompleting values on the form by query string; customising the language so your users never need to know about the notion of "group membership records".
   - Custom modules can create instances as needed, based on whatever triggers they need (e.g. role being granted, purchase being made, etc).
   - If you selected 'create by default' on the group role form, an instance will automatically be made for the user in that role whenever the role is assigned.

4. Views can then be made showing group info, role, and each instance.
  - Views access can be limited by the normal Group access option - Group Membership Record intercepts the normal Group permissions if a role has been limited by being enabled and/or date controlled.
  - For Group views, you can get a count of current and/or enabled records using "Group membership record count" field (MembershipRecordsForGroupCount)


# Installation

As normal.

# How to use in custom modules

- Call \Drupal::service('group_membership_record.manager')->create() where needed to create an instance or optionally assign the role while creating an instance. In a custom module, you'd generally want this to be triggered when a user is granted a role - in the Group module, this could happen when a group_membership type of GroupContent is inserted or updated, e.g:

    ```php
    function hook_membership_record_group_content_insert(GroupContent $entity) {
      $group_content_type = $entity->getGroupContentType();
      if ($group_content_type->get('content_plugin') == 'group_membership') {
          // User has joined group for first time
          // \Drupal::service('group_membership_record.manager')->create() here
      }
    }

    function hook_group_content_update(GroupContent $entity) {
      $group_content_type = $entity->getGroupContentType();
      if ($group_content_type->get('content_plugin') == 'group_membership') {
          // User's membership has changed - could have new roles or have had roles removed
          // If new roles:
            // \Drupal::service('group_membership_record.manager')->create() here
      }
    }
    ```

- Create new forms at new routes, extending the existing ones, to make form variations, e.g. if you want a custom 'end instance' form. Do this by:

    1. Create form in custom module extending GroupMembershipRecordForm - specify the protected values at the top of the GroupMembershipRecordForm class to make sure the functionality works to limit what groups and roles the user can select. Then you can extend / modify the form as much as you'd like.

    2. Define a form class and assign it to the group_membership_record entity in hook_entity_type_alter. HEre, we name it "su_clubs_societies_committee_member" but the form class that extends GroupMembershipRecordForm is "CommitteeMemberForm"

        ```php
        function su_clubs_societies_entity_type_alter(array &$entity_types) {
          $entity_types['group_membership_record']->setFormClass('su_clubs_societies_committee_member', CommitteeMemberForm::class);
        }
        ```

   3. Create a route in your module's *.routing.yml, with _entity_form being followed by the form class defined above. NOTE bundle type must be put in to the path, though if you provide a default you do not need to include it in the link you use:

        ```yml
        su_clubs_societies.committee_record.add:
          path: '/admin/clubs-societies/committee-record/add/{group_membership_record_type}'
          defaults:
            _title: Add committee member
            _entity_form: 'group_membership_record.su_clubs_societies_committee_member'
            group_membership_record_type: 'club_society_committee'
          requirements:
            _permission: 'administer committee record'
          options:
            parameters:
              group_membership_record_type:
                type: entity:group_membership_record_type
        ```

   4. Add route as action link in *.links.action.yml, or wherever, e.g. in module.links.action.yml this adds the form to a specific view with id club_society_committee_record:

        ```yml
        su_clubs_societies.committee_record.add_form:
          route_name: su_clubs_societies.committee_record.add
          title: 'Add committee member'
          appears_on:
            - view.club_society_committee_record.list
        ```

- Wherever you need to check if a user has a current role, use \Drupal::service('group_membership_record.manager')->currentInstanceExists() rather than standard Group membership checks

- In views:

  - Filter as normal with the Enabled field, or filter by 'Instance is current' to get past present or future

# Development

Contributed by Students' Union UCL.

## Todo for release

- Webform access integration - user can select a group or specify another element to provide the group, then specify the role and whether the instance needs to be current and/or enabled

- Prevent instances from appearing in views if the user no longer has the relevant role in that group, by default (because one setting allows you to not delete the instances when the role is unassigned) - or a views filter plugin to do this

- Relationship with subgroup - e.g. if we want a [placement] in an opp to only provide [volunteer] role in parent when 'current'.

## Nice to haves

- Custom add and edit routes (see FormRoutes.php)
-
- New view plugin to summarise instances for user and/or user in role as a list.

- Tab on Group listing all GRIs? Or just rely on devs/sites to create their own views.

- Views value counting current and/or enabled people with this role for a group.

- Ability to manage instances on the standard Group membership creation form

- Inline entity form that can be used in views to edit

- Option to create an instance by default for all existing roles when new group membership record type created / assigned to a role
-
- Tool / function to update enabled field for all instances for group, role, or user
