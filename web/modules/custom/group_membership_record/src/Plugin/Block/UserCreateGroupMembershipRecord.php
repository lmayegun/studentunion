<?php

namespace Drupal\group_membership_record\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Entity\EntityFormMode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupRole;
use Drupal\group\Plugin\GroupContentEnabler\GroupMembership;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "user_create_group_membership",
 *   admin_label = @Translation("Join with group membership record (user-facing)"),
 * )
 */
class UserCreateGroupMembershipRecord extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $group = group_membership_record_get_route_entity();
    if ($group->getEntityTypeId() !== 'group') {
      return [];
    }
    $groupRole = GroupRole::load($this->configuration['role_to_grant']);
    $account = User::load(\Drupal::currentUser()->id());

    if (\Drupal::currentUser()->isAnonymous() && $this->configuration['login_notice_show']) {
      $text = $this->configuration['login_notice_text'] != '' ? '<p>' . $this->configuration['login_notice_text'] . '</p>' : null;

      $link = Url::fromRoute('user.login', [], [
        'absolute' => TRUE,
        'query' => ['destination' => \Drupal::request()->getRequestUri()]
      ])->toString();
      $button = '<p><a href="' . $link . '" class="button">Log in</a></p>';

      return [
        '#markup' => $text . $button,
      ];
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $alreadyJoined = $gmrRepositoryService->currentEnabledInstanceExists($account, $group, $groupRole);
    if ($alreadyJoined) {
      if ($this->configuration['already_joined_text'] && $this->configuration['already_joined_text'] != '') {
        return [
          '#markup' =>  $this->configuration['already_joined_text'],
        ];
      } else {
        return [
          '#markup' =>  $this->t('You have already joined.'),
        ];
      }
    }

    if (isset($this->configuration['group_permission']) && $this->configuration['group_permission'] != '') {
      $access = $group->hasPermission($this->configuration['group_permission'], $account);
      if (!$access) {
        if ($this->configuration['no_permission_text'] && $this->configuration['no_permission_text'] != '') {
          return [
            '#markup' =>  $this->configuration['no_permission_text'],
          ];
        } else {
          return [
            '#markup' =>  $this->t('You do not have permission to join.'),
          ];
        }
      }
    }

    $text = $this->configuration['text'] && $this->configuration['text'] != '' ? '<p>' . $this->configuration['text'] . '</p>' : '';

    $link = Url::fromRoute('entity.group_membership_record.join_form', [
      'group' => $group->id(),
      'group_role' => $this->configuration['role_to_grant']
    ], [
      'absolute' => TRUE,
    ])->toString();
    $button = '<p><a href="' . $link . '" class="button button-group-join">' . $this->configuration['button_text'] . '</a></p>';

    return [
      '#markup' => $text . $button,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account)
  {
    $access = AccessResult::allowedIfHasPermission($account, 'access content');
    $access = $access->andIf(AccessResult::allowedIf(\Drupal::routeMatch()->getRouteName() != 'entity.group_membership_record.join_form'));

    $group = group_membership_record_get_route_entity();
    if ($group) {
      $access = $access->andIf(AccessResult::allowedIf($group->isPublished()));
    }
    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $config = $this->getConfiguration();

    $form['role_to_grant'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'group_role',
      '#title' => $this->t('Group role to grant'),
      '#default_value' => isset($this->configuration['role_to_grant']) ? GroupRole::load($this->configuration['role_to_grant']) : null,
      '#required' => TRUE,
      '#selection_handler' => 'group_role:not_internal'
    ];

    $form['group_permission'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group permission needed'),
      '#default_value' => $this->configuration['group_permission'] ?? '',
      '#required' => FALSE,
    ];

    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text to show user'),
      '#default_value' => $this->configuration['text'],
      '#required' => FALSE,
    ];

    $form['button_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text for button, e.g. Join'),
      '#default_value' => $this->configuration['button_text'] ?? 'Join this group',
      '#required' => TRUE,
    ];

    $form['login_notice_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show a notice to log in if user logged out.'),
      '#default_value' => $this->configuration['login_notice_show'] ?? '',
    ];

    $form['login_notice_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice for logged out users (shown if enabled)'),
      '#default_value' => $this->configuration['login_notice_text'] ?? '',
      '#description' => $this->t("Only shown if enabled above. A button saying 'log in' is shown afterwards."),
      '#required' => FALSE,
    ];

    $form['already_joined_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice for users already joined with this role. If not entered, block is just hidden.'),
      '#default_value' => $this->configuration['already_joined_text'] ?? '',
      '#required' => FALSE,
    ];

    $form['no_permission_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice for users who do not have permission to join.'),
      '#default_value' => $this->configuration['no_permission_text'] ?? '',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['role_to_grant'] = $form_state->getValue('role_to_grant');
    $this->configuration['text'] = $form_state->getValue('text');
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['login_notice_show'] = $form_state->getValue('login_notice_show');
    $this->configuration['login_notice_text'] = $form_state->getValue('login_notice_text');
    $this->configuration['already_joined_text'] = $form_state->getValue('already_joined_text');
    $this->configuration['group_permission'] = $form_state->getValue('group_permission');
    $this->configuration['no_permission_text'] = $form_state->getValue('no_permission_text');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags()
  {
    return Cache::mergeTags(parent::getCacheTags(), ['group_membership_record_list']);
  }

  public function getCacheContexts()
  {
    return Cache::mergeContexts(parent::getCacheContexts(), array('user', 'route.group'));
  }

  /**
   * @return int
   */
  public function getCacheMaxAge()
  {
    return 60 * 60; // 1 hour
  }
}
