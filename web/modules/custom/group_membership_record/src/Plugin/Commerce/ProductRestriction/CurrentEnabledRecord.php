<?php

namespace Drupal\group_membership_record\Plugin\Commerce\ProductRestriction;

use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginBase;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Provides product restriction by user role.
 *
 * @ProductRestrictionPlugin(
 *   id = "group_membership_record_current_enabled",
 *   label = @Translation("Restrict to current enabled group membership record"),
 *   description = @Translation(""),
 *   category = @Translation("User"),
 *   entity_type = "commerce_product",
 *   entity_bundles = {},
 *   weight = -1
 * )
 */
class CurrentEnabledRecord extends ProductRestrictionPluginBase implements ProductRestrictionPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'group_id' => [],
      'group_role_id' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['group_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Group(s)',
      '#target_type' => 'group',
      '#tags' => TRUE,
      '#default_value' => $this->getGroups(),
      '#selection_handler' => 'default',
      '#required' => TRUE,
    ];

    $form['group_role_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Role(s)',
      '#target_type' => 'group_role',
      '#tags' => TRUE,
      '#default_value' => $this->getRoles(),
      '#selection_handler' => 'group_role:not_internal',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['group_id'] = $values['group_id'];
    $this->configuration['group_role_id'] = $values['group_role_id'];
  }

  public function getGroups() {
    return $this->configuration['group_id'] ? Group::loadMultiple(array_column($this->configuration['group_id'], 'target_id')) : [];
  }

  public function getRoles() {
    return $this->configuration['group_role_id'] ? GroupRole::loadMultiple(array_column($this->configuration['group_role_id'], 'target_id')) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    $account = User::load(\Drupal::currentUser()->id());

    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    if ($this->getGroups() && $this->getRoles()) {
      foreach ($this->getGroups() as $group) {
        foreach ($this->getRoles() as $role) {
          if ($gmrRepositoryService->currentEnabledInstanceExists($account, $group, $role)) {
            return TRUE;
          }
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function accessErrorMessage($product_or_variation) {
    if ($this->getGroups() && $this->getRoles()) {
      $groupLabels = [];
      $roleLabels = [];
      foreach ($this->getGroups() as $group) {
        $groupLabels[] = $group->label();
      }

      foreach ($this->getRoles() as $role) {
        $roleLabels[] = $role->label();
      }

      return new TranslatableMarkup(
        'To purchase this product, you need to have current and enabled records with one of the role(s) "@roles" in one of the groups "@groups".',
        [
          '@roles' => implode(', ', $roleLabels),
          '@groups' => implode(', ', $groupLabels),
        ]
      );
    } else {
      return new TranslatableMarkup(
        'You cannot purchase this product because it is not configured correctly.',
        []
      );
    }
  }
}
