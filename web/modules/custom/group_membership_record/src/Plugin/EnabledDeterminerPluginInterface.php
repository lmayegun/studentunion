<?php

namespace Drupal\group_membership_record\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * An interface for enabled determiner plugins.
 *
 */
interface EnabledDeterminerPluginInterface extends PluginInspectionInterface
{
    /**
     * Provide a description of the plugin.
     *
     * @return string
     *   A string description of the plugin.
     */
    public function description();

    /**
     * Provide settings array
     *
     * @param null $setting_key
     *    A specific setting key
     * @return mixed
     *    A settings field keys array
     */
    public function settings($setting_key = null);
}
