<?php

namespace Drupal\group_membership_record\Plugin\views\filter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group\Entity\GroupType;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Views;

/**
 * Filter GMR by role (excluding internal Group roles that are confusing to the user)
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("group_membership_record_group_role_non_internal")
 */
class GroupRoleNonInternal extends FilterPluginBase {
  public function hasExtraOptions() {
    return TRUE;
  }


  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    return $this->valueOptions;
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['group_types'] = ['default' => NULL];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $group_types_loaded = GroupType::loadMultiple();
    $group_types = [];
    foreach ($group_types_loaded as $type) {
      $group_types[$type->id()] = $type->label();
    }

    $form['group_types'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Group types to filter roles by'),
      '#default_value' => $this->options['group_types'] ?? NULL,
      '#options' => $group_types,
    ];
  }

  protected function valueForm(&$form, FormStateInterface $form_state) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $roles = $gmrRepositoryService->getAllNonInternalRoles($this->options['group_types'] ?? null);
    $options = ['all' => 'All roles'];
    foreach ($roles as $role_id => $role) {
      $options[$role_id] = $role->label();
    }

    $form['value'] = [
      '#tree' => TRUE,
      'roles' => [
        '#type' => 'select',
        '#title' => $this->t('Group roles'),
        '#options' => $options,
        '#multiple' => 'TRUE',
        '#default_value' => !empty($this->value['roles']) ? $this->value['roles'] : [],
      ]
    ];
  }

  /**
   * Applying query filter. If you turn on views query debugging you should see
   * these clauses applied. If the filter is optional, and nothing is selected, this
   * code will never be called.
   */
  public function query() {
    $this->ensureMyTable();

    if (count($this->value['roles']) > 0 && !in_array('all', $this->value['roles'])) {
      $this->query->addWhere($this->options['group'], "group_role_id", $this->value['roles'], 'IN');
    }
  }

  /**
   * Display the filter on the administrative summary
   */
  public function adminSummary() {
    return $this->operator . ' ' . implode(',', $this->value['roles']);
  }
}
