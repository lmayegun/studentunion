<?php

namespace Drupal\group_membership_record\Plugin\views\filter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Views;

/**
 * Past/Present/Future event filter.
 *
 * // https://www.lilengine.co/articles/custom-views-filter-existing-daterange-field
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("group_membership_record_views_filter_currency")
 */
class InstanceCurrency extends FilterPluginBase
{

  protected function valueForm(&$form, FormStateInterface $form_state)
  {
    $form['value'] = [
      '#tree' => TRUE,
      'state' => [
        '#type' => 'select',
        '#title' => $this->t('Past, current or future'),
        '#options' => [
          'all' => $this->t('All'),
          'current' => $this->t('Current'),
          'past' => $this->t('Past'),
          'future' => $this->t('Future'),
        ],
        '#default_value' => !empty($this->value['state']) ? $this->value['state'] : 'all',
      ]
    ];
  }

  /**
   * Applying query filter. If you turn on views query debugging you should see
   * these clauses applied. If the filter is optional, and nothing is selected, this
   * code will never be called.
   */
  public function query()
  {
    $this->ensureMyTable();

    $conditions = [];

    $field = 'date_range';
    $start_field_name = "$this->tableAlias" . '.' . $field . "__value";
    $end_field_name = "$this->tableAlias" . '.' . $field . "__end_value";

    // Prepare sql clauses for each field.
    $date_start = $this->query->getDateFormat($this->query->getDateField($start_field_name, TRUE), 'Y-m-d', FALSE);
    $date_end = $this->query->getDateFormat($this->query->getDateField($end_field_name, TRUE), 'Y-m-d', FALSE);
    $date_now = $this->query->getDateFormat('FROM_UNIXTIME(***CURRENT_TIME***)', 'Y-m-d', FALSE);

    switch ($this->value['state']) {
      case 'current':
        $conditions[] = "(($date_now BETWEEN $date_start AND $date_end) OR ($date_end IS NULL AND $date_start <= $date_now))";
        break;

      case 'past':
        $conditions[] = "($date_now > $date_end)";
        break;

      case 'future':
        $conditions[] = "($date_now <= $date_start)";
        break;
    }

    $conditions = array_filter($conditions);
    if (count($conditions) > 0) {
      $this->query->addWhereExpression($this->options['group'], implode(" OR ", $conditions));
    }
  }

  public function adminSummary()
  {
    if ($this->isAGroup()) {
      return $this->t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return $this->t('exposed') . ', ' . $this->t('default state') . ': ' . $this->value['state'];
    } else {
      return $this->t('state') . ': ' . $this->value['state'];
    }
  }
}
