<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group\GroupMembership;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get whether the user has a record for the group role
 *
 * @ViewsField("group_membership_record_membership_current_record_by_record_role")
 */
class CurrentRecordByRecordRole extends FieldPluginBase {
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['roles'] = ['default' => []];
    $options['record_enabled'] = ['default' => TRUE];
    $options['record_current'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $repo */
    $repo = \Drupal::service('group_membership_record.repository');
    $form['roles'] = [
      '#type'          => 'select',
      '#title'         => t('Select role(s) to load users for this group'),
      '#options'       => $repo->getRolesInUseGroupedByTypes(),
      '#default_value' => $this->options['roles'],
      '#size'          => 10,
      '#multiple'      => TRUE,
      '#required'      => TRUE,
      '#attributes'    => [
        'id'    => 'edit-select-user',
        'style' => 'width:600px',
      ]
    ];

    $form['record_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only include enabled records'),
      '#default_value' => $this->options['record_enabled'],
    ];

    $form['record_current'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only include current records'),
      '#default_value' => $this->options['record_current'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $results = $this->getResults($values);
    if (count($results) > 0) {
      return [
        '#type'   => 'markup',
        '#markup' => '<ul><li>' . implode("</li><li>", $results) . '</li></ul>'
      ];
    } else {
      $role_ids = $this->options['roles'];
      $roleLabels = [];
      foreach ($role_ids as $role_id) {
        $entityRole = GroupRole::load($role_id);
        if ($entityRole) {
          $roleLabels[] = $entityRole->label();
        }
      }
      return [
        '#type'   => 'markup',
        '#markup' => t('No records found with group role(s): @roles', ['@roles' => implode(', ', $roleLabels)])
      ];
    }
  }

  public function getResults($values) {
    $arrayRecords = $this->getRecords($values);
    $results = [];

    foreach ($arrayRecords as $records) {
      /** @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $record */
      foreach ($records as $record) {
        $link = Link::createFromRoute(
          $record->getOwner()->getDisplayName(),
          'entity.group_membership_record.edit_form',
          ['group_membership_record' => $record->id()]
        );
        if (count($this->options['roles']) > 1) {
          $results[] = $link->toString() . ' (' . $record->getRole()->label() . ')';
        } else {
          $results[] = $link->toString();
        }
      }
    }
    return $results;
  }

  public function getRecords($values) {
    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $this->getEntity($values);

    $rolesSelected = $this->options['roles'];

    $alias = $this->field_alias;
    $values->$alias = '';

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $repo */
    $repo = \Drupal::service('group_membership_record.repository');

    $date = !empty($this->options['record_current']) ? new DateTime() : NULL;

    $results = [];
    foreach ($rolesSelected as $role) {
      $split = explode(':', $role);

      $gmrType = $split[0];
      $type = GroupMembershipRecordType::load($gmrType);

      $role = $split[1];
      $entityRole = GroupRole::load($role);

      if ($entityRole) {
        $results[] = $repo->get(NULL, $group, $entityRole, $type, $date, !empty($this->options['record_enabled']));
      }
    }
    return $results;
  }
}
