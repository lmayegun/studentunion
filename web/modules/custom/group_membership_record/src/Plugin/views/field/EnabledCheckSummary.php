<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\course\Entity\CourseEnrollment;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("group_membership_record_check_enabled_summary")
 */
class EnabledCheckSummary extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['text_disabled'] = ['default' => "Not enabled"];
    $options['text_enabled'] = ['default' => "Enabled"];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['text_disabled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text value for when disabled'),
      '#default_value' => $this->options['text_disabled']
    ];
    $form['text_enabled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text value for when enabled'),
      '#default_value' => $this->options['text_enabled']
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $record = $this->getEntity($values);

    $enabled = NULL;
    $explanations = [];

    $pluginManager = \Drupal::service('plugin.manager.group_membership_record.enabled_determiner');
    $plugins = $pluginManager->getForInstanceType($record->bundle());
    foreach ($plugins as $plugin) {
      $enabled = $plugin->determineEnabledValue($record);
      $explanationsLoaded = array_merge($explanations, $plugin->getEnabledExplanation($record));
    }

    if ($record->enabled != $enabled) {
      $record->enabled = $enabled;
      $record->save();
    }

    if ($enabled) {
      $enabledText = $this->options['text_enabled'] ?? 'Enabled';
    } else {
      $enabledText = $this->options['text_disabled'] ?? 'Not enabled';
    }

    $explanations = [];
    foreach ($explanationsLoaded as $loaded) {
      if (is_array($loaded)) {
        if (isset($loaded[1])) {
          $success = isset($loaded[1]) && $loaded[1];
          $class = $success ? 'test-success' : 'test-failure';
          $emoji = $success ? '✔️' : '❌';
          $explanation = '<li class="test ' . $class . '">' . $emoji . ' ' . $loaded[0] . '</li>';
          if ($success || !isset($loaded[1])) {
            $explanations[] = $explanation;
          } else {
            array_unshift($explanations, $explanation);
          }
        } else {
          $explanations[] = '<li class="test">' . $loaded[0] . '</li>';
        }
      } else {
        $explanations[] = $loaded;
      }
    }


    $list = '<ul class="group_membership_record__enabled-test-results" data-read-more-label="See explanation">' . implode('', $explanations) . '</ul>';
    return new FormattableMarkup($enabledText . $list, []);
  }
}
