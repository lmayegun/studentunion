<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\course\Entity\CourseEnrollment;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("group_membership_record_currency_field")
 */
class CurrencyField extends FieldPluginBase {
    /**
     * @{inheritdoc}
     */
    public function query() {
        // Leave empty to avoid a query on this field.
    }

    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
        $options = parent::defineOptions();

        $options['text_past'] = ['default' => "Past"];
        $options['text_current'] = ['default' => "Current"];
        $options['text_future'] = ['default' => "Future"];
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        parent::buildOptionsForm($form, $form_state);
        $form['text_past'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Text value for when past'),
            '#default_value' => $this->options['text_past']
        ];
        $form['text_current'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Text value for when current'),
            '#default_value' => $this->options['text_current']
        ];
        $form['text_future'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Text value for when future'),
            '#default_value' => $this->options['text_future']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values) {
        $record = $this->getEntity($values);

        $text = $record->getCurrency();
        switch ($text) {
            case 'past':
                return new FormattableMarkup($this->options['text_past'] ?? 'Past', []);
            case 'current':
                return new FormattableMarkup($this->options['text_current'] ?? 'Current', []);
            case 'future':
                return new FormattableMarkup($this->options['text_future'] ?? 'Future', []);
        }
        return null;
    }
}
