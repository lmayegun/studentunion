<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("group_membership_record_membership_current_enabled")
 */
class CurrentEnabledRecords extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $this->getEntity($values);

    $alias = $this->field_alias;
    $values->$alias = '';

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $repo */
    $repo = \Drupal::service('group_membership_record.repository');
    $records = $repo->get(NULL, $group, NULL, NULL, new DateTime(), TRUE);
    $results = [];

    /** @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $record */
    foreach ($records as $record) {
      $link = Link::createFromRoute(
        $record->getOwner()->getDisplayName(),
        'entity.group_membership_record.edit_form',
        ['group_membership_record' => $record->id()]
      );
      $results[] = $link->toString();
    }

    return [
      '#type' => 'markup',
      '#markup' => '<ul><li>' . implode("</li><li>", $results) . '</li></ul>'
    ];
  }
}
