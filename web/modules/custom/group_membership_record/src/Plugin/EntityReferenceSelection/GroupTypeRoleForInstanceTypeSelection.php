<?php

namespace Drupal\group_membership_record\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Only shows the group roles which are available for a group type and group instance type.
 *
 * @EntityReferenceSelection(
 *   id = "group_type:group_role:group_membership_record",
 *   label = @Translation("Group type role selection for instance type"),
 *   entity_types = {"group_role"},
 *   group = "group_role",
 *   weight = 0
 * )
 */
class GroupTypeRoleForInstanceTypeSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $configuration = $this->getConfiguration();

    /** @var \Drupal\group\Entity\GroupRoleInterface[] $group_roles */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $group_roles = $gmrRepositoryService->getRolesForInstanceType($configuration['group_membership_record_type_id']);

    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('group_type', $configuration['group_type_ids'], 'IN');
    $query->condition('internal', 0, '=');

    return $query;
  }

}
