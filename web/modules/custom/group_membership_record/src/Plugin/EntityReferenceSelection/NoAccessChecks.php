<?php

namespace Drupal\group_membership_record\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * No access checks because not using the Deriver DefaultSelection does.
 *
 * @EntityReferenceSelection(
 *   id = "group_membership_record:no_access_checks",
 *   label = @Translation("Entity reference with no access checks"),
 *   entity_types = {"group", "group_role", "user"},
 *   group = "group_membership_record",
 *   weight = 0
 * )
 */
class NoAccessChecks extends DefaultSelection {

  // No access checks because not using the Deriver DefaultSelection does.

}
