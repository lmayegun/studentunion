<?php

namespace Drupal\group_membership_record\Service;

use DateTime;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupRole;
use Drupal\user\UserInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group\GroupMembershipLoader;
use Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface;
use Drupal\user\Entity\User;

/**
 * Class GroupMembershipRecordManager.
 *
 * Functions for creating, modifying and deleting group membership records.
 */
class GroupMembershipRecordManager {

  /**
   * Constructs a new GroupMembershipRecordManager object.
   */
  public function __construct() {
  }

  /**
   * Create a role instance
   *
   * @param   UserInterface  $account  [$account description]
   * @param   Group          $group    [$group description]
   * @param   GroupRole      $role     [$role description]
   *
   * @return  GroupMembershipRecord        [return description]
   */
  function create(UserInterface $account, Group $group, GroupRole $role, $values = [], $save = true) {
    $values['type'] = $role->getThirdPartySetting('group_membership_record', 'group_membership_record_type');

    if (!$values['type']) {
      // No group membership record type associated with this role
      return false;
    }

    $values['user_id'] = $account->id();
    $values['group_id'] = $group->id();
    $values['group_role_id'] = $role->id();

    $record = \Drupal::entityTypeManager()->getStorage('group_membership_record')->create($values);
    if ($save) {
      $record->save();
    }

    return $record;
  }

  /**
   * WARNING does not check for currency or enabled
   * Use syncing instead
   * @param GroupMembershipRecord $record
   *
   * @return [type]
   */
  public function assignRoleForRecord(GroupMembershipRecord $record) {
    $account = $record->getOwner();
    $group = $record->getGroup();
    $role = $record->getRole();
    $this->assignRole($account, $group, $role);
  }

  public function assignRole($account, $group, $role) {
    if (!$account) {
      return false;
    }
    if (!$group) {
      return false;
    }
    if ($member = $group->getMember($account)) {
      // @todo test this
      // https://www.drupal.org/forum/support/post-installation/2017-02-19/drupal-8-group-module-how-to-add-a-role-to-a-group-member
      // https://www.drupal.org/node/2839968
      // https://drupal.stackexchange.com/questions/232530/programmatically-add-new-role-to-group-member/232646#232646
      $membership = $member->getGroupContent();
      $already_has_role = $this->hasRole($account, $group, $role);
      if (!$already_has_role) {
        $membership->group_roles[] = ['target_id' => $role->id()];
        $membership->save();
      }
      // \Drupal::logger('group_membership_record')->notice('Existing membership, adding role ID: ' . $role->id());
    } else {
      // \Drupal::logger('group_membership_record')->notice('Creating new membership with role ID: ' . $role->id());

      $group->addMember($account, ['group_roles' => ['target_id' => $role->id()]]);
    }
  }

  public function removeRole($account, $group, $role) {
    if (!$group) {
      // dd("REMOVE ROLE NO GROUP");
      return;
    }
    if ($member = $group->getMember($account)) {
      $membership = $member->getGroupContent();
      $already_has_role = $this->hasRole($account, $group, $role);
      if ($already_has_role) {
        $newGroupRoles = [];
        foreach ($membership->group_roles as $key => $existing_group_role) {
          if ($role->id() != $existing_group_role->entity->id()) {
            $newGroupRoles[] = ['target_id' => $existing_group_role->entity->id()];
          }
        }

        $membership->group_roles = $newGroupRoles;
        // \Drupal::logger('group_membership_record')->notice('New roles for group: ' . print_r($newGroupRoles, TRUE));
        if (count($newGroupRoles) == 0) {
          $membership->save();
          $group->removeMember($account);
        } else {
          $membership->save();
        }
      } else {

        // dd("REMOVE ROLE DOESNT ALREADY HAVE IT");
      }
    }
  }

  public function getUserIdsWithRole(string $groupType, int $groupId, string $roleId) {
    // We load this with a direct query to avoid loading the user entities and the membership entities.
    // (Slows performance).
    $connection = \Drupal::database();
    $query = $connection->select('group_content_field_data', 'gc');
    $query->condition('gc.type', $groupType . '-group_membership');
    $query->condition('gc.gid', $groupId);
    $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
    $query->condition('r.group_roles_target_id', $groupType . '-' . $roleId);
    $query->condition('r.deleted', 0);
    // Get user IDs (entity_id)
    $query->fields('gc', ['uid']);
    $results = $query->distinct()->execute()->fetchCol();
    array_filter($results);
    $uids = $results;
    return $uids;
  }

  public function hasRoleFromIds(array $uids, string $groupType, array $groupIds, array $roleIds) {
    // We load this with a direct query to avoid loading the user entities and the membership entities.
    // (Slows performance).
    $connection = \Drupal::database();

    // Load group memberships
    $query = $connection->select('group_content_field_data', 'gc');
    $query->condition('gc.type', $groupType . '-group_membership');
    $query->condition('gc.gid', $groupIds, 'IN');
    $query->condition('gc.entity_id', $uids, 'IN');

    // Hook into roles
    $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
    $query->condition('r.group_roles_target_id', $roleIds, 'IN');
    $query->condition('r.deleted', 0);

    $results = $query->distinct()->countQuery()->execute()->fetchField();

    return $results > 0;
  }

  public function hasRole(UserInterface $account, Group $group, GroupRole $role) {
    if ($member = $group->getMember($account)) {
      $membership = $member->getGroupContent();
      foreach ($membership->group_roles as $existing_role) {
        if ($role->id() == $existing_role->entity->id()) {
          return TRUE;
        }
        if (is_array($existing_role) && isset($existing_role['target_id']) && $role->id() == $existing_role['target_id']) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  public function findAndDelete(UserInterface $account = null, GroupInterface $group = null, GroupRoleInterface $role = null, GroupMembershipRecordTypeInterface $type, DateTime $date = null) {
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $records = $gmrRepositoryService->get($account, $group, $role, $type, $date);
    foreach ($records as $record) {
      $record->delete();
    }
  }

  /**
   * Update enabled field when instance is updated
   * Custom modules can provide plugins that determine what to set it as, per GRI type
   * but by default this doesn't change the value at all
   *
   * @param   GroupMembershipRecord  $record  [$entity description]
   *
   * @return  [type]                    [return description]
   */
  public function checkEnabled(GroupMembershipRecord $record, $save = true) {
    $pluginManager = \Drupal::service('plugin.manager.group_membership_record.enabled_determiner');
    $plugins = $pluginManager->getForInstanceType($record->bundle());
    foreach ($plugins as $plugin) {
      $enabled = $plugin->determineEnabledValue($record);
    }
    // @todo deal with enabling via multiple plugins
    return $enabled;
  }

  /**
   * This module controls the 'enabled' field for the relevant group membership records.
   *
   * Relevant if your module has deined an EnabledDeterminer plugin.
   *
   * Note if you call $current only (default), you need to make sure the check is run whenever the date fields might be updated
   * To ensure the enabled field gets recalculated then
   */
  public function findAndUpdateEnabled(UserInterface $account = null, Group $group = null, GroupRole $role = null, $currentOnly = true) {
    $records = \Drupal::service('group_membership_record.repository')->get($account, $group, $role, $currentOnly ? new DateTime() : null);
    foreach ($records as $record) {
      $record->updateEnabled();
    }
  }
}
