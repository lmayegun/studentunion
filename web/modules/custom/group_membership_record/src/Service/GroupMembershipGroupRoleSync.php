<?php

namespace Drupal\group_membership_record\Service;

use Drupal\Core\Cache\Cache;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group\GroupMembershipLoader;

use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Service\GroupMembershipRecordManager;
use Drupal\group_membership_record\Service\GroupMembershipRecordRepository;

/**
 * Class GroupMembershipGroupRoleSync.
 * 
 * Functions for creating, modifying and deleting group membership records.
 */
class GroupMembershipGroupRoleSync
{
    protected $debug = false;

    /**
     * The gmr manager service.
     *
     * @var \Drupal\group_membership_record\Service\GroupMembershipRecordManager
     */
    protected $gmrManagerService;

    /**
     * The gmr repo service.
     *
     * @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository
     */
    protected $gmrRepositoryService;

    /**
     * Constructs a new RoleSync.
     **/
    public function __construct()
    {
        /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordManager $gmrManagerService */
        $this->gmrManagerService = \Drupal::service('group_membership_record.manager');

        /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
        $this->gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    }

    public function syncGroupRolesForRecord(GroupMembershipRecord $record)
    {
        $account = $record->getOwner();
        $group = $record->getGroup();
        $role = $record->getRole();
        $this->syncGroupRoles($account, $group, $role);
    }

    /**
     * Updates group roles from group membership records
     * 
     * (where the group membership record controls access when set up on the group role)
     * 
     * This is complex because it's not 1 to 1 - 
     * there could be a record that indicates the user should have the role
     * as well as one that indicates they should not
     * so we need to loop through all of them, work out all possible combinations,
     * and work out from those whether the user should have that role for that group
     * 
     * We also need to check any roles the user might have that they should not
     * if GMRs control those roles
     */
    public function syncGroupRoles($account = null, Group $group = null, GroupRole $role = null)
    {
        $accountRoleGroupCombinations = [];
        $role_counts = [];

        // Get all group role instances and get a list of any role-group combinations that are being managed by group membership records
        $records = $this->gmrRepositoryService->get($account, $group, $role);

        foreach ($records as $record) {
            $group = $record->getGroup();
            $role = $record->getRole();
            $account = $record->getOwner();
            if (!$role || !$group || !$account) {
                continue;
            }

            if (!isset($role_counts[$role->id()])) {
                $role_counts[$role->id()] = 0;
            }
            $role_counts[$role->id()]++;

            $recordKey = implode('-', [$account->id(), $group->id(), $role->id()]);

            // Default to assuming we need to remove the role
            // Loop through all group membership records to find any valid ones
            // If there is one valid one, we want to assign the role
            // If we never find a valid one, we know to remove it
            if (!isset($accountRoleGroupCombinations[$recordKey])) {
                $accountRoleGroupCombinations[$recordKey] = [
                    'group' => $group,
                    'role' => $role,
                    'account' => $account,
                    'needs_enabled' => $role->getThirdPartySetting('group_membership_record', 'access_requires_enabled_record') ?: false,
                    'needs_current' => $role->getThirdPartySetting('group_membership_record', 'access_requires_current_record') ?: false,
                    'valid' => false,
                ];
            }

            if (!$accountRoleGroupCombinations[$recordKey]['valid']) {
                $enabled = true;
                $current = true;

                if ($accountRoleGroupCombinations[$recordKey]['needs_enabled']) {
                    $enabled = $record->isEnabled();
                }

                if ($accountRoleGroupCombinations[$recordKey]['needs_current']) {
                    $current = $record->isCurrent();
                }

                if ($enabled && $current) {
                    $accountRoleGroupCombinations[$recordKey]['valid'] = true;
                }

                $accountRoleGroupCombinations[$recordKey]['enabled'] = $enabled ? 'enabled' : 'not enabled';
                $accountRoleGroupCombinations[$recordKey]['current'] = $current ? 'current' : 'not current';
            }
        }

        // Make sure to remove any invalid roles we should be managing via GMR (enabled/current)
        // But which might not have a GMR alongside
        $this->addInvalidRolesWithoutMatchingRecords($accountRoleGroupCombinations, $account, $group, $role);

        $role_counts_valid = [];
        $role_counts_invalid = [];

        $groupIdsToInvalidate = [];

        // For each role-group combination, either add it, to ensure it exists, or remove it
        foreach ($accountRoleGroupCombinations as $key => $data) {
            $this->log('Syncing ' . $key);
            $this->log(implode(' - ', [$data['valid'] ? 'valid' : 'not valid', $data['enabled'] ?? 'unknown current', $data['current'] ?? 'unknown current']));

            $role_id = $data['role']->id();
            if ($data['valid'] === true) {
                $this->log('');

                $this->gmrManagerService->assignRole($data['account'], $data['group'], $data['role']);

                $groupIdsToInvalidate[] = $data['group']->id();

                if (!isset($role_counts_valid[$role_id])) {
                    $role_counts_valid[$role_id] = 0;
                }
                $role_counts_valid[$role_id]++;
            } else {
                $this->gmrManagerService->removeRole($data['account'], $data['group'], $data['role']);

                $groupIdsToInvalidate[] = $data['group']->id();

                if (!isset($role_counts_invalid[$role_id])) {
                    $role_counts_invalid[$role_id] = 0;
                }
                $role_counts_invalid[$role_id]++;
            }
        }

        foreach ($groupIdsToInvalidate as $id) {
            Cache::invalidateTags(['group_membership_record:group_roles:' . $id]);
        }

        $this->log('Valid roles: ' . print_r($role_counts_valid, true));
        $this->log('Invalid roles: ' . print_r($role_counts_invalid, true));
    }

    public function log($text)
    {
        if ($this->debug == true) {
            \Drupal::logger('group_membership_record')->notice($text);
        }
    }

    /**
     * User may have role for whatever reason, 
     * but where role should be controlled by GMR 
     * and there is no GMR to support the user having the role
     * we need to remove it
     * 
     * So, we now need to loop through any roles the user actually has
     * (where the role is controlled by GMR either by enabled or current)
     * and if they don't have a correspondign GMR, remove them
     */
    public function addInvalidRolesWithoutMatchingRecords(&$accountRoleGroupCombinations, $account = null, Group $group = null, GroupRole $role = null)
    {
        //$this->log('pre-invalid-check roles '.print_r(array_keys($accountRoleGroupCombinations), true));

        $groupMembershipLoader = new GroupMembershipLoader(\Drupal::service('entity_type.manager'), $account ?? \Drupal::currentUser());

        // Work out what users to check for based on their group membership and what criteria we have
        $memberships = [];
        if ($account) {
            $memberships = $groupMembershipLoader->loadByUser($account);
        } elseif ($group) {
            $memberships = $group->getMembers($role ? [$role] : null);
        }
        // @todo do we need to do this for all users if an account isn't specified!?

        foreach ($memberships as $group_membership) {
            $memberAccount = $group_membership->getUser();
            if ($account && $account->id() != $memberAccount->id()) {
                continue;
            }
            if ($group && $group->id() != $group_membership->getGroup()->id()) {
                continue;
            }
            foreach ($group_membership->getRoles() as $currentRole) {
                if ($role && $role->id() != $currentRole->id()) {
                    continue;
                }
                if ($this->gmrRepositoryService->checkRecordControlsAccessForRole($currentRole)) {
                    $key = implode('-', [$memberAccount->id(), $group_membership->getGroup()->id(), $currentRole->id()]);
                    if (!isset($accountRoleGroupCombinations[$key])) {
                        //		\Drupal::logger('group_membership_record')->notice('Controlled role is not found in valid GMRs - '.$key);
                        $accountRoleGroupCombinations[$key] = [
                            'group' => $group_membership->getGroup(),
                            'role' => $currentRole,
                            'account' => $memberAccount,
                            'needs_enabled' => $role->getThirdPartySetting('group_membership_record', 'access_requires_enabled_record') ?: false,
                            'needs_current' => $role->getThirdPartySetting('group_membership_record', 'access_requires_current_record') ?: false,
                            'valid' => false,
                        ];
                    }
                }
            }
        }
    }
}
