<?php

namespace Drupal\group_membership_record\Service;

use DateTime;
use Drupal;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface;
use Drupal\group_membership_record\GroupMembershipRecordQuery;
use Drupal\user\UserInterface;

/**
 * Class GroupMembershipRecordRepository.
 *
 * Functions for finding and retrieving group membership records.
 */
class GroupMembershipRecordRepository {

  /**
   * Constructs a new GroupMembershipRecordRepository object.
   */
  public function __construct() {
  }

  function getQuery(UserInterface $account = NULL, GroupInterface $group = NULL, GroupRoleInterface $role = NULL, GroupMembershipRecordTypeInterface $type = NULL, DateTime $date = NULL, $enabled = NULL, DateTime $dateEnd = NULL) {
    $query = new GroupMembershipRecordQuery();

    if ($account) {
      $query->setAccount($account);
    }

    if ($group) {
      $query->setGroup($group);
    }

    if ($role) {
      $query->setRole($role);
    }

    if ($type) {
      $query->setRecordType($type);
    }

    if ($enabled) {
      $query->setRequireEnabled($enabled);
    }

    if ($date) {
      $query->setDates($date, $dateEnd);
    }

    return $query;
  }

  /**
   * Returns a filtered array of instances.
   *
   * Optionally limited by: user, group, role, and/or date.
   *
   * @param UserInterface|NULL $account
   * @param GroupInterface|NULL $group
   * @param GroupRoleInterface|NULL $role
   * @param GroupMembershipRecordTypeInterface|NULL $type
   * @param DateTime|NULL $date
   * @param NULL $enabled
   * @param DateTime|NULL $dateEnd
   *
   * @return array
   */
  function getIds(UserInterface $account = NULL, GroupInterface $group = NULL, GroupRoleInterface $role = NULL, GroupMembershipRecordTypeInterface $type = NULL, DateTime $date = NULL, $enabled = NULL, DateTime $dateEnd = NULL) {
    $query = $this->getQuery($account, $group, $role, $type, $date, $enabled, $dateEnd);
    $ids = $query->executeForIds();
    return $ids;
  }

  /**
   * Returns a filtered array of instances.
   *
   * Optionally limited by: user, group, role, and/or date.
   *
   * @param UserInterface|NULL $account
   * @param GroupInterface|NULL $group
   * @param GroupRoleInterface|NULL $role
   * @param GroupMembershipRecordTypeInterface|NULL $type
   * @param DateTime|NULL $date
   * @param NULL $enabled
   * @param DateTime|NULL $dateEnd
   *
   * @return array
   */
  function get(UserInterface $account = NULL, GroupInterface $group = NULL, GroupRoleInterface $role = NULL, GroupMembershipRecordTypeInterface $type = NULL, DateTime $date = NULL, $enabled = NULL, DateTime $dateEnd = NULL) {
    $ids = $this->getIds($account, $group, $role, $type, $date, $enabled, $dateEnd);
    return Drupal::entityTypeManager()
      ->getStorage('group_membership_record')
      ->loadMultiple($ids);
  }

  function count(UserInterface $account = NULL, GroupInterface $group = NULL, GroupRoleInterface $role = NULL, GroupMembershipRecordTypeInterface $type = NULL, DateTime $date = NULL, $enabled = NULL, DateTime $dateEnd = NULL) {
    $query = $this->getQuery($account, $group, $role, $type, $date, $enabled, $dateEnd);
    return $query->getCount();
  }

  function getRolesForInstanceType($record_type) {
    $roles = GroupRole::loadMultiple();

    $ids = [];
    foreach ($roles as $role) {
      if ($role->getThirdPartySetting('group_membership_record', 'group_membership_record_type') === $record_type) {
        $ids[] = $role->id();
      }
    }
    return Drupal::entityTypeManager()
      ->getStorage('group_role')
      ->loadMultiple($ids);
  }

  function getGroupTypesForInstanceType($record_type) {
    $roles = $this->getRolesForInstanceType($record_type);
    $groupTypes = [];
    foreach ($roles as $role) {
      $groupTypes[$role->getGroupTypeId()] = $role->getGroupType();
    }
    return $groupTypes;
  }

  function getInstancesForInstanceType($record_type) {
    $query = Drupal::entityQuery('group_membership_record');
    $query = $query->condition('type', $record_type);
    $ids = $query->execute();
    return Drupal::entityTypeManager()
      ->getStorage('group_membership_record')
      ->loadMultiple($ids);
  }

  function getRolesForGroupType($group_type) {
    $query = Drupal::entityQuery('group_role');
    $query = $query->condition('group_type', $group_type);
    $query = $query->condition('internal', 0, '=');
    $ids = $query->execute();
    return Drupal::entityTypeManager()
      ->getStorage('group_role')
      ->loadMultiple($ids);
  }

  function getAllNonInternalRoles($group_types = NULL) {
    $query = Drupal::entityQuery('group_role');
    $query = $query->condition('internal', 0, '=');
    if ($group_types && count($group_types) > 0) {
      $query = $query->condition('group_type', $group_types, 'IN');
    }
    $ids = $query->execute();
    return Drupal::entityTypeManager()
      ->getStorage('group_role')
      ->loadMultiple($ids);
  }

  function checkRecordControlsAccessForRole(GroupRoleInterface $role) {
    return $role->getThirdPartySetting('group_membership_record', 'access_requires_enabled_record') == 1 || $role->getThirdPartySetting('group_membership_record', 'access_requires_current_record') == 1;
  }

  function getAllRolesForType($groupMembershipRecordType) {
    $storage = Drupal::entityTypeManager()->getStorage('group_role');
    $query = $storage->getQuery();
    $query->condition('third_party_settings.group_membership_record.group_membership_record_type', $groupMembershipRecordType->id());
    $ids = $query->execute();
    return Drupal::entityTypeManager()
      ->getStorage('group_role')
      ->loadMultiple($ids);
  }

  function getAllRolesControllingAccess(Group $group = NULL) {
    $storage = Drupal::entityTypeManager()->getStorage('group_role');
    $query = $storage->getQuery();
    if ($group) {
      $groupType = $group->bundle();
      $query->condition('group_type', $group->bundle());
    }
    $query->orConditionGroup()
      ->condition('third_party_settings.group_membership_record.access_requires_enabled_record', 1)
      ->condition('third_party_settings.group_membership_record.access_requires_current_record', 1);
    $ids = $query->execute();
    return Drupal::entityTypeManager()
      ->getStorage('group_role')
      ->loadMultiple($ids);
  }

  function getRolesInUse(UserInterface $account = NULL, Group $group = NULL) {
    $connection = Drupal::database();
    $query = $connection->select('group_membership_record', 'gri');
    if ($account) {
      $query = $query->condition('user_id', $account->id());
    }
    if ($group) {
      $query = $query->condition('group_id', $group->id());
    }
    $query->fields('gri', ['group_role_id']);
    $results = $query->distinct()->execute()->fetchCol();
    array_filter($results);
    return $results;
  }

  function getTypesInUse(UserInterface $account = NULL, Group $group = NULL, array $groupTypes = NULL) {
    $connection = Drupal::database();
    $query = $connection->select('group_membership_record', 'gri');
    if ($groupTypes && count($groupTypes) > 0) {
      $query->join('groups', 'g', 'g.id = gri.group_id');
      $query = $query->condition('g.type', $groupTypes, 'IN');
    }
    if ($account) {
      $query = $query->condition('gri.user_id', $account->id());
    }
    if ($group) {
      $query = $query->condition('gri.group_id', $group->id());
    }
    $query->fields('gri', ['type']);
    $results = $query->distinct()->execute()->fetchCol();
    array_filter($results);
    return $results;
  }

  /**
   * @param \Drupal\user\UserInterface|NULL $account
   * @param \Drupal\group\Entity\Group|NULL $group
   * @param array|NULL $groupTypes
   * @param $groupByType
   *
   * @return array
   */
  function getRolesInUseGroupedByTypes(UserInterface $account = NULL, Group $group = NULL, array $groupTypes = NULL, $groupByType = TRUE) {
    $connection = Drupal::database();
    $query = $connection->select('group_membership_record', 'gri');
    if ($groupTypes && count($groupTypes) > 0) {
      $query->join('groups', 'g', 'g.id = gri.group_id');
      $query = $query->condition('g.type', $groupTypes, 'IN');
    }
    if ($account) {
      $query = $query->condition('gri.user_id', $account->id());
    }
    if ($group) {
      $query = $query->condition('gri.group_id', $group->id());
    }
    $query->fields('gri', ['type', 'group_role_id']);
    $results = $query->distinct()->execute()->fetchAll();

    array_filter($results);

    $rolesResult = [];
    foreach ($results as $id => $record) {
      $roleTypeFull = $record->group_role_id;

      if ($roleTypeFull && strpos($roleTypeFull, '-') !== FALSE) {
        $id = $record->type . ':' . $roleTypeFull;

        $role = GroupRole::load($roleTypeFull);
        $roleLabel = $role ? $role->label() : $roleTypeFull;
        if ($groupByType) {
          $type = GroupMembershipRecordType::load($record->type);
          if (!$type) {
            $typeLabel = $record->type;
          } else {
            $typeLabel = $type->label();
          }
          $rolesResult[$typeLabel][$id] = $roleLabel;
        } else {
          $rolesResult[$id] = $roleLabel;
        }
      }
    }

    return $rolesResult;
  }

  /**
   * Returns TRUE if a current instance exists for that user for a group for a
   * role Determined by a date range field specified per role
   *
   * @param UserInterface $account [$account description]
   * @param Group $group [$group description]
   * @param GroupRole $role [$role description]
   * @param DateTime $date [$date description]
   *
   * @return  boolean                  [return description]
   */
  function currentInstanceExists(UserInterface $account, Group $group, GroupRole $role, DateTime $date = NULL) {
    if (!$date) {
      $date = new DateTime();
    }
    return count($this->get($account, $group, $role, NULL, $date)) > 0;
  }

  /**
   *
   * Returns TRUE if there is an enabled AND current instance for the role
   * (i.e. boolean is checked)
   *
   * @param UserInterface $account [$account description]
   * @param Group $group [$group description]
   * @param GroupRole $role [$role description]
   * @param DateTime $date [$date description]
   * @param NULL                     [ description]
   *
   * @return  [type]                   [return description]
   */
  function currentEnabledInstanceExists(UserInterface $account = NULL, Group $group = NULL, GroupRole $role = NULL, GroupMembershipRecordType $type = NULL, DateTime $date = NULL) {
    if (!$date) {
      $date = new DateTime();
    }
    return $this->enabledInstanceExists($account, $group, $role, $type, $date);
  }

  /**
   * Returns TRUE if there is an enabled instance for the role (i.e. boolean is
   * checked).
   *
   * @param UserInterface $account [$account description]
   * @param Group $group [$group description]
   * @param GroupRole $role [$role description]
   *
   * @return  boolean                  [return description]
   */
  function enabledInstanceExists(UserInterface $account = NULL, Group $group = NULL, GroupRole $role = NULL, GroupMembershipRecordTypeInterface $type = NULL, DateTime $date = NULL) {
    $records = $this->get($account, $group, $role, $type, $date);
    foreach ($records as $record) {
      if ($record->isEnabled()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Return group membership records whose source is a specified entity of a
   * specified type
   *
   * e.g. 'commerce_license', id 1244 = the instance created by that license
   */
  function getBySourceId($source_entity_type, $source_id) {
    $query = Drupal::entityQuery('group_membership_record')
      ->condition('source.target_id', $source_id, '=')
      ->condition('source.target_type', $source_entity_type, '=');
    $ids = $query->execute();
    return Drupal::entityTypeManager()
      ->getStorage('group_membership_record')
      ->loadMultiple($ids);
  }
}
