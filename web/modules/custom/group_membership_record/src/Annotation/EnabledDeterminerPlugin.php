<?php

namespace Drupal\group_membership_record\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EnabledDeterminerPlugin annotation object.
 * 
 * This allows other modules to define ways of setting the 'enabled' field, e.g.
 * enabled could be automatically set by different values or events
 *
 * Note that the "@ Annotation" line below is required and should be the last
 * line in the docblock. It's used for discovery of Annotation definitions.
 *
 * @see \Drupal\group_membership_record\Plugin\EnabledDeterminerPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class EnabledDeterminerPlugin extends Plugin
{
    /**
     * Description of plugin
     *
     * @var \Drupal\Core\Annotation\Translation
     *
     * @ingroup plugin_translatable
     */
    public $description;

    public $group_membership_record_types = [];
}
