<?php

namespace Drupal\group_membership_record\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for Group membership record edit forms.
 *
 * This can limit the groups you can select to the group type
 * And the roles you can select to the roles available to that group type
 * (or if you have specified a GroupMembershipRecordType, just to that GroupMembershipRecordType)
 *
 * @ingroup group_membership_record
 */
class GroupMembershipRecordForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The group type ID(s) to limit the group selection and role selection to
   *
   * @var array
   */
  protected $groupTypes = [];

  /**
   * Limit users allowed to be selected
   *
   * Can be passed by this name in query string of form with a comma delimited list
   *
   * @var array of uids
   */
  protected $allowedUsers = [];

  /**
   * Limit the form to existing members of the group
   */
  protected $limitToMembers = false;

  /**
   * {@inheritdoc}
   */
  protected $limitToMembersExcludeWithCurrentRole = [];

  /**
   * Limit groups allowed to be selected
   *
   * Can be passed by this name in query string of form with a comma delimited list
   *
   * @var array of group IDs
   */
  protected $allowedGroups = [];

  /**
   * Limit roles allowed to be selected
   *
   * Can be passed by this name in query string of form with a comma delimtied list
   *
   * @var array of role type IDs
   */
  protected $allowedRoles = [];

  /**
   * Limit roles allowed to be selected based on whether they are assigned to these types
   *
   * Can be passed by this name in query string of form with a comma delimtied list
   *
   * @var array
   */
  protected $allowedRoleInstanceTypes = [];

  /**
   * Turn autocompletes into select dropdowns if this number of option or fewer
   *
   * @var [type]
   */
  protected $optionsToTurnToSelect = 10;

  /**
   * Hide fields by field name
   *
   * @var array
   */
  protected $fieldsToHide = [];

  /**
   * Allow fields by field name; all other fields hidden
   *
   * @var array
   */
  protected $fieldsToAllow = [];

  /**
   * Disable fields by field name
   *
   * @var array
   */
  protected $fieldsToDisable = [];

  /**
   * Set default values, keyed by field name
   *
   * @var array
   */
  protected $fieldDefaultValues = [];

  /**
   * Set default start date of daterange, in format Y-m-d\TH:i:s
   *
   * @var string
   */
  protected $defaultStartDate = null;

  /**
   * Set default end date of daterange, in format Y-m-d\TH:i:s
   *
   * @var string
   */
  protected $defaultEndDate = null;

  /**
   * Message to display to user on successful submission of add form
   *
   * @var string
   */
  protected $messageAdded = 'Added.';

  /**
   * Message to display to user on successful submission of add form
   *
   * @var string
   */
  protected $messageUpdated = 'Saved.';

  protected $submitRedirect = false;
  protected $submitMessage = false;
  protected $joinForm = false;

  /**
   * If a group is already loaded for this form (e.g. for an edit form), prevent selecting another one.
   */
  protected $restrictToCurrentGroupParameter = false;

  public $group = null;

  public $customFormInfo = null;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $record = parent::create($container);
    $record->account = $container->get('current_user');
    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->getEntity();

    if ($this->group) {
      if ($this->restrictToCurrentGroupParameter) {
        $this->allowedGroups[] = $this->group->id();
      }
    }

    if ($form_state->get('join_form')) {
      $this->makeJoinForm($form);
    }

    $canEditGroupUserRole = true;
    // Based on allow_changing_core_fields on GMR type, disable editing the core fields
    if ($this->operation === 'edit') {
      $gmrType = GroupMembershipRecordType::load($entity->bundle());
      if (!$gmrType) {
        //@todo throw error
      }
      $canEditGroupUserRole = $gmrType->get('allow_changing_core_fields') != FALSE;
      $this->allowedRoleInstanceTypes = [$entity->bundle()];
      $this->allowedRoles = $entity->getRole() ? [$entity->getRole()->id()] : [];
      $this->allowedUsers = $entity->getUser() ? [$entity->getUser()->id()] : [];

      if ($gmrType->get('require_role') == FALSE) {
        $form['group_role_id']['widget'][0]['target_id']['#required'] = FALSE;
        $form['group_role_id']['widget'][0]['target_id']['#validated'] = FALSE;
      }
    }

    // Make sure user can't select internal roles (used to manage things internally in groups)
    $form['group_role_id']['widget'][0]['target_id']['#selection_handler'] = 'group_role:not_internal';

    // Make sure user can't select internal roles (used to manage things internally in groups)
    $form['group_role_id']['widget'][0]['target_id']['#selection_handler'] = 'group_role:not_internal';

    // Hide label
    $form['status']['#type'] = 'hidden';

    // @todo hide only if set in thirdPartySetting instance_naming = 'none'
    // @todo require if set in thirdPartySetting instance_naming = 'required'
    // But both of these depend on the specific role so would need to do by Ajax?
    $form['name']['#type'] = 'hidden';
    $form['name']['#value'] = 'Instance';

    // Hide fields that the user shouldn't be able to edit,
    // specified either in the positive by fieldsToAllow
    // or thenegative by fieldsToHide
    foreach ($form as $field_name => $data) {
      $ignoreTypes = ['actions', ''];
      if (!isset($form[$field_name]['#type']) || in_array($form[$field_name]['#type'], $ignoreTypes)) {
        continue;
      }

      $notAllowed = count($this->fieldsToAllow) > 0 && !in_array($field_name, $this->fieldsToAllow);
      $hidden = in_array($field_name, $this->fieldsToHide);
      $disabled = in_array($field_name, $this->fieldsToDisable);

      if ($notAllowed || $hidden) {
        $form[$field_name]['#type'] = 'hidden';
      }
      if ($disabled) {
        $form[$field_name]['#disabled'] = true;
      }

      if (isset($this->fieldDefaultValues[$field_name])) {
        $form[$field_name]['#default_value'] = $this->fieldDefaultValues[$field_name];
      }
    }

    if ($this->defaultStartDate) {
      $form['date_range']['widget'][0]['value']['#default_value'] = new DrupalDateTime($this->defaultStartDate);
    }
    if ($this->defaultEndDate) {
      $form['date_range']['widget'][0]['end_value']['#default_value'] = new DrupalDateTime($this->defaultEndDate);
    }

    if (!$canEditGroupUserRole) {
      $form['allow_changing_core_fields_note'] = [
        '#weight' => -96,
        '#markup' => '<p class="form-item__description"><b>Editing the user, group or role after record creation is disabled for this record type.</b> If you need e.g. to have a record for a different role for this user, create a new record. If this record is not accurate in terms of the user, group or role, delete it and create a correct one.</p>'
      ];

      $this->replaceCoreFieldsWithLinks($form, $form_state);
    } else {

      // Load any defaults from query string
      $keys = \Drupal::request()->query->all();
      $allowedLists = ['allowedGroups', 'allowedRoles', 'allowedUsers', 'allowedRoleInstanceTypes'];
      foreach ($allowedLists as $list) {
        if (isset($keys[$list])) {
          $this->$list = explode(',', $keys[$list]);
        }
      }

      // set allowed role instance type if it is set already as a bundle
      $form_object = $form_state->getFormObject();
      if ($form_object instanceof ContentEntityForm) {
        $this->allowedRoleInstanceTypes[] = $form_object->getEntity()->bundle();
      }

      /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
      $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

      if ($this->limitToMembers && $this->group) {
        $members = $this->group->getMembers();
        if (count($members) == 0) {
          \Drupal::messenger()->addMessage("Group has no members; cannot use this form.", 'error');
          return new RedirectResponse($_SERVER['HTTP_REFERER'] ?? $this->group->toUrl()->toString());
        }
        foreach ($members as $member) {
          if (count($this->limitToMembersExcludeWithCurrentRole) > 0) {
            foreach ($this->limitToMembersExcludeWithCurrentRole as $role) {
              if ($gmrRepositoryService->currentInstanceExists($member->getUser(), $this->group, GroupRole::load($role))) {
                continue 2;
              }
            }
          }
          $this->allowedUsers[] = $member->getUser()->id();
        }
      }


      // Limit roles based on instance types, if specified and roles not otherwise limited
      if (count($this->allowedRoleInstanceTypes) > 0) {
        if (count($this->allowedRoles) == 0) {
          $roles = [];
          foreach ($this->allowedRoleInstanceTypes as $group_membership_record_type) {
            $roles = array_merge($roles, $gmrRepositoryService->getRolesForInstanceType($group_membership_record_type));
          }
          $this->allowedRoles = array_keys($roles);
        }
      }

      // Limit roles based on group type, if specified and roles not otherwise limited
      if (count($this->groupTypes) == 0 && count($this->allowedRoleInstanceTypes) > 0) {
        $groupTypes = [];
        foreach ($this->allowedRoleInstanceTypes as $role_instance_type_id) {
          $groupTypes = array_merge($groupTypes, $gmrRepositoryService->getGroupTypesForInstanceType($role_instance_type_id));
        }
        $this->groupTypes = array_keys($groupTypes);
      }

      // Limit roles based on group type, if specified and roles not otherwise limited
      if (count($this->allowedRoles) == 0 && count($this->groupTypes) > 0) {
        $roles = [];
        foreach ($this->groupTypes as $group_type_id) {
          $roles = array_merge($roles, $gmrRepositoryService->getRolesForGroupType($group_type_id));
        }
        $this->allowedRoles = array_keys($roles);
      }

      // We can control autocompletes this way - turn them into selects if needed, limit the autocomplete options, etc.
      // [Class name in group module, ID field name, selection handler name]
      $fieldsToLimit = [
        'allowedRoles' => ['Drupal\group\Entity\GroupRole', 'group_role_id'],
        'allowedGroups' => ['Drupal\group\Entity\Group', 'group_id'],
        'allowedUsers' => ['\Drupal\user\Entity\User', 'user_id'],
      ];
      foreach ($fieldsToLimit as $field => $data) {
        $entity_type = $data[0];
        $id_field = $data[1];
        $selection_handler = 'group_membership_record:limit_to_ids';

        $widget = $form[$id_field]['widget'][0]['target_id'];

        // Set value if we're loading an existing GRI
        if ($entity && $field && $entity->hasField($field) && $entity->get($field)) {
          $currentRef = $entity->get($field)->referencedEntities();
          if (count($currentRef) > 0) {
            $widget['#default_value'] = $currentRef[0]->id();
          }
        }

        // If the autocomplete should be limited to specific IDs
        if (count($this->$field) > 0) {
          // Limit to the Ids given:
          $widget['#selection_handler'] = $selection_handler;
          $widget['#selection_settings']['ids'] = $this->$field;

          // Deal with only having one option
          if (count($this->$field) == 1) {
            // @todo replace with a link to the referenced entity, with no visible input
            $widget['#disabled'] = TRUE;
            $widget['#default_value'] = $entity_type::load($this->$field[0]);
          } else {
            // If only a few, convert to select rather than autocomplete:
            $widget = $this->convertAutocomplete($entity_type, $field, $widget, $entity);
          }
        } else {
          // Limit options to the specified group types
          if (count($this->groupTypes) > 0) {
            // Otherwise, at the very least limit group autocomplete to clubs and socs
            if ($field == 'allowedGroups') {
              // see ['#selection_handler'] in parent form type
              $widget['#selection_settings'] = ['target_bundles' => $this->groupTypes];
            } else if ($field == 'allowedRoles') {
              // Limit roles to only those assigned to this instance type
              // see GroupTypeRoleForInstanceTypeSelection
              $widget['#selection_handler'] = 'group_type:group_role:group_membership_record';
              $widget['#selection_settings']['group_type_ids'] = $this->groupTypes;
              $widget['#selection_settings']['group_membership_record_type_id'] = $entity->bundle();
            }
          }
        }

        $form[$id_field]['widget'][0]['target_id'] = $widget;
      }
    }

    if (count($this->allowedRoleInstanceTypes) > 0) {
      // If there's an "enabled determiner" plugin, prevent the user from controlling the enabled field manually
      // Or at least provide a message
      $pluginManager = \Drupal::service('plugin.manager.group_membership_record.enabled_determiner');
      $enabledDeterminerControlledTypes = [];
      $messageForChangingEnabled = [];
      foreach ($this->allowedRoleInstanceTypes as $group_membership_record_type) {
        $enabledDeterminerPlugins = $pluginManager->getForInstanceType($group_membership_record_type);
        if (count($enabledDeterminerPlugins) > 0) {
          $enabledDeterminerControlledTypes[] = $group_membership_record_type;
        }
        foreach ($enabledDeterminerPlugins as $plugin) {
          $messageForChangingEnabled[] = $plugin->messageForFormEnabledField;
        }
      }
      if (count($enabledDeterminerControlledTypes) > 0) {
        if (count($messageForChangingEnabled) > 0) {
          $description = '<b>Why can\'t I change the enabled field? </b>';
          $description .= implode(' ', $messageForChangingEnabled);
          $form['enabled']['widget']['#suffix'] = '<div class="form-item__description">' . $description . '</div>';
        }

        if (count($enabledDeterminerControlledTypes) == count($this->allowedRoleInstanceTypes)) {
          $form['enabled']['#disabled'] = true;
        }
      }
    }

    if ($this->customFormInfo) {
      $form['custom_info'] = array(
        '#type' => 'markup',
        '#markup' => $this->customFormInfo,
        '#weight' => -1000,
      );
    }

    return $form;
  }

  public function replaceCoreFieldsWithLinks(&$form, $form_state) {
    $record = $form_state->getFormObject()->getEntity();

    $group = $record->getGroup();
    if (!$group) {
      return;
    }

    $form['group_id']['#type'] = 'hidden';
    $form['group_id_link'] = [
      '#type' => 'link',
      '#title' => $group->label(),
      '#description' => 'Cannot be changed once the record is created.',
      "#weight" => -99,
      '#url' => $group->toUrl(),
      '#attributes' => ['target' => '_blank', 'class' => ''],
      '#prefix' => '<div><b>Group:</b> ',
      '#suffix' => '</div>'
    ];

    $account = $record->getOwner();
    $form['user_id']['#type'] = 'hidden';
    $form['user_id_link'] = [
      '#type' => 'link',
      '#title' => $account->getAccountName(),
      '#description' => 'Cannot be changed once the record is created.',
      "#weight" => -98,
      '#url' => $account->toUrl(),
      '#attributes' => ['target' => '_blank', 'class' => ''],
      '#prefix' => '<div><b>User:</b> ',
      '#suffix' => '</div>'
    ];

    $role = $record->getRole();
    $form['group_role_id']['#type'] = 'hidden';
    $form['group_role_id_link'] = [
      '#type' => '#markup',
      '#markup' => $role ? $role->label() : 'Unknown role ' . $record->group_role_id->value,
      '#description' => 'Cannot be changed once the record is created.',
      "#weight" => -97,
      '#prefix' => '<div><b>Role:</b> ',
      '#suffix' => '</div>'
    ];
  }

  /**
   * {@inheritdoc}
   */
  // TODO validate form function - optionally prevent duplicates

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $this->entity->set('name', $this->entity->id());
    $this->entity->set('status', 1);

    if ($this->joinForm) {
      $this->entity->set('enabled', 1);
      $this->entity->set('date_range', [
        'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, \Drupal::time()->getRequestTime()),
      ]);
    }

    $status = parent::save($form, $form_state);

    if ($this->submitMessage) {
      $this->messenger()->addMessage($this->t(strip_tags($this->submitMessage)), 'status');
    } elseif ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t($this->messageAdded, [
        //   '%label' => $entity->label(),
      ]));
    } elseif ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t($this->messageUpdated, [
        //   '%label' => $entity->label(),
      ]));
    }

    $group = $entity->getGroup();

    if ($group) {
      if ($this->submitRedirect) {
        $form_state->setRedirect($this->submitRedirect, ['group' => $entity->getGroup()->id()]);
      } else {
        $form_state->setRedirect('entity.group.canonical', ['group' => $entity->getGroup()->id()]);
      }
    } else {
      // $form_state->setRedirect('entity.group_membership_record.list', []);
    }
  }

  public function convertAutocomplete($entity_type, $field, $widget, $entity) {
    // Load options for select
    $options = [];
    $gmr_field = false;
    $label = '';
    $entities = $entity_type::loadMultiple($this->$field);
    foreach ($entities as $selectEntity) {
      switch ($field) {
        case 'allowedRoles':
          $label = $selectEntity->get('label');
          $gmr_field = 'group_role_id';
          break;

        case 'allowedUsers':
          $label = $selectEntity->get('name')->value;
          $gmr_field = 'user_id';
          break;

        default:
          $label = $selectEntity->get('label')->value;
          $gmr_field = 'group_id';
          break;
      }
      $options[$selectEntity->id()] = $label;
    }

    // Make a select
    $widget['#type'] = 'select';
    $widget['#options'] = $options;
    $widget['#multiple'] = false;
    $widget['#size'] = 1;

    return $widget;
  }

  public function makeJoinForm(&$form) {
    $this->joinForm = true;

    $fields = $this->entity->getFieldDefinitions();
    foreach ($fields as $field_name => $field) {
      if (isset($form[$field_name]) && method_exists($field, 'getThirdPartySettings')) {
        if (!$field->getThirdPartySetting('group_membership_record', 'show_on_join_form')) {
          $this->fieldsToHide[] = $field_name;
        }
      }
    }

    // Base fields to disable for join form
    $baseFields = ['enabled', 'date_range', 'user_id', 'group_id', 'group_role_id'];
    foreach ($baseFields as $field_name) {
      // $this->fieldsToDisable[] = $field_name;
      $this->fieldsToHide[] = $field_name;
    }

    // $gmr = $form_state->getFormObject()->getEntity();
    $gmr = $this->getEntity();
    $group_role = $gmr->get('group_role_id')->referencedEntities()[0];
    $group = $gmr->get('group_id')->referencedEntities()[0];

    $this->submitMessage = 'You have successfully joined ' . $group->label() . '.';

    if ($group_role) {
      $customText = $group_role->getThirdPartySetting('group_membership_record', 'join_form_text');
      if ($customText) {
        $this->customFormInfo = $customText['value'];
      }
      $customButton = $group_role->getThirdPartySetting('group_membership_record', 'join_form_button');
      if ($customButton && $customButton != '') {
        $form['actions']['submit']['#value'] = $customButton;
      }
      $customMessage = $group_role->getThirdPartySetting('group_membership_record', 'join_success_text');
      if ($customMessage) {
        $this->submitMessage = $customMessage['value'];
      }
    }

    $this->submitRedirect = 'entity.group.canonical';
  }
}
