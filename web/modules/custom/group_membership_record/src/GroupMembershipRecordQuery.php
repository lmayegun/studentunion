<?php

namespace Drupal\group_membership_record;

use DateTime;
use Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface;
use Exception;

/**
 * Wrapper of entity query for Group Membership Records.
 *
 * Functions for finding and retrieving group membership records.
 */
class GroupMembershipRecordQuery {

  /**
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  public $query;

  /**
   * Create an entity query.
   */
  public function __construct() {
    $this->query = Drupal::database()->select('group_membership_record', 'gmr');
    return $this;
  }

  /**
   * Get entity query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Get entity query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  public function setQuery($query) {
    $this->query = $query;
  }

  /**
   * Execute entity query and return unique UIDs.
   *
   * @return array
   *   Of user IDs.
   */
  public function executeForUids(): array {
    $uids = [];
    $this->query->fields('gmr', ['user_id']);
    return $this->query->distinct()->execute()->fetchCol('user_id');
  }

  /**
   * Execute entity query.
   *
   * @return array
   *  Of group membership records.
   */
  public function execute() {
    return $this->query->execute();
  }

  /**
   * Execute entity query.
   *
   * @return array
   *  Of group membership records.
   */
  public function executeForIds() {
    $this->query->fields('gmr', ['id']);
    return $this->query->distinct()->execute()->fetchCol('id');
  }

  /**
   * Execute entity query and return count.
   *
   * @return int
   *   Count of records returned by query.
   */
  public function getCount() {
    $query = clone $this->query;
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Set entity query to a count query.
   *
   * @return $this
   */
  public function count() {
    $this->query = $this->query->count();

    return $this;
  }

  /**
   * Filter records by user account.
   *
   * @param AccountInterface $account
   *
   * @return $this
   */
  public function setAccount(AccountInterface $account = NULL) {
    if ($account) {
      $this->query = $this->query->condition('gmr.user_id', $account->id());
    }
    return $this;
  }

  /**
   * Filter records by group.
   *
   * @param GroupInterface $group
   *
   * @return $this
   */
  public function setGroup(GroupInterface $group = NULL) {
    if ($group) {
      $this->query = $this->query->condition('gmr.group_id', $group->id());
    }
    return $this;
  }

  /**
   * Filter records by groups.
   *
   * @param array $group_ids
   *
   * @return $this
   */
  public function setGroups(array $group_ids = NULL) {
    if ($group_ids) {
      $this->query = $this->query->condition('gmr.group_id', $group_ids, 'IN');
    }
    return $this;
  }

  /**
   * Filter records by gmr type.
   *
   * @param \Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface|null $type
   *
   * @return $this
   */
  public function setRecordType(GroupMembershipRecordTypeInterface $type = NULL) {
    if ($type) {
      $this->query = $this->query->condition('gmr.type', $type->id());
    } else {
      throw new Exception('No valid type provided to setRecordType for Group Membership Record query.');
    }
    return $this;
  }

  /**
   * Filter records by group role.
   *
   * @param GroupRoleInterface $group_role
   *
   * @return $this
   */
  public function setRole(GroupRoleInterface $group_role = NULL) {
    if ($group_role) {
      $this->query = $this->query->condition('gmr.group_role_id', $group_role->id());
    } else {
      throw new Exception('No valid role provided to setRole for Group Membership Record query.');
    }
    return $this;
  }

  /**
   * Filter by only enabled records.
   *
   * @param bool $enabled
   *
   * @return $this
   */
  public function setRequireEnabled(bool $enabled = NULL) {
    $this->query = $this->query->condition('gmr.enabled', $enabled);
    return $this;
  }

  /**
   * Filter by only published groups.
   *
   * @param bool $status
   *
   * @return $this
   */
  public function setRequireGroupPublished(bool $status = NULL) {
    $this->query->join('groups_field_data', 'g', 'g.id = gmr.group_id');
    $this->query = $this->query->condition('g.status', $status);
    return $this;
  }

  /**
   * Set dates for record to be current at or between, based on record start
   * and end dates.
   *
   * @param DateTime $date
   * @param null $dateEnd
   *
   * @return $this
   */
  public function setDates(DateTime $date = NULL, $dateEnd = NULL) {
    $recordDateRange = 'date_range';

    if (!$date) {
      return $this;
    }

    // https://blog.werk21.de/en/2018/02/05/date-range-fields-and-entity-query-update
    $startDate = new DrupalDateTime($date->format('Y-m-d H:i:s'), DateTimeItemInterface::STORAGE_TIMEZONE);
    $startDate->setTime(0, 0, 0);

    // Our query assumes a date range
    // If the end date isn't supplied, we just use the startDate as a single day
    if ($dateEnd) {
      $endDate = new DrupalDateTime($dateEnd->format('Y-m-d H:i:s'), DateTimeItemInterface::STORAGE_TIMEZONE);
    } else {
      $endDate = new DrupalDateTime($startDate->format('Y-m-d H:i:s'), DateTimeItemInterface::STORAGE_TIMEZONE);
    }
    $endDate->setTime(23, 59, 59);

    $dateRangeStart = $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $dateRangeEnd = $endDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    // Filtering by range involves:
    // GMR starts during or before range.
    // GMR ends during or after range or ending date not set
    // (so we assume it's continuing).

    $startsBeforeEndsAfter = $this->query->andConditionGroup()
      ->condition($recordDateRange . '__value', $dateRangeStart, '<=')
      ->condition($recordDateRange . '__end_value', $dateRangeEnd, '>=');

    $startsBeforeEndsDuring = $this->query->andConditionGroup()
      ->condition($recordDateRange . '__value', $dateRangeStart, '>=')
      ->condition($recordDateRange . '__end_value', $dateRangeStart, '>=')
      ->condition($recordDateRange . '__end_value', $dateRangeEnd, '<=');

    $startsDuringEndsDuring = $this->query->andConditionGroup()
      ->condition($recordDateRange . '__value', $dateRangeStart, '>=')
      ->condition($recordDateRange . '__end_value', $dateRangeEnd, '<=');

    $startsDuringEndsAfter = $this->query->andConditionGroup()
      ->condition($recordDateRange . '__value', $dateRangeStart, '>=')
      ->condition($recordDateRange . '__end_value', $dateRangeEnd, '>=');

    $startsBeforeNoEnd = $this->query->andConditionGroup()
      ->condition($recordDateRange . '__value', $dateRangeStart, '<=')
      ->condition($recordDateRange . '__end_value', NULL, 'IS');

    $startsDuringNoEnd = $this->query->andConditionGroup()
      ->condition($recordDateRange . '__value', $dateRangeStart, '>=')
      ->condition($recordDateRange . '__value', $dateRangeEnd, '<=')
      ->condition($recordDateRange . '__end_value', NULL, 'IS');

    $dateOptions = $this->query->orConditionGroup()
      ->condition($startsBeforeEndsAfter)
      ->condition($startsBeforeEndsDuring)
      ->condition($startsDuringEndsDuring)
      ->condition($startsDuringEndsAfter)
      ->condition($startsBeforeNoEnd)
      ->condition($startsDuringNoEnd);

    $this->query = $this->query->condition($dateOptions);

    return $this;
  }
}
