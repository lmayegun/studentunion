<?php

namespace Drupal\group_membership_record\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Group membership record type entities.
 */
interface GroupMembershipRecordTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
