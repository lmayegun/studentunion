<?php

namespace Drupal\group_membership_record\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Group membership record type entity.
 *
 * @ConfigEntityType(
 *   id = "group_membership_record_type",
 *   label = @Translation("Group membership record type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\group_membership_record\GroupMembershipRecordTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\group_membership_record\Form\GroupMembershipRecordTypeForm",
 *       "edit" = "Drupal\group_membership_record\Form\GroupMembershipRecordTypeForm",
 *       "delete" = "Drupal\group_membership_record\Form\GroupMembershipRecordTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\group_membership_record\GroupMembershipRecordTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "group_membership_record_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "group_roles",
 *     "allow_changing_core_fields",
 *     "require_role",
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_of = "group_membership_record",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/group/membership_record/type/{group_membership_record_type}",
 *     "add-form" = "/admin/group/membership_record/type/add",
 *     "edit-form" = "/admin/group/membership_record/type/{group_membership_record_type}/edit",
 *     "delete-form" = "/admin/group/membership_record/type/{group_membership_record_type}/delete",
 *     "collection" = "/admin/group/membership_record/type"
 *   }
 * )
 */
class GroupMembershipRecordType extends ConfigEntityBundleBase implements GroupMembershipRecordTypeInterface {

  /**
   * The Group membership record type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Group membership record type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Group role(s) the type applies to.
   *
   * @var string
   */
  protected $group_roles;

  /**
   * Allow user to change core fields (group, user, role) after creation
   *
   * @var boolean
   */
  protected $allow_changing_core_fields;

  /**
   * Require a role for each GMR
   *
   * @var boolean
   */
  protected $require_role;
}
