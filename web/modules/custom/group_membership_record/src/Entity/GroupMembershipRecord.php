<?php

namespace Drupal\group_membership_record\Entity;

use DateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

use Drupal\group_membership_record\Event\GroupMembershipRecordEvent;
use Drupal\group_membership_record\Event\GroupMembershipRecordEventType;

/**
 * Defines the Group membership record entity.
 *
 * @ingroup group_membership_record
 *
 * @ContentEntityType(
 *   id = "group_membership_record",
 *   label = @Translation("Group membership record"),
 *   label_singular = @Translation("group membership record"),
 *   label_plural = @Translation("group membership records"),
 *   label_collection = @Translation("Group membership records"),
 *   label_count = @PluralTranslation(
 *     singular = "@count membership record",
 *     plural = "@count membership records"
 *   ),
 *   bundle_label = @Translation("Group membership record type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\group_membership_record\GroupMembershipRecordListBuilder",
 *     "views_data" = "Drupal\group_membership_record\Entity\GroupMembershipRecordViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\group_membership_record\Form\GroupMembershipRecordForm",
 *       "add" = "Drupal\group_membership_record\Form\GroupMembershipRecordForm",
 *       "edit" = "Drupal\group_membership_record\Form\GroupMembershipRecordForm",
 *       "delete" = "Drupal\group_membership_record\Form\GroupMembershipRecordDeleteForm",
 *       "join" = "Drupal\group_membership_record\Form\GroupMembershipRecordForm",
 *       "end" = "Drupal\group_membership_record\Form\GroupMembershipRecordEndForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\group_membership_record\GroupMembershipRecordHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\group_membership_record\GroupMembershipRecordAccessControlHandler",
 *   },
 *   base_table = "group_membership_record",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer group membership record entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/group/membership_record/{group_membership_record}",
 *     "add-page" = "/admin/group/membership_record/add",
 *     "add-form" = "/admin/group/membership_record/add/{group_membership_record_type}",
 *     "edit-form" = "/admin/group/membership_record/{group_membership_record}/edit",
 *     "delete-form" = "/admin/group/membership_record/{group_membership_record}/delete",
 *     "collection" = "/admin/group/membership_record",
 *   },
 *   bundle_entity_type = "group_membership_record_type",
 *   field_ui_base_route = "entity.group_membership_record_type.edit_form",
 * )
 */
class GroupMembershipRecord extends ContentEntityBase implements GroupMembershipRecordInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => NULL,
      'creator_user_id' => \Drupal::currentUser()->id(),
      'changed_user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->updated_user_id = \Drupal::currentUser()->id();
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return $this->get('group_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getRole() {
    return $this->get('group_role_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->getUser();
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->getUserId();
  }

  /**
   * {@inheritdoc}
   */
  public function setUserId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUser(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->setUserId($uid);
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    return $this->setUser($account);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    // This is not really used but left in case it becomes relevant.
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDefaultValue(TRUE)
      ->setRequired(FALSE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Group membership record entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    // Record metadata:
    $fields['creator_user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user who created the record.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['created_by_default'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Instance created by default'))
      ->setDescription(t('i.e. because that setting was enabled on the role and there were no other instances for that user for that role.'))
      ->setDefaultValue(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['source'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setLabel(t('Entities that triggered the creation of the instance'))
      ->setDescription(t('e.g. commerce license or order'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        // 'type' => 'dynamic_entity_reference_default',
        // 'weight' => 0,
        'region' => 'hidden'
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['changed_user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Last updated by'))
      ->setDescription(t('The user who last updated the record.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['group_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setSetting('target_type', 'group')
      ->setSetting('handler', 'group_membership_record:no_access_checks')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['group_role_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group role'))
      ->setSetting('target_type', 'group_role')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('Note: for some roles, the user must have a current date range to have the access to the group for that role.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['date_range'] = BaseFieldDefinition::create('daterange')
      ->setLabel('Date range')
      ->setDescription(t('Note: for some roles, the user must have a current date range to have the access to the group for that role.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('datetime_type', 'allday')
      ->setSetting('optional_end_date', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'daterange_default'
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // \Drupal::logger('group_membership_record')->notice('Postsave sync');

    $this->sync();

    // Make sure any cache elements for this group are reset
    // Especially important for group permissions
    if ($this->getGroup()) {
      Cache::invalidateTags($this->getGroup()->getCacheTagsToInvalidate());
    }

    $event = new GroupMembershipRecordEvent($update ? GroupMembershipRecordEventType::UPDATE : GroupMembershipRecordEventType::INSERT, $this);
    \Drupal::service('event_dispatcher')->dispatch($event->getEventType(), $event);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrency(DrupalDateTime $datetimeToTest = NULL) {
    $start_date = $this->get('date_range')->start_date;
    $end_date = $this->get('date_range')->end_date;
    if (!$datetimeToTest) {
      $datetimeToTest = new DrupalDateTime();
    }

    if ($end_date == $start_date) {
      $end_date = FALSE;
    }

    if (!$start_date) {
      return FALSE;
    }

    if ($start_date < $datetimeToTest && ($end_date > $datetimeToTest || !$end_date)) {
      return 'current';
    }

    if ($start_date > $datetimeToTest) {
      return 'future';
    }

    if ($end_date < $datetimeToTest) {
      return 'past';
    }

    return FALSE;
  }

  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    foreach ($entities as $gmr) {
      $gmr->sync();

      $event = new GroupMembershipRecordEvent(GroupMembershipRecordEventType::DELETE, $gmr);
      \Drupal::service('event_dispatcher')->dispatch($event->getEventType(), $event);
    }
  }

  /**
   * Check if record is current (date range includes today).
   */
  public function isCurrent() {
    return $this->getCurrency() == 'current';
  }

  /**
   * Check if record is enabled.
   */
  public function isEnabled($refresh = FALSE) {
    if ($refresh) {
      $this->updateEnabled();
    }
    return $this->get('enabled')->value;
  }

  /**
   * Update enabled field when instance is updated.
   *
   * Custom modules can provide plugins that determine what to set it as, per GRI type.
   * However, by default this doesn't change the value at all.
   *
   * @return \Drupal\group_membership_record\Entity\GroupMembershipRecord
   *   Updated GMR.
   */
  public function updateEnabled($save = TRUE) {
    $pluginManager = \Drupal::service('plugin.manager.group_membership_record.enabled_determiner');
    $plugins = $pluginManager->getForInstanceType($this->bundle());
    foreach ($plugins as $plugin) {
      $enabled = $plugin->determineEnabledValue($this);
    }
    if (isset($enabled)) {
      $this->enabled = $enabled;
    }
    if ($save) {
      $this->save();
    }
    return $this;
  }

  /**
   * Get the start date for the record.
   *
   * @return \DateTime
   *   Start date of record.
   */
  public function getStartDate() {
    return DateTime::createFromFormat(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $this->date_range->value);
  }

  /**
   * Get the end date for the record.
   *
   * @return \DateTime
   *   End date of record.
   */
  public function getEndDate() {
    return DateTime::createFromFormat(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $this->date_range->end_value);
  }

  /**
   * Set the start date of the record from a datetime or timestamp.
   *
   * Does not save the record.
   */
  public function setStartDate($datetimeOrTimestamp) {
    $timestamp = is_a($datetimeOrTimestamp, 'DateTime') ? $datetimeOrTimestamp->getTimestamp() : $datetimeOrTimestamp;

    $value = gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $timestamp);

    // Ensure end date is never before start date:
    $end_value = $this->get('date_range')->end_value < $value ? $value : $this->get('date_range')->end_value;

    $this->set('date_range', [
      'value' => $value,
      'end_value' => $end_value,
    ]);
  }

  /**
   * Set the end date of the record from a datetime or timestamp.
   *
   * Does not save the record.
   */
  public function setEndDate($datetimeOrTimestamp) {
    $timestamp = is_a($datetimeOrTimestamp, 'DateTime') ? $datetimeOrTimestamp->getTimestamp() : $datetimeOrTimestamp;

    $end_value = gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $timestamp);

    // Ensure start date is never after end date:
    $value = $this->get('date_range')->value >= $end_value ? $end_value : $this->get('date_range')->value;

    $this->set('date_range', [
      'value' => $value,
      'end_value' => $end_value,
    ]);
  }

  /**
   * Grant or revoke roles on groups and on the site, based on the record.
   */
  public function sync() {
    // Update group roles so access is removed as soon as the GMR ends:
    $this->syncGroupRoles();

    // Update site roles if assigned:
    $this->syncSiteRoles();

    // Trigger an event for hooking in to:
    $event = new GroupMembershipRecordEvent(GroupMembershipRecordEventType::SYNC, $this);
    \Drupal::service('event_dispatcher')->dispatch($event->getEventType(), $event);
  }

  /**
   * Sync the group roles for the record.
   *
   * I.e. grant or revoke group role if set on record.
   */
  public function syncGroupRoles() {
    \Drupal::service('group_membership_record.group_role_sync')->syncGroupRolesForRecord($this);
  }

  /**
   * Sync the site roles for the record.
   *
   * I.e. grant or revoke site role if set on record.
   */
  public function syncSiteRoles() {
    if ($this->hasField('field_site_role') && $this->field_site_role->entity) {
      $account = $this->getUser();
      $roleId = $this->field_site_role->entity->id();

      if ($this->isCurrent() && $this->isEnabled()) {
        $account->addRole($roleId);
        $account->save();
      } else {
        $account->removeRole($roleId);
        $account->save();
      }
    }
  }
}
