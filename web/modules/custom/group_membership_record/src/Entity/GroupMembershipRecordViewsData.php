<?php

namespace Drupal\group_membership_record\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Group membership record entities.
 */
class GroupMembershipRecordViewsData extends EntityViewsData
{

  /**
   * {@inheritdoc}
   */
  public function getViewsData()
  {
    $data = parent::getViewsData();

    // Filters
    $data['group_membership_record']['currency'] = [
      'title' => t('Record is past, current or future'),
      'filter' => [
        'field' => 'date_range',
        'id' => 'group_membership_record_views_filter_currency',
        'title' => t('Instance currency - past, current or future'),
        'help' => t('Filter by whether the instance is past, current or future, based on the instance\'s date range field.'),
      ],
    ];

    $data['group_membership_record']['group_role_non_internal'] = [
      'title' => t('Group roles (excluding internal/outsider roles)'),
      'filter' => [
        'field' => 'date_range',
        'id' => 'group_membership_record_group_role_non_internal',
        'title' => t('Group roles (excluding internal/outsider roles)'),
        'help' => t('Filter by group role (optionally per group type) excluding automatically generated duplicates used for outsider roles.'),
      ],
    ];

    // Fields
    $data['group_membership_record']['currency_field'] = [
      'title' => t('Record is past, current or future'),
      'help' => t(''),
      'field' => [
        'id' => 'group_membership_record_currency_field',
      ],
    ];

    $data['group_membership_record']['group_membership_record_check_enabled_summary'] = [
      'title' => t('Enabled explainer'),
      'help' => t('Show current status (enabled or not) and explanations why.'),
      'field' => [
        'id' => 'group_membership_record_check_enabled_summary',
      ],
    ];

    // Get GMRs for a user
    $data['users_field_data']['uid_gmr']['relationship'] = [
      'title' => $this->t('Group membership records'),
      'help' => $this->t(''),
      'id' => 'standard',
      'base' => 'group_membership_record',
      'base field' => 'user_id',
      'field' => 'uid',
      'label' => $this->t('Group membership records'),
    ];

    return $data;
  }
}
