<?php

namespace Drupal\group_membership_record\Event;

/**
 * Enumeration of entity event types.
 */
class GroupMembershipRecordEventType
{
    const INSERT = 'group_membership_record.insert';
    const UPDATE = 'group_membership_record.update';
    const DELETE = 'group_membership_record.delete';
    const START = 'group_membership_record.start';
    const END = 'group_membership_record.end';
    const ENABLE = 'group_membership_record.enable';
    const DISABLE = 'group_membership_record.disable';
    const SYNC = 'group_membership_record.sync';
}
