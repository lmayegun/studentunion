<?php

namespace Drupal\group_membership_record\Event;

use Drupal\group_membership_record\Entity\GroupMembershipRecordInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when group membership record ...
 */
class GroupMembershipRecordEvent extends Event
{
    /**
     * The group membership record.
     *
     * @var \Drupal\group_membership_record\Entity\GroupMembershipRecordInterface
     */
    public $record;

    /**
     * The event type.
     *
     * @var \Drupal\group_membership_record\Event\GroupMembershipRecordEventType
     */
    private $eventType;

    /**
     * Constructs the object.
     *
     * @param \Drupal\group_membership_record\Entity\GroupMembershipRecordInterface $record
     *   The group membership record.
     */
    public function __construct($event_type, GroupMembershipRecordInterface $record)
    {
        $this->eventType = $event_type;
        $this->record = $record;
    }

    /**
     * Method to get the record.
     *
     * @return \Drupal\group_membership_record\Entity\GroupMembershipRecordInterface $record
     *   The group membership record.
     */
    public function getRecord()
    {
        return $this->record;
    }

    /**
     * Method to get the event type.
     */
    public function getEventType()
    {
        return $this->eventType;
    }
}
