<?php

namespace Drupal\group_membership_record;

use Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\user\Entity\User;

/**
 * Defines a class to build a listing of Group membership record entities.
 *
 * @ingroup group_membership_record
 */
class GroupMembershipRecordListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Group membership record ID');
    $header['user_id'] = $this->t('User');
    $header['group_id'] = $this->t('Group');
    $header['group_role_id'] = $this->t('Role');
    $header['group_membership_record_type'] = $this->t('Record type');
    $header['enabled'] = $this->t('Enabled');
    $header['daterange'] = $this->t('Date range');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
    $row['id'] = $entity->id();
    $user = User::load($entity->get('user_id')->getString());
    $group = Group::load($entity->get('group_id')->getString());
    $role_id = $entity->get('group_role_id')->getString();
    $role = GroupRole::load($role_id);

    $row['user_id'] = $user ? $user->toLink() : $entity->get('user_id')
      ->getString();
    $row['group_id'] = $group ? $group->toLink() : $entity->get('group_id')
      ->getString();
    $row['group_role_id'] = $role ? $role->label() . ' (' . $entity->get('group_role_id')
        ->getString() . ')' : $entity->get('group_role_id')->getString();
    $row['group_membership_record_type'] = GroupMembershipRecordType::load($entity->get('type')
        ->getString())->label() . ' (' . $entity->get('type')
        ->getString() . ')';
    $row['enabled'] = $entity->get('enabled')->value == '1' ? 'Yes' : 'No';

    $start = new DrupalDateTime($entity->get('date_range')->value);
    $end = new DrupalDateTime($entity->get('date_range')->end_value);
    $row['daterange'] = Drupal::service('date.formatter')
        ->format($start->getTimestamp(), 'default_short_date_no_time_') . ' - ' . Drupal::service('date.formatter')
        ->format($end->getTimestamp(), 'default_short_date_no_time_');

    return $row + parent::buildRow($entity);
  }

}
