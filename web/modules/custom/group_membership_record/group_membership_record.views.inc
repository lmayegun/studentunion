<?php

/**
 * Implements hook_views_data_alter().
 */
function group_membership_record_views_data_alter(array &$data) {
  $data['groups']['group_membership_record_group_record_count'] = [
    'title' => t('Group membership record count'),
    'help' => t(''),
    'field' => [
      'id' => 'group_membership_record_group_record_count',
      'click sortable' => TRUE,
    ],
  ];
  $data['views']['group_membership_record_membership_check'] = [
    'title' => t('Group membership record membership check'),
    'help' => t(''),
    'field' => [
      'id' => 'group_membership_record_membership_check',
      'click sortable' => TRUE,
    ],
  ];
  $data['groups']['group_membership_record_membership_current_enabled'] = [
    'title' => t('Group membership record - current and enabled records'),
    'help' => t(''),
    'field' => [
      'id' => 'group_membership_record_membership_current_enabled',
      'click sortable' => FALSE,
    ],
  ];
  $data['groups']['group_membership_record_membership_current_record_by_record_role'] = [
    'title' => t('Group membership record - list users with role in group'),
    'help' => t('Display users who have the selected role in the group.'),
    'field' => [
      'id' => 'group_membership_record_membership_current_record_by_record_role',
      'click sortable' => FALSE,
    ],
  ];
}
