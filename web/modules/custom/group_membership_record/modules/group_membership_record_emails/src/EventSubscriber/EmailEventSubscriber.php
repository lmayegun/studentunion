<?php

namespace Drupal\group_membership_record_emails\EventSubscriber;

use Drupal;
use Drupal\group_membership_record\Entity\GroupMembershipRecordInterface;
use Drupal\group_membership_record\Event\GroupMembershipRecordEvent;
use Drupal\group_membership_record\Event\GroupMembershipRecordEventType;
use Drupal\group_membership_record_emails\Entity\GroupMembershipRecordEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailEventSubscriber implements EventSubscriberInterface
{

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        $events[GroupMembershipRecordEventType::INSERT][] = ['sendMailForEvent', 800];
        $events[GroupMembershipRecordEventType::DELETE][] = ['sendMailForEvent', 800];
        return $events;
    }

    public function sendMailForEvent($event)
    {
        $record = $event->getRecord();
        $emails = $this->loadEmailsForEvent($event);
        foreach ($emails as $email) {
            $status = $email->send($record);
        }
    }

    public function loadEmailsForEvent($event)
    {
        $query = \Drupal::entityQuery('group_membership_record_email')
            ->condition('status', TRUE)
            ->condition('trigger', $event->getEventType());
        $ids = $query->execute();
        if ($ids) {
            $emails_loaded = GroupMembershipRecordEmail::loadMultiple($ids);
            $emails = [];
            foreach ($emails_loaded as $email) {
                if ($email->checkForRecord($event->getRecord())) {
                    $emails[] = $email;
                }
            }
            return $emails;
        }
        return [];
    }
}
