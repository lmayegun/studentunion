<?php

namespace Drupal\group_membership_record_emails\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Group membership record email entities.
 */
interface GroupMembershipRecordEmailInterface extends ConfigEntityInterface
{

  // Add get/set methods for your configuration properties here.
}
