<?php

namespace Drupal\group_membership_record_emails\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Utility\Token;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Event\GroupMembershipRecordEventType;

/**
 * Defines the Group membership record email entity.
 *
 * @ConfigEntityType(
 *   id = "group_membership_record_email",
 *   label = @Translation("Group membership record email"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\group_membership_record_emails\GroupMembershipRecordEmailListBuilder",
 *     "form" = {
 *       "add" = "Drupal\group_membership_record_emails\Form\GroupMembershipRecordEmailForm",
 *       "edit" = "Drupal\group_membership_record_emails\Form\GroupMembershipRecordEmailForm",
 *       "delete" = "Drupal\group_membership_record_emails\Form\GroupMembershipRecordEmailDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\group_membership_record_emails\GroupMembershipRecordEmailHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "group_membership_record_email",
 *   config_export = {
 *     "id",
 *     "label",
 *     "to",
 *     "from_name",
 *     "from_email",
 *     "types",
 *     "group_types",
 *     "roles",
 *     "title",
 *     "body",
 *     "admin_notes",
 *     "trigger",
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/group/membership_record/emails/group_membership_record_email/{group_membership_record_email}",
 *     "add-form" = "/admin/group/membership_record/emails/group_membership_record_email/add",
 *     "edit-form" = "/admin/group/membership_record/emails/group_membership_record_email/{group_membership_record_email}/edit",
 *     "delete-form" = "/admin/group/membership_record/emails/group_membership_record_email/{group_membership_record_email}/delete",
 *     "collection" = "/admin/group/membership_record/emails/group_membership_record_email"
 *   }
 * )
 */
class GroupMembershipRecordEmail extends ConfigEntityBase implements GroupMembershipRecordEmailInterface {

  /**
   * The Group membership record email ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Group membership record email label.
   *
   * @var string
   */
  protected $label;

  /**
   * To email
   *
   * @var string
   */
  protected $to;

  /**
   * From name
   *
   * @var string
   */
  protected $from_name;

  /**
   * From email
   *
   * @var string
   */
  protected $from_email;

  /**
   * Title
   *
   * @var string
   */
  protected $title;

  /**
   * Email body
   *
   * @var string
   */
  protected $body;

  /**
   * Admin notes
   *
   * @var string
   */
  protected $admin_notes;

  /**
   * Trigger
   * - created
   * - enabled
   * - disabled
   * - starts
   * - ends
   * - deleted
   *
   * @var string
   */
  protected $trigger;

  /**
   * Record types
   *
   * @var array
   */
  protected $types;

  /**
   * Group types
   *
   * @var array
   */
  protected $group_types;

  /**
   * Group roles
   *
   * @var array
   */
  protected $roles;

  public function id() {
    return $this->id;
  }

  public static function prepareWithTokens($text, GroupMembershipRecord $record) {
    return \Drupal::token()->replace($text, [
      'group' => $record->getGroup(),
      'group_membership_record' => $record,
      'user' => $record->getUser()
    ]);
  }

  public function send(GroupMembershipRecord $record) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'group_membership_record_emails';
    $key = 'group_membership_record_email';

    $to = $this->to && $this->to != '' ? $this->to : $record->getUser()->getEmail();
    $params['body'] = static::prepareWithTokens($this->get('body'), $record);
    $params['subject'] =  static::prepareWithTokens($this->get('title'), $record);
    $params['from_name'] = $this->get('from_name');
    $params['from_email'] = $this->get('from_email');
    $params['trigger'] = $this->get('trigger');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;

    \Drupal::moduleHandler()->alter('group_membership_record_emails_mail_alter', $to, $params, $send, $this, $record);

    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      // drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
    } else {
      // drupal_set_message(t('Your message has been sent.'));
    }
  }

  public function checkForRecord(GroupMembershipRecord $record) {
    $result = true;

    if (count($this->types) > 0 && !in_array($record->bundle(), $this->types)) {
      $result = false;
    }

    if (count($this->roles) > 0 && $record->getRole() && !in_array($record->getRole()->id(), $this->roles)) {
      $result = false;
    }

    if (count($this->group_types) > 0 && !in_array($record->getGroup()->bundle(), $this->group_types)) {
      $result = false;
    }

    \Drupal::moduleHandler()->alter('group_membership_record_emails_mail_check_record', $result, $this, $record);

    return $result;
  }
}
