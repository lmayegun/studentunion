# Commerce Worldnet (commerce_worldnet)
===============

This module provides a payment gateway mixing the [two types of integration methods from Worldnet](https://docs.worldnettps.com/doku.php?id=developer:api_specification), HP (Hosted Pages) and XML.

The Hosted Page (HP) is an integration method where the entry of some sensitive data is handled
by the Payment Gateway so the merchant’s servers are not exposed to this data. This is advisable so as to
reduce the security overhead of the integrated solution. For this scenario, Worldnet becomes the
responsible party for maintaining the security and integrity of the sensitive data sent to these pages.

The XML integration method allows a Merchant's solution to integrate directly with the Payment Gateway,
perform specific “feature modeled” calls whereby the Payment Gateway processes a request and sends back a
response showing the result of the transaction requested or the errors related to it.

Basically, we use a HP integration method to perform the payment, and the XML method to do a refund request.
Therefore we do not have to keep sensitive data, such as card details but at the same time we have the
capability to perform refunds.

### Process

We send a request to worldnet with two internal urls for them to use. They request
these two urls in the following order:

1. path: "/worldnet/validation/{commerce_order}": Controller to create the payment entity with state pending
2. path: "/worldnet/response/{commerce_order}": Controller to update the payment entity to complete

## Information about refunds

### From Worldnet support:

If you purchase a product and then just after you make a refund, the transaction is marked as VOID.

"There was not other transaction processed for the refund because the refund was processed the same day the original transaction, therefore
it was marked as void instead of a refund. A partial refund will be a separate transaction as it was not the full refund been processed."

### From Worldnet website:

Multiple partial refunds are availaable to perform while the total sum do not exceed 100% of the original price.
This is also true not just for worldnet but commerce won't allow you in the UI to perform a refund over the total
price

"Standard refunds can be performed on any authorized transactions in the Worldnet system, either in the Open Batch or Closed Batch.
By default you can refund any amount up to 100% of the original transaction value. If multiple partial refunds are performed then the sum total of them cannot exceed 100% either. This is to prevent fraud. This 100% value is configurable at a Terminal level and if you wish to alter it please contact support@worldnettps.com with a reason why you need it altered."
[source](https://docs.worldnettps.com/doku.php?id=developer:api_specification:xml_payment_features#standard_refund)"

### Future improvements

## Testing

Please remember when enabling test mode or add the following code in your settings.local.php
[worldnet](https://docs.worldnettps.com/doku.php?id=developer:integration_docs:testing-guide).

```php
$config['commerce_payment.commerce_payment_gateway.worldnet_gateway']['configuration'] = [
  'mode' => 'test'
];
```

[Commerce2 source](https://docs.drupalcommerce.org/commerce2/developer-guide/payments/overriding-payment-config)

### Testing in your local environment

This following configuration is necessary because worldnet cannot reach our local machines.

Add this line to your settings.local.php.

```php
$settings['local_environment'] = true;
```

1. After the purchase of a product go to the order view e.g https://d8.local/admin/commerce/orders/5021
2. Copy the order id e.g. 5021
3. Go to https://d8.local/worldnet/validation/5021
4. Go to https://d8.local/worldnet/response/5021

The module would detect, with the configuration above, that we are in a local environment, and it will bypass
the validations.
