<?php

namespace Drupal\commerce_worldnet\Service;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Site\Settings;
use Drupal\user\Entity\User;
use SimpleXMLElement;

/**
 * Implements Class UclUserService Controller.
 */
class CommerceWorldnetService {

  /**
   * Constructs a new UclUserService object.
   */
  public function __construct() {
  }

  /**
   * This function is used to validate that the Authorisation Response Hash
   * from the server is correct. If authResponseHashIsValid(...) !=
   * $request["HASH"] then an error should be shown and the transaction should
   * not be approved.
   *
   * @param $orderId
   * @param $amount
   * @param $dateTime
   * @param $responseCode
   * @param $responseText
   * @param $responseHash
   * @param $terminalId
   * @param $secret
   *
   * @return bool
   */
  function authResponseHashIsValid($orderId, $amount, $dateTime, $responseCode, $responseText, $responseHash, $terminalId, $secret) {
    return (md5($terminalId . $orderId . $amount . $dateTime . $responseCode . $responseText . $secret) == $responseHash);
  }

  /**
   * Get payment gateway configuration (test or production)
   *
   * @param $order
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getPaymentGatewayConfiguration($order) {
    /** @var PaymentGatewayInterface $payment_gateway */
    $payment_gateways = \Drupal::entityTypeManager()
      ->getStorage('commerce_payment_gateway')
      ->loadMultipleForOrder($order);

    /** @var OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateways['worldnet_gateway']->getPlugin();
    return $payment_gateway_plugin->getConfiguration();
  }

  /**
   * Get from settings.local.php if the running environment
   *
   * @return mixed|null
   */
  function isLocalEnvironment() {
    return Settings::get('local_environment', FALSE);
  }

  /**
   * Find orders which have a payment that has gone through on Worldnet.
   * But which has not approved the order.
   */
  function recoverMissedAuthorisations() {
    $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');
    $logger = \Drupal::logger('commerce_worldnet');

    // $logger->info('recoverMissedAuthorisations in Worldnet...');

    $database = \Drupal::database();
    $query = $database->query(
      "SELECT payment_id FROM {commerce_payment} " .
        "WHERE payment_gateway = 'worldnet_gateway' " .
        "AND state = 'pending' " .
        "AND authorized <= DATE_SUB(NOW(), INTERVAL '1' HOUR) " .
        "AND order_ID NOT IN (SELECT order_id FROM commerce_order WHERE state = 'completed');"
    );
    $results = $query->fetchAllAssoc('payment_id');

    foreach ($results as $payment_id => $row) {
      if (!$row || !$payment_id) {
        continue;
      }

      $payment = Payment::load($payment_id);
      if (!$payment->getOrder()) {
        continue;
      }

      $worldnetPaid = $this->getPaymentStatus($payment);

      if ($worldnetPaid && $payment->getState()->getId() == 'pending') {
        $payment->setState('completed');
        $payment->setCompletedTime(\Drupal::time()->getRequestTime());
        $payment->setAmount($payment->getAmount());
        $payment->save();

        $order = $payment->getOrder();
        $order->setTotalPaid($payment->getAmount());
        $order->getState()->applyTransitionById('place');
        $order->save();

        $message = 'Recovered payment for order after failed initial authorisation: order id: ' . $order->id();
        $commerceLog->generate($order, 'commerce_worldnet_payment_status_template', ['comment' => $message])->save();
        $logger->info($message);
      }
    }
  }

  /**
   * Get payment status from Worldnet.
   *
   * @param  mixed $payment
   * @return bool
   *   True if paid, false if not, null if unknown.
   */
  function getPaymentStatus(Payment $payment) {
    $order = $payment->getOrder();
    $commerceWorldnetService = \Drupal::service('commerce_worldnet');
    $configuration = $commerceWorldnetService->getPaymentGatewayConfiguration($order);

    // https://docs.worldnettps.com/apis/merchant/#operation/getPayment
    $uniqueRef = $payment->getRemoteId();

    if ($configuration['mode'] === 'test') {
      return TRUE;
    }

    $mode = $configuration['mode'] === 'test' ? '_test' : '';
    $secret = $configuration['commerce_worldnet_shared_secret' . $mode];
    $terminalId = $configuration['commerce_worldnet_terminal_id' . $mode];

    $user = User::load(\Drupal::currentUser()->id());
    $user_email = $user->get('mail')->value;

    $date = $this->requestDateTime();

    $worldnet_hpp_url = 'https://' . ($configuration['mode'] == 'test' ? 'test' : '') . 'payments.worldnettps.com/merchant/xmlpayment';
    $hash = md5($terminalId . $uniqueRef . $date . $secret);

    //https://docs.worldnettps.com/doku.php?id=developer:api_specification:xml_payment_features#transaction_retrieving
    $xml_data = [
      'TERMINALID' => $terminalId,
      'UNIQUEREF'  => $uniqueRef,
      'DATETIME'   => $date,
      'HASH'       => $hash,
    ];

    $xml = new SimpleXMLElement('<GET_TRANSACTION_DETAILS/>');
    $xml_data = array_flip($xml_data);
    array_walk_recursive($xml_data, [$xml, 'addChild']);

    // Send the request out with CURL:
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $worldnet_hpp_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: text/xml"]);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);

    if (!empty($result)) {
      $xml = new SimpleXMLElement($result);
      $details = $xml->ADDITIONAL_DETAILS;
      return $details->RESPONSECODE == 'A';
    }
    return NULL;
  }

  // Source: https://docs.worldnettps.com/doku.php?id=developer:sample_codes:php_hosted_payments
  //This generates a DATETIME value in the correct format expected in the request.
  private function requestDateTime() {
    return date('d-m-Y:H:i:s:000');
  }
}
