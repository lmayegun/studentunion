<?php

namespace Drupal\commerce_worldnet\Controller;

use Drupal\commerce_log\CommerceLogServiceProvider;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentStorageInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

class WorldNetController extends ControllerBase {
  /**
   * path: "/worldnet/validation/{commerce_order}"
   *
   * Background validation is necessary for wordlnet to validate the transaction.
   * It sends the information by POST. We validate the transaction against the HASH sent, and
   * then we 'echo' a response that worldnet reads back
   *
   * @param RouteMatchInterface $route_match
   * @return Response
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   */
  function backgroundValidation(RouteMatchInterface $route_match) {
    $logger = \Drupal::logger('commerce_worldnet');
    $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');
    $order = $route_match->getParameter('commerce_order');
    $commerceWorldnetService = \Drupal::service('commerce_worldnet');

    $configuration = $commerceWorldnetService->getPaymentGatewayConfiguration($order);
    $mode = $configuration['mode'] === 'test' ? '_test' : '';
    $orderId = \Drupal::request()->request->get('ORDERID');
    $terminalId = \Drupal::request()->request->get('TERMINALID');
    $uniqueRef = \Drupal::request()->request->get('UNIQUEREF');
    $amount = \Drupal::request()->request->get('AMOUNT');
    $datetime = \Drupal::request()->request->get('DATETIME');
    $responseCode = \Drupal::request()->request->get('RESPONSECODE');
    $responseText = \Drupal::request()->request->get('RESPONSETEXT');
    $responseHash = \Drupal::request()->request->get('HASH');
    $security = $commerceWorldnetService->authResponseHashIsValid($orderId, $amount, $datetime, $responseCode, $responseText, $responseHash, $terminalId, $configuration['commerce_worldnet_shared_secret' . $mode]);
    $message = '';
    $success = FALSE;
    if ($security) {
      if ($orderId) {
        switch ($responseCode) {
          case "A":
            $message = 'OK';
            $success = TRUE;
            break;
          case "R":
          case "D":
          case "C":
          case "S":
          default:
            echo 'OK';
        }
      } else {
        $message = 'Order ID: ' . $order->id() . ' not found.';
      }
    } else {
      $message = 'Background validation hash incorrect.';
    }

    $commerceLog->generate($order, 'commerce_worldnet_payment_status_template', ['comment' => $message])->save();
    $logger->info('Validation response: ' . $message . ' .Order id: ' . $order->id());

    if ($success) {
      $this->createPayment($order, $uniqueRef, $responseCode);
      $logger->info('Payment created: Order id: ' . $order->id());
    }

    return $this->returnMessage($message);
  }

  /**
   * path: "/worldnet/response/{commerce_order}"
   *
   * @param RouteMatchInterface $route_match
   * @return Response
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function response(RouteMatchInterface $route_match) {
    /**
     * @var Order $order
     */
    $order = $route_match->getParameter('commerce_order');
    $validate = $this->validate($order);
    if ($validate['#success']) {
      $route = Url::fromRoute('commerce_payment.checkout.return', ['commerce_order' => $order->id(), 'step' => 'payment'], ['absolute' => TRUE]);
      $response = $this->returnLoadingPage($route);
    } else {
      $response = $this->returnMessage($validate['#message']);
    }

    return $response;
  }

  /**
   * Validate the payment process and set the payment status
   *
   * @param $order
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function validate($order) {
    $background = $_SERVER['REQUEST_METHOD'] == 'POST';
    // For background validation, return the string 'OK'.
    if ($background) {
      echo 'OK';
      return TRUE;
    }

    $logger = \Drupal::logger('commerce_worldnet');
    /** @var CommerceLogServiceProvider $commerceLog * */
    $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');
    $commerceWorldnetService = \Drupal::service('commerce_worldnet');

    $configuration = $commerceWorldnetService->getPaymentGatewayConfiguration($order);
    $mode = $configuration['mode'] === 'test' ? '_test' : '';

    if ($commerceWorldnetService->isLocalEnvironment()) {
      $orderId = $order->id();
      $security = TRUE;
      $uniqueRef = 'localTest';
      $responseCode = 'A';
      // Because in our local, Worldnet cant reach backgroundValidation() we need to create the payment in the only
      // url we can access. We create the payment here and just after we confirm it
      $this->createPayment($order, $uniqueRef, $responseCode);
    } else {
      $orderId = \Drupal::request()->query->get('ORDERID');
      $terminalId = \Drupal::request()->query->get('TERMINALID');
      $uniqueRef = \Drupal::request()->query->get('UNIQUEREF');
      $amount = \Drupal::request()->query->get('AMOUNT');
      $datetime = \Drupal::request()->query->get('DATETIME');
      $responseCode = \Drupal::request()->query->get('RESPONSECODE');
      $responseText = \Drupal::request()->query->get('RESPONSETEXT');
      $responseHash = \Drupal::request()->query->get('HASH');
      $security = $commerceWorldnetService->authResponseHashIsValid($orderId, $amount, $datetime, $responseCode, $responseText, $responseHash, $terminalId, $configuration['commerce_worldnet_shared_secret' . $mode]);
    }

    $message = '';
    $success = FALSE;
    if ($security) {
      if ($orderId) {
        switch ($responseCode) {
          case "A":
            $message .= 'Payment processed successfully. Remote ID: ' . $uniqueRef;
            $success = TRUE;
            break;
          case "R":
          case "D":
          case "C":
          case "S":
          default:
            $message .= 'Payment declined. Please refresh the page and try again with another card. Bank response: ' . $responseText;
        }
      } else {
        $message = 'Order ID: ' . $orderId . ' not found.';
        echo $message;
      }
    } else {
      $message .= 'Payment failed: invalid response hash. Please refresh the page and try again. Please contact the administrator to clarify if you will get charged for this order.';
      if (isset($orderId)) {
        $message .= 'Please quote Gateway Terminal ID: ' . $terminalId . ', and Order ID: ' . $orderId . ' when mailing or calling.';
      }
    }

    $commerceLog->generate($order, 'commerce_worldnet_payment_status_template', ['comment' => $message])->save();

    if ($success) {
      $logger->info('Payment confirmed: Order id: ' . $order->id());
      $this->confirmPayment($order);
      $logger->info($message);
    } else {
      $logger->info('Error in payment. Transaction reference: ' . $order->id());
    }

    return ['#success' => $success, '#message' => $message];
  }

  /**
   * Loading page and redirect to payment success
   *
   * @param $route
   * @return Response
   */
  function returnLoadingPage($route) {
    $build = array(
      'page' => array(
        '#theme'   => 'commerce_worldnet_loading_template',
        '#route'   => $route->toString(),
        '#success' => TRUE,
      ),
    );
    $html = \Drupal::service('renderer')->renderRoot($build);
    $response = new Response();
    $response->setContent($html);
    return $response;
  }

  /**
   * Return message in the iframe
   *
   * @param $message
   * @return Response
   */
  function returnMessage($message) {
    $build = array(
      'page' => array(
        '#theme'   => 'commerce_worldnet_background_validation',
        '#message' => $message,
      ),
    );
    $html = \Drupal::service('renderer')->renderRoot($build);
    $response = new Response();
    $response->setContent($html);
    return $response;
  }

  /**
   * Create payment
   *
   * @param $order
   * @param $uniqueRef
   * @param $responseCode
   * @throws EntityStorageException
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function createPayment($order, $uniqueRef, $responseCode) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state'           => 'pending',
      'amount'          => $order->getTotalPrice(),
      'payment_gateway' => 'worldnet_gateway',
      'authorized'      => \Drupal::time()->getRequestTime(),
      'order_id'        => $order->id(),
      'remote_id'       => $uniqueRef,
      'remote_state'    => $responseCode,
    ]);
    $payment->save();
  }

  /**
   * Confirm the payment for an order.
   *
   * @param mixed $order
   */
  public function confirmPayment($order) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadMultipleByOrder($order);
    foreach ($payments as $payment) {
      $payment->setState('completed');
      $payment->setCompletedTime(\Drupal::time()->getRequestTime());
      $payment->save();
    }
  }
}
