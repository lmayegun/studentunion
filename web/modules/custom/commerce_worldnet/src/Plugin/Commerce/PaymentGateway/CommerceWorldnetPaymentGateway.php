<?php

namespace Drupal\commerce_worldnet\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use SimpleXMLElement;

/**
 * Provides the QuickPay offsite Checkout payment gateway.
 * Resource:
 * https://docs.drupalcommerce.org/commerce2/developer-guide/payments/create-payment-gateway/getting-started
 *
 * @CommercePaymentGateway(
 *   id = "commerce_worldnet_payment_gateway",
 *   label = @Translation("WorldNet (Redirect to WorldNet)"),
 *   display_label = @Translation("WorldNet"),
 *   forms = {
 *     "offsite-payment" =
 *     "Drupal\commerce_worldnet\PluginForm\CommerceWorldnetPaymentForm",
 *     "refund-payment" =
 *     "Drupal\commerce_worldnet\PluginForm\PaymentRefundForm",
 *     "add-payment" = "Drupal\commerce_payment\PluginForm\ManualPaymentAddForm",
 *     "receive-payment" = "Drupal\commerce_payment\PluginForm\PaymentReceiveForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa", "maestro",
 *   },
 * )
 */
class CommerceWorldnetPaymentGateway extends OffsitePaymentGatewayBase implements CommerceWorldnetInterface {

  /**
   * Default configuration for gateway
   *
   * @return array
   */
  public function defaultConfiguration() {
    return [
      'commerce_worldnet_merchant_id'   => '',
      'commerce_worldnet_terminal_id'   => '',
      'commerce_worldnet_shared_secret' => '',
      'commerce_worldnet_email_receipt' => '',

      'commerce_worldnet_merchant_id_test'   => '',
      'commerce_worldnet_terminal_id_test'   => '',
      'commerce_worldnet_shared_secret_test' => '',
      'commerce_worldnet_email_receipt_test' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * Create gateway configuration form
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['commerce_worldnet_merchant_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Merchant ID'),
      '#maxlength'     => 64,
      '#size'          => 64,
      '#default_value' => $this->configuration['commerce_worldnet_merchant_id'],
    ];
    $form['commerce_worldnet_terminal_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Terminal ID'),
      '#maxlength'     => 64,
      '#size'          => 64,
      '#default_value' => $this->configuration['commerce_worldnet_terminal_id'],
    ];
    $form['commerce_worldnet_shared_secret'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Shared secret'),
      '#maxlength'     => 64,
      '#size'          => 64,
      '#default_value' => $this->configuration['commerce_worldnet_shared_secret'],
    ];
    $form['commerce_worldnet_email_receipt'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Email a receipt'),
      '#description'   => $this->t('If enabled, Worldnet will send and email receipt for the payment to the order email address'),
      '#default_value' => $this->configuration['commerce_worldnet_email_receipt'],
    ];

    // Testing configuration
    $form['commerce_worldnet_merchant_id_test'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Merchant ID (test)'),
      '#maxlength'     => 64,
      '#size'          => 64,
      '#default_value' => $this->configuration['commerce_worldnet_merchant_id_test'],
    ];
    $form['commerce_worldnet_terminal_id_test'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Terminal ID (test)'),
      '#maxlength'     => 64,
      '#size'          => 64,
      '#default_value' => $this->configuration['commerce_worldnet_terminal_id_test'],
    ];
    $form['commerce_worldnet_shared_secret_test'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Shared secret (test)'),
      '#maxlength'     => 64,
      '#size'          => 64,
      '#default_value' => $this->configuration['commerce_worldnet_shared_secret_test'],
    ];
    $form['commerce_worldnet_email_receipt_test'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Email a receipt (test)'),
      '#description'   => $this->t('If enabled, Worldnet will send and email receipt for the payment to the order email address'),
      '#default_value' => $this->configuration['commerce_worldnet_email_receipt_test'],
    ];

    return $form;
  }

  /**
   * Submit gateway configuration form
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['commerce_worldnet_merchant_id'] = $values['commerce_worldnet_merchant_id'];
    $this->configuration['commerce_worldnet_terminal_id'] = $values['commerce_worldnet_terminal_id'];
    $this->configuration['commerce_worldnet_shared_secret'] = $values['commerce_worldnet_shared_secret'];
    $this->configuration['commerce_worldnet_email_receipt'] = $values['commerce_worldnet_email_receipt'];

    $this->configuration['commerce_worldnet_merchant_id_test'] = $values['commerce_worldnet_merchant_id_test'];
    $this->configuration['commerce_worldnet_terminal_id_test'] = $values['commerce_worldnet_terminal_id_test'];
    $this->configuration['commerce_worldnet_shared_secret_test'] = $values['commerce_worldnet_shared_secret_test'];
    $this->configuration['commerce_worldnet_email_receipt_test'] = $values['commerce_worldnet_email_receipt_test'];
  }

  /**
   * Refunds the given payment.
   *
   * @param PaymentInterface $payment
   *   The payment to refund.
   * @param Price|null       $amount
   *   The amount to refund. If NULL, defaults to the entire payment amount.
   *
   * @param null             $itemsWithQuantity array keyed with OrderItem IDs with value of quantity being refunded
   * @param null             $adjustments array keyed with adjustment number, values type then amount
   *
   * @throws EntityStorageException Thrown when the transaction fails for any reason.
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL, $itemsWithQuantity = NULL, $adjustments = NULL) {
    $logger = \Drupal::logger('commerce_worldnet');

    $status = $payment->getState()->getValue()['value'];

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    $order = $payment->getOrder();
    try {
      $remote_id = $payment->getRemoteId();
      $decimal_amount = $amount->getNumber();
      // Call to Worldnet for refund
      $result = $this->ProcessRequestToGateway($status, $order, $remote_id, $decimal_amount);
    } catch (\Exception $e) {
      $logger->log('error', 'Error message about the failure');
      throw new PaymentGatewayException('Error message about the failure');
    }

    // Determine whether payment has been fully or partially refunded.
    if ($result['status']) {
      // Add message to commerce log
      $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');
      $commerceLog->generate($order, 'commerce_worldnet_payment_status_template', ['comment' => $result['message']])->save();

      // Mark item as refunded, change quantity and recalculate:
      $refundService = \Drupal::service('commerce_refund_order_item.refund');
      $refundService->refundFromGateway($itemsWithQuantity, $amount, $payment, $adjustments);
    }
    if ($result['status']) {
      \Drupal::messenger()->addMessage($result['message']);
    } else {
      \Drupal::messenger()->addError($result['message']);
    }
  }

  /**
   * Process refund request
   *
   * @param $status
   * @param $order
   * @param $remote_id
   * @param $decimal_amount
   *
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function ProcessRequestToGateway($status, $order, $remote_id, $decimal_amount) {
    $commerceWorldnetService = \Drupal::service('commerce_worldnet');
    $configuration = $commerceWorldnetService->getPaymentGatewayConfiguration($order);

    if ($commerceWorldnetService->isLocalEnvironment()) {
      $status = TRUE;
      $message = 'Testing in local environment. More information on commerce_worldnet/README.md';
    } else {
      $mode = $configuration['mode'] === 'test' ? '_test' : '';
      $secret = $configuration['commerce_worldnet_shared_secret' . $mode];
      $terminalId = $configuration['commerce_worldnet_terminal_id' . $mode];

      $user = User::load(\Drupal::currentUser()->id());
      $user_email = $user->get('mail')->value;

      if ($status) {
        $date = $this->requestDateTime();
        $worldnet_hpp_url = 'https://' . ($configuration['mode'] == 'test' ? 'test' : '') . 'payments.worldnettps.com/merchant/xmlpayment';
        $hash = md5($terminalId . $remote_id . $decimal_amount . $date . $secret);
        $xml_data = [
          'UNIQUEREF'  => $remote_id,
          'TERMINALID' => $terminalId,
          'AMOUNT'     => $decimal_amount,
          'DATETIME'   => $date,
          'HASH'       => $hash,
          'OPERATOR'   => $user_email,
          'REASON'     => 'Web refund',
        ];

        $xml = new SimpleXMLElement('<REFUND/>');
        $xml_data = array_flip($xml_data);
        array_walk_recursive($xml_data, [$xml, 'addChild']);

        // Send the request out with CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $worldnet_hpp_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: text/xml"]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if (!empty($result)) {
          $xml = new SimpleXMLElement($result);
          if ($xml->RESPONSETEXT == 'SUCCESS') {
            $message = $this->t('Successfully refunded £@amount. Remote ID: @ur. ', [
              '@ur'     => $remote_id,
              '@amount' => $decimal_amount,
            ]);
            $status = TRUE;
          } else {
            $message = $this->t('Attempted refund unsuccessful. Worldnet said: @response', ['@response' => $xml->ERRORSTRING]);
          }
        }
      } else {
        $message = $this->t('Attempted second refund of an already refunded order. Remote ID: @ur.', ['@ur' => $remote_id]);
      }
    }

    return ['status' => $status, 'message' => $message];
  }


  // Source: https://docs.worldnettps.com/doku.php?id=developer:sample_codes:php_hosted_payments
  //This generates a DATETIME value in the correct format expected in the request.
  private function requestDateTime() {
    return date('d-m-Y:H:i:s:000');
  }


  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    $operations = parent::buildPaymentOperations($payment);
    $operations['receive'] = [
      'title' => $this->t('Receive'),
      'page_title' => $this->t('Receive payment'),
      'plugin_form' => 'receive-payment',
      'access' => $payment_state == 'pending',
    ];

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['pending']);

    // If not specified, use the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $payment->state = 'completed';
    $payment->setAmount($amount);
    $payment->save();
  }
}
