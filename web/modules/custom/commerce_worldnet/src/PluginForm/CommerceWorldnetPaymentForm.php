<?php

namespace Drupal\commerce_worldnet\PluginForm;

use Drupal\commerce\EntityManagerBridgeTrait;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_price\Price;
use Drupal\commerce_worldnet\Plugin\Commerce\PaymentGateway\CommerceWorldnetInterface;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

class CommerceWorldnetPaymentForm extends BasePaymentOffsiteForm {

  use StringTranslationTrait;


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $host = \Drupal::request()->getHost();
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Adding a div for the iframe
    $form['commerce_worldnet_checkout'] = [
      '#type'       => 'html_tag',
      '#tag'        => 'div',
      '#attributes' => ['id' => 'worldnet-checkout'],
    ];

    // Payment gateway configuration data.
    $mode = $configuration['mode'] === 'test' ? '_test' : '';
    $data['url'] = 'https://' . ($configuration['mode'] === 'test' ? 'test' : '') . 'payments.worldnettps.com/merchant/paymentpage';
    // Add host address for the iframe
    $data['host'] = $host;
    $data['data']['TERMINALID'] = $configuration['commerce_worldnet_terminal_id' . $mode];
    if ($configuration['commerce_worldnet_email_receipt' . $mode]) {
      $data['data']['EMAIL'] = $configuration['commerce_worldnet_email_receipt' . $mode];
    }

    // Payment gateway information needed
    // Verification string
    $response = Url::fromRoute('commerce_worldnet.response', ['commerce_order' => $payment->getOrderId()], ['absolute' => TRUE]);
    $validation = Url::fromRoute('commerce_worldnet.validation', ['commerce_order' => $payment->getOrderId()], ['absolute' => TRUE]);
    $receiptPageURL = $response->toString();
    $validationURL = $validation->toString();
    $data['data']['DATETIME'] = $this->requestDateTime();
    $data['data']['HASH'] = $this->authRequestHash(
      $payment->getOrderId(),
      $payment->getAmount()
        ->getNumber(),
      $this->requestDateTime(),
      $configuration['commerce_worldnet_terminal_id' . $mode],
      $configuration['commerce_worldnet_shared_secret' . $mode],
      $receiptPageURL,
      $validationURL
    );

    $data['data']['RECEIPTPAGEURL'] = $receiptPageURL;
    $data['data']['VALIDATIONURL'] = $validationURL;
    $data['data']['TERMINALTYPE'] = 2;
    $data['data']['TRANSACTIONTYPE'] = 7;
    $data['data']['INIFRAME'] = 'Y';
    $data['data']['IPADDRESS'] = \Drupal::request()->getClientIp();
    $description = $this->t('Order number: @number', ['@number' => $payment->getOrderId()]);
    $data['data']['DESCRIPTION'] = $description->render();

    // Payment data.
    $data['data']['CURRENCY'] = $payment->getAmount()->getCurrencyCode();
    $data['data']['AMOUNT'] = $payment->getAmount()->getNumber();
    $data['data']['ORDERID'] = $payment->getOrderId();

    // Order and billing address.
    $order = $payment->getOrder();
    /** @var \Drupal\address\AddressInterface $billing_address */
    $billing_address = $order->getBillingProfile()
      ->get('address')
      ->first();
    $data['data']['CARDHOLDERNAME'] = $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName();
    $data['data']['POSTCODE'] = $postalcode = $billing_address->getPostalCode();
    $data['data']['ADDRESS1'] = $billing_address->getAddressLine1();
    $data['data']['ADDRESS2'] = $billing_address->getAddressLine2();

    // Attached the data to js so we create a form
    $form['#attached']['library'][] = 'commerce_worldnet/checkout';
    $form['#attached']['drupalSettings']['commerce_worldnet'] = $data;

    // Add cancel button
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Go back to review'),
      '#url'   => Url::fromUri($form['#cancel_url']),
    ];

    return $form;
  }

  // Source: https://docs.worldnettps.com/doku.php?id=developer:sample_codes:php_hosted_payments
  //This generates a DATETIME value in the correct format expected in the request.
  private function requestDateTime() {
    return date('d-m-Y:H:i:s:000');
  }

  # This is used to generate the Authorisation Request Hash.
  private function authRequestHash($orderId, $amount, $dateTime, $terminalId, $secret, $receiptPageURL, $validationURL) {
    return md5($terminalId . $orderId . $amount . $dateTime . $receiptPageURL . $validationURL . $secret);
  }
}
