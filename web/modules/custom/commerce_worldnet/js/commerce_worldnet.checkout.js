(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.commerceWorlnetPayment = {
    attach: function (context) {

      var options = drupalSettings.commerce_worldnet;

      //var $paymentForm = $('.payment-redirect-form', context);
      var $paymentForm = $('#worldnet-checkout', context);

      if ($paymentForm.length > 0) {

        $paymentForm.once('init-worldnet-checkout').each(function () {

          // Create iframe
          // Source:
          // https://stackoverflow.com/questions/4401780/loading-a-form-into-an-iframe
          var checkoutIframe = document.createElement('iframe');
          checkoutIframe.src = "about:blank";
          checkoutIframe.style.top = "0px";
          checkoutIframe.style.position = 'inherit';
          checkoutIframe.style.display = 'block';
          checkoutIframe.scrolling = 'no';
          checkoutIframe.style.zIndex = 100;
          checkoutIframe.style.border = "solid #48D236 10px";
          checkoutIframe.height = "800";
          checkoutIframe.width = "100%";
          checkoutIframe.style.border = 0;
          checkoutIframe.id = "worldnetFrame";
          checkoutIframe.name = "worldnetFrame";

          document.body.appendChild(checkoutIframe);

          var idocument = checkoutIframe.contentWindow.document;

          var form = idocument.createElement("form");
          form.setAttribute("method", "POST");
          form.setAttribute("target", "worldnetFrame");
          form.setAttribute(
            "action",
            options.url
          );

          for (var key in options.data) {
            var hiddenField = idocument.createElement("input");
            hiddenField.setAttribute('type', "hidden");
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', options.data[key]);
            form.appendChild(hiddenField);
          }

          checkoutIframe.appendChild(form);

          // Add the newly created element and its content into the DOM
          var correctPlace = document.getElementById("worldnet-checkout");
          correctPlace.parentNode.insertBefore(checkoutIframe, correctPlace);

          form.submit();

          return false;
        });
      }
    }
  };


})(jQuery, Drupal, drupalSettings);
