<?php

/**
 * Implements hook_views_data_alter().
 */
function su_travel_reclaim_views_data_alter(array &$data) {
  $data['webform_submission']['su_travel_reclaim_reclaims_count'] = [
    'title' => t('Count of linked travel reclaims to fixture'),
    'help' => t(''),
    'field' => [
      'id' => 'su_travel_reclaim_reclaims_count',
      'click sortable' => TRUE,
    ],
  ];
  $data['webform_submission']['su_travel_reclaim_reclaims_total_cost'] = [
    'title' => t('Total cost of linked travel reclaims to fixture'),
    'help' => t(''),
    'field' => [
      'id' => 'su_travel_reclaim_reclaims_total_cost',
      'click sortable' => TRUE,
    ],
  ];
}
