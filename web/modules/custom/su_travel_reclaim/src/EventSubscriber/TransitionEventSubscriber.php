<?php

namespace Drupal\su_travel_reclaim\EventSubscriber;

use Drupal;
use Drupal\webform_workflows_element\Event\WebformSubmissionWorkflowTransitionEvent;
use Drupal\webform_workflows_element\Service\WebformWorkflowsManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TransitionEventSubscriber.
 *
 * @package Drupal\webform_workflows_element\EventSubscriber
 */
class TransitionEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\webform_workflows_element\Service\WebformWorkflowsManager
   */
  protected WebformWorkflowsManager $workflowsManager;

  /**
   * Constructs.
   *
   * @param \Drupal\webform_workflows_element\Service\WebformWorkflowsManager
   *   The workflows manager.
   */
  public function __construct(WebformWorkflowsManager $workflowsManager) {
    $this->workflowsManager = $workflowsManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      WebformSubmissionWorkflowTransitionEvent::EVENT_NAME => 'onTransition',
    ];
  }

  /**
   * Subscribe to the user login event dispatched.
   *
   * @param \Drupal\webform_workflows_element\Event\WebformSubmissionWorkflowTransitionEvent $event
   *   Event object.
   *
   */
  public function onTransition(WebformSubmissionWorkflowTransitionEvent $event) {
    $webform_submission = $event->submission;
    $webform = $webform_submission->getWebform();
    
    $transition = $event->getTransition();
    if (!$transition) {
      return;
    }

    if ($webform->id() == 'create_fixture') {
      $this->fixtureTransition($event);
    }

    if ($webform->id() == 'create_travel_expense') {
      $this->reclaimTransition($event);
    }
  }

  public function fixtureTransition($event) {
    $workflowsManager = Drupal::service('webform_workflows_element.manager');

    // Fixture transition -> Reclaim transition
    $matchingTransitions = [
      'mark_completed' => 'mark_as_paid',
      'mark_completed_no_payment' => NULL,
      'approve_all_as_teamucl_budget' => 'approve_as_teamucl_budget',
      'approve_as_teamucl' => 'approve_as_teamucl',
      'approve_as_fixture_administrator' => 'approve_as_fixture_administrator',
    ];

    if (in_array($transition->id(), array_keys($matchingTransitions))) {
      $reclaimSubmissions = su_travel_reclaim_get_reclaims_for_fixture($event->submission);

      // Mark all appropriate reclaims for the fixture the same way.
      foreach ($reclaimSubmissions as $reclaimSubmission) {
        $success = $workflowsManager->runTransition($reclaimSubmission, $event->element_id, $matchingTransitions[$transition->id()]);
        if ($success) {
          $reclaimSubmission->save();
        }
        else {

        }
      }
    }
  }

  public function reclaimTransition($event) {

  }

}
