<?php

namespace Drupal\su_travel_reclaim\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class TravelReclaimsController.
 */
class TravelReclaimsController extends ControllerBase implements ContainerInjectionInterface {

  public function redirectFromWebform($destination = NULL) {
    $referrer = $_SERVER['HTTP_REFERER'];
    return new RedirectResponse('/');
  }

}
