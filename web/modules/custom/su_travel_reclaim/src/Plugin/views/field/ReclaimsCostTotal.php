<?php

namespace Drupal\su_travel_reclaim\Plugin\views\field;

use DateTime;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\su_travel_reclaim\Plugin\views\field\ReclaimsTrait;
use Drupal\views\Plugin\views\field\Standard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sum value of reclaims.
 *
 * @ViewsField("su_travel_reclaim_reclaims_total_cost")
 */
class ReclaimsCostTotal extends Standard {

  use ReclaimsTrait;

  public function calculateValue($values) {
    $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
    $webform_submissions = $this->getRecords($values);

    $number = 0;
    foreach ($webform_submissions as $webform_submission) {
      $number += (float) $webform_submission->getElementData('cost');
    }

    return $currency_formatter->format($number, 'GBP');
  }
}
