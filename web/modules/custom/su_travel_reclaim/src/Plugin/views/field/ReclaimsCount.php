<?php

namespace Drupal\su_travel_reclaim\Plugin\views\field;

use DateTime;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\su_travel_reclaim\Plugin\views\field\ReclaimsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Count of number of membership records for a group
 *
 * @ViewsField("su_travel_reclaim_reclaims_count")
 */
class ReclaimsCount extends NumericField {

  use ReclaimsTrait;

  public function calculateValue($values) {
    $submissions = $this->getRecords($values);
    $count = count($submissions);
    return $count;
  }
}
