<?php

namespace Drupal\su_travel_reclaim\Plugin\views\field;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\webform\Entity\WebformSubmission;

/**
 * [Description ReclaimsTrait]
 */
trait ReclaimsTrait {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['status'] = ['default' => 'pending'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['status'] = [
      '#type' => 'select',
      '#options' => [
        'all' => 'All',
        'pending' => 'Pending',
        'rejected' => 'Rejected',
        'paid' => 'Paid',
      ],
      '#title' => $this->t('Reclaims to count'),
      '#default_value' => $this->options['status'],
    ];
  }

  public function getRecords(ResultRow $values) {
    $webform_submission = $this->getEntity($values);
    $webform = $webform_submission->getWebform();

    $records = [];

    $status = $this->options['status'];

    $query = Drupal::service('webform_query');
    $query->setWebform('create_travel_expense');
    $query->addCondition('fixture', $webform_submission->id(), '=');

    switch ($status) {
      case 'all':
        break;

      case 'pending':
        $query->addCondition('workflow', 'paid', '<>');
        break;

      case 'rejected':
        $query->addCondition('workflow', 'rejected', '=');
        break;

      case 'paid':
        $query->addCondition('workflow', 'paid', '=');
        break;
    }
    $sids = array_column($query->execute(), 'sid');
    $submissions = WebformSubmission::loadMultiple($sids);

    $returnSubmissions = [];

    foreach ($submissions as $submission) {
      $workflowValues = $submission->getElementOriginalData('workflow');
      if (!$workflowValues) {
        continue;
      }
      $state = $workflowValues['workflow_state'];

      switch ($status) {
        case 'all':
          $returnSubmissions[] = $submission;
          break;

        case 'pending':
          if ($state != 'paid' && $state != 'rejected') {
            $returnSubmissions[] = $submission;
          }
          break;

        case 'paid':
        case 'rejected':
          if ($state == $status) {
            $returnSubmissions[] = $submission;
          }
          break;
      }
    }

    return $returnSubmissions;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $webform_submission = $this->getEntity($values);

    if ($webform_submission && $webform_submission->access('view')) {
      // Cache this value to avoid recalculating unecessarily for big views.
      $cid = __METHOD__ . $webform_submission->id();
      $options = '';
      foreach ($this->options as $key => $value) {
        if (is_array($value)) {
          $options .= print_r($value, TRUE);
        }
        else {
          $options .= $value;
        }
      }
      $cid .= hash('sha256', $options);

      if ($cache = Drupal::cache()->get($cid)) {
        $value = $cache->data;
      }
      else {
        $value = $this->calculateValue($values);

        Drupal::cache()
          ->set($cid, $value, Cache::PERMANENT, $this->getCacheTags($values));
      }

      // return count
      $alias = $this->field_alias;
      $values->$alias = $value;
    }

    return parent::render($values);
  }

  public function getCacheTags($values) {
    $webform_submission = $this->getEntity($values);
    $tags = $webform_submission->getCacheTags();
    return $tags;
  }

}
