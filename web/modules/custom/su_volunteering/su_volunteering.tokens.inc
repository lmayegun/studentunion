<?php

/**
 * @file
 * Builds placeholder replacement tokens for volunteering-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;
use Drupal\su_volunteering\Form\SettingsForm;

/**
 * Implements hook_token_info().
 */
function su_volunteering_token_info() {
  $types = [];
  $types['su_volunteering'] = [
    'name' => t("Volunteering settings"),
  ];

  $su_volunteering = [];

  $directories = SettingsForm::getDirectories();
  foreach ($directories as $key => $data) {
    $su_volunteering['directory_text_' . $key] = [
      'name' => t("Directory text: @label", ['@label' => $data['#title']]),
    ];
  }

  return [
    'types' => $types,
    'tokens' => [
      'su_volunteering' => $su_volunteering,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function su_volunteering_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'su_volunteering') {
    foreach ($tokens as $name => $original) {
      $config = \Drupal::config('su_volunteering.settings');
      $value = $config->get($name) ? $config->get($name)['value'] : '';
      $replacements[$original] = Markup::create($value);
    }
  }
  return $replacements;
}
