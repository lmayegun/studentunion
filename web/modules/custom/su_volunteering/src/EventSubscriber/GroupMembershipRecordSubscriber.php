<?php

namespace Drupal\su_volunteering\EventSubscriber;

use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Event\GroupMembershipRecordEvent;
use Drupal\group_membership_record\Event\GroupMembershipRecordEventType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GroupMembershipRecordSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      GroupMembershipRecordEventType::INSERT => ['onInsert', 100],
      GroupMembershipRecordEventType::UPDATE => ['onUpdate', 100],
      GroupMembershipRecordEventType::DELETE => ['onDelete', 100],
    ];
    return $events;
  }

  public function appliesToRecord($record) {
    $role = $record->getRole();
    $placement = FALSE;
    $leadership = FALSE;

    if ($role) {
      $roleId = $role->id();

      if (in_array($roleId, ['volunteering_opp-placement'])) {
        $placement = true;
      }
    }
    if ($placement) {
      $group = $record->getGroup();
      if ($group->hasField('field_vol_opp_leadership')) {
        if ($group->field_vol_opp_leadership->value == true) {
          $leadership = true;
        }
      }
    }
    return $placement && $leadership;
  }

  public function updateOrCreateLeadershipRecord($placementRecord) {
    if (!$this->appliesToRecord($placementRecord)) {
      return false;
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $opp = $placementRecord->getGroup();

    $parentOrg = su_volunteering_get_org_for_opp($opp);

    if (!$parentOrg) {
      return false;
    }

    $leaderRole = GroupRole::load('volunteering_org-leader');

    $records = \Drupal::entityTypeManager()->getStorage('group_membership_record')->loadByProperties(
      [
        'group_id' => $parentOrg->id(),
        'group_role_id' => $leaderRole->id(),
        'field_opportunity_placement' => $placementRecord->id(),
      ]
    );
    if (count($records) == 0) {
      $records = [
        GroupMembershipRecord::create([
          'type' => 'volunteering_leader',
          'group_id' => $parentOrg->id(),
          'group_role_id' => $leaderRole->id(),
          'user_id' => $placementRecord->getUser()->id()
        ])
      ];
    }

    /**
     * @var GroupMembershipRecord $leadershipRecord
     */
    foreach ($records as $leadershipRecord) {
      $leadershipRecord->field_opportunity_placement->entity = $placementRecord;
      $leadershipRecord->setStartDate($placementRecord->getStartDate());
      $leadershipRecord->setEndDate($placementRecord->getEndDate());
      $leadershipRecord->save();
    }
  }

  public function onInsert(GroupMembershipRecordEvent $event) {
    $record = $event->getRecord();
    $this->updateOrCreateLeadershipRecord($record);
  }

  public function onUpdate(GroupMembershipRecordEvent $event) {
    $record = $event->getRecord();
    $this->updateOrCreateLeadershipRecord($record);
  }

  public function onDelete(GroupMembershipRecordEvent $event) {
    $placementRecord = $event->getRecord();
    if (!$this->appliesToRecord($placementRecord)) {
      return false;
    }

    $opp = $placementRecord->getGroup();
    $parentOrg = su_volunteering_get_org_for_opp($opp);
    if (!$parentOrg) {
      return false;
    }

    $leaderRole = GroupRole::load('volunteering_org-leader');

    $records = \Drupal::entityTypeManager()->getStorage('group_membership_record')->loadByProperties(
      [
        'group_id' => $parentOrg->id(),
        'group_role_id' => $leaderRole->id(),
        'field_opportunity_placement' => $placementRecord->id(),
      ]
    );
    foreach ($records as $record) {
      $record->delete();
    }
  }
}
