<?php

namespace Drupal\su_volunteering\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for placements. 
 *
 * @ingroup Students' Union UCL
 */
class PlacementForm extends GroupMembershipRecordForm
{

  /**
   * {@inheritdoc}
   */
  protected $groupTypes = ['volunteering_opportunity'];

  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Created placement.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved placement.';

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = ['volunteering_opp-placement'];

  /**
   * {@inheritdoc}
   */
  protected $allowedMembershipRecordTypes = ['volunteering_placement'];

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $entity = $this->entity;
    parent::save($form, $form_state);

    // @todo work out how to deal with roles assigned by subgroup 
    // (i.e. if this placement is current & enabled, subgroup inheritance of roles in organisation yes;
    // if not, subgroup inheritance no)
  }
}
