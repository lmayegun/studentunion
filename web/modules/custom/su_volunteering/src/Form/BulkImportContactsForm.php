<?php

namespace Drupal\su_volunteering\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\group\Entity\Group;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportContactsForm extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_volunteering_bulk_contacts';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<p>This will create contacts for volunteering orgs if not already there only. Upload a file with the following columns:<ul>' .
        '<li><b>ID</b>: org ID</li>' .
        '<li><b>E-mail</b>: org e-mail</li>' .
        '<li><b>First contact name	First contact email	First contact phone	Second contact name	Second contact email	Second contact phone	Third contact name	Third contact email	Third contact phone                </b>: either UCL e-mail address or user ID</li>' .
        '</ul></p>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    // Import as normal
    parent::setOperations($batch_builder, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $group = Group::load($row['ID']);
    if (!$group || $group->bundle() != 'volunteering_org') {
      return FALSE;
    }
    if (isset($row['E-mail']) && $group->field_vol_org_email->value == '' && Drupal::service('email.validator')
        ->isValid($row['E-mail'])) {
      $group->field_vol_org_email = $row['E-mail'];
    }
    $names = ['First', 'Second', 'Third'];
    foreach ($names as $name) {
      if (!isset($row[$name . ' contact name']) || $row[$name . ' contact name'] == '') {
        continue;
      }
      $field = [
        'name' => $row[$name . ' contact name'],
        'email' => $row[$name . ' contact email'],
        'phone' => $row[$name . ' contact phone'],
      ];
      $alreadyThere = FALSE;
      foreach ($group->field_vol_org_contacts as $contact) {
        if ($contact->name == $row[$name . ' contact name']) {
          $alreadyThere = TRUE;
        }
      }
      if (!$alreadyThere) {
        $group->field_vol_org_contacts[] = $field;
      }
    }
    $group->save();
  }

}
