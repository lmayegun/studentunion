<?php

namespace Drupal\su_volunteering\Form;

use Drupal;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportVolunteeringProfile extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_volunteering_profile_import';
  }

  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['User'] = [
      'description' => 'either UCL e-mail address, UPI or UCL user ID',
      'example' => 'uczxmke, uczxmke@ucl.ac.uk or m.keeble.21@ucl.ac.uk',
      'required' => TRUE,
    ];

    $fields['Training Project Lead'] = [
      'description' => 'Include `1` to fill the field. Empty field or `0` will uncheck the field.',
      'example' => '1',
    ];

    return $fields;
  }

  /**
   * @param $row
   * @return bool
   */
  public static function validateRow($row): bool {
    /** @var Profile $profile */
    /** @var User $user_id */
    $service = Drupal::service('su_user.ucl_user');

    if ( isset($row['User']) ){
      $user = $service->getUserForUclFieldInput($row['User']);
      if ($user != null) {
        return true;
      }
    }

    return false;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processRow(array $row, array &$context) {
    $valid = static::validateRow($row);
    if (!$valid) {
      static::addError($row,$context, 'review row data');
      return FALSE;
    }

    $training_project_lead = empty($row['Training Project Lead']) ? 0 : $row['Training Project Lead'];

    /** @var Profile $profile */
    $service = Drupal::service('su_user.ucl_user');

    if ( isset($row['User']) ){
      $user = $service->getUserForUclFieldInput($row['User']);
      if ($user != null) {
        $profile = \Drupal::entityTypeManager()
          ->getStorage('profile')
          ->loadByUser( $user, 'volunteering');

        if (!$profile) {
          $profile = Profile::create([
            'type' => 'volunteering',
            'uid'  => $user->id()
          ]);
        }
      }
    }

    // Set Training Project Lead Field
    if ($training_project_lead == 1 && ($profile != null)) {
      $profile->set('field_vol_training_project_lead','1');
    } else {
      $profile->set('field_vol_training_project_lead','0');
    }

    $profile->save();
  }
}
