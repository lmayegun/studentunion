<?php

namespace Drupal\su_volunteering\Form;

use Drupal;
use Drupal\su_groups\Form\BulkImportGroupMembershipRecordForm;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportVolunteeringPlacements extends BulkImportGroupMembershipRecordForm {

  const groupMembershipRecordType = 'volunteering_placement';

  public static function getGroup($row) {
    $volunteeringService = Drupal::service('su_volunteering.opportunity');
    $opportunity = trim($row['Opportunity ID']);
    return $volunteeringService->getOppByID($opportunity);
  }

  public static function getHours($row): int {
    $hours = trim($row['Hours']);
    return intval($hours);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_volunteering_placements_import';
  }

  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = parent::getMapping();

    unset($fields['Group ID']);

    $fields['Opportunity ID'] = [
      'description' => 'Opportunity ID',
      'example' => '10412',
    ];

    $fields['Hours'] = [
      'description' => 'Hours of placement',
      'example' => '4',
    ];

    return $fields;
  }

  public static function updateGmrFromRow(&$gmr, $row) {
    parent::updateGmrFromRow($gmr, $row);

    if (isset($row['Hours'])) {
      $gmr->set('field_volunteering_hours', static::getHours($row));
    }
  }

}
