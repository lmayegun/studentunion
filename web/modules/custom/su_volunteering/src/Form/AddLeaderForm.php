<?php

namespace Drupal\su_volunteering\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class AddLeaderForm extends AddPlacementForm {
  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added leader.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved leader.';

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = ['volunteering_org-leader'];

  /**
   * {@inheritdoc}
   */
  protected $allowedMembershipRecordTypes = ['volunteering_leader'];

  /**
   * {@inheritdoc}
   */
  protected $fieldsToAllow = ['user_id', 'date_range', 'enabled'];

  /**
   * {@inheritdoc}
   */
  // protected $fieldsToHide = ['enabled'];
}
