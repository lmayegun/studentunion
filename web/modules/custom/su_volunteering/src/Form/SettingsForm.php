<?php

namespace Drupal\su_volunteering\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Students Union UCL Volunteering settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_volunteering_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['su_volunteering.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('su_volunteering.settings');

    $form['tabs'] = [
      '#type' => 'vertical_tabs'
    ];

    // Directories
    $form['tab_directories'] = [
      '#title' => 'Directories',
      '#type' => 'details',
      '#group' => 'tabs',
    ];

    $dirs = static::getDirectories();
    foreach ($dirs as $key => $data) {
      $key = 'directory_text_' . $key;
      if (isset($data['tokens'])) {
        $tokens = [];
        foreach ($data['tokens'] as $token => $description) {
          $tokens[] = '<b>' . $token . '</b> ' . $description;
        }
        $data['#description'] = 'Replacement tokens: ' . implode(" • ", $tokens);
        unset($data['tokens']);
      }

      $form[$key] = array_merge([
        '#group' => 'tab_directories',
        '#type' => 'text_format',
        '#default_value' => $config->get($key . '.value'),
        '#format'        => 'basic_html',
        '#allowed_formats' => ['basic_html'],
      ], $data);
    }

    // Blocks
    $form['tab_block'] = [
      '#title' => 'Blocks',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['block_leaders_community'] = [
      '#group' => 'tab_block',
      '#type' => 'text_format',
      '#title' => t('Information block - community organisation'),
      '#default_value' => $config->get('block_leaders_community.value'),
      '#format'        => 'basic_html',
      '#allowed_formats' => ['basic_html']
    ];
    $form['block_leaders_slp'] = [
      '#group' => 'tab_block',
      '#type' => 'text_format',
      '#title' => t('Information block - SLP'),
      '#default_value' => $config->get('block_leaders_slp.value'),
      '#format'        => 'basic_html',
      '#allowed_formats' => ['basic_html']
    ];

    // Emails
    $form['tab_emails'] = [
      '#title' => 'E-mails',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['tab_mod_emails'] = [
      '#title' => 'Moderation e-mails',
      '#type' => 'details',
      '#group' => 'tabs',
    ];

    $emails = static::getEmails();
    foreach ($emails as $key => $data) {
      if (isset($data['tokens'])) {
        $tokens = [];
        foreach ($data['tokens'] as $token => $description) {
          $tokens[] = '<b>' . $token . '</b> ' . $description;
        }
        $data['#description'] = 'Replacement tokens: ' . implode(" • ", $tokens);
        unset($data['tokens']);
      }

      $form[$key . '_subject'] = array_merge(
        [
          '#group' => 'tab_emails',
          '#type' => 'textfield',
          '#default_value' => $config->get($key . '_subject'),
          '#format'        => 'e_mail_html',
          '#allowed_formats' => ['e_mail_html'],
        ],
        $data,
        ['#title' => $data['#title'] . ': subject']
      );

      $form[$key] = array_merge([
        '#group' => 'tab_emails',
        '#type' => 'text_format',
        '#default_value' => $config->get($key . '.value'),
        '#format'        => 'e_mail_html',
        '#allowed_formats' => ['e_mail_html'],
      ], $data);
    }

    // Opportunity messages
    $form['tab_opp_messages'] = [
      '#title' => 'Opportunity messages',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['opp_message_community'] = [
      '#group' => 'tab_opp_messages',
      '#type' => 'text_format',
      '#title' => t('Opportunity message - community organisation'),
      '#default_value' => $config->get('opp_message_community.value'),
      '#format'        => 'basic_html',
      '#allowed_formats' => ['basic_html']
    ];
    $form['opp_message_slp'] = [
      '#group' => 'tab_opp_messages',
      '#type' => 'text_format',
      '#title' => t('Opportunity message - SLP'),
      '#default_value' => $config->get('opp_message_slp.value'),
      '#format'        => 'basic_html',
      '#allowed_formats' => ['basic_html']
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('su_volunteering.settings')
      ->set('block_leaders_community', $form_state->getValue('block_leaders_community'))
      ->set('block_leaders_slp', $form_state->getValue('block_leaders_slp'))
      ->set('opp_message_community', $form_state->getValue('opp_message_community'))
      ->set('opp_message_slp', $form_state->getValue('opp_message_slp'));

    foreach (static::getEmails() as $email_key => $data) {
      $config->set($email_key, $form_state->getValue($email_key));
      $config->set($email_key . '_subject', $form_state->getValue($email_key . '_subject'));
    }
    foreach (static::getDirectories() as $key => $data) {
      $key = 'directory_text_' . $key;
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  public static function getDirectories() {
    return [
      'main' => ['#title' => t('Main'),],
      'map' => ['#title' => t('Map'),],
      'skills' => ['#title' => t('Skills browser'),],
      'online' => ['#title' => t('Online opportunities'),],
      'east_london' => ['#title' => t('East London'),],
      'category' => ['#title' => t('Category'),],
      'skill' => ['#title' => t('Skill'),],
    ];
  }

  public static function getEmails() {
    $tokens_opps =
      [
        '[group:title]' => 'opportunity title',
        '[group:url]' => 'opportunity URL',
        '[group:edit-url]' => 'opportunity edit URL',
        // '[group:type-name]' => 'type of group (opportunity or organisation)',
      ];

    $tokens_orgs =
      [
        '[group:title]' => 'organisation title',
        '[group:url]' => 'organisation URL',
        '[group:edit-url]' => 'organisation edit URL',
        // '[group:type-name]' => 'type of group (opportunity or organisation)',
      ];

    $tokens_digest = array_merge(
      $tokens_orgs,
      [
        '[interest-list]' => 'the list of expressions of interest',
        '[expression-of-interest-count]' => 'the number of expressions of interest for the organisation',
      ],
    );

    return [
      'email_interest' => [
        '#title' => t('E-mail sent to someone who registers interest'),
        'tokens' => array_merge(
          $tokens_opps,
          [
            '[group:field_vol_opp_how_to_apply:value]' => 'the "how to apply" information from the opp'
          ]
        ),
      ],
      'email_digest_slp' => [
        '#title' => t('Digest e-mail - SLP'),
        'tokens' => $tokens_digest,
      ],
      'email_digest_community' => [
        '#title' => t('Digest e-mail - Community'),
        'tokens' => $tokens_digest,
      ],
      'email_moderation_volunteering_opp_new_draft' => [
        '#title' => t('Moderation e-mail - volunteering opp new draft'),
        'tokens' => $tokens_opps,
        '#group' => 'tab_mod_emails',
      ],
      'email_moderation_volunteering_opp_needs_review' => [
        '#title' => t('Moderation e-mail - volunteering opp needs review'),
        'tokens' => $tokens_opps,
        '#group' => 'tab_mod_emails',
      ],
      'email_moderation_volunteering_opp_published' => [
        '#title' => t('Moderation e-mail - volunteering opp published'),
        'tokens' => $tokens_opps,
        '#group' => 'tab_mod_emails',
      ],
      'email_moderation_volunteering_org_new_draft' => [
        '#title' => t('Moderation e-mail - volunteering org new draft'),
        'tokens' => $tokens_orgs,
        '#group' => 'tab_mod_emails',
      ],
      'email_moderation_volunteering_org_needs_review' => [
        '#title' => t('Moderation e-mail - volunteering org needs review'),
        'tokens' => $tokens_orgs,
        '#group' => 'tab_mod_emails',
      ],
      'email_moderation_volunteering_org_published' => [
        '#title' => t('Moderation e-mail - volunteering org published'),
        'tokens' => $tokens_orgs,
        '#group' => 'tab_mod_emails',
      ],
    ];
  }
}
