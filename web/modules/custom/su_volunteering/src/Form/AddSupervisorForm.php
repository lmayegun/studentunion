<?php

namespace Drupal\su_volunteering\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class AddSupervisorForm extends AddPlacementForm {
  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added supervisor.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved supervisor.';

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = ['volunteering_org-supervisor'];


  /**
   * {@inheritdoc}
   */
  protected $fieldsToAllow = ['user_id', 'date_range', 'enabled'];

  /**
   * {@inheritdoc}
   */
  // protected $fieldsToHide = ['enabled'];

}
