<?php

namespace Drupal\su_volunteering\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class AddInterestForm extends GroupMembershipRecordForm
{
  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added expression of interest.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved expression of interest.';

  /**
   * {@inheritdoc}
   */
  protected $allowedGroups = [];

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = ['volunteering_opp-interest'];

  /**
   * {@inheritdoc}
   */
  protected $allowedMembershipRecordTypes = ['volunteering_interest'];

  /**
   * {@inheritdoc}
   */
  protected $limitToMembers = false;

  /**
   * {@inheritdoc}
   */
  protected $fieldsToAllow = ['user_id', 'date_range'];

  /**
   * {@inheritdoc}
   */
  protected $fieldsToDisable = ['date_range'];

  /**
   * {@inheritdoc}
   */
  public $customFormInfo = '';

  /**
   * {@inheritdoc}
   */
  protected $restrictToCurrentGroupParameter = true;

  // @todo prevent creating group membership record if current one already exists
  // BUT we could have a person being a captain with multiple captain types

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null)
  {
    // Set group from parameter
    $this->group = $group;

    // Custom form message
    // $this->customFormInfo = 'An expression of interest added here will remain current until the end of the academic year.';

    // Set default daterange
    $now = new DateTime();
    $yearService = \Drupal::getContainer()->get('date_year_filter.year');
    $endOfAcademicYear = $yearService->getYearForDate($now, '2021-08-01T00:00:00')->end;
    $this->defaultStartDate = $now->format("Y-m-d\TH:i:s");
    $this->defaultEndDate = null; 

    /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }
}
