<?php

namespace Drupal\su_volunteering\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class InterestRejectForm extends AddInterestForm
{
    /**
     * {@inheritdoc}
     */
    protected $messageAdded = 'Added rejected expression of interest.';

    /**
     * {@inheritdoc}
     */
    protected $messageUpdated = 'Rejected expression of interest.';

    /**
     * {@inheritdoc}
     */
    protected $fieldsToAllow = ['field_vol_rejection_reason']; //'field_vol_interest_status',

    /**
     * {@inheritdoc}
     */
    protected $fieldsToDisable = [];

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null)
    {
        /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
        $form = parent::buildForm($form, $form_state);

        // Set group from parameter
        $this->group = $group;

        $now = new DateTime();
        $form['end_date'] = [
            '#type' => 'date',
            '#title' => $this->t('End date'),
            '#default_value' => implode('-', [
                'year' => date('Y', $now->getTimestamp()),
                'month' => date('m', $now->getTimestamp()),
                'day' => date('d', $now->getTimestamp()),
            ]),
            '#required' => TRUE,
        ];

        $form['actions']['delete']['#disabled']  = true;

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        $group_membership_record = GroupMembershipRecord::load($this->entity->id());
        $group_membership_record->set('enabled', false);
        $group_membership_record->set('date_range', [
            'value' => $group_membership_record->date_range->value,
            'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, strtotime($form_state->getValue('end_date')))
        ]);
        $group_membership_record->set('field_vol_interest_status', 'Rejected');
        $group_membership_record->set('field_vol_rejection_reason', $form_state->getValue('field_vol_rejection_reason'));
        $group_membership_record->field_rejection_user->target_id = \Drupal::currentUser()->id();
        $status = $group_membership_record->save();

        switch ($status) {
            default:
                $this->messenger()->addMessage($this->t('Rejected the %label.', [
                    '%label' => $group_membership_record->type->entity->label(),
                ]));
        }
        $form_state->setRedirectUrl(Url::fromUri($_SERVER['HTTP_REFERER']));
    }
}
