<?php

namespace Drupal\su_volunteering\Form;

use DateTime;
use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;
use Drupal\su_volunteering\Form\AddPlacementForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class PlaceFromInterestForm extends AddPlacementForm {

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null) {
        // Set group from parameter
        $this->group = $group;
        $this->allowedGroups = [$group->id()];

        if ($group->hasField('field_vol_opp_leadership')) {
            if ($group->field_vol_opp_leadership->value) {
                \Drupal::messenger()->addMessage("Note that this opportunity is marked as a leadership role. This means creating a placement in the opportunity also creates a leadership role in the organisation.", 'info');
            }
        }

        $entity = $this->entity;
        $interest = $this->getInterest($entity);
        if ($interest) {
            echo 'INTER!';
            $this->allowedUsers = [$interest->getOwner()->id()];
        } else {
            echo 'NO INTER!';
        }

        /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
        $form = parent::buildForm($form, $form_state);

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state) {
        $entity = $this->entity;
        parent::save($form, $form_state);

        if ($interest = $this->getInterest($entity)) {
            \Drupal::service('su_volunteering.group_membership_record_manager')->endInterest($interest, $entity);
        }

        // @todo work out how to deal with roles assigned by subgroup
        // (i.e. if this placement is current & enabled, subgroup inheritance of roles in organisation yes;
        // if not, subgroup inheritance no)
    }

    public function getInterest($entity = null) {
        $query = \Drupal::request()->query->get('interest');
        if ($query && $interest = GroupMembershipRecord::load($query)) {
            if (!$interest->getGroup()->hasPermission('modify placement', \Drupal::currentUser())) {
                return null;
            }
            return $interest;
        }
        return null;
    }
}
