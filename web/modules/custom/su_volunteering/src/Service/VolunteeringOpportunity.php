<?php

namespace Drupal\su_volunteering\Service;

use Drupal\group\Entity\Group;

/**
 * Implement Class Controller
 */
class VolunteeringOpportunity {

  /**
   * @param $id
   * @return bool
   */
  function checkOppByID( $id ): bool {
    /** @var  Group $opp_group */
    $opp_group= Group::load($id);
    if (isset($opp_group)) {
      $opp_group_id = $opp_group->getGroupType()->id();
      if ($opp_group_id == 'volunteering_opp') {
        return true;
      }
    }
    return false;
  }

  /**
   * @param $id
   */
  public function getOppByID( $id ) {
    /** @var  Group $opp_group */
    $opp_group= Group::load($id);
    return $opp_group;
  }
}
