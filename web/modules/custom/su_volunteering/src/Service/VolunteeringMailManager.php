<?php

namespace Drupal\su_volunteering\Service;

use DateTime;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupRole;
use Drupal\user\UserInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Service\GroupMembershipRecordManager;

class VolunteeringMailManager {

  public function alterModerationEmail($moderationEmailId, GroupInterface $group, &$data) {
    $to = [];
    $subject = '';
    $body = '';
    $from_email = '';

    // Get subject:
    $config = \Drupal::config('su_volunteering.settings');
    if ($config->get('email_moderation_' . $moderationEmailId . '_subject')) {
      $subject = $config->get('email_moderation_' . $moderationEmailId . '_subject');
    } else {
      \Drupal::logger('su_volunteering')->debug('No subject for ' . $moderationEmailId);
      return FALSE;
    }

    // Get body:
    if ($config->get('email_moderation_' . $moderationEmailId)) {
      $body = $config->get('email_moderation_' . $moderationEmailId)['value'];
    } else {
      \Drupal::logger('su_volunteering')->debug('No body for ' . $moderationEmailId);
      return FALSE;
    }

    $doGroupReplacement = FALSE;

    // E-mails to leaders.
    if (in_array($moderationEmailId, ['volunteering_org_needs_review', 'volunteering_opp_needs_review', 'volunteering_opp_published', 'volunteering_org_published'])) {
      $to = [];

      $org = su_volunteering_get_org_for_opp($group);
      if ($org) {
        $users = su_volunteering_get_users_for_role($org, ['volunteering_org-leader']);
        foreach ($users as $user) {
          $to[] = $user->getEmail();
        }
        $to = $to;
      }
    }

    // E-mails to supervisors or volunteering team.
    if ($moderationEmailId == 'volunteering_org_new_draft' || $moderationEmailId == 'volunteering_opp_new_draft') {
      $to = [];

      $org = su_volunteering_get_org_for_opp($group);
      $orgType = su_volunteering_get_org_type_for_group($group);
      if ($org) {
        if ($orgType == 'SLP') {
          $users = su_volunteering_get_users_for_role($org, ['volunteering_org-supervisor']);
          foreach ($users as $user) {
            $to[] = $user->getEmail();
          }
        } else {
          $to[] = 'su.volunteering@ucl.ac.uk';
        }
      }

      $to = $to;
    }

    if (strpos($moderationEmailId, 'opp') !== FALSE) {
      $doGroupReplacement = $group;
      $doGroupParentReplacement = $org ?? NULL;
    } else {
      $doGroupReplacement = $org ?? NULL;
      $doGroupParentReplacement = $org ?? NULL;
    }

    $to = array_unique($to);

    if (count($to) > 10) {
      \Drupal::logger('su_volunteering')->notice('Did not send content moderation email because too many recipients detected. ' . $moderationEmailId . ' - ' . $group->label() . ' - ' . $group->id());
      $to = [];
    }

    if ($doGroupReplacement) {
      $subject = su_volunteering_prepare_with_tokens(
        $subject,
        $doGroupReplacement,
        $doGroupParentReplacement
      );
      $body = su_volunteering_prepare_with_tokens(
        $body,
        $doGroupReplacement,
        $doGroupParentReplacement
      );
    }


    $data['subject'] = $subject;
    $data['to'] = $to;
    $data['params']['subject'] = $subject;
    $data['params']['message'] = $body;

    // $mailManager = \Drupal::service('plugin.manager.mail');
    // $langcode = \Drupal::currentUser()->getPreferredLangcode();
    // $send = TRUE;
    // $params = [];
    // $params['from_email'] = 'volunteering@ucl.ac.uk';
    // $params['subject'] = $subject;
    // $params['body'] = $body;
    // foreach ($to as $to_email) {
    //   $result = $mailManager->mail('su_volunteering', 'su_volunteering_moderation_email', $to_email, $langcode, $params, NULL, $send);
    // }
  }

  public function sendEmailDigest() {

    $config = \Drupal::config('su_volunteering.settings');

    $organisation_ids = \Drupal::entityQuery('group')
      ->condition('status', 1)
      ->condition('type', 'volunteering_org')
      ->execute();

    $period = '-7 days';
    $last_week = strtotime($period);
    $created = '';

    \Drupal::logger('su_volunteering')->debug('Volunteering digest sending for: ' . $period);

    // Run through each organisation and get the OG leaders
    \Drupal::logger('su_volunteering')->notice('Running digest for ' . count($organisation_ids) . ' organisations.');
    foreach ($organisation_ids as $organisation_id) {
      $organisation = Group::load($organisation_id);
      $opps = su_volunteering_get_opps($organisation);
      if (count($opps) == 0) {
        // \Drupal::logger('su_volunteering')->notice($organisation->label() . ' has no opportunities.');
        continue;
      }

      // Run through each opportunity in the organisation and add details of users' interests to a list variable
      $interest_per_organisation = 0;
      $list = '';
      foreach ($opps as $opportunity) {
        if (!$opportunity->isPublished()) {
          continue;
        }

        /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
        $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
        $records = $gmrRepositoryService->get(NULL, $opportunity, GroupRole::load('volunteering_opp-interest'), NULL, new DateTime(), TRUE);

        // Check each interest for the created date
        $interest_users = [];
        $interest_per_opportunity = 0;
        foreach ($records as $record) {
          $created = strtotime($record->date_range->value);
          // If it's within the last x days add details to the body of the email
          if ($record->getUser() && $created >= $last_week) {
            $interest_per_opportunity++;
            $interest_per_organisation++;
            $interest_users[] = [
              $record->getUser(),
              $created,
            ];
          }
        }

        if ($interest_per_opportunity > 0) {
          $list .= '<strong>Opportunity: <a href="https://studentsunionucl.org/group/' . $opportunity->id() . '">' . $opportunity->label() . '</a></strong>';
          $list .= '<br><table>';
          foreach ($interest_users as $data) {
            $interest_user = $data[0];
            $created = $data[1];
            $list .= '<tr>' .
              '<td>' . $interest_user->label() . '</td>' .
              '<td>' . $interest_user->getEmail() . '</td>' .
              '<td>' . gmdate('d/m/Y', $created) . '</td>' .
              '</tr>';
          }
          $list .= '</table><br>';
        }
      }

      if ($interest_per_organisation == 0) {
        continue;
      }

      if ($organisation->field_vol_organisation_type->value == 'Community') {
        $subject = $config->get('email_digest_community_subject');
        $message = $config->get('email_digest_community')['value'];
      } else if ($organisation->field_vol_organisation_type->value == 'SLP') {
        $subject = $config->get('email_digest_slp_subject');
        $message = $config->get('email_digest_slp')['value'];
      }

      $body = su_volunteering_prepare_with_tokens($message, $organisation);
      $body = str_replace('[interest-list]', $list, $body);
      $body = str_replace('[expression-of-interest-count]', $interest_per_organisation, $body);

      $subject = su_volunteering_prepare_with_tokens($subject, $organisation);
      // $params['subject'] = t('UCL students are interested in volunteering for ' . $organisation->label());
      // t('Volunteering: UCL students interested for ' . $organisation->label());

      $mailManager = \Drupal::service('plugin.manager.mail');
      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $send = TRUE;

      // Run through each leader:
      $leaderUsers = su_volunteering_get_users_for_role($organisation, ['volunteering_org-leader']);
      $sentToLeaders = 0;
      foreach ($leaderUsers as $leader) {
        $leader_email = $leader->getEmail();
        $params = [];
        $params['from_email'] = 'volunteering@ucl.ac.uk';
        $to = $leader_email;
        $params['subject'] = $subject;
        $params['body'] = $body;
        $result = $mailManager->mail('su_volunteering', 'su_volunteering_email_digest', $to, $langcode, $params, NULL, $send);

        $sentToLeaders++;
      }

      // \Drupal::logger('su_volunteering')->debug('Volunteering digest sent to ' . $sentToLeaders . ' leaders');

      $sendToVolunteering = FALSE;
      // Send an email per organisation to volunteering:
      if ($sendToVolunteering) {
        $params = [];
        $params['from_email'] = 'volunteering@ucl.ac.uk';
        $to = 'volunteering@ucl.ac.uk';
        $params['subject'] = $subject;
        $params['body'] = $body;
        $result = $mailManager->mail('su_volunteering', 'su_volunteering_email_digest', $to, $langcode, $params, NULL, $send);
      }
    }
  }
}
