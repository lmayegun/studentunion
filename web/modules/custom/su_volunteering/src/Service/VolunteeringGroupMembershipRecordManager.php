<?php

namespace Drupal\su_volunteering\Service;

use DateTime;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupRole;
use Drupal\user\UserInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group_membership_record\Service\GroupMembershipRecordManager;

/**
 * Class VolunteeringGroupMembershipRecordManager.
 * 
 * Functions for creating, modifying and deleting placements.
 */
class VolunteeringGroupMembershipRecordManager extends GroupMembershipRecordManager
{

    function endInterest(GroupMembershipRecord $interest, GroupMembershipRecord $placement = null)
    {
        $group_membership_record = GroupMembershipRecord::load($interest->id());

        $group_membership_record->set('enabled', false);
        $group_membership_record->set('date_range', [
            'value' => $group_membership_record->date_range->value,
            'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, \Drupal::time()->getRequestTime())
        ]);
        $group_membership_record->set('field_vol_interest_status', 'Placed');
        if ($placement) {
            $group_membership_record->field_placement->target_id = $placement->id();
        }
        $status = $group_membership_record->save();
    }
}
