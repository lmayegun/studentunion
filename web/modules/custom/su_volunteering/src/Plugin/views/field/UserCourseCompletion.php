<?php

namespace Drupal\su_volunteering\Plugin\views\field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("user_course_completion")
 */
class UserCourseCompletion extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['course'] = ['default' => NULL];
    $options['course_id'] = ['default' => NULL];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['course'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Course'),
      '#default_value' => $this->options['course'] ? Course::load($this->options['course']) : NULL,
      '#target_type' => 'course',
      '#selection_settings' => [
        // 'include_anonymous' => FALSE,
      ],
      '#tags' => FALSE,
      '#required' => FALSE, // @todo make TRUE
      '#maxlength' => NULL,
    ];

    $form['course_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Course ID (if not selecting course above)'),
      '#default_value' => $this->options['course_id'] ? $this->options['course_id'] : NULL,
      '#required' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $debug = '';

    if (!$this->options['course'] && !$this->options['course_id']) {
      return new FormattableMarkup("No course found.", []);
    }

    $keys = array_keys((array) $values);
    foreach ($keys as $key) {
      if (substr($key, -4) == '_uid') {
        $uid = $values->$key;
      }
    }

    if (!isset($uid)) {
      return new FormattableMarkup("No user found.", []);
    }

    $course = NULL;
    if ($this->options['course']) {
      $course = Course::load($this->options['course']);
    }

    if (!$course) {
      $completion = su_d7_auth_get_course_completion_d7($this->options['course_id'], $uid);
      $enrollment = su_d7_auth_get_course_completion_d7($this->options['course_id'], $uid, TRUE);
      $url = '';
    }
    else {
      $url = '/course/' . $this->options['course'] . '/enrollments';
      $enrollment = $course ? $course->getEnrollment(User::load($uid)) : FALSE;
      $completion = $enrollment ? $enrollment->isComplete() : FALSE;
      if (!$enrollment || !$completion) {
        $completion = su_d7_auth_get_course_completion_d7($this->options['course_id'], $uid);
        $enrollment = su_d7_auth_get_course_completion_d7($this->options['course_id'], $uid, TRUE);
        $url = '';
      }
    }

    if (!$enrollment) {
      return new FormattableMarkup($debug . 'Not enrolled in <a href=":href" target="_blank">%title</a>', [
        ':href' => $url,
        '%title' => 'course',
      ]); // $course->getTitle()
    }
    if ($completion) {
      return new FormattableMarkup($debug . 'Completed <a href=":href" target="_blank">%title</a>', [
        ':href' => $url,
        '%title' => 'course',
      ]);
    }
    else {
      return new FormattableMarkup($debug . '<a href=":href" target="_blank">%title</a> but not complete', [
        ':href' => $url,
        '%title' => 'Enrolled',
      ]);
    }
    return FALSE;
  }

}
