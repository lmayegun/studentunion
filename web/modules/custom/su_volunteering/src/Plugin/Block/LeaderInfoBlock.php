<?php

namespace Drupal\su_volunteering\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\Entity\User;

/** *
 * @Block(
 *   id = "volunteering_info_block",
 *   admin_label = @Translation("Volunteering leader information"),
 *   category = @Translation("Volunteering"),
 * )
 */
class LeaderInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $group = \Drupal::service('su_drupal_helper')->getRouteEntity();
    if (!$group) {
      return [];
    }
    if (!in_array($group->bundle(), ['volunteering_org', 'volunteering_opp'])) {
      return [];
    }

    $account = User::load(\Drupal::currentUser()->id());
    if (!GroupAccessResult::allowedIfHasGroupPermissions($group, $account, ['edit group'])->isAllowed()) {
      return [];
    }

    $orgType = su_volunteering_get_org_type_for_group($group);

    $config = \Drupal::config('su_volunteering.settings');
    if ($orgType == 'Community') {
      $message = $config->get('block_leaders_community')['value'];
    } else if ($orgType == 'SLP') {
      $message = $config->get('block_leaders_slp')['value'];
    }

    if (isset($message)) {
      return [
        '#markup' => Markup::create($message),
        '#cache' => array(
          'contexts' => array(
            'url.path',
          )
        )
      ];
    }
  }
}
