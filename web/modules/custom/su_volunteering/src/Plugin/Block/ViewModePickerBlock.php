<?php

namespace Drupal\su_volunteering\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\Entity\User;

/** *
 * @Block(
 *   id = "volunteering_view_mode_picker",
 *   admin_label = @Translation("Volunteering view mode picker"),
 *   category = @Translation("Volunteering"),
 * )
 */
class ViewModePickerBlock extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        if (\Drupal::routeMatch()->getRouteName() != 'entity.group.canonical') {
            return [];
        }
        $group = \Drupal::service('su_drupal_helper')->getRouteEntity();
        if (!$group) {
            return [];
        }
        if (!in_array($group->bundle(), ['volunteering_org', 'volunteering_opp'])) {
            return [];
        }

        $account = User::load(\Drupal::currentUser()->id());
        if (!GroupAccessResult::allowedIfHasGroupPermissions($group, $account, ['edit group'])->isAllowed()) {
            return [];
        }

        $text = '';
        $views = [
            'Group leader or admin' => 'group_leader_admin',
            'Expressed interest / volunteer' => 'group_member',
            'Logged in user but not signed up' => 'group_authenticated',
            'Logged out user' => 'default',
        ];

        $request = \Drupal::request();
        $current_mode = $request->query->get('mode') ?? 'group_leader_admin';

        $options = [];
        foreach ($views as $label => $id) {
            $options[] = ($id == $current_mode ? $label : '<a href="?mode=' . $id . '">' . $label . '</a>');
        }
        $text .= '<p><b>Test a view mode:</b> ' . implode(' | ', $options) . '</p>';

        if (isset($text)) {
            return [
                '#markup' => Markup::create('<div class="admin-box"><h4>Leader/admin tools</h4>' . $text . '</div>'),
            ];
        }
    }

    public function getCacheContexts()
    {
        return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
    }
}
