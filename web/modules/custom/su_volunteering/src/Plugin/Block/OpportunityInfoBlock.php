<?php

namespace Drupal\su_volunteering\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\Entity\User;

/**
 * Show a message (customisable in settings) for org leaders.
 *
 * @Block(
 *   id = "volunteering_opp_message_block",
 *   admin_label = @Translation("Volunteering opportunity message"),
 *   category = @Translation("Volunteering"),
 * )
 */
class OpportunityInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::routeMatch()->getRouteName() != 'entity.group.canonical') {
      return [];
    }

    $group = \Drupal::service('su_drupal_helper')->getRouteEntity();
    if (!$group) {
      return [];
    }
    if (!in_array($group->bundle(), ['volunteering_opp'])) {
      return [];
    }

    $config = \Drupal::config('su_volunteering.settings');

    /** @var \Drupal\subgroup\Entity\SubgroupHandlerInterface $handler */
    $subgroupHandler = \Drupal::entityTypeManager()->getHandler('group', 'subgroup');
    $org = $subgroupHandler->isLeaf($group) ? $subgroupHandler->getParent($group) : null;
    if (!$org) {
      return [];
    }

    if ($org->hasField('field_vol_org_message_for_opps') && $org->get('field_vol_org_message_for_opps')->getString()) {
      $message = $org->field_vol_org_message_for_opps->value;
    } elseif ($org->field_vol_organisation_type->value == 'Community') {
      $message = $config->get('opp_message_community')['value'];
    } else if ($org->field_vol_organisation_type->value == 'SLP') {
      $message = $config->get('opp_message_slp')['value'];
    }
    if ($message) {
      return [
        '#markup' => Markup::create('<div class="volunteering_opp-message">' . $message . '</div>'),
      ];
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route.group'));
  }
}
