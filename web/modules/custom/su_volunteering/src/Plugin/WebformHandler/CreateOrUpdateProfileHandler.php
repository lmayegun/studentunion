<?php

namespace Drupal\su_volunteering\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileType;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Create a new node entity from a webform submission.
 *
 * @WebformHandler(
 *   id = "create_or_update_profile",
 *   label = @Translation("Create or update a Profile entity for the user"),
 *   category = @Translation("Entity Creation"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */

class CreateOrUpdateProfileHandler extends WebformHandlerBase
{

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
            'profile_type' => '',
            'entity_values' => [],
            'skip_empty_webform_fields' => true
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $options = [];
        $list = ProfileType::loadMultiple();
        foreach ($list as $profileType) {
            $options[$profileType->id()] = $profileType->label();
        }
        $form['profile_type'] = [
            '#type' => 'select',
            '#title' => $this->t('Profile type'),
            '#options' => $options,
            '#required' => TRUE,
            '#default_value' => $this->configuration['profile_type']
        ];
        $form['skip_empty_webform_fields'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Don\'t update profile fields if webform field empty'),
            '#default_value' => $this->configuration['skip_empty_webform_fields']
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);

        $this->applyFormStateToConfiguration($form_state);
        $this->configuration['profile_type'] = $form_state->getValue('profile_type');
        $this->configuration['skip_empty_webform_fields'] = $form_state->getValue('skip_empty_webform_fields');
    }

    // Function to be fired after submitting the Webform.
    public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE)
    {
        // Get an array of the values from the submission.
        $values = $webform_submission->getData();

        $user = $webform_submission->getOwner();
        $profile_type = $this->configuration['profile_type'];

        if (!$profile_type) {
            return;
        }

        $profile = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser($user, $profile_type);

        if (!$profile) {
            $profile = Profile::create([
                'type' => $profile_type,
                'uid'  => $user->id(),
            ]);
            $profile->save();
        }
        foreach ($values as $field => $value) {
            if ($this->configuration['skip_empty_webform_fields'] && (!$value)) {
                continue;
            }
            if ($profile->hasField($field)) {
                $profile->set($field, $value);
            } else if ($profile->hasField('field_' . $field)) {
                $profile->set($field, $value);
            }
        }
        $profile->save();
    }
}
