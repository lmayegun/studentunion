<?php

namespace Drupal\group_membership_record\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides group membership record route controllers.
 */
class GroupMembershipRecordController extends ControllerBase
{

    /**
     * The current user.
     *
     * @var \Drupal\Core\Session\AccountInterface
     */
    protected $currentUser;

    /**
     * The entity form builder.
     *
     * @var \Drupal\Core\Entity\EntityFormBuilderInterface
     */
    protected $entityFormBuilder;

    /**
     * Constructs a new GroupMembershipController.
     *
     * @param \Drupal\Core\Session\AccountInterface $current_user
     *   The current user.
     * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
     *   The entity form builder.
     */
    public function __construct(AccountInterface $current_user, EntityFormBuilderInterface $entity_form_builder)
    {
        $this->currentUser = $current_user;
        $this->entityFormBuilder = $entity_form_builder;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('current_user'),
            $container->get('entity.form_builder')
        );
    }
}
