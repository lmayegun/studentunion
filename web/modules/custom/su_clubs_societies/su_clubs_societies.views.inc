<?php

/**
 * Implements hook_views_data_alter().
 */
function su_clubs_societies_views_data_alter(array &$data) {
  $data['groups']['su_clubs_socs_members_count'] = [
    'title' => t('Club/Society membership count'),
    'help' => t(''),
    'field' => [
      'id' => 'su_clubs_socs_members_count',
      'click sortable' => TRUE,
    ],
  ];
}
