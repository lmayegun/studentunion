<?php

namespace Drupal\su_clubs_societies\Plugin\GroupMembershipRecord\EnabledDeterminer;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Plugin\GroupMembershipRecord\EnabledDeterminer\DefaultEnabledDeterminer;
use Drupal\kst\KstPluginBase;

/**
 * Provides a plugin to determine whether a committee member is enabled.
 *
 * @EnabledDeterminerPlugin(
 *   id = "enabled_determiner_committee_member",
 *   description = @Translation("Determines whether record is enabled based on
 *   various club/soc specific things."), group_membership_record_types = {
 *     "club_society_committee",
 *   }
 * )
 */
class CommitteeMemberEnabledDeterminer extends DefaultEnabledDeterminer {

  public $messageForFormEnabledField = 'This is calculated based on whether the user has completed required courses, the club/society is validly registered for this year, and whether they are otherwise blocked or granted an access override.';

  // @todo set up the plugin to allow for multiple tests, which determinedEnabledValue then uses,
  // in a standard way so we can present the tests as list like eligibility

  /**
   * {@inheritdoc}
   */
  public function determineEnabledValue(GroupMembershipRecord $record) {
    $enabled = TRUE;

    $cid = __METHOD__ . $record->id();
    if ($item = Drupal::cache()->get($cid)) {
      return $item->data;
    }

    if (!$this->testCurrent($record, FALSE)) {
      // echo $record->id() . ': testCurrent' . PHP_EOL;
      $enabled = FALSE;
    }
    elseif (!$this->testNotBlocked($record, FALSE)) {
      // echo $record->id() . ': testNotBlocked' . PHP_EOL;
      $enabled = FALSE;
    }
    elseif (!$this->testResigned($record, FALSE)) {
      // echo $record->id() . ': testResigned' . PHP_EOL;
      $enabled = FALSE;
    }
    elseif ($this->testOveridden($record, FALSE)) {
      // echo $record->id() . ': testOveridden' . PHP_EOL;
      $enabled = TRUE;
    }
    elseif (!$this->testRoleCanEdit($record, FALSE)) {
      $enabled = FALSE;
    }
    else {
      // Required training
      $required_training_complete = $this->testRequiredTraining($record, FALSE);
      if (!$required_training_complete) {
        $enabled = FALSE;
      }

      if ($enabled) {
        // Club/Soc is registered this year
        $club_soc_registered = $this->testRequiredClubSocRegistration($record, FALSE);
        if (!$club_soc_registered) {
          $enabled = FALSE;
        }
      }
    }

    Drupal::cache()
      ->set($cid, $enabled, Cache::PERMANENT, $this->getCacheTags($record));

    return $enabled;
  }

  private function testCurrent(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Dates are current';
    $explanations = [['Record date range is current', TRUE, $testLabel]];

    $pass = $record->isCurrent();

    if (!$pass) {
      $explanations = [['Record date range not current', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }

  private function testNotBlocked(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Access not blocked by administrator';
    $explanations = [['Access not blocked by administrator', TRUE, $testLabel]];

    $pass = $record->get('field_access_blocked')->value == 0;

    if (!$pass) {
      $explanations = [['Access blocked by administrator', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }

  private function testResigned(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Not resigned';
    $explanations = [['Not resigned', TRUE, $testLabel]];

    $pass = $record->get('field_club_society_resigned')->value == 0;

    if (!$pass) {
      $explanations = [['Resigned', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }

  private function testOveridden(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Access checks overridden by administrator';
    // This is null success state as it overrides anything else
    $explanations = [
      [
        'Access checks overridden by administrator',
        NULL,
        $testLabel,
      ],
    ];

    $pass = $record->get('field_access_override')->value == 1;

    if (!$pass) {
      $explanations = [
        [
          'Access checks not overidden by administrator',
          NULL,
          $testLabel,
        ],
      ];
    }

    return $returnExplanations ? $explanations : $pass;
  }

  public function testRoleCanEdit(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Role allows for editing';
    $enabled = TRUE;
    $explanations = [];

    $role = $record->getRole();
    if (!$role) {
      $enabled = FALSE;
      $explanations[] = ['Record has no role assigned', FALSE];
      return $returnExplanations ? $explanations : $enabled;
    }
    $enabled = $role->hasPermission('edit group');
    if ($enabled) {
      $explanations[] = [
        $role->label() . ' role allows for website access',
        TRUE,
        $testLabel,
      ];
    }
    else {
      $explanations[] = [
        $role->label() . ' role does not allow for website access',
        FALSE,
        $testLabel,
      ];
    }
    return $returnExplanations ? $explanations : $enabled;
  }

  private function testRequiredTraining(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Training requirements met';
    $enabled = TRUE;
    $explanations = [];

    // Get courses for year
    $courses = $this->getRequiredCoursesForRole($record);

    foreach ($courses as $course) {
      // D8 completion
      $enrollment = $course->getEnrollment($record->getUser());
      if (!$enrollment) {
        $enrollment = $course->enroll($record->getUser());
      }
      $completed = $enrollment->isComplete();

      if (!$completed) {
        $completed = su_d7_auth_get_course_completion_d7($course->id(), $record->getUser()
          ->id());
      }

      if ($completed) {
        $explanations[] = [
          'Completed course ' . $course->getTitle(),
          TRUE,
          $testLabel,
        ];
      }
      else {
        $enabled = FALSE;
        $explanations[] = [
          'Not completed course ' . $course->getTitle(),
          FALSE,
          $testLabel,
        ];
      }
    }

    // $required_training_entities = $record->get('field_training_requirements');
    // if (count($required_training_entities) == 0) {
    //     $pass = TRUE;
    // } else {
    //     foreach ($required_training_entities as $training) {
    //         if ($training->type == 'course') {
    //             // check if course is complete
    //         } else if ($training->type == 'quiz') {
    //             // check if quiz has been passed
    //         }
    //     }
    // }

    if (count($explanations) == 0) {
      $explanations[] = ['No required courses found for this role.', TRUE];
    }

    return $returnExplanations ? $explanations : $enabled;
  }

  /**
   * We expect some courses named after the roles, and possibly the academic
   * year
   *
   * @todo optional - add setting on role as to what courses are required for
   *   what year, or on each course to assign to roles
   **/
  public function getRequiredCoursesForRole(GroupMembershipRecord $record) {
    return su_clubs_societies_get_required_courses_for_role($record);
  }

  private function testRequiredClubSocRegistration(GroupMembershipRecord $record, $returnExplanations) {
    $pass = FALSE;
    $yearObject = $this->getYearObjectForRecord($record);

    $testLabel = 'Registered for ' . $yearObject->name;

    $explanations = [
      [
        'Club/society not registered for ' . $yearObject->name,
        FALSE,
        $testLabel,
      ],
    ];

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = Drupal::service('su_clubs_societies.service');
    $registered = $clubSocService->registeredForYear($record->getGroup(), $yearObject);

    if ($registered) {
      $explanations = [
        ['Club/society registered for ' . $yearObject->name, TRUE, $testLabel],
      ];
    }

    return $returnExplanations ? $explanations : $registered;
  }

  public function getYearObjectForRecord(GroupMembershipRecord $record) {
    return su_clubs_societies_get_year_for_record($record);
  }

  public function getCacheTags($record) {
    $tags = [];
    $tags[] = 'course:list';
    if ($record->getRole()) {
      $tags[] = 'group_role:' . $record->getRole()->id();
    }
    $tags[] = 'webform_submission:list';
    return array_merge($tags, $record->getCacheTags());
  }

  public function getEnabledExplanation(GroupMembershipRecord $record) {
    $cid = __METHOD__ . $record->id();
    if ($item = Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $explanations = array_merge(
      $this->testRoleCanEdit($record, TRUE),
      $this->testRequiredTraining($record, TRUE),
      $this->testRequiredClubSocRegistration($record, TRUE),
      $this->testCurrent($record, TRUE),
      $this->testOveridden($record, TRUE),
      $this->testNotBlocked($record, TRUE),
      $this->testResigned($record, TRUE)
    );

    Drupal::cache()
      ->set($cid, $explanations, Cache::PERMANENT, $this->getCacheTags($record));

    return $explanations;
  }

}
