<?php

namespace Drupal\su_clubs_societies\Plugin\GroupMembershipRecord\EnabledDeterminer;

use Drupal\Core\Cache\Cache;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;

/**
 * Provides a plugin to determine whether a committee member is enabled.
 *
 * @EnabledDeterminerPlugin(
 *   id = "enabled_determiner_clubsoc_member",
 *   description = @Translation("Determines whether record is enabled based on whether student is current student and Union member."),
 *   group_membership_record_types = {
 *     "club_society_member",
 *   }
 * )
 */
class ClubSocMemberEnabledDeterminer extends CommitteeMemberEnabledDeterminer {
  public $messageForFormEnabledField = 'This is calculated based on whether the user is a current student and a Union member.';

  // @todo set up the plugin to allow for multiple tests, which determinedEnabledValue then uses,
  // in a standard way so we can present the tests as list like eligibility

  public function getCacheTags($record) {
    $tags = [];
    if ($record->getRole()) {
      $tags[] = 'group_role:' . $record->getRole()->id();
    }
    $tags[] = 'user:' . $record->getUser()->id();
    return array_merge($tags, $record->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function determineEnabledValue(GroupMembershipRecord $record) {
    $enabled = TRUE;

    $cid = __METHOD__ . $record->id();
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $current = $this->testCurrent($record, FALSE);
    $normalMember = $this->testUnionMember($record, FALSE) && $this->testCurrentStudent($record, FALSE);
    $assocMember = $this->testAssociateVisiting($record, FALSE);
    $member = $normalMember || $assocMember;
    $enabled = $current && $member;

    \Drupal::cache()->set($cid, $enabled, Cache::PERMANENT, $this->getCacheTags($record));

    return $enabled;
  }

  public function getEnabledExplanation(GroupMembershipRecord $record) {
    $cid = __METHOD__ . $record->id();
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $explanations = array_merge(
      $this->testCurrentStudent($record, TRUE),
      $this->testUnionMember($record, TRUE),
      $this->testAssociateVisiting($record, TRUE),
    );

    \Drupal::cache()->set($cid, $explanations, Cache::PERMANENT, $this->getCacheTags($record));

    return $explanations;
  }

  public function getYearObjectForRecord(GroupMembershipRecord $record) {
    return su_clubs_societies_get_year_for_record($record);
  }

  private function testCurrent(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Dates are current';
    $explanations = [['Record date range is current', TRUE, $testLabel]];

    $pass = $record->isCurrent();

    if (!$pass) {
      $explanations = [['Record date range not current', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }

  private function testCurrentStudent(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Is a current student';
    $explanations = [['User is a current student', TRUE, $testLabel]];

    $pass = $record->getUser()->hasRole('student');

    if (!$pass) {
      $explanations = [['User is not current student', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }


  private function testUnionMember(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Is an opted-in Union member';
    $explanations = [['User is an opted-in Union member', TRUE, $testLabel]];

    $studentService = \Drupal::service('su_students.student_services');
    $studentProfile = $studentService->getStudentProfile($record->getOwner(), FALSE);
    $pass = $studentService::isStudentCurrentMember($studentProfile);

    if (!$pass) {
      $explanations = [['User is not an opted-in Union member', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }

  private function testAssociateVisiting(GroupMembershipRecord $record, $returnExplanations) {
    $testLabel = 'Is an Associate/Visiting member';
    $explanations = [['User is an Associate/Visiting member', TRUE, $testLabel]];

    $pass = $record->getUser()->hasRole('associate_visiting');

    if (!$pass) {
      $explanations = [['User is not an Associate/Visiting member', FALSE, $testLabel]];
    }

    return $returnExplanations ? $explanations : $pass;
  }
}
