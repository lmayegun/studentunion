<?php

namespace Drupal\su_clubs_societies\Plugin\Commerce\ProductRestriction;

use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginBase;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\webform\Element\Webform;

/**
 * Provides product restriction by user role.
 *
 * @ProductRestrictionPlugin(
 *   id = "club_society_can_join_rules",
 *   label = @Translation("Restrict with club/society membership joining rules [for membership products]"),
 *   description = @Translation("Requires: membership of Union, no existing membership of the club/soc, and over 18 or approved exception. Do not use other restrictions if using this one."),
 *   category = @Translation("Clubs/Societies"),
 *   entity_type = "commerce_product",
 *   entity_bundles = {"club_society_membership"},
 *   weight = -10
 * )
 */
class ClubSocietyMembershipCanJoinRules extends ProductRestrictionPluginBase implements ProductRestrictionPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'group_id' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
  }

  public function getEvaluations(EntityInterface $entity) {
    $this->assertEntity($entity);
    $account = User::load(\Drupal::currentUser()->id());

    $evaluations = [];

    $evaluations['isUnionMember'] = in_array('member', $account->getRoles()) || in_array('associate_visiting', $account->getRoles());
    $evaluations['isCurrentStudentOrAssociateVisiting'] = in_array('student', $account->getRoles()) || in_array('associate_visiting', $account->getRoles());

    $group = $this->getClubSociety($entity);
    if (!$group) {
      // @todo throw an error
      return NULL;
    }

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
    $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');
    $evaluations['alreadyMember'] = $clubSocMembershipsService->userIsCurrentMember($account, $group);

    $studentService = \Drupal::service('su_students.student_services');

    $evaluations['haveStudentDataForUser'] = $studentService->haveStudentDataForUser($account) || in_array('associate_visiting', $account->getRoles());

    $evaluations['over18orExceptionApproved'] = $studentService->isStudentOver18($account) || in_array('associate_visiting', $account->getRoles());

    // Check if there's a webform submission for under 18s that's been approved that allows for purchasing this club/soc
    if (!$evaluations['over18orExceptionApproved']) {
      /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
      $clubSocService = \Drupal::service('su_clubs_societies.service');
      $evaluations['over18orExceptionApproved'] = $clubSocService->userApprovedForUnder18s($account, $group);
    }

    return $evaluations;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $evaluations = $this->getEvaluations($entity);

    // $logger = \Drupal::logger('su_clubs_societies');
    // $member = $evaluations['alreadyMember'] ? 'TRUE' : 'FALSE';
    // $logger->info('evaluate alreadyMember: ' . $member);

    if ($evaluations) {
      return $evaluations['isUnionMember'] && $evaluations['isCurrentStudentOrAssociateVisiting'] && !$evaluations['alreadyMember'] && $evaluations['haveStudentDataForUser'] && $evaluations['over18orExceptionApproved'];
    }
    return FALSE;
  }

  public function getClubSociety($product_or_variation) {
    if ($product_or_variation->getEntityTypeId() == 'commerce_product_variation') {
      $product = $product_or_variation->getProduct();
    } else {
      $product = $product_or_variation;
    }
    return $product->field_club_society->entity ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function accessErrorMessage($product_or_variation) {
    $evaluations = $this->getEvaluations($product_or_variation);

    // $logger = \Drupal::logger('su_clubs_societies');
    // $logger->info('accessErrorMessage alreadyMember: ' . $evaluations['alreadyMember']);

    if (!$evaluations) {
      return new TranslatableMarkup(
        'You cannot purchase this product because it is not configured correctly.',
        []
      );
    }

    $message = [];

    $account = User::load(\Drupal::currentUser()->id());

    $link = Url::fromRoute('user.login', [], [
      'absolute' => TRUE,
      'query' => ['destination' => \Drupal::request()->getRequestUri()]
    ])->toString();
    $login = \Drupal::currentUser()->isAnonymous() ? '<a href="' . $link . '">logged in</a>' : 'logged in';

    if (!$evaluations['isUnionMember']) {
      if (\Drupal::currentUser()->isAnonymous()) {
        $message[] = 'a member of Students\' Union UCL ' . $login . ' with your UCL account - current students can opt in to membership for free <a href="/optin">here</a>, and if you are not a student but would like to join societies and be a member of the Union, please become an <a href="/visiting-associate-membership">Associate or Visiting Member</a>';
      } elseif (!$account->hasRole('ucl_authenticated')) {
        $message[] = 'a member of Students\' Union UCL logged in with your UCL account - not a local account. <a href="/user/logout">Log out and lock back in with a UCL account.</a>';
      } else {
        $message[] = 'a member of Students\' Union UCL - current students can opt in to membership for free <a href="/optin">here</a>, and if you are not a student but would like to join societies and be a member of the Union, please become an <a href="/visiting-associate-membership">Associate or Visiting Member</a>';
      }
    }

    if (!$evaluations['isCurrentStudentOrAssociateVisiting']) {
      $message[] = 'either a current student or associate/visiting member';
    }

    if (!$evaluations['haveStudentDataForUser']) {
      $message[] = 'opted in to membership, ' . $login . ' with your UCL account, and the Union must have recieved your data from UCL to confirm your age (this may take some time over the enrolment period)';
    } elseif (!$evaluations['over18orExceptionApproved']) {
      $message[] = 'over the age of 18 or have a <a href="/forms/request-under-18s-membership-for-club-or-society">special approval to join</a>';
    }

    if ($evaluations['alreadyMember']) {
      return new TranslatableMarkup(
        'You are already a current member.',
        []
      );
    }

    if (count($message) > 1) {
      return new TranslatableMarkup(
        'To purchase this product, you need to be ' . $message[0] . '.',
        []
      );
    } else {
      return new TranslatableMarkup(
        'To purchase this product, you need to be <ul><li>' . implode('</li><li>', $message) . '</li></ul>',
        []
      );
    }
  }
}
