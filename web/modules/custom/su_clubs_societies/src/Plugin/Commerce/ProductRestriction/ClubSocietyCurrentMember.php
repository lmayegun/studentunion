<?php

namespace Drupal\su_clubs_societies\Plugin\Commerce\ProductRestriction;

use DateTime;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginBase;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Provides product restriction by user role.
 *
 * @ProductRestrictionPlugin(
 *   id = "club_society_current_member",
 *   label = @Translation("Restrict to current members of clubs/societies"),
 *   category = @Translation("Clubs/Societies"),
 *   entity_type = "commerce_product",
 *   entity_bundles = {},
 *   weight = -10
 * )
 */
class ClubSocietyCurrentMember extends ProductRestrictionPluginBase implements ProductRestrictionPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'group_ids' => null,
      'paid' => null
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['group_ids'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Club/society',
      '#target_type' => 'group',
      '#default_value' => $this->configuration['group_ids'] ? Group::loadMultiple(array_column($this->configuration['group_ids'], 'target_id')) : null,
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['club_society'],
      ],
      '#tags' => TRUE,
      '#required' => TRUE,
    ];

    $form['paid'] = [
      '#type' => 'checkbox',
      '#title' => 'Check this box to require member to be a paid member (membership product was not free)',
      '#default_value' => $this->configuration['paid'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['group_ids'] = $values['group_ids'];
    $this->configuration['paid'] = $values['paid'];
  }

  public function getGroups() {
    return $this->configuration['group_ids'] ? Group::loadMultiple(array_column($this->configuration['group_ids'], 'target_id')) : null;
  }

  public function requiresPaidMembership() {
    return $this->configuration['paid'];
  }

  public function getEvaluations(EntityInterface $entity) {
    $this->assertEntity($entity);
    $account = User::load(\Drupal::currentUser()->id());

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $groups = $this->getGroups();
    if (!$groups) {
      // @todo throw an error
      return NULL;
    }
    $role = GroupRole::load('club_society-member');
    foreach ($groups as $group) {
      $records = $gmrRepositoryService->get($account, $group, $role, NULL, new DateTime(), TRUE);
      foreach ($records as $record) {
        if ($this->requiresPaidMembership()) {
          /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
          $clubSocService = \Drupal::service('su_clubs_societies.service');
          $alreadyPurchasedVariation = $clubSocService->getVariationFromMembershipRecord($record);
          if ($alreadyPurchasedVariation && $alreadyPurchasedVariation->getPrice()->getNumber() > 0) {
            return TRUE;
          }
        } else {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    return $this->getEvaluations($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function accessErrorMessage($product_or_variation) {
    $evaluation = $this->getEvaluations($product_or_variation);

    if (!$evaluation) {
      $groups = $this->getGroups();
      $titles = [];
      foreach ($groups as $group) {
        $titles[] = $group->label();
      }
      if ($this->requiresPaidMembership()) {
        return new TranslatableMarkup(
          'You cannot purchase this product because you do not have a current paid (not free) membership of @titles.' . (count($titles) == 1 ? ' You may be able to upgrade your membership to a paid membership on <a href="@url">the club/society page</a>.' : ''),
          [
            '@titles' => static::naturalLanguageJoin($titles),
            '@url' => $group->toUrl()->toString()
          ]
        );
      } else {
        return new TranslatableMarkup(
          'You cannot purchase this product because you are not a current member of @titles.' . (count($titles) == 1 ? ' <a href="@url">Join here</a>.' : ''),
          [
            '@titles' => static::naturalLanguageJoin($titles),
            '@url' => $group->toUrl()->toString(),
          ]
        );
      }
    }
  }
}
