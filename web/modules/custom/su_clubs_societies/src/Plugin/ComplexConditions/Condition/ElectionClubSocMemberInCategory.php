<?php

namespace Drupal\su_clubs_societies\Plugin\ComplexConditions\Condition;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType;
use Drupal\taxonomy\Entity\Term;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_clubsoc_member_category",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Club/society member in category"),
 *   display_label = @Translation("Club/society member in category"),
 *   category = @Translation("Group memberships"),
 *   weight = 0,
 * )
 */
class ElectionClubSocMemberInCategory extends ElectionClubSocMember {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'categories' => [],
        'categories_any_or_all' => 'any',
      ] + parent::defaultConfiguration();
  }

  public function getMessage($params) {
    $message = 'User must have be been a member of a club or society in ';
    if ($params['@categories_count'] > 1) {
      $message .= $this->configuration['categories_any_or_all'] . ' of the required categories (@categories)';
    }
    else {
      $message .= 'the category "@categories"';
    }
    $message .= ' for at least 28 days.';
    return $message;
  }

  public function getParams() {
    $this->configuration['group_roles'] = ['club_society-member'];

    $params = parent::getParams();

    $termIds = array_column($this->configuration['categories'], 'target_id');
    $terms = Term::loadMultiple($termIds);
    $params['@categories'] = [];
    foreach ($terms as $term) {
      $params['@categories'][$term->id()] = $term->label();
    }
    asort($params['@categories']);
    $params['@categories_count'] = count($params['@categories']);
    $params['@categories'] = implode(", ", $params['@categories']);
    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['groups']['#access'] = FALSE;
    $form['groups_any_or_all']['#access'] = FALSE;

    $groupOptions = [];
    $form['categories'] = [
      '#title' => $this->t('Category/ies'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => ['club_and_society_categories'],
      ],
      '#tags' => TRUE,
      '#default_value' => $this->configuration['categories'] ? Term::loadMultiple(array_column($this->configuration['categories'], 'target_id')) : NULL,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];
    $form['categories_any_or_all'] = [
      '#type' => 'select',
      '#title' => $this->t('In any or all categories'),
      '#options' => [
        'any' => 'Any',
        'all' => 'All',
      ],
      '#default_value' => $this->configuration['categories_any_or_all'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['categories'] = $values['categories'];
    $this->configuration['categories_any_or_all'] = $values['categories_any_or_all'];
  }

  public function getGroupIds() {
    $termIds = array_column($this->configuration['categories'], 'target_id');
    if (count($termIds) == 0) {
      return [];
    }

    $groups = Drupal::entityQuery('group')
      ->condition('type', 'club_society')
      ->condition('field_club_category', $termIds, 'IN')
      ->execute();
    return $groups;
  }

  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->configuration['groups_any_or_all'] = 'any';
    $requirements = parent::evaluateRequirements($entity, $account, $parameters);

    foreach ($requirements as $id => $requirement) {
      if ($requirement->isFailed()) {
        $category_ids = array_column($this->configuration['categories'], 'target_id');
        $exemption = static::checkCategoryExemption($account, $category_ids);
        $requirement->setPass($exemption);
      }
    }

    return $requirements;
  }

  public static function checkCategoryExemption(AccountInterface $account, array $category_ids) {
    if (empty(ElectionConditionExemptionType::load('club_society_category'))) {
      return FALSE;
    }

    $countExemptions = Drupal::entityQuery('election_condition_exemption')
      ->condition('bundle', 'club_society_category')
      ->condition('status', 1)
      ->condition('field_exempt_user', [$account->id()], 'IN')
      ->condition('field_term', $category_ids, 'IN')
      ->count()
      ->execute();

    return $countExemptions > 0;
  }

  public function generateRandomConditionConfig() {
    $vid = 'club_and_society_categories';
    $terms = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($vid);
    $termsLoaded = [];
    foreach ($terms as $term) {
      $termsLoaded[$term->tid] = $term->tid;
    }

    $term_id = array_rand($termsLoaded, 1);

    $config = parent::generateRandomConditionConfig();
    $config['target_plugin_configuration'] = array_merge($config['target_plugin_configuration'], [
      'categories' => [
        [
          'target_id' => $term_id,
        ]
      ],
      "categories_any_or_all" => "any",
    ]);
    return $config;
  }

}
