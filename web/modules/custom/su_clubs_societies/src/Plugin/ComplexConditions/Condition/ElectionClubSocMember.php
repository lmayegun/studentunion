<?php

namespace Drupal\su_clubs_societies\Plugin\ComplexConditions\Condition;

use DateTime;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\ElectionGroupRole;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_clubsoc_member",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Club/society member"),
 *   display_label = @Translation("Club/society member"),
 *   category = @Translation("Group memberships"),
 *   weight = 0,
 * )
 */
class ElectionClubSocMember extends ElectionGroupRole {

  const MEMBER_SINCE = '-28 days';
  const TARGET_BUNDLES = ['club_society'];

  /**
   * {@inheritdoc}
   */
  public function getMessage($params) {

    if ($params['@groups_count'] > 1) {
      $message = 'User must have been a member of ' . $this->configuration['groups_any_or_all'] . ' of the following clubs/societies for at least 28 days: @groups';
    } else {
      $message = 'User must have been a member of the club/society "@groups" for at least 28 days.';
    }
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['groups']['#title'] = t('Clubs/Societies (can select multiple)');
    $form['groups_any_or_all']['#title'] = t('Require any or all of the clubs/societies selected above');

    $form['group_roles']['#access'] = FALSE;
    $form['group_roles_any_or_all']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['group_roles'] = ['club_society-member'];
    $this->configuration['group_roles_any_or_all'] = 'any';
  }

  public function buildRequirements($pass) {
    $requirements = parent::buildRequirements($pass);
    foreach ($requirements as $key => $id) {
      $requirements[$key]->setDescription(t('Contact <a href="mailto:su.activites-reception@ucl.ac.uk">Activities Reception</a> for more information. Note you must also be a Union member to be counted as a current member of a club/society.'));
    }
    return $requirements;
  }

  public static function checkMembershipTimeExemption(AccountInterface $account, GroupInterface $clubSociety) {
    if (empty(ElectionConditionExemptionType::load('club_society_28_day_rule'))) {
      return FALSE;
    }

    $exemptions = \Drupal::entityQuery('election_condition_exemption')
      ->condition('bundle', 'club_society_28_day_rule')
      ->condition('status', 1)
      ->condition('field_exempt_user', [$account->id()], 'IN')
      ->condition('field_club_society', [$clubSociety->id()], 'IN')
      ->condition('changed', strtotime(static::MEMBER_SINCE), '>=')
      ->execute();

    if (count($exemptions) == 0) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  public function getMemberships($account) {
    // Get user's group memberships
    $membershipService = \Drupal::service('group.membership_loader');
    $memberships = $membershipService->loadByUser($account);
    $final_memberships = [];

    $groupIds = $this->getGroupIds();

    foreach ($memberships as $membership) {
      $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
      $records = $gmrRepositoryService->get($account, NULL, GroupRole::load('club_society-member'), NULL, new DateTime());
      foreach ($records as $record) {
        $group = $record->getGroup();
        if (!in_array($group->id(), $groupIds)) {
          continue;
        }

        $inTimePeriod = $record->getCurrency(new DrupalDateTime(static::MEMBER_SINCE)) == 'current';
        if ($inTimePeriod) {
          $final_memberships[] = $membership;
          continue;
        }

        $exemption = static::checkMembershipTimeExemption($account, $group);
        if ($exemption) {
          $final_memberships[] = $membership;
          continue;
        }
      }
    }
    return $final_memberships;
  }

  public function generateRandomConditionConfig() {
    $query = \Drupal::database()->select('groups_field_data', 'g')
      ->fields('g', ['id'])
      ->condition('type', 'club_society')
      ->condition('status', TRUE)
      ->range(0, 100)
      ->orderRandom();
    $results = $query->execute()->fetchCol();
    $club_society_id = reset($results);

    if (!$club_society_id) {
      return;
    }

    $config = parent::generateRandomConditionConfig();
    $config['target_plugin_configuration'] = array_merge($config['target_plugin_configuration'], [
      'groups' => [
        [
          'target_id' => $club_society_id,
        ]
      ],
      "groups_any_or_all" => "any",
      "group_roles" =>  [
        "club_society-member",
      ],
      "group_roles_any_or_all" => "any",
    ]);
    return $config;
  }
}
