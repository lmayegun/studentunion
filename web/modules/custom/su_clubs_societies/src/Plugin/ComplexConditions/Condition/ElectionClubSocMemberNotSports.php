<?php

namespace Drupal\su_clubs_societies\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\ElectionGroupRole;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\group\Entity\Group;
use Drupal\taxonomy\Entity\Term;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_clubsoc_member_category_not_sports",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Club/society member in non-Sport society"),
 *   display_label = @Translation("Club/society member in non-Sport society"),
 *   category = @Translation("Group memberships"),
 *   weight = 0,
 * )
 */
class ElectionClubSocMemberNotSports extends ElectionClubSocMemberInCategory {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['categories']['#access'] = FALSE;
    $form['categories_any_or_all']['#access'] = FALSE;

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['categories'] = [];

    $term_ids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'club_and_society_categories')
      ->execute();
    $categoryTerms = Term::loadMultiple($term_ids);
    foreach ($categoryTerms as $term) {
      if ($term->label() != 'Sport' && $term->label() != 'Sports') {
        $this->configuration['categories'][] = ['target_id' => $term->id()];
      }
    }
    $this->configuration['categories_any_or_all'] = 'any';
  }
}
