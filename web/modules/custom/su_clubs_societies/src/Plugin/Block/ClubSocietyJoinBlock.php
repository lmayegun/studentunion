<?php

namespace Drupal\su_clubs_societies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\Entity\User;

/** *
 * @Block(
 *   id = "club_soc_join_block",
 *   admin_label = @Translation("Club/society join block"),
 *   category = @Translation("Clubs/Societies"),
 * )
 */
class ClubSocietyJoinBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $group = \Drupal::service('su_drupal_helper')->getRouteEntity();
    if (!$group) {
      return [];
    }
    if ($group->bundle() !== 'club_society') {
      return [];
    }

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = \Drupal::service('su_clubs_societies.service');

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
    $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');

    if ($clubSocMembershipsService->userIsCurrentMember(\Drupal::currentUser(), $group)) {
      return $this->buildAlreadyMember($group);
    } elseif (!$clubSocService->checkOnSaleDates()) {
      return $this->buildNotOnSale($group);
    } elseif (!$clubSocService->checkOnSaleForGroup($group)) {
      return $this->buildNoProducts($group);
    } else {
      return $this->buildJoinForm($group);
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('user', 'route.group'));
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 60 * 15; // 15 minutes
  }

  public function buildAlreadyMember($group) {
    $build = [];

    $message = \Drupal::config('su_clubs_societies.settings')->get('message_already_member');

    if ($message && $message['value'] != '') {
      $build['markup'] = [
        '#markup' => Markup::create($message['value']),
      ];
    }

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = \Drupal::service('su_clubs_societies.service');

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
    $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');

    if ($clubSocMembershipsService->userCanUpgradeMembership(\Drupal::currentUser(), $group)) {
      $upgradeLink = Url::fromRoute('su_clubs_societies.upgrade_membership', ['group' => $group->id()])->toString();
      $build['upgrade'] = [
        '#markup' => ' <p>If you need to upgrade your membership (to a higher price one), click the button below.</p><a href="' . $upgradeLink . '" class="button">Upgrade membership</a>',
      ];
    }

    if ($clubSocMembershipsService->userCanCancelMembership(\Drupal::currentUser(), $group)) {
      $cancelLink = Url::fromRoute('su_clubs_societies.cancel_membership', ['group' => $group->id()])->toString();
      $build['cancel'] = [
        '#markup' => ' <p>If you would like to cancel your membership, click the button below.</p><a href="' . $cancelLink . '" class="button">Cancel membership</a>',
      ];
    }

    return $build;
  }

  public function buildNoProducts($group) {
    $build = [];

    $message = \Drupal::config('su_clubs_societies.settings')->get('message_no_products');

    if ($message && $message['value'] != '') {
      $build['markup'] = [
        '#markup' => Markup::create($message['value']),
      ];
    }

    $this->addSignupForm($build, $group);

    return $build;
  }
  public function buildNotOnSale($group) {
    $build = [];

    $message = \Drupal::config('su_clubs_societies.settings')->get('message_not_on_sale');
    if ($message && $message['value'] != '') {
      $build['markup'] = [
        '#markup' => Markup::create($message['value']),
      ];
    }

    $this->addSignupForm($build, $group);

    return $build;
  }

  public function buildJoinForm(GroupInterface $group) {
    $view_mode = 'sidebar_add_to_cart';
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('group');
    $build = $view_builder->view($group, $view_mode);
    return $build;
  }

  public function addSignupForm(&$build, $group) {

    $build['form_header'] = [
      '#markup' => Markup::create("You can sign up to the club/society mailing list in the meantime."),
    ];

    $form_class = '\Drupal\group_mailing_lists\Form\SignupForm';
    $build['form'] = \Drupal::formBuilder()->getForm($form_class, $group);
  }
}
