<?php

namespace Drupal\su_clubs_societies\Plugin\views\field;

use DateTime;
use Drupal\commerce_product\Entity\ProductAttribute;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Plugin\views\field\MembershipRecordsForGroupCount;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Count of number of membership records for a group
 *
 * @ViewsField("su_clubs_socs_members_count")
 */
class ClubSocMembersCount extends MembershipRecordsForGroupCount {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['roles'] = ['default' => ['club_society-member']];
    $options['membership_types'] = ['default' => []];
    $options['membership_types_option'] = ['default' => 'include'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);


    $attribute = ProductAttribute::load('club_society_membershi');
    $attributeValueStorage = \Drupal::entityTypeManager()->getStorage('commerce_product_attribute_value');
    $values = $attributeValueStorage->loadMultipleByAttribute($attribute->id());
    foreach ($values as $index => $attribute) {
      $options[$attribute->getName()] = $attribute->getName();
    }

    $form['membership_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Membership type(s)'),
      '#default_value' => $this->options['membership_types'],
      '#options' => $options,
      '#multiple' => TRUE,
    ];

    $form['membership_types_option'] = [
      '#type' => 'select',
      '#title' => $this->t('For above membership type(s), restrict to or exclude.'),
      '#default_value' => $this->options['membership_types_option'],
      '#options' => [
        'include' => 'Restrict results to the selected membership types',
        'exclude' => 'Exclude selected membership types (count all others)',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function filterRecords($records) {
    if (isset($this->options['membership_types']) && count($this->options['membership_types']) > 0) {
      $finalRecords = [];
      foreach ($records as $record) {
        if ($this->options['membership_types_option'] == 'include') {
          if (in_array($record->field_membership_type->value, $this->options['membership_types'])) {
            $finalRecords[] = $record;
          }
        }

        if ($this->options['membership_types_option'] == 'exclude') {
          if (!in_array($record->field_membership_type->value, $this->options['membership_types'])) {
            $finalRecords[] = $record;
          }
        }
      }
      return $finalRecords;
    }
    return $records;
  }
}
