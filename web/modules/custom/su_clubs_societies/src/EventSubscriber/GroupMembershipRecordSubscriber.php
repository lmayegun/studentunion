<?php

namespace Drupal\su_clubs_societies\EventSubscriber;

use Drupal\group_membership_record\Event\GroupMembershipRecordEvent;
use Drupal\group_membership_record\Event\GroupMembershipRecordEventType;
use Drupal\user\Entity\Role;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GroupMembershipRecordSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      GroupMembershipRecordEventType::INSERT => ['onInsert', 100],
      GroupMembershipRecordEventType::UPDATE => ['onUpdate', 100],
      // Don't need to run it on update as Club/Soc GMRs roles can't be changed after creation
    ];
    return $events;
  }

  public function onInsert(GroupMembershipRecordEvent $event) {
    $record = $event->getRecord();
    $role = $record->getRole();
    if ($role) {
      $roleId = $role->id();

      if (in_array($roleId, ['club_society-president', 'club_society-treasurer'])) {
        $siteRole = Role::load('club_society_president_or_treasurer');
        $record->field_site_role->entity = $siteRole;
        $record->save();
        $record->syncSiteRoles();

        su_clubs_societies_update_president($record->getGroup());
      }

      if (strpos($roleId, 'club_society')) {
        su_clubs_societies_enrol_in_required_courses($record);
      }

      if (strpos($roleId, 'club_society')) {
        su_clubs_societies_enrol_in_required_courses($record);
      }
    }
  }

  public function onUpdate(GroupMembershipRecordEvent $event) {
    $record = $event->getRecord();
    $role = $record->getRole();
    if ($role) {
      $roleId = $role->id();

      if (strpos($roleId, 'club_society')) {
        su_clubs_societies_enrol_in_required_courses($record);
        su_clubs_societies_update_president($record->getGroup());
      }
    }
  }
}
