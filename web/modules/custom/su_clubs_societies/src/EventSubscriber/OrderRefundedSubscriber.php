<?php

namespace Drupal\su_clubs_societies\EventSubscriber;

use DateTime;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderItemEvent;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_refund_order_item\Event\OrderRefundedEvent;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderRefundedSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.refund.order_item' => ['onOrderRefund', 100],

      'commerce_order.commerce_order.predelete' => ['onOrderCancelOrDelete', 100],
      'commerce_order.cancel.post_transition' => ['onOrderCancelOrDeleteWorkflow', 100],

      'commerce_order.commerce_order_item.predelete' => ['onOrderItemDelete', 100],
    ];
    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param OrderRefundedEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderRefund(OrderRefundedEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getOrder();
    $itemsWithQuantity = $event->getItemsWithQuantity();
    $orderRecords = [];

    foreach ($itemsWithQuantity as $orderItemId => $quantityRefunded) {
      if ($quantityRefunded == 0) {
        continue;
      }

      $orderItem = OrderItem::load($orderItemId);
      $this->removeMembershipForOrderItem($orderItem);
    }
  }

  public function onOrderCancelOrDelete(OrderEvent $event) {
    $items = $event->getOrder()->getItems();
    foreach ($items as $orderItem) {
      $this->removeMembershipForOrderItem($orderItem);
    }
  }

  public function onOrderCancelOrDeleteWorkflow(WorkflowTransitionEvent $event) {
    $items = $event->getEntity()->getItems();
    foreach ($items as $orderItem) {
      $this->removeMembershipForOrderItem($orderItem);
    }
  }

  /**
   * @param OrderItemEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderItemDelete(OrderItemEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $orderItem = $event->getOrderItem();
    $this->removeMembershipForOrderItem($orderItem);
  }

  public function removeMembershipForOrderItem($orderItem) {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = $orderItem->getPurchasedEntity();
    if (!$variation) {
      return;
    }
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    if ($product = $variation->getProduct()) {
      if ($product->bundle() !== 'club_society_membership') {
        return;
      }
      $clubSoc = $product->get('field_club_society')->referencedEntities() ? $product->get('field_club_society')->referencedEntities()[0] : null;

      if (!isset($orderRecords)) {
        $query = \Drupal::entityQuery('group_membership_record')
          ->condition('field_order', $orderItem->getOrder()->id());
        $ids = $query->execute();
        if ($ids) {
          $orderRecords = GroupMembershipRecord::loadMultiple($ids);
        }
      }
      if (isset($orderRecords)) {
        foreach ($orderRecords as $record) {
          if ($clubSoc->id() == $record->getGroup()->id()) {
            // $logger = \Drupal::logger('su_clubs_societies')->info('Order ' . $orderItem->getOrder()->id() . ' cancelled or refunded, ending membership ' . $record->id());
            $record->setEndDate(strtotime('yesterday 23:59'));
            $record->save();
          }
        }
      }
    }
  }
}
