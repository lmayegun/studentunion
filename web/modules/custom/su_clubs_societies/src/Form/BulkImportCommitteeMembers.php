<?php

namespace Drupal\su_clubs_societies\Form;

use Drupal;
use Drupal\group\Entity\Group;
use Drupal\su_groups\Form\BulkImportGroupMembershipRecordForm;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportCommitteeMembers extends BulkImportGroupMembershipRecordForm {

  const groupMembershipRecordType = 'club_society_committee';

  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = parent::getMapping();

    unset($fields['Group ID']);
    $fields['Club/Society'] = [
      'description' => 'Club/society three letter code OR group website ID number',
      'example' => 'ABA',
    ];
    $fields['Role details'] = [
      'description' => 'additional details - free text',
      'example' => 'Social Media Officer',
    ];
    $fields['Interim'] = [
      'description' => 'TRUE or FALSE or blank to change nothing for that row',
      'allowed_values' => ['TRUE', 'FALSE', ''],
    ];
    $fields['Resigned'] = [
      'description' => 'TRUE or FALSE or blank to change nothing for that row',
      'allowed_values' => ['TRUE', 'FALSE', ''],
    ];
    $fields['Captain type'] = [
      'description' => 'If a captain, optional type.',
      'allowed_values' => ['BUCS', 'Events', 'LUSL', 'TeamUCL', ''],
    ];

    return $fields;
  }

  public static function validateRow($row, &$context): bool {
    $group = static::getGroup($row);
    if (!$group) {
      static::addError($row, $context, 'Club/soc not found with code/ID ' . $row['Club/Society']);
      return FALSE;
    }

    $captainTerm = static::getCaptainTerm($row);
    if (isset($record['Captain type']) && !$captainTerm) {
      static::addError($row, $context, 'Cant find captain type with type name ' . $row['Captain type']);
      return FALSE;
    }

    // Check parent ones
    $validValues = parent::validateRow($row, $context);
    if (!$validValues) {
      return FALSE;
    }

    return TRUE;
  }

  public static function getGroup($row) {
    $clubSocCode = trim($row['Club/Society']);
    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = Drupal::service('su_clubs_societies.service');
    $group = $clubSocService->getClubSocByDept($clubSocCode);
    if (Group::load((int) $clubSocCode)) {
      $group = Group::load((int) $clubSocCode);
    }
    if (!$group) {
      return parent::getGroup($row);
    }
    return $group;
  }

  private static function getCaptainTerm($record): ?int {
    $captainTerm = NULL;
    if (isset($record['Captain type']) && $record['Captain type'] != '') {
      $captainTerm = su_drupal_helper_get_taxonomy_term_by_name($record['Captain type'], 'clubs_societies_captain_types');
    }
    return $captainTerm;
  }

  public static function updateGmrFromRow(&$gmr, $row) {
    parent::updateGmrFromRow($gmr, $row);

    if (isset($row['Role details'])) {
      $gmr->set('field_club_society_role_details', trim($row['Role details']));
    }

    if ($captainTerm = static::getCaptainTerm($row)) {
      $gmr->field_captain_type->entity = $captainTerm;
    }

    if (isset($row['Resigned']) && $row['Resigned'] != '') {
      $gmr->set('field_club_society_resigned', $row['Resigned'] == 'TRUE');
    }

    if (isset($row['Interim']) && $row['Interim'] != '') {
      $gmr->set('field_club_society_interim', $row['Interim'] == 'TRUE');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_clubs_societies_committee_upload';
  }

}
