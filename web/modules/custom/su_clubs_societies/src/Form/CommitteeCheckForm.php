<?php

namespace Drupal\su_clubs_societies\Form;

use DateInterval;
use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;

/**
 * Configure Bookable resources settings for this site.
 */
class CommitteeCheckForm extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_clubs_societies_committee_checker';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['description'] = [
      '#markup' => '<p>This tool checks committee roles assigned for a given year and returns a report showing how many are assigned to each role, whether they have bought club/society membership for that year, and other useful facts. You can then filter that CSV to identify specific issues.</p><p>For the current year, this is determined using the current officers (as of today). For past and future years, it is determined based on the start of the committee year (i.e. handover).'
    ];

    $yearService = \Drupal::getContainer()->get('date_year_filter.year');

    $options = [];

    $now = new DateTime('now');
    $date = new DateTime('-1 years');
    $start = su_clubs_societies_get_start_of_committee_year_for_date($date)->start;
    $options[$start->format("Y-m-d\TH:i:s")] = $yearService->formatDateAsYear($date, $start);

    $options[$now->format("Y-m-d\TH:i:s")] = $yearService->formatDateAsYear($now, $start) . ' (as of today)';

    $date = new DateTime('+1 years');
    $start = su_clubs_societies_get_start_of_committee_year_for_date($date)->start;
    $options[$start->format("Y-m-d\TH:i:s")] = $yearService->formatDateAsYear($date, $start);

    $form['academic_year'] = [
      '#type'      => 'select',
      '#title'     => $this->t('Date to check'),
      '#options'     => $options,
      '#default_value' => $now->format("Y-m-d\TH:i:s"),
      '#validated' => TRUE,
    ];

    $form['include_enabled_checks'] = [
      '#type'      => 'checkbox',
      '#title'     => $this->t('Include summary of role requirements such as course and quiz (for president and treasurer)'),
      '#default_value' => FALSE,
    ];

    unset($form['submit']);
    unset($form['actions']['submit']);
    $form['actions']['submit'] = [
      '#type'    => 'submit',
      '#value'     => $this->t('Run checks and download report'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $ids = \Drupal::entityQuery('group')
      // ->condition('status', 1)
      ->condition('type', 'club_society')
      ->execute();

    $chunks = array_chunk($ids, 10);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }

    $values = $form_state->getValues();
    $_SESSION[$this->getFormId() . 'academic_year'] = $values['academic_year'] ?? NULL;
    $_SESSION[$this->getFormId() . 'include_enabled_checks'] = $values['include_enabled_checks'] ?? NULL;
  }

  /**
   * Sorts an array arbitrarily by keys.
   *
   * @param mixed $inputArray
   *   Arrays to sort.
   * @param array $sort_order
   *   Keys in order.
   */
  public static function sortByArbitraryKeys(array &$inputArray, array $order) {
    // https://stackoverflow.com/questions/5632427/php-sort-by-arbitrary-order
    $sort_order = array_flip($order);
    uksort($inputArray, function ($a, $b) use ($order) {
      $a = array_search($a, $order);
      $b = array_search($b, $order);
      if ($a === FALSE && $b === FALSE) {
        return 0;
      } elseif ($a === FALSE) {
        return 1;
      } elseif ($b === FALSE) {
        return -1;
      } else {
        return $a - $b;
      }
    });
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch($chunk, array &$context) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $date = DateTime::createFromFormat("Y-m-d\TH:i:s", $_SESSION[$this->getFormId() . 'academic_year']);

    $yearService = \Drupal::getContainer()->get('date_year_filter.year');
    $start = su_clubs_societies_get_start_of_committee_year_for_date($date)->start;
    $year = $yearService->formatDateAsYear($date, $start);

    $roles = \Drupal::entityTypeManager()->getStorage('group_role')->loadByProperties([
      'group_type' => 'club_society',
      'internal' => FALSE,
    ]);

    $this->sortByArbitraryKeys($roles, [
      'club_society-president',
      'club_society-treasurer',
      'club_society-member',
    ]);

    foreach ($chunk as $id) {
      $group = Group::load($id);
      if (!$group) {
        return FALSE;
      }

      $array = [
        'Year' => $year,
        'Club/Society' => $group->label(),
        'Club/Society code' => $group->field_finance_department_code->entity ? $group->field_finance_department_code->entity->getName() : '',
        'Club/Society affiliated' => $group->field_clubsoc_affiliated->value ? 'Yes' : 'No',
        'Club/Society published' => $group->status->value ? 'Yes' : 'No',
      ];

      foreach ($roles as $role) {
        $records = $gmrRepositoryService->get(NULL, $group, $role, NULL, $date, NULL);

        $array[$role->label() . ' total'] = 0;
        $array[$role->label() . ' enabled / with access'] = 0;
        $array[$role->label() . ' with society membership'] = 0;

        foreach ($records as $record) {
          if ($record->hasField('field_club_society_resigned') && $record->field_club_society_resigned->value != 0) {
            continue;
          }

          $array[$role->label() . ' total'] = $array[$role->label() . ' total'] + 1;

          if ($record->isEnabled()) {
            $array[$role->label() . ' enabled / with access'] = $array[$role->label() . ' enabled / with access'] + 1;
          }

          if ($gmrRepositoryService->count($record->getUser(), $group, GroupRole::load('club_society-member'), NULL, $date, NULL) > 0) {
            $array[$role->label() . ' with society membership'] = $array[$role->label() . ' with society membership'] + 1;
          }

          if ($_SESSION[$this->getFormId() . 'include_enabled_checks']) {
            $prefix = $role->label() . ' - ' .  $record->getUser()->getDisplayName() . ': ';
            $this->addRoleChecks($record, $prefix, $array);

            $courses = su_clubs_societies_get_required_courses_for_role($record);

            foreach ($courses as $course) {
              $enrollment = $course->getEnrollment($record->getUser());

              if (!$enrollment) {
                $enrollment = $course->enroll($record->getUser());
              }

              $array[$prefix . ' course completion ' . $course->getTitle()] = $enrollment->isComplete() ? 'Yes' : 'No';

              $objects = $course->getObjects();
              foreach ($objects as $courseObject) {
                if (!$courseObject->getOption('enabled')) {
                  continue;
                }
                $fulfillment = $courseObject->getFulfillment($record->getUser());
                $array[$prefix . $courseObject->getTitle()] = $fulfillment->isComplete() ? 'Yes' : 'No';
              }
            }
          }
        }
      }

      $this->addLine($array, $context);
    }
  }

  /**
   * Check enabled state for a role.
   *
   * @param mixed $record
   * @param mixed $prefix
   * @param mixed $array
   *   CSV results array.
   */
  public function addRoleChecks($record, $prefix, &$array) {
    $enabled = NULL;
    $explanations = [];

    $pluginManager = \Drupal::service('plugin.manager.group_membership_record.enabled_determiner');
    $plugins = $pluginManager->getForInstanceType($record->bundle());
    foreach ($plugins as $plugin) {
      $enabled = $plugin->determineEnabledValue($record);
      $explanationsLoaded = array_merge($explanations, $plugin->getEnabledExplanation($record));
    }

    $explanations = [];
    foreach ($explanationsLoaded as $loaded) {
      if (is_array($loaded)) {
        if (isset($loaded[1])) {
          $success = isset($loaded[1]) && $loaded[1];
          $result = $success ? 'Yes' : 'No';
          $array[$prefix . $loaded[2]] = $result;
        }
      }
    }
  }
}
