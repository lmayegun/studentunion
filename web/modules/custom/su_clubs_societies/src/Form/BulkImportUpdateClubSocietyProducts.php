<?php

namespace Drupal\su_clubs_societies\Form;

use Drupal;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductAttribute;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\csv_import_export\Form\BatchImportCSVForm;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportUpdateClubSocietyProducts extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_clubs_societies_products_upload';
  }

  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['DEPT'] = [
      'description' => 'the three letter code',
      'example' => 'ABY',
    ];
    $fields['Type'] = [
      'description' => 'membership type name',
      'example' => 'Taster',
    ];
    $fields['Price'] = [
      'description' => 'price - decimal numbers only',
      'example' => '3.00',
    ];
    $fields['Action'] = [
      'description' => '"enable" (to create or update) or "disable"',
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    // Import as normal
    parent::setOperations($batch_builder, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = Drupal::service('su_clubs_societies.service');

    $clubSocCode = trim($row['DEPT']);
    $group = $clubSocService->getClubSocByDept($clubSocCode);
    if (!$group) {
      static::addError($row, $context, 'Group not found with code ' . $clubSocCode);
      return FALSE;
    }

    $membershipType = strip_tags(trim($row['Type']));
    if (strlen($membershipType) > 35) {
      static::addError($row, $context, 'Membership type cannot be longer than 35 characters.');
      return FALSE;
    }

    if ($membershipType == '') {
      $membershipType = 'Standard';
    }

    $price = $row['Price'];
    $priceObject = FALSE;
    if ($price != '') {
      $priceObject = new Price($price, 'GBP');
    }

    $action = $row['Action'] ?? 'enable';

    $attribute = ProductAttribute::load('club_society_membershi');
    $attributeValueStorage = Drupal::entityTypeManager()
      ->getStorage('commerce_product_attribute_value');
    $values = $attributeValueStorage->loadMultipleByAttribute($attribute->id());

    $valueToUse = FALSE;
    foreach ($values as $index => $value) {
      if ($value->getName() == $membershipType) {
        $valueToUse = $value;
        break;
      }
    }
    if (!$valueToUse) {
      $valueToUse = $attributeValueStorage->create([
        'attribute' => $attribute->id(),
        'langcode' => $attribute->get('langcode'),
      ]);
      $valueToUse->setName($membershipType);
      $valueToUse->save();
    }

    $product = $group->field_product->entity;
    if (!$product) {
      $product = $clubSocService->createProduct($group);
    }

    $variations = $product->getVariations();

    $existingVariation = FALSE;
    foreach ($variations as $variation) {
      $attributeValues = $variation->attribute_club_society_membershi->referencedEntities();
      if (count($attributeValues) == 0) {
        continue;
      }
      $attributeValue = reset($attributeValues);
      if ($attributeValue && $attributeValue->label() == $membershipType) {
        $existingVariation = $variation;
        break;
      }
    }

    $action = strtolower($action);
    if ($action == 'enable') {
      if ($existingVariation) {
        $existingVariation->setPublished(TRUE);
        if ($priceObject) {
          $existingVariation->setPrice($priceObject);
        }
        $existingVariation->save();
        return TRUE;
      }
      else {
        if (!$priceObject) {
          static::addError($row, $context, 'Must provide valid price (even just 0) as there is no existing variation for that membership type.');
          return FALSE;
        }
        $variation = ProductVariation::create([
          'type' => 'club_society_membership',
          'sku' => strtoupper('CSC_' . $clubSocCode . '_' . Drupal::service('su_drupal_helper')
              ->getMachineName($membershipType)),
          'title' => $product->getTitle() . ' - ' . $membershipType,
          'price' => $priceObject,
          'status' => 1,
        ]);
        $variation->attribute_club_society_membershi->entity = $valueToUse;
        $variation->product_id->entity = $product;
        $variation->maximum_order_quantity->value = 1;
        $variation->save();
        $product->addVariation($variation);
        $product->save();
        return TRUE;
      }
    }
    elseif ($action == 'disable') {
      if ($existingVariation) {
        $existingVariation->setPublished(FALSE);
        if ($priceObject) {
          $existingVariation->setPrice($priceObject);
        }
        $existingVariation->save();
        return TRUE;
      }
    }
    static::addError($row, $context, 'Could not find or create variation.');
    return FALSE;
  }

}
