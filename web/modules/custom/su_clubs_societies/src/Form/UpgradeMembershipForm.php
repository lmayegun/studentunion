<?php

namespace Drupal\su_clubs_societies\Form;

use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\user\Entity\User;

/**
 * Form allowing users to select more expensive membership type.
 */
class UpgradeMembershipForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_clubs_societies_upgrade_membership';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Group $group = NULL) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = \Drupal::service('su_clubs_societies.service');

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
    $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $this->t('If you need a higher level of membership, you can upgrade here. You will need to pay any price difference.'),
    ];

    $variations = [];
    $account = User::load(Drupal::currentUser()->id());
    $currentRecord = $clubSocMembershipsService->userIsCurrentMember($account, $group, TRUE);
    if (!$currentRecord) {
      $this->messenger()->addMessage('You do not have a current membership.');
      return $this->redirect('entity.group.canonical', ['group' => $group->id()]);
    }

    $possibleVariations = $clubSocMembershipsService->userCanUpgradeMembership($account, $group, TRUE);
    foreach ($possibleVariations as $variation) {
      $price = $variation->getPrice();
      $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
      $priceFormatted = $currency_formatter->format($price->getNumber(), $price->getCurrencyCode(), []);
      $membershipType = $variation->getAttributeValue('attribute_club_society_membershi');
      $variations[$variation->id()] = $membershipType->label() . ' - ' . $priceFormatted;
    }

    $alreadyPurchasedVariation = $clubSocService->getVariationFromMembershipRecord($currentRecord);
    if (!$alreadyPurchasedVariation) {
      // dd('No variation purchased but can upgrade membership?');
      $this->messenger()->addMessage('You do not have a current membership.');
      return $this->redirect('entity.group.canonical', ['group' => $group->id()]);
    }
    $form_state->set('alreadyPurchasedVariation', $alreadyPurchasedVariation->id());
    $form_state->set('currentRecord', $currentRecord->id());

    $form['upgradeToVariation'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the membership type to upgrade to'),
      '#options' => $variations,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purchase upgrade'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $alreadyPurchasedVariation = ProductVariation::load($form_state->get('alreadyPurchasedVariation'));
    if (!$alreadyPurchasedVariation) {
      return FALSE;
    }
    $alreadyPaid = $alreadyPurchasedVariation->getPrice()->getNumber();

    $upgradeToVariation = ProductVariation::load($form_state->getValue('upgradeToVariation'));

    $total = $upgradeToVariation->getPrice()->getNumber() - $alreadyPaid;

    $order_item = OrderItem::create([
      'type' => 'membership',
      'purchased_entity' => $upgradeToVariation->id(),
      'quantity' => 1,
      'unit_price' => new Price($total, $alreadyPurchasedVariation->getPrice()->getCurrencyCode(), []),
      'overridden_unit_price' => TRUE,
    ]);
    $order_item->field_record_upgraded_from->entity = GroupMembershipRecord::load($form_state->get('currentRecord'));
    $order_item->save();

    $order = Order::create([
      'type' => 'default',
      'mail' => \Drupal::currentUser()->getEmail(),
      'uid' => \Drupal::currentUser()->id(),
      'store_id' => 1,
      'order_items' => [$order_item],
      'placed' => \Drupal::time()->getCurrentTime(),
      // 'checkout_step' => 'payment',
      'state' => 'draft',
      'payment_gateway' => 'worldnet_gateway',
    ]);
    $order->recalculateTotalPrice();
    $order->save();
    $order->set('order_number', $order->id());
    $order->save();

    $form_state->setRedirect('commerce_checkout.form', ['commerce_order' => $order->id()]);
    return;
  }
}
