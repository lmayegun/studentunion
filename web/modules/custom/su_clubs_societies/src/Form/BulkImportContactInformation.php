<?php

namespace Drupal\su_clubs_societies\Form;

use Drupal;
use Drupal\csv_import_export\Form\BatchImportCSVForm;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportContactInformation extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_clubs_societies_import_contact';
  }

  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['User'] = [
      'description' => 'either UCL e-mail address, UPI or UCL user ID',
      'example' => 'uczxmke, uczxmke@ucl.ac.uk or m.keeble.21@ucl.ac.uk',
      'required' => TRUE,
    ];
    $fields['Personal e-mail'] = [
    ];
    $fields['Personal phone number'] = [
    ];
    $fields['Emergency contact e-mail'] = [
    ];
    $fields['Emergency contact phone'] = [
    ];
    $fields['Emergency contact name'] = [
    ];
    return $fields;
  }

  private static function getUser($row) {
    $service = Drupal::service('su_user.ucl_user');
    return $service->getUserForUclFieldInput($row['User']);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processRow(array $row, array &$context) {
    $user = static::getUser($row);
    if (!$user) {
      static::addError($row, $context, 'Cannot find user with that information.');
      return FALSE;
    }

    /** @var \Drupal\profile\ProfileStorageInterface $storage */
    $storage = Drupal::entityTypeManager()->getStorage('profile');
    $profile = $storage->loadByUser($user, 'contact_information');

    if (!$profile) {
      $profile = Drupal\profile\Entity\Profile::create([
        'type' => 'contact_information',
        'uid' => $user->id(),
      ]);
    }

    $fields = [
      'Personal e-mail' => 'field_personal_email',
      'Personal phone number' => 'field_personal_contact_phone',
      'Emergency contact e-mail' => 'field_emergency_contact_email',
      'Emergency contact phone' => 'field_emergency_contact_phone',
      'Emergency contact name' => 'field_emergency_contact_name',
    ];

    foreach ($fields as $column_id => $field_id) {
      if (isset($row[$column_id]) && $row[$column_id]) {
        $profile->set($field_id, $row[$column_id]);
      }
    }

    $profile->save();
  }

}
