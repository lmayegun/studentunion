<?php

namespace Drupal\su_clubs_societies\Form;

use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\user\Entity\User;

/**
 * Form allowing users to select more expensive membership type.
 */
class CancelMembershipForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_clubs_societies_cancel_membership';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Group $group = NULL) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = \Drupal::service('su_clubs_societies.service');

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
    $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Are you sure you want to cancel your membership?'),
    ];

    $variations = [];
    $account = User::load(Drupal::currentUser()->id());
    $currentRecord = $clubSocMembershipsService->userIsCurrentMember($account, $group, TRUE);
    if (!$currentRecord) {
      $this->messenger()->addMessage('You do not have a current membership.');
      return $this->redirect('entity.group.canonical', ['group' => $group->id()]);
    }

    $alreadyPurchasedVariation = $clubSocService->getVariationFromMembershipRecord($currentRecord);
    if (!$alreadyPurchasedVariation) {
      // dd('No variation purchased but can upgrade membership?');
      $this->messenger()->addMessage('You do not have a current membership.');
      return $this->redirect('entity.group.canonical', ['group' => $group->id()]);
    }
    $form_state->set('alreadyPurchasedVariation', $alreadyPurchasedVariation->id());
    $form_state->set('currentRecord', $currentRecord->id());

    if (!$clubSocMembershipsService->userCanCancelMembership($account, $group)) {
      $this->messenger()->addMessage('You cannot cancel your current type of membership. You may need to request a refund.');
      return $this->redirect('entity.group.canonical', ['group' => $group->id()]);
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel membership'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $currentRecord = GroupMembershipRecord::load($form_state->get('currentRecord'));
    if (!$currentRecord) {
      return FALSE;
    }

    $currentRecord->setEndDate(strtotime('yesterday midnight'));
    $currentRecord->enabled->value = FALSE;
    $currentRecord->save();

    $order = $currentRecord->field_order->entity;
    if ($order) {
      if (count($order->getItems()) == 1) {
        if ($order->getState()->isTransitionAllowed('cancel')) {
          $order->getState()->applyTransitionById('cancel');
          $order->save();
        }
      }
    }

    $this->messenger()->addMessage('Membership cancelled.');
    $form_state->setRedirect('entity.group.canonical', ['group' => $currentRecord->getGroup()->id()]);
  }
}
