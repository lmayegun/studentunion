<?php

namespace Drupal\su_clubs_societies\Form;

use DateTime;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class CommitteeMemberForm extends GroupMembershipRecordForm
{

  /**
   * {@inheritdoc}
   */
  protected $groupTypes = ['club_society'];

  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added committee member.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved committee member.';

  /**
   * {@inheritdoc}
   */
  protected $allowedGroups = [];

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = [];

  /**
   * {@inheritdoc}
   */
  protected $allowedMembershipRecordTypes = ['club_society_committee'];

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['user_id']['#title'] = 'User';
    $form['group_id']['#title'] = 'Club/Society';
    $form['group_role_id']['#title'] = 'Role';

    // Set default daterange
    $startDate = \Drupal::config('su_clubs_societies.settings')->get('date_committee_role_starts');
    $now = new DateTime();
    $yearService = \Drupal::getContainer()->get('date_year_filter.year');
    $year = $yearService->getYearForDate($now, $startDate);
    $this->defaultStartDate = $now->format("Y-m-d\TH:i:s");
    $this->defaultEndDate = $year->end->format("Y-m-d\TH:i:s");

    /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
    $form = parent::buildForm($form, $form_state);

    // @todo make the autocomplete properly save the user ID
    // $form['user_id']['#type'] = 'textfield';
    // $form['user_id']['#autocomplete_route_name'] = 'su_user.autocomplete_ucl_users';

    // @todo default start date to today, end date to the end of the academic year?

    return $form;
  }
}
