<?php

namespace Drupal\su_clubs_societies\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Students Union UCL Events and Whats on settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_clubs_societies_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['su_clubs_societies.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tabs'] = [
      '#type' => 'vertical_tabs'
    ];

    $form['tab_product'] = [
      '#title' => 'Memberships & products',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['date_membership_ends'] = [
      '#group' => 'tab_product',
      '#type' => 'date',
      '#title' => $this->t('Date when club/society memberships end'),
      '#default_value' => $this->config('su_clubs_societies.settings')->get('date_membership_ends') ? $this->config('su_clubs_societies.settings')->get('date_membership_ends') : null,
      '#description' => $this->t('The year part of this date is ignored; this does not need to be updated each year.')
    ];
    $form['date_on_sale'] = [
      '#group' => 'tab_product',
      '#type' => 'date',
      '#title' => $this->t('Date when club/society memberships are on sale'),
      '#default_value' => $this->config('su_clubs_societies.settings')->get('date_on_sale') ? $this->config('su_clubs_societies.settings')->get('date_on_sale') : null,
      '#description' => $this->t('The year part of this date is ignored; this does not need to be updated each year.')
    ];
    $form['date_off_sale'] = [
      '#group' => 'tab_product',
      '#type' => 'date',
      '#title' => $this->t('Date when club/society memberships go off sale'),
      '#default_value' => $this->config('su_clubs_societies.settings')->get('date_off_sale') ? $this->config('su_clubs_societies.settings')->get('date_off_sale') : null,
      '#description' => $this->t('The year part of this date is ignored; this does not need to be updated each year.')
    ];

    $form['tab_committee'] = [
      '#title' => 'Committee',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['date_committee_role_starts'] = [
      '#group' => 'tab_committee',
      '#type' => 'date',
      '#title' => $this->t('Date when committee role starts (handover date)'),
      '#default_value' => $this->config('su_clubs_societies.settings')->get('date_committee_role_starts') ? $this->config('su_clubs_societies.settings')->get('date_committee_role_starts') : null,
      '#description' => $this->t('Note changing this does NOT change existing records. Also note, the year part of this date is ignored; this does not need to be updated each year.')
    ];
    $form['date_reregistration_starts'] = [
      '#group' => 'tab_committee',
      '#type' => 'date',
      '#title' => $this->t('Date when reregistrations count for the next academic year'),
      '#default_value' => $this->config('su_clubs_societies.settings')->get('date_reregistration_starts') ? $this->config('su_clubs_societies.settings')->get('date_reregistration_starts') : null,
      '#description' => $this->t('The year part of this date is ignored; this does not need to be updated each year.')
    ];

    $form['tab_page'] = [
      '#title' => 'Club/Soc page',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['message_already_member'] = [
      '#group' => 'tab_page',
      '#type' => 'text_format',
      '#title' => $this->t('Message shown when already member'),
      '#format' => 'basic_html',
      '#default_value' => $this->config('su_clubs_societies.settings')->get('message_already_member') ? $this->config('su_clubs_societies.settings')->get('message_already_member')['value'] : '',
    ];
    $form['message_not_on_sale'] = [
      '#group' => 'tab_page',
      '#type' => 'text_format',
      '#title' => $this->t('Message shown when not on sale'),
      '#format' => 'basic_html',
      '#default_value' => $this->config('su_clubs_societies.settings')->get('message_not_on_sale') ? $this->config('su_clubs_societies.settings')->get('message_not_on_sale')['value'] : '',
    ];
    $form['message_no_products'] = [
      '#group' => 'tab_page',
      '#type' => 'text_format',
      '#title' => $this->t('Message shown when group has no products'),
      '#format' => 'basic_html',
      '#default_value' => $this->config('su_clubs_societies.settings')->get('message_no_products') ? $this->config('su_clubs_societies.settings')->get('message_no_products')['value'] : '',
    ];

    $form['tab_forms'] = [
      '#title' => 'Forms',
      '#type' => 'details',
      '#group' => 'tabs',
    ];
    $form['cs_forms'] = [
      '#group' => 'tab_forms',
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Club/Society forms'),
      '#multiple' => TRUE,
      '#target_type' => 'webform',
      '#tags' => TRUE,
      '#default_value' => $this->config('su_clubs_societies.settings')->get('cs_forms') ? $this->config('su_clubs_societies.settings')->get('cs_forms') : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('su_clubs_societies.settings')
      ->set('message_already_member', $form_state->getValue('message_already_member'))
      ->set('message_not_on_sale', $form_state->getValue('message_not_on_sale'))
      ->set('message_no_products', $form_state->getValue('message_no_products'))
      ->set('date_on_sale', $form_state->getValue('date_on_sale'))
      ->set('date_off_sale', $form_state->getValue('date_off_sale'))
      ->set('date_membership_ends', $form_state->getValue('date_membership_ends'))
      ->set('date_committee_role_starts', $form_state->getValue('date_committee_role_starts'))
      ->set('date_reregistration_starts', $form_state->getValue('date_reregistration_starts'))
      ->set('cs_forms', $form_state->getValue('cs_forms'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
