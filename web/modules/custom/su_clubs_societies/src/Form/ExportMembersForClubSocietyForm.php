<?php

namespace Drupal\su_clubs_societies\Form;

use DateTime;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\user\Entity\User;

/**
 * Allow user to upload a CSV and be given a download of student data from the website
 *
 * This defaults to providing an anonymised export, but can also provide a full identifiable export
 *
 *
 */
class ExportMembersForClubSocietyForm extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_clubs_societies_export';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['description'] = [
      '#markup' => '<p>Upload a CSV file with at least one identifying column for each club/society, e.g. three letter code, name, or ID.</p>'
    ];

    $form['file_upload'] = [
      '#type'              => 'file',
      '#title'             => $this->t('Import CSV File'),
      '#size'              => 40,
      '#description'       => $this->t('Upload a CSV file with a three letter code, name, or ID in a column.'),
      '#required'          => FALSE,
      '#autoupload'        => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];

    $form['data'] = [
      '#type' => 'fieldset',
      '#title' => 'Data protection agreements',
      '#description'   => 'For more information see our <a href="https://studentsunionucl.org/data-protection-and-privacy-policy" target="_blank">Data protection and privacy policy</a> or contact <a href="mailto:su.data-protection@ucl.ac.uk">su.data-protection@ucl.ac.uk</a>.',
    ];

    $form['data']['use'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to use the data only for the legitimate interests of the Union and its members, as outlined in our data protection and privacy policy.'),
      "#required"      => true
    ];

    $form['data']['share'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to keep this file within the Union, and not to share this file with any students, officers, UCL staff or unless explicitly permitted by our data protection policy.'),
      "#required"      => true
    ];

    $form['data']['delete'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to delete this export as soon as I no longer require it.'),

      "#required"      => true
    ];

    unset($form['submit']);
    unset($form['actions']['submit']);
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Upload and download'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $validators = ['file_validate_extensions' => ['csv']];
    $file = file_save_upload('file_upload', $validators, FALSE, NULL, FileSystemInterface::EXISTS_REPLACE)[0];

    $importForm = new BatchImportCSVForm();
    $array = $importForm->convertCSVtoArray($file->getFileUri());

    if (!$array) {
      return FALSE;
    }

    $array = array_unique($array, SORT_REGULAR);

    $form_state->set('array', $array);

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->getBatchTitle($form, $form_state))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'exportFinished']);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $rows = $form_state->get('array');
    foreach ($rows as $row) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$row]);
    }
  }

  public function getClubSocFromRow($row) {
    $service = \Drupal::service('su_clubs_societies.service');
    foreach ($row as $field => $data) {
      if ($club_society = $service->getClubSocFromUserInput($data)) {
        return $club_society;
      }
    }
    return NULL;
  }

  public function processBatch(array $row, array &$context) {
    $group = $this->getClubSocFromRow($row);
    if (!$group) {
      return $row;
    }

    if (!isset($context['already_processed'])) {
      $context['already_processed'] = [];
    }

    if (in_array($group->id(), $context['already_processed'])) {
      return;
    }

    $context['already_processed'][] = $group->id();

    // Start with existing data
    $line = $row;
    $line['Club/Society ID'] = $group->id();
    $line['Club/Society Code'] = $group->field_finance_department_code->entity ? $group->field_finance_department_code->entity->getName() : '';

    $members = \Drupal::service('group_membership_record.repository')->get(NULL, $group, GroupRole::load('club_society-member'), NULL, new DateTime(), TRUE);
    foreach ($members as $member) {
      $line['E-mail'] = $member->getUser()->getEmail();
      $line['Member type'] = $member->getUser()->hasRole('member') ? 'Student member' : 'Associate/Visiting';
      $this->addLine($line, $context);
    }
  }
}
