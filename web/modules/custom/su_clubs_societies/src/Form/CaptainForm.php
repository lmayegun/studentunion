<?php

namespace Drupal\su_clubs_societies\Form;

use DateTime;
use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class CaptainForm extends GroupMembershipRecordForm {

  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added captain.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved captain.';

  /**
   * {@inheritdoc}
   */
  protected $allowedGroups = [];

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = ['club_society-captain'];

  /**
   * {@inheritdoc}
   */
  protected $allowedMembershipRecordTypes = ['club_society_committee'];

  /**
   * {@inheritdoc}
   */
  protected $limitToMembers = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $limitToMembersExcludeWithCurrentRole = ['club_society-captain'];

  /**
   * {@inheritdoc}
   */
  protected $fieldsToAllow = ['user_id', 'field_captain_type', 'date_range'];

  /**
   * {@inheritdoc}
   */
  protected $fieldsToDisable = ['date_range'];

  /**
   * {@inheritdoc}
   */
  public $customFormInfo = '';

  /**
   * {@inheritdoc}
   */
  protected $restrictToCurrentGroupParameter = TRUE;

  // @todo prevent creating group membership record if current one already exists
  // BUT we could have a person being a captain with multiple captain types

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = NULL) {
    // Set group from parameter
    $this->group = $group;

    // Custom form message
    $this->customFormInfo = 'A captain added here will be a captain until the end of the normal committee year.';


    $clubSocStartDate = Drupal::config('su_clubs_societies.settings')
      ->get('date_committee_role_starts');
    $clubSocEndDate = new DateTime($clubSocStartDate);
    $clubSocEndDate->setDate(date('Y'), $clubSocEndDate->format('m'), $clubSocEndDate->format('d') - 1);
    $clubSocEndDate->setTime(23, 59, 59);

    // Set default daterange
    $now = new DateTime();
    $yearService = Drupal::getContainer()->get('date_year_filter.year');
    $endOfAcademicYear = $yearService->getYearForDate($now, $clubSocEndDate->format('Y-m-d\TH:i:s'))->end;
    $this->defaultStartDate = $now->format("Y-m-d\TH:i:s");
    $this->defaultEndDate = $endOfAcademicYear->format("Y-m-d\TH:i:s");

    /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

}
