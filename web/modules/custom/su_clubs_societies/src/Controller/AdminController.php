<?php

namespace Drupal\su_clubs_societies\Controller;

use Drupal\system\Controller\SystemController;

class AdminController extends SystemController
{

    /**
     * {@inheritdoc}
     */
    public function overview($link_id = 'su_clubs_societies.admin')
    {
        $build['blocks'] = parent::overview($link_id);
        return $build;
    }
}
