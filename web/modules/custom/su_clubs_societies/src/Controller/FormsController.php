<?php

namespace Drupal\su_clubs_societies\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\group\Entity\GroupInterface;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformInterface;

class FormsController extends ControllerBase implements ContainerInjectionInterface {

  public static function getWebformNodes() {
    $cs_forms = [];
    $forms = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'webform', 'status' => 1]);
    foreach ($forms as $form) {
      $category = $form->get('field_club_society_form_category')->referencedEntities();
      if ($category) {
        $cs_forms[$form->label()] = $form;
      }
    }
    ksort($cs_forms);
    return $cs_forms;
  }

  public static function getGroupReferenceElements($webform) {
    $finalElements = [];
    $elements = $webform->getElementsDecodedAndFlattened();
    foreach ($elements as $key => $element) {
      if (!in_array($element['#type'], ['webform_entity_select', 'webform_entity_autocomplete'])) {
        continue;
      }
      if (!in_array($element['#target_type'], ['group'])) {
        continue;
      }
      $finalElements[$key] = array_merge($element, ['#id' => $key]);
    }
    return $finalElements;
  }

  public static function getBestReferenceElement($webform) {
    $referenceElements = static::getGroupReferenceElements($webform);
    if (count($referenceElements) == 0) {
      return NULL;
    }
    if (count($referenceElements) == 1) {
      return reset($referenceElements);
    }
    foreach ($referenceElements as $key => $element) {
      if (isset($element['#required']) && !$element['#required']) {
        unset($referenceElements[$key]);
      }
    }
    if (count($referenceElements) == 1) {
      return reset($referenceElements);
    }
    foreach ($referenceElements as $key => $element) {
      if (!in_array($key, ['group', 'webform_access_group', 'club_society'])) {
        unset($referenceElements[$key]);
      }
    }
    if (count($referenceElements) == 1) {
      return reset($referenceElements);
    }
    // Cannot identify:
    return NULL;
  }

  public static function getSubmissionsForWebform(GroupInterface $group, WebformInterface $webform) {
    $clubSocReferenceElement = static::getBestReferenceElement($webform);
    if (!$clubSocReferenceElement) {
      return NULL;
    }

    $query = \Drupal::service('webform_query');
    $query->addCondition($clubSocReferenceElement['#id'], $group->id())
      ->setWebform($webform->id());
    $sids = array_column($query->execute(), 'sid');
    $submissions = WebformSubmission::loadMultiple($sids);
    return $submissions;
  }

  function overview(GroupInterface $group) {
    $build = [
      ['note' => [
        '#type' => 'markup',
        '#markup' => '<p>This page lists forms that clubs/societies may need to submit at various points in the year. Make sure to read the form information carefully, or refer to our <a target="_blank" href="/how-to/clubs-and-societies">how-to guides</a> for guidance.</p><p>Click on the form title to fill it out, view previous submissions, etc. Note that we are currently transitioning forms to this platform so submission information may not be available on this page for all forms, but will be available if you click through to the form to complete it.</p>'
      ]]
    ];

    $rows = [];

    $webformNodes = static::getWebformNodes();
    foreach ($webformNodes as $node) {
      $webform = $node->webform->entity;

      $mostRecentSubmission = '';
      $element_id = '';

      if ($webform) {

        // Get submissions:
        $submissions = static::getSubmissionsForWebform($group, $webform);
        if (is_null($submissions)) {
          $viewSubmissionsText = \Drupal::currentUser()->hasPermission('change club society name') ? 'cannot link form to club/society [this message only shown to admin]' : '';
        } else {
          $submissionsCount = count($submissions);
          $viewSubmissionsText = '' . $submissionsCount . ' submission' . ($submissionsCount != 1 ? 's' : '') . ' for this club/society';

          foreach ($submissions as $submission) {
            if ($submission->getChangedTime() > $mostRecentSubmission) {
              $mostRecentSubmission = $submission->getChangedTime();
            }
          }
          $mostRecentSubmission = $mostRecentSubmission ? \Drupal::service('date.formatter')->format($mostRecentSubmission, 'short') : '';
        }
        $viewSubmissionsUrl = $node->toUrl()->toString();

        $element = static::getBestReferenceElement($webform);
        $element_id = $element ? $element['#id'] : '';
      } else {
        $mostRecentSubmission = '';
        $viewSubmissionsUrl = $node->toUrl()->toString();
        $viewSubmissionsText = '';
      }


      $nodeUrl = $node->toUrl();
      if ($element_id) {
        $nodeUrl->setOption('query', [
          $element_id => $group->id(),
        ]);
      }
      $nodeLink = Link::fromTextAndUrl($node->label(), $nodeUrl);

      // $submissionsLink = '<a href="' . $viewSubmissionsUrl . '" target="_blank">' . $viewSubmissionsText . '</a>';
      $submissionsLink = $viewSubmissionsText;

      $category = $node->get('field_club_society_form_category') ? $node->get('field_club_society_form_category')->referencedEntities()[0]->label() : '';

      if (!isset($rows[$category])) {
        $rows[$category] = [];
      }
      $rows[$category][] = [
        'Form' => t($nodeLink->toString()->__toString()),
        'Submissions' => t($submissionsLink),
        'Most recent submission' => t($mostRecentSubmission),
      ];
    }

    if (count($rows) == 0) {
      return [];
    }

    ksort($rows);

    $i = 0;
    foreach ($rows as $category => $categoryRows) {
      $headers = count($categoryRows) > 0 ? array_keys(reset($categoryRows)) : [];
      $build['forms' . $i] =
        [
          '#type' => 'table',
          '#caption' => $category,
          '#header' => $headers,
          '#rows' => $categoryRows,
          '#empty' => t('No forms.'),
        ];
      $i++;
    }

    return $build;
  }
}
