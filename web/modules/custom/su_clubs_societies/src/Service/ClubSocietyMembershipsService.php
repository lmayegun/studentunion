<?php

namespace Drupal\su_clubs_societies\Service;

use DateTime;
use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\GroupMembershipRecordQuery;
use Drupal\user\Entity\User;

/**
 * Implements Class Controller.
 */
class ClubSocietyMembershipsService {

  public function getMemberUidsForGroup($group) {
    // We load this with a direct query to avoid loading the membership entities fully.
    // (Slows performance).
    $connection = Drupal::database();
    $query = $connection->select('group_content_field_data', 'gc');
    $query->condition('gc.type', 'club_society-group_membership');
    $query->condition('gc.gid', $group->id());
    $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
    $query->condition('r.group_roles_target_id', 'club_society-member');
    $query->fields('gc', ['uid']);
    $results = $query->distinct()->execute()->fetchCol();
    array_filter($results);
    $uids = $results;
    return $uids;
  }


  /**
   * @throws \Exception
   */
  public function getMemberUidsForGroupRecordsInCategories(array $category_ids = NULL) {
    $connection = Drupal::database();

    $memberRole = GroupRole::load('club_society-member');

    $queryCurrent = new GroupMembershipRecordQuery();

    $queryCurrent->setRole($memberRole)
      ->setRequireEnabled(TRUE)
      ->setDates(new DateTime());

    if ($category_ids) {
      $gids = Drupal::entityQuery('group')
        ->condition('type', 'club_society')
        ->condition('field_club_category', $category_ids, 'IN')
        ->execute();

      if (count($gids) == 0) {
        return [];
      }

      $queryCurrent->setGroups($gids);
    }

    return $queryCurrent->executeForUids();
  }

  public function getMemberUidsForGroupsInCategories(array $category_ids = NULL) {
    // We load this with a direct query to avoid loading the membership entities.
    // (Slows performance).
    $connection = Drupal::database();
    $query = $connection->select('group_content_field_data', 'gc');
    $query->condition('gc.type', 'club_society-group_membership');

    $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
    $query->condition('r.group_roles_target_id', 'club_society-member');

    if ($category_ids) {
      $query->join('group__field_club_category', 'cat', 'cat.entity_id = gc.gid');
      $query->condition('cat.field_club_category_target_id', $category_ids, 'IN');
    }

    $query->fields('gc', ['uid']);

    $results = $query->distinct()->execute()->fetchCol();

    array_filter($results);
    return $results;
  }

  public function userCanUpgradeMembership($account, Group $group, $returnVariations = FALSE) {
    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = Drupal::service('su_clubs_societies.service');

    $variations = [];
    $account = User::load($account->id());
    $record = $this->userIsCurrentMember($account, $group, TRUE);
    $alreadyPurchasedVariation = $clubSocService->getVariationFromMembershipRecord($record);
    if (!$alreadyPurchasedVariation) {
      Drupal::logger('su_clubs_societies')->notice('No purchased variation.');
      return FALSE;
    }

    $product = $group->field_product->entity;
    foreach ($product->getVariations() as $variation) {
      $alreadyHave = $variation->id() == $alreadyPurchasedVariation->id();

      // Used to be called Remote, some possible names
      $remoteNames = ['Remote', 'Taster', 'Trial'];
      $remote = in_array($variation->attribute_club_society_membershi->entity->label(), $remoteNames);

      $costsMore = $variation->getPrice()
          ->getNumber() >= $alreadyPurchasedVariation->getPrice()->getNumber();
      if (!$alreadyHave && !$remote && $costsMore) {
        if ($returnVariations) {
          $variations[] = $variation;
        }
        else {
          return TRUE;
        }
      }
    }

    if ($returnVariations) {
      return $variations;
    }
    return FALSE;
  }

  public function userIsCurrentMember($account, Group $group, $returnRecord = FALSE) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');

    $account = User::load($account->id());
    $role = GroupRole::load('club_society-member');
    if ($returnRecord) {
      $records = $gmrRepositoryService->get($account, $group, $role, NULL, new DateTime());
      $record = reset($records);
      return $record;
    }
    else {
      return $gmrRepositoryService->currentEnabledInstanceExists($account, $group, $role);
    }
  }

  public function countMembershipsForUser($account) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');
    return $gmrRepositoryService->count($account, NULL, GroupRole::load('club_society-member'), NULL, new DateTime(), TRUE);
  }

  public function userCanCancelMembership($account, Group $group) {
    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = Drupal::service('su_clubs_societies.service');

    $variations = [];
    $account = User::load($account->id());
    $record = $this->userIsCurrentMember($account, $group, TRUE);
    $alreadyPurchasedVariation = $clubSocService->getVariationFromMembershipRecord($record);
    if (!$alreadyPurchasedVariation) {
      Drupal::logger('su_clubs_societies')->notice('No purchased variation.');
      return FALSE;
    }

    if ($alreadyPurchasedVariation->attribute_club_society_membershi->entity->label() == 'Remote') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Create membership from purchased order.
   *
   * @param Order $order
   */
  public function createMembershipFromOrder(Order $order) {
    $orderItems = $order->getItems();
    foreach ($orderItems as $orderItem) {
      $this->createMembershipFromOrderItem($orderItem);
    }
  }

  /**
   * Create membership from order item.
   *
   * @param OrderItem $OrderItem
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createMembershipFromOrderItem(OrderItem $orderItem) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordManager $gmrManager */
    $gmrManager = Drupal::service('group_membership_record.manager');
    /** @var \Drupal\date_year_filter\Service\YearService $yearService */
    $yearService = Drupal::service('date_year_filter.year');
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');

    $order = $orderItem->getOrder();
    $role = GroupRole::load('club_society-member');
    $account = $order->getCustomer();

    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = $orderItem->getPurchasedEntity();
    if (!$variation) {
      return;
    }

    $product = $variation->getProduct();
    if (!$product) {
      return;
    }

    if ($product->bundle() != 'club_society_membership') {
      return;
    }

    // \Drupal::logger('su_clubs_societies')->debug("Order placed for club/soc membership - " . $order->id());

    $clubSoc = $product->get('field_club_society')
      ->referencedEntities() ? $product->get('field_club_society')
      ->referencedEntities()[0] : NULL;
    if (!$clubSoc) {
      Drupal::logger('su_clubs_societies')
        ->warning("For order " . $order->id() . ' could not find club/society for product');
      return;
    }

    $start = gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $order->getPlacedTime());

    // If upgrading from a previous membership, end original membership record and copy start date across to new upgraded one:
    if ($oldRecord = $orderItem->field_record_upgraded_from->entity) {
      $start = gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, strtotime($oldRecord->date_range->value));

      Drupal::logger('su_clubs_societies')
        ->debug("Disabling old membership on upgrade - " . $oldRecord->id());
      $oldRecord->setEndDate($order->getPlacedTime() - (60 * 60 * 24));
      $oldRecord->enabled = FALSE;
      $oldRecord->save();

      // This seems to be necessary to get the record to update:
      Drupal::entityTypeManager()
        ->getStorage('group_membership_record')
        ->resetCache([$oldRecord->id()]);
    }

    // Calculate end of membership
    $endDate = Drupal::config('su_clubs_societies.settings')
      ->get('date_membership_ends');
    $year = $yearService->getYearForDate(DateTime::createFromFormat('U', $order->getPlacedTime()), $endDate);
    $endTime = $year->end->getTimestamp();

    // \Drupal::logger('su_clubs_societies')->debug("End time will be - " . $endTime);

    $values = [
      'date_range' => [
        'value' => $start,
        'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $endTime),
      ],
      'enabled' => TRUE,
      'field_membership_type' => $variation->attribute_club_society_membershi->entity->label(),
    ];

    // \Drupal::logger('su_clubs_societies')->debug("Values? - " . print_r($values, TRUE));

    $currentInstanceExists = $gmrRepositoryService->currentEnabledInstanceExists($account, $clubSoc, $role, NULL, DateTime::createFromFormat('U', $endTime));

    if (!$currentInstanceExists) {
      /** @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $groupMembershipRecord */
      $groupMembershipRecord = $gmrManager->create($account, $clubSoc, $role, $values);
      $groupMembershipRecord->field_order->entity = $order;
      $groupMembershipRecord->save();

      // \Drupal::logger('su_clubs_societies')->debug("Created new membership record - " . $groupMembershipRecord->id());
    }
    else {
      // \Drupal::logger('su_clubs_societies')->debug("Not creating again");
    }
  }

}
