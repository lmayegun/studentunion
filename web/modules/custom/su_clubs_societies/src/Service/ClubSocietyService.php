<?php

namespace Drupal\su_clubs_societies\Service;

use DateTime;
use Drupal;
use Drupal\commerce_product\Entity\Product;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\user\UserInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Implements Class Controller.
 */
class ClubSocietyService {

  /**
   * Constructs a new object.
   */
  public function __construct() {
  }

  public function userHasGroupPermission(UserInterface $account, Group $group = NULL, $permission, $returnGroups = FALSE) {
    $result = FALSE;
    $groups = [];
    if (!$group) {
      $memberships = Drupal::service('group.membership_loader')
        ->loadByUser($account);
      foreach ($memberships as $membership) {
        $group = $membership->getGroup();
        if ($group->bundle() == 'club_society') {
          if (GroupAccessResult::allowedIfHasGroupPermissions($group, $account, [$permission])
            ->isAllowed()) {
            if ($returnGroups) {
              $groups[] = $group;
            }
            else {
              $result = TRUE;
              break;
            }
          }
        }
      }
    }
    elseif ($group && $group->bundle() === 'club_society') {
      $membership = Drupal::service('group.membership_loader')
        ->load($group, $account);
      $result = GroupAccessResult::allowedIfHasGroupPermissions($group, $account, [$permission])
        ->isAllowed();
      if ($result) {
        $groups[] = $group;
      }
    }
    return $returnGroups ? $groups : $result;
  }

  public function userHasGroupRole(UserInterface $account, Group $group = NULL, GroupRole $role) {
    if (!$group) {
      $memberships = Drupal::service('group.membership_loader')
        ->loadByUser($account, $role->id());
      foreach ($memberships as $membership) {
        if ($membership->getGroup()->bundle() == 'club_society') {
          return TRUE;
        }
      }
    }
    elseif ($group && $group->bundle() === 'club_society') {
      $membership = Drupal::service('group.membership_loader')
        ->load($group, $account);
      return $membership ? TRUE : FALSE;
    }
    return FALSE;
  }

  /**
   * Get club/soc from user input (e.g. finance code, name, ID)
   *
   * $string has to be a COMPLETE identifier, this is not for searching partial
   * strings
   */
  public function getClubSocFromUserInput($string) {
    $possibleFields = [];

    if (strlen($string) == 3) {
      if ($clubSoc = $this->getClubSocByDept($string)) {
        return $clubSoc;
      }
    }

    $possibleFields['id'] = $string;
    $possibleFields['label'] = $string;

    foreach ($possibleFields as $field => $value) {
      $results = Drupal::entityQuery('group')
        ->condition('type', 'club_society')
        ->condition($field, $value, '=')
        ->execute();
      if (count($results) > 0) {
        return Group::load(reset($results));
      }
    }

    return NULL;
  }

  public function getClubSocByDept($dept) {
    $terms = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $dept, 'vid' => 'finance_department']);
    $term = !empty($terms) ? reset($terms) : FALSE;
    if ($term) {
      $gids = Drupal::entityQuery('group')
        ->condition('field_finance_department_code', $term->id())
        ->execute();
      $gid = reset($gids);
      return Group::load($gid);
    }
    return NULL;
  }

  public function getVariationFromMembershipRecord(GroupMembershipRecord $record) {
    $order = $record->field_order->entity;
    if ($order) {
      $items = $order->getItems();
    }
    else {
      $product = $record->getGroup()->field_product->entity;
      if (!$product) {
        return NULL;
      }
      $orderItems = su_shop_user_has_purchased_variation_of_product($record->getUser(), $product, TRUE);
      $items = [];
      foreach ($orderItems as $item) {
        $createdAroundRecord = $item->getCreatedTime() >= (strtotime($record->date_range->value) - (60 * 60 * 5));
        $createdWithinRecord = $item->getCreatedTime() <= (strtotime($record->date_range->value) + (60 * 60 * 5));
        if ($createdAroundRecord && $createdWithinRecord) {
          $items[] = $item;
        }
      }
    }
    if (!$items) {
      return NULL;
    }
    foreach ($items as $item) {
      if ($item->bundle() == 'membership') {
        $variation = $item->getPurchasedEntity();
        $product = $variation->getProduct();
        $group = $product->field_club_society->entity;
        if ($group->id() == $record->getGroup()->id()) {
          return $variation;
        }
      }
    }
    return NULL;
  }

  public function checkOnSaleDates() {
    $cid = __METHOD__;
    if ($item = Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $value = FALSE;

    $offSetting = DateTime::createFromFormat('Y-m-d', Drupal::config('su_clubs_societies.settings')
      ->get('date_off_sale'));
    $onSetting = DateTime::createFromFormat('Y-m-d', Drupal::config('su_clubs_societies.settings')
      ->get('date_on_sale'));
    $now = DateTime::createFromFormat('U', strtotime('now'));

    /** @var \Drupal\date_year_filter\Service\YearService $yearService */
    $yearService = Drupal::service('date_year_filter.year');
    $yearObject = $yearService->getYearForDate($now, $onSetting);
    $startYear = $yearObject->start->format('Y');
    $endYear = $yearObject->end->format('Y');
    $on = new DateTime($onSetting->format($startYear . "-m-d"));
    $off = new DateTime($offSetting->format($endYear . "-m-d"));

    // echo $on->format('Y-m-d') . ' on, ' . $off->format('Y-m-d') . ' off';
    // echo PHP_EOL . ($on <= $now ? 'ON <= NOW' : 'ON > NOW');
    // echo PHP_EOL . ($off >= $now ? 'OFF >= NOW' : 'OFF < NOW');
    if ($on <= $now && $off >= $now) {
      $value = TRUE;
    }

    Drupal::cache()
      ->set($cid, $value, strtotime('midnight tomorrow'), ['su_clubs_societies.settings']);
    return $value;
  }

  public function createProduct(GroupInterface $group) {
    if ($group->bundle() != 'club_society') {
      return;
    }

    // Standardise product title names
    $title = $group->label() . ' membership';

    // Check if club/society already has a product referenced
    $groupProduct = NULL;
    $product = $group->field_product ? $group->field_product->entity : NULL;
    if ($product) {
      $groupProduct = $product;
    }

    if ($groupProduct) {
      $groupProduct->set('title', $title);
      $groupProduct->field_club_society->entity = $group;
      $groupProduct->save();
      return TRUE;
    }

    // See if a product exists but not linked to group
    $query = Drupal::entityQuery('commerce_product');
    $query->condition('type', "club_society_membership");
    $query->condition('field_club_society', $group->id());
    $productIDs = $query->execute();
    $products = Product::loadMultiple($productIDs);

    if (count($products) > 0) {
      $product = reset($products);
      $product->set('title', $title);
    }
    else {
      // Otherwise create a linked product
      $product = Product::create([
        'title' => $title,
        'type' => 'club_society_membership',
        'field_product_restrictions' => [
          [
            'target_plugin_id' => 'club_society_can_join_rules',
            'target_plugin_configuration' => [],
          ],
        ],
      ], 'commerce_product', 'club_society_membership');
    }
    $product->field_club_society->entity = $group;
    $product->setStoreIds([1]);
    $product->save();

    $group->field_product->entity = $product;
    $group->save();
    return $product;
  }

  public function checkOnSaleForGroup($group) {
    if (!$group) {
      return FALSE;
    }
    if ($group->bundle() !== 'club_society') {
      return FALSE;
    }
    $products = $group->get('field_product')->referencedEntities();
    if (!$products || !$products[0]) {
      return FALSE;
    }
    $groupProduct = $products[0];
    if ($groupProduct->status->value == 0) {
      return FALSE;
    }
    if (count($groupProduct->getVariations()) == 0) {
      return FALSE;
    }

    return TRUE;
  }

  public function registeredForYear($group, $yearObject) {
    $registered = FALSE;

    $registrationStarts = DateTime::createFromFormat('Y-m-d', Drupal::config('su_clubs_societies.settings')
      ->get('date_reregistration_starts'));
    $now = DateTime::createFromFormat('U', strtotime('now'));

    /** @var \Drupal\date_year_filter\Service\YearService $yearService */
    $yearService = Drupal::service('date_year_filter.year');
    $yearObject = $yearService->getYearForDate($now, $registrationStarts);

    $registrationWebform = 'webform_130664';
    $clubSocReferenceElement = 'webform_access_group';
    $workflowElement = 'workflow';
    $targetWorkflowState = 'approved';

    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = Webform::load($registrationWebform);

    if (!$group) {
      return FALSE;
    }

    $query = Drupal::service('webform_query');
    $query->addCondition($clubSocReferenceElement, $group->id())
      ->setWebform($registrationWebform);
    $sids = array_column($query->execute(), 'sid');
    $submissions = WebformSubmission::loadMultiple($sids);

    foreach ($submissions as $submission) {
      $workflowValues = $submission->getElementOriginalData($workflowElement);
      $state = $workflowValues['workflow_state'];
      $approved = ($state == $targetWorkflowState);

      $created = DateTime::createFromFormat('U', $submission->getCreatedTime());
      $thisYear = ($created >= $yearObject->start) && ($created < $yearObject->end);

      if ($approved && $thisYear) {
        $registered = TRUE;
        break;
      }
    }

    return $registered;
  }

  public function userApprovedForUnder18s($account, $group) {
    $webform = Webform::load('webform_125592');
    if (!$webform) {
      return FALSE;
    }

    $submissions = webform_workflows_element_queries_get_submission_with_state($webform, 'workflow', 'approved', $account->id());

    if ($submissions) {
      foreach ($submissions as $submission) {
        // Must be for this club/soc:
        $clubsSocs = $submission->getElementData('clubs_societies');
        if (!in_array($group->id(), $clubsSocs)) {
          continue;
        }

        return TRUE;
      }
    }
    return FALSE;
  }

}
