<?php

namespace Drupal\simpleads\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\NumericFilter;
use Drupal\Component\Utility\Html;

/**
 * SimpleAds base entity reference exposed filter.
 */
class SimpleAdsViewFilter extends NumericFilter {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    if ($this->value != 'All') {
      $field = "$this->tableAlias.$this->realField";

      $info = $this->operators();
      if (!empty($info[$this->operator]['method'])) {
        $this->{$info[$this->operator]['method']}($field);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple($field) {
    if ($this->value['value'] != 'All') {
      $this->query->addWhere($this->options['group'], $field, $this->value['value'], $this->operator);
    }
  }

  /**
   * Get select field option values.
   */
  protected function getReferenceOptions($type) {
    $options = [];
    $entities = \Drupal::entityTypeManager()
      ->getStorage($type)
      ->loadByProperties(['status' => TRUE]);
    foreach ($entities as $entity) {
      $options[$entity->id()] = Html::decodeEntities($entity->getName());
    }
    return $options;
  }

}
