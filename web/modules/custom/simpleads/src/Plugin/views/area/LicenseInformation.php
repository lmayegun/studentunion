<?php

namespace Drupal\simpleads\Plugin\views\area;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\simpleads\Form\LicenseForm;

/**
 * License information.
 *
 * @ingroup simpleads
 *
 * @ViewsArea("simpleads_license_info")
 */
class LicenseInformation extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $config = \Drupal::config(LicenseForm::CONFIG_NAME);
    if (empty($config->get('license_email')) || empty($config->get('license_number'))) {
      return [
        '#type' => 'inline_template',
        '#template' => '<div class="simpleads-msg require-license"><strong>Please <a href="{{ link }}" class="use-ajax"  data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;525&quot;}" data-drupal-link-system-path="{{ link|trim(\'/\', \'left\') }}">provide license information</a> in order to be able to get module updates.</strong></div>',
        '#context' => [
          'link' => Url::fromRoute('simpleads.license')->toString(),
        ],
        '#weight' => -200,
        '#attached' => [
          'library' => ['simpleads/simpleads.admin']
        ],
      ];
    }
  }

}
