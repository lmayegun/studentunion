<?php

namespace Drupal\simpleads\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Developer contact information.
 *
 * @ingroup simpleads
 *
 * @ViewsArea("simpleads_contact_developer")
 */
class ContactDeveloper extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    return [
      '#type' => 'inline_template',
      '#template' => '<div class="simpleads-info-msg">If you have questions or you would like customizations please <a href="https://downloads.minnur.com/contact-developer" target="_blank">contact developer</a>.</div>',
      '#weight' => 100,
      '#attached' => [
        'library' => ['simpleads/simpleads.admin']
      ],
    ];
  }

}
