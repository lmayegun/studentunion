<?php

namespace Drupal\su_finance\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the finance transaction entity.
 *
 * @ingroup Students' Union UCL
 *
 * @ContentEntityType(
 *   id = "finance_supplier",
 *   label = @Translation("Finance supplier"),
 *   label_singular = @Translation("finance supplier"),
 *   label_plural = @Translation("finance suppliers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count finance supplier",
 *     plural = "@count finance suppliers"
 *   ),
 *   admin_permission = "administer finance suppliers",
 *   base_table = "finance_suppliers",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class FinanceSupplier extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the group was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['account_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Account Code'));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Supplier Name'));

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('E-mail'));

    return $fields;
  }

  public static function findByCode($code) {
    $query = \Drupal::entityQuery('finance_supplier');
    $query->condition('account_code', $code);
    $ids = $query->execute();
    return FinanceSupplier::loadMultiple($ids);
  }
}
