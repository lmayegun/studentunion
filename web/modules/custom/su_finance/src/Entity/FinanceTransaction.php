<?php

namespace Drupal\su_finance\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the finance transaction entity.
 *
 * @ingroup Students' Union UCL
 *
 * @ContentEntityType(
 *   id = "finance_transaction",
 *   label = @Translation("Finance transaction"),
 *   label_singular = @Translation("finance transaction"),
 *   label_plural = @Translation("finance transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count finance transaction",
 *     plural = "@count finance transactions"
 *   ),
 *   admin_permission = "administer finance transactions",
 *   base_table = "finance_transactions",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class FinanceTransaction extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the group was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['dept'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('DEPT'))
      ->setSetting('max_length', 10);

    $fields['cc'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('CC'))
      ->setSetting('max_length', 10);

    $fields['gl'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('GLCode'))
      ->setSetting('max_length', 10);

    $fields['description'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('description'))
      ->setSetting('max_length', 255);

    $fields['gl_description'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('gl_description'))
      ->setSetting('max_length', 255);

    $fields['transaction_reference'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('transaction_reference'))
      ->setSetting('max_length', 255);

    $fields['invoice_reference'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('invoice_reference'))
      ->setSetting('max_length', 255);

    $fields['period'] =  BaseFieldDefinition::create('integer')
      ->setLabel(t('period'));

    $fields['total'] =  BaseFieldDefinition::create('decimal')
      ->setLabel(t('total'));

    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date'))
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE);

    $fields['account_code'] =  BaseFieldDefinition::create('string')
      ->setLabel(t('account_code'))
      ->setSetting('max_length', 255);

    return $fields;
  }

  public static function findByDeptAndDates($dept, $start, $end) {
    $query = \Drupal::entityQuery('finance_transaction');
    $query->condition('date', $start->getTimestamp(), ">=");
    $query->condition('date', $end->getTimestamp(), "<=");
    $query->condition('dept', $dept);
    $transactionIds = $query->execute();
    return FinanceTransaction::loadMultiple($transactionIds);
  }
}
