<?php

namespace Drupal\su_finance\Service;

/*

Functions to produce a report for SU finance with one line for each of:
- Sale (order item original price)
- Refund (order item)
- then for each adjustment (promotion, shipping, etc)

This is not available in views because unlike in D7, adjustments aren't their own order items / line items,
so can't be reported correctly

*/

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShippingMethod;
use Drupal\commerce_shipping\Plugin\Commerce\TaxType\Shipping;
use Drupal\taxonomy\Entity\Term;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

class   CommerceReport {

  function getBlankLine() {
    return [
      'Date'            => '',
      'Type'            => '',
      'Order status'    => '',
      'Title'           => '',
      'Order ID'        => '',
      'Product ID'      => '',
      'Line item ID'    => '',
      'CC'              => '',
      'DEPT'            => '',
      'GL'              => '',
      'Unit price'      => '',
      'Cost price'      => '',
      'Quantity'        => 1,
      'Net'             => '',
      'Tax %'           => NULL,
      'VAT Code'        => '',
      'Tax amount'      => NULL,
      'Total'           => '',
      'Payment gateway' => '',
      'Customer name'   => '',
      'Customer e-mail' => '',
    ];
  }

  function loadOrderIdsPosRefunded($date_start, $date_end, $date_type, $statuses, $exclude_free_orders, $payment_gateways) {
    $query = \Drupal::database()->select('commerce_order', 'o');

    $query->condition('type', 'pos');
    $query->condition('total_price__number', '0.00', '<');

    // Filter by date
    $query->condition('completed', strtotime($date_start), '>=');
    $query->condition('completed', strtotime($date_end), '<=');

    $query->fields('o', ['order_id']);

    $order_ids = $query->distinct()->execute()->fetchCol();

    return $order_ids;
  }

  function loadOrderIds($date_start, $date_end, $date_type, $statuses, $exclude_free_orders, $payment_gateways) {
    $query = \Drupal::database()->select('commerce_order', 'o');

    // Filter by status

    // Filter by date
    $payment = FALSE;
    if ($date_type === 'payment') {
      $payment = TRUE;
      $query->join('commerce_payment', 'payment', 'payment.order_id = o.order_id');

      // Use authorized date, or if that's not set the completed date:
      $authorised = $query->andConditionGroup()
        ->condition('payment.authorized', strtotime($date_start), '>=')
        ->condition('payment.authorized', strtotime($date_end), '<=');

      $authorisedBlank = $query->andConditionGroup()
        ->condition('payment.authorized', NULL, 'IS NULL')
        ->condition('payment.completed', strtotime($date_start), '>=')
        ->condition('payment.completed', strtotime($date_end), '<=');

      $or = $query->orConditionGroup()
        ->condition($authorised)
        ->condition($authorisedBlank);

      $query->condition($or);
    } elseif ($date_type === 'payment_completed') {
      $payment = TRUE;
      $query->join('commerce_payment', 'payment', 'payment.order_id = o.order_id');
      $query->condition('payment.completed', strtotime($date_start), '>=');
      $query->condition('payment.completed', strtotime($date_end), '<=');
    } else {
      $query->condition($date_type, strtotime($date_start), '>=');
      $query->condition($date_type, strtotime($date_end), '<=');
    }

    if ($exclude_free_orders) {
      $query->condition('total_price__number', '0.00', '>');
    };

    if ($payment && $payment_gateways && count($payment_gateways) > 0) {
      $query->condition('payment.payment_gateway', $payment_gateways, 'IN');
    }

    $query->fields('o', ['order_id']);

    $order_ids = $query->distinct()->execute()->fetchCol();

    return $order_ids;
  }

  function loadRefundedOrderItemIds($date_start, $date_end, $date_type, $statuses, $exclude_free_orders, $payment_gateways) {
    $fields = [
      'field_refunded',
      'refunded',
    ];

    $order_item_ids = [];
    foreach ($fields as $field) {

      $query = \Drupal::entityQuery('commerce_order_item');
      $query->condition($field, TRUE);

      // Filter by date
      $query->condition($field . '_date', date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, strtotime($date_start)), '>=');
      $query->condition($field . '_date', date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, strtotime($date_end)), '<=');

      $order_item_ids = array_merge($order_item_ids, $query->execute());
    }

    return $order_item_ids;
  }

  function getPaymentForOrder($order) {
    $query = \Drupal::entityQuery('commerce_payment')
      ->condition('order_id', $order->id())
      ->execute();

    if (!empty($query)) {
      $payments = \Drupal::entityTypeManager()
        ->getStorage('commerce_payment')
        ->loadMultiple(array_keys($query));

      // in my use case, always only one payment per order, should be refined if it's not the case
      foreach ($payments as $payment) {
        return $payment;
      }
    }
  }


  /**
   * Function to standardise adding lines
   *
   * Defaults to a blank line, then adds what it can from arguments it's given
   */
  function makeLine($report_type, string $type, $order = NULL, $orderItem = NULL, $product = NULL, $overrides = []) {
    if ($report_type == 'exchequer') {
      return $this->makeLineExchequer($type, $order, $orderItem, $product, $overrides);
    } elseif ($report_type == 'online_reconciliation') {
      // 'Date'            => '',
      // 'Order status'    => '',
      // 'Order ID'        => '',
      // 'Product ID'      => '',
      // 'Line item ID'    => '',
      // 'CC'              => '',
      // 'DEPT'            => '',
      // 'GL'              => '',
      // 'Unit price'      => '',
      // 'Cost price'      => '',
      // 'Quantity'        => 1,
      // 'Total'           => '',
      // 'Customer name'   => '',
      // 'User name'       => '',
    }

    $array = $this->getBlankLine();

    $array['Type'] = $type;

    // Order specific field
    if ($order) {
      $customer = $order->getCustomer();
      $billingProfile = $order->getBillingProfile() ? $order->getBillingProfile()->get('address')->getValue() : NULL;

      $array['Order ID'] = $order->id();
      $array['Order status'] = $order->getState()->getLabel();

      // Date is either date created, or date paid
      // To help reconcile with Worldnet as that will be by payment date
      $array['Date'] = $this->getDate($order);

      $array['Payment gateway'] = $order->get('payment_gateway')->first() ? $order->get('payment_gateway')->first()->entity->label() : '';
      if (!$array['Payment gateway'] && $order->bundle() == 'pos') {
        $array['Payment gateway'] = 'Point of sale';
      }

      $array['Customer name'] = $customer->get('field_first_name')->getString() . ' ' . $customer->get('field_last_name')->getString();
      $array['Billing name'] = $billingProfile ? $billingProfile[0]['given_name'] . ' ' . $billingProfile[0]['family_name'] : 'Not provided';
      $array['Customer e-mail'] = $order->getEmail();
    }

    /**
     * @var OrderItem $orderItem
     */
    if ($orderItem) {
      // $array['Line item ID'] = $orderItem->id();
      $array['Title'] = $orderItem->getTitle();
      $array['Quantity'] = $orderItem->getQuantity();
      $array['Unit price'] = $orderItem->getUnitPrice()->getNumber();
      /**
       * @var ProductVariation $variation
       */
      $variation = $orderItem->getPurchasedEntity();
      if ($variation) {
        $array['Product ID'] = $variation->getProduct()->id();
      }
      if ($variation && $variation->get('type')->getString() === 'clothing') {
        $costPriceField = $orderItem->getPurchasedEntity()->get('field_cost_price');
        $array['Cost price'] = $costPriceField && $costPriceField->first() ? $orderItem->getPurchasedEntity()->get('field_cost_price')->first()->toPrice()->getNumber() : 'unknown';
      }

      $refundedAmount = 0;
      $refundedQuantity = 0;
      $refundedDate = 0;

      // Renaming fields - temporary code
      if ($orderItem->hasField('field_refunded_amount') && $orderItem->field_refunded->value == 1) {
        $refundType = 'old';
      } elseif ($orderItem->getTotalPrice()->getNumber() < 0 && $orderItem->getOrder()->bundle() == 'pos') {
        $refundType = 'pos';
        $type = 'Refund';
      } else {
        $refundType = 'current';
      }

      if ($refundType == 'old') {
        // @todo remove 'old' code and fields after 2021-22 year
        $refunded = $orderItem->field_refunded->value == 1;
        $amountValue = $orderItem->field_refunded_amount->getValue();
        $refundedAmount = $amountValue && $amountValue[0]['number'] ? $orderItem->get('field_refunded_amount')->first()->toPrice()->getNumber() : 0;
        if ($refunded) {
          $refundedQuantity =
            $orderItem->field_refunded_quantity->value ? $orderItem->field_refunded_quantity->value : '';
          $refundedDate =
            $orderItem->field_refunded_date->value ? \Drupal::service('date.formatter')->format(strtotime($orderItem->field_refunded_date->value), 'custom', 'd/m/Y') : '';
        }
      } elseif ($refundType == 'pos') {
        $refunded = TRUE;
        $refundedAmount = abs($orderItem->getUnitPrice()->getNumber());
        $refundedQuantity = $orderItem->getQuantity();
        $refundedDate = \Drupal::service('date.formatter')->format($orderItem->getCreatedTime(), 'custom', 'd/m/Y');
      } else {
        $refunded = $orderItem->refunded->value == 1;
        if ($orderItem->get('refunded_amount') && !$orderItem->get('refunded_amount')->isEmpty()) {
          $refundedAmount = $orderItem->get('refunded_amount')->first()->toPrice()->getNumber();
        }
        // $hasAmount = $amountValue && $amountValue[0]['number'];
        // $refundedAmount = $hasAmount ? $orderItem->get('refunded_amount')->first()->toPrice()->getNumber() : 0;
        if ($refunded) {
          $refundedQuantity =
            $orderItem->refunded_quantity->value ? $orderItem->refunded_quantity->value : '';
          $refundedDate =
            $orderItem->refunded_date->value ? \Drupal::service('date.formatter')->format(strtotime($orderItem->refunded_date->value), 'custom', 'd/m/Y') : '';
        }
      }

      if ($type == 'Refund') {
        $array['Total'] = $refundedAmount;
        $array['Quantity'] = $refundedQuantity;
        $array['Date'] = $refundedDate;
      } else {
        if ($refunded) {
          $array['Quantity'] = $refundedQuantity;
        }
        $array['Total'] = $orderItem->getAdjustedTotalPrice()->getNumber() + $refundedAmount;
      }
    }

    if ($product) {
      if ($product->bundle() === 'club_society_membership') {
        $group = $product->field_club_society->entity;
        $array['GL'] = '69308';
        $array['CC'] = $this->getTaxonomyName($group, 'field_finance_cost_code');
        $array['DEPT'] = $this->getTaxonomyName($group, 'field_finance_department_code');
      } else {
        $array['GL'] = $this->getTaxonomyName($product, 'field_finance_gl_code');
        $array['CC'] = $this->getTaxonomyName($product, 'field_finance_cost_centre');
        $array['DEPT'] = $this->getTaxonomyName($product, 'field_finance_department');
        if (!$array['GL']) {
          $array['GL'] = $this->calculateGlCode($product, $array);
        }
      }
      if ($product->hasField('field_cost_price')) {
        $array['Cost price'] = $product->get('field_cost_price')->value()->getNumber();
      }

      // Get tax category and calculate tax proportion accordingly
      $this->calculateTaxProduct($product, $array);
    }

    if ($type == 'Refund') {
      $array['Net'] = -1 * abs($array['Net']);
      $array['Tax amount'] = -1 * abs($array['Tax amount']);
      $array['Total'] = -1 * abs($array['Total']);
    }

    foreach ($overrides as $key => $value) {
      $array[$key] = $value;
    }

    if ($array['Total'] == 0) {
      $array['Payment gateway'] = 'No payment';
    }

    return $array;
  }

  function calculateGlCode($product, $row = []) {
    if ($product->hasField('field_event_owner_group') && $group = $product->field_event_owner_group->entity) {
      // Club society events
      if ($group->bundle() == 'club_society') {
        return '69301';
      }
    }
    if ($product->bundle() == 'event') {
      if (isset($row['DEPT'])) {
        switch ($row['DEPT']) {
          case 'PAC':
            return '62100';

          case 'MKR':
            return '62125';

          case 'SOD':
            return '62100';
        }
      }
    }
    return '';
  }

  /**
   * Function to standardise adding lines
   *
   * Defaults to a blank line, then adds what it can from arguments it's given
   */
  function makeLineExchequer(string $type, $order = NULL, $orderItem = NULL, $product = NULL, $overrides = []) {
    $array = $this->makeLine('', $type, $order, $orderItem, $product, $overrides);

    $array = [
      'Transdate'       => $array['Date'],
      'cc'              => $array['CC'],
      'dept'            => $array['DEPT'],
      'nomcod'          => $array['GL'],
      'Vatcod'          => $array['VAT Code'],
      'Desc'            => $array['Order ID'] . '|' . $array['Title'],
      'Netamt'          => $array['Net'],
      'Vat'             => $array['Net'],
      'Amt'             => $array['Tax amount'],
      'Gross'           => $array['Total'],
      'Type'            => $array['Type'],
      'Payment gateway' => $array['Payment gateway'],
    ];

    return $array;
  }

  function getLinesForOrderItem(OrderItem $orderItem, $report_type = '', $type = 'Sale') {
    $lines = [];
    $purchasedEntity = $orderItem->getPurchasedEntity();
    if (!$purchasedEntity) {
      $lines[] = $this->makeLine($report_type, 'Sale of deleted product', $orderItem->getOrder(), $orderItem, NULL);
    } else {
      $product = $purchasedEntity->getProduct();

      $lines[] = $this->makeLine($report_type, $type, $orderItem->getOrder(), $orderItem, $product);
    }
    return $lines;
  }

  function getLinesForOrder(Order $order, $report_type = '') {
    $lines = [];

    // Get order items, excluding refunds as they are dated differently.
    $orderItems = $order->getItems();
    foreach ($orderItems as $item) {
      $lines = array_merge($lines, $this->getLinesForOrderItem($item, $report_type, 'Sale'));
    }

    /**
     * Get adjustments
     * $order->collectAdjustments(); That gets you both order and order item adjustments.
     *
     * Adjustment types
     * commerce_shipping/commerce_shipping.commerce_adjustment_types.yml
     * commerce/modules/order/commerce_order.commerce_adjustment_types.yml
     *
     *
     * @var Adjustment     $adjustment
     * @var Promotion      $promotion
     * @var Shipment       $shipment
     * @var ShippingMethod $shipping_method
     */
    $adjustments = $order->getAdjustments(); // $order->collectAdjustments(); That gets you both order and order item adjustments. but we don't want these for this report as they get totalled for the order
    foreach ($adjustments as $adjustment) {
      $title = FALSE;
      if ($adjustment->getType() === 'promotion' || $adjustment->getType() === 'shipping_promotion') {
        $promotion = Promotion::load($adjustment->getSourceId());
        if ($promotion) {
          $title = $promotion->getName();
          $additionalFields['CC'] = $this->getTaxonomyName($promotion, 'finance_cost_centre');
          $additionalFields['DEPT'] = $this->getTaxonomyName($promotion, 'finance_department');
          $additionalFields['GL'] = $this->getTaxonomyName($promotion, 'finance_gl_code');
        } else {
          $title = 'Deleted promotion' . ($adjustment->getSourceId() ? ' - ' . $adjustment->getSourceId() : '');
        }
        $label = 'Promotion';
      } else if ($adjustment->getType() === 'shipping') {
        $label = 'Shipping';
        $shipment = Shipment::load($adjustment->getSourceId());
        $shipping_method = ShippingMethod::load($shipment->getShippingMethodId());
        $title = $shipping_method ? $shipping_method->getName() : 'Deleted shipping method';

        $additionalFields['CC'] = 'CO';
        $additionalFields['DEPT'] = 'NEC';
        $additionalFields['GL'] = '64283';

        $this->calculateTaxShipping($adjustment, $array);
      } else {
        $label = 'Manual adjustment';

        // For point of sale assume all adjustments are out of the shop budget
        if ($order->bundle() == 'pos') {
          $additionalFields['CC'] = 'CO';
          $additionalFields['DEPT'] = 'NEC';
          $additionalFields['GL'] = '64283';
        }
      }

      $additionalFields['Total'] = $adjustment->getAmount()->getNumber();
      $additionalFields['Title'] = $title ? $title : $adjustment->getLabel();

      $lines[] = $this->makeLine($report_type, $label, $order, NULL, NULL, $additionalFields);
    }

    return $lines;
  }

  public function getTaxonomyName($entity, $field) {
    // !is_null($group->field_finance_cost_code->entity) ? $group->field_finance_cost_code->entity->getName() : ''
    if ($entity->get($field)->getString()) {
      $term = Term::load($entity->get($field)->getString());
      if (!$term) {
        return '';
      }
      return $term->getName();
    } else {
      return '';
    }
  }

  public function calculateTaxShipping($shippingAdjustment, &$array) {
    $array['VAT Code'] = 'S';
    $array['Tax %'] = 20;

    $this->calculateTax($array);
  }

  public function calculateTaxProduct($product, &$array) {
    // If there's a field_tax_category reference to a VAT code, use that
    if ($product->hasField('field_tax_category') && isset($product->get('field_tax_category')->referencedEntities()[0])) {
      $taxCategory = $product->get('field_tax_category')->referencedEntities()[0];
      $array['VAT Code'] = $taxCategory->field_vat_code->value;
    } else {
      // Otherwise, by product type:
      switch ($product->bundle()) {
        case 'clothing':
        case 'associate_visiting_membership':
        case 'default':
        case 'gym_membership':
        case 'jobshop_advertisement':
          $array['VAT Code'] = 'S';
          break;

        default:
          $array['VAT Code'] = 'Exempt';
          break;
      }
    }

    $this->calculateTax($array);
  }

  public function calculateTax(&$array) {
    if ($array['VAT Code'] && $array['VAT Code'] != '') {
      switch ($array['VAT Code']) {
        case 'S':
          $array['Tax %'] = 20;
          break;

        case 'R':
          $array['Tax %'] = 5;
          break;

        case 'Z':
        case 'Exempt':
        case 'No VAT':
          $array['Tax %'] = 0;
          break;

        default:
          $array['Tax %'] = NULL;
          break;
      }
    }

    if (isset($array['Total']) && $array['Tax %'] && $array['Tax %'] != '') {
      $proportion = (1 / (1 + ($array['Tax %'] / 100)));
      $array['Net'] = round($proportion * $array['Total'], 2);
      $array['Tax amount'] = round($array['Total'] - $array['Net'], 2);
    } else {
      $array['Net'] = '';
      $array['Tax amount'] = '';
    }
  }

  function getDate($order) {
    $format = 'd/m/Y';
    // $array['Date updated'] = \Drupal::service('date.formatter')->format($order->getChangedTime()), 'custom', 'd/m/Y';
    $payment = $this->getPaymentForOrder($order);
    if ($payment && $payment->getCompletedTime()) {
      return \Drupal::service('date.formatter')->format($payment->getCompletedTime(), 'custom', $format);
    }
    return \Drupal::service('date.formatter')->format($order->getCreatedTime(), 'custom', $format);
  }

  function getFinanceDetails($product) {
    $array = [];
    if ($product->bundle() === 'club_society_membership') {
      $group = $product->field_club_society->entity;
      $array['GL'] = !is_null($group->field_finance_general_ledger_cod->entity) ? $group->field_finance_general_ledger_cod->entity->getName() : '';
      $array['CC'] = !is_null($group->field_finance_cost_code->entity) ? $group->field_finance_cost_code->entity->getName() : '';
      $array['DEPT'] = !is_null($group->field_finance_department_code->entity) ? $group->field_finance_department_code->entity->getName() : '';
    } else {
      $array['GL'] = $product->field_finance_gl_code->entity->getName();
      $array['CC'] = $product->field_finance_cost_centre->entity->getName();
      $array['DEPT'] = $product->field_finance_department->entity->getName();
    }
    return $array;
  }
}
