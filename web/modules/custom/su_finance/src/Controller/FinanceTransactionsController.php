<?php

namespace Drupal\su_finance\Controller;

use DateTime;
use Drupal\Core\Controller\ControllerBase;
use Drupal\su_finance\Entity\FinanceTransaction;
use League\Csv\Writer;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class FinanceTransactionsController extends ControllerBase {

  /**
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function viewGroupPage($group) {
    $build = [];

    $fieldExists = $group->field_finance_department_code;
    $dept = $fieldExists && $group->field_finance_department_code->entity ? $group->field_finance_department_code->entity->getName() : false;
    if (!$dept) {
      $this->messenger()->addError($this->t('Group does not have department code set, so it will not have finance transactions associated with it.'));

      $response = new RedirectResponse($_SERVER['HTTP_REFERER'] ?? "/");
      return $response;
    }

    $yearService = \Drupal::service('date_year_filter.year');

    // $year = DateTime::createFromFormat('U', strtotime('-1 year'));
    $year = new \DateTime();

    $yearObject = $yearService->getYearForDate($year, '2021-08-01');
    $transactions     = FinanceTransaction::findByDeptAndDates($dept, $yearObject->start, $yearObject->end);
    $grantAllocations = []; // $this->grantAllocationRepository->findByClub($group);
    $finance          = $this->getFinanceTransactionsBalances($transactions, $grantAllocations);
    $descriptions     = $this->getFinanceTransactionsGLDescs($transactions, $grantAllocations, FALSE);

    $build['finances'] = [
      '#theme'            => 'group_finances',
      '#group'            => $group,
      '#finance'          => $finance,
      '#descIncome'       => $descriptions['income'],
      '#descExpenditure'  => $descriptions['expenditure']
    ];

    return $build;
  }

  public function getFinanceTransactionsBalances($transactions, $grantAllocations) {
    $results = [
      'grant'       => [
        'expenditure' => 0,
        'income'      => 0,
        'total'       => 0,
        'glcodes'     => [],
      ],
      'nongrant'    => [
        'expenditure' => 0,
        'income'      => 0,
        'total'       => 0,
        'glcodes'     => [],
      ],
      'totals'      => [
        'expenditure' => 0,
        'income'      => 0,
        'total'       => 0,
        'glcodes'     => [],
      ],
      'lastUpdated' => NULL,
      'count'       => 0,
    ];

    foreach ($transactions as $t) {
      // Get transaction values
      $value       = $t->total->value;
      $income      = $value < 0 ? abs($value) : 0;
      $expenditure = $value >= 0 ? $value : 0;
      $total       = ($value * -1);

      // Add to correct array
      $type   = substr($t->cc->value, 0, 1) == 'N' ? 'nongrant' : 'grant';
      $glCode = $t->gl->value;
      $this->addToFinanceTransactionsBalance($results, $type, $income, $expenditure, $total, $glCode, $t->gl_description->value);

      // Update last updated
      if ($results['lastUpdated'] === NULL || !$t->created || $results['lastUpdated'] < $t->created->value) {
        $results['lastUpdated'] = $t->created->value;
      }

      $results['count'] = $results['count'] + 1;
    }

    return $results;
  }

  public function addToFinanceTransactionsBalance(&$results, $type, $income, $expenditure, $total, $glCode, $glDescription) {

    $results[$type]['income']      += $income;
    $results[$type]['expenditure'] += $expenditure;
    $results[$type]['total']       += $total;

    // Add to totals array
    $results['totals']['income']      += $income;
    $results['totals']['expenditure'] += $expenditure;
    $results['totals']['total']       += $total;

    // Split into GL codes
    if (!isset($results[$type]['glcodes'][$glCode])) {
      $results[$type]['glcodes'][$glCode] = ['name' => $glDescription, 'code' => $glCode, 'income' => 0, 'expenditure' => 0, 'total' => 0];
    }
    if (!isset($results['totals']['glcodes'][$glCode])) {
      $results['totals']['glcodes'][$glCode] = $results[$type]['glcodes'][$glCode];
    }

    $results[$type]['glcodes'][$glCode]['income']      += $income;
    $results[$type]['glcodes'][$glCode]['expenditure'] += $expenditure;
    $results[$type]['glcodes'][$glCode]['total']       += $total;

    $results['totals']['glcodes'][$glCode]['income']      += $income;
    $results['totals']['glcodes'][$glCode]['expenditure'] += $expenditure;
    $results['totals']['glcodes'][$glCode]['total']       += $total;
  }

  public function getFinanceTransactionsGLDescs($transactions, $grantAllocations, $asPercentage) {
    $results = ['income' => [], 'expenditure' => []];
    $totals  = [];

    foreach ($transactions as $t) {
      $total = $t->total->value;
      $desc  = $t->gl_description->value;
      $this->setGLDesc($totals, $results, $total, $desc);
    }

    foreach ($grantAllocations as $g) {
      $total = $g->total->value;
      $desc  = $g->gl_description->value;
      $this->setGLDesc($totals, $results, $total, $desc);
    }

    if ($asPercentage) {
      foreach (array_keys($results) as $type) {
        foreach ($results[$type] as $desc => $value) {
          $results[$type][$desc] = $value / $totals[$type];
        }
      }
    }
    return $results;
  }

  public function setGLDesc(&$totals, &$results, $total, $desc) {
    // Work out if income or expenditure
    $type = 'income';
    if ($total > 0) {
      $type = 'expenditure';
    }
    if (!isset($totals[$type])) {
      $totals[$type] = 0;
    }

    // Get description and create key in array if new
    if (!isset($results[$type][$desc])) {
      $results[$type][$desc] = 0;
    }

    // Add total to specific description, and to overall total for this type
    $results[$type][$desc] += abs($total);
    $totals[$type]         += abs($total);
  }
}
