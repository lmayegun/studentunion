<?php

namespace Drupal\su_finance\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;

/**
 * Configure Bookable resources settings for this site.
 */
class LineItemReportForm extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_finance_line_item_report';
  }

  public function getDownloadTitle($form, $form_state) {
    return "Line item finance export " . \Drupal::service('date.formatter')->format(strtotime('now'), 'short');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['date_start'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Start date'),
      '#default_value' => $_SESSION[$this->getFormId() . 'date_start'] ?? DrupalDateTime::createFromTimestamp(strtotime('midnight, first day of last month')),
    ];

    $form['date_end'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('End date'),
      '#default_value' => $_SESSION[$this->getFormId() . 'date_end'] ?? DrupalDateTime::createFromTimestamp(strtotime('23:59:59, last day of last month')),
    ];

    $form['date_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Date type'),
      '#options'       => [
        'created'   => 'Created',
        'placed'    => 'Placed',
        'payment'   => 'Payment',
        // 'payment_completed' => 'Payment completed (ignore authorised date)',
        // 'completed' => 'Completed',
        'changed'   => 'Changed / last updated',
      ],
      '#default_value' => $_SESSION[$this->getFormId() . 'date_type'] ?? "payment",
      '#description'   => "Note: payment will only include orders for which we have a payment record. It will include it based on the 'completed' date of the original payment, not the time of the most recent refund."
    ];

    $form['exclude_free_orders'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Only include orders with a payment (total price > 0)'),
      '#default_value' => $_SESSION[$this->getFormId() . 'exclude_free_orders'] ?? 0,
    ];

    $form['payment_gateways'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Payment gateways'),
      '#description'   => "Note: you must select a payment date (either Payment or Payment completed) to filter by payment gateway.",
      '#options'       => ['' => 'All', 'worldnet_gateway' => 'Worldnet only', 'pos_debit' => 'Point of sale debit only'],
      '#default_value' => $_SESSION[$this->getFormId() . 'payment_gateways'] ?? '',
    ];

    $form['exclude_refunds_within_24_hours'] = [
      '#type'          => 'hidden',
      '#title'         => $this->t('Exclude refunds within 24 hours'),
      '#default_value' => $_SESSION[$this->getFormId() . 'exclude_refunds_within_24_hours'] ?? 0,
    ];

    $form['statuses'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Statuses'),
      '#options'       => ['' => 'All'],
      '#default_value' => $_SESSION[$this->getFormId() . 'statuses'] ?? NULL,
    ];

    $form['report_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Report type'),
      '#options'       => ['exchequer' => 'Exchequer', '' => 'Standard'],
      '#default_value' => $_SESSION[$this->getFormId() . 'report_type'] ?? '',
    ];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => 'Run report',
    ];
    return $form;
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $fields = ['date_start', 'date_end', 'date_type', 'statuses', 'report_type', 'exclude_free_orders', 'payment_gateways', 'exclude_refunds_within_24_hours'];
    foreach ($fields as $field) {
      // Save into session for revisiting the form
      $_SESSION[$this->getFormId() . $field] = $values[$field] ?? NULL;
    }

    // Get orders and add as operations
    $service = \Drupal::service('su_finance.commerce_report');
    $orders = $service->loadOrderIds(
      $values['date_start'],
      $values['date_end'],
      $values['date_type'],
      $values['statuses'],
      $values['exclude_free_orders'],
      array_filter(explode(',', $values['payment_gateways'])),
      $values['exclude_refunds_within_24_hours']
    );

    // POS refunds delete the payments, so we can't filter on payment dates.
    $pos_refunds = $service->loadOrderIdsPosRefunded(
      $values['date_start'],
      $values['date_end'],
      $values['date_type'],
      $values['statuses'],
      $values['exclude_free_orders'],
      array_filter(explode(',', $values['payment_gateways'])),
      $values['exclude_refunds_within_24_hours']
    );

    $orders = array_merge($orders, $pos_refunds);

    $orderOps = [];
    foreach ($orders as $order_id) {
      $orderOps[] = [
        'order_id' => $order_id,
        'report_type' => $values['report_type']
      ];
    }

    $refundedOrderItems = $service->loadRefundedOrderItemIds(
      $values['date_start'],
      $values['date_end'],
      $values['date_type'],
      $values['statuses'],
      $values['exclude_free_orders'],
      array_filter(explode(',', $values['payment_gateways'])),
      $values['exclude_refunds_within_24_hours']
    );
    // Get refunded order items as refunds:
    foreach ($refundedOrderItems as $order_item_id) {
      $orderOps[] = [
        'refunded_order_item_id' => $order_item_id,
        'report_type' => $values['report_type']
      ];
    }

    $chunkSize = 500;
    $chunks = array_chunk($orderOps, $chunkSize);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public function processBatch(array $rows, array &$context) {
    $service = \Drupal::service('su_finance.commerce_report');
    foreach ($rows as $row) {
      $newLines = [];
      if (isset($row['refunded_order_item_id'])) {
        $orderItem = OrderItem::load($row['refunded_order_item_id']);
        $newLines = $service->getLinesForOrderItem($orderItem, $row['report_type'], 'Refund');
      } else {
        $order = Order::load($row['order_id']);
        $newLines = $service->getLinesForOrder($order, $row['report_type']);
      }
      foreach ($newLines as $line) {
        $this->addLine($line, $context);
      }
    }
  }

  public function sortResults($rows, $results) {
    // @todo Sort by date desc
    $date_type = $_SESSION[$this->getFormId() . 'date_type'];

    return $rows;
  }
}
