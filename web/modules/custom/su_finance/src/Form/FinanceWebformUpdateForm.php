<?php

namespace Drupal\su_finance\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Configure Bookable resources settings for this site.
 */
class FinanceWebformUpdateForm extends BatchDownloadCSVForm {

  use FinanceWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $webform = NULL) {
    $form = parent::buildForm($form, $form_state);

    $form['intro'] = [
      '#markup' => t('This form allows you to run a specific transition (e.g. mark as paid) on multiple submissions by providing their submission IDs below.'),
    ];

    if (!$webform) {
      return ['#markup' => t('Cannot identify webform from context.')];
    }

    if ($webform && !in_array($webform, array_keys($this->getForms()))) {
      return ['#markup' => t('Webform does not support finance update.')];
    }

    $form_state->set('webform_id', $webform);

    $formInfo = $this->getForms()[$webform];

    if (isset($formInfo['export_note']) && $formInfo['export_note']) {
      $form['export_note'] = [
        '#markup' => $formInfo['export_note'],
        '#weight' => -50,
      ];
    }

    $webform = Webform::load($webform);

    $workflow = $this->getWebformWorkflow($webform);
    $transitions = $workflow->getTypePlugin()->getTransitions();
    $transitionOptions = [];
    foreach ($transitions as $transition_id => $transition) {
      $transitionOptions[$transition_id] = $transition->label();
    }

    $form['transition'] = [
      '#type' => 'select',
      '#title' => 'Transition to run',
      '#options' => $transitionOptions,
      '#default_value' => $formInfo['default_transition'] ?? '',
    ];

    $form['log_public'] = [
      '#type' => 'textarea',
      '#title' => 'Log message',
      '#rows' => 3,
    ];

    $form['submission_ids'] = [
      '#type' => 'textarea',
      '#title' => 'Submission IDs to update (copy paste from Excel with a new one on each line)',
      '#rows' => 20,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Update submissions',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->getBatchTitle($form, $form_state))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'exportFinished']);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $submission_ids_string = str_replace([
      ',',
      ';',
      "\r",
      "\n",
    ], ',', $values['submission_ids']);
    $submission_ids = explode(',', $submission_ids_string);

    $chunk_size = 100;
    $chunks = array_chunk($submission_ids, $chunk_size);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [
        [
          'submission_ids' => $chunk,
          'transition_id' => $values['transition'],
          'log_public' => $values['log_public'],
          'webform_id' => $form_state->get('webform_id'),
        ],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_finance_webform_update';
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    if ($webform_id = $form_state->get('webform_id')) {
      $webform = Webform::load($webform_id);
      $name = $webform->label();
    }
    $date = Drupal::service('date.formatter')
      ->format(strtotime('now'), 'short');
    return ($name ? '' . $name . ' - ' : '') . 'finance update result - ' . $date;
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch(array $data, array &$context = NULL) {
    $submission_ids = $data['submission_ids'];
    $transition_id = $data['transition_id'];
    $log_public = $data['log_public'];

    $webform = Webform::load($data['webform_id']);
    if (!$webform) {
      //@todo throw error
      return;
    }

    $workflow = $this->getWebformWorkflow($webform);
    $states = $workflow->getTypePlugin()->getStates();

    foreach ($submission_ids as $submission_id) {
      $message = '';
      $submission = WebformSubmission::load($submission_id);
      if (!$submission) {
        $message = 'Could not find submission.';
      }

      $webform = $submission->getWebform();
      if ($webform->id() != $data['webform_id']) {
        $message = 'Could not find this submission for this webform.';
      }

      $workflow_value = $submission->getElementData('workflow');

      if (!$message && $transition_id) {
        $currentStateId = $workflow_value['workflow_state'];

        $currentState = $states[$currentStateId];

        $transitionsAvailable = $currentState->getTransitions();
        if (!in_array($transition_id, array_keys($transitionsAvailable))) {
          $message = 'No valid transition.';
        }
        else {
          $workflow_value['transition'] = $transition_id;
          $workflow_value['log_public'] = $log_public;
          $submission->setElementData('workflow', $workflow_value);
          $submission->save();
          $message = 'Updated successfully.';
        }
      }
      else {
        if (!$message) {
          $message = 'No valid transition.';
        }
      }

      $line = [
        'Webform' => $submission->getWebform()->label(),
        'Submission ID' => $submission->id(),
        'Result' => $message,
      ];
      $this->addLine($line, $context);
    }
  }

}
