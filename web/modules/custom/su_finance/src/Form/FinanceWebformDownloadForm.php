<?php

namespace Drupal\su_finance\Form;

use Drupal;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Download webform results in finance-appropriate formats.
 *
 * We need a special export because a lot of these forms use composite elements
 * which need a row per composite item unique values
 * with shared data from the submission copied for other fields.
 *
 * NOTE for a webform to use this,
 * you need to define it in getForms() in this class
 * and provide the code to convert a submission to CSV rows.
 * See the existing functions in getForms as an example.
 */
class FinanceWebformDownloadForm extends BatchDownloadCSVForm {

  use FinanceWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_finance_webform_download';
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    if ($webform_id = $form_state->get('webform_id')) {
      $webform = Webform::load($webform_id);
      $name = $webform->label();
    }
    $date = Drupal::service('date.formatter')
      ->format(strtotime('now'), 'short');
    return ($name ? '' . $name . ' - ' : '') . 'finance export - ' . $date;
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $webform = NULL) {
    $form = parent::buildForm($form, $form_state);

    if (!$webform) {
      return ['#markup' => t('Cannot identify webform from context.')];
    }

    if ($webform && !in_array($webform, array_keys($this->getForms()))) {
      return ['#markup' => t('Webform does not support finance export.')];
    }

    $form_state->set('webform_id', $webform);

    $formInfo = $this->getForms()[$webform];

    $webform = Webform::load($webform);

    $workflow = $this->getWebformWorkflow($webform);
    $states = $workflow->getTypePlugin()->getStates();
    $stateOptions = [];
    foreach ($states as $state_id => $state) {
      $stateOptions[$state_id] = $state->label();
    }

    $form['states'] = [
      '#type' => 'checkboxes',
      '#title' => 'Submission statuses to include',
      '#options' => $stateOptions,
      '#default_value' => $formInfo['default_states'],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Export',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, string $webform = NULL) {
    if ($form_state->getValue('webform')) {
      $form_state->set('webform_id', $form_state->getValue('webform'));
    }

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->getBatchTitle($form, $form_state))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'exportFinished']);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $webform = Webform::load($form_state->get('webform_id'));

    // Get submissions and add as operations
    $submissions = [];
    foreach ($form_state->getValue('states') as $state_id => $value) {
      if ($value != $state_id) {
        continue;
      }

      $submissionsWithState = webform_workflows_element_queries_get_submission_with_state($webform, 'workflow', $state_id);
      if ($submissionsWithState) {
        $submissions = array_merge($submissions, $submissionsWithState);
      }
    }
    $submission_ids = [];
    foreach ($submissions as $submission) {
      $submission_ids[] = $submission->id();
    }

    $submission_ids = array_unique($submission_ids);

    $chunk_size = 50;
    $chunks = array_chunk($submission_ids, $chunk_size);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public function processBatch(array $submissions, array &$context) {
    $forms = $this->getForms();
    foreach ($submissions as $submission_id) {
      $submission = WebformSubmission::load($submission_id);
      $form = $forms[$submission->getWebform()->id()];
      $lines = $form['process_submission']($submission);
      foreach ($lines as $line) {
        $this->addLine($line, $context);
      }
    }
  }

  public function sortResults($rows, $results) {
    // @todo Sort by date desc
    return $rows;
  }

}
