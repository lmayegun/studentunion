<?php

namespace Drupal\su_finance\Form;

use Drupal;
use Drupal\group\Entity\Group;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\workflows\Entity\Workflow;

trait FinanceWebformTrait {

  /**
   * Define the forms available and their processing functions.
   *
   * @return [type]
   */
  public function getForms() {
    $forms = [];

    // Payment request form (for clubs and societies)
    $forms['payment_request_form'] = [
      'default_states' => [
        'submitted_to_finance_team_for_payment',
        'approved_by_finance_panel',
      ],
      'default_transition' => [
        'paid',
      ],
      'process_submission' => function ($submission) {
        $lines = [];
        $values = $submission->getData();

        $group = Group::load($values['webform_access_group']);
        $cc_type = 'X';
        if ($group->bundle() == 'club_society') {
          if (su_clubs_societies_is_group_sports($group)) {
            $cc_type = 'C';
          }
        }
        $dept = $group->field_finance_department_code->entity ? $group->field_finance_department_code->entity->getName() : '';

        $stateLabel = $submission->getElementData('workflow')['workflow_state_label'];

        // Eventual export fields
        $shared_data = [
          'INVOICES' => '',
          'Ttype' => '',
          'Accode' => '',
          'Submission Id' => $submission->id(), // @todo confirm with Finance
          'Invoice No.' => $submission->id(), // @todo confirm with Finance
          'Transdate' => date('d/m/Y', $submission->getCompletedTime()),
          'cc' => '', // populated per purchase
          'dept' => $dept,
          'nomcod' => '', // populated per purchase
          'Vatcode' => 3,
          'Desc' => strtoupper($values['payable_to']),
          'Netamt' => '', // populated per purchase
          'Vatamt' => 0,
          'Account Number' => $values['account_number'],
          'Sort Code' => $values['sort_code'],
          'Payee email address' => $values['payee_ucl_email_address'],
          'Workflow state' => $stateLabel,
        ];

        $rows = $submission->getElementData('purchases');
        $i = 1;
        foreach ($rows as $row) {
          $row_data = [];

          if ($group->bundle() == 'volunteering_org') {
            // VSU expenses have different cc and dept
            $vsu_expense = substr($row['transaction_dept'], 3) == 'VSU';
            if ($vsu_expense) {
              $row_data['cc'] = 'GZ';
              $row_data['dept'] = 'VSU';
            }
            else {
              $row_data['cc'] = 'NX';
              $row_data['dept'] = $dept;
            }
            $row_data['nomcod'] = '99700';
          }
          else {
            // Club/Society are allocated on the form
            $row_data['cc'] = $row['account_code_cc'] . $cc_type;
            $row_data['nomcod'] = $row['account_code_nomcod'];
          }
          $row_data['Netamt'] = $row['net'];

          if (count($row_data) > 0) {
            $line = array_merge($shared_data, $row_data);
            $lines[] = $line;
          }

          $i++;
        }
        return $lines;
      },
    ];

    // Volunteering payment request is the same
    $forms['vol_payment_request_form'] = $forms['payment_request_form'];

    // Purchase request form
    $forms['webform_135417'] = [
      'default_states' => [
        'submitted_to_finance_team_for_payment',
        'approved_by_finance_panel',
      ],
      'default_transition' => [
        'paid',
      ],
      'export_note' =>
        t('Note: this export only includes UK bank transfers.'),
      'process_submission' => function (WebformSubmissionInterface $submission) {
        $lines = [];
        $values = $submission->getData();

        // Only include UK bank transfers:
        if ($values['how_should_these_goods_services_be_purchased'] != 'Bank transfer') {
          return;
        }

        $group = Group::load($values['webform_access_group']);
        $club_cc_type = 'X';
        if ($group->bundle() == 'club_society') {
          if (su_clubs_societies_is_group_sports($group)) {
            $club_cc_type = 'C';
          }
        }
        $dept = $group->field_finance_department_code->entity ? $group->field_finance_department_code->entity->getName() : '';

        $stateLabel = $submission->getElementData('workflow')['workflow_state_label'];

        // Eventual export fields
        $shared_data = [
          //          'NAME CHK' => '',
          //          'Type' => 'PJI',
          //          'NAME_CHK' => '',
          //          'Invoice No.' => '',
          'Submission' => $submission->id(),
          'Workflow state' => $stateLabel,
          'Club/Society' => $group->label(),
          'Transdate' => date('d/m/Y H:m', $submission->getCompletedTime()),
          'Desc' => '',
          'Netamt' => '',
          'Vatamt' => 0,
          'cc' => '',
          'dept' => $dept,
          'nomcod' => '',
          'Method' => '',
          'Company' => $submission->getElementData('company_name'),
          'Invoice no' => $submission->getElementData('invoice_number'),
          'Account number' => '',
          'Sort code' => '',
          //          'Email' => $submission->getElementData('e_mail'),
        ];

        $rows = $submission->getElementData('description_of_goods_or_services');

        $method = $submission->getElementData('how_should_these_goods_services_be_purchased');
        $shared_data['Method'] = $method;
        if ($method == 'Bank transfer') {
          $shared_data['Account number'] = $submission->getElementData('account_number');
          $shared_data['Sort code'] = $submission->getElementData('sort_code');
        }

        $i = 1;
        foreach ($rows as $row) {
          $row_data = [];

          $row_data['Desc'] = $row['description'];
          $row_data['nomcod'] = $row['type'];
          $row_data['Netamt'] = $row['total_cost'];
          $row_data['cc'] = $row['grant_nongrant'] . $club_cc_type;

          if (count($rows) > 1) {
            $row_data['Submission'] = $shared_data['Submission'] . '-' . $i;
          }

          if (count($row_data) > 0) {
            $line = array_merge($shared_data, $row_data);
            $lines[] = $line;
          }

          $i++;
        }
        return $lines;
      },
    ];

    $forms['create_travel_expense'] = [
      'default_states' => [
        'pending_payment',
      ],
      'default_transition' => [
        'mark_as_paid',
      ],
      'process_submission' => function (WebformSubmissionInterface $submission) {
        $values = $submission->getData();
        $account = $submission->getOwner();

        $refereeReceipt = ($values['type_of_request'] == '99742');

        // Eventual export fields
        $shared_data = [
          'INVOICES' => '',
          'Type' => $refereeReceipt ? 'UR' : 'TE',
          'Accode' => '',
          'Submission Id' => $submission->id(),
          'Transdate' => date('d/m/Y H:m', $submission->getCompletedTime()),
          'cc' => 'BZ',
          'dept' => 'CEN',
          'nomcod' => '22798',
          'Vatcod' => '3',
          'Desc' => $account->getDisplayName(),
          'Netamt' => '',
          'Vatamt' => 0,
          'Account name' => '',
          'Account number' => '',
          'Sort Code' => '',
          'Payee email address' => $account->getEmail(),
        ];

        $bankDetailsProfile = Drupal::entityTypeManager()
          ->getStorage('profile')
          ->loadByUser($account, 'bank_details');

        if ($bankDetailsProfile) {
          $shared_data['Account name'] = $bankDetailsProfile->field_bank_account_name->value;
          $shared_data['Account number'] = $bankDetailsProfile->field_bank_account_number->value;
          $shared_data['Sort Code'] = $bankDetailsProfile->field_bank_sort_code->value;
        }

        // Travel reclaims only do one row per submission:
        return [
          $shared_data,
        ];
      },
    ];

    // Elections budget form
    $forms['elections_budget_form'] = [
      'process_submission' => function ($submission) {
        $values = $submission->getData();

        // Eventual export fields
        $shared_data = [
          'INVOICES' => '',
          'Ttype' => '',
          'Accode' => '',
          'Submission Id' => $submission->id(),
          'Invoice No.' => $submission->id(),
          'Transdate' => $submission->submitted->value,
          'cc' => 'GZ',
          'dept' => 'ELE',
          'nomcod' => '90714',
          'Vatcode' => 3,
          'Desc' => strtoupper($values['approved_by']),
          'Netamt' => '', // Set per purchase
          'Vatamt' => 0,
          'Account Number' => $values['account_number'],
          'Sort Code' => $values['sort_code'],
          'Payee name' => $submission->getOwner()->getDisplayName(),
          'Payee email address' => $values['email_address'],
        ];

        $rows = [];
        $i = 1;
        foreach ($rows as $row) {
          $row_data = [];

          if (!empty($row['net' . $i]) || ($i == 1 && !empty($row['net']))) {
            $row_data['Netamt'] = $row['net' . ($i > 1 ? $i : '')];
          }

          if (count($row_data) > 0) {
            $line = array_merge($shared_data, $row_data);
            $this->addLine($line, $context);
          }

          $i++;
        }
      },
    ];

    return $forms;
  }

  private function getWebformWorkflow(?Webform $webform) {
    $workflowsManager = Drupal::service('webform_workflows_element.manager');
    $workflow_elements = $workflowsManager->getWorkflowElementsForWebform($webform);
    $workflow_id = reset($workflow_elements)['#workflow'];
    $workflow = Workflow::load($workflow_id);
    return $workflow;
  }

}
