<?php

namespace Drupal\su_finance\Form;

use DateTime;
use Drupal;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\su_finance\Entity\FinanceTransaction;

/**
 * Implement Class BulkUserImport for import form.
 */
class FinanceTransactionsUploadForm extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_finance_transactions_upload';
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    // Delete existing transactions
    $query = Drupal::entityQuery('finance_transaction');
    $ids = $query->execute();
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 1;
    $chunks = array_chunk($ids, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [
        $this,
        'processBatchDelete',
      ], [$chunk]);
    }

    // Import as normal
    parent::setOperations($batch_builder, $form, $form_state);
  }

  public static function processBatchDelete($rows, array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['deleted'] = 0;
    }

    $storage_handler = Drupal::entityTypeManager()
      ->getStorage('finance_transaction');
    $entities = $storage_handler->loadMultiple($rows);
    $storage_handler->delete($entities);

    $context['sandbox']['deleted'] = (isset($context['sandbox']['deleted']) ? $context['sandbox']['deleted'] : 0) + count($rows);
  }


  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['DEPT'] = [
    ];
    $fields['CC'] = [
    ];
    $fields['GL Code'] = [
    ];
    $fields['GL DESC'] = [
    ];
    $fields['TRANS. REF'] = [
    ];
    $fields['PERIOD'] = [
    ];
    $fields['ACC CODE'] = [
    ];
    $fields['INVOICE REF'] = [
    ];
    $fields['PAYER/PAYEE DESCRIPTION'] = [
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    // D8 Field => CSV title
    $map = [
      'dept' => 'DEPT',
      'cc' => 'CC',
      'gl' => 'GL Code',
      'gl_description' => 'GL DESC',
      'transaction_reference' => 'TRANS. REF',
      'period' => 'PERIOD',
      'account_code' => 'ACC CODE',
      'invoice_reference' => 'INVOICE REF',
      'description' => 'PAYER/PAYEE DESCRIPTION',
    ];

    $data = [];
    foreach ($map as $field => $csv) {
      if (isset($row[$csv])) {
        $data[$field] = preg_replace('/[\x00-\x1F\x7F]/u', '', $row[$csv]);
      }
    }
    $data['total'] = floatval(str_replace(',', '', $row['NET TOTAL']));

    $convertedDate = DateTime::createFromFormat('d/m/Y', $row['DATE']);
    $data['date'] = $convertedDate ? $convertedDate->getTimestamp() : 0;

    $financeTransaction = FinanceTransaction::create($data);
    $financeTransaction->save();
  }

}
