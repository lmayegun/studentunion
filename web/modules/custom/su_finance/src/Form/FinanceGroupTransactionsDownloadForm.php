<?php

namespace Drupal\su_finance\Form;

use DateTime;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\su_finance\Entity\FinanceTransaction;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Configure Bookable resources settings for this site.
 */
class FinanceGroupTransactionsDownloadForm extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_finance_transactions_group_download';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['date_start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start date'),
      '#default_value' => $_SESSION[$this->getFormId() . 'date_start'] ?? DrupalDateTime::createFromTimestamp(strtotime('midnight, August 1')),
    ];
    $form['date_end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End date'),
      '#default_value' => $_SESSION[$this->getFormId() . 'date_end'] ?? DrupalDateTime::createFromTimestamp(strtotime('midnight, August 1 + 1 year - 1 day')),
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Run report',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->getBatchTitle($form, $form_state))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'exportFinished']);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $fields = ['date_start', 'date_end'];
    foreach ($fields as $field) {
      // Save into session for revisitng the form
      $_SESSION[$this->getFormId() . $field] = $values[$field] ?? null;
    }

    // Get orders and add as operations
    // $group = Group::load($values['group']);
    $current_route = \Drupal::routeMatch();
    $group = $current_route->getParameters()->get('group');
    if ($group) {
      $dept = $group->field_finance_department_code->entity->getName();
      $transactions = FinanceTransaction::findByDeptAndDates($dept, $values['date_start'], $values['date_end']);
    } else {
      $transactions = [];
    }
    $chunk_size = 100;
    $chunks = array_chunk($transactions, $chunk_size);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public function processBatch(array $transactions, array &$context) {
    foreach ($transactions as $transaction) {
      $line = [
        'Department' => $transaction->dept->value,
        'CC' => $transaction->cc->value,
        'GL Code'  => $transaction->gl->value,
        'GL Description'  => $transaction->gl_description->value,
        'Description' => $transaction->description->value,
        'Date' => DateTime::createFromFormat("U", $transaction->date->value)->format('Y-m-d'),
        'Transaction Reference' => $transaction->transaction_reference->value,
        'Period'  => $transaction->period->value,
        'Invoice reference' => $transaction->invoice_reference->value,
        'Account code' => $transaction->account_code->value,
        'Total' => $transaction->total->value
      ];
      $this->addLine($line, $context);
    }
  }

  public function sortResults($rows, $results) {
    // @todo Sort by date desc
    return $rows;
  }
}
