<?php

namespace Drupal\su_finance\Form;

use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\su_finance\Entity\FinanceSupplier;

/**
 * Implement Class BulkUserImport for import form.
 */
class FinanceSuppliersUploadForm extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_finance_suppliers_upload';
  }

  /**
   * Define the fields expected for the CSV.
   *
   * @return array
   *   Array of fields.
   */
  public static function getMapping(): array {
    $fields = [
      'Account Code',
      'Supplier Name',
      'Email Contact',
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    // Import as normal
    parent::setOperations($batch_builder, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    if ($supplier = FinanceSupplier::findByCode($row['Account Code'])) {
      // All good.
    }
    else {
      $supplier = FinanceSupplier::create(['account_code' => $row['Account Code']]);
    }
    $map = [
      'name' => 'Supplier Name',
      'mail' => 'Email Contact',
    ];

    $data = [];
    foreach ($map as $field => $csv) {
      if (isset($row[$csv])) {
        $supplier->set($field, preg_replace('/[\x00-\x1F\x7F]/u', '', $row[$csv]));
      }
    }

    $supplier->save();
  }

}
