<?php

namespace Drupal\word_limit_per_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the SummaryWordLimit constraint.
 */
class WordLimitValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\text\Plugin\Field\FieldType\TextWithSummaryItem $item */
    foreach ($items as $item) {

      /** @var \Drupal\field\Entity\FieldConfig $field_config */
      $field_config = $item->getFieldDefinition();

      $word_limit = $field_config->getThirdPartySetting('word_limit_per_field', 'word_limit');

      if ($word_limit && $item->value) {
        $count = $this->getWordCountFromString($item->value);
        $over = $count > $word_limit;
        if ($over) {
          $this->context->addViolation($constraint->overWordLimit, [
            '%field_name' => $field_config->getName(),
            '%limit_count' => $word_limit,
            '%current_count' => $count,
          ]);
        }
      }
    }
  }

  public function getWordCountFromString($value) {
    $str = strip_tags($value);
    $a = preg_split('/\W+/u', $str, -1, PREG_SPLIT_NO_EMPTY);
    return count($a);
  }
}
