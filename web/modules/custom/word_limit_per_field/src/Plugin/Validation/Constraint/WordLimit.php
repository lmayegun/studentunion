<?php

namespace Drupal\word_limit_per_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the summary of a text field is within the word count limit.
 *
 * @Constraint(
 *   id = "WordLimit",
 *   label = @Translation("Word Limit", context = "Validation"),
 *   type = "string"
 * )
 */
class WordLimit extends Constraint {

  // The message that will be shown if the summary is over the word limit count.
  public $overWordLimit = 'The %field_name is over the limit of %limit_count words. You used %current_count words.';
}
