<?php

namespace Drupal\word_limit_per_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the SummaryWordLimit constraint.
 */
class SummaryWordLimitValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\text\Plugin\Field\FieldType\TextWithSummaryItem $item */
    foreach ($items as $item) {

      /** @var \Drupal\field\Entity\FieldConfig $field_config */
      $field_config = $item->getFieldDefinition();

      $word_limit_summary = $field_config->getThirdPartySetting('word_limit_per_field', 'word_limit_summary');

      if ($word_limit_summary) {
        $count = str_word_count($item->summary);
        $over = $count > $word_limit_summary;
        if ($over) {
          $this->context->addViolation($constraint->overWordLimit, [
            '%field_name' => $field_config->getName(),
            '%limit_count' => $word_limit_summary,
            '%current_count' => $count,
          ]);
        }
    }
  }
}
