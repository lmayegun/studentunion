# Word Limit Per Field

This simple module allows you to add a word count limit to various text fields, and/or its summary (if it has a summary).

This is enabled via third party settings on the field config, and implemented using a validation constraint.

* For a full description of the module, visit the project page: https://www.drupal.org/project/word_limit_per_field

* To submit bug reports and feature suggestions, or to track changes: https://www.drupal.org/project/issues/word_limit_per_field

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.

## Configuration

To set a word count limit for a field, edit a text field and enter a number in the _Word limit count_ field.

For example, to limit the _Basic page_ summary length:

  1. Navigate to Administration > Extend and enable the module.
  2. Navigate to Administration > Structure > Content types > Basic page > Manage fields > Body > Edit.
  3. Set maximum number of words allowed in the _Word limit count_ box.
  4. Click _Save settings_.
