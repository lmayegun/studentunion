<?php

namespace Drupal\su_groups\EventSubscriber;

use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\entity_clone\Event\EntityCloneEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Handle cloning subgroups.
 */
class CloneSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // $events[\Drupal\entity_clone\Event\EntityCloneEvents::PRE_CLONE][] = ['myPreClone'];
    $events[EntityCloneEvents::POST_CLONE][] = ['postClone'];
    return $events;
  }

  /**
   * An example event subscriber.
   *
   * Dispatched before an entity is cloned and saved.
   *
   * @see \Drupal\entity_clone\Event\EntityCloneEvents::PRE_CLONE
   */
  // public function myPreClone(\Drupal\entity_clone\Event\EntityCloneEvent $event): void {
  //   $original = $event->getEntity();
  //   $newEntity = $event->getClonedEntity();
  // }

  /**
   * An example event subscriber.
   *
   * Dispatched after an entity is cloned and saved.
   *
   * @see \Drupal\entity_clone\Event\EntityCloneEvents::POST_CLONE
   */
  public function postClone(EntityCloneEvent $event): void {
    $original = $event->getEntity();
    if ($original->bundle() == 'volunteering_opp') {
      $newEntity = $event->getClonedEntity();

      /** @var \Drupal\subgroup\Entity\SubgroupHandlerInterface $handler */
      $subgroupHandler = \Drupal::entityTypeManager()->getHandler('group', 'subgroup');
      $subgroupHandler->removeLeaf($newEntity, TRUE);

      $parent = su_groups_get_parent($original);

      $parent->addContent($newEntity, 'subgroup:volunteering_opp');
      $parent->save();
    }
  }
}
