<?php

namespace Drupal\su_groups\EventSubscriber;

use Drupal\commerce_product\Entity\Product;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class OrderCompleteSubscriber.
 *
 * @package Drupal\su_groups
 */
class OrderCompleteSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['orderCompleteHandler'];

    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param WorkflowTransitionEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function orderCompleteHandler(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    // Order items in the cart.
    $items = $order->getItems();
    foreach ($items as $item) {
      $product              = $item->getPurchasedEntity();
      $product_type         = $product->get('type')->getValue()[0]['target_id'];
      $product_id           = $product->get('product_id')
                                      ->getValue()[0]['target_id'];
      $product_variation_id = $product->get('variation_id')
                                      ->getValue()[0]['value'];
      $user_uid             = $product->get('uid')->getValue()[0]['target_id'];

      if ($product_type === '') {
        $load_product = $product = Product::load($product_id);
        /**
         * @var \Drupal\group\Entity\Group $group
         */
        $group = $load_product->field_group->entity;
        /**
         * @var \Drupal\group\Entity\GroupRole $role
         */
        $role     = $load_product->field_group_role->entity;
        if($group && $role) {
          $role_ref = $role->getOriginalId();
          $user     = User::load($user_uid);
          $group->addMember($user, ['group_roles' => [$role_ref]]);
          $group->save();
        }
      }
    }
  }

  /**
   * @param \Drupal\commerce_product\Entity\Product $product
   *
   * @return array
   */
  private function getFields($product) {
    $values = [];
    foreach ($product->getFields() as $name => $property) {
      $values[$name] = $property;
    }
    return $values;
  }

}
