<?php

namespace Drupal\su_groups\Form;

use DateTime;
use Drupal;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;

/**
 * Implement Class BulkUserImport for import form.
 */
abstract class BulkImportGroupMembershipRecordForm extends BatchImportCSVForm {

  const groupMembershipRecordType = 'club_society_committee';

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processRow(array $row, array &$context) {
    $valid = static::validateRow($row, $context);
    if (!$valid) {
      return FALSE;
    }

    $action = $row['Action'] ?? 'create';

    $gmr = static::getGroupMembershipRecord($row, $action);

    // If an error, or we've already ended all of them:
    if ($gmr == 'error' || $gmr == 'ended') {
      return FALSE;
    }

    // Having loaded it, we can delete it, if asked:
    if ($action == 'delete') {
      $gmr->delete();
      return TRUE;
    }

    // Create if we need to:
    if (!$gmr) {
      $gmr = static::createGroupMembershipRecord($row);
    }

    // Update if we need to:
    static::updateGmrFromRow($gmr, $row);
    $gmr->save();

    return TRUE;
  }

  public static function validateRow($row, &$context): bool {
    $mapping = static::getMapping();
    foreach ($mapping as $columnName => $data) {
      // Check if required
      if (isset($data['required']) && $data['required']) {
        if (!isset($row[$columnName])) {
          static::addError($row, $context, 'Required field not provided: ' . $columnName);
          return FALSE;
        }
      }

      // Check if allowed value:
      if (isset($data['allowed_values']) && !in_array(trim($row[$columnName]), $data['allowed_values'])) {
        static::addError($row, $context, $columnName . ' must be one of: ' . implode(', ', $data['allowed_values']));
        return FALSE;
      }
    }

    $start = static::getStart($row);
    if (!$start || $start < DateTime::createFromFormat('d/m/Y', '01/01/2000')) {
      static::addError($row, $context, 'Could not work start date out from ' . $row['Start'] . ' - must be d/m/Y like 01/12/2021. Check your format.');
      return FALSE;
    }

    $end = static::getEnd($row);
    if (!$end || $end < DateTime::createFromFormat('d/m/Y', '01/01/2000')) {
      static::addError($row, $context, 'Could not work end date out from ' . $row['End'] . ' - must be d/m/Y like 01/12/2021. Check your format.');
      return FALSE;
    }

    $role = static::getRole($row);
    if (!$role) {
      static::addError($row, $context, 'Role not found with name ' . $row['Role']);
      return FALSE;
    }

    $user = static::getUser($row);
    if (!$user) {
      static::addError($row, $context, 'User not found with ID ' . $row['User']);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['Start'] = [
      'description' => 'date to start record',
      'dataFormat' => 'd/m/Y',
      'example' => '15/06/2025',
      'required' => TRUE,
    ];
    $fields['End'] = [
      'description' => 'date to end record',
      'dataFormat' => 'd/m/Y',
      'example' => '14/06/2026',
      'required' => TRUE,
    ];
    $fields['User'] = [
      'description' => 'either UCL e-mail address, UPI or UCL user ID',
      'example' => 'uczxmke, uczxmke@ucl.ac.uk or m.keeble.21@ucl.ac.uk',
      'required' => TRUE,
    ];
    $fields['Group ID'] = [
      'description' => 'Group ID',
      'example' => '2134',
      'required' => TRUE,
    ];

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');
    $allowedRoles = $gmrRepositoryService->getAllRolesForType(GroupMembershipRecordType::load(static::groupMembershipRecordType));
    $labels = su_drupal_helper_get_labels_for_entities($allowedRoles);
    $fields['Role'] = [
      'description' => 'Role name (one only, must match exactly)',
      'example' => implode(', ', $labels),
      'required' => TRUE,
      'allowed_values' => $labels,
    ];

    $fields['Action'] = [
      'description' => 'blank or "create" to create new record; "end" to end any existing memberships that match the row using the provided end date; "update" to try to update the existing record matching those dates if found; "delete" to delete but be careful with deleting, there is no way to restore them!',
      'example' => 'create, update, end, delete',
      'allowed_values' => ['', 'update', 'create', 'end', 'delete'],
    ];

    return $fields;
  }

  private static function getStart($row) {
    return DateTime::createFromFormat('d/m/Y', $row['Start']);
  }

  private static function getEnd($row) {
    return DateTime::createFromFormat('d/m/Y', $row['End']);
  }

  private static function getRole($row) {
    $role = trim($row['Role']);
    $role = su_drupal_helper_get_entity_by_name('group_role', $role);
    return GroupRole::load($role->id());
  }

  private static function getUser($row) {
    $service = Drupal::service('su_user.ucl_user');
    return $service->getUserForUclFieldInput($row['User']);
  }

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function getGroupMembershipRecord($row, $action) {
    $user = static::getUser($row);
    $group = static::getGroup($row);
    $role = static::getRole($row);

    /** @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $gmr */
    $gmr = NULL;

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');
    $existingRecords = $gmrRepositoryService->get($user, $group, $role, GroupMembershipRecordType::load('club_society_committee'), static::getStart($row));
    if (count($existingRecords) > 0) {
      if ($action == 'create') {
        static::addError($row, $context, 'User already has ' . count($existingRecords) . ' record(s) with these details for the start date.');
        return 'error';
      }
      elseif ($action == 'end') {
        foreach ($existingRecords as $gmr) {
          $gmr->setEndDate(static::getEnd($row));
          $gmr->save();
        }
        return 'ended';
      }

      if (!isset($row['Action'])) {
        static::addError($row, $context, 'Identical existing record appears to exist; not creating a new one unless told to by setting Action to "create".');
        return 'error';
      }
      if ($action == 'update' && count($existingRecords) > 1) {
        static::addError($row, $context, 'Multiple existing identical records (' . count($existingRecords) . '), not sure which to update.');
        return 'error';
      }
      $gmr = reset($existingRecords);
    }

    if (!$gmr && $action == 'update') {
      static::addError($row, $context, 'Cannot find existing record to update.');
      return 'error';
    }

    if (!$gmr && $action == 'end') {
      static::addError($row, $context, 'Cannot find existing record to end.');
      return 'error';
    }

    if (!$gmr && $action == 'delete') {
      static::addError($row, $context, 'Cannot find existing record to delete.');
      return 'error';
    }

    return $gmr;
  }

  public static function getGroup($row) {
    $group = NULL;
    $groupId = trim($row['Group ID']);
    if (Group::load((int) $groupId)) {
      $group = Group::load((int) $groupId);
    }
    return $group;
  }

  private static function createGroupMembershipRecord(array $row) {
    return GroupMembershipRecord::create([
      'type' => static::groupMembershipRecordType,
      'group_id' => static::getGroup($row)->id(),
      'group_role_id' => static::getRole($row)->id(),
      'user_id' => static::getUser($row)->id(),
      'date_range' => [
        'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, static::getStart($row)
          ->getTimestamp()),
        'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, static::getEnd($row)
          ->getTimestamp()),
      ],
    ]);
  }

  public static function updateGmrFromRow(&$gmr, $row) {
    // Would modify custom fields for the group membership record here.
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_group_gmr_import';
  }

}
