<?php

namespace Drupal\su_groups\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\webform\WebformInterface;
use Laminas\Diactoros\Response\RedirectResponse;

/**
 * Implement Class BulkUserImport for import form.
 */
class WebformSelectAccessGroupForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'su_groups_webform_select_access_group';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = null)
    {
        $form_state->set('webform', $webform);
        $element = $webform->getElementDecoded('webform_access_group');
        $targetTypes = isset($element['#selection_settings']['target_bundles']) ? array_keys($element['#selection_settings']['target_bundles']) : [];

        // Get user's groups
        $options = [];
        $grp_membership_service = \Drupal::service('group.membership_loader');
        $memberships = $grp_membership_service->loadByUser(User::load(\Drupal::currentUser()->id()));
        foreach ($memberships as $membership) {
            $group = $membership->getGroup();
            if (isset($targetTypes)) {
                if (in_array($group->bundle(), $targetTypes)) {
                    $options[$group->id()] = $group->label();
                }
            } else {
                $options[$group->id()] = $group->label();
            }
        }

        $form['markup'] = [
            '#markup' => $this->t('To submit this form, you need to select a group that the form will be submitted for.')
        ];

        $form['webform_access_group'] = [
            '#type' => 'select',
            '#title' => $this->t('Select group'),
            '#options' => $options
        ];

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Continue to form'),
            '#button_type' => 'primary',
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $webform = $form_state->get('webform');
        $url = $webform->toUrl();
        $url->setOption('query', ['webform_access_group' => $form_state->getValue('webform_access_group')]);
        $form_state->setRedirectUrl($url);
        return;
    }
}
