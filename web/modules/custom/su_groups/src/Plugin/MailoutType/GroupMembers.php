<?php

namespace Drupal\su_groups\Plugin\MailoutType;

use DateTime;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\Group;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\mailouts\Plugin\MailoutType\EntityMailoutBase;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\User;
use InvalidArgumentException;

/**
 * Plugin implementation of the mailout_type.
 *
 * @MailoutType(
 *   id = "group_members",
 *   label = @Translation("Group members"),
 *   description = @Translation("Send mail to group members.")
 * )
 */
class GroupMembers extends EntityMailoutBase {
  public $entityTypeId = 'group';

  public function routesForActions() {
    return [
      'entity.group.canonical' => t('Send mailout to members'),
    ];
  }

  public function routesForTasks() {
    return [
      'entity.group.canonical' => t('Mailouts'),
    ];
  }

  public function addFormOptions(&$form, FormStateInterface $form_state, $params = NULL) {
    parent::addFormOptions($form, $form_state, $params);

    $entity_id = $params['entity_id'] ?? NULL;
    if (!$entity_id) {
      return;
    }
    $group = Group::load($entity_id);
    $user = User::load(\Drupal::currentUser()->id());
    if (!$group) {
      if ($user->hasPermission('send global group mailouts')) {
        $enableRoles = TRUE;
        $enableTypes = TRUE;
      } else {
        return FALSE;
      }
    } else {
      $enableRoles = $group->hasPermission('send group mailout by group role', $user);
      $enableTypes = $group->hasPermission('send group mailout by record type', $user);
      if (!$enableRoles && !$enableTypes) {
        return FALSE;
      }
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    if ($form_state->get('allowed_group_types') && count($form_state->get('allowed_group_types')) > 0) {
      $types = $gmrRepositoryService->getTypesInUse(NULL, NULL, $form_state->get('allowed_group_types'));
    } else {
      $types = $gmrRepositoryService->getTypesInUse(NULL, $group);
    }

    if ($enableTypes && $enableRoles) {
      $form['filter_type'] = [
        '#type' => 'select',
        '#title' => "Filter by...",
        '#options' => [
          'types' => 'Membership types',
          'roles' => 'Group roles'
        ],
        '#default_value' => 'types',
        '#multiple' => FALSE,
        '#required' => TRUE,
      ];
    }

    if ($enableTypes) {
      $options = [];
      foreach ($types as $type) {
        $type = GroupMembershipRecordType::load($type);
        if (!$type) {
          continue;
        }
        $options[$type->id()] = $type->label();
      }
      if (count($types) > 0) {
        $states = [];
        if ($enableRoles) {
          $states = [
            'invisible' => array(
              ':input[name="filter_type"]' => array('value' => 'roles'),
            ),
            'disabled' => array(
              ':input[name="filter_type"]' => array('value' => 'roles'),
            )
          ];
        }
        $form['types'] = [
          '#type' => 'checkboxes',
          '#title' => "Membership types to e-mail",
          '#options' => $options,
          '#default_value' => [],
          '#multiple' => TRUE,
          '#states' => $states,
          '#required' => TRUE,
        ];
      }
    }

    if ($enableRoles) {
      $options = [];

      if ($form_state->get('allowed_group_types')) {
        $group_types = $form_state->get('allowed_group_types');
      } else {
        $group_types = $group ? [$group->bundle()] : NULL;
      }
      $roles = $gmrRepositoryService->getAllNonInternalRoles($group_types);
      foreach ($roles as $role) {
        $options[$role->id()] = $role->label();
      }

      if (count($roles) > 0) {
        $states = [];
        if ($enableTypes) {
          $states = [
            'invisible' => array(
              ':input[name="filter_type"]' => array('value' => 'types'),
            ),
            'disabled' => array(
              ':input[name="filter_type"]' => array('value' => 'types'),
            )
          ];
        }
        $form['roles'] = [
          '#type' => 'checkboxes',
          '#title' => "Group roles",
          '#options' => $options,
          '#default_value' => [],
          '#multiple' => TRUE,
          '#states' => $states,
        ];
      }
    }
  }

  public function getRecipientsFromForm($form, FormStateInterface $form_state, $params = NULL) {
    $entity_id = $params['entity_id'] ?? NULL;
    $group = Group::load($entity_id);
    if (!$group) {
      throw new InvalidArgumentException('No group found');
    }

    $filter_type = $form_state->getValue('filter_type');
    $types = $form_state->getValue('types');
    $roles = $form_state->getValue('roles');
    if ($filter_type && $filter_type == 'roles') {
      $types = NULL;
      $form_state->setValue('types', []);
    }

    if ($filter_type && $filter_type == 'types') {
      $roles = NULL;
      $form_state->setValue('roles', []);
    }

    $records = [];
    if (!$roles) {
      $roles = [NULL];
    }
    if (!$types) {
      /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
      $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
      $types = $gmrRepositoryService->getTypesInUse(NULL, $group, $types);
    }

    // Types are always set
    // Roles are always set or set to null
    foreach ($types as $type) {
      if ($type === 0) {
        continue;
      }
      foreach ($roles as $role) {
        if ($role === 0) {
          continue;
        }
        $records = array_merge($records, $this->getForType($group, $type, $role, TRUE, TRUE));
      }
    }

    $emails = [];
    foreach ($records as $record) {
      $emails[] = $this->getEmailFromRecord($record);
    }

    array_unique($emails);
    return $emails;
  }

  public function getConfirmationText() {
    return t('I confirm this e-mail relates to group activity only and is appropriate for its audience.');
  }

  public function getForType($group, $type, $role, $current = TRUE, $enabled = TRUE) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    return $gmrRepositoryService->get(NULL, $group, $role, $type ? GroupMembershipRecordType::load($type) : NULL, $current ? new DateTime() : NULL, $enabled);
  }

  public function getEmailFromRecord($record) {
    if ($record->hasField('field_signup_email') && $record->field_signup_email->value) {
      return $record->field_signup_email->value;
    }

    $user = $record->getUser();
    if ($user) {
      return $user->getEmail();
    }
    return FALSE;
  }

  public function access(AccountInterface $account, array $params = NULL) {
    $parent = parent::access($account, $params);
    if (!$parent->allowed()) {
      return $parent;
    }

    $entity_id = $params['entity_id'] ?? NULL;
    if (!$entity_id) {
      return AccessResult::forbidden();
    }
    $group = Group::load($entity_id);
    return AccessResult::allowedIf($group->hasPermission('send group mailout', $account));
  }
}
