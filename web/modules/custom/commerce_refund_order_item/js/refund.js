(function ($, Drupal, drupalSettings) {

  'use strict';
  /**
   * @type {{attach: Drupal.behaviors.paymentRefund.attach}}
   */
  Drupal.behaviors.paymentRefund = {
    attach: function (context) {

      $(document).ready(function () {
        $('#edit-payment-amount-number, #edit-amount-number').val(0);
      });

      $('#edit-payment-table, #edit-table').change(function () {
        var total = 0;
        $('tr.selected').each(function (key, element) {
          var price = parseFloat($(element).attr('price'));
          total = total + price;
        });
        $('#edit-payment-amount-number, #edit-amount-number').val(total);
      });

    }
  };

})(jQuery, Drupal, drupalSettings);
