<?php

namespace Drupal\commerce_refund_order_item;

use Drupal\commerce_product\Entity\ProductType;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\election\Entity\ElectionPostType;

/**
 * Provides dynamic permissions for Fake of different types.
 *
 * @ingroup election
 *
 */
class RefundPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach (ProductType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    if (\Drupal::service('module_handler')->moduleExists('commerce_shipping')) {
      $perms["shipping refund adjustment payments"] = [
        'title' => $this->t('Refund payments for shipping'),
      ];
    }

    $perms["refund arbitrary amount"] = [
      'title' => $this->t('Refund an arbitrary amount'),
    ];

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\election\Entity\ProductType $type
   *   The ProductType.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(ProductType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id refund payments" => [
        'title' => $this->t('Refund payments for %type_name product type', $type_params),
      ],
    ];
  }
}
