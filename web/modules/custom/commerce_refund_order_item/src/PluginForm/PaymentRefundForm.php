<?php

namespace Drupal\commerce_refund_order_item\PluginForm;

use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\PluginForm\PaymentRefundForm as PaymentRefundFormBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_refund_order_item\RefundFormTrait;
use Drupal\Core\Form\FormStateInterface;
use ReflectionClass;

class PaymentRefundForm extends PaymentRefundFormBase {

  use RefundFormTrait;

  public function getOrder(): Order {
    return Drupal::routeMatch()->getParameter('commerce_order');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $order = $this->getOrder();
    $store = $order->getStore();

    $form['#success_message'] = t('Payment refunded.');

    $this->addRefundTable($form, $this->entity);

    $refundService = Drupal::service('commerce_refund_order_item.refund');

    // @todo work out how to check if gateway does refund it.
    $refundByGateway = TRUE;
    $restoreStock = $refundService->stockControlledByCoreEvents() || $refundService->stockControlledByRefund();
    $shippingOrder = $refundService->getShipmentsForOrder($order) ? TRUE : FALSE;

    $willDo = [
      'reduce the order total price by that amount',
      'add an order item showing what was refunded',
    ];

    $wontDo = [
      'contact the purchaser (unless you choose to send an e-mail)',
    ];

    if ($refundByGateway) {
      $willDo[] = 'immediately refund the money to the user via the payment gateway';
    }
    else {
      $wontDo[] = 'refund the money to the user via the payment gateway';
    }

    if ($restoreStock) {
      $willDo[] = 'restore the stock for the item(s) refunded';
    }
    else {
      $wontDo[] = 'restore the stock for the item(s) refunded';
    }

    if ($shippingOrder) {
      $willDo[] = '(for shipping orders) remove the quantity from any shipments in draft or processing state';
    }

    $this->addWillDoWontDo($form, $willDo, $wontDo, $this->t('Refunding this order will:'), $this->t('This refund will not:'));

    $form['send_refund_email_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Send e-mail regarding refund'),
      'send_refund_email' => [
        '#type' => 'checkbox',
        '#title' => t('Send an e-mail to the purchaser'),
      ],
      'send_refund_email_subject' => [
        '#type' => 'textfield',
        '#title' => $this->t('Subject'),
        '#default_value' => $this->t('Refund processed for order @orderNumber', [
          '@orderNumber' => $order->getOrderNumber(),
          '@storeName' => $store->label(),
        ]),
        '#states' => [
          'visible' => [
            ':input[name="payment[send_refund_email_fieldset][send_refund_email]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      // @todo make default e-mail configurable
      'send_refund_email_message' => [
        '#type' => 'text_format',
        '#title' => $this->t('Message'),
        '#default_value' => $this->t('Hello,<br><br>A refund has been processed for the amount [refund_amount] for order @orderNumber on your behalf.<br><br>Order items refunded:<br><br>[order_items]<br><br>Note you may not receive the money immediately.<br><br>Kind regards,<br><br>@storeName', [
          '@orderNumber' => $order->getOrderNumber(),
          '@storeName' => $store->label(),
        ]),
        '#description' => t('Use [refund_amount] for the refund amount, and [order_items] for a list of what is being refunded.'),
        '#states' => [
          'visible' => [
            ':input[name="payment[send_refund_email_fieldset][send_refund_email]"]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $balance = $payment->getBalance();
    if ($amount->greaterThan($balance)) {
      $form_state->setError($form['amount'], t("Can't refund more than @amount.", ['@amount' => $balance->__toString()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    $itemsSelected = array_filter($values['table']);

    $itemsWithQuantity = $this->submitGetItems($itemsSelected, 'order_items');
    $adjustments = $this->submitGetItems($itemsSelected, 'adjustments');
    $amount = Price::fromArray($values['amount']);

    $formattedOrderItems = [];
    foreach ($itemsWithQuantity as $item_id => $quantity) {
      $formattedOrderItems[] = t('@quantity x @label', [
        '@quantity' => $quantity,
        '@label' => Drupal\commerce_order\Entity\OrderItem::load($item_id)
          ->label(),
      ]);
    }
    foreach ($adjustments as $container => $adjustment) {
      $formattedOrderItems[] = t('@label', [
        '@label' => $adjustment[0],
      ]);
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    $reflectionClass = new ReflectionClass(get_class($payment_gateway_plugin));
    if (!$reflectionClass->hasMethod('refundPayment')) {
      Drupal::logger('commerce_refund_order_item')
        ->alert('Payment gateway does not implement refundPayment method (' . $payment_gateway_plugin->getPluginId() . ')');
      $this->commerceRefundOrderItemRefund->refundPaymentNoGateway($payment, $amount, $itemsWithQuantity, $adjustments);
    }
    else {
      $payment_gateway_plugin->refundPayment($payment, $amount, $itemsWithQuantity, $adjustments);
    }

    // Send mail
    if ($values['send_refund_email_fieldset']['send_refund_email'] == 1) {
      $order = $this->getOrder();
      $store = $order->getStore();

      $to = $order->getEmail();

      $currency_formatter = Drupal::service('commerce_price.currency_formatter');
      $formattedAmount = $currency_formatter->format($amount->getNumber(), $amount->getCurrencyCode());

      $mailManager = Drupal::service('plugin.manager.mail');
      $module = 'commerce_refund_order_item';
      $key = 'commerce_refund_order_item_refund_email';
      $params = [
        'from_email' => $store->getEmail(),
        'from_name' => $store->label(),
        'subject' => $values['send_refund_email_fieldset']['send_refund_email_subject'],
        'body' => $values['send_refund_email_fieldset']['send_refund_email_message']['value'],
        'reply-to' => $store->getEmail(),
        'token_values' => [
          '[refund_amount]' => $formattedAmount,
          '[order_items]' => implode('<br>', $formattedOrderItems),
        ],
      ];
      $langcode = Drupal::currentUser()->getPreferredLangcode();
      $send = TRUE;
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    }
  }

}
