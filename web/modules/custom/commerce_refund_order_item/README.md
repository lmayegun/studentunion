A module that allows you to store refund information as order items on the order.

If you partially refund an order item, this will split the order item in two, leaving the unrefunded quantity as one order item, and creating an extra refunded order item with the refunded details.

This allows for easy reporting/reconciliation with finance systems or payment providers which may want to have e.g. a line for each sale and a separate line for each refund.

