<?php

namespace Drupal\mailouts;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;

/**
 * Base class for mailout_type plugins.
 */
abstract class MailoutTypePluginBase extends PluginBase implements MailoutTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }


  public function routesForActions() {
  }

  /**
   * Show mailout form on page(s)
   *
   * @return array
   *   Array keyed with route ID, values as button label
   */
  public function routesForTasks() {
  }

  public function addFormOptions(&$form, FormStateInterface $form_state, array $params = NULL) {
  }

  public function getRecipientsFromForm($form, FormStateInterface $form_state, array $params = NULL) {
    return [];
  }

  /**
   * @return string
   */
  public function getRouteData() {
    return ['entity_id' => $this->getEntityId()];
  }

  public function getConfirmationText() {
    return NULL;
  }

  /**
   * Return access result for sending mailout.
   *
   * @param AccountInterface $account
   * @param array $params
   *
   * @return AccessResult
   */
  public function access(AccountInterface $account, array $params = NULL) {
    $forType = $account->hasPermission('send mailouts of type ' . $this->getPluginId());
    $forAll = $account->hasPermission('send all mailouts');
    return AccessResult::allowedIf($forAll || $forType);
  }

  public function getBody(&$form, FormStateInterface $form_state) {
    return $form_state->getValue('body')['value'];
  }
}
