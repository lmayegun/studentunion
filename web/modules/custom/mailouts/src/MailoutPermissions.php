<?php

namespace Drupal\mailouts;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Permissions generator for TVI.
 */
class MailoutPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TaxonomyViewsIntegratorPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Get permissions for Taxonomy Views Integrator.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() {
    $permissions = [];

    $plugin_manager = \Drupal::service('plugin.manager.mailout_type');
    $plugins = $plugin_manager->getDefinitions();

    foreach ($plugins as $plugin_id => $plugin) {
      $plugin = $plugin_manager->createInstance($plugin_id);

      $permissions += [
        'send mailouts of type ' . $plugin_id => [
          'title' => $this->t('Send mailouts of type %type', ['%type' => $plugin->label()]),
        ],
      ];
    }

    return $permissions;
  }
}
