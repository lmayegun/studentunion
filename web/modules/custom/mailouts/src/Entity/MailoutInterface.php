<?php

namespace Drupal\mailouts\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Mailout entities.
 *
 * @ingroup mailouts
 */
interface MailoutInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Mailout name.
   *
   * @return string
   *   Name of the Mailout.
   */
  public function getName();

  /**
   * Sets the Mailout name.
   *
   * @param string $name
   *   The Mailout name.
   *
   * @return \Drupal\mailouts\Entity\MailoutInterface
   *   The called Mailout entity.
   */
  public function setName($name);
}
