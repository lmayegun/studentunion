<?php

namespace Drupal\mailouts;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Mailout entities.
 *
 * @ingroup mailouts
 */
class MailoutListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Mailout ID');
    $header['subject'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\group_mailing_lists\Entity\Mailout $entity */
    $row['id'] = $entity->id();
    $row['subject'] = Link::createFromRoute(
      $entity->label(),
      'entity.mailout.edit_form',
      ['mailout' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }
}
