<?php

namespace Drupal\mailouts;

/**
 * Interface for mailout_type plugins.
 */
interface MailoutTypeInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

}
