<?php

namespace Drupal\mailouts\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\mailouts\Plugin\MailoutType\EntityMailoutBase;

class LocalAction extends LocalActionDefault {

  public function getRouteParameters(RouteMatchInterface $route_match) {
    $params = parent::getRouteParameters($route_match);

    $plugin_manager = \Drupal::service('plugin.manager.mailout_type');
    $plugins = $plugin_manager->getDefinitions();

    foreach ($plugins as $plugin_id => $plugin) {
      $plugin = $plugin_manager->createInstance($plugin_id);
      $params = array_merge($params, $plugin->getRouteData());
    }

    return $params;
  }
}
