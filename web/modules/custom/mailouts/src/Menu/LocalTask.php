<?php

namespace Drupal\mailouts\Menu;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class LocalTask extends LocalTaskDefault {

  public function getRouteParameters(RouteMatchInterface $route_match) {
    $params = parent::getRouteParameters($route_match);

    $plugin_manager = \Drupal::service('plugin.manager.mailout_type');
    $plugins = $plugin_manager->getDefinitions();

    foreach ($plugins as $plugin_id => $plugin) {
      $plugin = $plugin_manager->createInstance($plugin_id);
      $params = array_merge($params, $plugin->getRouteData());
    }

    return $params;
  }
}
