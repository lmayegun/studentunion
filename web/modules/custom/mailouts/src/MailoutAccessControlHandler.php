<?php

namespace Drupal\mailouts;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Mailout entity.
 *
 * @see \Drupal\mailouts\Entity\Mailout.
 */
class MailoutAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\mailouts\Entity\MailoutInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view mailouts');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit mailouts');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete mailouts');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'send mailouts');
  }
}
