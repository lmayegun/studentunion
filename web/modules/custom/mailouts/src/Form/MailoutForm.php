<?php

namespace Drupal\mailouts\Form;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\mailouts\Entity\Mailout;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class MailoutForm extends FormBase {

  public $mailoutTypePlugin;

  public $mailoutType;

  /**
   * @return string
   */
  public function getFormId() {
    return 'mailout_send';
  }

  public static function getMailoutTypePlugin($mailout_type) {
    $plugin_manager = \Drupal::service('plugin.manager.mailout_type');
    $plugin = $plugin_manager->createInstance($mailout_type, []);
    return $plugin;
  }

  public function getReferringUrl() {
    $referer = \Drupal::request()->headers->get('referer');
    $base_url = Request::createFromGlobals()->getSchemeAndHttpHost();
    $alias = substr($referer, strlen($base_url));
    // $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($alias);
    return $url_object ?: NULL;
  }

  /**
   * Weight; Used to sort the list of form elements
   * before being output; lower numbers appear before higher numbers.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|RedirectResponse
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $mailout_type = NULL) {
    $params = \Drupal::request()->query->all();
    $form_state->set('params', $params);

    $user = \Drupal::currentUser();
    $name = $user->getDisplayName();
    $email = $user->getEmail();

    // Load plugin and allow it to add its options to filter recipients etc.
    $this->mailoutTypePlugin = static::getMailoutTypePlugin($mailout_type);
    $this->mailoutTypePlugin->addFormOptions($form, $form_state, $params);

    $form['from_email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail address for replies'),
      '#required' => TRUE,
      '#default_value' => $email,
    ];

    $form['from_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name to send the mail from'),
      '#required' => TRUE,
      '#default_value' => $name,
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#required' => TRUE,
      '#default_value' => ''
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('E-mail body'),
      '#required' => TRUE,
      '#format' => 'e_mail_html',
      '#allowed_formats' => ['e_mail_html'],
      '#default_value' => ''
    ];
    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [],
    ];

    $form['exclude_some'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Exclude some e-mail addresses'),
    ];

    $form['excluded'] = [
      '#type' => 'textarea',
      '#title' => $this->t('E-mail addresses to exclude'),
      '#description' => $this->t('Seperate by commas, semicolons or new lines.'),
      '#default_value' => '',
      '#states' => [
        'visible' => [
          ':input[name="exclude_some"]' => ['checked' => TRUE],
        ],
      ],
    ];

    if ($this->mailoutTypePlugin->getConfirmationText()) {
      $form['data'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->mailoutTypePlugin->getConfirmationText(),
        "#required"      => TRUE,
      ];
    }

    $form['actions'] = ['#type' => 'actions', '#weight' => 48,];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Send'),
      '#button_type' => 'primary',
      '#weight' => 48,
    ];

    $form['log_header'] = [
      '#weight' => 49,
      '#markup' => t('<h2>Previous mailouts</h2>')
    ];
    $form['log'] = $this->mailoutList($mailout_type, \Drupal::request()->query->all());

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array                                $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tos = $this->mailoutTypePlugin->getRecipientsFromForm($form, $form_state, $form_state->get('params'));

    // Exclude any excluded addresses
    $excluded = '';
    if ($form_state->getValue('excluded')) {
      $delimiters = [',', ' ', ';', "\r", "\n"];
      $excluded_string = str_replace($delimiters, $delimiters[0], $form_state->getValue('excluded'));
      $excluded = array_filter(explode($delimiters[0], $excluded_string));
      $tos = array_diff($tos, $excluded);
    }

    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'mailouts';
    $key = 'mailout';
    $params = [
      'from_email' => $form_state->getValue('from_email'),
      'from_name' => $form_state->getValue('from_name'),
      'subject' => $form_state->getValue('subject'),
      'body' => $this->mailoutTypePlugin->getBody($form, $form_state),
      'reply-to' => $form_state->getValue('from_email'),
      'token_values' => [],
    ];
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    foreach ($tos as $to) {
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    }

    $mailout_type = $this->mailoutTypePlugin->getPluginId();

    $mailout = Mailout::create([
      'user_id' => \Drupal::currentUser()->id(),
      'mailout_type' => $mailout_type,
      'body' => ['value' => $params['body'], 'format' => 'e_mail_html'],
      'subject' => $params['subject'],
      'from_name' => $params['from_name'],
      'from_email' => $params['from_email'],
      'recipients' => $tos,
      'recipients_count' => count($tos),
      'created' => strtotime('now'),
      'excluded' => $excluded,
      'data' => \Drupal::request()->query->all(),
      'hash' => $this->generateHash($mailout_type, \Drupal::request()->query->all()),
    ]);
    $mailout->save();

    $params['subject'] = '[MAILOUT COPY]: ' . $params['subject'];
    $result = $mailManager->mail($module, $key, $params['from_email'], $langcode, $params, NULL, $send);

    \Drupal::messenger()->addMessage("Sent to " . count($tos) . " recipients.");
  }

  public static function getTitle($mailout_type = NULL) {
    $plugin = static::getMailoutTypePlugin($mailout_type);
    return t('Send mailout: @label', ['@label' => $plugin->label()]);
  }

  public function access(AccountInterface $account, string $mailout_type = NULL) {
    if ($mailout_type) {
      $params = $params = \Drupal::request()->query->all();
      $plugin = static::getMailoutTypePlugin($mailout_type);
      return $plugin->access($account, $params);
    }
    return AccessResult::allowedIfHasPermission($account, 'send mailouts of type ' . $mailout_type);
  }

  public function generateHash($mailout_type, $params) {
    return md5($mailout_type . serialize($params));
  }

  public function mailoutList($mailout_type, $params) {
    $hash = $this->generateHash($mailout_type, $params);
    // dd($mailout_type, $params, $hash);
    $view_render = views_embed_view('mailouts', 'embed', $hash);
    $view_render['#weight'] = 50;
    return $view_render;
  }
}
