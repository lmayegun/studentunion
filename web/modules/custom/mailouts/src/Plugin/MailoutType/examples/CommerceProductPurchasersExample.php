<?php

namespace Drupal\mailouts\Plugin\MailoutType;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailouts\MailoutTypePluginBase;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the mailout_type.
 *
 * (
 *   id = "purchasers",
 *   label = @Translation("Product purchasers"),
 *   description = @Translation("Send mail to product purchasers, optionally by variation.")
 * )
 */
class CommerceProductPurchasersExample extends EntityMailoutBase {
  public $entityTypeId = 'commerce_product';

  public function routesForActions() {
    return [
      'entity.commerce_product.canonical' => t('Send mailout to customers')
    ];
  }

  public function routesForTasks() {
    return [
      'entity.commerce_product.canonical' => t('Mailouts to customers')
    ];
  }

  public function addFormOptions(&$form, FormStateInterface $form_state, $params = NULL) {
    parent::addFormOptions($form, $form_state, $params);

    $entity_id = $params['entity_id'] ?? NULL;
    $commerce_product = Product::load($entity_id);

    $options = [];
    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      if (!$variation) {
        continue;
      }
      $options[$variation->id()] = $variation->label();
    }

    if (count($variations) > 0) {
      $states = [];
      $form['variations'] = [
        '#type' => 'checkboxes',
        '#title' => t("Select the variations for which to e-mail the purchasers"),
        '#options' => $options,
        '#default_value' => count($variations) == 1 ? [reset($variations)->id()] : [],
        '#multiple' => TRUE,
        '#states' => $states,
      ];
    }
  }

  public function getRecipientsFromForm($form, FormStateInterface $form_state, $entity_id = NULL) {
    $variation_ids = $form_state->getValue('variations');

    // Get mail for all completed orders containing this variation:
    $query = \Drupal::database()->select('commerce_order_item', 'i');
    $query->join('commerce_order', 'o', 'i.order_id = o.order_id');
    $query->fields('o', ['mail'])
      ->condition('i.purchased_entity', $variation_ids, 'IN')
      ->condition('i.quantity', 0, ">")
      ->condition('o.state', 'completed')
      ->distinct();
    $mails = $query->execute()->fetchCol();
    return $mails;
  }

  public function getConfirmationText() {
    return t('I confirm this e-mail relates to the purchased product only and is appropriate for its audience.');
  }
}
