<?php

namespace Drupal\mailouts\Plugin\MailoutType;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailouts\MailoutTypePluginBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Base for entity mailouts.
 *
 */
abstract class EntityMailoutBase extends MailoutTypePluginBase {

  public $entityTypeId;

  public function routesForActions() {
    return [
      'entity.' . $this->entityTypeId . '.canonical' => t('Send mailout')
    ];
  }

  public function routesForTasks() {
    return [
      'entity.' . $this->entityTypeId . '.canonical' => t('Mailouts')
    ];
  }

  public function getRouteData() {
    return [
      'entity_id' => $this->getEntityId(),
      'entity_type_id' => $this->entityTypeId,
    ];
  }


  public function addFormOptions(&$form, FormStateInterface $form_state, array $params = NULL) {
    $entity_id = $params['entity_id'] ?? NULL;
    if (!$entity_id) {
      $form['#access'] = FALSE;
      return;
    }

    $entity = \Drupal::entityTypeManager()->getStorage($this->entityTypeId)->load($entity_id);
    if (!$entity) {
      $form['#access'] = FALSE;
      return;
    }
  }

  public function getRecipientsFromForm($form, FormStateInterface $form_state, array $params = NULL) {
    return [];
  }

  public function getEntityId() {
    return $this->getRouteEntity() ? $this->getRouteEntity()->id() : NULL;
  }

  /**
   * Returns the entity of the current route.
   *
   * @return Drupal\Core\Entity\EntityInterface
   *   The entity or NULL if this is not an entity route.
   */
  public static function getRouteEntity() {
    $route_match = \Drupal::routeMatch();
    $route_name = $route_match->getRouteName();

    if ($route_match->getParameter('view_id') && $route_match->getParameter('node')) {
      $node = $route_match->getParameter('node');
      if ($node instanceof Node) {
        return $route_match->getParameter('node');
      } elseif (gettype($node) == 'string') {
        return Node::load($route_match->getParameter('node'));
      }
    }

    // Look for a canonical entity view page, e.g. node/{nid}, user/{uid}, etc.
    $matches = [];
    preg_match('/entity\.(.*)\.(latest[_-]version|canonical)/', $route_name, $matches);
    if (!empty($matches[1])) {
      $entity_type = $matches[1];
      return $route_match->getParameter($entity_type);
    }

    // Look for a rest entity view page, e.g. "node/{nid}?_format=json", etc.
    $matches = [];
    // Matches e.g. "rest.entity.node.GET.json".
    preg_match('/rest\.entity\.(.*)\.(.*)\.(.*)/', $route_name, $matches);
    if (!empty($matches[1])) {
      $entity_type = $matches[1];
      return $route_match->getParameter($entity_type);
    }

    // Look for entity object 'add' pages, e.g. "node/add/{bundle}".
    $route_name_matches = [];
    preg_match('/(entity\.)?(.*)\.add(_form)?/', $route_name, $route_name_matches);
    if (!empty($route_name_matches[2])) {
      $entity_type = $route_name_matches[2];
      $definition = \Drupal::entityTypeManager()->getDefinition($entity_type, FALSE);
      if (!empty($definition)) {
        $type = $route_match->getRawParameter($definition->get('bundle_entity_type'));
        if (!empty($type)) {
          return \Drupal::entityTypeManager()
            ->getStorage($entity_type)
            ->create([
              $definition->get('entity_keys')['bundle'] => $type,
            ]);
        }
      }
    }

    // Look for entity object 'edit' pages, e.g. "node/{entity_id}/edit".
    $route_name_matches = [];
    preg_match('/entity\.(.*)\.edit_form/', $route_name, $route_name_matches);
    if (!empty($route_name_matches[1])) {
      $entity_type = $route_name_matches[1];
      $entity_id = $route_match->getRawParameter($entity_type);

      if (!empty($entity_id)) {
        return \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->load($entity_id);
      }
    }

    // Look for entity object 'add content translation' pages, e.g.
    // "node/{nid}/translations/add/{source_lang}/{translation_lang}".
    $route_name_matches = [];
    preg_match('/(entity\.)?(.*)\.content_translation_add/', $route_name, $route_name_matches);
    if (!empty($route_name_matches[2])) {
      $entity_type = $route_name_matches[2];
      $definition = \Drupal::entityTypeManager()->getDefinition($entity_type, FALSE);
      if (!empty($definition)) {
        $node = $route_match->getParameter($entity_type);
        $type = $node->bundle();
        if (!empty($type)) {
          return \Drupal::entityTypeManager()
            ->getStorage($entity_type)
            ->create([
              $definition->get('entity_keys')['bundle'] => $type,
            ]);
        }
      }
    }

    // Special handling for the admin user_create page. In this case, there's only
    // one bundle and it's named the same as the entity type, so some shortcuts
    // can be used.
    if ($route_name == 'user.admin_create') {
      $entity_type = $type = 'user';
      $definition = \Drupal::entityTypeManager()->getDefinition($entity_type);
      if (!empty($type)) {
        return \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->create([
            $definition->get('entity_keys')['bundle'] => $type,
          ]);
      }
    }

    // Entity will be found in the route parameters.
    $route_match = \Drupal::routeMatch();
    // Entity will be found in the route parameters.
    if (($route = $route_match->getRouteObject()) && ($parameters = $route->getOption('parameters'))) {
      // Determine if the current route represents an entity.
      foreach ($parameters as $name => $options) {
        if (isset($options['type']) && strpos($options['type'], 'entity:') === 0) {
          $entity = $route_match->getParameter($name);
          if ($entity instanceof ContentEntityInterface && $entity->hasLinkTemplate('canonical')) {
            return $entity;
          }

          // Since entity was found, no need to iterate further.
          return NULL;
        }
      }
    }

    return NULL;
  }
}
