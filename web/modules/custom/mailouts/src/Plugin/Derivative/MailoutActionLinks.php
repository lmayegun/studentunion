<?php

namespace Drupal\mailouts\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Provides dynamic tabs based on node types.
 */
class MailoutActionLinks extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $plugin_manager = \Drupal::service('plugin.manager.mailout_type');
    $plugins = $plugin_manager->getDefinitions();

    foreach ($plugins as $plugin_id => $plugin) {
      $plugin = $plugin_manager->createInstance($plugin_id);
      $tasks = $plugin->routesForActions();
      foreach ($tasks as $route => $label) {
        $id = $plugin_id . $route;
        $this->derivatives[$id] = $base_plugin_definition;
        $this->derivatives[$id]['title'] = $label;
        $this->derivatives[$id]['appears_on'] = [$route];
        $this->derivatives[$id]['route_name'] = 'mailouts.send_form';
        $this->derivatives[$id]['route_parameters'] = array_merge([
          'mailout_type' => $plugin_id,
        ], $plugin->getRouteData());
      }
    }

    return $this->derivatives;
  }
}
