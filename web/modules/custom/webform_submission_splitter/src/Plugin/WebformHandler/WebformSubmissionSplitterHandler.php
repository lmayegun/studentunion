<?php

namespace Drupal\webform_submission_splitter\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Splits a webform submission.
 *
 * @WebformHandler(
 *   id = "webform_submission_splitter",
 *   label = @Translation("Split/duplicate based on multi-value field"),
 *   category = @Translation("Submissions"),
 *   description = @Translation("Split/duplicate based on multi-value field."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 *   tokens = TRUE,
 * )
 */
class WebformSubmissionSplitterHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'element_to_duplicate_on' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Get webform elements:
    $webform = $this->webform;
    $elements = $webform->getElementsDecodedAndFlattened();

    //Find only multivalue elements:
    $options = [];
    foreach ($elements as $key => $element) {
      if (isset($element["#multiple"]) && $element["#multiple"]) {
        $options[$key] = $element['#title'];
      }
    }

    $form['element_to_duplicate_on'] = [
      '#type' => 'select',
      '#title' => $this->t('Element to use to split the form'),
      '#default_value' => $this->configuration['element_to_duplicate_on'],
      '#options' => $options,
      '#required' => TRUE,
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['element_to_duplicate_on'] = $form_state->getValue('element_to_duplicate_on');
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if ($update) {
      return;
    }

    $element = $this->configuration['element_to_duplicate_on'];
    $values = $webform_submission->getElementData($element);

    $firstDone = FALSE;
    foreach ($values as $value) {
      if (!$firstDone) {
        $submission = $webform_submission;
        $firstDone = TRUE;
      } else {
        $submission = $webform_submission->createDuplicate();
      }

      // Set submission element value to this element value:
      $submission->setElementData($element, [$value]);

      // Save:
      $submission->save();
    }
  }
}
