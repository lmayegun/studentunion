CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Use
 * Maintainers


INTRODUCTION
------------

The Webform Submission Splitter module adds a webform handler
that allows you to select a multiple value element
and then "split" the submission by that element's values.

This clones the submission as many times as that element has values,
and sets the value of each cloned submission to one of those values.

For example, if you have a multiple e-mail element and the user submits
with four e-mail addresses, if you have set up this handler for that field,
you will end up with four submissions, not just one.

- Original submission:
  - Submission ID 1
    - Element e-mail =
      - one@drupal.org
      - two@drupal.org
      - three@drupal.org
      - four@drupal.org

- After handler:
  - Submission ID 1
    - Element e-mail =
      - one@drupal.org
  - Submission ID 2
    - Element e-mail =
      - two@drupal.org
  - Submission ID 3
    - Element e-mail =
      - three@drupal.org
  - Submission ID 4
    - Element e-mail =
      - four@drupal.org

 * For a full description of the module visit:
   https://www.drupal.org/project/webform_submission_splitter

 * To submit bug reports and feature suggestions, or to track changes
   visit: https://www.drupal.org/project/issues/webform_submission_splitter


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Webform Submission Splitter module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


USE
---

    1. Add as a webform handler under "Handlers / Emails" on your webform settings.
    2. Select the multiple value field to split bu.
    3. Save.


MAINTAINERS
-----------

 * Maxwell Keeble (maxwellkeeble) - https://www.drupal.org/u/maxwellkeeble
