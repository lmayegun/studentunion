<?php

namespace Drupal\commerce_ticketing_scanner\Controller;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ScannerController.
 */
class ScanningEndpointsController extends ControllerBase {

  /**
   * Load a ticket entity from a uuid.
   *
   * @param string $ticket_uuid
   *
   * @return CommerceTicket
   */
  private function getTicket(string $ticket_uuid) {
    $ticket_loaded_by_uuid = \Drupal::entityTypeManager()->getStorage('commerce_ticket')->loadByProperties(['uuid' => $ticket_uuid]);
    return reset($ticket_loaded_by_uuid);
  }

  /**
   * Check if ticket available.
   */
  public function check(Product $commerce_product, ProductVariation $commerce_product_variation = NULL, $ticket_uuid) {
    $ticket = $this->getTicket($ticket_uuid);
    if (!$ticket) {
      return ['error' => '500'];
    }

    if ($commerce_product_variation) {
      $for_this_event = $ticket->getPurchasedEntity()->id() == $commerce_product_variation->id();
    } else {
      $for_this_event = $ticket->getPurchasedEntity()->getProduct()->id() == $commerce_product->id();
    }

    if (!$for_this_event) {
      return ['error' => 'not for this event'];
    }

    $orderState = $ticket->getOrder()->getState()->getId();
    $completedNotCancelled = $orderState != 'canceled';
    if (!$completedNotCancelled) {
      return ['error' => 'order cancelled'];
    }

    $order = $ticket->getOrder();
    return [
      'state' =>  $ticket->getState()->getId(),
      'email' => $order->getEmail()
    ];
  }

  /**
   * Check the ticket and return Json response.
   */
  public function getCheck(Product $commerce_product, ProductVariation $commerce_product_variation, $ticket_uuid) {
    $check = $this->check($commerce_product, $commerce_product_variation, $ticket_uuid);
    return new JsonResponse($check);
  }

  /**
   * Check the ticket and return Json response.
   */
  public function getCheckAll(Product $commerce_product, $ticket_uuid) {
    $check = $this->check($commerce_product, NULL, $ticket_uuid);
    return new JsonResponse($check);
  }

  public function setStatusNoVariation(Product $commerce_product, $ticket_uuid, $new_state) {
    return $this->setStatus($commerce_product, NULL, $ticket_uuid, $new_state);
  }

  /**
   * Set used or unused.
   */
  public function setStatus(Product $commerce_product, $commerce_product_variation = NULL, $ticket_uuid = NULL, $new_state = NULL) {
    $ticket = $this->getTicket($ticket_uuid);
    if (!$ticket) {
      return new JsonResponse([], 500);
    }

    $check = $this->check($commerce_product, $commerce_product_variation, $ticket_uuid);
    if (isset($check['error'])) {
      return new JsonResponse($check);
    }

    $ticket_state = $ticket->getState();
    $ticket_state_transitions = $ticket_state->getTransitions();

    if ($new_state == 'used') {
      if (!empty($ticket_state_transitions['use'])) {
        $ticket_state->applyTransition($ticket_state_transitions['use']);
        $ticket->save();
        return new JsonResponse(['state' => 'used']);
      } else {
        return new JsonResponse(['error' => 'cannot set as used']);
      }
    } elseif ($new_state == 'active') {
      if (!empty($ticket_state_transitions['unuse'])) {
        $ticket_state->applyTransition($ticket_state_transitions['unuse']);
        $ticket->save();
        return new JsonResponse(['state' => 'active']);
      } else {
        return new JsonResponse(['error' => 'cannot set to active']);
      }
    }

    return new JsonResponse(['error' => 'no transitions available']);
  }
}
