<?php

namespace Drupal\su_students\Service;

use DateTime;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;
use Drupal\webform\Entity\Webform;
use PDO;

/**
 * Implements Class StudentService Controller.
 */
class StudentService {

  /**
   * Constructs a new SuLoginOauth2Service object.
   */
  public function __construct() {
  }

  /**
   * Load student profile by user entity
   *
   * @param      $user
   * @param bool $createProfile
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function getStudentProfile($user, $createProfile = FALSE) {
    $profile = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser($user, 'student');
    if (!$profile && $createProfile) {
      $profile = StudentService::createStudentProfile($user);
    }
    return $profile;
  }

  public static function isParttime(Profile $student) {
    return $student->field_student_attendance_mode->entity && $student->field_student_attendance_mode->entity->label() == 'Part time';
  }

  public static function isUndergrad(Profile $student) {
    return $student->field_student_study_mode->entity && $student->field_student_study_mode->entity->label() == 'Undergraduate';
  }

  /**
   * Return TRUE if user is student (regardless of opt-in status)
   *
   * @param $user
   * @param $study_mode_to_check could be the taxonomy term full name, or the code (PGT, PGR, UG)
   * @return false
   */
  public static function isStudyMode($user, $study_mode_to_check) {
    if (!self::isStudent(($user))) {
      return;
    }

    if ($profile = self::getStudentProfile($user)) {
      $study_mode = $profile->get('field_student_study_mode')->entity->getName();
      if ($study_mode == $study_mode_to_check) {
        return TRUE;
      }
      $studentDataDefinitions = \Drupal::service('su_students.data_definitions');
      $definitions = array_flip($studentDataDefinitions->getMappedNameStudyMode());
      if ($definitions[$study_mode] == $study_mode_to_check) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Return TRUE if user is student (regardless of opt-in status)
   *
   * @param $user
   * @return false
   */
  public static function isStudent($user) {
    if (in_array('student', $user->getRoles())) {
      if ($profile = self::getStudentProfile($user)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Create single student profile
   *
   * @param      $user
   * @param bool $optin
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createStudentProfile($user, $optin = FALSE) {
    $profile = Profile::create([
      'type' => 'student',
      'uid'  => $user->id(),
    ]);
    if ($optin) {
      static::optInStudent($user);
    }
    $profile->save();
    return $profile;
  }

  /**
   * Delete single student profile
   *
   * @param $user
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function deleteStudentProfile($user) {
    $profile = StudentService::getStudentProfile($user);
    return $profile->delete();
  }

  /**
   * Return TRUE if user is student and has opted in, FALSE otherwise
   *
   * @param $user
   * @return false
   */
  public static function isStudentOptin($user) {
    $optin = FALSE;
    if (in_array('student', $user->getRoles())) {
      if ($profile = self::getStudentProfile($user)) {
        $optin = $profile->get('field_profile_optin')->value === '1';
      }
    }
    return $optin;
  }

  /**
   * Return TRUE if user is student and has opted out, FALSE otherwise
   *
   * @param $user
   * @return bool
   */
  public static function isStudentOptout($user) {
    $optout = FALSE;
    if (in_array('student', $user->getRoles())) {
      if ($profile = self::getStudentProfile($user)) {
        $optout = $profile->get('field_profile_optin')->value == 0;
      }
    }
    return $optout;
  }

  /**
   * Returns TRUE if student is over 18, FALSE otherwise
   *
   * @param $user
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function isStudentOver18($user) {
    $age = StudentService::getStudentAge($user);
    if (!$age) {
      return NULL;
    }
    return StudentService::getStudentAge($user) >= 18;
  }

  /**
   * Returns TRUE if student is over 18, FALSE otherwise
   *
   * @param $user
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function haveStudentDataForUser($user) {
    $age = StudentService::getStudentAge($user);
    $haveAge = $age ? TRUE : FALSE;

    // $profile = StudentService::getStudentProfile($user);
    // $haveStudentNumber = $profile->get('field_student_number')->value;

    return $haveAge;
  }

  /**
   * Returns TRUE if student is over 18, FALSE otherwise
   *
   * @param $user
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function haveStudentDataForProfile($profile) {
    $time = strtotime($profile->get('field_student_date_of_birth')->value);
    return $time !== FALSE && !is_null($time);
  }


  /**
   * Return student age
   *
   * @param $user
   * @return false|int|mixed|string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function getStudentAge($user) {
    $profile = StudentService::getStudentProfile($user);
    if (!$profile) {
      return NULL;
    }
    $time = strtotime($profile->get('field_student_date_of_birth')->value);
    if (is_null($time)) {
      return NULL;
    }

    $birthDate = DateTime::createFromFormat('U', $time);
    if (!$birthDate) {
      return NULL;
    }

    $now = new DateTime();
    $interval = $now->diff($birthDate);
    return $interval->y;
  }

  public static function filterOutNonRecentStudents($students) {
    $final = [];
    foreach ($students as $student) {
      $student = Profile::load($student);
      if ($student->getOwner()->getLastAccessedTime() >= strtotime('-2 month')) {
        $final[] = $student->id();
      }
    }
    return $final;
  }

  /**
   * Get all students ready to optin
   * i.e. optin = true AND we have no student data
   *
   * @param bool $optin
   * @return array|int
   */
  public static function getUPIsToOptInOptOut($optin = TRUE) {
    $students = static::getAllOptinOptoutStudents($optin);

    // Temporary 2021-12:
    if (count($students) > 200) {
      $students = static::filterOutNonRecentStudents($students);
    }

    // Temporary 2021-12: to avoid overloading Portico
    $total = random_int(250, 350);
    if (count($students) > $total) {
      $students = array_slice($students, 0, $total);
    }

    $list = [];
    $type = $optin ? 'optin' : 'optout';
    \Drupal::logger('su_students')->debug('Preparing ' . count($students) . ' students to ' . $type);
    foreach ($students as $student) {
      $student = Profile::load($student);
      $user = $student->getOwner();

      if (!$user->hasRole('ucl_authenticated')) {
        continue;
      }

      $upi = $user->get('field_upi')->value;

      $query = \Drupal::service('webform_query');
      $query->addCondition('field_profile_optin', $optin ? TRUE : FALSE);
      $query->addCondition('upi', $upi);
      $query->setWebform('opt_in_to_membership');
      $sids = array_column($query->execute(), 'sid');
      if (count($sids) == 0) {
        continue;
      }

      if (!$upi) {
        $upi = $user->field_ucl_user_id->value;
      }

      if ($upi) {
        $list[] = $upi;

        $field = $optin ? 'field_optin_sent_to_portico' : 'field_optout_sent_to_portico';
        $student->set($field, date('Y-m-d\TH:i:s', strtotime('now')));
        $student->save();
      }
    }

    sort($list);

    \Drupal::logger('su_students')->debug('Sending ' . count($list) . ' students to ' . $type . ' to portico.');
    return $list;
  }

  /**
   * Get all students, that want to optin or optout
   * In this case we are using the date of birth
   * If we optin we need to check that we have not send it before
   *
   * @param bool $optin
   * @param bool $opt_in_sent_to_portico
   * @return array|int
   */
  public static function getAllOptinOptoutStudents($optin = TRUE) {
    // Get date:
    $date = new DrupalDateTime('now');
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    // Query:
    $query = \Drupal::entityQuery('profile')
      ->accessCheck(FALSE);
    $query->condition('type', 'student');
    $string = $optin ? '1' : '0';
    $query->condition('field_profile_optin', $string, '=');

    if (!$optin) {
      // Temporarily prevent false positives:
      $monthAgo = new DateTime('-3 days');
      $query->exists('field_student_date_of_birth');
      $query->condition('field_profile_optin_changed', $monthAgo->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), '>');
    }

    if ($optin) {
      // $field_study_end_condition = $query->orConditionGroup()
      //   ->condition('field_student_study_end', $formatted, '>')
      //   ->notExists('field_student_study_end');
      // $query->condition($field_study_end_condition);

      $or = $query->orConditionGroup()
        ->notExists('field_student_date_of_birth')
        ->notExists('field_student_study_end');
      $query->condition($or);

      // $query->notExists('field_optin_sent_to_portico');
    }
    $result =  $query->execute();
    return $result;
  }

  /**
   * Get all users with student role
   * e.g  [4491] => 4491
   *
   * @return array|int
   */
  public static function getAllStudentsByStudentRole() {
    if ($cache = \Drupal::cache()->get('su_students_all_students_ids_by_role')) {
      $students = $cache->data;
    } else {
      $students = \Drupal::entityQuery('user')
        ->condition('status', 1)
        ->condition('roles', 'student')
        ->execute();
      \Drupal::cache()->set('su_students_all_students_ids_by_role', $students, 60 * 60 * 24);
    }
    return $students;
  }

  /**
   * @param Profile $profile
   *
   * @return [type]
   */
  public static function isStudentCurrentMember(Profile $profile = NULL) {
    if (!$profile) {
      return false;
    }

    // Allow study end date - grace period of 1 month
    $gracePeriod = 60 * 60 * 24 * 30;
    if (!$profile->field_student_study_end->value) {
      return FALSE;
    }
    $studyEnd = strtotime($profile->field_student_study_end->value);
    $inGracePeriod = ($studyEnd + $gracePeriod) >= strtotime('now');
    if ($profile->field_profile_optin->value == 1 && $inGracePeriod) {
      return TRUE;
    }
    if (!$profile->field_profile_optin->value) {
      // \Drupal::logger('su_students')->debug("Student profile opted out " . $profile->id());
    }
    if (!$inGracePeriod) {
      // \Drupal::logger('su_students')->debug("Student profile not in grace period " . $profile->field_student_study_end->value . " - " . $profile->id());
    }
    return FALSE;
  }

  /**
   * @param mixed $user
   *
   * @return [type]
   */
  public static function updateOptinRoles(&$user, $save = TRUE) {
    $user = User::load($user->id());
    $profile = static::getStudentProfile($user);

    if (!$profile || !static::isStudentCurrentMember($profile)) {
      // if (!$profile) {
      //   \Drupal::logger('su_students')->debug("Student has no profile - removing member role " . $user->id());
      // }
      // if (!static::isStudentCurrentMember($profile)) {
      //   \Drupal::logger('su_students')->debug("Student is not current - removing member role " . $user->id());
      // }
      $user->removeRole('member');
      // $user->removeRole('student');
    } else {
      $user->addRole('member');
      $user->addRole('student');
    }
    if ($save) {
      $user->save();
    }
  }

  /**
   * @param $user
   * @return mixed
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function optInStudent(&$user, &$profile = NULL, $save = TRUE) {

    if (!$profile) {
      $profile = static::getStudentProfile($user);
    }

    if ($profile) {
      $profile->set('field_profile_optin', TRUE);
      $profile->set('field_profile_optin_changed', \Drupal::time()->getRequestTime());
      $profile->set('field_date_to_anonymise', NULL);
      $profile->set('status', 1);
      if ($save) {
        $profile->save();
      }

      $hadMemberRole = $user->hasRole('member');
      // E-mail student if newly opted in.
      if (!$hadMemberRole && $profile->get('field_profile_optin_email_sent')->value == 0) {
        su_students_send_optin_data_recieved_mail($user, $profile);
      }
      static::updateOptinRoles($user, $save);
    }
  }

  /**
   * @param $user
   * @return mixed
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function optOutAndScheduleAnonymiseStudent($user) {
    $profile = self::getStudentProfile($user);
    if ($profile) {
      // Should not be opted out - they have not chosen to leave the data, necessarily,
      // Just stopped being a member.
      // $profile->set('field_profile_optin', FALSE);
      // $profile->set('field_profile_optin_changed', \Drupal::time()->getRequestTime());

      $yearService = \Drupal::service('date_year_filter.year');
      $endOfAcademicYear = $yearService->getYearForDate(new DateTime(), '2022-09-01')->end;
      $expiry = $endOfAcademicYear->getTimestamp();
      $expiry = gmdate(DateTimeItemInterface::DATE_STORAGE_FORMAT, $expiry);
      $profile->set('field_date_to_anonymise', $expiry);
      $profile->set('status', 0);

      $profile->save();
    }
    static::updateOptinRoles($user);
  }

  public static function getStudentProfileIdsScheduledToAnonymise() {
    $query = \Drupal::entityQuery('profile')
      ->accessCheck(FALSE);
    $query->condition('type', 'student');
    $query->condition('status', 1);
    $query->condition('field_date_to_anonymise', strtotime('now'), '<');
    $query->condition('field_date_to_anonymise', strtotime('-2 years'), '>');
    return $query->execute();
  }

  /**
   * @param $user
   * @return mixed
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function anonymiseStudent($user) {
    $profile = self::getStudentProfile($user);
    if ($profile) {
      // Should not be opted out - they have not chosen to leave the data, necessarily,
      // Just stopped being a member.
      // $profile->set('field_profile_optin', FALSE);
      // $profile->set('field_profile_optin_changed', \Drupal::time()->getRequestTime());
      $profile->set('status', 0);
      $profile->save();

      $userService = \Drupal::service('su_user.ucl_user');
      $userService->anonymiseProfile($profile);
    }
    static::updateOptinRoles($user);
  }

  /**
   * Remove student profiles without users
   * e.g.  [485] => Array ( [profile_id] => 485 [uid] => 86822 )
   *
   * @param $studentProfile
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteProfileWithoutUser($studentProfile) {
    if ($user = User::load($studentProfile['uid'])) {
      $profile = Profile::load($studentProfile['profile_id']);
      $profile->delete();
    }
  }

  /**
   * @param $vid
   */
  public function deleteTaxonomyTerms($vid) {
    // Load IDs
    $result = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $vid)
      ->execute();

    // Delete
    $storage_handler = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $entities = $storage_handler->loadMultiple($result);
    $storage_handler->delete($entities);
  }

  public static function addOptinRequest($user) {
    $values = [
      'webform_id' => 'opt_in_to_membership',
      'entity_type' => NULL,
      'entity_id' => NULL,
      'in_draft' => FALSE,
      'uid' => $user->id(),
      'langcode' => 'en',
      'remote_addr' => '',
      'data' => [
        'i_confirm_i_am_a_current_ucl_student_' => 1,
        'field_profile_optin' => 1,
        'upi' => $user->field_upi,
      ],
    ];

    // Check webform is open.
    $webform = Webform::load($values['webform_id']);
    $is_open = WebformSubmissionForm::isOpen($webform);

    if ($is_open === TRUE) {
      // Validate submission.
      $errors = WebformSubmissionForm::validateFormValues($values);

      // Check there are no validation errors.
      if (!empty($errors)) {
        // print_r($errors);
      } else {
        // Submit values and get submission ID.
        $webform_submission = WebformSubmissionForm::submitFormValues($values);
      }
    }
  }
}
