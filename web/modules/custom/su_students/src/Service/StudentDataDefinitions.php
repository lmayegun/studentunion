<?php

namespace Drupal\su_students\Service;

use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;
use PDO;

/**
 * Implements Class StudentService Controller.
 */
class StudentDataDefinitions {

  /**
   * Lists fields we store on the student profile, including their sources (e.g. UCL CSV)
   *
   * For taxonomy references (i.e. field on Student profile is a taxonomy field):
   *
   * 'drupal_taxonomy'              = taxonomy machine name - taxonomy must have field_ucl_code as a field
   * 'drupal_taxonomy_create_entry' = create a new taxonomy entry to match the UCL code
   *
   * @return array
   */
  public static function getFieldDefinitions() {
    $fields = [];

    // From UCL

    // For user entity
    $fields[] = [
      'source'                  => 'ucl',
      'name_ucl_csv'            => 'Surname',
      'name_ucl_webservice'     => 'surname',
      'name_drupal'             => 'field_last_name',
      'description'             => 'Surname (e.g. Davies)',
      'drupal_entity'           => 'User',
      'function_clean_from_csv' => function ($field, $value) {
        return mb_convert_case($value, MB_CASE_TITLE);
      },
      'permission'              => 'view id student data'
    ];

    $fields[] = [
      'source'                  => 'ucl',
      'name_ucl_csv'            => 'First Name',
      'name_ucl_webservice'     => 'forename1',
      'name_drupal'             => 'field_first_name',
      'drupal_entity'           => 'User',
      'description'             => 'First name (e.g. Alexandra)',
      'function_clean_from_csv' => function ($field, $value) {
        return mb_convert_case($value, MB_CASE_TITLE);
      },
      'permission'              => 'view id student data'
    ];

    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'UCL Email',
      'name_ucl_webservice' => 'uclEmail',
      'name_drupal'         => 'mail',
      'drupal_entity'       => 'User',
      'description'         => 'Email address  (e.g. e.windsor@ucl.ac.uk)',
      'permission'          => 'view id student data'
    ];

    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'UPI',
      'name_ucl_webservice' => 'upi',
      'name_drupal'         => 'field_upi',
      'drupal_entity'       => 'User',
      'description'         => 'UPI (e.g. WINDE01)',
      'permission'          => 'view id student data'
    ];

    $fields[] = [
      'source'                  => 'ucl',
      'name_ucl_csv'            => 'Username',
      'name_ucl_webservice'     => 'username',
      'name_drupal'             => 'field_ucl_user_id',
      'description'             => 'User ID (e.g. zcxtm23, zcxasdf)',
      'drupal_entity'           => 'User',
      'function_clean_from_csv' => function ($field, $value) {
        return strtolower($value);
      },
      'permission'          => 'view id student data'
    ];

    // For profile
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Middle Initial',
      'name_ucl_webservice' => 'middleInitial',
      'name_drupal'         => 'field_student_middle_name',
      'description'         => 'Middle initial (e.g. W)',
      'permission'          => 'view id student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_display'        => 'Student number',
      'name_ucl_csv'        => 'Student Code',
      'name_ucl_webservice' => 'studentCode',
      'name_drupal'         => 'field_student_number',
      'description'         => 'Student number (e.g. 13062413, or old style PAR13062413)',
      'permission'          => 'view id student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_display'        => 'Student number - SCJ',
      'name_ucl_csv'        => 'SCJ Code',
      'name_ucl_webservice' => 'studentCode',
      'name_drupal'         => 'field_student_number_scj',
      'description'         => 'Student number with SCJ code',
      'permission'          => 'view id student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_display'        => 'Student number - SPR',
      'name_ucl_csv'        => 'SPR Code',
      'name_ucl_webservice' => 'studentCode',
      'name_drupal'         => 'field_student_number_spr',
      'description'         => 'Student number with SPR code',
      'permission'          => 'view id student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_machine'        => 'field_gender',
      'name_ucl_csv'        => 'Gender',
      'name_ucl_webservice' => 'gender',
      'name_drupal'         => 'field_student_gender',
      'description'         => 'Gender (M = Male. F = Female)',
      'permission'          => 'view sensitive student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Date of Birth',
      'name_ucl_webservice' => 'dateOfBirth',
      'name_drupal'         => 'field_student_date_of_birth',
      'description'         => 'Date of birth (j/n/y format e.g. 31/12/75 or 1/1/76)',
      'function_clean_from_csv' => function ($field, $value) {
        return self::convertUCLCSVDate($value);
      },
      'permission'          => 'view sensitive student data'
    ];
    $fields[] = [
      'source'                       => 'ucl',
      'name_display'                 => 'Course code',
      'name_ucl_csv'                 => 'Course',
      'name_ucl_webservice'          => 'course',
      'name_drupal'                  => 'field_student_course',
      'drupal_taxonomy'              => 'ucl_courses',
      'drupal_taxonomy_create_entry' => TRUE,
      'description'                  => 'Course code (e.g. UBSSTASING05)',
      'permission'                   => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_display'        => 'Course name',
      'name_ucl_csv'        => 'Course Name',
      'name_ucl_webservice' => 'courseName',
      'name_drupal'         => 'field_student_course_name',
      'drupal_taxonomy'     => 'ucl_courses',
      'description'         => 'Course name (e.g. Research Degree:  Ear Institute)',
      // 'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Enrolment Status',
      'name_ucl_webservice' => 'enrolmentStatus',
      'name_drupal'         => 'field_student_enrolment_status',
      'description'         => 'Enrolment status (E = Enrolled)',
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Mode of Attendance',
      'name_ucl_webservice' => 'modeOfAttendance',
      'name_drupal'         => 'field_student_attendance_mode',
      'drupal_taxonomy'              => 'students_attendance_modes',
      'drupal_taxonomy_create_entry' => TRUE,
      'description'         => 'Mode of attendance (FT = Full Time. PT = Part Time. Modular)',
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Fee Status',
      'name_ucl_webservice' => 'feeStatus',
      'name_drupal'          => 'field_student_fee_status',
      'drupal_taxonomy'              => 'students_fee_statuses',
      'drupal_taxonomy_create_entry' => TRUE,
      'description'         => 'Fee status (H, E, O, HC, I, X, EC, HP, EP)',
      'permission'          => 'view sensitive student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_display'        => 'Year of study',
      'name_ucl_webservice' => 'courseBlock',
      'name_ucl_csv'        => 'Course Block',
      'name_drupal'         => 'field_student_year_of_study',
      'description'         => 'Year of study (e.g. 2)',
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Course Start',
      'name_ucl_webservice' => 'studentCourseStart',
      'name_drupal'         => 'field_student_study_start',
      'description'         => 'Start of study (j/n/y format e.g. 31/12/96 or 1/1/97)',
      'function_clean_from_csv' => function ($field, $value) {
        return self::convertUCLCSVDate($value);
      },
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Course End',
      'name_ucl_webservice' => 'studentCourseEnd',
      'name_drupal'         => 'field_student_study_end',
      'description'         => 'End of study (j/n/y format e.g. 31/12/99 or 1/1/00)',
      'function_clean_from_csv' => function ($field, $value) {
        return self::convertUCLCSVDate($value);
      },
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'                       => 'ucl',
      'name_ucl_csv'                 => 'Department Code',
      'name_ucl_webservice'          => 'departmentCode',
      'name_drupal'                  => 'field_student_department',
      'drupal_taxonomy'              => 'ucl_faculties_and_departments',
      'drupal_taxonomy_create_entry' => TRUE,
      'description'                  => 'Department',
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'                       => 'ucl',
      'name_ucl_csv'                 => 'Faculty Code',
      'name_ucl_webservice'          => 'facultyCode',
      'name_drupal'                  => 'field_student_faculty',
      'drupal_taxonomy'              => 'ucl_faculties_and_departments',
      'drupal_taxonomy_create_entry' => FALSE,
      'description'                  => 'Faculty',
      'permission'          => 'view academic student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Contact Postcode',
      'name_ucl_webservice' => 'studentContactPostcode',
      'name_drupal'         => 'field_student_postcode',
      'description'         => 'Postcode (e.g. SW1A 1AA)',
      'permission'          => 'view sensitive student data'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Term Time Accomodation', // this is misspelt in the export (UCL's fault)
      'name_ucl_webservice' => 'termTimeAccommodation',
      'name_drupal'         => 'field_student_term_time_accom',
      'drupal_taxonomy'              => 'students_term_time_accomodation', // this is misspelt as the taxonomy name (Max's fault)
      'drupal_taxonomy_create_entry' => TRUE,
      'description'         => 'Term time accommodation (C, H, L, N, O, P, V, X)',
      'permission'          => 'view sensitive student data'
    ];
    $fields[] = [
      'source'                       => 'ucl',
      'name_ucl_csv'                 => 'Hall of Residence',
      'name_ucl_webservice'          => 'hallOfResidence',
      'name_drupal'                  => 'field_student_hall_of_residence',
      'drupal_taxonomy'              => 'students_halls_of_residence',
      'drupal_taxonomy_create_entry' => TRUE,
      'description'                  => 'Hall of residence',
      'permission'                   => 'view sensitive student data'
    ];
    $fields[] = [
      'source'                       => 'ucl',
      'name_ucl_csv'                 => 'Caring Responsibility',
      'name_ucl_webservice'          => 'caringResponsibility',
      'name_drupal'                  => 'field_student_caring_resp',
      'drupal_taxonomy'              => 'students_caring_responsibilities',
      'drupal_taxonomy_create_entry' => TRUE,
      'description'                  => 'Caring responsibility (A = Adult family member, friend or neighbour. B = Both adult and child support. C = Child/children under 18. D = Do not wish to disclose. N = None).',
      'permission'                   => 'view academic student data'
    ];

    // Calculated

    $fields[] = [
      'source'             => 'calculated',
      'name_display'       => 'Study mode',
      'name_drupal'        => 'field_student_study_mode',
      'description'        => '',
      'drupal_taxonomy'              => 'students_study_modes',
      'drupal_taxonomy_create_entry' => TRUE,
      'function_calculate' => function ($field, $student_data) {
        $value = $student_data['field_student_course'];

        // Some courses have the wrong code structure for their study mode.
        // So, we manually override.
        // @todo sort this somewhere else.
        $pgtCourses = ['UPGEDUSHAB01'];
        if (in_array($value, $pgtCourses)) {
          return 'PGT';
        }

        switch (substr(strtolower($value), 0, 1)) {
          case 't':
            return 'PGT';

          case 'd':
          case 'r':
            return 'PGR';

          case 'u':
            return 'UG';
        }
      },
      'permission'          => 'view academic student data'
    ];


    $fields[] = [
      'source'             => 'calculated',
      'name_display'       => 'Full name',
      'name_drupal'         => 'field_full_name',
      'description'        => '',
      'function_calculate' => function ($field, $student_data) {
        return ucwords(strtolower($student_data['field_first_name'])) . ' ' . ucwords(strtolower($student_data['field_last_name']));
      },
      'permission'          => 'view academic student data'
    ];

    // These are in the source, but we are not importing
    // because they're not needed and it represents a data risk
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Contact Address 1',
      'name_ucl_webservice' => 'studentContactAddress1',
      'description'         => 'Address line 1 (e.g. Buckingham Palace)'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Contact Address 2',
      'name_ucl_webservice' => 'studentContactAddress2',
      'description'         => 'Address line 2 (e.g. St James Park)'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Contact Address 3',
      'name_ucl_webservice' => 'studentContactAddress3',
      'description'         => 'Address line 3 (e.g. London)'
    ];
    $fields[] = [
      'source'              => 'ucl',
      'name_ucl_csv'        => 'Student Contact Address 4',
      'name_ucl_webservice' => 'studentContactAddress4',
      'description'         => 'Address line 4 (e.g. England)'
    ];

    return $fields;
  }

  public function getMappedNameTermTimeAccommodation() {
    return [
      'C' => 'C',
      'H' => 'H',
      'L' => 'L',
      'N' => 'N',
      'O' => 'O',
      'P' => 'P',
      'V' => 'V',
      'X' => 'X',
    ];
  }

  public function getMappedNameFeeStatus() {
    return [
      'E' => 'European',
      'H' => 'UK',
      'HC' => 'HC',
      'I' => 'International',
      'O' => 'Overseas',
      'OP' => 'OP',
      'X' => 'Unknown',
    ];
  }

  /**
   * Return the name of attendance mode
   *
   * @return string[]
   */
  function getMappedNameAttendanceMode() {
    return [
      'FT' => 'Full time',
      'PT' => 'Part time',
      'Modular' => 'Modular',
    ];
  }

  /**
   * Return the name of study mode
   *
   * @return string[]
   */
  function getMappedNameStudyMode() {
    return [
      'PGT' => 'Postgraduate (Taught)',
      'PGR' => 'Postgraduate (Research)',
      'UG' => 'Undergraduate',
    ];
  }

  /**
   * Return the name of caring responsibility type
   *
   * @return string[]
   */
  function getCaringResponsibilityCodeToNameMap() {
    return [
      'A' => 'Adult family member, friend or neighbour',
      'B' => 'Both adult and child support',
      'C' => 'Child/children under 18',
      'D' => 'Do not wish to disclose',
      'N' => 'None',
    ];
  }

  /**
   * Return the name of the department
   *
   * @return string[]
   */
  function getFacultyAndDepartmentCodeToNameMap() {
    return [
      'ART'       => 'Arts & Humanities',
      'ARTFO_ART' => "Arts and Humanities Faculty Office",
      'BASCO_ART' => "Arts and Sciences BASc",
      'ENGLS_ART' => "English Language and Literature",
      'EUSPS_ART' => "European Social and Political Studies",
      'EISPS_ART' => 'European and International Social and Political Studies',
      'GRKLT_ART' => "Greek and Latin",
      'HEBRW_ART' => "Hebrew and Jewish Studies",
      'INFST_ART' => "Information Studies",
      'LANIE_ART' => "Centre for Languages and International Education",
      'MUDIC_ART' => "Centre for Multidisciplinary and Intercultural Inquiry",
      'PHILO_ART' => "Philosophy",
      'SELCS_ART' => "School of European Languages, Culture and Society",
      'SLADE_ART' => "Slade School of Fine Art",
      'SSEEL_ART' => "SSEES - East European Languages and Culture",
      'SSERS_ART' => "SSEES - Russian",
      'BEN'       => 'Bartlett Faculty of the Built Environment',
      'BSARC_BEN' => "Bartlett School of Architecture",
      'BSCPM_BEN' => "Bartlett School of Construction and Project Management",
      'BSEER_BEN' => "Bartlett School of Environment, Energy and Resources",
      'BSGST_BEN' => "Bartlett School of Graduate Studies",
      'BSIDI_BEN' => "Institute for Digital Innovation in the Built Environment",
      'BSIGP_BEN' => "Institute for Global Prosperity",
      'BSPLN_BEN' => "Bartlett School of Planning",
      'BSIPP_BEN' => "Institute for Innovation and Public Purpose",
      'BSREI_BEN' => "Real Estate Institute",
      'CASAN_BEN' => "Centre for Advanced Spatial Analysis",
      'DEVPU_BEN' => "Development Planning Unit",
      'ENERG_BEN' => "UCL Energy Institute",
      'BRN'       => 'Brain Sciences',
      'BRSCI_BRN' => "Faculty of Brain Sciences",
      'EARIN_BRN' => "Ear Institute",
      'NEURO_BRN' => "Institute of Neurology",
      'OPHTH_BRN' => "Institute of Ophthalmology",
      'PSYAT_BRN' => "Division of Psychiatry",
      'PSYLA_BRN' => "Division of Psychology and Language Sciences",
      'QSION_BRN' => 'UCL Queen Square Institute of Neurology',
      'ENG'       => 'Engineering',
      'BENGN_ENG' => "Biochemical Engineering",
      'CENGN_ENG' => "Chemical Engineering",
      'CIVLG_ENG' => "Civil, Environmental and Geomatic Engineering",
      'CMPLX_ENG' => "Centre for Mathematics, Physics and Engineering in the Life Sciences and Experimental Biology",
      'COMPS_ENG' => "Computer Science",
      'ELECN_ENG' => "Electronic and Electrical Engineering",
      'ENGSC_ENG' => "Faculty of Engineering Sciences",
      'MANAG_ENG' => "UCL School of Management",
      'MANSC_ENG' => "Management Science and Innovation",
      'MECHN_ENG' => "Mechanical Engineering",
      'MPHBE_ENG' => "Medical Physics and Biomedical Engineering",
      'SECUR_ENG' => "Security and Crime Science",
      'STEPP_ENG' => "Science, Technology, Engineering and Public Policy",
      'UCLAU_ENG' => "UCL Australia",
      'INT'       => 'Australia or Qatar',
      'UCL-Q_INT' => "UCL Qatar",
      'CULCM_IOE' => "Culture, Communication and Media",
      'CURPA_IOE' => "Curriculum, Pedagogy and Assessment",
      'EDPAS_IOE' => "Education, Practice and Society",
      'EDUCA_IOE' => "UCL Institute of Education",
      'HUMSS_IOE' => "Humanities and Social Sciences",
      'LCFLL_IOE' => "London Centre for Leadership in Learning",
      'LEARN_IOE' => "Learning and Leadership",
      'LIFCE_IOE' => "Lifelong and Comparative Education",
      'PSYHD_IOE' => "Psychology and Human Development",
      'SOCSC_IOE' => "Social Science",
      'LANIE_IOE' => 'Centre for Languages and International Education',
      'LAW'       => 'Laws',
      'LAWSD_LAW' => "Laws [department]",
      'LIF'       => 'Life Sciences',
      'BIOSC_LIF' => "Division of Biosciences",
      'GATSB_LIF' => "Gatsby Computational Neuroscience Unit",
      'LIFES_LIF' => "Faculty of Life Sciences",
      'LMCBL_LIF' => "Laboratory for Molecular Cell Biology",
      'PHMCY_LIF' => "School of Pharmacy",
      'SWNCB_LIF' => "Sainsbury Wellcome Centre for Neural Circuits and Behaviour",
      'MAP'       => 'Mathematical & Physical Sciences',
      'CHEMS_MAP' => "Chemistry",
      'EARTH_MAP' => "Earth Sciences",
      'MATHS_MAP' => "Mathematics",
      'NATSC_MAP' => "Natural Sciences",
      'PHYSA_MAP' => "Physics and Astronomy",
      'SCITS_MAP' => "Science and Technology Studies",
      'SPACE_MAP' => "Space and Climate Physics",
      'STATS_MAP' => "Statistical Science",
      'RADRE_MAP' => 'Risk and Disaster Reduction',
      'MDS'       => 'Medical Sciences',
      'CANCR_MDS' => "Cancer Institute",
      'EASTD_MDS' => "Eastman Dental Institute",
      'INFEC_MDS' => "Division of Infection and Immunity",
      'MDSCI_MDS' => "Faculty of Medical Sciences",
      'MEDCN_MDS' => "Division of Medicine",
      'SURGS_MDS' => "Division of Surgery and Interventional Science",
      'UCLMS_MDS' => "UCL Medical School",
      'PHS'       => 'Population Health Sciences',
      'CARDI_PHS' => "Institute of Cardiovascular Science",
      'CHILD_PHS' => "Institute of Child Health",
      'EPIHC_PHS' => "Institute of Epidemiology and Health Care",
      'GLOBH_PHS' => "Institute for Global Health",
      'GOSCH_PHS' => "UCL GOS Institute of Child Health",
      'HEINF_PHS' => "Institute of Health Informatics",
      'IOCTM_PHS' => "Institute of Clinical Trials and Methodology",
      'PHSCI_PHS' => "Faculty of Population Health Sciences",
      'WOMEN_PHS' => "Institute for Women's Health",
      'SSEES_SES' => "SSEES - School of Slavonic and East European Studies",
      'SES'       => "SSEES - School of Slavonic and East European Studies [Faculty]",
      'SHS'       => 'Social & Historical Sciences',
      'AMERC_SHS' => "Institute of the Americas",
      'ANTHR_SHS' => "Anthropology",
      'ARCLG_SHS' => "Institute of Archaeology",
      'CALTG_SHS' => "Centre for the Advancement of Learning and Teaching",
      'ECONS_SHS' => "Economics",
      'GEOGR_SHS' => "Geography",
      'HARTD_SHS' => "History of Art",
      'HISTR_SHS' => "History",
      'POLSC_SHS' => "Political Science",
      'SHSFO_SHS' => "Social and Historical Sciences Faculty Office",
      'SSEHI_SHS' => "SSEES - History",
      'SSESS_SHS' => "SSEES - Social Sciences",
      'IOE'       => 'Institute of Education',
    ];
  }

  public static function convertUCLCSVDate($value) {
    $date_formatted = FALSE;

    if (strlen($value) <= 7) {
      $date_formatted = DateTime::createFromFormat("j/n/y", $value);
    } else if (strlen($value) <= 8) {
      $date_formatted = DateTime::createFromFormat("d/m/y", $value);
    } else {
      $date_formatted = DateTime::createFromFormat("d/m/Y", $value);
    }

    if (!$date_formatted && strtotime($value)) {
      $date_formatted = new DateTime($value);
    }

    if (!$date_formatted) {
      return $value;
    }

    $value = $date_formatted->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);
    return $value;
  }
}
