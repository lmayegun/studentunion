<?php

namespace Drupal\su_students\Form;

use Drupal\su_students\Controller\StudentImportController;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implement Class BulkUserImport for import form.
 */
class StudentImportWebserviceForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_students_webservice';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_students.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['anonymise_expired'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Anonymise any expired students'),
    );

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import from webservice'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    StudentImportController::importFromWebservice();
  }

}
