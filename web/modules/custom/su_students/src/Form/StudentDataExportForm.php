<?php

namespace Drupal\su_students\Form;

use DateTime;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\user\Entity\User;

/**
 * Allow user to upload a CSV and be given a download of student data from the website
 *
 * This defaults to providing an anonymised export, but can also provide a full identifiable export
 *
 *
 */
class StudentDataExportForm extends BatchDownloadCSVForm {
  const minRowsForAnonymous = 30;

  const permissionsForAnonymous = ['view engagement student data', 'view academic student data', 'view sensitive student data'];
  const permissionsForFull = ['view id student data', 'view engagement student data', 'view academic student data', 'view sensitive student data'];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_students_student_data_export';
  }

  public function getAllowedReportTypes() {
    $account = User::load(\Drupal::currentUser()->id());
    $types = ['anonymised' => 'Anonymised'];

    foreach (static::permissionsForFull as $permission) {
      if ($account->hasPermission($permission)) {
        $types['full'] = 'Full student data';
      }
    }
    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['description'] = [
      '#markup' => '<p>Upload a CSV file with at least one identifying column including a UCL user ID, UCL e-mail, or UPI. The website will then match that ID with any currently enrolled students and return you a CSV with additional fields of data.</p><p>If you only need to analyse the demographics of the students, it is strongly recommended to run only an anonymised report, significantly reducing the risk of a breaching personal data protection.</p><p>These fields are determined by your level of access and the category of data (ID, academic, engagement or sensitive) - you may need to request a higher level of access if you cannot get the fields you want, or try the anonymous export.</p>'
    ];

    $form['file_upload'] = [
      '#type'              => 'file',
      '#title'             => $this->t('Import CSV File'),
      '#size'              => 40,
      '#description'       => $this->t('Upload a CSV file with a UCL e-mail, UPI or user ID in a column.'),
      '#required'          => false,
      '#autoupload'        => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];


    $form['export_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Export type'),
      '#options'       => $this->getAllowedReportTypes(),
      '#default_value' => "anonymised",
      '#description'   => 'Note you may not have access to all report types, and your "full" report may only show a subset of fields, based on your user role. Contact the systems administrator if you need higher level access.',
      "#required" => true
    ];

    $form['data'] = [
      '#type' => 'fieldset',
      '#title' => 'Data protection agreements',
      '#description'   => 'For more information see our <a href="https://studentsunionucl.org/data-protection-and-privacy-policy" target="_blank">Data protection and privacy policy</a> or contact <a href="mailto:su.data-protection@ucl.ac.uk">su.data-protection@ucl.ac.uk</a>.',
    ];

    $form['data']['use'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to use the data only for the legitimate interests of the Union and its members, as outlined in our data protection and privacy policy.'),
      "#required"      => true
    ];

    $form['data']['share'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to keep this file within the Union, and not to share this file with any students, officers, UCL staff or unless explicitly permitted by our data protection policy.'),
      "#required"      => true
    ];

    $form['data']['delete'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to delete this export as soon as I no longer require it.'),

      "#required"      => true
    ];

    unset($form['submit']);
    unset($form['actions']['submit']);
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Import and download'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $validators = ['file_validate_extensions' => ['csv']];
    $file = file_save_upload('file_upload', $validators, FALSE, NULL, FileSystemInterface::EXISTS_REPLACE)[0];

    $importForm = new BatchImportCSVForm();
    $array = $importForm->convertCSVtoArray($file->getFileUri());

    if (!$array) {
      return FALSE;
    }

    $form_state->set('array', $array);
    $_SESSION['StudentDataExportForm:ExportType'] = $form_state->getValue('export_type');

    if ($_SESSION['StudentDataExportForm:ExportType'] == 'anonymised' && count($array) < static::minRowsForAnonymous) {
      \Drupal::messenger()->addError(t('To avoid the risk of the data being de-anonymised and being used to identify individual students, you cannot run an anonymised export with fewer than @min rows - you provided a CSV containing @count rows.', [
        '@min' => static::minRowsForAnonymous,
        '@count'      => count($array),
      ]));
      return false;
    }

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->getBatchTitle($form, $form_state))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'exportFinished']);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $studentsToProcessEachBatch = 100;

    $rows = $form_state->get('array');
    $chunks = array_chunk($rows, $studentsToProcessEachBatch);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public function getUserFromRow($row) {
    $service = \Drupal::service('su_user.ucl_user');
    foreach ($row as $field => $data) {
      if ($uid = $service->getUserForUclFieldInput($data, TRUE)) {
        return $uid;
      }
      // Webform result exports
      if ($field == 'Submitted by: ID' && $data) {
        return $data;
      }
    }
    return NULL;
  }

  public function getFieldsAllowedForCurrentUser() {
    $account = User::load(\Drupal::currentUser()->id());
    $fields = [];

    $studentDataDefinitions = \Drupal::service('su_students.data_definitions');
    $fieldDefinitions = $studentDataDefinitions->getFieldDefinitions();

    foreach ($fieldDefinitions as $field) {
      if (!isset($field['permission'])) {
        continue;
      }
      if ($_SESSION['StudentDataExportForm:ExportType'] == 'anonymised') {
        $include = in_array($field['permission'], static::permissionsForAnonymous);
      } else {
        $include = $account->hasPermission($field['permission']);
      }

      if ($include) {
        $fields[] = $field;
      }
    }
    return $fields;
  }

  public function getBatchTitle($form, $form_state) {
    return $this->t('Student data export');
  }

  public function processBatch(array $rows, array &$context) {
    if (!in_array($_SESSION['StudentDataExportForm:ExportType'],  array_keys($this->getAllowedReportTypes()))) {
      return FALSE;
    }

    $fields = $this->getFieldsAllowedForCurrentUser();

    foreach ($rows as $row) {

      $studentUser = User::load($this->getUserFromRow($row));
      if (!$studentUser) {
        $line = $row;
        $this->addLine($line, $context);
        continue;
      }

      // Load student
      $studentService = \Drupal::service('su_students.student_services');
      if ($studentUser) {
        $student = $studentService->getStudentProfile($studentUser, FALSE);
      } else {
        $student = NULL;
      }

      if ($_SESSION['StudentDataExportForm:ExportType'] == 'anonymised') {
        $line = [];
      } else {
        // Start with existing data
        $line = $row;
        $line['Website user ID'] = $studentUser->id();
        $line['E-mail'] = $studentUser->getEmail();
      }

      if ($student) {
        $line['Student status'] = ($studentService::isStudentCurrentMember($student) ? 'Current student' : 'Former/future student');
      } else {
        $line['Student status'] = 'Not a student';
      }

      foreach ($fields as $field) {
        $entity = isset($field['drupal_entity']) && $field['drupal_entity'] == 'User' ? $studentUser : $student;

        if (!$entity || !$entity->hasField($field['name_drupal']) || !$entity->get($field['name_drupal'])) {
          $line[$field['description']] = '--- field not found ---';
          continue;
        }

        $fieldType = $entity->get($field['name_drupal'])->getFieldDefinition()->getType();
        $desc = $entity->get($field['name_drupal'])->getFieldDefinition()->getLabel();
        if (gettype($desc) == 'object') {
          $desc = $desc->__toString();
        }

        if ($fieldType == 'date' || $fieldType == 'datetime') {
          $fieldName = $field['name_drupal'];
          $date = $entity->$fieldName->value ?? FALSE;

          switch ($field['name_drupal']) {
            case 'field_student_date_of_birth':
              $desc = 'Age';
              $diff = TRUE;
              break;

            default:
              $diff = FALSE;
          }

          if ($date) {
            $date = new DateTime($date);
            if ($diff) {
              $now = new DateTime();
              $interval = $now->diff($date);
              $line[$desc] = $interval->y;
            } else {
              $line[$desc] = $date->format('Y-m-d');
            }
          } else {
            $line[$desc] = '';
          }
        } elseif ($fieldType == 'entity_reference') {
          $fieldName = $field['name_drupal'];
          $term = $entity->$fieldName->entity;
          if ($term) {
            $line[$desc] = $term->label();
          } else {
            $line[$desc] = ''; //print_r($entity->get($field)->value);
          }
        } else {
          $line[$desc] = isset($field['name_drupal']) && $entity->get($field['name_drupal']) ? $entity->get($field['name_drupal'])->getString() : '';
        }
      }

      $this->addLine($line, $context);
    }
  }

  public function sortResults($rows, $results) {
    if ($_SESSION['StudentDataExportForm:ExportType'] == 'anonymised') {
      shuffle($rows);
    }
    return $rows;
  }
}
