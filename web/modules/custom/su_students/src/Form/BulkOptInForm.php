<?php

namespace Drupal\su_students\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class BulkOptInForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_students_bulk_optin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upis'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter UPIs, user IDs and/or UCL e-mails, each on a new line or comma separated'),
      '#description' => $this->t('ONLY DO THIS DUE TO DATA ERROR WHERE YOU HAVE CLEAR CONSENT FROM STUDENTS, NOT ON BEHALF OF STUDENTS WHO HAVE NOT CONSENTED.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $service = \Drupal::service('su_user.ucl_user');
    $studentService = \Drupal::service('su_students.student_services');

    $upis = explode("\n", explode("\r", explode(',', $form_state->getValue('upis'))));

    foreach ($upis as $upi) {
      if ($upi) {
        $account = $service->getUserForUclFieldInput($upi);
        if (!$studentService->isStudentOptin($account)) {
          $studentService::optInStudent($account);
        }
      }
    }
  }
}
