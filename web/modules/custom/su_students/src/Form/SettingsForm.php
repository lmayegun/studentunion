<?php

namespace Drupal\su_students\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_students.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_students_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('su_students.settings');
    $form['opt_in_successful_e_mail'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Opt in successful e-mail'),
      '#default_value' => $config->get('opt_in_successful_e_mail'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('su_students.settings')
      ->set('opt_in_successful_e_mail', $form_state->getValue('opt_in_successful_e_mail')['value'])
      ->save();
  }

}
