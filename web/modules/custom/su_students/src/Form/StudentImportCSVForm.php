<?php

namespace Drupal\su_students\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\File\FileSystemInterface;
use Drupal\profile\Entity\Profile;
use Drupal\su_students\Controller\StudentImportController;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Implement Class BulkUserImport for import form.
 */
class StudentImportCSVForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_students_csv';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_students.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['file_upload'] = [
      '#type'              => 'file',
      '#title'             => $this->t('Import CSV File'),
      '#size'              => 40,
      '#description'       => $this->t('Select the CSV file to be imported.'),
      '#required'          => FALSE,
      '#autoupload'        => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];

    $form['anonymise_expired'] = array(
      '#type'        => 'checkbox',
      '#title'       => $this->t('Opt out and schedule anonymising any students not in list'),
      '#description' => $this->t('If user is not in the list imported, this will opt out any students (prevent using union services), and at the end of the academic year this will remove their student data profile.'),
    );

    $form['only_new'] = array(
      '#type'        => 'checkbox',
      '#title'       => $this->t('Only import students without current data on our site'),
      '#description' => $this->t('Select to quickly import newly opted in students. Note this means updated data for existing students is not imported, and would need a separate import run.'),
    );

    // $form['remove_orphan_student_profile'] = array(
    //   '#type'        => 'checkbox',
    //   '#title'       => $this->t('Remove students profile without a user'),
    //   '#description' => $this->t('This will remove every student profile that does not have a user attached.'),
    // );

    $form['users_per_operation'] = array(
      '#type'          => 'number',
      '#max'           => 5000,
      '#default_value' => 25,
      '#title'         => $this->t('Users per operation'),
      '#description'   => $this->t(
        'Every batch call is an HTTP request. So you need to find the perfect blend of
      how many students you can process before another HTTP request gets fired.
      Two things to consider are memory and max execution time (current server: @max seconds). You will want to process as many students as
      possible per batch to reduce the number of HTTP request without reaching the max execution time',
        [
          '@max' => ini_get('max_execution_time'),
        ]
      ),
    );

    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $validators = ['file_validate_extensions' => ['csv']];
    $file = file_save_upload('file_upload', $validators, FALSE, NULL, FileSystemInterface::EXISTS_REPLACE)[0];
    $csv_students = $this->convertCSVtoArray($file->getFileUri());

    if (!$csv_students) {
      return FALSE;
    }

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Importing students'))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'importFinished']);

    $studentDataDefinitions = \Drupal::service('su_students.data_definitions');
    $fieldDefinitions = $studentDataDefinitions->getFieldDefinitions();

    $database = \Drupal::database();
    $query = $database->query("SELECT field_ucl_user_id_value FROM {user__field_ucl_user_id} WHERE entity_id IN (SELECT uid FROM {profile} INNER JOIN profile__field_student_date_of_birth pfsdob ON pfsdob.entity_id = profile.profile_id  WHERE type = 'student' AND status = 1 AND pfsdob.entity_id > 0)");
    $existingUserIds = $query->fetchCol();

    $studentsFromCSV = [];

    $studentListNew = [];
    $studentListExisting = [];

    foreach ($csv_students as $csv_student) {
      $student = [];

      if (!array_keys($csv_student)) {
        continue;
      }

      // Remove BOM characters from first column https://stackoverflow.com/questions/32184933/remove-bom-%C3%AF-from-imported-csv-file
      $firstColumn = array_keys($csv_student)[0];
      if (substr($firstColumn, 0, 3) == chr(hexdec('EF')) . chr(hexdec('BB')) . chr(hexdec('BF'))) {
        $csv_student[substr($firstColumn, 3)] = $csv_student[$firstColumn];
        unset($csv_student[$firstColumn]);
      }

      // Convert CSV filenames to the drupal filenames
      foreach ($fieldDefinitions as $field) {
        if ($field['source'] != 'ucl') {
          continue;
        }

        // Save value
        if (isset($field['name_drupal']) && isset($csv_student[$field['name_ucl_csv']])) {
          $csv_value = $csv_student[$field['name_ucl_csv']];

          // Run function defined to clean that field
          if (isset($field['function_clean_from_csv'])) {
            $csv_value = $field['function_clean_from_csv']($field, $csv_value);
          }

          $student[$field['name_drupal']] = $csv_value;
        }
      }

      // Add to batch to process
      if (!in_array($student['field_ucl_user_id'], $existingUserIds)) {
        $studentListNew[] = $student;
      } else {
        $studentListExisting[] = $student;
      }
      $studentUCLIdsFromCSV[] = $student['field_ucl_user_id'];
    }

    // We anonymise users first.
    if ($form_state->getValue('anonymise_expired')) {
      $usersToAnonymise = StudentImportCSVForm::getUsersNotInList($studentUCLIdsFromCSV);
      $chunks = array_chunk($usersToAnonymise, $form_state->getValue('users_per_operation'), TRUE);
      foreach ($chunks as $chunk) {
        $batch_builder->addOperation([$this, 'processBatchScheduleAnonymiseStudent'], [$chunk]);
      }
    }

    // Chunk the array of studentList, doing new students first for efficiency
    if ($form_state->getValue('only_new')) {
      $listOrder = [$studentListNew];
    } else {
      $listOrder = [$studentListNew, $studentListExisting];
    }
    foreach ($listOrder as $list) {
      $chunks = array_chunk($list, $form_state->getValue('users_per_operation'), TRUE);
      foreach ($chunks as $chunk) {
        $batch_builder->addOperation([$this, 'processBatchImportStudent'], [$chunk]);
      }
    }

    // if ($form_state->getValue('remove_orphan_student_profile')) {
    // $studentService = \Drupal::service('su_students.student_services');
    // $studentsProfiles = $studentService::getAllStudentsByStudentProfile();
    // $chunks = array_chunk($studentsProfiles, $form_state->getValue('users_per_operation'), TRUE);
    // foreach ($chunks as $chunk) {
    //   $batch_builder->addOperation([$this, 'processBatchOrphanStudentProfile'], [$chunk]);
    // }
    // }

    $studentService = \Drupal::service('su_students.student_services');
    $studentsProfiles = $studentService::getStudentProfileIdsScheduledToAnonymise();
    $chunks = array_chunk($studentsProfiles, $form_state->getValue('users_per_operation'), TRUE);
    foreach ($chunks as $chunk) {
      $batch_builder->addOperation([$this, 'processBatchAnonymiseStudent'], [$chunk]);
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * @param       $studentProfiles
   * @param array $context
   */
  public static function processBatchOrphanStudentProfile($studentProfiles, array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['total_quantity'] = count($studentProfiles);
      $context['sandbox']['created'] = 0;
      $context['results']['codes'] = [];
      $context['results']['total_quantity'] = count($studentProfiles);
    }
    $total_quantity = $context['sandbox']['total_quantity'];
    $created = &$context['sandbox']['created'];
    $studentService = \Drupal::service('su_students.student_services');

    foreach ($studentProfiles as $studentProfile) {
      $studentService->deleteProfileWithoutUser($studentProfile);
      $context['results']['codes'][] = $studentProfile['uid'];
      $created++;
      $context['message'] = t('Student profile: @studentProfile deleted', [
        '@studentProfile' => $studentProfile['profile_id'],
      ]);
    }
    $context['finished'] = $created / $total_quantity;
  }

  /**
   * @param       $profile_ids
   * @param array $context
   */
  public static function processBatchAnonymiseStudent($profile_ids, array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['total_quantity'] = count($profile_ids);
      $context['sandbox']['created'] = 0;
      $context['results']['codes'] = [];
      $context['results']['total_quantity'] = count($profile_ids);
    }
    $total_quantity = $context['sandbox']['total_quantity'];
    $created = &$context['sandbox']['created'];
    $studentService = \Drupal::service('su_students.student_services');

    foreach ($profile_ids as $profile_id) {
      $studentProfile = Profile::load($profile_id);
      if (!$studentProfile) {
        continue;
      }

      $account = $studentProfile->getOwner();

      // Check if current (e.g. in grace period)
      if ($studentService::isStudentCurrentMember($studentProfile)) {
        $context['message'] = t('User: @user not anonymised - has a current student profile', [
          '@user' => $account->getAccountName(),
        ]);
        $created++;
        continue;
      }

      $studentService->anonymiseStudent($account);

      /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
      $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');
      $clubSocs = $clubSocMembershipsService->countMembershipsForUser($account);
      if ($clubSocs > 0 && !$account->hasRole('associate_visiting')) {
        \Drupal::logger('su_students')->error(t(
          'STUDENT @details IS MEMBER OF CLUB/SOCS: @count',
          ['@details' => $account->id(), '@count' => $clubSocs]
        ));

        // Try opting them in again if they haven't opted out.
        // (To avoid chaos / on the assumption they would want to continue
        // and this is likely a UCL error)
        // If they've properly left, this will do nothing as UCL will ignore the UPI.
        // If they haven't, this will fix it next import.
        if (!$studentService::isStudentOptout($account)) {
          $studentService::optInStudent($account, $studentProfile);
        }
      }

      $context['results']['codes'][] = $account;
      $created++;
      $context['message'] = t('User: @user anonymised', [
        '@user' => $account->getAccountName(),
      ]);
    }
    $context['finished'] = $created / $total_quantity;
  }

  /**
   * @param       $users
   * @param array $context
   */
  public static function processBatchScheduleAnonymiseStudent($users, array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['total_quantity'] = count($users);
      $context['sandbox']['created'] = 0;
      $context['results']['codes'] = [];
      $context['results']['total_quantity'] = count($users);
    }
    $total_quantity = $context['sandbox']['total_quantity'];
    $created = &$context['sandbox']['created'];
    $studentService = \Drupal::service('su_students.student_services');

    foreach ($users as $user) {
      $user = User::load($user);
      if (!$user) {
        continue;
      }

      // Check if current (e.g. in grace period)
      $studentProfile = $studentService->getStudentProfile($user, FALSE);
      if ($studentService::isStudentCurrentMember($studentProfile)) {
        $context['message'] = t('User: @user not anonymised - has a current student profile', [
          '@user' => $user->getAccountName(),
        ]);
        $created++;
        continue;
      }

      $studentService->optOutAndScheduleAnonymiseStudent($user);

      /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
      $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');
      $clubSocs = $clubSocMembershipsService->countMembershipsForUser($user);
      if ($clubSocs > 0 && !$user->hasRole('associate_visiting')) {
        \Drupal::logger('su_students')->error(t(
          'STUDENT @details IS MEMBER OF CLUB/SOCS: @count',
          ['@details' => $user->id(), '@count' => $clubSocs]
        ));

        // Try opting them in again if they haven't opted out.
        // (To avoid chaos / on the assumption they would want to continue
        // and this is likely a UCL error)
        // If they've properly left, this will do nothing as UCL will ignore the UPI.
        // If they haven't, this will fix it next import.
        if (!$studentService::isStudentOptout($user)) {
          $studentService::optInStudent($user, $studentProfile);
        }
      }

      $context['results']['codes'][] = $user;
      $created++;
      $context['message'] = t('User: @user anonymised', [
        '@user' => $user->getAccountName(),
      ]);
    }
    $context['finished'] = $created / $total_quantity;
  }

  /**
   * @param       $students
   * @param array $context
   */
  public static function processBatchImportStudent($students, array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['total_quantity'] = count($students);
      $context['sandbox']['created'] = 0;
      $context['results']['codes'] = [];
      $context['results']['total_quantity'] = count($students);
    }

    $total_quantity = $context['sandbox']['total_quantity'];
    $created = &$context['sandbox']['created'];

    foreach ($students as $student) {
      $username = $student['field_ucl_user_id'] . '@ucl.ac.uk';

      $user = user_load_by_name($username);
      if ($user) {
        $message = 'Updated';
        static::updateUser($user, $student);
      } else {
        $message = 'Created';
        static::createUser($student);
      }

      $context['results']['codes'][] = $student;
      $created++;
      $context['message'] = t('@message @username', [
        '@message'  => $message,
        '@username' => $username,
      ]);
    }
    $context['finished'] = $created / $total_quantity;
  }

  /**
   * @param $student_data
   */
  public static function createUser($student_data) {
    // Disable notification emails before creating the user
    \Drupal::configFactory()->getEditable('user.settings')->set('notify.status_activated', FALSE)->save();
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $user = User::create([
      'uid'                      => '',
      'name'                     => $student_data['field_ucl_user_id'] . '@ucl.ac.uk',
      'mail'                     => $student_data['mail'],
      'init'                     => $student_data['field_ucl_user_id'] . '@ucl.ac.uk',
      'status'                   => 1,
      'langcode'                 => $language,
      'preferred_langcode'       => $language,
      'preferred_admin_langcode' => $language,
    ]);
    $user->enforceIsNew();
    $user->save();

    $studentService = \Drupal::service('su_students.student_services');

    // If there is no student profile, it creates it.
    $studentService->createStudentProfile($user, TRUE);

    static::updateUser($user, $student_data);

    // Enable notification emails after again
    \Drupal::configFactory()->getEditable('user.settings')->set('notify.status_activated', TRUE)->save();
    return $user;
  }

  /**
   * @param User $user
   * @param      $student_data
   */
  public static function updateUser(User $user, $student_data) {
    $studentService = \Drupal::service('su_students.student_services');

    // if there is no student profile, it creates it
    $profile = $studentService->getStudentProfile($user, TRUE);

    // Opt in user and student, though don't save yet to avoid unecessary saves:
    $studentService::optInStudent($user, $profile, FALSE);

    if (!isset($_SESSION['fieldDefinitions'])) {
      $studentDataDefinitions = \Drupal::service('su_students.data_definitions');
      $fieldDefinitions = $studentDataDefinitions->getFieldDefinitions();
    } else {
      $fieldDefinitions = $_SESSION['fieldDefinitions'];
    }

    foreach ($fieldDefinitions as $field) {
      $startTime = \Drupal::time()->getCurrentMicroTime();

      // Skip if not defined for Drupal.
      if (!isset($field['name_drupal'])) {
        continue;
      }

      // If it's a calculated field, give the information to the anonymous function to calculate
      if (isset($field['function_calculate'])) {
        $new_value = $field['function_calculate']($field, $student_data);
      } else {
        // Otherwise pull it from the data straightaway
        $new_value = isset($student_data[$field['name_drupal']]) ? $student_data[$field['name_drupal']] : NULL;
      }

      // Deal with taxonomy values
      if (isset($field['drupal_taxonomy'])) {
        $new_value = static::importTaxonomy($student_data, $field, $new_value);
      }

      if (is_null($new_value)) {
        continue;
      }

      if ($user->hasField($field['name_drupal'])) {
        $user->set($field['name_drupal'], $new_value);
      } elseif ($profile->hasField($field['name_drupal'])) {
        $profile->set($field['name_drupal'], $new_value);
      }
      $endTime = \Drupal::time()->getCurrentMicroTime();

      if ($endTime - $startTime > 0.1) {
        \Drupal::logger('su_students')->debug($field['name_drupal'] . ' - long - ' . ($endTime - $startTime));
      }
    }

    // Add student role and save.
    $user->addRole('student');
    $user->addRole('ucl_authenticated');
    $user->activate();
    $user->save();

    // Set or update last updated date (field type 'Timestamp') and save.
    $profile->set('field_last_updated', \Drupal::time()->getRequestTime());
    $profile->save();
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  public function importFinished($success, $results, $operations) {
    if ($success) {
      $processed = isset($results['codes']) ? count($results['codes']) : 0;
      // An incomplete set of coupons was generated.
      if (isset($results['total_quantity']) && $processed != $results['total_quantity']) {
        \Drupal::messenger()->addWarning(t('Processed @processed out of %total students.
        Consider adding a unique prefix/suffix or increasing the pattern length to improve results.', [
          '@processed' => $processed,
          '@total'     => $results['total_quantity'],
        ]));
      } else {
        \Drupal::messenger()->addMessage(\Drupal::translation()->formatPlural(
          $processed,
          'Processed 1 student.',
          'Processed @count students.'
        ));
      }
    } else {
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(t('An error occurred while processing @operation with arguments: @args', [
        '@operation' => $error_operation[0],
        '@args'      => print_r($error_operation[0], TRUE),
      ]));
    }
  }

  /**
   * Return a list of unique ids to anonymise
   *
   * @param array $currentUserIds
   * @return array
   */
  public static function getUsersNotInList($currentUserUCLIds) {
    $usersToAnonymise = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', 'member')
      ->condition('field_ucl_user_id', $currentUserUCLIds, 'NOT IN')
      ->execute();

    // Return a list of unique ids, combining both list
    return $usersToAnonymise;
  }

  /**
   * Import taxonomies. If it does not exist, create it, otherwise update if different
   *
   * @param $student_data
   * @param $field
   * @param $value
   * @return string
   */
  public static function importTaxonomy($student_data, $field, $value) {
    $cid = $field['drupal_taxonomy'] . ':' . $value;

    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $studentDataDefinitions = \Drupal::service('su_students.data_definitions');
    $parent = NULL;
    $name = FALSE;
    switch ($field['drupal_taxonomy']) {
      case 'ucl_faculties_and_departments':
        $options = $studentDataDefinitions->getFacultyAndDepartmentCodeToNameMap();
        $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
        break;
      case 'students_caring_responsibilities':
        $options = $studentDataDefinitions->getCaringResponsibilityCodeToNameMap();
        $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
        break;
      case 'students_study_modes':
        $options = $studentDataDefinitions->getMappedNameStudyMode();
        $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
        break;
      case 'students_attendance_modes':
        $options = $studentDataDefinitions->getMappedNameAttendanceMode();
        $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
        break;
      case 'students_term_time_accomodation':
        $options = $studentDataDefinitions->getMappedNameTermTimeAccommodation();
        $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
        break;
      case 'ucl_courses':
        // Detect if name or code
        $name = $student_data['field_student_course_name'] ?? $student_data['field_student_course'];
        $value = $student_data['field_student_course'];
        break;
      default:
        $name = $value;
    }

    $term = NULL;
    if ($value) {
      $term_value = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties([
          'vid'            => $field['drupal_taxonomy'],
          'field_ucl_code' => $value,
        ]);
      $term = reset($term_value);
    }

    // Create and/or update term if needed
    if (empty($term) && $name && $value && isset($field['drupal_taxonomy_create_entry']) && $field['drupal_taxonomy_create_entry']) {
      $term = Term::create([
        'name'           => $name,
        'vid'            => $field['drupal_taxonomy'],
        'field_ucl_code' => $value,
        'parent'         => $parent,
      ])->save();
    } elseif ($term) {
      if (!$term->parent->entity) {
        $term->set('parent', $parent);
      }
      if ($term->get('name')->value == $value || $term->get('name')->value == '') {
        $term->set('name', $name);
      }
      $term->save();
    }
    $value = is_object($term) ? $term->id() : FALSE;

    \Drupal::cache()->set($cid, $value, strtotime('+ 1 day'), $term && is_object($term) ? $term->getCacheTags() : []);
  }

  /**
   * @param string $filename
   * @param string $delimiter
   * @return array|false
   */
  public function convertCSVtoArray($filename = '', $delimiter = ',') {
    if (!file_exists($filename) || !is_readable($filename)) return FALSE;

    $header = NULL;
    $data = array();

    if (($handle = fopen($filename, 'r')) !== FALSE) {
      // Loop through rows
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
        if (!$header) {
          $header = $row;
        } else {
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }

    return $data;
  }
}
