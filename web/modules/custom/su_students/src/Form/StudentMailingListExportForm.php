<?php

namespace Drupal\su_students\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\Profile;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Allow user to download Mailchimp export
 */
class StudentMailingListExportForm extends BatchDownloadCSVForm {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_students_student_mailing_list_export';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['data'] = [
      '#type'        => 'fieldset',
      '#title'       => 'Data protection agreements',
      '#description' => 'For more information see our <a href="https://studentsunionucl.org/data-protection-and-privacy-policy" target="_blank">Data protection and privacy policy</a> or contact <a href="mailto:su.data-protection@ucl.ac.uk">su.data-protection@ucl.ac.uk</a>.',
    ];

    $form['data']['use'] = [
      '#type'     => 'checkbox',
      '#title'    => $this->t('I agree to use the data only for the legitimate interests of the Union and its members, as outlined in our data protection and privacy policy.'),
      "#required" => TRUE
    ];

    $form['data']['delete'] = [
      '#type'     => 'checkbox',
      '#title'    => $this->t('I agree to delete this export as soon as I no longer require it.'),
      "#required" => TRUE
    ];

    unset($form['submit']);
    unset($form['actions']['submit']);
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Download'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $studentUserIds = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', 'student')
      ->sort('access', 'DESC')
      // ->range(0, 1000)
      ->execute();

    $chunks = array_chunk($studentUserIds, 150);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public function processBatch(array $chunk, array &$context) {
    foreach ($chunk as $studentUserId) {
      $user = User::load($studentUserId);

      // Exclude unsubscribed
      if ($user->field_user_unsubscribed->value) {
        $subscribed = !in_array('site', $user->field_user_unsubscribed->value);
        if (!$subscribed) {
          continue;
        }
      }

      /**
       * Load student
       *
       * @var Profile $student
       */
      $studentService = \Drupal::service('su_students.student_services');
      $student = $studentService->getStudentProfile($user, FALSE);

      if ($student && $student->get('field_profile_optin')->getString() === '1') {
        $studentArray = [];
        $studentArray['Email Address'] = $user->getEmail(); // @todo check if this is the right e-mail, i.e. not the user ID email
        $studentArray['First Name'] = $user->field_first_name->value;
        $studentArray['Last Name'] = $user->field_last_name->value;
        $studentArray['UPI'] = $user->field_upi->value;
        $studentArray['Job Title'] = '';
        $studentArray['Department'] = $this->getTerm($student, 'field_student_department');
        $studentArray['Faculty'] = $this->getTerm($student, 'field_student_faculty');
        $studentArray['Faculty Code'] = $this->getTerm($student, 'field_student_faculty', 'field_ucl_code');
        $studentArray['Date of birth'] = $student ? $student->get('field_student_date_of_birth')->getString() : '';
        $studentArray['Fee status'] = $this->getTerm($student, 'field_student_fee_status');
        $studentArray['Year of study'] = $student ? $student->get('field_student_year_of_study')->getString() : '';
        $studentArray['Study start date'] = Date('Y-m-d', strtotime($student->field_student_study_start->value));
        $studentArray['Study end date'] = Date('Y-m-d', strtotime($student->field_student_study_end->value));
        $studentArray['Study type (PG/UG)'] = $this->getTerm($student, 'field_student_study_mode');
        $studentArray['Nationality'] = ''; // @todo
        $studentArray['Course Name'] = $this->getTerm($student, 'field_student_course');
        $studentArray['Hall of Residence'] = $this->getTerm($student, 'field_student_hall_of_residence');
        $studentArray['Caring Responsibility'] = $this->getTerm($student, 'field_student_caring_resp');
        $studentArray['TeamUCL'] = su_clubs_societies_user_is_member_of_category($user, 'Sports') ? 'TRUE' : 'FALSE';

        $tags = [];
        $academicYearName = '';
        // $tags[] = "Current student " . $current_academic_year . '/' . ($current_academic_year + 1);
        // if (strtotime($student->study_end) >= $start_next_year) {
        //     $tags[] = "Continuing student " . ($current_academic_year + 1) . '/' . ($current_academic_year + 2);
        // }
        // if (strtotime($student->study_start) >= uclu_custom_get_start_of_academic_year()) {
        //     $tags[] = "New student " . ($current_academic_year) . '/' . ($current_academic_year + 1);
        // }

        //$studentArray['Voted '.$academicYearName] = ''; // @todo
        //$studentArray['Club/Society member '.$academicYearName] = ''; // @todo
        $studentArray['Tags'] = implode(", ", $tags);

        // $tags['Logged in to website'] =  empty($user->login) ? 'FALSE' : 'TRUE';

        $this->addLine($studentArray, $context);
      }
    }
  }

  /**
   * Get taxonomy term name or or other term field
   * from student entity reference field
   *
   * @param        $student
   * @param        $taxonomy
   * @param string $field
   * @return string
   */
  public function getTerm($student, $taxonomy, $field = 'name') {
    $value = '';
    $term = $student ? Term::load($student->get($taxonomy)->getString()) : FALSE;
    if ($term) {
      $value = $term->get($field)->value;
    }
    return $value;
  }

  public function sortResults($rows, $results) {
    shuffle($rows);
    return $rows;
  }
}
