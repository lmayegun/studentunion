<?php

namespace Drupal\su_students\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\user\Entity\User;

/**
 * Implement Class BulkUserImport for import form.
 */
class StudentImportEmergencyForm extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_clubs_societies_csv_emergency';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<p>NOTE this import assumes we want to opt in any students imported. DO NOT USE if the student is not opted in. <br><br>
                This is to manually fix certain fields on the student profile without dgenerating a duplicate profile. Edit programatically as needed. Upload a file with the following columns:<ul>' .
        '<li><b>ucl_user_id</b> to match user OR provide uid</li>' .
        '<li><b>uid</b> to match user OR provide ucl_user_id</li>' .
        '<li><b>dob</b> in format YYYY-MM-DD</li>' .
        '<li><b>study_start</b> in format YYYY-MM-DD</li>' .
        '<li><b>study_end</b> in format YYYY-MM-DD</li>' .
        '</ul></p>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $record, array &$context) {
    if (isset($record['ucl_user_id']) && $record['ucl_user_id']) {
      $users = Drupal::entityQuery('user')
        ->condition('field_ucl_user_id', $record['ucl_user_id'])
        ->execute();
    }
    elseif (isset($record['uid']) && $record['uid']) {
      $users = Drupal::entityQuery('user')
        ->condition('uid', $record['uid'])
        ->execute();
    }
    else {
      static::addError($record, $context, 'Can\'t find user - no user ID or uid.');
      return FALSE;
    }

    if (count($users) == 0) {
      static::addError($record, $context, 'Can\'t find user.');
      return FALSE;
    }

    $userId = reset($users);
    $user = User::load($userId);
    $studentService = Drupal::service('su_students.student_services');

    $student = $studentService->getStudentProfile($user, TRUE);

    $dateFields = [
      'dob' => 'field_student_date_of_birth',
      'study_start' => 'field_student_study_start',
      'study_end' => 'field_student_study_end',
    ];
    foreach ($dateFields as $source => $destination) {
      if (isset($record[$source]) && $record[$source] && strtotime($record[$source]) > 0) {
        $student->set($destination, gmdate(DateTimeItemInterface::DATE_STORAGE_FORMAT, strtotime($record[$source])));
      }
    }

    // Need to set this to make anything make sense
    $student->set('field_profile_optin', TRUE);
    $student->set('field_profile_optin_changed', Drupal::time()
      ->getRequestTime());

    $student->save();

    return TRUE;
  }

}
