<?php

namespace Drupal\su_students\Controller;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\profile\Entity\Profile;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Implements Class UserImportController Controller.
 */
class StudentImportController extends ControllerBase {

  public function notCurrentButClubSoc() {
    $result = [];

    $studentService = \Drupal::service('su_students.student_services');

    // $studentsByRole [4491] => 4491
    $usersToAnonymise = $studentService->getAllStudentsByStudentRole();
    foreach ($usersToAnonymise as $uid) {
      $user = User::load($uid);
      /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
      $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');
      $clubSocs = $clubSocMembershipsService->countMembershipsForUser($user);
      $studentProfile = $studentService->getStudentProfile($user, FALSE);
      if ($studentService::isStudentCurrentMember($studentProfile)) {
        continue;
      }
      if ($clubSocs > 0) {
        $message = t(
          'STUDENT @details IS MEMBER OF CLUB/SOCS: @count',
          [
            '@details' => $user->id(),
            '@count' => $clubSocs,
          ]
        );
        \Drupal::logger('su_students')->error($message);
        $result[] = ['#markup' => t(
          '<p>STUDENT @details IS MEMBER OF CLUB/SOCS: @count</p>',
          ['@details' => $user->id(), '@count' => $clubSocs]
        )];
      }
    }
    // if (count($result) > 10) {
    return array_merge(
      ['#markup' => count($result) . ' results<br><br>'],
      $result
    );
    // }
  }
}
