<?php

namespace Drupal\su_students\Plugin\EntityReferenceSelection;

use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;
use Drupal\taxonomy\Plugin\EntityReferenceSelection\TermSelection;

/**
 * Used to filter terms to include only children.
 *
 * Useful for departments which are always children of Faculty terms.
 *
 * @EntityReferenceSelection(
 *   id = "default:terms_children",
 *   label = @Translation("Child terms (non-top-level) only"),
 *   entity_types = {"taxonomy_term"},
 *   group = "taxonomy_term",
 *   weight = 5
 * )
 */
class TermChildrenOnly extends TermSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('parent', 0, '>');
    return $query;
  }
}
