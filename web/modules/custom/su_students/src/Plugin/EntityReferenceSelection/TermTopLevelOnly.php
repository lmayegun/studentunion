<?php

namespace Drupal\su_students\Plugin\EntityReferenceSelection;

use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;
use Drupal\taxonomy\Plugin\EntityReferenceSelection\TermSelection;

/**
 * Used to filter terms to include only top level.
 *
 * Useful for faculties which are always top level.
 *
 * @EntityReferenceSelection(
 *   id = "default:terms_top_level",
 *   label = @Translation("Top level terms only"),
 *   entity_types = {"taxonomy_term"},
 *   group = "taxonomy_term",
 *   weight = 5
 * )
 */
class TermTopLevelOnly extends TermSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('parent', 0);
    return $query;
  }
}
