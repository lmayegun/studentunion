<?php

namespace Drupal\su_students\Plugin\UserRegistrationCondition;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\su_login\Plugin\UserRegistrationConditionPluginBase;
use Drupal\su_login\Plugin\UserRegistrationConditionPluginInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @see \Drupal\su_login\Annotation\UserRegistrationConditionPlugin
 * @see \Drupal\su_login\Plugin\UserRegistrationConditionPluginInterface
 *
 * @UserRegistrationConditionPlugin(
 *   id = "optin_condition",
 *   description = @Translation("Student can choose to opt in."),
 *   userRoles = {
 *     "student",
 *   }
 * )
 */
class OptinCondition extends UserRegistrationConditionPluginBase implements UserRegistrationConditionPluginInterface {
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Provide a redirect condition - must return true or user will be redirected
   *
   * @param User $user
   * @return
   */
  public function condition(User $user) {
    $studentService = \Drupal::service('su_students.student_services');
    return $studentService->isStudentOptin($user) || $studentService->isStudentOptout($user);
  }

  /**
   * Check that optin has been checked
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function isConditionAccepted($form, $form_state, $user) {
    return $form_state->getValue('optin');
  }

  /**
   * Get the value from the checkbox condition form element
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function isConditionRevoked($form, $form_state, $user) {
    return !$form_state->getValue('optin');
  }

  /**
   * Check if this plugin applies to this user
   *
   * In this case, only if they have no student profile
   */
  public function appliesToUser($user) {
    $studentService = \Drupal::service('su_students.student_services');
    $profile = $studentService->getStudentProfile($user);
    return $profile ? FALSE : TRUE;
  }

  /**
   * Alter accept form to add student condition
   *
   * @param $form
   * @param $user
   * @param $config
   */
  public function alterAcceptForm(&$form, $user, &$config) {
    $studentService = \Drupal::service('su_students.student_services');
    $profile = $studentService->getStudentProfile($user, TRUE);

    if ($profile) {
      $config = \Drupal::config('su_students.suloginconfiguration');

      $form['optin_info'] = [
        '#type'   => 'item',
        '#markup' => $config->get('optin_info.value'),
        '#title'  => t('Optin'),
        '#weight' => -15,
      ];

      $active = [TRUE => t('Yes'), FALSE => t('No')];

      $form['optin'] = [
        '#type'          => 'radios',
        '#title'         => t('Do you want to opt-in to membership?'),
        '#weight'        => -15,
        '#required'      => TRUE,
        '#default_value' => TRUE,
        '#options'       => $active,
        '#description'   => $config->get('optin_description.value')
      ];
    }
  }

  /**
   * Save condition
   *
   * IF they accept:
   *  Add member role
   *  check optin
   *  add optin date
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterAcceptFormSubmit($form, $form_state, $user) {
    $studentService = \Drupal::service('su_students.student_services');
    if ($this->isConditionAccepted($form, $form_state, $user)) {
      $studentService::optInStudent($user);
    } else {
      $studentService->optOutAndScheduleAnonymiseStudent($user);
    }
  }

  /**
   * Default revoke form
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterRevokeForm(&$form, $user, &$config) {
    $form['optout_info'] = [
      '#type'   => 'item',
      '#markup' => $config->get('optout_info.value'),
      '#title'  => t('Optout'),
      '#weight' => -15,
    ];

    $form['optout'] = [
      '#type'          => 'checkbox',
      '#title'         => t('I would like to cancel my membership and opt out.'),
      '#description'   => $config->get('optout_description.value'),
      '#weight'        => -15,
      '#default_value' => FALSE,
    ];
  }

  /**
   * Send an email to portico and to su.systems
   * Anonymise student profile
   *
   * IF they revoke:
   *  anonymiseStudent:
   *    remove member role
   *    anonymiseProfile:
   *      set all profile fields to null except:
   *        field_profile_optin which is unchecked
   *        field_profile_optin_changed with the date
   *
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterRevokeFormSubmit($form, $form_state, $user) {
    if ($revoke = $this->isConditionRevoked($form, $form_state, $user)) {
      $user = \Drupal::currentUser();
      $user = User::load($user->id());
      $studentService = \Drupal::service('su_students.student_services');
      $studentService->optOutAndScheduleAnonymiseStudent($user);
    }
  }

  /**
   * Add optin text and description into admin panel
   *
   * @param $form
   * @param $form_state
   * @param $config
   */
  public function alterAdminConfigForm(&$form, $form_state, &$config) {
    $route_provider = \Drupal::service('router.route_provider');
    $link = $route_provider->getRouteByName('su_login.accept_required')->getPath();

    $form['student_condition'] = [
      '#type'  => 'details',
      '#title' => t('Student condition'),
      '#open'  => TRUE,
    ];

    $form['student_condition']['optin_details'] = [
      '#type'  => 'details',
      '#title' => t('Accept form: OPTIN'),
      '#open'  => FALSE,
    ];

    $form['student_condition']['optin_details']['optin_info'] = array(
      '#title'         => t('Optin info'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('optin_info.value'),
      '#format'        => $config->get('optin_info.format'),
    );

    $form['student_condition']['optin_details']['optin_description'] = array(
      '#title'         => t('Optin description'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('optin_description.value'),
      '#format'        => $config->get('optin_description.format'),
    );

    $form['student_condition']['optout_details'] = [
      '#type'  => 'details',
      '#title' => t('Revoke form: OPTOUT'),
      '#open'  => FALSE,
    ];

    $form['student_condition']['optout_details']['optout_info'] = array(
      '#title'         => t('Optout info'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('optout_info.value'),
      '#format'        => $config->get('optout_info.format'),
    );

    $form['student_condition']['optout_details']['optout_description'] = array(
      '#title'         => t('Optout description'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('optout_description.value'),
      '#format'        => $config->get('optout_description.format'),
    );
  }

  /**
   * Save admin configuration values
   *
   * @param $form
   * @param $form_state
   * @param $config
   */
  public function alterAdminConfigFormSubmit($form, $form_state, &$config) {
    $values = $form_state->getValues();
    $config->set('optin_info.value', $values['optin_info']['value'])
      ->set('optin_info.format', $values['optin_info']['format'])
      ->set('optin_description.value', $values['optin_description']['value'])
      ->set('optin_description.format', $values['optin_description']['format'])
      ->set('optout_info.value', $values['optout_info']['value'])
      ->set('optout_info.format', $values['optout_info']['format'])
      ->set('optout_description.value', $values['optout_description']['value'])
      ->set('optout_description.format', $values['optout_description']['format']);
  }

  public function redirect($plugin, $form_state) {
//    \Drupal::logger('su_students')->warning('optin redirect');
    if (strpos($plugin->getRedirectDestination(), 'http') !== FALSE) {
      $destination = $plugin->getRedirectDestination() ?? 'https://studentsunionucl.org/user';
      $url = Url::fromUri($destination);
    } else {
      $destination = "/" . $plugin->getRedirectDestination() ?? '/user';
      $url = Url::fromUserInput($destination);
    }
    $routeName = $url->isRouted() ? $url->getRouteName() : '<front>';
    $form_state->setRedirect($routeName, $url->isRouted() ? $url->getRouteParameters() : []);
  }
}
