<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;


use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_department",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Department"),
 *   display_label = @Translation("Department"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionDepartment extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['ucl_faculties_and_departments'];
  const SELECT_TITLE = 'Department';
  const REQUIREMENT_LABEL_ANY = 'Student is in at least one of these departments';
  const REQUIREMENT_LABEL_ONE = 'Student is in this department';
  const REQUIREMENT_LABEL_ALL = 'Student is in all of these departments';
  const STUDENT_PROFILE_FIELD = 'field_student_department';
  const SELECTION_HANDLER = 'default:terms_children';
}
