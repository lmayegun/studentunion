<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_hall",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Hall of Residence"),
 *   display_label = @Translation("Hall of Residence"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionHallOfResidence extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['students_halls_of_residence'];
  const SELECT_TITLE = 'Hall of residence';
  const REQUIREMENT_LABEL_ONE = 'Student is in this hall of residence';
  const REQUIREMENT_LABEL_ANY = 'Student is in at least one of these halls of residence';
  const REQUIREMENT_LABEL_ALL = 'Student is in all of these halls of residence';
  const STUDENT_PROFILE_FIELD = 'field_student_hall_of_residence';
}
