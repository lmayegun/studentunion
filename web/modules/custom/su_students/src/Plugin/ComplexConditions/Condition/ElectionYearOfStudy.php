<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_year_of_study",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Year of study"),
 *   display_label = @Translation("Year of study"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionYearOfStudy extends StudentDataFieldBase {
  const SELECT_TITLE = 'Year of study';
  const REQUIREMENT_LABEL_ONE = 'Student is in this year of study';
  const REQUIREMENT_LABEL_ANY = 'Student is in at least one of these years of study';
  const REQUIREMENT_LABEL_ALL = 'Student is in all of these years of study';
  const STUDENT_PROFILE_FIELD = 'field_student_year_of_study';
}
