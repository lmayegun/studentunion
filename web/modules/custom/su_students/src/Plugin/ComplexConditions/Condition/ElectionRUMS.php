<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\ConditionRequirement;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_rums",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("RUMS"),
 *   display_label = @Translation("UCL Medical School department or MBBS course or Intercalated BSc course"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionRUMS extends ComplexConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $department = NULL;
    $course = NULL;

    $inMedSchool = FALSE;
    $inCourse = FALSE;
    $exemption = FALSE;

    $studentService = \Drupal::service('su_students.student_services');
    $profile = $studentService::getStudentProfile($account);
    if ($profile) {
      $department = $profile->field_student_department->entity ? $profile->field_student_department->entity : NULL;
      $inMedSchool = $department ? $department->id() == 41506 : FALSE;

      $course = $profile->field_student_course->entity ? $profile->field_student_course->entity : NULL;
      $inCourse = $course ? ($course->id() == 41525 || $course->id() == 42318) : FALSE;
    }

    if (!$inMedSchool && !$inCourse) {
      $exemption = static::checkExemption($account);
    }

    $requirements['rums'] = new ConditionRequirement([
      'id' => 'rums',
      'label' => t("Must be a member of RUMS by either being a current student in the UCL Medical School department, or in the MBBS or Intercalated BSc course.", []),
      'description' => t('@user: department @department, course @course.', [
        '@user' => $account->getDisplayName(),
        '@department' => $department ? $department->label() : 'unknown',
        '@course' => $course ? $course->label() : 'unknown',
      ]),
      'pass' => $inMedSchool || $inCourse || $exemption,
    ]);

    return $requirements;
  }

  public static function checkExemption(AccountInterface $account) {
    if (empty(ElectionConditionExemptionType::load('rums'))) {
      return FALSE;
    }

    $countExemptions = \Drupal::entityQuery('election_condition_exemption')
      ->condition('bundle', 'rums')
      ->condition('status', 1)
      ->condition('field_exempt_user', [$account->id()], 'IN')
      ->count()
      ->execute();

    return $countExemptions > 0;
  }
}
