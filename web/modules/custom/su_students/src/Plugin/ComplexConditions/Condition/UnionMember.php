<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\Annotation\ComplexConditions;
use Drupal\complex_conditions\ConditionRequirement;
use Drupal\complex_conditions\Event\ConditionRequirementEvents;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ConditionBase;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\UserRole;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType;
use Drupal\user\Entity\Role;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "union_member",
 *   label = @Translation("Union member"),
 *   display_label = @Translation("User is a current member of the Union"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class UnionMember extends UserRole {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['user_roles']['#access'] = FALSE;
    $form['user_roles_any_all']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['user_roles'] = ['member', 'officer'];
    $this->configuration['user_roles_any_all'] = 'any';
  }

  /**
   * {@inheritdoc}
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $requirements = parent::evaluateRequirements($entity, $account, $parameters);

    if (isset($requirements['has_any_roles']) && $requirements['has_any_roles']) {
      if ($requirements['has_any_roles']->isFailed()) {
        $exemption = static::checkExemption($account, $entity->getElection());
        $requirements['has_any_roles']->setPass($exemption);
      }

      $requirements['has_any_roles']->setLabel(t('Must be a current Union member and current UCL student.'));
      $requirements['has_any_roles']->setDescription(t('This is determined by data we are provided by UCL. If you are a current student but not a Union member, you can opt in via <a href="/optin">the opt in form</a> but note this may take several days to process. Current sabbatical officers are not opted in this way but can nominate and vote as a member.'));
    }

    return $requirements;
  }

  public function generateRandomConditionConfig() {
    $config = parent::generateRandomConditionConfig();
    $config['target_plugin_configuration']['user_roles'] = ['member', 'officer'];

    return $config;
  }

  public static function checkExemption(AccountInterface $account, ElectionInterface $election) {
    if (empty(ElectionConditionExemptionType::load('union_membership'))) {
      return FALSE;
    }

    $exemptions = \Drupal::entityQuery('election_condition_exemption')
      ->condition('bundle', 'union_membership')
      ->condition('status', 1)
      ->condition('field_exempt_user', [$account->id()], 'IN')
      ->condition('field_exempt_election', [$election->id()], 'IN')
      ->execute();

    if (count($exemptions) == 0) {
      return FALSE;
    } else {
      return TRUE;
    }
  }
}
