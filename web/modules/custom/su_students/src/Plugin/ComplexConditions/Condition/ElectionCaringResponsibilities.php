<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_caring",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Caring responsibilities"),
 *   display_label = @Translation("Caring responsibilities"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionCaringResponsibilities extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['students_caring_responsibilities'];
  const SELECT_TITLE = 'Caring responsibilities';
  const REQUIREMENT_LABEL_ONE = 'Student has this caring responsibility';
  const REQUIREMENT_LABEL_ANY = 'Student has at least one of these caring responsibilities';
  const REQUIREMENT_LABEL_ALL = 'Student has all of these caring responsibilities';
  const STUDENT_PROFILE_FIELD = 'field_student_caring_resp';
}
