<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\ConditionRequirement;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\Election;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemption;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

abstract class StudentDataTaxonomyBase extends ElectionConditionBase {
  const TARGET_ENTITY_TYPE = 'taxonomy_term';
  const TARGET_BUNDLES = [];
  const SELECT_TITLE = 'Term';
  const REQUIREMENT_LABEL_ANY = 'User has at least one of these terms:';
  const REQUIREMENT_LABEL_ONE = 'User has this term';
  const REQUIREMENT_LABEL_ALL = 'User has all of these terms:';
  const STUDENT_PROFILE_FIELD = '';
  const SELECTION_HANDLER = NULL;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entities' => [],
      'any_or_all' => 'any',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $groupOptions = [];
    $form['entities'] = [
      '#title' => $this->t(static::SELECT_TITLE),
      '#type' => 'entity_autocomplete',
      '#target_type' => static::TARGET_ENTITY_TYPE,
      '#selection_settings' => [
        'target_bundles' => static::TARGET_BUNDLES,
      ],
      '#tags' => TRUE,
      '#default_value' => Term::loadMultiple(array_column($this->configuration['entities'], 'target_id')),
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    if (static::SELECTION_HANDLER) {
      $form['entities']['#selection_handler'] = static::SELECTION_HANDLER;
    }

    $form['any_or_all'] = [
      '#type' => 'select',
      '#title' => $this->t('In any or all'),
      '#options' => [
        'any' => 'Any',
        'all' => 'All',
      ],
      '#default_value' => $this->configuration['any_or_all'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['entities'] = $values['entities'];
    $this->configuration['any_or_all'] = $values['any_or_all'];
  }

  public function getUserValue(AccountInterface $account) {
    $studentService = \Drupal::service('su_students.student_services');

    $profile = $studentService::getStudentProfile($account);
    if (!$profile) {
      return NULL;
    }

    $field = $profile->get(static::STUDENT_PROFILE_FIELD);
    if (!$field) {
      return NULL;
    }

    $referenced = $field->referencedEntities();
    return $referenced;
  }

  /**
   * Return true or false if access.
   *
   * @param string $phase
   *
   * @return boolean
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->assertParameters($parameters);
    $requirements = [];

    $storage = \Drupal::entityTypeManager()->getStorage(static::TARGET_ENTITY_TYPE);
    $target_entity_ids = array_column($this->configuration['entities'], 'target_id');
    // $target_entity_ids = array_keys($target_entities);
    if (!is_object(reset($target_entity_ids))) {
      $target_entities = $storage->loadMultiple($target_entity_ids);
    } else {
      $target_entities = $target_entity_ids;
      $target_entity_ids = [];
      foreach ($target_entities as $target_entity) {
        $target_entity_ids[] = $target_entity->id();
      }
    }
    if (!$target_entities) {
      return [];
    }
    $names = [];
    foreach ($target_entities as $entity) {
      if ($entity) {
        $names[] = $entity->label();
      }
    }

    // Get user's values for the taxonomy (e.g. what their department is)
    $user_entities = $this->getUserValue($account);
    $user_entity_ids = [];
    if ($user_entities) {
      foreach ($user_entities as $entity) {
        $user_entity_ids[] = $entity->id();
      }
    }

    // If we don't have an overlap yet, check if any exemptions
    if (count(array_intersect($target_entity_ids, $user_entity_ids)) == 0) {
      $user_entity_ids = array_merge($user_entity_ids, $this->getExemptionIds($account));
    }

    $studentService = \Drupal::service('su_students.student_services');

    $profile = $studentService::getStudentProfile($account);
    $requirements['student_member'] = new ConditionRequirement([
      'id' => 'student_member',
      'label' => t('User must be a current UCL student and opted-in Union member', [
        '@names' => implode(', ', $names),
        '@message' => static::REQUIREMENT_LABEL_ANY,
      ]),
      'description' => t('Current students can <a href="/optin" target="_blank">opt in to membership here</a>, though note it may take several days to process.'),
      'pass' => $studentService->isStudentCurrentMember($profile),
    ]);

    if ($this->configuration['any_or_all'] == 'any') {
      $any = $user_entity_ids && count(array_intersect($target_entity_ids, $user_entity_ids)) > 0;
      $requirements['has_any_' . static::STUDENT_PROFILE_FIELD] = new ConditionRequirement([
        'id' => 'has_any_' . static::STUDENT_PROFILE_FIELD,
        'label' => t('@message: @names', [
          '@names' => implode(', ', $names),
          '@message' => count($names) == 1 ? static::REQUIREMENT_LABEL_ONE : static::REQUIREMENT_LABEL_ANY,
        ]),
        'pass' => $any,
      ]);
    } else {
      $all = $user_entity_ids && count(array_intersect($target_entity_ids, $user_entity_ids)) == count($target_entity_ids);
      $requirements['has_all_' . static::STUDENT_PROFILE_FIELD] = new ConditionRequirement([
        'id' => 'has_all_' . static::STUDENT_PROFILE_FIELD,
        'label' => t('@message: @names', [
          '@names' => implode(', ', $names),
          '@message' => count($names) == 1 ? static::REQUIREMENT_LABEL_ONE : static::REQUIREMENT_LABEL_ALL,
        ]),
        'pass' => $all,
      ]);
    }

    return $requirements;
  }

  public function generateRandomConditionConfig() {
    $config = parent::generateRandomConditionConfig();
    $target_entities = [];
    foreach (static::TARGET_BUNDLES as $type) {
      $storage = \Drupal::entityTypeManager()->getStorage(static::TARGET_ENTITY_TYPE);
      $target_entities = array_merge($target_entities, $storage->loadByProperties(['vid' => $type]));
    }
    $randomTerms = array_rand($target_entities, random_int(1, min(4, count($target_entities))));
    if (!is_array($randomTerms)) {
      $randomTerms = [$randomTerms];
    }
    $randomTermsTarget = [];
    foreach ($randomTerms as $term) {
      $randomTermsTarget[] = ['target_id' => $target_entities[$term]];
    }

    $config['target_plugin_configuration'] = array_merge($config['target_plugin_configuration'], [
      'entities' => $randomTermsTarget,
      'any_or_all' => random_int(0, 3) == 0 ? 'all' : 'any',
    ]);
    return $config;
  }

  public function getExemptionIds($account) {
    $ids = [];
    foreach (static::TARGET_BUNDLES as $bundle) {
      $exemption_type_id = substr('t_' . $bundle, 0, 31);
      if (empty(ElectionConditionExemptionType::load($exemption_type_id))) {
        $this->createExemptionType($bundle);
        return [];
      }

      $exemptions = \Drupal::entityQuery('election_condition_exemption')
        ->condition('status', 1)
        ->condition('bundle', $exemption_type_id)
        ->condition('field_exempt_user', [$account->id()], 'IN')
        ->execute();

      if (count($exemptions) == 0) {
        continue;
      }

      $exemptions = ElectionConditionExemption::loadMultiple($exemptions);
      foreach ($exemptions as $exemption) {
        $entities = $exemption->field_term->referencedEntities();
        foreach ($entities as $entity) {
          $ids[] = $entity->id();
        }
      }
    }

    return $ids;
  }

  public function createExemptionType($bundle) {
    $vocabulary = Vocabulary::load($bundle);
    if (!$vocabulary) {
      return;
    }

    $exemption_type_id = substr('t_' . $bundle, 0, 31);

    $new = ElectionConditionExemptionType::create([
      'id' => $exemption_type_id,
      'label' => static::SELECT_TITLE,
    ]);
    $new->save();

    $entity_type = 'election_condition_exemption';
    $fields = [
      'field_exempt_user' => [
        'label' => 'Exempt user(s)',
        'type' => 'entity_reference',
        'entity_target_type' => 'user',
        'target_bundles' => NULL,
      ],
      'field_term' => [
        'label' => 'Exempt values',
        'type' => 'entity_reference',
        'entity_target_type' => 'taxonomy_term',
        'target_bundles' => static::TARGET_BUNDLES,
      ],
    ];

    foreach ($fields as $field_name => $settings) {

      if (!$field_storage = FieldStorageConfig::loadByName($entity_type, $field_name)) {
        FieldStorageConfig::create([
          'field_name' => $field_name,
          'entity_type' => $entity_type,
          'type' => $settings['type'],
          'cardinality' => -1,
          'settings' => [
            'target_type' => $settings['entity_target_type'],
            'include_anonymous' => FALSE,
          ],
        ])->save();
      }

      FieldConfig::create([
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $exemption_type_id,
        'label' => $settings['label'],
        'cardinality' => -1,
        'required' => TRUE,

        // Optional to target bundles.
        'settings' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => $settings['target_bundles'],
            'auto_create' => 'false',
            'sort' => ['field' => '_none', 'direction' => 'ASC'],
            'filter' => ['type' => '_none'],
            'include_anonymous' => FALSE,
          ],
        ],
      ])->save();

      $form_display = EntityFormDisplay::load($entity_type . '.' . $exemption_type_id . '.default');
      if (!$form_display) {
        $form_display = EntityFormDisplay::create([
          'targetEntityType' => $entity_type,
          'bundle' => $exemption_type_id,
          'mode' => 'default',
          'status' => TRUE,
        ]);
        $form_display->save();
      }
      $form_display->setComponent($field_name, ['type' => 'autocomplete_deluxe'])
        ->save();
    }
  }
}
