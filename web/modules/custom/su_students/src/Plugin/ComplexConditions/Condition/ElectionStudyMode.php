<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_study_mode",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Study mode"),
 *   display_label = @Translation("Study mode"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionStudyMode extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['students_study_modes'];
  const SELECT_TITLE = 'Study mode';
  const REQUIREMENT_LABEL_ONE = 'Student has this study mode';
  const REQUIREMENT_LABEL_ANY = 'Student has at least one of these study modes';
  const REQUIREMENT_LABEL_ALL = 'Student has all of these study modes';
  const STUDENT_PROFILE_FIELD = 'field_student_study_mode';
}
