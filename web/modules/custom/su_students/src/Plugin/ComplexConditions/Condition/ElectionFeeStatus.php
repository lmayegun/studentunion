<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_hall",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Fee status (UK, EU, Overseas)"),
 *   display_label = @Translation("Fee status (UK, EU, Overseas)"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionFeeStatus extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['students_fee_statuses'];
  const SELECT_TITLE = 'Fee status';
  const REQUIREMENT_LABEL_ONE = 'Student has this fee status';
  const REQUIREMENT_LABEL_ANY = 'Student has at least one of these fee statuses';
  const REQUIREMENT_LABEL_ALL = 'Student has all of these fee statuses';
  const STUDENT_PROFILE_FIELD = 'field_student_fee_status';
}
