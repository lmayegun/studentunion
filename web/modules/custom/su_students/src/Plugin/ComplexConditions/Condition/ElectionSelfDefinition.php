<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\complex_conditions\ConditionRequirement;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election\Phase\PhaseInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\UserInputRequired;
use Drupal\profile\Entity\Profile;

/**
 * Require an action before voting
 *
 * In this case, a text is shown and checkbox, but could e.g. be a self-definition question
 *
 * @ComplexCondition(
 *   id = "su_students_self_definition",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Self-definition required"),
 *   display_label = @Translation("Self-definition required"),
 *   category = @Translation("Actions"),
 *   weight = 0,
 * )
 */
class ElectionSelfDefinition extends UserInputRequired {

  const SELF_DEFINITION_OPTIONS = [
    'bme' => 'BME',
    'bi' => 'Bisexual',
    'asexual' => 'Asexual',
    'carer' => 'Carer',
    'disabled' => 'Disabled',
    'trans' => 'Trans',
    'lgbq' => 'LGBQ',
    'woman' => 'Woman',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    unset($form['text_interest']);
    unset($form['text_nominations']);
    unset($form['text_voting']);
    unset($form['checkbox_text']);

    $form['field'] = [
      '#type' => 'select',
      '#multiple' => FALSE,
      '#title' => $this->t('Self-definition category'),
      '#options' => static::SELF_DEFINITION_OPTIONS,
      '#default_value' => $this->configuration['field'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['field'] = $values['field'];
  }

  public function getPrefixText(PhaseInterface $phase) {
    $key = $this->configuration['field'];
    $label = static::SELF_DEFINITION_OPTIONS[$key];

    $this->configuration['text_interest'] = 'You must self-define as ' . $label . ' in order to express interest in this position.';
    $this->configuration['text_nominations'] = 'You must self-define as ' . $label . ' in order to nominate for this position.';
    $this->configuration['text_voting'] = 'You must self-define as ' . $label . ' in order to vote for this position.';

    return $this->configuration['text_' . $phase->id()];
  }

  public function getCheckboxText(PhaseInterface $phase) {
    $key = $this->configuration['field'];
    $label = static::SELF_DEFINITION_OPTIONS[$key];

    $this->configuration['checkbox_text'] = t('I self define as @label', ['@label' => $label]);
    return $this->configuration['checkbox_text'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->assertParameters($parameters);
    $requirements = [];

    $keys = [$this->configuration['field']];
    foreach ($keys as $key) {
      $profile = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser($account, 'self_define');
      $pass = FALSE;
      if ($profile && $profile->hasField('field_self_define_' . $key)) {
        $pass = $profile->get('field_self_define_' . $key)->getValue() ?: FALSE;
      }

      $description = NULL;
      if (!$pass) {
        $description = t('<a href="@link">Self define here</a>', [
          '@link' => '/user/' . $account->id() . '/self_define',
        ]);
      }

      $requirements['self_defined_' . $key] = new ConditionRequirement([
        'id' => 'self_defined_' . $key,
        'label' => t('User must self-define as @definition', [
          '@definition' => static::SELF_DEFINITION_OPTIONS[$key],
        ]),
        'description' => $description,
        'pass' => $pass,
        'requires_user_input' => !$pass,
      ]);
    }

    return $requirements;
  }

  public function ajaxUserInputRequiredConfirmationChange(array &$form, FormStateInterface $form_state) {
    $field = $this->configuration['field'];
    $checkboxValue = $form_state->getValue('confirm_checkbox');
    if ($checkboxValue) {
      $profile = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser(\Drupal::currentUser(), 'self_define');
      if (!$profile) {
        $profile = Profile::create([
          'type' => 'self_define',
          'uid'  => \Drupal::currentUser()->id(),
        ]);
        $profile->save();
      }
      $profile->set('field_self_define_' . $field, 'Yes');
      $profile->save();

      $election_post = $this->getElectionPostFromContext();
      $election_post->resetEligibility(\Drupal::currentUser());
    }
  }

  public function generateRandomConditionConfig() {
    $config = parent::generateRandomConditionConfig();

    $config['target_plugin_configuration'] = array_merge($config['target_plugin_configuration'], [
      'field' => array_rand(static::SELF_DEFINITION_OPTIONS),
    ]);
    return $config;
  }
}
