<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_faculty",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Faculty"),
 *   display_label = @Translation("Faculty"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionFaculty extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['ucl_faculties_and_departments'];
  const SELECT_TITLE = 'Faculty';
  const REQUIREMENT_LABEL_ANY = 'Student is in at least one of these faculties';
  const REQUIREMENT_LABEL_ONE = 'Student is in this faculty';
  const REQUIREMENT_LABEL_ALL = 'Student is in all of these faculties';
  const STUDENT_PROFILE_FIELD = 'field_student_faculty';
  const SELECTION_HANDLER = 'default:terms_top_level';
}
