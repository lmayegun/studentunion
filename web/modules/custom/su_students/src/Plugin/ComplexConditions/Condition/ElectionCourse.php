<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;


use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_course",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Course"),
 *   display_label = @Translation("Course"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionCourse extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['ucl_courses'];
  const SELECT_TITLE = 'Course';
  const REQUIREMENT_LABEL_ANY = 'Student is in at least one of these courses';
  const REQUIREMENT_LABEL_ONE = 'Student is in this course';
  const REQUIREMENT_LABEL_ALL = 'Student is in all of these courses';
  const STUDENT_PROFILE_FIELD = 'field_student_course';
}
