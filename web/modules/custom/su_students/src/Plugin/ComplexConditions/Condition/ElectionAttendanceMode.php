<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ComplexConditions\Condition\GroupRole as ConditionGroupRole;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\su_students\Plugin\ComplexConditions\Condition\StudentDataTaxonomyBase;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_su_student_attendance_mode",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Attendance mode"),
 *   display_label = @Translation("Attendance mode"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionAttendanceMode extends StudentDataTaxonomyBase {
  const TARGET_BUNDLES = ['students_attendance_modes'];
  const SELECT_TITLE = 'Attendance mode';
  const REQUIREMENT_LABEL_ONE = 'Student has this attendance mode';
  const REQUIREMENT_LABEL_ANY = 'Student has at least one of these attendance modes';
  const REQUIREMENT_LABEL_ALL = 'Student has all of these attendance modes';
  const STUDENT_PROFILE_FIELD = 'field_student_attendance_mode';
}
