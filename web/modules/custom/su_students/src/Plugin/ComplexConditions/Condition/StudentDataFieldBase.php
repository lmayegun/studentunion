<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\ConditionRequirement;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\taxonomy\Entity\Term;

abstract class StudentDataFieldBase extends ElectionConditionBase {
  const TARGET_ENTITY_TYPE = 'profile';
  const TARGET_BUNDLES = [];
  const SELECT_TITLE = 'Term';
  const REQUIREMENT_LABEL_ANY = 'User has at least one of these terms';
  const REQUIREMENT_LABEL_ONE = 'User has this term';
  const REQUIREMENT_LABEL_ALL = 'User has all of these terms';
  const STUDENT_PROFILE_FIELD = 'field_student_year_of_study';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'value' => [],
    ] + parent::defaultConfiguration();
  }

  public function getFieldPossibleValues() {
    $query = \Drupal::database()
      ->select(static::TARGET_ENTITY_TYPE . '__' . static::STUDENT_PROFILE_FIELD, 'p')
      ->fields('p', [static::STUDENT_PROFILE_FIELD . '_value'])
      ->distinct(TRUE)
      ->execute()
      ->fetchCol();
    asort($query);
    $results = [];
    foreach ($query as $key => $value) {
      $results[$value] = $value;
    }
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $groupOptions = [];
    $form['value'] = [
      '#title' => $this->t(static::SELECT_TITLE),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $this->getFieldPossibleValues(),
      '#default_value' => $this->configuration['value'],
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    $form['any_or_all'] = [
      '#type' => 'select',
      '#title' => $this->t('Any or all'),
      '#options' => [
        'any' => 'Any',
        'all' => 'All',
      ],
      '#default_value' => $this->configuration['any_or_all'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['value'] = $values['value'];
    $this->configuration['any_or_all'] = $values['any_or_all'];
  }

  public function getUserValue(AccountInterface $account) {
    $studentService = \Drupal::service('su_students.student_services');

    $profile = $studentService::getStudentProfile($account);
    if (!$profile) {
      return NULL;
    }

    $field = $profile->get(static::STUDENT_PROFILE_FIELD);
    if (!$field) {
      return NULL;
    }

    $referenced = $field->getString();
    return $referenced;
  }

  /**
   * Return true or false if access.
   *
   * @param string $phase
   *
   * @return boolean
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->assertParameters($parameters);
    $requirements = [];

    $names = !is_array($this->configuration['value']) ? [] : $this->configuration['value'];
    $user_values = $this->getUserValue($account);

    $studentService = \Drupal::service('su_students.student_services');

    $profile = $studentService::getStudentProfile($account);
    $requirements['student_member'] = new ConditionRequirement([
      'id' => 'student_member',
      'label' => t('User must be a current UCL student and opted-in Union member', []),
      'description' => t('Current students can <a href="/optin" target="_blank">opt in to membership here</a>, though note it may take several days to process.'),
      'pass' => $studentService->isStudentCurrentMember($profile),
    ]);

    if ($this->configuration['any_or_all'] == 'any') {
      $intersect = in_array($user_values, $this->configuration['value']);
      $any = $user_values && $intersect;
      $requirements['has_any_' . static::STUDENT_PROFILE_FIELD] = new ConditionRequirement([
        'id' => 'has_any_' . static::STUDENT_PROFILE_FIELD,
        'label' => t('@message: @names', [
          '@names' => implode(', ', $names),
          '@message' => count($names) == 1 ? static::REQUIREMENT_LABEL_ONE : static::REQUIREMENT_LABEL_ANY,
        ]),
        'pass' => $any,
      ]);
    } else {
      $all = $user_values && count(array_intersect($this->configuration['value'], $user_values)) == count($this->configuration['value']);
      $requirements['has_all_' . static::STUDENT_PROFILE_FIELD] = new ConditionRequirement([
        'id' => 'has_all_' . static::STUDENT_PROFILE_FIELD,
        'label' => t('@message: @names', [
          '@names' => implode(', ', $names),
          '@message' => count($names) == 1 ? static::REQUIREMENT_LABEL_ONE : static::REQUIREMENT_LABEL_ALL,
        ]),
        'pass' => $all,
      ]);
    }

    return $requirements;
  }

  public function generateRandomConditionConfig() {
    $values = $this->getFieldPossibleValues();
    $random = array_rand($values, rand(1, 4));
    if (!is_array($random)) {
      $random = [$random];
    }

    $config = parent::generateRandomConditionConfig();
    $config['target_plugin_configuration'] = array_merge($config['target_plugin_configuration'], [
      'value' => $random,
      'any_or_all' => random_int(0, 3) == 0 ? 'all' : 'any',
    ]);
    return $config;
  }
}
