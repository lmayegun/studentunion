<?php

namespace Drupal\su_students\Plugin\ComplexConditions\Condition;

use DateTime;
use Drupal\complex_conditions\ConditionRequirement;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\UserInterface;

/**
 * Condition.
 *
 * Students who are registerd as a part-time student at UCL
 * or a student who began an undergraduate course after the age of 21,
 * or a postgraduate course after the age of 24
 *
 * @ComplexCondition(
 *   id = "election_mature_age_or_parttime",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("Student is mature age or part-time"),
 *   display_label = @Translation("Student is mature age or part-time"),
 *   category = @Translation("Student data"),
 *   weight = 0,
 * )
 */
class ElectionMatureAgeOrPartTime extends ElectionConditionBase implements ComplexConditionInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
  }

  /**
   * Return true or false if access.
   *
   * @param string $phase
   *
   * @return boolean
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->assertParameters($parameters);
    $requirements = [];

    // // Determine the age of the student when starting the course.
    // $study_start = strtotime($student->study_start);
    // $dob = strtotime($student->dob);
    // $age_at_start = _uclu_student_dob_to_age($dob, $study_start);
    // if ($student->isUndergrad()) {
    // return $age_at_start >= 21;
    // }
    // return $age_at_start >= 24;

    $studentService = \Drupal::service('su_students.student_services');

    $profile = $studentService::getStudentProfile($account);
    if (!$profile) {
      $pass = FALSE;
      return $this->buildRequirements($pass);
    }

    if ($studentService->isParttime($profile)) {
      $pass = TRUE;
      return $this->buildRequirements($pass);
    }

    $dateOfBirth = new DateTime($profile->field_student_date_of_birth->value);
    $studyStart = new DateTime($profile->field_student_study_start->value);
    $ageAtStudyStart = $studyStart->diff($dateOfBirth)->y;
    if ($studentService->isUndergrad($profile)) {
      $pass = $ageAtStudyStart >= 21;
      return $this->buildRequirements($pass);
    } else {
      $pass = $ageAtStudyStart >= 24;
      return $this->buildRequirements($pass);
    }

    $pass = FALSE;
    return $this->buildRequirements($pass);
  }

  public function getMessage($params) {
    $message = 'Registered as a part-time student at UCL or a student who began an undergraduate course after the age of 21, or a postgraduate course after the age of 24.';
    return $message;
  }

  public function getParams() {
    return [];
  }

  public function buildRequirements($pass) {
    $id = 'mature_or_part_time_' . uniqid();
    $params = $this->getParams();
    $message = $this->getMessage($params);
    $label = t($message, $params);
    $requirements[$id] = new ConditionRequirement([
      'id' => $id,
      'label' => $label,
      'pass' => $pass,
    ]);
    return $requirements;
  }
}
