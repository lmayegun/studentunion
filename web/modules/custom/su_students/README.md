CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


# Introduction

Students are users in our website created when:

* They log in through UCL with su_login_oauth2 module
* By importing the students with a CSV document provided by Portico

These students will have: 'student' and 'member' user roles, and a 'student' profile

The main functionality of this module is  importing the students with a CSV document provided by Portico.
In /admin/students you can see two links. One to import from CSV and another import from Webservice
that it is disabled at this moment.

Import from CSV is a link to a form, here we are required to upload the CSV from Portico. 

YOU MUST OPEN AND SAVE THE CSV AS "CSV (UTF-8)" IN EXCEL BEFORE IMPORTING.

Also, we have
different options:

* Anonimyse the user. This will remove from the user account the 'student' role. It also removes the entire
'student' profile. We will only maintain the user account.

* Remove students profile without a user. It checks all profiles which users no longer exists and delete these
profiles.

When we click import it will run a Batch that will process all the rows of the document. It will check if the
user exist by their username, if it does not exist it will create one, otherwise it will update that date.

We also record in the student profile when was last updated.

# Requirements

# Recommended modules

# Installation

# Configuration

# Troubleshooting

# FAQ

# Maintainers
