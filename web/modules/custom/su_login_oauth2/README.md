CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


# Introduction

The Shibboleth software is a web-based single sign-on system made up of the main two components:

* The _Identity Provider (IdP)_ is responsible for user authentication and providing user information to
the Service Provider (SP). In our case UCL.
* The _Service Provider (SP)_ is responsible for protecting an online resource and consuming information
from the Identity Provider (IdP). In our case UCL API.

**With this module and API we do not need to have shibboleth installed and running in our server.**

This module includes three main functionalities:

* A connection to UCL API (SP) to login through UCL (IdP)
* Admin form to save configuration necessary to use the UCL API
* Functionality to create a drupal user if does not exist, and login.

su_login_oauth2 provides the functionality to use UCL API (https://uclapi.com/). The only thing you need
to do is create an account with them. Set up a new API. Copy the API's settings and paste into
in /admin/config/people/su_login_oauth2.

It edits the form created by su_login and implements a submit button that redirects to UCL.

UCL API redirects back to the "callback URL" path set in the settings for the token on the actual uclapi.com website. Currently https://studentsunionucl.org/ucl-login


# Requirements

It uses the module su_login to implement the button 'Login with UCL'.

# Recommended modules

# Installation

# Configuration

# Troubleshooting

# FAQ

# Maintainers
