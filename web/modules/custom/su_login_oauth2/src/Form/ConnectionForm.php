<?php
/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */

namespace Drupal\su_login_oauth2\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConnectionForm extends ConfigFormBase
{

  /**
   * @return array
   */
  protected function getEditableConfigNames()
  {
    return [
      'su_login_oauth2.adminsettings',
    ];
  }

  /**
   * @return string
   */
  public function getFormId()
  {
    return 'su_login_oauth2_admin_form';
  }

  /**
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('su_login_oauth2.adminsettings');

    $dashboard = new FormattableMarkup('<a href="@link" target="_blank">UCL API dashboard</a>', ['@link' => 'https://uclapi.com/dashboard']);

    // Access Token Endpoint or API Token
    $form['api_token'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API token'),
      '#description'   => t('You will get this value from your @dashboard', ['@dashboard' => $dashboard]),
      '#default_value' => $config->get('api_token'),
      '#required'      => TRUE,
    ];

    // Client Id
    $form['client_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client ID'),
      '#description'   => t('You will get this value from your @dashboard', ['@dashboard' => $dashboard]),
      '#default_value' => $config->get('client_id'),
      '#required'      => TRUE,
    ];

    // Client secret
    $form['client_secret'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client secret'),
      '#description'   => t('You will get this value from your @dashboard', ['@dashboard' => $dashboard]),
      '#default_value' => $config->get('client_secret'),
      '#required'      => TRUE,
    ];

    $link = new FormattableMarkup('<a href="@link" target="_blank">UCL API documentation</a>', ['@link' => 'https://uclapi.com/docs/']);

    // Authorize Endpoint
    $form['authorize_endpoint'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Authorize endpoint'),
      '#description'   => t('This is the UCL API base URL. You can see more information on @link', ['@link' => $link]),
      '#default_value' => t('https://uclapi.com/oauth'),
      '#disabled'      => TRUE,
      '#required'      => TRUE,
    ];

    // Button message
    $form['button_message'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Button message'),
      '#description'   => t('Message for the button at the login page to connect with a UCL account'),
      '#default_value' => $config->get('button_message'),
      '#required'      => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    $this->config('su_login_oauth2.adminsettings')
      ->set('api_token', $form_state->getValue('api_token'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('authorize_endpoint', $form_state->getValue('authorize_endpoint'))
      ->set('button_message', $form_state->getValue('button_message'))
      ->save();
  }

}
