<?php

namespace Drupal\su_login_oauth2\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class PageInit implements EventSubscriberInterface {

  public function storeDestination(ResponseEvent $event) {
    if ($event->getRequest()->query->get('destination')) {
      $destination = $event->getRequest()->query->get('destination');
      $loginService = \Drupal::service('su_login.destination_cookie_service');
      if (!headers_sent()) {
        if ($event->getResponse()->headers) {
          $cookie = $loginService->setCookie($destination);
          $event->getResponse()->headers->setCookie($cookie);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('storeDestination');
    return $events;
  }
}
