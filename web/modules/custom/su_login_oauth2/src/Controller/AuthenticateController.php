<?php

namespace Drupal\su_login_oauth2\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;

class AuthenticateController extends ControllerBase {

  /**
   * Login the user from UCL API, create it if it does not exist.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function uclAPIAuthenticateResponse(Request $request) {
    $config = \Drupal::config('su_login_oauth2.adminsettings');

    // $result = $request->query->get('result');
    // $client_id = $request->query->get('client_id');
    // $state = $request->query->get('state');
    $code = $request->query->get('code');

    $token = callAPIGetToken($config, $code);
    if (!empty($token->token)) {
      $ucl_data = callAPIGetUserData($config, $token->token);
      if (!empty($ucl_data)) {
        $user = user_load_by_name($ucl_data->email);
        if (!$user) {
          $user = $this->createUserFromOauth($ucl_data);
        }
        $user = $this->updateUserFromOauth($user, $ucl_data);
        $user = $this->addUserRolesFromOauth($user, $ucl_data);
        $user->save();

        return $this->loginRedirectUserFromOauth($user, $request);
      } else {
        return $this->loginRedirectUserFromOauthError($request);
      }
    } else {
      return $this->loginRedirectUserFromOauthError($request);
    }
  }

  /**
   * Use the cookie to save the destination, use a hook so other modules can edit it
   * Other modules need to keep this cookie otherwise it will be deleted
   *
   * @param $user
   * @param $request
   */
  function loginRedirectUserFromOauthError($request) {
    $loginService = \Drupal::service('su_login.destination_cookie_service');
    $destination = $loginService->getCookie($request);
    if (!$destination) {
      $destination = '/';
    }

    \Drupal::logger('su_login_oauth2')->notice("OAUTH no token given, user redirected to " . $destination);

    // \Drupal::messenger()->addMessage('Apologies, there has been an issue logging you in.', 'error');
    \Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', $destination);
    return new RedirectResponse($destination);
  }

  /**
   * Use the cookie to save the destination, use a hook so other modules can edit it
   * Other modules need to keep this cookie otherwise it will be deleted
   *
   * @param $user
   * @param $request
   */
  function loginRedirectUserFromOauth($user, $request) {
    $user = User::load($user->id());
    if (!empty($user)) {
      $loginService = \Drupal::service('su_login.destination_cookie_service');
      $destination = $loginService->getCookie($request);
      user_login_finalize($user);
      if (!$destination) {
        $destination = '/user';
      }
      \Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', $destination);

      return new RedirectResponse($destination);
    } else {
      return $this->loginRedirectUserFromOauthError($request);
    }
  }

  /**
   * Create user and profile if it does not exist from the UCL API
   *
   * @param $ucl_data
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\user\Entity\User
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function createUserFromOauth($ucl_data) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $user = User::create([
      'uid'                       => '',
      'name'                      => $ucl_data->cn . '@ucl.ac.uk',
      'mail'                      => $ucl_data->email ?? $ucl_data->cn . '@ucl.ac.uk',
      'init'                      => $ucl_data->cn . '@ucl.ac.uk',
      'langcode'                  => $language,
      'preferred_langcode'        => $language,
      'preferred_admin_langcode'  => $language,
      'status'                    => 1,
      'field_ucl_user_id'         => $ucl_data->cn,
      'field_upi'                 => strtoupper($ucl_data->upi),
      'field_ucl_department_name' => $ucl_data->department ?? '',
    ]);
    $user->enforceIsNew();
    $user->save();
    if ($ucl_data->is_student) {
      $studentService = \Drupal::service('su_students.student_services');
      $studentService->createStudentProfile($user);
    }
    return $user;
  }

  /**
   * @param $user
   * @param $ucl_data
   * @return mixed
   */
  function updateUserFromOauth($user, $ucl_data) {
    if (isset($ucl_data->given_name) && $user->get('field_first_name')->value !== $ucl_data->given_name) {
      $user->set('field_first_name', $ucl_data->given_name);
    }
    if (isset($ucl_data->sn) && $user->get('field_last_name')->value !== $ucl_data->sn) {
      $user->set('field_last_name', $ucl_data->sn); //(strpos($ucl_data->full_name, ' ') === FALSE) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $ucl_data->full_name));
    }
    if (isset($ucl_data->full_name) && $user->get('field_full_name')->value !== $ucl_data->full_name) {
      $user->set('field_full_name', $ucl_data->full_name);
    }
    if (isset($ucl_data->upi) && $user->get('field_upi')->value !== strtoupper($ucl_data->upi)) {
      $user->set('field_upi', strtoupper($ucl_data->upi));
    }
    return $user;
  }

  /**
   * @param $user
   * @param $ucl_data
   * @return mixed
   */
  function addUserRolesFromOauth($user, $ucl_data) {
    $roles = $user->getRoles();
    if (!in_array('ucl_authenticated', $roles)) {
      $user->addRole('ucl_authenticated');
    }

    $isStudent = $ucl_data->is_student;
    if ($isStudent) {
      $user->addRole('student');
    } else if (!in_array('member', $roles)) {
      $user->removeRole('student');
    }

    // Staff roles
    $isStaff = in_array('ucl-staff', $ucl_data->ucl_groups) || in_array('all-staff', $ucl_data->ucl_groups);
    if (!in_array('ucl_staff', $roles) && $isStaff) {
      $user->addRole('ucl_staff');
    }
    $isUnionStaff = in_array('uclunion-staff', $ucl_data->ucl_groups);
    if (!in_array('su_staff', $roles) && $isUnionStaff) {
      $user->addRole('su_staff');
    }
    return $user;
  }
}
