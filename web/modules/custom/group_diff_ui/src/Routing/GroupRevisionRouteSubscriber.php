<?php

namespace Drupal\group_diff_ui\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Swaps out the revision UI access callbacks.
 */
class GroupRevisionRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('entity.group.version_history')) {
      $requirements = $route->getRequirements();
      $requirements['_access_group_revision'] = 'view';
      $route->setRequirements($requirements);
      $route->setDefault('_controller', '\Drupal\group_diff_ui\Controller\GroupRevisionController::revisionOverview');
      $route->setOption('_group_operation_route', TRUE);
    }
  }
}
