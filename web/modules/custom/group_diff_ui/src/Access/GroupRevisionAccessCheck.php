<?php

namespace Drupal\group_diff_ui\Access;

use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_diff_ui\Access\EntityRevisionAccessCheckBase;
use Drupal\group\Entity\Group;
use Symfony\Component\Routing\Route;

/**
 * Provides an access checker for Block Content revisions.
 *
 * @ingroup group_access
 */
class GroupRevisionAccessCheck extends EntityRevisionAccessCheckBase implements AccessInterface {

  const GROUP_ENTITY = 'group';

  /**
   * Constructs a new GroupRevisionAccessCheck.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The currently active route match object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    $this->entityStorage = $entity_type_manager->getStorage(self::GROUP_ENTITY);
    $this->entityAccess = $entity_type_manager->getAccessControlHandler(self::GROUP_ENTITY);
    $this->routeMatch = $route_match;
  }

  /**
   * Checks routing access for the Group revision.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param int $group_revision
   *   (optional) The group revision ID. If not specified, but $group is, access
   *   is checked for that object's revision.
   * @param EditorialContentEntityBase $group
   *   (optional) A group object. Used for checking access to a group's default
   *   revision when $group_revision is unspecified. Ignored when $group_revision
   *   is specified. If neither $group_revision nor $group are specified, then
   *   access is denied.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, AccountInterface $account, int $group_revision = NULL, EditorialContentEntityBase $group = NULL) {
    $operation = $route->getRequirement('_access_group_revision');
    $map = [
      'view' => 'view all group revisions',
      'update' => 'revert all group revisions',
      'delete' => 'delete all group revisions',
    ];

    if (!$group) {
      if (empty($route_match)) {
        $route_match = $this->routeMatch;
      }
      $group = Group::load($route_match->getParameter('group'));
    }

    return parent::getAccessResult($route, $account, $group_revision, $group, $map, $operation);
  }
}
