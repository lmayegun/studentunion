(function ($, Drupal) {
  Drupal.behaviors.manageEventFields = {
    attach: function (context, settings) {
      'use strict';

      function getNumberOfVariations() {
        return $('table[id^="variations-values"] tbody tr').length;
      }

      function updateStylingAndInfo() {
        // the DOM IDs change each ajax call, so we need to update them again.
        $('[id^="variations-values"] thead h4.label').html('Ticket type(s)');
        $('[id^="edit-variations--"][id$="description"]').hide();
        $('[id^="edit-variations-add-more"]').val('Add additional ticket type');
      }

      function resetFormFields() {
        let elementsToHide = [];
        let elementsToShow = [];
        let elementsToCheck = [];
        const elementsToUncheck = [];

        function addElement(show, element) {
          if (show) {
            elementsToShow.push(element);
          }
          else {
            elementsToHide.push(element);
          }
        }

        var variations = getNumberOfVariations();

        // Info about the product (overall event)
        var groupField = $('[name="field_event_owner_group[0][target_id]"]');
        var groupAssigned = groupField && groupField.val() && groupField.val() !== '_none';

        var ticketingType = $('#edit-field-ticketing-type').val();
        console.log('ticketingType', ticketingType);

        // We default form elements to paid tickets.
        // Then we hide and show things if there are differences.
        var noTickets = (ticketingType === 'none');
        var freeEvent = (ticketingType === 'free');
        var externalEvent = (ticketingType === 'external');
        var clubSocTicketRequest = (ticketingType === 'clubsoc');

        var rules = [];

        // Define some rules as to what gets hidden and shown.
        rules.push({
          name: 'External event, hide anything to do with internal finance stock and price',
          evaluate: externalEvent || noTickets,
          productFieldsToHide: ['edit-group-finance', 'edit-group-stock-status-indicator', 'edit-field-event-ticketholder-info-wrapper', 'edit-field-event-administrators-wrapper'],
          variationFieldsToHide: ['field-stock', 'is-ticket', 'group-ticketholder-options', 'product-restrictions-form', 'maximum-order-quantity', 'commerce-stock-always-in-stock'],
          hidePrice: true
        });

        rules.push({
          name: 'Hide "is ticket" for all things',
          evaluate: true,
          variationFieldsToHide: ['is-ticket'],
        });

        rules.push({
          name: 'Internal event, hide external links',
          evaluate: !externalEvent || noTickets,
          productFieldsToHide: ['edit-field-external-link-wrapper'],
          variationFieldsToHide: ['field-event-external-link', 'field-external-link']
        });

        rules.push({
          name: 'Free event - hide price and finance',
          evaluate: freeEvent || noTickets,
          productFieldsToHide: ['edit-group-finance', 'field-tax-category', 'field-finance-department', 'field-finance-cost-centre', 'field-finance-gl-code'],
          hidePrice: true
        });

        rules.push({
          name: 'ClubSoc ticket request - hide all ticketing',
          evaluate: clubSocTicketRequest || noTickets,
          productFieldsToHide: ['edit-group-stock-status-indicator', 'edit-field-event-ticketholder-info-wrapper', 'edit-field-event-administrators-wrapper', 'field-event-free', 'field-external-tickets'],
          variationFieldsToHide: ['field-stock', 'is-ticket', 'commerce-stock-always-in-stock', 'field-event-external-link', 'field-external-link'],
          hidePrice: true,
          setNotTicket: true,
        });

        rules.push({
          name: 'Hide three finance fields if group assigned',
          evaluate: groupAssigned,
          productFieldsToHide: ['field-finance-department', 'field-finance-cost-centre', 'field-finance-gl-code']
        });

        // rules.push({
        //   name: 'Show some fields if ticketed',
        //   evaluate: !noTickets,
        //   variationFieldsToHide: ['field-stock',
        // 'group-ticketholder-options', 'product-restrictions-form',
        // 'maximum-order-quantity', 'commerce-stock-always-in-stock'] });

        rules.push({
          name: 'Hide variations completely if not ticketed',
          evaluate: noTickets,
          productFieldsToHide: ['edit-variations']
        });

        rules.push({
          name: 'Hide stock if always in it',
          hideIfAlwaysInStock: ['field-stock']
        });

        // Show everything by default
        rules.forEach(function (rule) {
          if (rule.productFieldsToHide) {
            rule.productFieldsToHide.forEach(element => {
              addElement(true, '[data-drupal-selector="' + element + '"]');
              addElement(true, '[data-drupal-selector="edit-' + element + '-wrapper"]');
            });
          }
        });

        // Process the rules
        rules.forEach(function (rule) {
          if (rule.productFieldsToHide) {
            rule.productFieldsToHide.forEach(element => {
              addElement(!rule.evaluate, '[data-drupal-selector="' + element + '"]');
              addElement(!rule.evaluate, '[data-drupal-selector="edit-' + element + '-wrapper"]');
            });
          }
        });

        // Variation fields
        for (let i = 0; i < variations; i++) {
          // Price
          var priceParent = '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-price-0"]';
          var priceElement = '#edit-variations-' + i + '-inline-entity-form-price-0-number';
          $(priceParent).show();

          rules.forEach(function (rule) {
            if (rule.evaluate && rule.hidePrice) {
              $(priceParent).hide();
            }

            if (rule.evaluate && rule.setNotTicket) {
              $('#edit-variations-' + i + '-inline-entity-form-is-ticket-value').prop("checked", false);
              console.log($('#edit-variations-' + i + '-inline-entity-form-is-ticket-value'));
            }

            if (rule.variationFieldsToShow) {
              rule.variationFieldsToShow.forEach(element => {
                addElement(rule.evaluate, '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-' + element + '-0"]');
                addElement(rule.evaluate, '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-' + element + '-wrapper"]');
                addElement(rule.evaluate, '[data-drupal-selector="edit-variations-widget-' + i + '-inline-entity-form-' + element + '"]');
              });
            }

            if (rule.variationFieldsToHide) {
              rule.variationFieldsToHide.forEach(element => {
                addElement(!rule.evaluate, '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-' + element + '-0"]');
                addElement(!rule.evaluate, '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-' + element + '-wrapper"]');
                addElement(!rule.evaluate, '[data-drupal-selector="edit-variations-widget-' + i + '-inline-entity-form-' + element + '"]');
              });
            }

            var alwaysInStock = $('[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-commerce-stock-always-in-stock-value"').is(':checked');
            if (rule.hideIfAlwaysInStock) {
              rule.hideIfAlwaysInStock.forEach(element => {
                addElement(!alwaysInStock, '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-' + element + '-0"]');
                addElement(!alwaysInStock, '[data-drupal-selector="edit-variations-' + i + '-inline-entity-form-' + element + '-wrapper"]');
                addElement(!alwaysInStock, '[data-drupal-selector="edit-variations-widget-' + i + '-inline-entity-form-' + element + '"]');
              });
            }
          });
        }


        // Do the actual thing

        // console.log('elementsToShow', elementsToShow);
        elementsToShow.forEach(element => {
          $(element).show();
        })

        // console.log('elementsToHide', elementsToHide);
        elementsToHide.forEach(element => {
          $(element).hide();
        });

        // console.log('elementsToCheck', elementsToCheck);
        elementsToCheck.forEach(element => {
          $(element).prop('checked', true);
        });

        elementsToUncheck.forEach(element => {
          $(element).prop('checked', false);
        });
      }

      // If any checkbox is changed
      $('input[type="checkbox"]').on('click', function () {
        resetFormFields();
      });

      // If a group is selected
      $('[name^="field_event_owner_group"]').on('change', function () {
        resetFormFields();
      });

      // If ticketing type changed:
      $('[name^="field_ticketing_type"]').on('change', function () {
        resetFormFields();
      });

      $(document).ready(function () {
        resetFormFields();
        updateStylingAndInfo();

        $(window.document).ajaxComplete(function (event, xmlHttpRequest, ajaxOptions) {
          updateStylingAndInfo();
        });
      });

    }

  }
}(jQuery, Drupal));
