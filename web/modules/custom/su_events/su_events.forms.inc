<?php

// Functions modifying forms for products for events

use Drupal\block\Entity\Block;
use Drupal\commerce_cart\Form\AddToCartFormInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;

/**
 * Modify forms for events products and variations.
 *
 * @param mixed $form
 * @param FormStateInterface $form_state
 * @param mixed $form_id
 *
 * @return [type]
 */
function su_events_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Add code to manage hiding and showing event fields.
  $productForm = in_array($form_id, [
    'commerce_product_event_edit_form',
    'commerce_product_event_add_form',
  ]);
  $productVariationForm = in_array($form_id, [
    'commerce_product_variation_event_add_form',
    'commerce_product_variation_event_edit_form',
  ]);
  if ($productForm || $productVariationForm) {
    $form['#attached']['library'][] = 'su_events/forms';

    if ($productForm) {
      $form['#validate'][] = 'su_events_product_form_validate';
      array_unshift($form['actions']['submit']['#submit'], 'su_events_product_form_submit');
      $form['variations']['widget'][0]['#field_title'] = new TranslatableMarkup('Date and ticket variations');

      $form['stores']['widget']['#default_value'] = [1];
    }
    if ($productVariationForm) {
      $form['#validate'][] = 'su_events_product_variation_form_validate';
      array_unshift($form['actions']['submit']['#submit'], 'su_events_product_variation_form_submit');
      su_events_form_alter_variation($form, $form_state);
    }

    unset($form['actions']['submit_continue']);

    $clubSocEditing = su_events_editing_as_club_society_officer($form_state->getformObject()
      ->getEntity());
    su_events_form_alter_for_club_society_editing($form, $form_state, $productForm ? 'commerce_product' : 'commerce_product_variation', $clubSocEditing);
  }

  // Modify cart for out of stock events
  if ($form_state->getBuildInfo()['callback_object'] instanceof AddToCartFormInterface) {
    // $form['submit'] = array('#access' => FALSE);
  }
}

/**
 * @param Block $block
 * @param mixed $operation
 * @param AccountInterface $account
 *
 * @return [type]
 */
function su_events_block_access(Block $block, $operation, AccountInterface $account) {
  if ($operation == 'view') {
    if ($block->id() == 'entityviewproduct_2') {
      $product = Drupal::service('su_drupal_helper')->getRouteEntity();
      if ($product && $product->bundle() == 'event') {
        return AccessResult::forbiddenIf(TRUE); //->addCacheableDependency($block);
      }
    }
  }
  return AccessResult::neutral();
}


/**
 * Alter the validation to allow for what su_events.
 * The su_events.forms.js is doing and showing fields conditionally.
 *
 * @param mixed $form
 * @param FormStateInterface $form_state
 *
 * @return [type]
 */
function su_events_product_form_validate(&$form, FormStateInterface $form_state) {
  $form_errors = $form_state->getErrors();

  $form_state->clearErrors();

  $variationsCount = count($form_state->getValue('variations'));

  $product = $form_state->getFormObject()->getEntity();

  $ticketingType = $form_state->getValue('field_ticketing_type')[0]['value'];

  $noTickets = $ticketingType == 'none';
  $free = $ticketingType == 'free';
  $external = $ticketingType == 'external';
  $clubSocTicketRequest = $ticketingType == 'clubsoc' && su_events_editing_as_club_society_officer($product);

  // PRICE IF FREE:
  if ($free || $external || $noTickets || $clubSocTicketRequest) {
    $i = 0;
    while ($i <= $variationsCount) {
      unset($form_errors['variations][' . $i . '][inline_entity_form][price']);
      unset($form_errors['variations][' . $i . '][inline_entity_form][price][0']);
      unset($form_errors['variations][' . $i . '][inline_entity_form][price][0][number']);
      $i++;
    }
  }

  // FINANCE INFO.
  // Form field => group field.
  $fields = [
    'field_finance_department' => 'field_finance_department_code',
    'field_finance_cost_centre' => 'field_finance_cost_code',
    'field_finance_gl_code' => 'field_finance_general_ledger_cod',
    'field_tax_category' => '',
  ];
  $groupSet = isset($form_state->getValue('field_event_owner_group')[0]['target_id']);
  foreach ($fields as $formField => $groupField) {
    if ($groupSet || $free || $noTickets || $external) {
      // Remove the field_mobile form error.
      unset($form_errors[$formField]);
    }
  }

  // Ticket:
  unset($form_errors['is_ticket']);

  // Price for new variatioons
  if (isset($form_errors['actions][submit']) && $form_errors['actions][submit']->__toString() == 'Price field is required.') {
    unset($form_errors['actions][submit']);
  }

  // Now loop through and re-apply the remaining form error messages.
  foreach ($form_errors as $name => $error_message) {
    $form_state->setErrorByName($name, $error_message);
  }
}

function su_events_product_form_submit(&$form, FormStateInterface $form_state) {
  $ticketingType = $form_state->getValue('field_ticketing_type')[0]['value'];

  // PRICE
  if ($ticketingType === 'free' || $ticketingType === 'external') {
    $variations = $form_state->getValue('variations');
    $variationsCount = count($variations);
    $i = 0;
    while ($i < $variationsCount) {
      if (isset($variations[$i])) {
        $variations[$i]['inline_entity_form']['price'][0]['number'] = 0.00;
      }
      $i++;
    }
    $form_state->setValue('variations', $variations);
  }

  // FINANCE INFO
  $fields = [ // Form field => group field
    'field_finance_department' => 'field_finance_department_code',
    'field_finance_cost_centre' => 'field_finance_cost_code',
    // 'field_finance_gl_code'     => 'field_finance_general_ledger_cod'
  ];
  if (isset($form_state->getValue('field_event_owner_group')[0]['target_id'])) {
    $group = Group::load($form_state->getValue('field_event_owner_group')[0]['target_id']);
    if ($group) {
      foreach ($fields as $formField => $groupField) {
        if ($group->hasField($groupField) && $group->get($groupField)->entity) {
          $termID = $group->get($groupField)->entity->id();
          $form_state->setValue($formField, ['target_id' => $termID]);
        }
      }
      if ($group->bundle() == 'club_society') {
        $id = su_drupal_helper_get_taxonomy_term_by_name('69301', 'finance_gl_code');
        $form_state->setValue('field_finance_gl_code', ['target_id' => $id]);
      }
    }
  }
}

/**
 * Set date of variation to product's date if relevant.
 */
function su_events_set_variation_date_to_product(ProductVariationInterface &$entity, ProductInterface $product = NULL, $save = FALSE) {
  if (!$entity) {
    return;
  }

  if (!$entity->hasField('field_date_range_override')) {
    return;
  }

  if ($entity->field_date_range_override->value) {
    return;
  }

  if (!$product) {
    $product = $entity->getProduct();
  }

  if (!$product) {
    return;
  }

  if (!$product->hasField('field_date_range')) {
    return;
  }

  $eventDaterange = $product->get('field_date_range')->getValue();
  $entity->field_date_range = $eventDaterange;
  if ($save) {
    $entity->save();
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert()
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function su_events_commerce_product_insert(ProductInterface $entity) {
  su_events_update_variations_for_product($entity);
}

/**
 * Implements hook_ENTITY_TYPE_update()
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function su_events_commerce_product_update(ProductInterface $entity) {
  su_events_update_variations_for_product($entity);
}

function su_events_update_variations_for_product(ProductInterface $entity) {
  if ($entity->bundle() == 'event') {
    // Set the dates for any variations from the event date.
    // This is a design compromise, allowing us to have different dates per variation
    // (and use variations for that purpose)
    // While making it easy for the user to create events where all variations share the same datetime
    // Because that's 90% of events...

    if (!$entity->hasField('field_date_range')) {
      return;
    }
    $variations = $entity->getVariations();
    if (!$variations) {
      return;
    }

    foreach ($variations as $variation) {
      // Set price to 0 for some cases:
      $ticketingType = $entity->field_ticketing_type->value;
      $meeting = $entity->bundle() == 'meeting';
      $ticketingTypeZeroPrice = in_array($ticketingType, [
        'free',
        'external',
      ]);
      if ($meeting || $ticketingTypeZeroPrice) {
        $variation->setPrice(new Price(0.00, 'GBP'));
        // Can't do this as it would blanken valid tickets: || su_events_editing_as_club_society_officer($event)) {
      }

      // Only make it a ticket if it's of the right type to be:
      $variation->is_ticket->value = in_array($ticketingType, [
        'paid',
        'free',
        'clubsoc',
      ]);

      su_events_set_variation_date_to_product($variation, $entity, TRUE);
    }
  }
}

function su_events_commerce_product_variation_presave(ProductVariation $entity) {
  if ($entity->bundle() == 'event') {
    su_events_set_variation_date_to_product($entity, NULL, FALSE);
  }
}


function su_events_inline_entity_form_entity_form_alter(array &$entity_form, FormStateInterface &$form_state) {
  if ($entity_form['#entity_type'] == 'commerce_product_variation' && $entity_form['#bundle'] == 'event') {

    su_events_form_alter_variation($entity_form, $form_state);

    $clubSocEditing = su_events_editing_as_club_society_officer($form_state->getformObject()
      ->getEntity());
    su_events_form_alter_for_club_society_editing($entity_form, $form_state, 'commerce_product_variation', $clubSocEditing);

    array_unshift($entity_form['#ief_element_submit'], 'su_events_inline_entity_form_entity_form_submit_alter_event');
  }
}

/**
 *
 * @param array $reference_form
 *   The reference entity form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state of the parent form.
 *
 * @see inline_entity_form_reference_form_submit()
 */
function su_events_inline_entity_form_entity_form_submit_alter_event(array $reference_form, FormStateInterface $form_state) {
}

function su_events_product_variation_form_validate(&$form, FormStateInterface $form_state) {
  $form_errors = $form_state->getErrors();

  $form_state->clearErrors();

  // FINANCE INFO
  unset($form_errors['is_ticket']);

  // Now loop through and re-apply the remaining form error messages.
  foreach ($form_errors as $name => $error_message) {
    $form_state->setErrorByName($name, $error_message);
  }
}

function su_events_product_variation_form_submit(&$form, FormStateInterface $form_state) {
}

function su_events_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter') {
    $group = $implementations['su_events'];
    unset($implementations['su_events']);
    $implementations['su_events'] = $group;
  }
}

// function su_events_form_alter_add_to_cart(&$form, FormStateInterface $form_state)
// {
//     $variation = su_events_get_variation($form, $form_state);
//     $product = $variation->getProduct();
// }

function su_events_go_to_external_link(array $form, FormStateInterface $form_state) {
  $variation = su_events_get_variation($form, $form_state);
  $product = $variation->getProduct();

  $link = NULL;
  if ($link = $variation->field_event_external_link->uri || $link = $product->field_external_link->uri) {
    // Redirect to confirmation page
    Drupal::service('request_stack')
      ->getCurrentRequest()->query->set('destination', $link);
  }
}

function su_events_get_variation($form, FormStateInterface $form_state) {
  $form_data = $form_state->getStorage();

  if (!empty($form_data['selected_variation'])) {
    return ProductVariation::load($form_data['selected_variation']);
  }
  else {
    return $form_state->getFormObject()->getEntity()->getPurchasedEntity();
  }
  return NULL;
}

function su_events_editing_as_club_society_officer($product) {
  if (!$product) {
    return FALSE;
  }
  if ($product instanceof ProductVariation) {
    $product = $product->getProduct();
  }
  $session_key = 'su_events_commerce_product_access' . $product->id();
  $officer = isset($_SESSION[$session_key]) && $_SESSION[$session_key] == 'create events for club or society';
  if ($officer) {
    $account = User::load(Drupal::currentUser()->id());
    if ($account->hasPermission('update any event commerce_product')) {
      return FALSE;
    }
    return TRUE;
  }
  return FALSE;
}

function su_events_form_alter_variation(&$form, FormStateInterface &$form_state) {
  $form['is_ticket']['widget']['value']['#title'] = new TranslatableMarkup('This ticket type is bookable');
  $form['is_ticket']['widget']['value']["#required"] = FALSE;

  $form['commerce_stock_always_in_stock']['widget']['value']['#title'] = new TranslatableMarkup('Unlimited tickets available?');
}

function su_events_form_alter_for_club_society_editing(&$form, FormStateInterface &$form_state, $entity_type, $editingAsClubSoc) {
  /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
  $clubSocService = Drupal::service('su_clubs_societies.service');

  if ($editingAsClubSoc) {
    // Limit group selection to the user's C&S
    // Currently doesn't work as it's set as an autocomplete entity ref
    if (isset($form['field_event_owner_group'])) {
      $form['field_event_owner_group']['widget']['#required'] = TRUE;
      $allowedGroups = $clubSocService->userHasGroupPermission(User::load(Drupal::currentUser()
        ->id()), NULL, 'create events for club or society', TRUE);
      $options = ['_none' => '- None -'];
      foreach ($allowedGroups as $group) {
        $options[$group->id()] = $group->label();
      }
      $form['field_event_owner_group']['widget']['#options'] = $options;

      if ($group) {
        $form['field_event_owner_group']['widget']['#default_value'] = $group->id();
      }
    }

    // For variations, disable changing stock and price.
    // But keep the values, they could have been set by someone with permissions.
    if ($entity_type == 'commerce_product_variation' && isset($form['price'])) {
      $variation = $form['#default_value'] ?? NULL;
      if (!$variation || (!$variation->getPrice()) || ($variation->getPrice() && $variation->getPrice()
            ->getNumber() == 0.00)) {
        $form['price']['widget'][0]['#default_value'] = [
          'number' => 0.00,
          'currency_code' => 'GBP',
        ];
      }
      elseif ($variation->getPrice()->getNumber() > 0) {
        $form['field_stock']['widget'][0]['#disabled'] = TRUE;
        $form['commerce_stock_always_in_stock']['widget']['#disabled'] = TRUE;
        $form['is_ticket']['widget']['#disabled'] = TRUE;
      }
      $form['price']['widget'][0]['#disabled'] = TRUE;
    }
  }
  else {
    $account = User::load(Drupal::currentUser()->id());
    if ($form_state->get('field_ticketing_type') != 'clubsoc' && !$account->hasPermission('manage club society fields on events')) {
      unset($form['field_ticketing_type']['widget']['#options']['clubsoc']);
    }
  }
}
