<?php

namespace Drupal\su_events\EventSubscriber;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderItemEvent;
use Drupal\commerce_refund_order_item\Event\OrderRefundedEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cancel tickets.
 */
class OrderRefundedSubscriber implements EventSubscriberInterface {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.refund.order_item' => ['onOrderRefund', 100],

      'commerce_order.commerce_order.predelete' => ['onOrderCancelOrDelete', 100],
      'commerce_order.cancel.post_transition'   => ['onOrderCancelOrDeleteWorkflow', 100],

      'commerce_order.commerce_order_item.predelete' => ['onOrderItemDelete', 100]
    ];
    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param OrderRefundedEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderRefund(OrderRefundedEvent $event) {
    \Drupal::logger('su_events')->debug('onOrderRefund');
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getOrder();
    $itemsWithQuantity = $event->getItemsWithQuantity();
    $orderRecords = [];
    foreach ($itemsWithQuantity as $orderItemId => $quantityRefunded) {
      if ($quantityRefunded == 0) {
        continue;
      }

      $orderItem = OrderItem::load($orderItemId);
      $this->cancelTicketForOrderItem($orderItem, $quantityRefunded);
    }
  }

  public function onOrderCancelOrDelete(OrderEvent $event) {
    \Drupal::logger('su_events')->debug('onOrderCancelOrDelete');
    $this->cancelTicketForOrder($event->getOrder());
  }

  public function onOrderCancelOrDeleteWorkflow(WorkflowTransitionEvent $event) {
    \Drupal::logger('su_events')->debug('onOrderCancelOrDeleteWorkflow');
    $this->cancelTicketForOrder($event->getEntity());
  }

  /**
   * @param OrderItemEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderItemDelete(OrderItemEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $orderItem = $event->getOrderItem();
    $this->cancelTicketForOrderItem($orderItem);
  }

  function cancelTicketForOrder($order, $quantityRefunded = NULL) {
    \Drupal::logger('su_events')->debug('cancelTicketForOrder');
    // Cancel all related tickets
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_ticket');
    $ticket_ids = $storage->getQuery()
      ->condition('order_id', $order->id())
      ->sort('id')
      ->execute();

    $tickets = $storage->loadMultiple($ticket_ids);
    $cancelled = 0;
    foreach ($tickets as $ticket) {
      // Only cancel as many ticket(s) as were refunded:
      if ($quantityRefunded && $cancelled >= $quantityRefunded) {
        break;
      }
      $this->cancelTicket($ticket);
      $cancelled++;
    }
  }


  public function cancelTicketForOrderItem($orderItem, $quantityRefunded = NULL) {
    $storage = $this->entityTypeManager->getStorage('commerce_ticket');
    // Cancel all related tickets:
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_ticket');
    $ticket_ids = $storage->getQuery()
      ->condition('order_item_id', $orderItem->id())
      ->sort('id')
      ->execute();

    $tickets = $storage->loadMultiple($ticket_ids);
    $cancelled = 0;
    foreach ($tickets as $ticket) {
      // Only cancel as many ticket(s) as were refunded:
      if ($quantityRefunded && $cancelled >= $quantityRefunded) {
        break;
      }
      $this->cancelTicket($ticket);
      $cancelled++;
    }
  }

  public function cancelTicket($ticket) {
    \Drupal::logger('su_events')->debug('cancelTicket');
    $ticket_state = $ticket->getState();
    $ticket_state_transitions = $ticket_state->getTransitions();
    if (!empty($ticket_state_transitions['cancel'])) {
      $ticket_state->applyTransition($ticket_state_transitions['cancel']);
      $ticket->save();
    }
  }
}
