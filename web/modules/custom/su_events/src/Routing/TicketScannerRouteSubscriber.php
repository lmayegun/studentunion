<?php

namespace Drupal\su_events\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class TicketScannerRouteSubscriber extends RouteSubscriberBase
{

    /**
     * {@inheritdoc}
     */
    protected function alterRoutes(RouteCollection $collection)
    {
        $routes = [
            'commerce_ticketing_scanner.scanner_page',
            'commerce_ticketing_scanner.check',
            'commerce_ticketing_scanner.setStatus'
        ];

        foreach ($routes as $route) {
            $route = $collection->get($route);
            if ($route) {
                $route->setRequirements([
                    '_event_sales_access' => 'TRUE',
                ]);
            }
        }
    }
}
