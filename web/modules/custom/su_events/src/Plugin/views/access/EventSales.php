<?php


namespace Drupal\su_events\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\su_events\Controller\EventSales as ControllerEventSales;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Class ViewsCustomAccess
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *     id = "su_events_sales_access",
 *     title = @Translation("Students' Union UCL - access sales and out of stock registrations"),
 *     help = @Translation("View must have commerce_product parameter. Either the right roles on the site or in a group, or being added to the product sales list."),
 * )
 */
class EventSales extends AccessPluginBase implements CacheableDependencyInterface
{

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The product
   *
   * @var \Drupal\commerce\Entity\ProductInterface
   */
  protected $product;

  /**
   * The group context from the route.
   *
   * @var \Drupal\Core\Plugin\Context\ContextInterface
   */
  protected $context;

  /**
   * Constructs a Permission object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Plugin\Context\ContextProviderInterface $context_provider
   *   The group route context.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler, ContextProviderInterface $context_provider)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;

    $contexts = $context_provider->getRuntimeContexts(['commerce_product']);
    $this->context = $contexts['commerce_product'];
    if ($this->context) {
      $this->product = $this->context->getContextValue();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('commerce_product.product_route_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle()
  {
    return '';
  }


  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account)
  {
    return ControllerEventSales::access($account, $this->product ?? null);
  }


  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route)
  {
    $route->setRequirement('_custom_access', '\Drupal\su_events\Controller\EventSales::access');

    // Upcast any %commerce_product path key the user may have configured so the
    // '_custom_access' access check will receive a properly loaded product.
    $route->setOption('parameters', ['commerce_product' => ['type' => 'entity:commerce_product']]);
  }


  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge()
  {
    return Cache::mergeMaxAges(Cache::PERMANENT, $this->context ? $this->context->getCacheMaxAge() : Cache::PERMANENT);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts()
  {
    return Cache::mergeContexts([], $this->context ? $this->context->getCacheContexts() : []);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags()
  {
    if ($this->context) {
      return $this->context->getCacheTags();
    }
  }
}
