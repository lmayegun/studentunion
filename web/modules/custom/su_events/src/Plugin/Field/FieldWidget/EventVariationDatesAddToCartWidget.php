<?php

namespace Drupal\su_events\Plugin\Field\FieldWidget;

use Drupal\commerce_product\Plugin\Field\FieldWidget\ProductVariationTitleWidget;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\smart_date\Entity\SmartDateFormat;
use Drupal\smart_date\SmartDateTrait;

/**
 * Plugin implementation of the 'commerce_product_variation_event_date' widget.
 *
 * @FieldWidget(
 *   id = "commerce_product_variation_event_date",
 *   label = @Translation("Product variation event dates"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EventVariationDatesAddToCartWidget extends ProductVariationTitleWidget {

  use SmartDateTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = $form_state->get('product');
    $variations = $this->loadEnabledVariations($product);
    $options = [];

    // Dates in use allows us to hide the dates if all variations share the same date:
    $datesInUse = [];
    foreach ($variations as $option) {
      $context = commerce_stock_enforcement_get_context($option);
      $stock = commerce_stock_enforcement_get_stock_level($option, $context);

      $prefix = '';
      if ($option->get('field_event_variation_title') && $option->get('field_event_variation_title')->value != '') {
        $prefix = $option->get('field_event_variation_title')->value;

        if ($stock <= 0) {
          $prefix = '[SOLD OUT] ' . $prefix;
        }
      }

      $start_timestamp = $option->get('field_date_range')->value;
      $end_timestamp = $option->get('field_date_range')->end_value;

      $format = SmartDateFormat::load($prefix != '' ? 'compact' : 'default');
      $formatted = static::formatSmartDate($start_timestamp, $end_timestamp, $format->getOptions(), NULL, 'string');
      $datesInUse[] = $formatted;
      $options[$option->id()] = [
        'prefix' => $prefix,
        'formatted' => $formatted,
        'stock' => $stock,
      ];
    }
    $datesInUse = array_unique($datesInUse);
    foreach ($options as $variationId => $data) {
      $prefix = $data['prefix'];
      $formatted = $data['formatted'];
      $stock = $data['stock'];

      $date = ($prefix == '' || count($datesInUse) > 1 ? $formatted : '');
      $prefix = ($prefix ? $prefix . ($date != '' ? ': ' : '') : '');
      $element['variation']['#options'][$variationId] = $prefix . $date;
    }

    return $element;
  }
}
