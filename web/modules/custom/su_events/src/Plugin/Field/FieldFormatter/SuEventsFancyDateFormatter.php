<?php

namespace Drupal\su_events\Plugin\Field\FieldFormatter;

use DateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\smart_date\Plugin\Field\FieldFormatter\SmartDateDefaultFormatter;
use Drupal\smart_date\SmartDateTrait;

/**
 * Plugin implementation of the 'Default' formatter for 'smartdate' fields.
 *
 * This formatter renders the time range using <time> elements, with
 * configurable date formats (from the list of configured formats) and a
 * separator.
 *
 * @FieldFormatter(
 *   id = "su_events_fancy_date",
 *   label = @Translation("Students' Union UCL events fancy date"),
 *   field_types = {
 *     "smartdate",
 *     "daterange",
 *     "datetime",
 *     "timestamp",
 *     "published_at"
 *   }
 * )
 */
class SuEventsFancyDateFormatter extends SmartDateDefaultFormatter {

  use SmartDateTrait {
    viewElements as protected traitViewElements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $original_elements = $this->traitViewElements($items, $langcode);

    $elements = [];

    if (!$original_elements || !isset($original_elements[0])) {
      return parent::viewElements($items, $langcode);
    }

    $start = DateTime::createFromFormat("U", $original_elements[0]['#value']);
    $end = DateTime::createFromFormat("U", $original_elements[0]['#end_value']);
    // $interval = $start->diff($end);

    $sameDay = (!$end || $start->format('Y-m-d') == $end->format('Y-m-d'));
    //    $allDay = $original_elements[0]['#start']['#text']['time']['#markup'] == 'all day';
    $to = '<span class="to">to</span>';

    if ($sameDay) {
      // 1 date, then time underneath
      $elements['date'] = $this->generateDateElement('start', $original_elements, FALSE);
      $elements['separator']['#markup'] = '<br>';
      $startTime = '<span class="time">' . $original_elements[0]['start']['#text']['time']['#markup'] . '</span>';
      if (isset($original_elements[0]['end'])) {
        $endTime = '<span class="time">' . $original_elements[0]['end']['#text']['time']['#markup'] . '</span>';
        $elements['times'] = [
          '#theme' => 'time',
          '#text' => ['#markup' => $startTime . ' ' . $to . ' ' . $endTime],
        ];
      }
      else {
        $elements['times'] = [
          '#theme' => 'time',
          '#text' => ['#markup' => $startTime],
        ];
      }
    }
    else {
      // date with time underneath
      $elements['date'] = $this->generateDateElement('start', $original_elements);
      $end = $this->generateDateElement('end', $original_elements);
      if ($end) {
        // to
        $elements['separator']['#markup'] = '<br>' . $to . '</br>';
        // date with time underneath
        $elements['date_end'] = $end;
      }
    }

    $addToCal = $start && $end;
    if ($addToCal) {
      $variation = $items[0]->getEntity();
      $product = $variation->getProduct();

      $elements['add_to_cal'] = [
        '#theme' => 'add_to_cal',
        '#start' => $start,
        '#end' => $end,
        '#allday' => FALSE,
        '#url' => "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'],
        '#title' => $product->getTitle(),
        '#description' => '',
        '#location' => $product->field_event_venue->entity ? $product->field_event_venue->entity->getName() : '',
      ];
    }

    return [$elements];
  }

  public function generateDateElement($type, $original_elements, $includeTime = TRUE) {
    if (!isset($original_elements[0][$type])) {
      return [];
    }
    $text = [
      'date' => ['#markup' => '<span class="date">' . $original_elements[0][$type]['#text']['date']['#markup'] . '</span>'],
    ];

    if ($includeTime && isset($original_elements[0][$type]['#text']) && isset($original_elements[0][$type]['#text']['time'])) {
      $text['join'] = ['#markup' => '<br>'];
      $text['time'] = ['#markup' => '<span class="time">' . $original_elements[0][$type]['#text']['time']['#markup'] . '</span>'];
    }

    return [
      '#theme' => 'time',
      '#attributes' => $original_elements[0][$type]['#attributes'],
      '#text' => $text,
    ];
  }

}
