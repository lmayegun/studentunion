<?php

namespace Drupal\su_events\Plugin\Block;

use Drupal;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/** *
 * @Block(
 *   id = "event_add_to_cart_block",
 *   admin_label = @Translation("Event add to cart block"),
 *   category = @Translation("Events"),
 * )
 */
class EventCartBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_name = Drupal::routeMatch()->getRouteName();
    if ($route_name !== 'entity.commerce_product.canonical') {
      return [];
    }

    $product = $this->getProduct();

    if ($product->bundle() !== 'event') {
      return [];
    }

    $productVariation = $this->getVariation($product);

    if (!$productVariation) {
      return [];
    }

    if ($productVariation->bundle() !== 'event') {
      return [];
    }

    // External events.
    if ($this->eventIsExternal($productVariation)) {
      if ($this->eventIsPast($productVariation)) {
        return $this->buildPastEvent($productVariation);
      }
      elseif ($this->eventIsCancelled($productVariation)) {
        return $this->buildCancelledEvent($productVariation);
      }
      else {
        return $this->buildExternalEvent($productVariation);
      }
    }
    elseif ($this->eventIsBookable($productVariation)) {
      if ($this->userNotLoggedIn($productVariation)) {
        // MUST check logged in before any others that build cart.
        // @todo restrict all event products to logged in to make this unecessary.
        return $this->buildUserNotLoggedIn($productVariation);
      }
      elseif ($this->eventIsSoldOut($productVariation)) {
        return $this->buildCart($productVariation);
      }
      elseif ($this->eventIsPast($productVariation)) {
        return $this->buildPastEvent($productVariation);
      }
      elseif ($this->eventIsCancelled($productVariation)) {
        return $this->buildCancelledEvent($productVariation);
      }
      else {
        return $this->buildCart($productVariation);
      }
    }
    else {
      if ($this->eventIsCancelled($productVariation)) {
        return $this->buildCancelledEvent($productVariation);
      }
      elseif ($this->eventIsVolunteeringOneoff($productVariation)) {
        return $this->buildVolunteeringOneOff($productVariation);
      }
    }

    return [];
  }

  public function getProduct() {
    $product = Drupal::service('su_drupal_helper')->getRouteEntity();

    if ($product && $product->getEntityTypeId() == 'commerce_product_variation') {
      return $product->getProduct();
    }
    return $product;
  }

  public function getVariation($product) {
    if ($product && $product->getEntityTypeId() == 'commerce_product_variation') {
      return $product;
    }

    /** @var \Drupal\commerce_product\ProductVariationStorageInterface $variation_storage */
    $variation_storage = Drupal::entityTypeManager()
      ->getStorage('commerce_product_variation');
    if ($product) {
      return $variation_storage->loadFromContext($product);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    $product = $this->getProduct();
    if ($product) {
      $tags = Cache::mergeTags($tags, $product->getCacheTags());
      $productVariation = $this->getVariation($product);
      if ($productVariation) {
        $tags = Cache::mergeTags($tags, $productVariation->getCacheTags());
      }
    }
    return Cache::mergeTags(parent::getCacheTags(), $tags);
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0; // 60 * 5; // cache for 5 minutes, as time goes on relative to the event date(s)
  }

  public function userNotLoggedIn($productVariation) {
    return Drupal::currentUser()->isAnonymous();
  }

  public function buildUserNotLoggedIn($productVariation) {
    $text = '';

    $currency_formatter = Drupal::service('commerce_price.currency_formatter');
    $price = $currency_formatter->format($productVariation->getPrice()
      ->getNumber(), $productVariation->getPrice()->getCurrencyCode());
    if ($price == '£0.00') {
      $price = 'Free';
    }
    $text .= '<div class="product--variation-field--variation_price__138705 bold-large field field--name-price field--type-commerce-price field--label-hidden field__item">' . $price . '</div>';
    $text .= $this->configuration['login_notice_text'] ?? '<p>To purchase tickets for an event, you\'ll need to log in.</p>';

    $link = Url::fromRoute('user.login', [], [
      'absolute' => TRUE,
      'query' => ['destination' => Drupal::request()->getRequestUri()],
    ])->toString();
    $button = '<p><a href="' . $link . '" class="button">Log in</a></p>';

    return [
      '#markup' => $text . $button,
    ];
  }


  public function eventIsExternal($productVariation) {
    $url_external = FALSE;

    $product = $productVariation->getProduct();
    if ($product->hasField('field_external_link') && $product->field_external_link->uri) {
      $url_external = TRUE;
    }
    if ($productVariation->hasField('field_event_external_link') && $productVariation->field_event_external_link->uri) {
      $url_external = TRUE;
    }
    return ($product->hasField('field_ticketing_type') && $product->field_ticketing_type->value == 'external' && $url_external);
  }

  public function eventIsSoldOut($productVariation) {
    return $this->eventIsBookable($productVariation) && !commerce_stock_notifications_check_stock($productVariation);
  }

  public function eventIsBookable(ProductVariation $productVariation) {
    if ($productVariation->hasField('is_ticket')) {
      if (!$productVariation->is_ticket->value) {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function eventIsCancelled(ProductVariation $productVariation) {
    $product = $productVariation->getProduct();
    if ($product->hasField('field_event_category')) {
      $category = $product->get('field_event_category')->referencedEntities();
      if (count($category) > 0 && $category[0]->getName() == 'Cancelled') {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function eventIsPast(ProductVariation $productVariation) {
    if ($productVariation->hasField('field_date_range')) {
      $started = $productVariation->get('field_date_range')->value < strtotime('now');
      $ended = $productVariation->get('field_date_range')->end_value && $productVariation->get('field_date_range')->end_value < strtotime('now');
      // dd([$started, $ended]);
      if ($started && (!$productVariation->get('field_date_range')->end_value || $ended)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function eventIsInProgress(ProductVariation $productVariation) {
    if ($productVariation->hasField('field_date_range')) {
      $started = $productVariation->get('field_date_range')->value < strtotime('now');
      $end = (int) $productVariation->get('field_date_range')->end_value;
      $ended = $end && $end < strtotime('now');
      if ($started && $end && !$ended) {
        return TRUE;
      }
    }

    return FALSE;
  }


  public function buildCart($productVariation) {
    $product = $productVariation->getProduct();
    if (!$product) {
      return [];
    }
    $view_mode = 'product_sidebar_add_to_cart';
    $view_builder = Drupal::entityTypeManager()
      ->getViewBuilder('commerce_product');
    $build = $view_builder->view($product, $view_mode);
    return $build;
  }

  public function buildExternalEvent($productVariation) {
    $build = [];

    $product = $productVariation->getProduct();

    $links = [];
    if ($product->hasField('field_external_link') && $product->field_external_link->uri) {
      $links[] = [
        'name' => NULL,
        'url' => $product->field_external_link->uri,
        'title' => $product->field_external_link->title,
      ];
    }
    foreach ($product->getVariations() as $variation) {
      if ($variation->hasField('field_event_external_link') && $variation->field_event_external_link->uri) {
        if (!in_array($product->field_external_link->uri, array_column($links, 'url'))) {
          $links[] = [
            'name' => su_events_get_variation_dates_formatted($variation, FALSE),
            'url' => $variation->field_event_external_link->uri,
            'title' => $variation->field_event_external_link->title,
          ];
        }
      }
    }

    // Don't show it as "external" if it's a Union link.
    $unionLink = FALSE;

    $message = '';
    if (count($links) > 0) {
      foreach ($links as $link) {
        if ($link['url']) {
          if ($link['name']) {
            $message .= '<h4>' . $link['name'] . '</h4>';
          }
          $message .= '<p><a href="' . $link['url'] . '" target="_blank" class="button" rel="noopener">' . $link['title'] . '</a></p>';
        }

        if (strpos($link['url'], "studentsunionucl") !== FALSE) {
          $unionLink = TRUE;
        }
      }
    }
    else {
      $message .= '<p class="terms">See the event description for more information.</p>';
    }

    if ($message) {
      $build['markup'] = [
        '#markup' => Markup::create($message),
      ];
      if (!$unionLink) {
        $build['terms'] = [
          '#markup' => Markup::create('<p class="terms">If this event is booked on an external website, it may be subject to that platform\'s terms and conditions.</p>'),
        ];
      }
    }

    return $build;
  }

  public function buildPastEvent($productVariation) {
    $build = [];

    $message = '<p>This event has already happened. Tickets cannot be booked.</p><p><a href="/whats-on" class="button" rel="noopener">Browse more events</a></p>';

    if ($message) {
      $build['markup'] = [
        '#markup' => Markup::create($message),
      ];
    }

    return $build;
  }

  public function eventIsVolunteeringOneoff($productVariation) {
    if (
      !$productVariation->getProduct()->field_event_owner_group
    ) {
      return FALSE;
    }
    $ownerGroup = $productVariation->getProduct()->field_event_owner_group->entity;
    if ($ownerGroup && $ownerGroup->bundle() == 'volunteering_opp') {
      return TRUE;
    }
    return FALSE;
  }

  public function buildVolunteeringOneOff($productVariation) {
    $build = [];

    $ownerGroup = $productVariation->getProduct()->field_event_owner_group->entity;
    if (!$ownerGroup) {
      return [];
    }

    $message = '<p>Express your interest in this volunteering event on the volunteering opportunity page.</p><p><a href="' . $ownerGroup->toUrl()
        ->toString() . '" class="button" rel="noopener">' . $ownerGroup->label() . '</a></p>';

    if ($message) {
      $build['markup'] = [
        '#markup' => Markup::create($message),
      ];
    }

    return $build;
  }

  public function buildCancelledEvent($productVariation) {
    $build = [];

    $message = '<p>This event has been cancelled. Contact the event organiser for more information.</p><p><a href="/whats-on" class="button" rel="noopener">Browse more events</a></p>';

    if ($message) {
      $build['markup'] = [
        '#markup' => Markup::create($message),
      ];
    }

    return $build;
  }

  public function buildInProgressEvent($productVariation) {
    $build = [];

    $message = '<p>This is event is currently in progress. Tickets cannot be booked, however there may be more information about how to still get involved on this event page.</p><p><a href="/whats-on" class="button" rel="noopener">Browse more events</a></p>';

    if ($message) {
      $build['markup'] = [
        '#markup' => Markup::create($message),
      ];
    }

    return $build;
  }

}
