<?php

namespace Drupal\su_events\Plugin\search_api\processor;

use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes past events from being indexed.
 *
 * @SearchApiProcessor(
 *   id = "search_api_exclude_past_events",
 *   label = @Translation("Search API exclude past event products from being indexed"),
 *   description = @Translation("Excludes past event products from being indexed."),
 *   stages = {
 *     "alter_items" = -50
 *   }
 * )
 */
class ExcludePastEvents extends ProcessorPluginBase {

  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $object = $item->getOriginalObject()->getValue();
      $bundle = $object->bundle();
      if ($object->getEntityTypeId() == 'commerce_product' && $bundle == 'event') {
        $keep = FALSE;
        foreach ($object->getVariations() as $variation) {
          // Index if in the last two week
          if (!su_events_is_passed($variation, 60 * 60 * 24 * 10)) {
            $keep = TRUE;
            break;
          }
        }
        if (!$keep) {
          unset($items[$item_id]);
        }
      }
    }
  }
}
