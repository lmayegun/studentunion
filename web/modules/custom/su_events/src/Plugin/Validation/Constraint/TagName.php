<?php

namespace Drupal\su_events\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "TagName",
 *   label = @Translation("Tag name cannot be hashtag", context = "Validation"),
 *   type = "string"
 * )
 */
class TagName extends Constraint
{

    /**
     * {@inheritdoc}
     */
    public function validate($items, Constraint $constraint)
    {
        foreach ($items as $item) {
            if (substr($item->value, 0, 1) == '#') {
                $this->context->addViolation("Tag name cannot be a hashtag. Use normal capitalisation and spacing for these tags, like 'Welcome 2022 Events'.", ['%value' => $item->value]);
            }
        }
    }
}
