<?php

namespace Drupal\su_events\Form;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_ticketing\Entity\CommerceTicket;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchForm;
use Drupal\Core\Database\Database;

/**
 */
class ResendTickets extends BatchForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_events_resend_tickets';
  }

  public function getTicketIds(ProductInterface $product) {

    // Get tickets for product:
    $variationIds = $product->getVariationIds();

    $db = Database::getConnection();
    $query = $db->select('commerce_ticket', 'ct')
      ->fields('ct', ['id']);
    $query->innerJoin('commerce_order_item', 'oi', 'oi.order_item_id = ct.order_item_id');
    $query = $query->condition('oi.purchased_entity', $variationIds, 'IN')
      ->condition('ct.state', 'active');
    $results = $query->execute()
      ->fetchCol();

    $data = [];
    foreach ($results as $id) {
      $data[] = ['id' => $id];
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $productId = $form_state->get('product_id');
    $product = Product::load($productId);
    $data = $this->getTicketIds($product);

    $this->totalRows = count($data);

    // Chunk the array:
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 1;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function processOperation(array $data, array &$context) {
    foreach ($data as $ticketId) {
      $stateKey = 'ticketResend-' . date('Y-m-d') . '-' . $ticketId;

      if (\Drupal::state()->get($stateKey)) {
        \Drupal::logger('su_events')->debug('Already sent today - ticket receipt ' . $ticketId . ' NOT resent in bulk, triggered by ' . \Drupal::currentUser()->getEmail() . '.');
        continue;
      }

      /** @var \Drupal\commerce_ticketing\CommerceTicketInterface $ticket */
      $ticket = CommerceTicket::load($ticketId);
      $result = \Drupal::service('commerce_ticketing.ticket_receipt_mail')->send($ticket);
      \Drupal::state()->set($stateKey, TRUE);
      if ($result) {
        \Drupal::logger('su_events')->debug('Ticket receipt ' . $ticketId . ' resent in bulk, triggered by ' . \Drupal::currentUser()->getEmail() . '.');
        \Drupal::state()->set($stateKey, TRUE);
      } else {
        \Drupal::logger('su_events')->debug('Error - Ticket receipt ' . $ticketId . ' NOT resent in bulk, triggered by ' . \Drupal::currentUser()->getEmail() . '.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ProductInterface $commerce_product = NULL) {
    $form = parent::buildForm($form, $form_state);

    $form_state->set('product_id', $commerce_product->id());

    $count = count($this->getTicketIds($commerce_product));
    $form['warning'] = [
      '#type' => 'markup',
      '#weight' => -50,
      '#markup' => '<b>Warning:</b> this will resend ' . $count . ' tickets to the relevant customers. It can only be done once per day.'
    ];

    return $form;
  }
}
