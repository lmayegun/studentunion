<?php

namespace Drupal\su_events\Access;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_ticketing_scanner\Form\ScannerForm;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Checks access for displaying configuration translation page.
 */
class TicketScannerAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   * The access result.
   */
  public function access(AccountInterface $account, Product $commerce_product = null, ProductVariation $commerce_product_variation = null, string $ticket_uuid = null) {
    if (!$commerce_product) {
      return AccessResult::forbidden();
    }

    // Standard access
    $access = (new ScannerForm)->access($account, $commerce_product);

    // Event sales access
    $access = $access->orIf(su_events_sales_access($account, $commerce_product));

    return $access;
  }
}
