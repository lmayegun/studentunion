<?php

namespace Drupal\su_events\Controller;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_ticketing\CommerceTicketInterface;
use Drupal\commerce_ticketing\Controller\TicketController;
use Drupal\commerce_ticketing_pdf\Controller\PDFController;
use Drupal\commerce_ticketing_pdf\PDF\BasePdf;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * 
 */
class EventSales extends ControllerBase
{

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function access(AccountInterface $account, ProductInterface $commerce_product = null)
  {
    return su_events_sales_access($account, $commerce_product);
  }
}
