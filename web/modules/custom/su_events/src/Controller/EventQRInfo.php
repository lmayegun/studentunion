<?php

namespace Drupal\su_events\Controller;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\smart_date\Entity\SmartDateFormat;
use Drupal\smart_date\SmartDateTrait;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class EventQRInfo extends ControllerBase
{
    public function generateQRcode($url)
    {
        $logoUrl = 'https://studentsunionucl.org/sites/uclu.org/files/su_logo_for_su_jobs_15.png';

        return "http://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor=FFFFFF&amp;data=' . urlencode($url) . '&amp;qzone=1&amp;margin=0&amp;size=600x600&amp;ecc=L";
    }

    public function renderListInfoPage(ProductInterface $commerce_product)
    {
        $host =  \Drupal::request()->getSchemeAndHttpHost();

        $url = $commerce_product->toUrl();
        $url->setOption('query', [
            'qr' => '1',
        ]);
        $url = $host . $url->toString();

        $qrs = []; //['Event page (' . $commerce_product->label() . ')' => $url];
        $variations = $commerce_product->getVariations();
        foreach ($variations as $variation) {
            $url = $variation->toUrl();
            $url->setOption('query', [
                'qr' => '1',
            ]);
            $url = $host . $url->toString();

            $name = su_events_get_variation_dates_formatted($variation, true);
            $qrs[$name] = $url;
        }

        $qrsHtml = '';
        foreach ($qrs as $name => $url) {
            $qrUrl = $this->generateQRcode($url);
            $qrsHtml .= '<h3>' . $name . '</h3><p>' . $url . '</p><p><img width="200px" height="200px" src="' . $qrUrl . '" alt="qr code" /></p>';
        }

        return [
            '#type' => 'markup',
            '#markup' => '<p>Share a QR code to take people straight to a product purchase page for the event, or any specific variation.</p>
            <p>You can also link people directly, or generate your own QR code, using the raw URLs provided.</p>' . $qrsHtml
        ];
    }
}
