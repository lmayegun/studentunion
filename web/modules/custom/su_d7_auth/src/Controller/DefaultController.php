<?php

/**
 * @file
 * Contains \Drupal\su_d7_auth\Controller\DefaultController.
 */

namespace Drupal\su_d7_auth\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Default controller for the su_d7_auth module.
 */
class DefaultController extends ControllerBase {

  public function _su_d7_auth_title() {
    return $this->config('su_d7_auth.settings')->get('title');
  }

  public function su_d7_auth(Request $request) {
    // Skip pass through if coming from D7 (to avoid infinite loop of redirects)
    if ($request->query->get('spt')) {
      $default4xxController = new \Drupal\system\Controller\Http4xxController();
      return $default4xxController->on404();
    }

    $servers = explode("\n", $this->config('su_d7_auth.settings')->get('servers'));
    // Initialize the variables.
    $new_status = '';
    $new_status_code = 0;
    $new_path = '';
    $save_redirect = $this->config('su_d7_auth.settings')->get('save_redirect');
    $statuses = _su_d7_auth_redirect_options();
    $redirect_code = $this->config('su_d7_auth.settings')->get('redirect_code');
    $force_status = $this->config('su_d7_auth.settings')->get('force_redirect_code');
    $code = NULL;

    // Prepare protocol set to current to use for servers .
    $protocol = $GLOBALS['base_url'] == $GLOBALS['base_insecure_url'] ? 'http://' : 'https://';
    if ($request->getRequestUri() !== '/su_d7_auth') {
      foreach ($servers as $server) {
        // If a status code is set, our work looping through servers is done.
        if ($new_status_code) {
          break;
        }

        $path = (FALSE === strpos($server, '://')) ? $protocol . trim($server) : trim($server);
        if (FALSE === strpos($path, '[request_uri]')) {
          $path .= $request->getRequestUri();
        } else {
          $path = str_replace('/[request_uri]', $request->getRequestUri(), $path);
        }
        $client = \Drupal::httpClient(
          '',
          [
            'request.options' => [
              'timeout' => 3,
              'connection_timeout' => 2,
            ],
          ]
        );

        // Disable confirming SSL etc
        $code = 200;
        if (\Drupal::currentUser()->isAuthenticated()) {
          $new_path_d7 = $this->loginToD7($request, $server, $protocol, $path);
          if ($new_path_d7) {
            $path = $new_path_d7;
          }
        }

        // try {
        //   $response = $client->head($path);
        //   $code = $response->getStatusCode();
        // } catch (\GuzzleHttp\Exception\ClientException $e) {
        //   watchdog_exception('su_d7_auth', $e, 'Caught response: ' . $e->getResponse()->getStatusCode());
        // } catch (\GuzzleHttp\Exception\RequestException $e) {
        //   watchdog_exception('su_d7_auth', $e);
        // } catch (\GuzzleHttp\Exception\ServerException $e) {
        //   watchdog_exception('su_d7_auth', $e);
        // } catch (\InvalidArgumentException $e) {
        //   watchdog_exception('su_d7_auth', $e, 'Please ensure you have configured fallback servers and that the URLs for each are valid.  %type: @message in %function (line %line of %file).');
        // }

        if ($code >= 200 && $code <= 250) {
          // We found the actual document on this server, set to 302 Found.
          $new_status_code = $redirect_code;
          // TODO figure out how to get Guzzle to give us back the URL/path
          // that we finally ended up on.  There doesn't seem to be a way so
          // we would have to send the Max-Forwards request-header and collect
          // each path as we go.

          $new_path = $path;
        } elseif ($code >= 300 && $code <= 305) {
          // This is never going to happen...  if we decide we care...
          // TODO figure out how to get Guzzle to give us back the URL/path
          // that we finally ended up on.  There doesn't seem to be a way so
          // we would have to send the Max-Forwards request-header and collect
          // each path as we go.
          // For now, knowing that the path we started with ultimately ended up
          // with a 200 status code (captured in above) is good enough.

          // No resource, but found another redirect, so follow the redirect.
          $new_status_code = !$force_status ? $code : $redirect_code;
          // $new_path = (isset($header_info->redirect_url)) ? ($header_info->redirect_url) : $path;
        }
        // Anything other than 200s or 300s and the path wasn't found, so we
        // have nothing to redirect to.
      }
    }
    // Generate the redirect code based on redirect code.
    if ($new_status_code) {
      $new_status = $statuses[$new_status_code];
    }
    // If there is no status/path to use, see if we are searching.
    if (!$new_status && !$new_path) {
      // TODO Integrate with Search 404 instead of doing this ourselves.
      $search_path = $this->config('su_d7_auth.settings')->get('search');
      if ($search_path) {
        \Drupal::messenger()->addError(t('The page you were trying to navigate to could not be found. Below you will see search results which may help you find what you are looking for.'));
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = $search_path . '/' . trim(str_replace('/', ' ', $request->getRequestUri()));
        // We do not save a search redirect.
        $save_redirect = FALSE;
      }
    }
    // If there is no status/path to use, see if a simple redirect is specified.
    if (!$new_status && !$new_path) {
      $redirect_path = $this->config('su_d7_auth.settings')->get('redirect');
      if ($redirect_path) {
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = str_replace('/[request_uri]', $request->getRequestUri(), $redirect_path);
      }
    }
    // If there is still no status/path to use, fall back to set 404 path.
    if (!$new_status && !$new_path) {
      $redirect_path = $this->config('su_d7_auth.settings')->get('site_404');
      if ($redirect_path) {
        // Remove the destination because Drupal 8 is trying to drive its
        // developers to suicide.  There is a proposed fix in this issue:
        // https://www.drupal.org/project/drupal/issues/2950883
        \Drupal::request()->query->remove('destination');
        $new_status = '302 Found';
        $new_status_code = 302;
        $url = Url::fromUserInput($redirect_path)
          ->setAbsolute()
          ->toString(TRUE);
        // Because of the somewhat nightmarish early rendering / cacheability
        // issue described in #3032255) let's do the redirect separately.
        $new_path = $url->getGeneratedUrl();
        $response = new LocalRedirectResponse($new_path, $new_status_code);
        \Drupal::logger('su_d7_auth')->notice('Redirected to 404 path ' . $new_path);
        // Add the $url object as a dependency of [ the response ]
        $response->addCacheableDependency($url);
        return $response;
      }
    }
    // If there is a status and a path, we go there.
    if ($new_status && $new_path) {
      // TODO after Redirect module gets some developer documentation
      // https://www.drupal.org/project/redirect/issues/3001965
      /*
      if (\Drupal::moduleHandler()->moduleExists('redirect') && $save_redirect) {
        // Save to Redirect module system.
        $redirect = new stdClass();
        redirect_object_prepare($redirect, [
          'source' => ltrim($request->getRequestUri(), '/'),
          'redirect' => $new_path,
          'status_code' => $new_status_code,
        ]);
        redirect_save($redirect);
      }
      */
      // Remove the destination because Drupal 8 is trying to drive its
      // developers to suicide.  There is a proposed fix in this issue:
      // https://www.drupal.org/project/drupal/issues/2950883
      \Drupal::request()->query->remove('destination');
      // TODO probably only do the above and below when actually redirecting to
      // a remote site, not doing one of the in-site redirects that's also
      // possible.
//      \Drupal::logger('su_d7_auth')->notice('Redirected to found path ' . $new_path);
      return new TrustedRedirectResponse($new_path, $new_status_code);
    }

    $default4xxController = new \Drupal\system\Controller\Http4xxController();
    return $default4xxController->on404();
  }

  public function loginToD7(Request $request, $server, $protocol, $path) {
    if (!\Drupal::currentUser()->isAuthenticated()) {
      return false;
    }

    \Drupal::service('page_cache_kill_switch')->trigger();

    $time = time();

    $hash = $this->createAuthMapUser(\Drupal::currentUser(), $time);

    $path = str_replace(trim($server), '', $path);
    $destination = trim($server) . '/d8?d=' . urlencode($path) . '&u=' . \Drupal::currentUser()->id() . '&t=' . $time . ($protocol == 'https://' ? '&https' : '');

    return $destination;
  }

  public function createAuthMapUser($account, $time) {
    $account = User::load($account->id());

    $hash = Crypt::hmacBase64($account->id(), Settings::getHashSalt() . $time);

    Database::setActiveConnection('legacy');
    $db = \Drupal\Core\Database\Database::getConnection();

    // Create a D7 authmap for use
    $db->delete('authmap')
      ->condition('authname', $account->getAccountName())
      ->execute();

    $count = $db->select('authmap', 'a')->fields('a')->condition('authname', $account->getAccountName())->countQuery()->execute()->fetchField();
    if ($count == 0) {
      $db->merge('authmap')->fields([
        'module' => 'shib_auth',
        'authname' => $account->getAccountName(),
        'uid' => $account->id(),
      ])
        ->condition('uid', $account->id())
        ->condition('module', 'shib_auth')
        ->condition('authname', $account->getAccountName())
        ->execute();
    }

    // Create a custom hash:
    $db->merge('d8_hash')->fields([
      'uid' => $account->id(),
      'hash' => $hash,
      'created' => time(),
      'name' => $account->getAccountName(),
      'mail' => $account->getEmail(),
      'full_name' => $account->getDisplayName(),
      'roles' => implode(',', $account->getRoles()),
    ])->condition('uid', $account->id())->execute();

    return $hash;
  }

  public function redirectToD7(Request $request) {
    $servers = explode("\n", $this->config('su_d7_auth.settings')->get('servers'));
    $server = reset($servers);
    $protocol = $GLOBALS['base_url'] == $GLOBALS['base_insecure_url'] ? 'http://' : 'https://';

    $path = urldecode($request->query->get('d'));

    $code = 301;
    if (\Drupal::currentUser()->isAuthenticated()) {
      $new_path_d7 = $this->loginToD7($request, $server, $protocol, $path);
      if ($new_path_d7) {
        $path = $new_path_d7;
      }
    }

//    \Drupal::logger('su_d7_auth')->notice('Redirected to D7 path ' . $path);
    return new TrustedRedirectResponse($path, $code);
  }
}
