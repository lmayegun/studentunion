<?php

namespace Drupal\su_d7_auth\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchForm;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Database;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\profile\Entity\ProfileInterface as EntityProfileInterface;
use Drupal\user\Entity\User;

/**
 */
class UpdateRosaForm extends BatchForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'update_rosa';
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $data = [];

    $ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', ['student', 'member'], 'IN')
      ->sort('access', 'DESC')
      ->execute();
    foreach ($ids as $id) {
      $data[] = [
        'type' => 'student',
        'id' => $id,
      ];
    }

    $ids = \Drupal::entityQuery('group')
      ->condition('type', 'club_society')
      //      ->condition('id', [82268], 'IN')
      ->execute();

    foreach ($ids as $id) {
      $data[] = [
        'type' => 'club_society',
        'id' => $id,
      ];
      $data[] = [
        'type' => 'club_society_d7',
        'id' => $id,
      ];
    }

    $this->totalRows = count($data);

    // Chunk the array:
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 1;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public static function updateClubSoc($club) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $studentService = \Drupal::service('su_students.student_services');

    $records = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-president'), NULL, new DateTime(), TRUE);
    $record = reset($records);
    $presidentStudentNumber = '';
    if ($record) {
      $presidentUser = $record->getUser();
      $presidentStudent = $studentService->getStudentProfile($presidentUser);
      $presidentStudentNumber = $presidentStudent->field_student_number->value;
      if (strpos($presidentStudentNumber, 'status not Open') != FALSE) {
        $presidentStudentNumber = (int) filter_var($presidentStudentNumber, FILTER_SANITIZE_NUMBER_INT);
      }
    }

    $records = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-treasurer'), NULL, new DateTime(), TRUE);
    $record = reset($records);
    $treasurerStudentNumber = '';
    if ($record) {
      $treasurerUser = $record->getUser();
      $treasurerStudent = $studentService->getStudentProfile($treasurerUser);
      $treasurerStudentNumber = $treasurerStudent->field_student_number->value;
      if (strpos($treasurerStudentNumber, 'status not Open') != FALSE) {
        $treasurerStudentNumber = (int) filter_var($treasurerStudentNumber, FILTER_SANITIZE_NUMBER_INT);
      }
    }

    $records = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-captain'), NULL, new DateTime());

    $captains = [];
    foreach ($records as $record) {
      $captainUser = $record->getUser();
      $captainStudent = $studentService->getStudentProfile($captainUser);
      $captainStudentNumber = $captainStudent->field_student_number->value;
      if (strpos($captainStudentNumber, 'status not Open') != FALSE) {
        $captainStudentNumber = (int) filter_var($captainStudentNumber, FILTER_SANITIZE_NUMBER_INT);
      }
      $captains[] = $captainStudentNumber;
    }

    $records = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-member'), NULL, new DateTime());
    $records28DaysAgo = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-member'), NULL, new DateTime('-28 days'));
    $records = array_merge($records, $records28DaysAgo);
    $members = [];
    \Drupal::logger('su_d7_auth')->notice($club->label() . ' - ' . count($records) . ' members');
    foreach ($records as $record) {
      $memberUser = $record->getUser();
      if (!$memberUser) {
        continue;
      }
      $memberStudent = $studentService->getStudentProfile($memberUser);
      if (!$memberStudent) {
        \Drupal::logger('su_d7_auth')->notice('Rosa update cannot find student profile for member user (possibly associate/visiting) - ' . $memberUser->id());
        continue;
      }
      $memberStudentNumber = $memberStudent->field_student_number->value;
      if (strpos($memberStudentNumber, 'status not Open') != FALSE) {
        $memberStudentNumber = (int) filter_var($memberStudentNumber, FILTER_SANITIZE_NUMBER_INT);
      }
      $members[] = $memberStudentNumber;
    }

    $members = array_unique($members);

    $n = $club->field_finance_cost_code->entity ? $club->field_finance_cost_code->entity->getName() : '';
    $g = str_replace('N', 'G', $n);
    $dept = $club->field_finance_department_code->entity ? $club->field_finance_department_code->entity->getName() : '';

    // Generate data:
    $array = [
      'name' => $club->label(),
      'email' => $club->field_email->value,
      'drupal_id' => $club->id(),
      'president_id' => $presidentStudentNumber,
      'treasurer_id' => $treasurerStudentNumber,
      'active' => 1,
      'affiliated' => $club->field_clubsoc_affiliated->value,
      'cost_code' => $n . $dept,
      'grant_cost_code' => $g . $dept,
      'captain' => serialize($captains),
      'members' => serialize($members),
    ];

    Database::setActiveConnection('rosa');
    $db = Database::getConnection();
    $db->merge('club_society')->fields($array)->condition('drupal_id', $club->id())->execute();

    Database::setActiveConnection('default');
    \Drupal::logger('su_d7_auth')->debug('Rosa: updating club_society ' . $club->id());
  }


  public static function updateClubSocD7($club) {
    Database::setActiveConnection('legacy');
    $db = Database::getConnection();

    if ($club->field_clubsoc_affiliated->value) {
      $node = [
        'nid' => $club->id(),
        'type' => 'club_or_society',
        'language' => 'und',
        'title' => $club->label(),
        'uid' => 1,
        'status' => 1,
        'uuid' => $club->uuid(),
        'changed' => strtotime('now'),
      ];
      $db->merge('node')
        ->key('nid', $club->id())
        ->fields($node)
        ->execute();

      $og = [
        'gid' => $club->id(),
        'etid' => $club->id(),
        'entity_type' => 'node',
        'label' => $club->label(),
        'state' => 1,
        'created' => strtotime('now'),
      ];
      $db->merge('og')
        ->key('etid', $club->id())
        ->key('gid', $club->id())
        ->fields($og)
        ->execute();
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $records = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-member'), NULL, new DateTime());
    $records28DaysAgo = $gmrRepositoryService->get(NULL, $club, GroupRole::load('club_society-member'), NULL, new DateTime('-28 days'));
    $records = array_merge($records, $records28DaysAgo);

    foreach ($records as $record) {
      $username = $record->getUser()->getAccountName();

      $uids = $db->select('users', 'u')
        ->fields('u', ['uid'])
        ->condition('name', $username)
        ->execute()
        ->fetchAllAssoc('uid');

      $uid = reset($uids);
      if (!$uid) {
        continue;
      }
      $uid = $uid->uid;
      if (!$uid) {
        continue;
      }

      $gid = $club->id();
      $rid = 81;

      $created = strtotime($record->date_range->value);
      if ($record->field_order) {
        $order = $record->field_order->entity;
        if ($order) {
          $items = $order->getItems();
          foreach ($items as $item) {
            $productVariation = $item->getPurchasedEntity();
            if ($productVariation) {
              $product = $productVariation->getProduct();
              $soc = $product->field_club_society ? $product->field_club_society->entity : NULL;
              if ($soc) {
                if ($item->field_record_upgraded_from && $oldRecord = $item->field_record_upgraded_from->entity) {
                  $created = $oldRecord->date_range->value ? strtotime($oldRecord->date_range->value) : $created;
                }
              }
            }
          }
        }
      }

      // Insert og_membership:
      $og_membership = [
        'type' => 'csm',
        'etid' => $uid,
        'entity_type' => 'user',
        'gid' => $gid,
        'state' => 1,
        'group_type' => 'node',
        'field_name' => 'og_group_ref',
        'language' => 'en',
        'created' => $created,
      ];

      $db->merge('og_membership')
        ->key('etid', $uid)
        ->key('gid', $club->id())
        ->fields($og_membership)
        ->execute();

      // Insert og_users_roles:
      $og_users_roles = [
        'uid' => $uid,
        'rid' => $rid,
        'gid' => $gid,
        'group_type' => 'node',
      ];

      $db->merge('og_users_roles')
        ->key('uid', $uid)
        ->key('rid', $rid)
        ->key('gid', $gid)
        ->fields($og_users_roles)
        ->execute();
    }
    Database::setActiveConnection('default');
    \Drupal::logger('su_d7_auth')->debug('D7: updating club_society ' . $club->id());
  }

  public static function mergeProfiles(array $profiles) {
    $studentService = \Drupal::service('su_students.student_services');

    $realProfile = NULL;
    $profilesToDelete = [];
    foreach ($profiles as $profile) {
      if ($studentService->haveStudentDataForProfile($profile)) {
        $realProfile = $profile;
      } else {
        $profilesToDelete[] = $profile;
      }
    }

    foreach ($profilesToDelete as $delete) {
      if ($delete->field_student_number->value && !$realProfile->field_student_number->value) {
        $realProfile->set(
          'field_student_number',
          $delete->field_student_number->value
        );
        $realProfile->save();
      }
      //      $delete->delete();
    }

    return $realProfile;
  }

  public static function updateProfileFromD7(AccountInterface $account, EntityProfileInterface $profile) {
    Database::setActiveConnection('legacy');
    $db = Database::getConnection();
    $result = $db->select('uclu_student', 'us')
      ->fields('us')
      ->condition('ucl_user_id', $account->field_ucl_user_id->value)
      ->execute()
      ->fetchAssoc();
    Database::setActiveConnection('default');
    if (!$result) {
      return $profile;
    }

    // Drupal 8 field => drupal 7 field uclu_student table
    $fields = [
      'field_student_number' => 'student_number',
      'field_student_number_scj' => 'student_number_scj',
      'field_student_number_spr' => 'student_number_spr',
      'field_student_enrolment_status' => 'enrolment_status',
      'field_student_gender' => 'gender',
    ];
    foreach ($fields as $d8 => $d7) {
      if (!$profile->$d8->value) {
        $profile->set($d8, $result[$d7]);
      }
    }

    // case 'students_study_modes':
    //   $options = $studentDataDefinitions->getMappedNameStudyMode();
    //   $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
    //   break;
    // case 'students_term_time_accomodation':
    //   $options = $studentDataDefinitions->getMappedNameTermTimeAccommodation();
    //   $name = isset($options[$value]) && $options[$value] ? $options[$value] : $value;
    //   break;
    $taxonomy_field = [
      'field_student_faculty' => [
        'field_name' => 'faculty_code',
        'vid' => 'ucl_faculties_and_departments'
      ],
      'field_student_caring_resp' => [
        'field_name' => 'caring_responsibility',
        'vid' => 'students_caring_responsibilities'
      ],
      'field_student_course' => [
        'field_name' => 'course_code',
        'vid' => 'ucl_courses'
      ],
      'field_student_attendance_mode' => [
        'field_name' => 'attendance_mode',
        'vid' => 'students_attendance_modes'
      ],
    ];
    foreach ($fields as $d8 => $d7) {
      if (!$profile->$d8->value) {
        $vid = $d7['vid'];
        $ucl_code_value = isset($result[$d7['field_name']]) ? $result[$d7['field_name']] : '';
        $term = su_drupal_helper_get_taxonomy_term_by_ucl_code($ucl_code_value, $vid);
        if ($term) {
          $profile->set($d8, $term);
        }
      }
    }
    $profile->save();
    return $profile;
  }

  public static function updateStudent($studentUser) {
    $profiles = \Drupal::entityTypeManager()->getStorage('profile')->loadMultipleByUser($studentUser, 'student');
    if (count($profiles) > 1) {
      $student = static::mergeProfiles($profiles);
    } else {
      $student = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser($studentUser, 'student');
    }

    if (!$student) {
      // print_r(['USER NO STUDENT PROFILE', $studentUser->id()]);
      return;
    }

    $student = static::updateProfileFromD7($studentUser, $student);

    if (!$studentUser->field_upi->value || !$student->field_student_number->value) {
      return;
    }

    $studentNumber = $student->field_student_number->value;
    if (strpos($studentNumber, 'status not Open') != FALSE) {
      $studentNumber = (int) filter_var($studentNumber, FILTER_SANITIZE_NUMBER_INT);
    }

    $array = [
      'access_role' => 'a:1:{i:0;s:12:"ROLE_STUDENT";}',
      'upi' => $studentUser->field_upi->value,
      'user_id' => $studentUser->field_ucl_user_id->value,
      'number' => $studentNumber,
      // 'date_of_birth' => '', // 1995-03-05 00:00:00.000
      'first_name' => $studentUser->field_first_name->value,
      // 'middle_name' => '',
      'last_name' => $studentUser->field_last_name->value,
      'full_name' => $studentUser->field_full_name->value,
      // 'gender' => '',
      'email' => $studentUser->getEmail(),
      'enrolment_status' => 'E',
      // 'course_code' => '',
      // 'attendance_mode' => '',
      // 'fee_status' => '',
      // 'study_year' => '',
      // 'study_start' => '',
      // 'study_end' => '',
      // created
      // last_modified,
      'username' => $studentNumber,
      // api_key
      // import_id
    ];

    // Update or insert:
    Database::setActiveConnection('rosa');
    $db = Database::getConnection();
    $db->merge('student')->fields($array)->condition('number', $studentNumber)->execute();
    Database::setActiveConnection('default');

    \Drupal::logger('su_d7_auth')->debug('Rosa: updating student ' . $studentNumber);
  }

  /**
   * {@inheritdoc}
   */
  public static function processOperation(array $data, array &$context) {
    if ($data['type'] == 'student') {
      static::updateStudent(User::load($data['id']));
    } elseif ($data['type'] == 'club_society_d7') {
      static::updateClubSocD7(Group::load($data['id']));
    } else {
      static::updateClubSoc(Group::load($data['id']));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    return $form;
  }
}
