<?php

namespace Drupal\commerce_ticketing_checkin\Form;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_ticketing\Entity\CommerceTicket;
use Drupal\commerce_ticketing_checkin\Form\CheckinMultistepFormBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Class ScannerForm.
 */
class CheckinConfirmForm extends CheckinMultistepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'checkin_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Product $commerce_product = NULL) {

    $form = parent::buildForm($form, $form_state);

    $ticket_ids = [];
    $rows = [];
    $users = $this->store->get('tickets');
    $uid = array_keys($users)[0];
    $count = count($users);

    $variations = explode(',', \Drupal::request()->query->get('variations'));
    $tickets = $this->getTicketsForUser($variations, $uid);
    foreach ($tickets as $ticket_id) {
      $ticket = CommerceTicket::load($ticket_id);
      $row = [];
      $row['Number'] = $ticket->get('ticket_number')->value;
      $row['Product'] = $ticket->getPurchasedEntity()->label();
      $row['Purchaser'] = $ticket->getOwner()->label();
      $rows[] = $row;

      $ticket_ids[] = $ticket_id;
    }

    $form_state->set('ticket_ids', $ticket_ids);

    $form['summary'] = [
      '#type' => 'table',
      '#header' => array_keys($rows[0]),
      '#rows' => $rows,
      '#caption' => $count == 1 ? $this->t('Ticket found') : $this->t('Tickets found'),
    ];

    if ($count > 1) {
      $options = [
        $count => 'All',
      ];
      $form['ticket_quantity'] = [
        '#type' => 'select',
        '#title' => $this->t('How many of your @count tickets should we check in?', ['@count' => $count]),
        '#required' => TRUE,
        '#options' => $options,
      ];
    }

    $form_state->set('commerce_product', $commerce_product->id());

    $form['actions']['submit']['#value'] = $this->t('Check in');

    $form['actions']['cancel'] = array(
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => array(
        'class' => array('button secondary'),
      ),
      '#weight' => 0,
      '#url' => Url::fromRoute(
        'commerce_ticketing_checkin.search_form',
        [
          'commerce_product' => $form_state->get('commerce_product'),
        ],
        [
          'query' => [
            'hash' => \Drupal::request()->query->get('hash'),
            'variations' => \Drupal::request()->query->get('variations'),
          ],
        ]
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo mark tickets as used
    $ticket_ids = $form_state->get('ticket_ids');
    $max = $form_state->getValue('ticket_quantity') ?? count($ticket_ids);
    $count = 0;
    foreach ($ticket_ids as $ticket_id) {
      $ticket = CommerceTicket::load($ticket_id);

      $ticket->set('state', 'used');
      $ticket->save();

      if ($count == $max) {
        continue;
      }
    }

    // clear form store
    $this->deleteStore($this->formFieldKeys());

    $message = 'Checked in. This form is now available for the next ticketholder check-in.';
    \Drupal::messenger()->addMessage($this->t($message));

    $form_state->setRedirect(
      'commerce_ticketing_checkin.search_form',
      [
        'commerce_product' => $form_state->get('commerce_product'),
      ],
      [
        'query' => [
          'hash' => \Drupal::request()->query->get('hash'),
          'variations' => \Drupal::request()->query->get('variations'),
        ],
      ]
    );
  }
}
