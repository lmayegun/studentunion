<?php

namespace Drupal\su_user\Service;

use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;

/**
 * Implements Class UclUserService Controller.
 */
class UclUserService {

  /**
   * Constructs a new UclUserService object.
   */
  public function __construct() {
  }

  /**
   * Get user when giving a UCL e-mail, UPI or user ID.
   *
   * $string has to be a COMPLETE identifier, this is not for searching partial strings.
   */
  public function getUserForUclFieldInput($string, $return_id = FALSE) {
    // We can't always determine what a string is
    // Where it isn't an e-mail
    // (uczxm34 AND UCZXM43 could both be user IDs and UPIs)
    // So we can test different fields where it's ambiguous
    $possibleFields = [];

    if (!$string) {
      return NULL;
    }

    if (\Drupal::service('email.validator')->isValid($string)) {
      $preArroba = substr($string, 0, strrpos($string, '@'));
      if ($this->isPossibleUclUserId($preArroba) && substr($string, strrpos($string, '@') == '@ucl.ac.uk')) {
        $possibleFields['name'] = $string;
        $possibleFields['field_ucl_user_id'] = $preArroba;
      } else {
        $possibleFields['mail'] = $string;
      }
    } else {
      if ($this->isPossibleUPI($string)) {
        $possibleFields['field_upi'] = $string;
      }

      if ($this->isPossibleUclUserId($string)) {
        $possibleFields['field_ucl_user_id'] = $string;
      }
    }

    foreach ($possibleFields as $field => $value) {
      $results = \Drupal::entityQuery('user')
        ->condition('status', 1)
        ->condition($field, $value, '=')
        ->execute();
      if (count($results) > 0) {
        $uid = reset($results);
        if ($return_id) {
          return $uid;
        } else {
          return User::load($uid);
        }
      }
    }

    return NULL;
  }

  public function isPossibleUPI($string) {
    return preg_match('/\b[a-z]{5}[0-9]{2}\b/i', $string);
  }

  public function isPossibleUclUserId($string) {
    return preg_match('/^[a-z][a-z0-9]{6}$/i', $string);
  }

  /**
   * Set revoke fields in the profile and
   * remove content to all fields that start with 'field_'
   *
   * @param Profile $profile
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function anonymiseProfile(Profile $profile) {
    foreach ($profile->getFields() as $field) {
      $name = $field->getName();
      if (preg_match('(^field_)', $name)) {
        $profile->set($field->getName(), NULL);
      }
    }
    $profile->set('field_profile_optin', FALSE);
    $profile->set('field_profile_optin_changed', \Drupal::time()->getRequestTime());
    $profile->save();
  }
}
