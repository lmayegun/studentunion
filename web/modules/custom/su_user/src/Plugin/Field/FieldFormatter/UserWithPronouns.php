<?php

namespace Drupal\su_user\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\user\Plugin\Field\FieldFormatter\UserNameFormatter;

/**
 * Plugin implementation of the 'user_name_ucl' formatter.
 *
 * @FieldFormatter(
 *   id = "user_name_with_pronouns",
 *   label = @Translation("User name with pronouns"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class UserWithPronouns extends UserNameFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];


    foreach ($items as $delta => $item) {
      /** @var $user \Drupal\user\UserInterface */
      if ($user = $item->getEntity()) {
        $pronouns = $user->get('field_pronouns')->getString() ? ' [' . $user->get('field_pronouns')->getString() . ']' : '';
        if ($this->getSetting('link_to_entity')) {
          $elements[$delta] = [
            '#theme' => 'username',
            '#account' => $user,
            '#suffix' => $pronouns,
            '#link_options' => ['attributes' => ['rel' => 'user']],
            '#cache' => [
              'tags' => $user->getCacheTags(),
            ],
          ];
        } else {
          $elements[$delta] = [
            '#markup' => $user->getDisplayName() . $pronouns,
            '#cache' => [
              'tags' => $user->getCacheTags(),
            ],
          ];
        }
      }
    }

    return $elements;
  }
}
