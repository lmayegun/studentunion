<?php

namespace Drupal\su_user\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\user\Plugin\Field\FieldFormatter\UserNameFormatter;

/**
 * Plugin implementation of the 'user_name_ucl' formatter.
 *
 * @FieldFormatter(
 *   id = "user_name_ucl",
 *   label = @Translation("User name with UCL  and pronouns"),
 *   description = @Translation("Display the user name with UCL fields like UPI and user ID."),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class UserWithUCLInfoFormatter extends UserNameFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $separator = ' • ';
    // $elements = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      $account = $item->getEntity();

      if ($this->getSetting('link_to_entity')) {
        $name = '<a href="/user/' . $account->id() . '">' . $account->getDisplayName() . '</a>';
      } else {
        $name = $account->getDisplayName();
      }
      $title_suffix = '';

      $userId = $account->get('field_ucl_user_id')->value;
      $upi = $account->get('field_upi')->value;
      if ($this->getSetting('link_to_entity')) {
        $email = '<a href="mailto:' . $account->get('mail')->value . '">' . $account->get('mail')->value . '</a>';
      } else {
        $email = $account->get('mail')->value;
      }

      $pronouns = $account->get('field_pronouns')->getString() ? $account->get('field_pronouns')->getString() : null;

      $ucl = [$name, $email, $userId, $upi, $pronouns];
      $ucl = array_filter($ucl);
      $title_suffix = implode($separator, $ucl);
      $elements[$delta]['#suffix'] = $title_suffix;
    }
    return $elements;
  }
}
