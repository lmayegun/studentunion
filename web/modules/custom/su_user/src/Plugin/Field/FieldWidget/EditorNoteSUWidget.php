<?php

namespace Drupal\su_user\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\editor_note\EditorNoteHelperService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\editor_note\Plugin\Field\FieldWidget\EditorNoteWidget;

/**
 * Editor note widget.
 *
 * @FieldWidget(
 *   id = "editor_note_widget_su",
 *   label = @Translation("Students' Union improved widget"),
 *   module = "editor_note",
 *   field_types = {
 *     "editor_note_item"
 *   }
 * )
 */
class EditorNoteSUWidget extends EditorNoteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#title'] = $this->fieldDefinition->getLabel();
    $element['#type'] = 'fieldset';
    if (!(!empty($entity) && !$entity->isNew())) {
      unset($element['add_note']);
    }
    return $element;
  }


  /**
   * Validate the editor note field.
   *
   * COPY of parent one
   *
   * @param array $element
   *   Element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object.
   */
  public function validate(array $element, FormStateInterface $form_state) {
    if ($this->getSetting('formatted')) {
      $value = $element['editor_notes']['value']['#value'];
    } else {
      $value = $element['editor_notes']['#value'];
    }

    $value = trim($value);
    $triggering_control = $form_state->getTriggeringElement();

    if (isset($triggering_control['#attributes']['data-type']) && $triggering_control['#attributes']['data-type'] == 'ajax-submit-note') {
      if (empty($value)) {
        $form_state->setError($element, $this->t('Update note field is required.'));
      } else {
        $form_state->clearErrors();
      }
    }

    $form_state->setValueForElement($element, ['value' => $value]);
  }
}
