<?php

namespace Drupal\su_user\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;

/** *
 * @Block(
 *   id = "su_user_profile_blurb",
 *   admin_label = @Translation("User profile introduction"),
 *   category = @Translation("Users"),
 * )
 */
class UserProfileBlurb extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = \Drupal::service('su_drupal_helper')->getRouteEntity();

    if (!$user) {
      return [];
    }
    if ($user->getEntityTypeId() !== 'user') {
      return [];
    }

    $message = '';
    $adminBox = '';

    if ($user->hasRole('student') || $user->hasRole('member')) {
      $adminBox = $this->generateAdminNotice($user);
      $message .= \Drupal::config('su_user.settings')->get('text_for_students_and_members');
    } elseif ($user->hasRole('su_staff')) {
      $message .= \Drupal::config('su_user.settings')->get('text_for_union_staff');
    } else {
      $message .= \Drupal::config('su_user.settings')->get('text_for_non_students');
    }

    // Build and output
    $build = [];

    if ($message && $message != '') {
      $build['markup'] = [
        '#markup' => Markup::create($message),
      ];
    }

    if ($adminBox && $adminBox != '') {
      $build['notice'] = [
        '#markup' => Markup::create('<div class="admin-box">' . $adminBox . '</div>'),
      ];
    }

    return $build;
  }

  public function getCacheTags() {
    if ($user = \Drupal::routeMatch()->getParameter('user')) {
      return Cache::mergeTags(parent::getCacheTags(), array('user:' . $user->id()));
    } else {
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

  public function generateAdminNotice($user) {
    if ($user->hasRole('student')) {
      if (!$user->hasRole('member')) {
        return 'You appear to be a student but are not opted in to Union membership. If you are a current student, <a href="/optin">opt in to membership</a> to participate in the Union by joining clubs and societies, attend member-only events, and representing your fellow students.</div>';
      }

      $randomOptions = [
        'not_joined_clubsoc' => NULL,
        'not_joined_vol' => NULL,
        'recommend_clubsoc' => NULL,
        'hey_look_data' => NULL,
        'hey_look_selfdefine' => NULL,
      ];

      $return = '';


      while ($return == '') {
        $option = array_rand($randomOptions);

        switch ($option) {
          case 'not_joined_clubsoc':
          case 'recommend_clubsoc':
            $role = 'club_society-member';
            $return = $this->generateRoleRecommendation(
              $user,
              $role,
              'It looks like you haven\'t joined any clubs or societies yet. Why not <a href="/clubs-societies/directory">browse our directory</a> - you\'re bound to find something interesting.'
            );
            break;

          case 'not_joined_vol':
          case 'recommend_vol':
            $role = 'volunteering_opp-interest';
            $return = $this->generateRoleRecommendation(
              $user,
              $role,
              'It looks like you haven\'t expressed interest in any volunteering opportunities currently. Why not <a href="/volunteering/directory">browse our Volunteering directory</a> - we have all kinds of opportunities across London and online.'
            );
            break;

          case 'hey_look_data':
            $return = '<b>Did you know?</b> As part of our <a href="/data-protection-and-privacy-policy" target="_blank">Data protection and privacy policy</a>, '
              . 'we let you know how we manage your data. If you ever want to change what data we share or store about you, '
              . 'find the "Data sharing" section of your <a href="/user/' . $user->id() . '/edit" target="_blank">profile editing page</a> '
              . 'or use the <a href="/contact-us" target="_blank">Contact Us form</a> with the subject "Data protection or privacy request".';
            break;

          case 'hey_look_selfdefine':
            $return = '<b>Did you know?</b> You can <a href="/user/' . $user->id() . '/self_define" target="_blank">self-define your demographic and personal data</a> at the Union at any time. '
              . 'This will allow you to vote for relevant student officers '
              . ' and help the Union understand and improve engagement across the student body. '
              . ' We never share this data, and only use it anonymously - see our <a href="/data-protection-and-privacy-policy" target="_blank">data protection and privacy policy</a> for more.';
            break;
        }
      }

      return $return;
    }
  }

  public function generateRoleRecommendation($user, $role_id, $messageIfNone = '') {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $count = $gmrRepositoryService->currentEnabledInstanceExists($user, NULL, GroupRole::load($role_id));
    if ($count > 0) {
      // @todo recommend clubs and societies or volunteering opportunities based on current memberships
      return $this->recommendGroups($user, $role_id);
    } else {
      return $messageIfNone;
    }
  }

  public function recommendGroups($user, $role_id) {
    // @todo
    return '';
  }
}
