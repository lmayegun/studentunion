<?php

namespace Drupal\su_user\Plugin\Tamper;

use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;

/**
 * Plugin implementation for casting to integer.
 *
 * @Tamper(
 *   id = "ucl_info_to_uid",
 *   label = @Translation("Convert UCL info to User ID"),
 *   description = @Translation("This plugin will convert any UPI, user ID, or UCL e-mail to a Drupal user ID."),
 *   category = "User"
 * )
 */
class ConvertUCLInfoToUserID extends TamperBase {

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    $service = \Drupal::service('su_user.ucl_user');
    $user = $service->getUserForUclFieldInput($data);
    return $user ? $user->id() : $data;
  }
}
