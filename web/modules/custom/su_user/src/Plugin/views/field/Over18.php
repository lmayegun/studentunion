<?php

namespace Drupal\su_user\Plugin\views\field;

use DateTime;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\course\Entity\CourseEnrollment;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\Boolean;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get whether the user has a record for a group.
 *
 * @ViewsField("su_user_over_18")
 */
class Over18 extends Boolean {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $user = $this->getEntity($values);

    $studentService = \Drupal::service('su_students.student_services');
    $value = $studentService->isStudentOver18($user);

    if (!empty($this->options['not'])) {
      $value = !$value;
    }

    if (is_null($value)) {
      return 'Unknown';
    }

    if ($this->options['type'] == 'custom') {
      $custom_value = $value ? $this->options['type_custom_true'] : $this->options['type_custom_false'];
      return ViewsRenderPipelineMarkup::create(UtilityXss::filterAdmin($custom_value));
    } elseif (isset($this->formats[$this->options['type']])) {
      return $value ? $this->formats[$this->options['type']][0] : $this->formats[$this->options['type']][1];
    } else {
      return $value ? $this->formats['yes-no'][0] : $this->formats['yes-no'][1];
    }
  }
}
