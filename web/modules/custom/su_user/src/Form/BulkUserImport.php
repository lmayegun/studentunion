<?php

namespace Drupal\su_user\Form;

use Drupal;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\user\Entity\User;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkUserImport extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_user_bulk_import';
  }

  /**
   * Define the fields expected for the CSV.
   *
   * @return array
   *   Array of fields.
   */
  public static function getMapping(): array {
    $fields = [];

    $fields['User ID'] = [
      'description' => 'Numerical ID or other identifying info. Provide if updating users. Leave blank and have action = "create" to automatically assign a new user ID.',
      'example' => '',
    ];

    $fields['Username'] = [
      'description' => 'Their username for logging in',
      'required' => TRUE,
    ];

    $fields['E-mail'] = [
      'description' => 'A full e-mail address',
      'required' => TRUE,
    ];

    $fields['Full name'] = [
      'description' => 'Full name',
    ];

    $fields['Action'] = [
      'description' => '"create" or "update"',
    ];

    $fields['Validate by e-mail'] = [
      'description' => 'TRUE or FALSE to force matching User ID and e-mail',
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $fields = static::getMapping();

    foreach ($fields as $name => $field) {
      if (isset($field['required']) && $field['required'] == TRUE && !isset($row[$name])) {
        static::addError($row, $context, 'Required field not provided: ' . $name);
        return FALSE;
      }
    }

    if (isset($row['User ID'])) {
      $service = Drupal::service('su_user.ucl_user');
      $account = $service->getUserForUclFieldInput($row['User ID']);
      if (!$account) {
        $account = User::load($row['User ID']);
      }

      if (!$account && $row['Action'] == 'update') {
        static::addError($row, $context, 'Cannot find user by User ID to update.');
        return FALSE;
      }

      if ($account && $row['Action'] == 'create') {
        static::addError($row, $context, 'User already exists with that ID.');
        return FALSE;
      }
    }

    $email = $row['E-mail'];
    if ((Drupal::service('email.validator')->isValid($email))) {
      if (!$account) {
        $values = [
          'name' => $email,
          'mail' => $email,
          'pass' => Drupal::service('password_generator')->generate(12),
          'status' => TRUE,
        ];
        $account = User::create($values);
      }
      else {
        if (isset($row['Validate by e-mail']) && $row['Validate by e-mail'] == 'TRUE' && $account->getEmail() != $row['E-mail']) {
          static::addError($row, $context, 'Validating user ID against e-mail failed.');
          return FALSE;
        }
        $account->setEmail($email);
      }
    }
    else {
      static::addError($row, $context, 'Invalid e-mail.');
      return FALSE;
    }

    if (isset($row['Full name']) && $row['Full name'] != '') {
      $account->field_full_name->value = $row['Full name'];
    }

    $account->activate();
    $account->save();

    return TRUE;
  }

}
