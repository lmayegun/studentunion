<?php

namespace Drupal\su_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_user.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('su_user.settings');
    $form['text_for_students_and_members'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text for students and members'),
      '#default_value' => $config->get('text_for_students_and_members'),
    ];
    $form['text_for_non_students'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text for non-students'),
      '#default_value' => $config->get('text_for_non_students'),
    ];
    $form['text_for_union_staff'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text for Union staff'),
      '#default_value' => $config->get('text_for_union_staff'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('su_user.settings')
      ->set('text_for_students_and_members', $form_state->getValue('text_for_students_and_members')['value'])
      ->set('text_for_non_students', $form_state->getValue('text_for_non_students')['value'])
      ->set('text_for_union_staff', $form_state->getValue('text_for_union_staff')['value'])
      ->save();
  }
}
