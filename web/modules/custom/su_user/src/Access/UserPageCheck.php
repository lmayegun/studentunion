<?php

namespace Drupal\su_user\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\user\Entity\User;

/**
 * Checks access for displaying configuration translation page.
 */
class UserPageCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, $user) {
    $account = User::load($account->id());
    return AccessResult::allowedIfHasPermission($account, 'administer users')
      ->orIf(AccessResult::allowedIf($account->hasRole('su_staff')))
      ->orIf(AccessResult::allowedIfHasPermission($account, 'view user information'))
      ->orIf(AccessResult::allowedIf($user == $account->id()));
  }
}
