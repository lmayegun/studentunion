<?php

/**
 * @file
 * Contains \Drupal\su_d7_auth\Controller\DefaultController.
 */

namespace Drupal\su_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;

/**
 * Default controller for the su_d7_auth module.
 */
class D7Redirects extends ControllerBase {

  public function rosa(Request $request) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->isAuthenticated()) {
      \Drupal::logger('su_user')->notice('Drupal 7 redirect to Rosa');
      return new TrustedRedirectResponse("/d7?d=/user/" . $account->id() . "/my-data", 301);
    } else {
      return new TrustedRedirectResponse("/", 301);
    }
  }

  public function courses_training_d7(Request $request) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->isAuthenticated()) {
      \Drupal::logger('su_user')->notice('Drupal 7 redirect to Courses & Training');
      //dd("/d7?d=/user/" . $account->id() . "/courses");
      return new TrustedRedirectResponse("/d7?d=/user/" . $account->id() . "/courses", 301);
    } else {
      return new TrustedRedirectResponse("/", 301);
    }
  }
}
