/**
 * @file
 *
 */

(function ($, Drupal) {
  'use strict';

  function exportColumnToCSV($table, $columns, filename) {

    var $rows = $table.find('tr:has(td)'),

      // Temporary delimiter characters unlikely to be typed by keyboard
      // This is to avoid accidentally splitting the actual contents
      tmpColDelim = String.fromCharCode(11), // vertical tab character
      tmpRowDelim = String.fromCharCode(0), // null character

      // actual delimiter characters for CSV format
      rowDelim = '","',
      colDelim = '"\r\n"',

      // Grab text from table into CSV formatted string
      csv = '"' + $rows.map(function (i, row) {
        var $row = $(row), $cols = $row.find('td');

        return $cols.map(function (j, col) {
          if ($columns.indexOf(j) > -1) {
            var $col = $(col), text = $col.text();

            text = text.replace(/"/g, '""'); // escape double quotes
            text = text.split("*").join(""); // remove asterisk at end
            return text;
          }
        }).get().join(tmpColDelim);

      }).get().join(tmpRowDelim)
        .split(tmpRowDelim).join(rowDelim)
        .split(tmpColDelim).join(colDelim) + '"',

      // Data URI
      csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    if (window.navigator.msSaveBlob) { // IE 10+
      //alert('IE' + csv);
      window.navigator.msSaveOrOpenBlob(new Blob([csv], {type: "text/plain;charset=utf-8;"}), "csvname.csv")
    }
    else {
      downloadDataLink(filename, csvData);
    }
  }

  function exportTableToCSV($table, filename) {

    var $rows = $table.find('tr:has(td),tr:has(th)'),

      // Temporary delimiter characters unlikely to be typed by keyboard
      // This is to avoid accidentally splitting the actual contents
      tmpColDelim = String.fromCharCode(11), // vertical tab character
      tmpRowDelim = String.fromCharCode(0), // null character

      // actual delimiter characters for CSV format
      colDelim = '","',
      rowDelim = '"\r\n"',

      // Grab text from table into CSV formatted string
      csv = '"' + $rows.map(function (i, row) {
        var $row = $(row), $cols = $row.find('td,th');

        return $cols.map(function (j, col) {
          var $col = $(col), text = $col.text();

          return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

      }).get().join(tmpRowDelim)
        .split(tmpRowDelim).join(rowDelim)
        .split(tmpColDelim).join(colDelim) + '"',


      // Data URI
      csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
    console.log(csvData);

    if (window.navigator.msSaveBlob) { // IE 10+
      //alert('IE' + csv);
      window.navigator.msSaveOrOpenBlob(new Blob([csv], {type: "text/plain;charset=utf-8;"}), "csvname.csv")
    }
    else {
      downloadDataLink(filename, csvData);
    }
  }

  function downloadDataLink(filename, csvData) {
    var link = document.createElement('a');
    document.body.appendChild(link); // Firefox requires the link to be in
                                     // the body
    link.download = filename;
    link.href = csvData;
    link.click();
    document.body.removeChild(link); // remove the link when done
  }

  $("#edit-template").on('click', function (event) {
    var $title = $('h1.page-title').text().trim();
    if (!$title) {
      $title = 'Template';
    }
    exportColumnToCSV($('#edit-columns-table'), [0], $title + '.csv');
    event.preventDefault();
    return false;
  });

}(jQuery, Drupal));
