/**
 * @file
 *
 */

(function ($, Drupal) {
  'use strict';
  $(document).ready(function () {
    var link = document.createElement('a');
    document.body.appendChild(link); // Firefox requires the link to be in the body
    link.download = $('.file-download').attr('download');
    link.href = $('.file-download').attr('href');
    link.click();
    document.body.removeChild(link); // remove the link when done
  });

}(jQuery, Drupal));
