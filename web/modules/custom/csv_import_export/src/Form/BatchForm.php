<?php

namespace Drupal\csv_import_export\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\LocalRedirectResponse;

/**
 * Implement Class for batch form.
 *
 * For most uses, just define operations in "setOperations" and then "processRow"
 */
class BatchForm extends FormBase {

  var $totalRows = 0;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_drupal_helper_batch_form';
  }

  /**
   * Pass processBatch an array of operations, chunked if you'd like
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    // Example
    $messagesToSend = [
      'First', 'Second', 'Third'
    ];
    foreach ($messagesToSend as $message) {
      $data[] = ['message' => $message];
    }

    $this->totalRows = count($data);

    // Chunk the array
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 1;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  public function addOperation(&$batch_builder, $method, $data) {
    $batch_builder->addOperation($method, $data);
  }

  /**
   * Process a row
   *
   * @param array $row
   * @param array $context
   * @return void
   */
  public static function processOperation(array $data, array &$context) {
    // Process the row here

    // Example
    \Drupal::messenger()->addMessage(t($data['message']));

    // Add errors via $this->addError($row, $context, $errorText);

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['batch_chunk_size'] = array(
      '#type'          => 'number',
      '#min'           => 1,
      '#max'           => 5000,
      '#default_value' => 50,
      '#title'         => $this->t('Rows to process per operation'),
      '#weight' => 10,
      // '#description'   => $this->t('Every batch call is an HTTP request. So you need to find the perfect blend of
      // how many rows you can process before another HTTP request gets fired.
      // Two things to consider are memory and max execution time (current server: @max seconds). You will want to process as many rows as
      // possible per batch to reduce the number of HTTP request without reaching the max execution time',
      //   [
      //     '@max' => ini_get('max_execution_time'),
      //   ]),
    );

    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Run'),
      '#button_type' => 'primary',
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Processing...'))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'batchFinished']);

    $this->prepareBatch($batch_builder, $form_state);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  /**
   * Execute with batch builder
   *
   * @param [type] $batch_builder
   * @param [type] $array
   * @param [type] $form_state
   * @return void
   */
  public function prepareBatch(&$batch_builder, $form_state) {
  }

  public static function addError(array $row, &$context, string $errorText) {
    if (!isset($context['results']['errors'])) {
      $context['results']['errors'] = [];
    }
    $row = ['Error' => $errorText] + $row;
    $context['results']['errors'][] = $row;
  }

  /**
   * @param       $rows
   * @param array $context
   */
  public function processBatch(array $operations, array &$context) {
    if (empty($context['results'])) {
      $context['results'] = [];
      $context['results']['rows_processed'] = 0;
      $context['results']['rows_total'] = $this->totalRows;
      $context['results']['errors'] = [];
    }

    foreach ($operations as $operation) {
      static::processOperation($operation, $context);
      $context['results']['rows_processed']++;
    }
    // $context['finished'] = $context['results']['rows_processed'] / $context['results']['rows_total'];
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  public function batchFinished($success, $results, $operations) {
    $id = uniqid();

    if ($success) {
      $processed = $results['rows_processed'];
      $errors = count($results['errors']);
      if ($errors > 0) {
        \Drupal::messenger()->addWarning(t('Processed @processed out of @total. There were @errors errors - see downloaded error list for more information.', [
          '@errors' => $errors,
          '@processed' => $processed,
          '@total'     => $results['rows_total'],
        ]));
        return $this->generateErrorReport($results, $operations);
      } else if ($processed != $results['rows_total']) {
        \Drupal::messenger()->addWarning(t('Processed @processed out of @total.', [
          '@processed' => $processed,
          '@total'     => $results['rows_total'],
        ]));
      } else {
        \Drupal::messenger()->addMessage(\Drupal::translation()->formatPlural(
          $processed,
          'Processed 1 row.',
          'Processed @count rows.'
        ));
      }

      // \Drupal::messenger()->addMessage(t('<a class="button" href="@link">Click here to continue</a>', [
      //   '@link' => $_SERVER['HTTP_REFERER']
      // ]));
    } else {
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(t('An error occurred while processing @operation with arguments: @args', [
        '@operation' => $error_operation[0],
        '@args'      => print_r($error_operation[0], TRUE),
      ]));
    }

    $batch = &batch_get();
    $batch_id = $batch && isset($batch['id']) ? $batch['id'] : $this->getFormId();
    $tempstore = \Drupal::service('tempstore.private')->get('csv_import_export');
    $tempstore->set('finish-page-render-' . $batch_id, $this->batchFinishedRenderArray($results));
    if (isset($results['errors']) && count($results['errors']) > 0) {
      $tempstore->set('batch-errors-' . $batch_id, $results['errors']);
    }
    return new LocalRedirectResponse('/batch/' . $batch_id . '/finished');
  }

  public function batchFinishedRenderArray(array $results) {
    return [];
  }

  function generateErrorReport($results, $operations) {
    $filename = $this->getFormId() . strtotime('now');
    return (new BatchDownloadCSVForm)->getFileResponse($filename, 'Error report.csv', $results['errors']);
  }
}
