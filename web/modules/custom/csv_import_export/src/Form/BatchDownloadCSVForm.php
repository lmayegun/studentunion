<?php

namespace Drupal\csv_import_export\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Configure Bookable resources settings for this site.
 */
class BatchDownloadCSVForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'csv_import_export_batch_download_csv_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions'] = [
      '#type' => 'actions',
      '#tree' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Download',
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  public function getBatchTitle($form, $form_state) {
    return $this->t('Exporting...');
  }

  public function getDownloadTitle($form, $form_state) {
    return $this->getBatchTitle($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->getBatchTitle($form, $form_state))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'exportFinished']);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  /**
   * Set the batch operations (including chunking the rows).
   *
   * @param mixed $batch_builder
   *   Batch API builder.
   * @param array $form
   *   Form API form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    // Add operations here
    $rows = [];
    foreach ($rows as $row) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$row]);
    }
  }

  public function addOperation(&$batch_builder, $method, $data) {
    $batch_builder->addOperation($method, $data);
  }

  public function processBatch(array $row, array &$context) {
    $newLines = [];
    foreach ($newLines as $line) {
      $this->addLine($line, $context);
    }
  }

  public function addLine(array $line, &$context) {
    if (!isset($context['results']['lines'])) {
      $context['results']['lines'] = [];
    }

    $context['results']['lines'][] = $line;
  }

  public function filterResults($rows, $results) {
    return $rows;
  }

  public function sortResults($rows, $results) {
    return $rows;
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  public function exportFinished($success, $results, $operations) {
    if ($success && !isset($results['lines'])) {
      \Drupal::messenger()->addError(t('No results found.', []));
    } elseif ($success) {
      $results['lines'] = $this->filterResults($results['lines'], $results);
      $results['lines'] = $this->sortResults($results['lines'], $results);

      $filename = $this->getFormId() . strtotime('now');

      $batch = &batch_get();
      if ($batch) {
        $file_title = $this->getDownloadTitle($batch['form_state']->getCompleteForm(), $batch['form_state']);
      } else {
        $file_title = 'Export download';
      }

      $SESSION['csv_download_redirect'] = $_SERVER['HTTP_REFERER'] ?? '/';
      return $this->getFileResponse($filename, $file_title, $results['lines']);
    } else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      \Drupal::messenger()->addError($message);
    }
  }

  public function getFileResponse($filename, $file_title, $lines) {
    $filepath = 'temporary://';
    $filename = $filename . '.csv';
    $full_path = $filepath . $filename;

    $file = File::create([
      'uid' => \Drupal::currentUser()->id(),
      'filename' => $filename,
      'uri' => $full_path,
      'status' => 1,
    ]);
    $file->setTemporary(TRUE);
    $file->save();

    // Output as CSV
    $uri = \Drupal::service('file_system')->saveData("", $full_path);
    $handle = fopen($uri, 'r+');

    // Headers
    $header = array_keys($lines[0]);
    fputcsv($handle, $header);
    foreach ($lines as $line) {
      fputcsv($handle, array_values($line));
    }
    fclose($handle);

    // Redirect to download page
    $batch = &batch_get();
    $batch_id = $batch && isset($batch['id']) ? $batch['id'] : $this->getFormId();

    /** @var \Drupal\Core\TempStore\PrivateTempStore  $store */
    $tempstore = \Drupal::service('tempstore.private')->get('csv_import_export');
    $tempstore->set('finish-page-render-' . $batch_id, $this->batchFinishedRenderArray($lines));
    $tempstore->set('batch-file-uri-' . $batch_id, $uri);
    $tempstore->set('batch-file-' . $batch_id, $file->id());
    $tempstore->set('batch-file-title-' . $batch_id, $file_title);

    $response = new RedirectResponse("/batch/" . $batch_id . "/finished");
    $response->send();
  }

  public function batchFinishedRenderArray(array $results) {
    return [];
  }
}
