<?php

namespace Drupal\csv_import_export\Form;

use Drupal;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implement Class for import form.
 *
 * Extend this form for a CSV import.
 *
 * For most uses, just define "processRow" to handle each CSV row
 */
class BatchImportCSVForm extends BatchForm {

  var array $array = [];

  /**
   * Convert mapping array to user-friendly list.
   *
   * @return string
   *   HTML string.
   */
  public static function getMappingAsList() {
    $t = '';
    $fields = static::getMapping();
    foreach ($fields as $name => $data) {
      $t .= '<li>';
      $t .= '<b>' . $name . (isset($data['required']) && $data['required'] ? '*' : '') . '</b>: ';
      $t .= ($data['description'] ?? '');
      $t .= (isset($data['example']) && $data['example'] ? ' (e.g. ' . $data['example'] . ')' : '');
      $t .= '</li>';
    }
    return $t;
  }

  /**
   * Define the fields expected for the CSV.
   *
   * @return array
   *   Array of fields.
   */
  public static function getMapping(): array {
    $fields = [];

    return $fields;
  }

  public static function checkRequiredFields(&$record, &$context) {
    $required = static::getRequiredFields();
    foreach ($required as $field => $data) {
      if (!isset($record[$field])) {
        static::addError($record, $context, 'Required field not provided: ' . $field);
        return FALSE;
      }
    }
  }

  public static function getRequiredFields() {
    $required = [];
    foreach (static::getMapping() as $field => $data) {
      if (isset($data['required']) && $data['required'] == TRUE) {
        $required[$field] = $data;
      }
    }
    return $required;
  }

  /**
   * Add an error message for the row when it fails importing.
   *
   * @param array $row
   * @param mixed $context
   * @param string $errorText
   *
   * @return [type]
   */
  public static function addError(array $row, &$context, string $errorText) {
    $row = ['Error' => $errorText] + $row;
    $context['results']['errors'][] = $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (count($this->getMapping()) > 0) {
      $this->addIntroColumnsTable($form);
    }
    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Import CSV File'),
      '#size' => 40,
      '#description' => $this->t('Select the CSV file to be imported.'),
      '#required' => FALSE,
      '#autoupload' => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];

    $form['batch_chunk_size'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 5000,
      '#default_value' => 200,
      '#title' => $this->t('Batch size (reduce if getting timeouts)'),
      // '#description'   => $this->t('Every batch call is an HTTP request. So you need to find the perfect blend of
      // how many rows you can process before another HTTP request gets fired.
      // Two things to consider are memory and max execution time (current server: @max seconds). You will want to process as many rows as
      // possible per batch to reduce the number of HTTP request without reaching the max execution time',
      //   [
      //     '@max' => ini_get('max_execution_time'),
      //   ]),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import and process'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function addIntroColumnsTable(&$form) {
    $rows = [];
    foreach (static::getMapping() as $column_name => $column) {
      $example = $column['example'] ?? '';
      if (!$example) {
        $example = isset($column['allowed_values']) && $column['allowed_values'] ? trim(implode(', ', $column['allowed_values']), ', ') : '';
      }

      $rows[] = [
        $column_name . (isset($column['required']) && $column['required'] ? '*' : ''),
        $column['description'] ?? '',
        $example,
      ];
    }
    $form['columns_table'] = [
      '#type' => 'table',
      '#prefix' => t('<p>Upload a CSV file with the following columns:<ul>'),
      '#header' => [t('Column name'), t('Description'), t('Example')],
      '#rows' => $rows,
      '#weight' => -40,
    ];
    $form['template'] = [
      '#type' => 'button',
      '#value' => $this->t('Download template'),
      '#weight' => -41,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $validators = ['file_validate_extensions' => ['csv']];
    $upload = file_save_upload('file_upload', $validators, FALSE, NULL, FileSystemInterface::EXISTS_REPLACE);
    $file = $upload ? $upload[0] : FALSE;
    if (!$file) {
      return FALSE;
    }
    $array = $this->convertCSVtoArray($file->getFileUri());

    if (!$array) {
      return FALSE;
    }

    $array = $this->addExtraColumns($array, $form, $form_state);

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Importing...'))
      ->setProgressMessage('Processed @current out of @total operations.')
      ->setFinishCallback([$this, 'importFinished']);

    $this->array = $array;

    $this->prepareBatch($batch_builder, $form_state);

    $this->setOperations($batch_builder, $form, $form_state);

    batch_set($batch_builder->toArray());
  }

  /**
   * @param string $filename
   * @param string $delimiter
   *
   * @return array|FALSE
   */
  public function convertCSVtoArray($filename = '', $delimiter = ',') {
    if (!file_exists($filename) || !is_readable($filename)) {
      return FALSE;
    }

    $header = NULL;
    $data = [];

    if (($handle = fopen($filename, 'r')) !== FALSE) {
      // Loop through rows
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
        if (!$header) {
          $header = $row;
        }
        else {
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }

    $results = [];
    foreach ($data as $row) {
      // Remove BOM characters from first column https://stackoverflow.com/questions/32184933/remove-bom-%C3%AF-from-imported-csv-file
      $firstColumn = array_keys($row)[0];
      if (substr($firstColumn, 0, 3) == chr(hexdec('EF')) . chr(hexdec('BB')) . chr(hexdec('BF'))) {
        $row[substr($firstColumn, 3)] = $row[$firstColumn];
        unset($row[$firstColumn]);
      }
      $results[] = $row;
    }

    return $results;
  }

  public function addExtraColumns($array, $form, $form_state) {
    return $array;
  }

  /**
   * @param mixed $batch_builder
   * @param mixed $form_state
   *
   * @return [type]
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $this->totalRows = count($this->array);

    // Chunk the array:
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 1;
    $chunks = array_chunk($this->array, $chunk_size, TRUE);

    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

  /**
   * @param       $rows
   * @param array $context
   */
  public function processBatch(array $rows, array &$context) {
    if (empty($context['results'])) {
      $context['results'] = [];
      $context['results']['rows_processed'] = 0;
      $context['results']['rows_total'] = $this->totalRows;
      $context['results']['errors'] = [];
    }

    foreach ($rows as $row) {
      static::processRow($row, $context);
      $context['results']['rows_processed'] = $context['results']['rows_processed'] + 1;
    }
  }

  /**
   * Process a row
   *
   * @param array $row
   * @param array $context
   *
   * @return void
   */
  public static function processRow(array $row, array &$context) {
    // Process the row here

    // Add errors via static::addError($row, $context, $errorText);
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  public function importFinished($success, $results, $operations) {
    if ($success) {
      $processed = $results['rows_processed'];
      $total = $results['rows_total'];
      $errors = count($results['errors']);

      if ($errors > 0) {
        Drupal::messenger()
          ->addWarning(t('Processed @processed out of @total. There were @errors errors - see downloaded error list for more information.', [
            '@errors' => $errors,
            '@processed' => $processed,
            '@total' => $total,
          ]));
        return $this->generateErrorReport($results, $operations);
      }
      else {
        if ($processed != $results['rows_total']) {
          Drupal::messenger()
            ->addWarning(t('Processed @processed out of @total.', [
              '@processed' => $processed,
              '@total' => $total,
            ]));
        }
        else {
          Drupal::messenger()->addMessage(Drupal::translation()->formatPlural(
            $processed,
            'Processed 1 row.',
            'Processed @count rows.'
          ));
        }
      }
    }
    else {
      $error_operation = reset($operations);
      Drupal::messenger()
        ->addError(t('An error occurred while processing @operation with arguments: @args', [
          '@operation' => print_r($error_operation[0], TRUE),
          '@args' => print_r($error_operation[0], TRUE),
        ]));
    }
  }

  /**
   * Output a CSV file with errors column.
   *
   * @param mixed $results
   *   Results context array.
   * @param mixed $operations
   *   Operations.
   */
  public function generateErrorReport($results, $operations) {
    $filename = $this->getFormId() . strtotime('now');
    return (new BatchDownloadCSVForm)->getFileResponse($filename, 'Error report.csv', $results['errors']);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'csv_import_export_batch_import_csv_form';
  }

}
