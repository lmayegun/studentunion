<?php

namespace Drupal\csv_import_export\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BatchFinishedController extends ControllerBase {

  public function render($batch = NULL) {
    $tempstore = \Drupal::service('tempstore.private')->get('csv_import_export');

    $output = [];

    $file_path = NULL;
    if ($tempstore->get('batch-file-uri-' . $batch)) {
      $file_path = \Drupal::service('file_url_generator')->generateAbsoluteString($tempstore->get('batch-file-uri-' . $batch));
    } elseif ($tempstore->get('batch-file-' . $batch)) {
      $file = File::load($tempstore->get('batch-file-' . $batch));
      $file_path = $file->createFileUrl(FALSE);
    }

    $file_title = $tempstore->get('batch-file-title-' . $batch) ?: 'Download';
    if ($file_path) {
      $output[] = [
        'download' => [
          '#markup' => '<a class="btn button file-download" href="' . $file_path . '" download="' . $file_title . '.csv">Download "' . $file_title . '"</a>',
          '#attached' => [
            'library' => [
              'csv_import_export/finished'
            ],
          ],
        ],
      ];
    }

    $result = $tempstore->get('finish-page-render-' . $batch);
    if ($result && count($result) > 0 && count($result) < 500) {
      $output[] = $result;
    }

    // If fewer than 500 rows, show on page.
    $errors = $tempstore->get('batch-errors-' . $batch);
    if ($errors && count($errors) > 0 && count($errors) < 500) {
      $output[] = [
        '#type' => 'table',
        '#prefix' => '<h2>Errors</h2>',
        '#header' => array_keys($errors[0]),
        '#rows' => $errors,
      ];
    }

    if ($output) {
      return $output;
    } else {
      return [];
    }
  }
}
