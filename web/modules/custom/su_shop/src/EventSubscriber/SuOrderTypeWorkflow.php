<?php

namespace Drupal\su_shop\EventSubscriber;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\State;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sends an email when the order transitions to Fulfillment.
 */
class SuOrderTypeWorkflow implements EventSubscriberInterface
{

  use StringTranslationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Constructs a new OrderFulfillmentSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Mail\MailManagerInterface         $mail_manager
   *   The mail manager.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    MailManagerInterface $mail_manager
  ) {
    $this->languageManager = $language_manager;
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
    return [
      'commerce_order.place.post_transition' => ['changeStatus'],
    ];
  }

  /**
   * Change status
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @var Order                                                 $order
   */
  public function changeStatus(WorkflowTransitionEvent $event)
  {
    $order = $event->getEntity();

    if ($order->bundle() == 'pos') {
      return;
    }

    $shipping = FALSE;
    $shipping_product_types = ['shipping_item'];
    /**
     * @var OrderItem $order_item
     */
    foreach ($order->getItems() as $order_item) {
      /**
       * Bundle is the 'order item type' admin/commerce/config/order-item-types
       *
       * @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchased_entity
       */
      if (in_array($order_item->bundle(), $shipping_product_types)) {
        $shipping = TRUE;
      }
    }

    if ($shipping) {
      $order->getState()->applyTransitionById('process');
    } else {
      $order->getState()->applyTransitionById('completed');
    }
    $order->save();
  }
}
