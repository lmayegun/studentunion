<?php

namespace Drupal\su_shop\EventSubscriber;

use Dompdf\Dompdf;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderItemEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_promotion\CouponCodePattern;
use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\commerce_refund_order_item\Event\OrderRefundedEvent;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TCPDF;

/**
 * Sends an email when the order transitions to Fulfillment.
 */
class OrderPlacedSendVouchers implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => ['generateAndSendVouchers'],

      'commerce_order.refund.order_item' => ['onOrderRefund', 100],
      'commerce_order.commerce_order.predelete' => ['onOrderCancelOrDelete', 100],
      'commerce_order.cancel.post_transition' => ['onOrderCancelOrDeleteWorkflow', 100],

      'commerce_order.commerce_order_item.predelete' => ['onOrderItemDelete', 100]
    ];
  }

  /**
   * Change status
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @var Order                                                 $order
   */
  public function generateAndSendVouchers(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();

    $expiryLength = 60 * 60 * 24 * 90;
    $expiry = $order->getPlacedTime() + $expiryLength;

    $vouchers = [];

    if ($order->getData('vouchers_generated')) {
      return;
    }

    /**
     * @var OrderItem $order_item
     */
    foreach ($order->getItems() as $order_item) {
      /**
       * @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchased_entity
       */
      $purchased_entity = $order_item->getPurchasedEntity();

      if ($purchased_entity->getProduct()->bundle() == 'voucher') {
        $vouchersToSend = $order_item->getQuantity();
        $voucherAttribute = $purchased_entity->attribute_voucher_value->entity->label();
        $voucherValue = new Price((int) filter_var($voucherAttribute, FILTER_SANITIZE_NUMBER_INT), 'GBP');

        $promotion = $this->generatePromotion($order_item, $purchased_entity->bundle(), $voucherValue, $expiry);
        if (!$promotion) {
          continue;
        }

        $codes = $this->generateCoupons($promotion, $vouchersToSend);
        if (!isset($vouchers[$voucherAttribute])) {
          $vouchers[$voucherAttribute] = [];
        }
        $vouchers[$voucherAttribute] = $codes;

        $order_item->field_voucher_promotion->entity = $promotion;
        $order_item->save();
      }
    }

    if (count($vouchers) > 0) {
      $order->setData('vouchers_generated', TRUE);
      $order->save();

      $promotion->set('require_coupon', TRUE);
      $promotion->save();

      $this->sendEmail($order, $vouchers, $expiry);

      $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');
      $message = 'Voucher(s) generated and sent to customer at ' . $order->getEmail() . ' - ';
      foreach ($vouchers as $amount => $codes) {
        $message .= ' [' . $amount . '] ' . implode(', ', $codes) . ' ';
      }
      $commerceLog->generate($order, 'order_comment', ['comment' => $message])->save();
    }
  }

  public function sendEmail($order, $vouchers, $expiry) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'su_shop';
    $key = 'email_vouchers';
    $to = $order->getEmail();
    $params['headers'] = ['Cc' => 'su.shop@ucl.ac.uk'];

    //@todo generate PDFs
    $attachments = NULL;
    foreach ($vouchers as $amount => $codes) {
      foreach ($codes as $code) {
        $filepath = $this->generatePDF($amount, $code, $expiry);
        if ($filepath) {
          $attachments[] = ['filepath' => $filepath];
        }
      }
    }

    $params['title'] = 'The Hanger voucher codes (order ' . $order->getOrderNumber() . ')';

    $params['message'] = 'Hello,<br><br>Following your order ' . $order->getOrderNumber() . ', please see below the coupon codes for the vouchers requested.<br><br>';
    if ($attachments && count($attachments) > 0) {
      $params['message'] = 'The same codes are also attached as PDF vouchers.<br><br>';
    }
    $params['message'] = 'Customers can enter these during online checkout, or quote them at the till in the Hanger shop.<br><br>';
    $params['message'] = 'Note the coupons can only be used once and expire 90 days from issuing, i.e. <b>' . \Drupal::service('date.formatter')->format($expiry, 'long') . '</b>.<br><br>';
    $params['message'] .= '<table><tr><th>Amount</th><th>Coupon code</th></tr>';
    foreach ($vouchers as $amount => $codes) {
      foreach ($codes as $code) {
        $params['message'] .= '<tr><td>' . $amount . '</td><td>' . $code . '</td></tr>';
      }
    }
    $params['message'] .= '</table>';

    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;

    return $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
  }

  public function generatePromotion(OrderItemInterface $order_item, string $variation_type, Price $voucherValue, $expiry) {
    // Hanger vouchers only for now
    if ($variation_type != 'voucher') {
      return NULL;
    }

    $values = [
      'name' => 'Voucher coupons for order ID ' . $order_item->getOrder()->id() . ' (order number ' . $order_item->getOrder()->getOrderNumber() . ', order item ' . $order_item->id() . ')',
      'display_name' => t('The Hanger voucher'),
      'order_types' => [
        0 => [
          'target_id' => 'default',
        ],
        1 => [
          'target_id' => 'pos',
        ],
      ],
      'end_date' => [
        0 => gmdate(
          DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
          $expiry
        ),
      ],
      'offer' => [
        0 => [
          'target_plugin_id'  => 'order_fixed_amount_off',
          'target_plugin_configuration' => [
            'amount' => [
              'number' => $voucherValue->getNumber(),
              'currency_code' => $voucherValue->getCurrencyCode(),
            ],
          ],
        ],
      ],
      'conditions' => [
        0 => [
          'target_plugin_id' => 'hanger_order',
          'target_plugin_configuration' => [
            'product_types' => [],
          ],
        ],
      ],
    ];
    $promotion = Promotion::create($values);
    $promotion->save();
    return $promotion;
  }

  public function generateCoupons(Promotion $promotion, int $quantity) {
    $pattern = new CouponCodePattern('alphanumeric', '', '', 10);
    $coupon_code_generator = \Drupal::service('commerce_promotion.coupon_code_generator');
    $codes = $coupon_code_generator->generateCodes($pattern, $quantity);

    if (!empty($codes)) {
      $coupon_storage = \Drupal::entityTypeManager()->getStorage('commerce_promotion_coupon');

      foreach ($codes as $code) {
        $coupon_values = [
          'code' => $code,
          'promotion_id' => $promotion->id(),
          'usage_limit' => 1,
          'usage_limit_customer' => 0,
        ];
        $coupon = $coupon_storage->create($coupon_values);
        $coupon->save();

        $promotion->addCoupon($coupon);
        $promotion->save();
      }
      return $codes;
    } else {
      return NULL;
    }
  }

  public function generatePDF(string $amountLabel, string $code, $expiry) {
    $expiryFormatted = \Drupal::service('date.formatter')->format($expiry, 'long');

    $pdf = new TCPDF('L', 'mm', 'A4');
    // $pdf

    return NULL;
  }


  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param OrderRefundedEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderRefund(OrderRefundedEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $itemsWithQuantity = $event->getItemsWithQuantity();

    foreach ($itemsWithQuantity as $orderItemId => $quantityRefunded) {
      if ($quantityRefunded == 0) {
        continue;
      }
      $orderItem = OrderItem::load($orderItemId);
      $this->removeCouponsForOrderItem($orderItem);
    }
  }

  public function onOrderCancelOrDelete(OrderEvent $event) {
    $items = $event->getOrder()->getItems();
    foreach ($items as $orderItem) {
      $this->removeCouponsForOrderItem($orderItem);
    }
  }

  public function onOrderCancelOrDeleteWorkflow(WorkflowTransitionEvent $event) {
    $items = $event->getEntity()->getItems();
    foreach ($items as $orderItem) {
      $this->removeCouponsForOrderItem($orderItem);
    }
  }

  /**
   * @param OrderItemEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderItemDelete(OrderItemEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $orderItem = $event->getOrderItem();
    $this->removeCouponsForOrderItem($orderItem);
  }

  /**
   * Disable promotion and coupons.
   *
   * @param mixed $orderItem
   * @param  $quantity
   *
   * @return [type]
   */
  public function removeCouponsForOrderItem($orderItem, $quantity = FALSE) {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = $orderItem->getPurchasedEntity();
    if (!$variation) {
      return;
    }

    if ($variation->bundle() !== 'voucher') {
      return;
    }

    $promotion = $orderItem->field_voucher_promotion->entity;
    if (!$promotion) {
      return;
    }
    $coupons = $promotion->getCoupons();

    // If we're refunding all coupons, disable the promotion.
    if (is_null($quantity) || $quantity >= count($coupons)) {
      $promotion->setEnabled(FALSE);
      $promotion->save();
    }

    // Disable any unused coupons up until the quantity, or we run out of coupons.
    $quantityToRefund = $quantity ? $quantity : count($coupons);
    $quantityRefunded = 0;
    foreach ($coupons as $coupon) {
      if (!$coupon->available()) {
        continue;
      }

      if ($quantityRefunded < $quantityToRefund) {
        $coupon->setEnabled(FALSE);
        $coupon->save();
        $quantityRefunded++;
      }
    }
  }
}
