<?php

namespace Drupal\su_shop\EventSubscriber;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Event\LocalStockTransactionEvent;
use Drupal\commerce_stock_local\Event\LocalStockTransactionEvents;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\State;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sends an email when the order transitions to Fulfillment.
 */
class SuStock implements EventSubscriberInterface
{

    use StringTranslationTrait;

    /**
     * The language manager.
     *
     * @var \Drupal\Core\Language\LanguageManagerInterface
     */
    protected $languageManager;

    /**
     * Constructs a new OrderFulfillmentSubscriber object.
     *
     * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
     *   The language manager.
     * @param \Drupal\Core\Mail\MailManagerInterface         $mail_manager
     *   The mail manager.
     */
    public function __construct(
        LanguageManagerInterface $language_manager
    ) {
        $this->languageManager = $language_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            LocalStockTransactionEvents::LOCAL_STOCK_TRANSACTION_INSERT => 'onTransactionInsert',
        ];
    }

    /**
     * @param \Drupal\commerce_stock_local\Event\LocalStockTransactionEvent $event
     *
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    public function onTransactionInsert(LocalStockTransactionEvent $event)
    {
        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
        $variation = $event->getEntity();
        if ($variation->hasField('field_stock_cached')) {
            $stockManager = \Drupal::getContainer()->get('commerce_stock.service_manager');
            $stockLevel = $stockManager->getStockLevel($variation);
            $variation->set('field_stock_cached', $stockLevel);
            $variation->save();
        }
    }
}
