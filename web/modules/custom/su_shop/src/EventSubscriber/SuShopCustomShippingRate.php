<?php

namespace Drupal\su_shop\EventSubscriber;

use Drupal\commerce_shipping\Event\FilterShippingMethodsEvent;
use Drupal\commerce_shipping\Event\ShippingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SuShopCustomShippingRate
 *
 * @package Drupal\su_shop\EventSubscriber
 */
class SuShopCustomShippingRate implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ShippingEvents::FILTER_SHIPPING_METHODS => ['filterMethod', 100],
    ];
  }

  /**
   * if the only method are click and collect and custom, custom will be display otherwise unset it
   * 1 => click and collect
   * 16 => custom shipping
   *
   * @param ShippingEvents $event
   */
  public function filterMethod(FilterShippingMethodsEvent $event) {
    $methods = $event->getShippingMethods();
    if (count($methods) !== 2) {
      // if (isset($methods[16])) {
      //   unset($methods[16]);
      // }
    }
  }
}
