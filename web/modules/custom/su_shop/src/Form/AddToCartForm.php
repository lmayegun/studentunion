<?php

namespace Drupal\su_shop\Form;

use Drupal\commerce_cart\Form\AddToCartForm as FormAddToCartForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the order item add to cart form.
 *
 * https://docs.drupalcommerce.org/commerce2/developer-guide/products/displaying-products/code-recipes
 */
class AddToCartForm extends FormAddToCartForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // $form['']
    // @todo add link to size guide

    return $form;
  }

}
