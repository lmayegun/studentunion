<?php

namespace Drupal\su_shop\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ShopSettingsForm.
 */
class ShopSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_shop.shopsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_shop_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('su_shop.shopsettings');

    $form['hanger_open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('The Hanger online shop is open'),
      '#description' => $this->t('You must enter a "closed message" below.'),
      '#default_value' => $config->get('hanger_open') ?? TRUE,
    ];

    $form['hanger_closed_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message shown on shop and products when closed.'),
      '#description' => $this->t('Include dates where possible, and a contact address / instruction. This is shown on the main page, and each product page.'),
      '#default_value' => $config->get('hanger_closed_message'),
    ];

    $form['hanger_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message shown on main shop page. Leave blank to show none.'),
      '#default_value' => $config->get('hanger_message'),
    ];

    $form['order_receipt_email'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Order receipt e-mail text'),
      '#description' => $this->t('Shown at top of order receipt e-mail. Do not include information about how to access the order via the profile, this is included already.'),
      '#default_value' => $config->get('order_receipt_email'),
    ];

    $form['order_receipt_email_event'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Order receipt e-mail text for orders including events'),
      '#description' => $this->t('Shown at top of order receipt e-mail.'),
      '#default_value' => $config->get('order_receipt_email_event'),
    ];

    $form['order_receipt_email_shipping_item'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Order receipt e-mail text for orders including shipping (i.e. from the Union shop, the Hanger)'),
      '#description' => $this->t('Shown at top of order receipt e-mail.'),
      '#default_value' => $config->get('order_receipt_email_shipping_item'),
    ];

    $form['order_receipt_email_membership'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Order receipt e-mail text for orders including Club/Society memberships'),
      '#description' => $this->t('Shown at top of order receipt e-mail.'),
      '#default_value' => $config->get('order_receipt_email_membership'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('su_shop.shopsettings')
      ->set('hanger_open', $form_state->getValue('hanger_open'))
      ->set('hanger_closed_message', $form_state->getValue('hanger_closed_message')['value'])
      ->set('hanger_message', $form_state->getValue('hanger_message')['value'])
      ->set('order_receipt_email', $form_state->getValue('order_receipt_email')['value'])
      ->set('order_receipt_email_event', $form_state->getValue('order_receipt_email_event')['value'])
      ->set('order_receipt_email_shipping_item', $form_state->getValue('order_receipt_email_shipping_item')['value'])
      ->save();
  }
}
