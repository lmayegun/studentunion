<?php

namespace Drupal\su_shop\Form;

use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\user\Entity\User;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkCancelOrders extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_shop_bulk_cancel_order';
  }

  /**
   * Define the fields expected for the CSV.
   *
   * @return array
   *   Array of fields.
   */
  public static function getMapping(): array {
    $fields = [];

    $fields['Order ID'] = [
      'description' => 'Required.',
      'example' => '',
      'required' => TRUE,
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $fields = static::getMapping();

    foreach ($fields as $name => $field) {
      if (isset($field['required']) && $field['required'] == TRUE && !isset($row[$name])) {
        static::addError($row, $context, 'Required field not provided: ' . $name);
        return FALSE;
      }
    }

    if (Order::load($row['Order ID'])) {
      $order = Order::load($row['Order ID']);
      if ($order->getState()->isTransitionAllowed('cancel')) {
        $order->getState()->applyTransitionById('cancel');
        $order->save();
      }
      else {
        static::addError($row, $context, 'Cannot apply cancel transition to order ' . $order->id());
        return FALSE;
      }

      /** @var CommerceLogServiceProvider $commerceLog * */
      $commerceLog = Drupal::entityTypeManager()->getStorage('commerce_log');

      $account = User::load(Drupal::currentUser()->id());
      $message = t('Order cancelled by bulk import - user ' . $account->getEmail() . ' (' . $account->id() . ')', []);

      $commerceLog->generate($order, 'order_comment', ['comment' => $message])
        ->save();

      return TRUE;
    }
    else {
      static::addError($row, $context, 'Order ID not found.');
      return FALSE;
    }
  }

}
