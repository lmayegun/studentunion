<?php

namespace Drupal\su_shop\Form;

use DateTime;
use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\user\Entity\User;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportOrders extends BatchImportCSVForm {

  const ALLOWED_VARIATIONS = 3;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_shop_bulk_import_orders';
  }

  /**
   * Define the fields expected for the CSV.
   *
   * @return array
   *   Array of fields.
   */
  public static function getMapping(): array {
    $fields = [];

    $fields['Order ID'] = [
      'description' => 'Required if "update"',
      'example' => '',
    ];

    $fields['User ID'] = [
      'description' => 'An e-mail, numeric user ID or UCL user ID.',
      'example' => 'uczxmke, uczxmke@ucl.ac.uk or m.keeble.21@ucl.ac.uk',
      'required' => TRUE,
    ];

    $fields['Date'] = [
      'description' => '',
      'dataFormat' => 'Y-m-d H:i:s',
      'example' => '2021-08-30 02:04:29',
      'required' => TRUE,
    ];

    $fields['Total price'] = [
      'description' => '',
      'example' => '',
      'required' => TRUE,
    ];

    $fields['Keep unpaid'] = [
      'description' => 'Enter TRUE to prevent marking the order as paid (total paid will remain 0) - use if existing payment exists',
      'example' => '',
    ];

    $fields['Action'] = [
      'description' => '"create" or "update" or "add order item" - if not update, trying to re-use an order ID will throw an error for that row',
      'example' => '',
    ];

    for ($x = 1; $x <= static::ALLOWED_VARIATIONS; $x++) {
      $fields['Variation ID ' . $x] = [
        'description' => '',
        'example' => '',
        'required' => $x == 1,
      ];
      $fields['Variation quantity ' . $x] = [
        'description' => '',
        'example' => '',
        'required' => $x == 1,
      ];
      $fields['Variation price ' . $x] = [
        'description' => '',
        'example' => '',
        'required' => $x == 1,
      ];
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $fields = static::getMapping();

    foreach ($fields as $name => $field) {
      if (isset($field['required']) && $field['required'] == TRUE && !isset($row[$name])) {
        static::addError($row, $context, 'Required field not provided: ' . $name);
        return FALSE;
      }
    }

    $service = Drupal::service('su_user.ucl_user');
    $account = $service->getUserForUclFieldInput($row['User ID']);
    if (!$account) {
      $account = User::load($row['User ID']);
    }

    if (!$account) {
      static::addError($row, $context, 'Cannot find user by ' . $row['User ID']);
      return FALSE;
    }

    $action = $row['Action'];

    $date = DateTime::createFromFormat($fields['Date']['dataFormat'], $row['Date']);
    if (!$date) {
      static::addError($row, $context, 'Cannot work out date from that format. Needs to be ' . $fields['Date']['dataFormat']);
      return FALSE;
    }
    $timestamp = $date->getTimestamp();

    if (Order::load($row['Order ID'])) {
      if ($action == 'update' || $action == 'add order item') {
        $order = Order::load($row['Order ID']);
      }
      else {
        static::addError($row, $context, 'Order number already exists and action is not "update".');
        return FALSE;
      }
    }
    else {
      if ($row['Action'] == 'create') {

        // @todo avoid duplicate order numbers
        $orderNumber = $row['Order ID'];

        $order = Order::create([
          'order_id' => $row['Order ID'],
          'type' => 'default',
          'order_number' => $orderNumber,
          'mail' => $account->getEmail(),
          'store_id' => 1,
          'total_price' => new Price($row['Total price'], 'GBP'),
          'total_paid' => isset($row['Keep unpaid']) && $row['Keep unpaid'] == 'TRUE' ? NULL : new Price($row['Total price'], 'GBP'),
          'state' => 'completed',
          'created' => $timestamp,
          'changed' => $timestamp,
          'placed' => $timestamp,
          'completed' => $timestamp,
          'uid' => $account->id(),
          'payment_gateway' => 'worldnet_gateway',
        ]);
        $order->save();
      }
      else {
        static::addError($row, $context, 'Order does not exist and action is not "create".');
        return FALSE;
      }
    }

    $existingVariationIds = [];
    $existingOrderItems = $order->getItems();
    foreach ($existingOrderItems as $item) {
      $existingVariationIds[] = $item->getPurchasedEntityId();
    }

    $addToPaid = 0;
    for ($x = 1; $x <= static::ALLOWED_VARIATIONS; $x++) {
      if (!isset($row['Variation ID ' . $x])) {
        continue;
      }

      $variationID = $row['Variation ID ' . $x];
      if (!$variationID) {
        continue;
      }
      $variation = ProductVariation::load($variationID);

      if (!$variation) {
        static::addError($row, $context, 'Cannot find variation ' . $x);
        return FALSE;
      }

      if (in_array($variationID, $existingVariationIds)) {
        static::addError($row, $context, 'Order already has order item with variation ' . $x);
        return FALSE;
      }

      $orderItem = OrderItem::create([
        'type' => $variation->bundle() == 'event' ? 'event' : 'shipping_item',
        'order_id' => $order->id(),
        'purchased_entity' => $variation->id(),
        'title' => $variation->getTitle(),
        'quantity' => $row['Variation quantity ' . $x],
        'unit_price' => new Price($row['Variation price ' . $x], 'GBP'),
        'total_price' => new Price($row['Variation price ' . $x], 'GBP'),
        'created' => $timestamp,
        'changed' => $timestamp,
      ]);
      $orderItem->save();
      $order->addItem($orderItem);

      $addToPaid += $row['Variation price ' . $x];
    }
    $order->recalculateTotalPrice();
    $paid = new Price($order->total_paid->value['number'] + $addToPaid, 'GBP');
    $order->total_paid->value = $paid;
    $order->save();

    /** @var CommerceLogServiceProvider $commerceLog * */
    $commerceLog = Drupal::entityTypeManager()->getStorage('commerce_log');

    $message = t('Order imported from Drupal 7 (old website). Some data only available there.', []);

    $commerceLog->generate($order, 'order_comment', ['comment' => $message])
      ->save();

    return TRUE;
  }

}
