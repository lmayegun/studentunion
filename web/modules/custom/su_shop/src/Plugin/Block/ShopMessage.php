<?php

namespace Drupal\su_shop\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\Entity\User;

/**
 * Show a message (customisable in settings) for org leaders.
 *
 * @Block(
 *   id = "su_shop_message_block",
 *   admin_label = @Translation("Shop (The Hanger) message"),
 *   category = @Translation("The Hanger"),
 * )
 */
class ShopMessage extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::routeMatch()->getRouteName() != 'view.shop_directory.page_1') {
      $product = \Drupal::service('su_drupal_helper')->getRouteEntity();
      if (!$product || !$product->bundle()) {
        return [];
      }
      if (!in_array($product->bundle(), ['clothing'])) {
        return [];
      }
    }

    $config = \Drupal::config('su_shop.shopsettings');

    $message = NULL;
    $hangerOpen = \Drupal::config('su_shop.shopsettings')->get('hanger_open');
    if (!$hangerOpen && $config->get('hanger_closed_message') && $config->get('hanger_closed_message') != '') {
      $message = \Drupal::config('su_shop.shopsettings')->get('hanger_closed_message');
    } elseif ($config->get('hanger_message') && $config->get('hanger_message') != '') {
      $message = $config->get('hanger_message');
    }
    if ($message) {
      return [
        '#markup' => Markup::create('<div class="messages messages--info">' . $message . '</div>'),
      ];
    }
  }

  public function getCacheTags() {
    $fields = [
      'config:su_shop',
      'config:su_shop.shopsettings',
    ];
    return Cache::mergeTags(parent::getCacheTags(), $fields);
  }
}
