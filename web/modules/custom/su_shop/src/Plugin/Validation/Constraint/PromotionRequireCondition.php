<?php

namespace Drupal\su_shop\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "PromotionRequireCondition",
 *   label = @Translation("Require a condition for commerce promotions", context = "Validation"),
 *   type = "string"
 * )
 */
class PromotionRequireCondition extends Constraint {

  /**
   * The message that will be shown if the value is not an integer.
   *
   * @var string
   */
  public $noCondition = 'Promotion %value does not have a condition. To avoid promotions applying to all orders on the site, you are required to provide a condition in the conditions section.';
}
