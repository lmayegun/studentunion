<?php

namespace Drupal\su_shop\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class PromotionRequireConditionValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($commerce_promotion, Constraint $constraint) {
    if (count($commerce_promotion->getConditions()) == 0) {
      $this->context->addViolation($constraint->noCondition, ['%value' => $commerce_promotion->label()]);
    }
  }
}
