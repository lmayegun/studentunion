<?php

namespace Drupal\su_shop\Plugin\MailoutType;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\mailouts\Plugin\MailoutType\EntityMailoutBase;

/**
 * Plugin implementation of the mailout_type.
 *
 * @MailoutType(
 *   id = "purchasers",
 *   label = @Translation("Product purchasers"),
 *   description = @Translation("Send mail to product purchasers, optionally by variation.")
 * )
 */
class CommerceProductPurchasers extends EntityMailoutBase {
  public $entityTypeId = 'commerce_product';

  public function routesForActions() {
    return [
      // 'entity.commerce_product.canonical' => t('Send mailout to customers')
      'view.event_sales.page_1' => t('Send mailout to customers')
    ];
  }

  public function routesForTasks() {
    return [
      // 'entity.commerce_product.canonical' => t('Mailouts to customers')
    ];
  }

  public function access(AccountInterface $account, array $params = NULL) {
    $parent = parent::access($account, $params);
    if (!$parent->allowed()) {
      return $parent;
    }

    if (!isset($params['entity_id'])) {
      return AccessResult::neutral();
    }

    $commerce_product = Product::load($params['entity_id']);
    return su_events_sales_access($account, $commerce_product);
  }

  public function addFormOptions(&$form, FormStateInterface $form_state, $params = NULL) {
    parent::addFormOptions($form, $form_state, $params);

    $entity_id = $params['entity_id'] ?? NULL;
    $commerce_product = Product::load($entity_id);

    $options = [];
    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      if (!$variation) {
        continue;
      }
      $options[$variation->id()] = $variation->label();
    }

    if (count($variations) > 0) {
      $states = [];
      $form['variations'] = [
        '#type' => 'checkboxes',
        '#title' => t("Select the variations for which to e-mail the purchasers"),
        '#options' => $options,
        '#default_value' => count($variations) == 1 ? [reset($variations)->id()] : [],
        '#multiple' => TRUE,
        '#states' => $states,
        '#required' => TRUE,
      ];
    } else {
      $form['#access'] = FALSE;
    }

    $form['include_product_details'] = [
      '#type' => 'checkbox',
      '#title' => t("Include product information at top of e-mail"),
      '#description' => t("i.e. name of product and link to product page"),
      '#default_value' => TRUE,
    ];
  }

  public function getRecipientsFromForm($form, FormStateInterface $form_state, $entity_id = NULL) {
    $variation_ids = $form_state->getValue('variations');

    // Get mail for all completed orders containing this variation:
    $query = \Drupal::database()->select('commerce_order_item', 'i');
    $query->join('commerce_order', 'o', 'i.order_id = o.order_id');
    $query->fields('o', ['mail'])
      ->condition('i.purchased_entity', $variation_ids, 'IN')
      ->condition('i.quantity', 0, ">")
      ->condition('o.state', 'completed')
      ->distinct();
    $mails = $query->execute()->fetchCol();
    return $mails;
  }

  public function getConfirmationText() {
    return t('I confirm this e-mail relates to the purchased product only and is appropriate for its audience.');
  }

  public function getBody(&$form, FormStateInterface $form_state) {
    $variation_ids = $form_state->getValue('variations');

    $variation = ProductVariation::load(reset($variation_ids));
    $product = $variation->getProduct();
    $product_url = $product->toLink()->toString();
    $product_name = '';
    return '<p><b>Note:</b> the following e-mail relates to your purchase of ' . $product_url . ' from Students\' Union UCL.</p><hr>' . $form_state->getValue('body')['value'];
  }
}
