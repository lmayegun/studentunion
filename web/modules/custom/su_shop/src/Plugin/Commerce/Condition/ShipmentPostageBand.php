<?php

namespace Drupal\su_shop\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce_product\Plugin\Commerce\Condition\ProductTrait;
use Drupal\commerce_product\Plugin\Commerce\Condition\VariationTypeTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the quantity condition for shipments.
 *
 * @CommerceCondition(
 *   id = "shipment_postage_band",
 *   label = @Translation("Shipment postage band"),
 *   category = @Translation("Shipping"),
 *   entity_type = "commerce_order",
 * )
 */
class ShipmentPostageBand extends ConditionBase
{

  use ProductTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
        'postage_band' => '>',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);

    $operator = $this->configuration['postage_band'];

    $form['postage_band'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Postage band'),
      '#options'       => $this->getPostageBands(),
      '#default_value' => $operator,
      '#required'      => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['postage_band'] = $values['postage_band'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity)
  {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    foreach ($order->getItems() as $order_item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchased_entity */
      $purchased_entity = $order_item->getPurchasedEntity();
      if ($purchased_entity->hasField('field_postage_band')) {
        $field = $purchased_entity->get('field_postage_band')->getValue();
        $variation_band = !empty($field) ? $purchased_entity->get('field_postage_band')->getValue()[0]['target_id'] : FALSE;
        if (!$purchased_entity || $purchased_entity->getEntityTypeId() != 'commerce_product_variation') {
          continue;
        }
        if (isset($this->configuration['postage_band']) && $this->configuration['postage_band'] === $variation_band) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Gets the comparison operators.
   *
   * @return array
   *   The comparison operators.
   */
  protected function getPostageBands()
  {
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('postage_band');
    $options = [];
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    return $options;
  }

}
