<?php

namespace Drupal\su_shop\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce_product\Plugin\Commerce\Condition\OrderProductType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the product type condition for orders.
 *
 * @CommerceCondition(
 *   id = "hanger_order",
 *   label = @Translation("Hanger order"),
 *   display_label = @Translation("Order is a Hanger order (only contains Hanger products)"),
 *   category = @Translation("The Hanger"),
 *   entity_type = "commerce_order",
 * )
 */
class HangerOrder extends OrderProductType {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);

    $hangerOrFree = 0;

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    foreach ($order->getItems() as $order_item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchased_entity */
      $purchased_entity = $order_item->getPurchasedEntity();
      if (!$purchased_entity || $purchased_entity->getEntityTypeId() != 'commerce_product_variation') {
        continue;
      }

      $product_type = $purchased_entity->getProduct()->bundle();
      $product_type_hanger = in_array($product_type, ['clothing']);
      if ($product_type_hanger) {
        $hangerOrFree++;
        continue;
      }

      $free = $order_item->getAdjustedTotalPrice()->getNumber() == 0;
      if ($free) {
        $hangerOrFree++;
        continue;
      }
    }


    $allPaidAreHanger = count($order->getItems()) == $hangerOrFree;
    return $allPaidAreHanger;
  }
}
