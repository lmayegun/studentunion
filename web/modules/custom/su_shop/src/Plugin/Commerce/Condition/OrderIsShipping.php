<?php

namespace Drupal\su_shop\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce_product\Plugin\Commerce\Condition\ProductTrait;
use Drupal\commerce_product\Plugin\Commerce\Condition\VariationTypeTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the quantity condition for shipments.
 *
 * @CommerceCondition(
 *   id = "shipment_exists",
 *   label = @Translation("Order has shipping"),
 *   category = @Translation("Shipping"),
 *   entity_type = "commerce_order",
 * )
 */
class OrderIsShipping extends ConditionBase
{
  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity)
  {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    $adjustments = $order->getAdjustments();

    foreach ($adjustments as $adjustment) {
      if ($adjustment->getType() === 'shipping') {
        return true;
      }
    }

    return FALSE;
  }
}
