<?php

namespace Drupal\su_shop\Plugin\Commerce\EmailEvent;

use Drupal\commerce_email\Plugin\Commerce\EmailEvent\EmailEventBase;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 * @CommerceEmailEvent(
 *   id = "shipment_ready_to_collect",
 *   label = @Translation("Shipment ready to collect"),
 *   event_name = "commerce_shipment.ready_for_collection.post_transition",
 *   entity_type = "commerce_shipment",
 * )
 */
class ShipmentReadyToCollect extends EmailEventBase {

  /**
   * {@inheritdoc}
   */
  public function extractEntityFromEvent($event) {
    assert($event instanceof WorkflowTransitionEvent);
    return $event->getEntity();
  }
}
