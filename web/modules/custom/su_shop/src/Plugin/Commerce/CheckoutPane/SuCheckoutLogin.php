<?php

namespace Drupal\su_shop\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Login;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the login pane.
 *
 * @CommerceCheckoutPane(
 *   id = "su_login",
 *   label = @Translation("Login or continue as guest, including UCL login"),
 *   default_step = "login",
 * )
 */
class SuCheckoutLogin extends Login {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);

    $pane_form['ucl_customer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('UCL student or staff - login or create account'),
      '#attributes' => [
        'class' => [
          'form-wrapper__login-option',
          'form-wrapper__ucl-customer',
        ],
      ],
      '#weight' => -50,
    ];

    $pane_form['ucl_customer']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Log in via UCL'),
      '#op' => 'login-ucl',
      '#attributes' => [
        'formnovalidate' => 'formnovalidate',
      ],
      '#limit_validation_errors' => [
        array_merge($pane_form['#parents'], ['ucl_customer']),
      ],
      '#submit' => [],
    ];

    $pane_form['ucl_customer']['submit2'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Union account via UCL'),
      '#op' => 'login-ucl',
      '#attributes' => [
        'formnovalidate' => 'formnovalidate',
      ],
      '#limit_validation_errors' => [
        array_merge($pane_form['#parents'], ['ucl_customer']),
      ],
      '#submit' => [],
    ];

    $pane_form['returning_customer']['#title'] = 'Returning customer (non-UCL accounts)';

    $pane_form['register']['#title'] = 'New customer (not UCL student or staff)';

    return $pane_form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $triggering_element = $form_state->getTriggeringElement();
    $trigger = !empty($triggering_element['#op']) ? $triggering_element['#op'] : 'continue';

    if ($trigger == 'login-ucl') {

      // Set login cookie for OAuth so we go to the right checkout pane
      $nextStep = Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $this->order->id(),
        'step' => $this->checkoutFlow->getNextStepId($this->getStepId()),
      ]);
      $loginService = \Drupal::service('su_login.destination_cookie_service');
      $loginService->setCookie($nextStep->toString());

      // Store a value in the order so we can assign owner after login.
      // see su_shop_user_login()
      $hash = md5(\Drupal::time()->getRequestTime() . $this->order->id());
      $_SESSION['order_owner_key'] = [
        'order_id' => $this->order->id(),
        'hash' => $hash,
        'next_checkout_step' => $this->checkoutFlow->getNextStepId($this->getStepId()),
      ];
      $this->order->setData('order_owner_key', $hash);
      $this->order->save();

      // Get URL for OAuth and redirect to it
      $config = \Drupal::config('su_login_oauth2.adminsettings');
      $urlRaw = su_login_oauth2_get_auth_url($config);
      $url = Url::fromUri($urlRaw);

      \Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', $urlRaw);

      return new TrustedRedirectResponse($urlRaw);
    } else {
      parent::submitPaneForm($pane_form, $form_state, $complete_form);
    }
  }
}
