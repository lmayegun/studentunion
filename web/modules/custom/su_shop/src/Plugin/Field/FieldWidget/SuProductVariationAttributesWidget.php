<?php

namespace Drupal\su_shop\Plugin\Field\FieldWidget;

use Drupal\commerce_product\Plugin\Field\FieldWidget\ProductVariationAttributesWidget;
use Drupal\commerce_product\ProductAttributeFieldManagerInterface;
use Drupal\commerce_product\ProductVariationAttributeMapperInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commerce_product_variation_attributes' widget.
 *
 * @FieldWidget(
 *   id = "su_shop_commerce_product_variation_attributes",
 *   label = @Translation("Product variation attributes for SU Shop"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SuProductVariationAttributesWidget extends ProductVariationAttributesWidget {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    /** @var \Drupal\commerce_stock\StockServiceManagerInterface $stockManager */
    $stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    $currentVariation = $items->referencedEntities() ? $items->referencedEntities()[0] : $items->referencedEntities();
    $product = $currentVariation->getProduct();
    $variations = $product->getVariations();
    $locations = StockLocation::loadMultiple();
    $attribute = FALSE;
    if (count($element['attributes']['attribute_colour']['#options']) == 0) {
      $attribute = 'attribute_size';
    }
    if (count($element['attributes']['attribute_size']['#options']) == 0) {
      $attribute = 'attribute_colour';
    }

    if ($attribute) {
      $options = $element['attributes'][$attribute]['#options'];
      foreach ($options as $key => $attributeLabel) {
        $inStock = TRUE;
        foreach ($variations as $variation) {
          if ($variation->$attribute->entity->label() == $attributeLabel) {
            $checker = $stockServiceManager->getService($variation)->getStockChecker();
            if (!$checker->getIsInStock($variation, $locations)) {
              $inStock = FALSE;
            }
          }
        }

        if (!$inStock) {
          $element['attributes'][$attribute]['#options'][$key] = $attributeLabel . ' [SOLD OUT]';
        }
      }
    }

    return $element;
  }
}
