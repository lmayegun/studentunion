<?php

namespace Drupal\su_shop\Plugin\Field\FieldFormatter;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_price\Plugin\Field\FieldFormatter\PriceDefaultFormatter as FieldFormatterPriceDefaultFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commerce_price_default' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_price_with_free",
 *   label = @Translation("Default with text for 'Free'"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class PriceWithFreeFormatter extends FieldFormatterPriceDefaultFormatter
{

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $options = $this->getFormattingOptions();
        $elements = [];
        foreach ($items as $delta => $item) {

            $formatted = $item->number == 0 ? 'Free' : $this->currencyFormatter->format($item->number, $item->currency_code, $options);
            $elements[$delta] = [
                '#markup' => $formatted,
                '#cache' => [
                    'contexts' => [
                        'languages:' . LanguageInterface::TYPE_INTERFACE,
                        'country',
                    ],
                ],
            ];
        }

        return $elements;
    }
}
