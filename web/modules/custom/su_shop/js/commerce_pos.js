/**
 * @file
 * commerce_pos UI behaviors.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Order Item input:
   * Chooses first autocomplete option when 'Enter' is pressed.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the condition summaries.
   */
  Drupal.behaviors.catchBarcodeScanner = {
    attach: function (context, settings) {
      // $('input.form-autocomplete').on('blur', function () {
      //   console.log('catchBarcodeScanner blur');
      //   // $(this).trigger("keydown");
      //   $('input.form-autocomplete')
      //     .keypress(function (event) {
      //       if (event.which == 13) {
      //         // $(this).trigger("change");
      //         console.log('enter');
      //       };
      //     });
      // });
    }
  }
}(jQuery, Drupal, drupalSettings));
