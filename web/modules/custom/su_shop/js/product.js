/**
 * @file
 * Condition UI behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Prevent the original selection being an out of stock product.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the condition summaries.
   */
  Drupal.behaviors.dealWithOutOfStock = {
    attach: function (context, settings) {
      $(document).ajaxComplete(function (event, xhr, settings) {
        // Do this only once
        $(document, context).once('dealWithOutOfStock').each(function () {
          console.log('dealWithOutOfStock');
          var select = '[name^="purchased_entity[0][attributes]"]';
          var options = select + ' option';
          var optionsReversed = $(select + ' option').get().reverse();
          var firstNotSoldOut = 1;
          var resetForm = false;
          $(options).each(function () {
            if ($(this).html().indexOf('SOLD OUT') == -1) {
              firstNotSoldOut = $(this).attr('value');
              resetForm = true;
              return false;
            }
          });

          if (resetForm) {
            console.log('dealWithOutOfStock resetForm');
            $(select).val(firstNotSoldOut);
            $(select).trigger("change");
            $(select).trigger("chosen:updated");
          }
        });
      });
    }
  };
}(jQuery, Drupal));
