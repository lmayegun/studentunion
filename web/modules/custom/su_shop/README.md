## Migrating shipping methods from d7 to d8

In drupal 7 shipping methods are build with rules. We created a custom rule that detects the basket size depending
on the postage band, quantity of products and weight.

However, in drupal 8 we have created a shipping condition to set if the shipping method applies depending if the
products have a certain postage band. The rest of the conditions, quantity and weight are by default in drupal.

The following code is the one we used to detect the basket size in drupal 7 and that would help up to recreate the
shipping methods in drupal 8. Remember shipping methods is not configuration and cannot be exported.

    /**
     * Basket measurements rules
     *
     *  Small
     *  - single small item of Band A only
     *
     *  Medium
     *  - up to 4 < 500g items; or 1-2 items which include 1 Band B item
     *
     *  Large
     *  - more than two items where at least one is > 500g; or more than 4 items of any kind

     switch ($postage_band) {
           case 'small':
             if ($quantity == 1 && $product_shipping_band == 'A') {
               return TRUE;
             }
             break;
           case 'medium':
             if (($quantity <= 4 && $product_over_weight < 1) || (($quantity == 1 || $quantity == 2) && $product_band_b)) {
               return TRUE;
             }
             break;
           case 'large':
             if ($quantity > 4 || ($quantity > 2 && $product_over_weight >= 1)) {
               return TRUE;
             }
             break;
           default:
             return FALSE;
             break;
         }
     */

## Drupal 8 configuration

Small = Shipping quantity equals 1 AND (All conditions must pass) Shipment postage band A

Medium = Shipment weight LESS THAN 1KG AND (All conditions must pass) Shipment quantity LESS THAN OR EQUAL TO 4

Large = Shipment weight GREATER THAN OR EQUAL TO 1KG OR (Only one condition must pass) Shipment quantity GREATER THAN 2
