<?php

namespace Drupal\workflows_diagram\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\workflows\WorkflowInterface;
use Exception;

/**
 * Returns responses for Workflows Diagram routes.
 */
class WorkflowsDiagramController extends ControllerBase {

  /**
   * Permissible orientations for the flowchart.
   *
   * https://mermaid-js.github.io/mermaid/#/./flowchart?id=flowchart-orientation
   */
  const DIAGRAM_ORIENTATIONS = [
    'TB', // Top to bottom
    'TD', // Top to bottom (top down)
    'BT', // Bottom to top
    'LR', // Left to right
    'RL', // Right to left
  ];

  /**
   * Produce a Mermaid compatible string from a workflow.
   *
   * @param WorkflowInterface $workflow
   *   Workflow object.
   * @param array|null $classes
   *   Classes to apply per state or transition,
   *   keyed by 'transitions' and 'states'.
   * @param string|null $title
   *   Flowchart title.
   * @param array|null $disabled
   *   IDs of states or transitions to exclude from diagram,
   *   keyed by 'transitions' and 'states'.
   * @param string $orientation
   *   One of static DIAGRAM_ORIENTATIONS, e.g.
   *   TB top to bottom, BT bottom to top,
   *   LR left to right, RL right to left
   *
   * @return string
   *   Mermaid-compatible string.
   */
  public static function convertWorkflowToMermaid(WorkflowInterface $workflow, array $classes = NULL, string $title = NULL, array $disabled = NULL, string $orientation = 'TB'): string {
    $workflowType = $workflow->getTypePlugin();
    $states = $workflowType->getStates();

    // @todo mermaid currently doesn't support a title directly.
    // This may change: https://github.com/mermaid-js/mermaid/issues/1433
    //    if (is_null($title)) {
    //      $title = $workflow->label();
    //    }

    // Set an orientation if the one given is not valid
    if (!$orientation || !in_array($orientation, static::DIAGRAM_ORIENTATIONS)) {
      $orientation = 'TB';
    }

    // We collect classes to add to the end:
    $mermaid_classes = '';

    // Start mermaid output string with graph type:
    $mermaid = 'graph ' . $orientation . ';';

    // Initial state, put at start:
    try {
      $initialState = $workflowType->getInitialState();
    } catch (Exception $ex) {
      $initialState = NULL;
    }
    if ($initialState) {
      unset($states[$initialState->id()]);
      $states = [$initialState] + $states;
    }

    // States:
    foreach ($states as $state) {
      // Do not render state if disabled:
      if ($disabled && isset($disabled['states']) && in_array($state->id(), $disabled['states'])) {
        continue;
      }

      // Add state to mermaid diagram:
      $mermaid .= $state->id() . '["' . static::convertToMermaidString($state->label()) . '"];';

      // Add classes to mermaid diagram:
      if ($classes && isset($classes['states']) && isset($classes['states'][$state->id()])) {
        foreach ($classes['states'][$state->id()] as $class) {
          $mermaid_classes .= 'class ' . $state->id() . ' ' . $class . ';';
        }
      }
    }

    // Transitions:
    $transitions = $workflowType->getTransitions();
    foreach ($transitions as $transition) {
      // Do not render transition if disabled:
      if ($disabled && isset($disabled['transitions']) && in_array($transition->id(), $disabled['transitions'])) {
        continue;
      }

      foreach ($transition->from() as $from_transition) {
        $from = $from_transition->id();
        $to = $transition->to()->id();

        // Do not render transition if any of the involved states are disabled:
        if ($disabled && isset($disabled['states']) && count(array_intersect($disabled['states'], [
            $from,
            $to,
          ])) > 0) {
          continue;
        }

        // Create transition arrow in mermaid:
        $mermaid .= $from . '--->' . '|"' . static::convertToMermaidString($transition->label()) . '"|' . $to . ';';

        // @todo not sure how to refer to transitions with this, may not be possible:
        // if ($classes && isset($classes['transitions']) && isset($classes['transitions'][$transition->id()])) {
        //   foreach ($classes['transitions'][$transition->id()] as $class) {
        //     $mermaid_classes .= 'class ' . $transition->id() . ' ' . trim($class) . ';';
        //   }
        // }
      }
    }

    // To avoid hitting "end" issue with a state or transition named 'end':
    $mermaid = str_replace(';end[', ';End[', $mermaid);
    $mermaid = str_replace(';end-', ';End-', $mermaid);
    $mermaid = str_replace('|end|', '|End|', $mermaid);
    $mermaid = str_replace('|end;', '|End;', $mermaid);

    // Add classes to end of mermaid string:
    $mermaid .= $mermaid_classes;

    return $mermaid;
  }

  /**
   * Convert a string into a safe Mermaid string.
   *
   * @param string $string
   *   String to convert.
   *
   * @return string
   *   Safe mermaid string.
   */
  public static function convertToMermaidString(string $string): string {
    return str_replace('"', '#quot;', $string);
  }

}
