Display a flow chart representation of a core Workflows module workflow.

Uses Mermaid JS for rendering: https://github.com/mermaid-js/mermaid

By default this shows the diagram in a details box on the workflow edit form.

The template can also be used wherever the render API works, e.g. in a controller:

    // We load the workflow:
    $workflow = Workflow::load('basic_approval');

    // We can add CSS classes to states and transitions:
    $classes = [
      'states' => [
        'approved' => 'workflow-state-green',
        'rejected' => 'workflow-state-red',
      ],
      'transitions' => [
        'approve' => 'workflow-transition-green',
        'reject' => 'workflow-transition-red',
      ],
    ];

    // We can disable states or transitions,
    // if they are not used for a specific usage of the workflow:
    $disabled = [
      'states' => [
        'queried'
      ],
      'transitions' => [
        'query',
        'refer_to_higher_level',
      ],
    ];

    // Optionally set the orientation of the diagram, here to left-to-right.
    // https://mermaid-js.github.io/mermaid/#/./flowchart?id=flowchart-orientation
    orientation = 'LR';

    $build['diagram'] = [
      '#theme'    => 'workflows_diagram',
      '#workflow' => $workflow,
      '#classes'  => $classes,
      '#disabled' => $disabled,
      '#orientation' => orientation,
    ];
