<?php

namespace Drupal\su_autocomplete;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityAutocompleteMatcherInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * This matcher class is using default matcher.
 *
 * It use default matcher class as dependency to have default functionality
 * and include matches by entity id.
 *
 * @see \Drupal\Core\Entity\EntityAutocompleteMatcherInterface
 */
class EntityAutocompleteMatcher implements EntityAutocompleteMatcherInterface {

  /**
   * The autocomplete matcher for entity references.
   *
   * @var \Drupal\Core\Entity\EntityAutocompleteMatcherInterface
   */
  protected $matcher;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The current logged in user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity reference selection handler plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionManager;

  /**
   * Constructs a EntityIdAutocompleteMatcher object.
   *
   * @param \Drupal\Core\Entity\EntityAutocompleteMatcherInterface $matcher
   *   The autocomplete matcher for entity references.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The entity reference selection handler plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current logged in user.
   */
  public function __construct(EntityAutocompleteMatcherInterface $matcher, SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, AccountInterface $current_user) {
    $this->matcher = $matcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->currentUser = $current_user;
    $this->selectionManager = $selection_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    if (empty($string)) {
      return [];
    }

    $overrides = ['user', 'group'];
    if (isset($string) && in_array($target_type, $overrides)) {
      $matches = $this->getMatchesForType($target_type, $selection_handler, $selection_settings, $string);
    }

    if (isset($matches) && $matches) {
      return $matches;
    }

    return $this->matcher->getMatches($target_type, $selection_handler, $selection_settings, $string);
  }

  /**
   * Check access to autocomplete results by entity id.
   */
  protected function access() {
    return true;
  }

  public function getMatchesForType($target_type, $selection_handler, $selection_settings, $string = '') {
    // Check access
    if ($target_type == 'user' && !$this->currentUser->hasPermission('view user ucl basic information')) {
      // echo 'NO PERMISSION';
      return null;
    }

    // Setup
    $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;

    // Get results for different types
    if ($target_type == 'user') {
      $fields = ['field_full_name', 'name', 'field_upi', 'field_ucl_user_id', 'mail'];
      $query = $this->getQueryUser($selection_settings, $match_limit, $fields, $string);
    } elseif ($target_type == 'group') {
      $fields = ['label', 'type', 'field_vol_organisation_type', 'field_officer_type'];
      $query = $this->getQueryEntity($target_type, $selection_settings, $match_limit, $fields, $string);
    }

    if (!$query) {
      // echo 'NO QYERY';
      return [];
    }

    $result = $query->execute();
    if (empty($result)) {
      // echo 'NO RESULT';
      return [];
    }

    // Get labels
    $entity_labels = [];
    $entities = \Drupal::entityTypeManager()->getStorage($target_type)->loadMultiple($result);
    foreach ($entities as $entity_id => $entity) {
      $bundle = $entity->bundle();
      $plain = Html::escape($this->entityRepository->getTranslationFromContext($entity)
        ->label());
      $key = "$plain ($entity_id)";

      // Loop through the entities and convert them into autocomplete output.
      $entity_labels[$bundle][$entity->id()] = [
        'label' => $this->getEntityLabel($entity, $fields),
        'key' => $key
      ];
    }

    return $this->convertToMatches($entity_labels);
  }

  // Loop through the entities and convert them into autocomplete output.
  public function convertToMatches($entity_labels) {
    $matches = [];
    foreach ($entity_labels as $values) {
      foreach ($values as $entity_id => $key_label) {
        $key = $key_label['key'];
        $label = $key_label['label'];

        // Strip things like starting/trailing white spaces, line breaks and
        // tags.
        $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(strip_tags($key))));

        // Names containing commas or quotes must be wrapped in quotes.
        $key = Tags::encode($key);

        $matches[] = ['value' => $key, 'label' => $label];
      }
    }
    return $matches;
  }

  public function getEntityLabel(EntityInterface $entity, $fields = null) {
    if ($fields) {
      $labels = [];
      foreach ($fields as $field) {
        $labels[] = !empty($entity->$field->value) ? $entity->$field->value : '';
      }
    } else {
      $labels = [$entity->label()];
    }
    $labels = array_filter($labels);
    return implode(' | ', $labels);
  }

  public function getQueryEntity($target_type, $selection_settings, $match_limit, $fields, $string) {
    $query = \Drupal::entityQuery($target_type);

    $db = Database::getConnection();

    // print_r($selection_settings["filter"]);
    // $selected_roles = [];
    // foreach ($selection_settings["filter"]["bundle"] as $key => $role) {
    //   if (!empty($role)) {
    //     $selected_roles[] = $role;
    //   }
    // }
    // if (!empty($selected_roles)) {
    //   $query->condition('roles', $selected_roles, 'IN');
    // }

    // Match by any of the fields
    $orGroup = $query->orConditionGroup();
    foreach ($fields as $field) {
      $orGroup->condition($field, '%' . $db->escapeLike($string) . '%', 'like');
    }
    $query->condition($orGroup);

    // // Add entity-access tag.
    // $query->addTag($target_type . '_access');

    // // Add the Selection handler for system_query_entity_reference_alter().
    // $query->addTag('entity_reference');
    // $query->addMetaData('entity_reference_selection_handler', $this);

    $query->range(0, $match_limit);
    return $query;
  }

  public function getQueryUser($selection_settings, $match_limit, $fields, $string) {
    $query = $this->getQueryEntity('user', $selection_settings, $match_limit, $fields, $string);

    $query->condition('status', TRUE);

    // Check selected roles.
    $selected_roles = [];
    if (isset($selection_settings["filter"]) && isset($selection_settings["filter"]["role"])) {
      foreach ($selection_settings["filter"]["role"] as $key => $role) {
        if (!empty($role)) {
          $selected_roles[] = $role;
        }
      }
      if (!empty($selected_roles)) {
        $query->condition('roles', $selected_roles, 'IN');
      }
    }
    return $query;
  }
}
