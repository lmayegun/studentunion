<?php

namespace Drupal\group_mailing_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_mailing_lists\Entity\Mailout;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MailoutForm extends FormBase {
  /**
   * @return string
   */
  public function getFormId() {
    return 'group_mailing_lists_mailout';
  }

  public static function getFilterFields(&$form, $form_state, $group = null) {
    $user = User::load(\Drupal::currentUser()->id());
    if (!$group) {
      if ($user->hasPermission('send global group mailouts')) {
        $enableRoles = true;
        $enableTypes = true;
      } else {
        return false;
      }
    } else {
      $enableRoles = $group->hasPermission('send group mailout by group role', $user);
      $enableTypes = $group->hasPermission('send group mailout by record type', $user);
      if (!$enableRoles && !$enableTypes) {
        return false;
      }
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    if ($form_state->get('allowed_group_types') && count($form_state->get('allowed_group_types')) > 0) {
      $types = $gmrRepositoryService->getTypesInUse(null, null, $form_state->get('allowed_group_types'));
    } else {
      $types = $gmrRepositoryService->getTypesInUse(null, $group);
    }

    if ($enableTypes && $enableRoles) {
      $form['filter_type'] = [
        '#type' => 'select',
        '#title' => "Filter by...",
        '#options' => [
          'types' => 'Membership types',
          'roles' => 'Group roles',
        ],
        '#default_value' => 'types',
        '#multiple' => FALSE,
      ];
    }

    if ($enableTypes) {
      $options = [];
      foreach ($types as $type) {
        $type = GroupMembershipRecordType::load($type);
        if (!$type) {
          continue;
        }
        $options[$type->id()] = $type->label();
      }
      if (count($types) > 0) {
        $states = [];
        if ($enableRoles) {
          $states = [
            'invisible' => array(
              ':input[name="filter_type"]' => array('value' => 'roles'),
            ),
            'disabled' => array(
              ':input[name="filter_type"]' => array('value' => 'roles'),
            )
          ];
        }
        $form['types'] = [
          '#type' => 'checkboxes',
          '#title' => "Membership types to e-mail",
          '#options' => $options,
          '#default_value' => [],
          '#multiple' => true,
          '#states' => $states
        ];
      }
    }

    if ($enableRoles) {
      $options = [];

      if ($form_state->get('allowed_group_types')) {
        $group_types = $form_state->get('allowed_group_types');
      } else {
        $group_types = $group ? [$group->bundle()] : null;
      }
      $roles = $gmrRepositoryService->getAllNonInternalRoles($group_types);
      foreach ($roles as $role) {
        $options[$role->id()] = $role->label();
      }

      if (count($roles) > 0) {
        $states = [];
        if ($enableTypes) {
          $states = [
            'invisible' => array(
              ':input[name="filter_type"]' => array('value' => 'types'),
            ),
            'disabled' => array(
              ':input[name="filter_type"]' => array('value' => 'types'),
            )
          ];
        }
        $form['roles'] = [
          '#type' => 'checkboxes',
          '#title' => "Group roles",
          '#options' => $options,
          '#default_value' => [],
          '#multiple' => true,
          '#states' => $states
        ];
      }
    }
    return true;
  }

  /**
   * Weight; Used to sort the list of form elements
   * before being output; lower numbers appear before higher numbers.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|RedirectResponse
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null) {

    $user = \Drupal::currentUser();

    $email = $group->field_email->value ?? $user->getEmail();

    $filterable = $this->getFilterFields($form, $form_state, $group);

    $form['from_email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail address for replies'),
      '#required' => TRUE,
      '#default_value' => $email
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#required' => TRUE,
      '#default_value' => ''
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('E-mail body'),
      '#required' => TRUE,
      '#format' => 'e_mail_html',
      '#allowed_formats' => ['e_mail_html'],
      '#default_value' => ''
    ];

    $form['data'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I confirm this e-mail relates to group activities, is appropriate for its audience and does not reflect poorly on the group and/or the Union.'),
      "#required"      => true
    ];

    if ($group) {
      $form_state->set('group', $group->id());
    }

    if ($filterable) {
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => 'Send',
        '#button_type' => 'primary',
      ];
    } else {
      return false;
    }
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array                                $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\group_mailing_lists\Service\MailingListService $service */
    $service = \Drupal::service('group_mailing_lists.mailing_list');

    $group = Group::load($form_state->get('group'));
    $filter_type = $form_state->getValue('filter_type');
    $tos = $service->getEmailsFromForm($group, true, $form_state);

    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'group_mailing_lists';
    $key = 'group_mailing_lists_mailout';
    $params = [
      'from_email' => $form_state->getValue('from_email'),
      'from_name' => $group->label(),
      'subject' => $form_state->getValue('subject'),
      'body' => $form_state->getValue('body')['value'],
    ];
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;
    foreach ($tos as $to) {
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    }
    $mailout = Mailout::create([
      'user_id' => \Drupal::currentUser()->id(),
      'body' => ['value' => $params['body'], 'format' => 'e_mail_html'],
      'subject' => $params['subject'],
      'from_email' => $params['from_email'],
      'recipients' => $tos,
      'recipients_count' => count($tos),
      'group' => $group,
      'created' => strtotime('now'),
      'types' => GroupMembershipRecordType::loadMultiple($form_state->getValue('types')),
      'roles' => GroupRole::loadMultiple($form_state->getValue('roles'))
    ]);
    $mailout->save();

    $params['subject'] = '[MAILOUT COPY]: ' . $params['subject'];
    $result = $mailManager->mail($module, $key, $params['from_email'], $langcode, $params, NULL, $send);

    \Drupal::messenger()->addMessage("Sent to " . count($tos) . " recipients.");
  }

  public static function getTitle(GroupInterface $group = null) {
    return 'Send mailout for ' . $group->label();
  }
}
