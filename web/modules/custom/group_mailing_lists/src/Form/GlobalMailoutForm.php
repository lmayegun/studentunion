<?php

namespace Drupal\group_mailing_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group\Entity\GroupType;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_mailing_lists\Entity\Mailout;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GlobalMailoutForm extends MailoutForm
{
    /**
     * @return string
     */
    public function getFormId()
    {
        return 'group_mailing_lists_mailout_global';
    }

    /**
     * Weight; Used to sort the list of form elements
     * before being output; lower numbers appear before higher numbers.
     *
     * @param array                                $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return array|RedirectResponse
     */
    public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null)
    {
        if ($queryString = \Drupal::request()->query->get('group_types')) {
            $allowed_group_types = explode(',', $queryString);
            array_filter($allowed_group_types);
            $form_state->set('allowed_group_types', $allowed_group_types);
        } else {
            $allowed_group_types = null;
        }

        $groupTypes = GroupType::loadMultiple($allowed_group_types ?? null);
        $options = [];
        foreach ($groupTypes as $type_id => $type) {
            $options[$type_id] = $type->label();
        }
        $form['group_types'] = [
            '#type' => 'checkboxes',
            '#title' => "Group type(s) to send to",
            '#options' =>  $options,
            '#default_value' =>  $allowed_group_types ?? [],
        ];
        $form = parent::buildForm($form, $form_state, null);
        return $form;
    }

    public static function getTitle(GroupInterface $group = null)
    {
        return 'Send global mailout for groups';
    }
}
