<?php

namespace Drupal\group_mailing_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SignupForm extends FormBase
{
    /**
     * @return string
     */
    public function getFormId()
    {
        return 'group_mailing_lists_signup';
    }

    /**
     * Weight; Used to sort the list of form elements
     * before being output; lower numbers appear before higher numbers.
     *
     * @param array                                $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return array|RedirectResponse
     */
    public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null)
    {
        honeypot_add_form_protection($form, $form_state, array('honeypot', 'time_restriction'));

        $user = \Drupal::currentUser();
        $email = $user->getEmail();
        if (isset($_SESSION['signup_email'])) {
            $email = $_SESSION['signup_email'];
        }

        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Your e-mail address'),
            '#required' => TRUE,
            '#default_value' => $email
        ];
        $form['markup'] = [
            '#markup' => '<p class="terms">By submitting this form, you agree your e-mail address will be provided to this group to contact you about the group\'s activities. It will not otherwise be shared or used by the Union.</p>'
        ];
        if ($group->bundle() == 'club_society') {
            $form['markup_c&s'] = [
                '#markup' => '<p class="terms">Note this is not the same as joining the club/society - you can join via their Club/Society page at ' . $group->toLink()->toString() . ' when memberships are on sale.</p>'
            ];
        }

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => 'Sign up',
            '#button_type' => 'primary',
        ];

        $form_state->set('group', $group->id());

        return $form;
    }

    /**
     * Form submission handler.
     *
     * @param array                                $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $email = $form_state->getValue('email');
        $_SESSION['signup_email'] = $email;

        $user = \Drupal::currentUser();
        if (\Drupal::currentUser()->isAnonymous()) {
            // Check mail, name
            $user = user_load_by_mail($email);
            if (!$user) {
                $user = user_load_by_name($email);
            }
            if ($user) {
                $uid = $user->id();
            } else {
                $uid = 0;
                // @todo could create a user account?
            }
        } else {
            $uid = $user->id();
        }

        $form_state->getValue('site_name');
        $record = GroupMembershipRecord::create([
            'type' => 'sign_up',
            'group_id' => $form_state->get('group'),
            'group_role_id' => '',
            'user_id' => $uid,
            'date_range' => [
                'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, \Drupal::time()->getRequestTime()),
                'end_value' => ''
            ],
            'field_signup_email' => $email,
            'field_ip' => $_SERVER['REMOTE_ADDR']
        ]);
        $record->save();


        \Drupal::messenger()->addMessage("Sign-up saved. Look forward to being contacted!", 'status');
    }

    public static function getTitle(GroupInterface $group = null)
    {
        return 'Sign up to ' . $group->label();
    }
}
