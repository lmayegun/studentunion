<?php

namespace Drupal\group_mailing_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MailingListForm extends BatchDownloadCSVForm {
  /**
   * @return string
   */
  public function getFormId() {
    return 'group_mailing_lists_download';
  }

  /**
   * Weight; Used to sort the list of form elements
   * before being output; lower numbers appear before higher numbers.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|RedirectResponse
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null) {
    $form['markup'] = [
      '#markup' => '<p>You can download a mailing list here of the current e-mail addresses that are subscribed to the group (e.g. by becoming a member, or via QR code signup).</p>'
    ];
    $form['markup2'] = [
      '#markup' => '<p>You can also download a list of addresses that have unsubscribed, to help remove them from any mailing lists you may have. <b>Under no circumstances can these addresses be contacted.</b></p>'
    ];

    $filterable = MailoutForm::getFilterFields($form, $form_state, $group);

    $form['data'] = [
      '#type' => 'fieldset',
      '#title' => 'Data protection agreements',
      '#description'   => 'For more information see our <a href="https://studentsunionucl.org/data-protection-and-privacy-policy" target="_blank">Data protection and privacy policy</a> or contact <a href="mailto:su.data-protection@ucl.ac.uk">su.data-protection@ucl.ac.uk</a>.',
    ];

    $form['data']['use'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to use the data only to support group activities, as outlined in our data protection and privacy policy.'),
      "#required"      => true
    ];

    $form['data']['share'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree not to share this file with any other students, officers, UCL staff or unless explicitly permitted by our data protection policy.'),
      "#required"      => true
    ];

    $form['data']['delete'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('I agree to delete this export as soon as I no longer require it.'),
      "#required"      => true
    ];

    if ($filterable) {
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => 'Download list of subscribed addresses',
        '#button_type' => 'primary',
        '#submit' => array(
          '::getSubscribed',
        ),
      ];

      $form['actions']['submit_unsubscribed'] = [
        '#type' => 'submit',
        '#value' => 'Download list of unsubscribed addresses',
        '#button_type' => 'secondary',
        '#submit' => array(
          '::getUnsubscribed',
        ),
      ];
    }

    $form_state->set('group', $group->id());

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array                                $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function getSubscribed(array &$form, FormStateInterface $form_state) {
    $form_state->set('subscribed', true);
    parent::submitForm($form, $form_state);
  }

  public function getUnsubscribed(array &$form, FormStateInterface $form_state) {
    $form_state->set('subscribed', false);
    parent::submitForm($form, $form_state);
  }

  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\group_mailing_lists\Service\MailingListService $service */
    $service = \Drupal::service('group_mailing_lists.mailing_list');
    $subscribed = $form_state->get('subscribed');
    $key = 'Email ' . ($subscribed ? '(subscribed)' : ' (UNSUBSCRIBED - DO NOT CONTACT)');

    $group = Group::load($form_state->get('group'));
    $rows = $service->getEmailsFromForm($group, $subscribed, $form_state);

    foreach ($rows as $row) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [[$key => $row]]);
    }
  }

  public function processBatch(array $row, array &$context) {
    $this->addLine($row, $context);
  }

  public static function getTitle(GroupInterface $group = null) {
    return 'Mailing list for ' . $group->label();
  }
}
