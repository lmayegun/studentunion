<?php

namespace Drupal\group_mailing_lists\Form;

use DateTime;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserProfileForm extends FormBase
{
    /**
     * @return string
     */
    public function getFormId()
    {
        return 'group_mailing_lists_contact_preferences';
    }

    /**
     *
     * @param array                                $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return array|RedirectResponse
     */
    public function buildForm(array $form, FormStateInterface $form_state, AccountInterface $user = null)
    {
        // @todo
        // $form['unsubscribe_from_all'] = [
        //     '#type' => 'checkbox',
        //     '#title' => $this->t('Unsubscribe from all'),
        // ];

        $subscriptions = [];
        $subscriptions['site'] = \Drupal::config('system.site')->get('name') . ' general mailing list';
        /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
        $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
        $records = $gmrRepositoryService->get($user, null, null, null, new DateTime(), true);
        foreach ($records as $record) {
            $group = $record->getGroup();
            $subscriptions['group:' . $group->id()] = $group->label();
        }

        $unsubscriptions = array_column($user->get('field_user_unsubscribed')->getValue(), 'value');

        foreach ($subscriptions as $key => $label) {
            $form[$key] = [
                '#type' => 'select',
                '#title' => $label,
                '#options' => [
                    '' => 'Subscribed',
                    $key => 'Unsubscribed'
                ],
                '#default_value' => in_array($key, $unsubscriptions) ? $key : ''
            ];
        }

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => 'Save contact preferences',
            '#button_type' => 'primary',
        ];

        $form_state->set('user', $user->id());
        $form_state->set('subscriptions', $subscriptions);

        return $form;
    }

    /**
     * Form submission handler.
     *
     * @param array                                $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user = User::load($form_state->get('user'));
        $subscriptions = $form_state->get('subscriptions');
        $unsubscriptions = [];
        if ($form_state->get('unsubscribe_from_all') === 1) {
            $unsubscriptions = $subscriptions;
        } else {
            $values = $form_state->getValues();
            foreach ($values as $value) {
                if ($value == '' || !in_array($value, array_keys($subscriptions))) {
                    continue;
                }
                $unsubscriptions[] = $value;
            }
        }
        $user->field_user_unsubscribed = $unsubscriptions;
        $user->save();

        \Drupal::messenger()->addMessage("Settings saved. It may take several days for e-mails to stop arriving. Note you may need to contact some groups or organisations directly if they manage your contact details separately.", 'status');
    }

    public static function getTitle(AccountInterface $account = null)
    {
        return 'Manage contact preferences';
    }

    /**
     * Checks access for the single/multiple pages.
     *
     * @param \Drupal\user\UserInterface $user
     *   The user account.
     * @param \Drupal\profile\Entity\ProfileTypeInterface $profile_type
     *   The profile type.
     * @param \Drupal\Core\Session\AccountInterface $account
     *   The currently logged in account.
     *
     * @return \Drupal\Core\Access\AccessResultInterface
     *   The access result.
     */
    public static function checkAccess(UserInterface $user, AccountInterface $account)
    {
        if ($user->id() == $account->id()) {
            return AccessResult::allowedIfHasPermission($account, 'edit own group contact preferences');
        } else {
            return  $user->access('edit', $account, TRUE);
        }
        return AccessResult::forbidden();
    }
}
