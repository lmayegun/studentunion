<?php

namespace Drupal\group_mailing_lists\Service;

use DateTime;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Service\GroupMembershipRecordRepository;
use Drupal\user\Entity\User;

/**
 * Implements  MailingListService Controller.
 */
class MailingListService
{

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    public $repo;

    /**
     * Constructs a new object.
     */
    public function __construct(GroupMembershipRecordRepository $gmrRepositoryService)
    {
        $this->repo = $gmrRepositoryService;
    }

    public function getEmails($subscribed = true, $group, $types = null, $roles = null, $groupTypes = null)
    {
        $records = [];
        if (!$roles) {
            $roles = [null];
        }
        if (!$types) {
            $types = $this->repo->getTypesInUse(null, $group, $groupTypes);
        }
        foreach ($types as $type) {
            foreach ($roles as $role) {
                $records = array_merge($records, $this->getForType($group, $type, $role, $subscribed ?? null, $subscribed ?? null));
            }
        }

        $emails = [];
        foreach ($records as $record) {
            $emails[] = $this->getEmailFromRecord($record);
        }

        array_unique($emails);
        return $emails;
    }

    public function getSubscribed(GroupInterface $group, $types)
    {
        $subscribed = $this->getEmails(true, $group, $types);
        $unsubscribed = $this->getUnsubscribed($group);
        $result = array_diff($subscribed, $unsubscribed);
        array_unique($result);
        return $result;
    }

    public function getUnsubscribed(GroupInterface $group)
    {
        $user_ids = \Drupal::entityQuery('user')
            ->condition('field_user_unsubscribed', 'group:' . $group->id(), 'IN')
            ->execute();
        $emails = [];
        foreach ($user_ids as $user_id) {
            $user = User::load($user_id);
            $emails[] = $user->getEmail();
            if (\Drupal::service('email.validator')->isValid($user->name->value)) {
                $emails[] = $user->name->value;
            }
        }
        array_unique($emails);
        return $emails;
    }

    public function getEmailFromRecord($record)
    {
        if ($record->hasField('field_signup_email') && $record->field_signup_email->value) {
            return $record->field_signup_email->value;
        }
        $user = $record->getUser();
        if ($user) {
            return $user->getEmail();
        }
        return false;
    }

    public function getForType($group, $type, $role, $current = true, $enabled = true)
    {
        return $this->repo->get(null, $group, $role, GroupMembershipRecordType::load($type), $current ? new DateTime() : null, $enabled);
    }

    public function getEmailsFromForm($group, $subscribed, &$form_state)
    {
        $filter_type = $form_state->getValue('filter_type');
        $types = $form_state->getValue('types');
        $roles = $form_state->getValue('roles');
        if ($filter_type && $filter_type == 'roles') {
            $types = null;
            $form_state->setValue('types', []);
        }

        if ($filter_type && $filter_type == 'types') {
            $roles = null;
            $form_state->setValue('roles', []);
        }

        if ($subscribed) {
            return $this->getSubscribed($group, $types, $roles);
        } else {
            return $this->getUnsubscribed($group, $types, $roles);
        }
    }
}
