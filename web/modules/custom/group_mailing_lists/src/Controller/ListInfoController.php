<?php

namespace Drupal\group_mailing_lists\Controller;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\fair\Entity\FairStall;
use Drupal\group\Entity\GroupInterface;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class ListInfoController extends ControllerBase
{
    public function renderListInfoPage(GroupInterface $group)
    {
        $host =  \Drupal::request()->getSchemeAndHttpHost();;
        $url = $host . $group->toUrl()->toString() . '/signup';
        $urlEncoded = urlencode($url);
        return [
            '#type' => 'markup',
            '#markup' => '<p>Share this QR code to allow people to sign up to this group (without needing to join or formally express interest). You (and other group leaders) can then contact these sign-ups or add them to your own mailing list.</p>
            <p>You can also link people to sign-up, or generate your own QR code, using the URL "' . $url . '".</p>
            <p>Signups are automatically deleted after three months. Check regularly especially after large events, and either contact for follow-through or move them into your own mailing list.</p>
            <p><b>Note:</b> you are obliged to provide a means of unsubscribing from any communications you may send. This must be managed by your group via your existing processes.</p>
            <p><img width="200px" height="200px" src="http://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor=FFFFFF&amp;data=' . $urlEncoded . '&amp;qzone=1&amp;margin=0&amp;size=600x600&amp;ecc=L" alt="qr code" /></p>'
        ];
    }
}
