<?php

namespace Drupal\group_mailing_lists\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Mailout entities.
 *
 * @ingroup group_mailing_lists
 */
interface MailoutInterface extends ContentEntityInterface, EntityOwnerInterface
{

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Mailout name.
   *
   * @return string
   *   Name of the Mailout.
   */
  public function getName();

  /**
   * Sets the Mailout name.
   *
   * @param string $name
   *   The Mailout name.
   *
   * @return \Drupal\group_mailing_lists\Entity\MailoutInterface
   *   The called Mailout entity.
   */
  public function setName($name);
}
