<?php

namespace Drupal\group_mailing_lists;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Mailout entity.
 *
 * @see \Drupal\group_mailing_lists\Entity\Mailout.
 */
class MailoutAccessControlHandler extends EntityAccessControlHandler
{

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
  {
    /** @var \Drupal\group_mailing_lists\Entity\MailoutInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view mailout entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit mailout entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete mailout entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL)
  {
    return AccessResult::allowedIfHasPermission($account, 'add mailout entities');
  }
}
