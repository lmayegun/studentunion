# How to write a migration

# OR edit an existing one (start at step 9)

We've already generated the template migrations, which are stored in su_migration/config/sync

1. To start a migration, find the template in config/sync and copy its content into a new file in config/install, named
   migrate_plus.migration.d7_something.yml
2. Remove the uuid from the file
3. Edit the label and rename it to match the end of the yml filename
4. Change migration group to 'su_migration'
5. Change the destination if needed, e.g. for a node -> group the destination should be entity:group
6. By default set migration dependenceis to

7. Change the destination if needed, e.g. for a node -> group the destination should be entity:group

8. By default comment out migration dependencies

9. When ready to test the migration, or if you ever make any changes:

Roll back the migration:

drush migrate-rollback <name of the migration here>

Import it into your current config by using

drush config-import --partial --source=modules/custom/su_migration/config/install

If any issues, try:

drush ms --group=su_migration -vvv

Run migration with drush (you will see some more output in terminal)

drush migrate-import <yml file name>
drush migrate-import group_club_or_society

# Guides and resources

## Processing plugins

https://www.drupal.org/docs/8/api/migrate-api/migrate-process-plugins/list-of-core-migrate-process-plugins
https://www.drupal.org/docs/8/api/migrate-api/migrate-process-plugins/list-of-process-plugins-provided-by-migrate-plus

## Other

https://www.drupal.org/docs/8/upgrade/customize-migrations-when-upgrading-to-drupal-8
https://www.phase2technology.com/blog/drupal-8-migrations
https://www.metaltoad.com/blog/drupal-8-migrations-part-4-migrating-nodes-drupal-7

## Migrating groups

https://thinktandem.io/blog/2018/03/30/migrating-drupal-7-organic-groups-to-drupal-8-group/
https://www.drupal.org/node/2797845

# Example commands

To import 10 clubs or societies (node -> group):

drush migrate-import page_from_how_to --limit=10

10 articles (node -> node);

drush migrate-import page_from_article --limit=10

To undo:

drush migrate-rollback group_club_or_society

To reset status:

drush migrate:reset-status group_club_or_society

To stop:

drush migrate:stop group_club_or_society

# Commands for SU migration

Should run all:

drush mr d7_course_object_fulfillment && drush migrate:import d7_course_object_fulfillment -vvv --execute-dependencies

Full:

drush mr d7_course && drush migrate:import d7_course -vvv && drush mr d7_course_object && drush migrate:import
d7_course_object -vvv && drush mr d7_course_enrollment && drush migrate:import d7_course_enrollment -vvv --force &&
drush mr d7_course_object_fulfillment && drush migrate:import d7_course_object_fulfillment -vvv

# Troubleshooting

drush ms --group=su_migration -vvv

# NOTE FOR FUTURE DEVELOPERS

If you want to look at the original migration, check the history of this module (and especially this readme file).
Things have been deleted when no longer needed to reduce clutter.

# Quiz and course

drush config-import --partial --source=modules/custom/su_migration/config/good_to_go

drush migrate-import d7_quiz_result --verbose -vvv
drush migrate-import d7_quiz_answers --verbose -vvv
drush migrate-import d7_quiz_multichoice --verbose -vvv
drush migrate-import d7_quiz_relationship --verbose -vvv // Throwing error for some
drush migrate-import d7_quiz_results_answers --verbose -vvv
drush migrate-import d7_course_enrollment --verbose -vvv --force
drush migrate-import d7_course_object_fulfillment -vvv --verbose --force

# NOTE FOR FUTURE DEVELOPERS

If you want to look at the original migration, check the history of this module. Things have been deleted when no longer
needed to reduce clutter.

