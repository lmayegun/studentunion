<?php

namespace Drupal\su_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\user\Plugin\migrate\source\d7\User as d7_user;

/**
 * Published nodes from the d7 database.
 *
 * @MigrateSource(
 *   id = "su_migration_active_recent_user",
 *   source_module = "user"
 * )
 */
class ActiveRecentUser extends d7_user {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    $query->condition('u.status', 1);

    $query->condition('u.login', strtotime("-2 years"), ">=");

    return $query;
  }

}
