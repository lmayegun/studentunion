<?php

namespace Drupal\su_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7_node;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_question_truefalse",
 *   source_module = "quiz"
 * )
 */
class QuizQuestionTrueFalse extends D7_node {

  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $this->addTrueFalseProps($row, $nid);
      $this->addQuizProps($row, $nid);
    }

    return parent::prepareRow($row);
  }

  protected function addTrueFalseProps(Row $row, $nid) {
    $query = $this->select('quiz_truefalse_node', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'correct_answer',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

  protected function addQuizProps(Row $row, $nid) {
    $query = $this->select('quiz_node_properties', 'qn')
      ->fields('qn', [
        'max_score',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qn.nid', $nid);

    $results = $query->execute()->fetchCol();

    if (!empty($results)) {
      $result = reset($results);

      $row->setSourceProperty('max_score', $result);
    }
  }
}
