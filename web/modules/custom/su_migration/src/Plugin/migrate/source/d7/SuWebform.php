<?php

namespace Drupal\su_migration\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\webform_migrate\Plugin\migrate\source\d7\D7Webform;

/**
 * Drupal menu link source from database.
 *
 * @MigrateSource(
 *   id = "su_webform",
 *   core = {7},
 *   source_module = "webform",
 *   destination_module = "webform"
 * )
 */
class SuWebform extends D7Webform
{

    /**
     * {@inheritdoc}
     */
    public function prepareRow(Row $row)
    {
        $nid = $row->getSourceProperty('nid');
        $row->setSourceProperty('field_migrate', $this->getFieldValues('node', 'field_migrate', $nid));

        $publishedAndNotNoMigrate = $row->getSourceProperty('status') == 1 && $row->getSourceProperty('field_migrate') !== 0;
        $yesMigrate = $row->getSourceProperty('field_migrate') == 1;
        if (!$publishedAndNotNoMigrate && !$yesMigrate) {
            return false;
        }
    }

    /**
     * Retrieves field values for a single field of a single entity.
     *
     * @param string $entity_type
     *   The entity type.
     * @param string $field
     *   The field name.
     * @param int $entity_id
     *   The entity ID.
     * @param int|null $revision_id
     *   (optional) The entity revision ID.
     * @param string $language
     *   (optional) The field language.
     *
     * @return array
     *   The raw field values, keyed by delta.
     */
    protected function getFieldValues($entity_type, $field, $entity_id, $revision_id = NULL, $language = NULL)
    {
        $table = (isset($revision_id) ? 'field_revision_' : 'field_data_') . $field;
        $query = $this->select($table, 't')
            ->fields('t')
            ->condition('entity_type', $entity_type)
            ->condition('entity_id', $entity_id)
            ->condition('deleted', 0);
        if (isset($revision_id)) {
            $query->condition('revision_id', $revision_id);
        }
        // Add 'language' as a query condition if it has been defined by Entity
        // Translation.
        if ($language) {
            $query->condition('language', $language);
        }
        $values = [];
        foreach ($query->execute() as $row) {
            foreach ($row as $key => $value) {
                $delta = $row['delta'];
                if (strpos($key, $field) === 0) {
                    $column = substr($key, strlen($field) + 1);
                    $values[$delta][$column] = $value;
                }
            }
        }
        return $values;
    }
}
