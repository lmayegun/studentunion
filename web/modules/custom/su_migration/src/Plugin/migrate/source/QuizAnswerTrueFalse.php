<?php

namespace Drupal\su_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_answer_truefalse",
 *   source_module = "quiz"
 * )
 */
class QuizAnswerMultichoice extends DrupalSqlBase {

  public function query() {
    $query = $this->select('quiz_multichoice_answers', 'ma')
      ->fields('ma', [
        'id',
        'answer',
        'answer_format',
        'feedback_if_chosen',
        'feedback_if_chosen_format',
        'feedback_if_not_chosen',
        'feedback_if_not_chosen_format',
        'score_if_chosen',
        'score_if_not_chosen',
      ])
      ->orderBy('ma.id', 'DESC');

    return $query;
  }

  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'ma',
      ],
    ];
  }

  public function fields() {
    $fields = [
      'id' => $this->t('The Answer ID'),
      'answer' => $this->t('Answer test'),
      'feedback_if_chosen' => $this->t('Feedback if answer is chosen'),
      'feedback_if_not_chosen' => $this->t('Feedback if answer is not chosen'),
      'score_if_chosen' => $this->t('Score if chosen'),
      'score_if_not_chosen' => $this->t('Score if not chosen'),
      'correct' => $this->t('If the answer is marked as correct or not'),
    ];

    return $fields;
  }

  public function prepareRow(Row $row) {

    $answer = [
      0 => [
        'value' => $row->getSourceProperty('answer'),
        'format' => $row->getSourceProperty('answer_format'),
      ]
    ];
    $row->setSourceProperty('answer', $answer);

    $feedback_if_chosen = [
      0 => [
        'value' => $row->getSourceProperty('feedback_if_chosen'),
        'format' => $row->getSourceProperty('feedback_if_chosen_format'),
      ]
    ];
    $row->setSourceProperty('feedback_if_chosen', $feedback_if_chosen);

    $feedback_if_not_chosen = [
      0 => [
        'value' => $row->getSourceProperty('feedback_if_not_chosen'),
        'format' => $row->getSourceProperty('feedback_if_not_chosen_format'),
      ]
    ];
    $row->setSourceProperty('feedback_if_not_chosen', $feedback_if_not_chosen);

    $score_if_chosen = $row->getSourceProperty('score_if_chosen');
    $score_if_not_chosen = $row->getSourceProperty('score_if_not_chosen');
    $correct_default = $score_if_chosen > $score_if_not_chosen ? '1' : '0';
    $row->setSourceProperty('correct', $correct_default);

    return parent::prepareRow($row);
  }
}
