<?php

namespace Drupal\su_migration\Plugin\migrate\source\og;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 organic group membership from database.
 *
 * @MigrateSource(
 *   id = "d7_og_membership",
 *   source_module = "og"
 * )
 */
class OrganicGroupMembership extends FieldableEntity {
  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Select node in its last revision.
    $query = $this->select('og_membership', 'ogm')
      ->fields('ogm', [
        'id',
        'type',
        'etid',
        'entity_type',
        'gid',
        'state',
        'created',
        'group_type',
        'field_name',
        'language',
      ]);

    if (isset($this->configuration['og_membership_type'])) {
      $query->condition('ogm.type', $this->configuration['og_membership_type']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $id = $row->getSourceProperty('id');
    $type = $row->getSourceProperty('type');
    $language = $row->getSourceProperty('language');

    // Get Field API field values.
    foreach ($this->getFields('og_membership', $type) as $field_name => $field) {
      // Ensure we're using the right language if the entity and the field are
      // translatable.
      $field_language = $language ?? NULL;
      $row->setSourceProperty($field_name, $this->getFieldValues('og_membership', $field_name, $id, NULL, $field_language));
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('OG membership ID'),
      'type' => $this->t('Type'),
      'etid' => $this->t('Member entity ID (e.g. uid'),
      'entity_type' => $this->t('Member entity type (user)'),
      'gid' => $this->t('Organic group ID'),
      'state' => $this->t('State'),
      'created' => $this->t('Created'),
      'group_type' => $this->t('Organic group type (e.g. node)'),
      'field_name' => $this->t('Organic group reference field name'),
      'language' => $this->t('Language (fr, en, ...)'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    $ids['id']['alias'] = 'ogm';
    return $ids;
  }

}
