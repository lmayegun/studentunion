<?php

namespace Drupal\su_migration\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Drupal menu link source from database.
 *
 * @MigrateSource(
 *   id = "su_node",
 *   source_module = "node"
 * )
 */
class SuNode extends Node
{

    /**
     * {@inheritdoc}
     */
    public function prepareRow(Row $row)
    {
        // Get Field API field values.
        $nid = $row->getSourceProperty('nid');
        $vid = $row->getSourceProperty('vid');
        $type = $row->getSourceProperty('type');
        $entity_translatable = $this->isEntityTranslatable('node') && (int) $this->variableGet('language_content_type_' . $type, 0) === 4;
        $source_language = $this->getEntityTranslationSourceLanguage('node', $nid);
        $language = $entity_translatable && $source_language ? $source_language : $row->getSourceProperty('language');
        foreach ($this->getFields('node', $type) as $field_name => $field) {
            $field_language = $entity_translatable && $field['translatable'] ? $language : NULL;
            $row->setSourceProperty($field_name, $this->getFieldValues('node', $field_name, $nid, $vid, $field_language));
        }

        // Skip if not migrating
        $publishedAndNotNoMigrate = $row->getSourceProperty('status') == 1 && $row->getSourceProperty('field_migrate') !== 0;
        $editedInLastYear = $row->getSourceProperty('changed') > strtotime('-1 year 2 months');
        $yesMigrate = $row->getSourceProperty('field_migrate') == 1;
        if (!$publishedAndNotNoMigrate && !($editedInLastYear || $yesMigrate)) {
            return false;
        }

        $row->setSourceProperty('webform_id', 'webform_' . $row->getSourceProperty('nid'));

        // Convert student ID references to users
        if ($row->hasSourceProperty('field_policy_proposer')) {
            $fieldsToConvertFromStudentToUser = ['field_policy_proposer', 'field_policy_seconders'];
            foreach ($fieldsToConvertFromStudentToUser as $field) {
                $row->setSourceProperty("converted_" . $field, null);

                $values = $row->getSourceProperty($field);
                if (count($values) == 0) {
                    continue;
                }
                $newValues = $row->getSourceProperty($field);
                foreach ($values as $key => $array) {
                    $studentId = $array['target_id'];

                    $member_uuids = $this->select('uclu_student', 's')
                        ->fields('s', ['member_uuid'])
                        ->condition('id', $studentId)
                        ->execute()
                        ->fetchAll();
                    if (!$member_uuids) {
                        continue;
                    }

                    $member_uuid = $member_uuids[0]['member_uuid'];

                    $uids = $this->select('member', 'm')
                        ->fields('m', ['uid'])
                        ->condition('uuid', $member_uuid)
                        ->execute()
                        ->fetchAll();
                    if ($uids) {
                        $uid = $uids[0]['uid'];
                        $newValues[$key]['target_id'] = $uid;
                    } else {
                        continue;
                    }
                }
                $row->setSourceProperty("converted_" . $field, $newValues);
            }
        }

        return parent::prepareRow($row);
    }
}
