<?php

namespace Drupal\su_migration\Plugin\migrate\source\d7;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as d7_node;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 volunteering opportunities from database.
 *
 * @MigrateSource(
 *   id = "d7_volunteering_opp",
 *   source_module = "node"
 * )
 */
class SuVolunteeringOpportunity extends d7_node
{

  /**
   * {@inheritdoc}
   * 
   * Populate some additional information for things we won't be migrating directly (og roles, memberships)
   */
  public function prepareRow(Row $row)
  {
    // Set moderation state
    $row->setSourceProperty('moderation_state', $row->get('status') == 1 ? 'published' : 'draft');

    // Get organisation ID
    $org_id = null;
    $orgs = $this->select('og_membership', 'ogm')
      ->fields('ogm', ['gid'])
      ->condition('type', 'volunteering_organisation')
      ->condition('entity_type', 'node')
      ->condition('etid', $row->getSourceProperty('nid')) // Opportunity ID
      ->execute()
      ->fetchAll();

    foreach ($orgs as $loaded_org_id) {
      if (isset($loaded_org_id['gid']) && $loaded_org_id['gid']) {
        $org_id = $loaded_org_id['gid'];
      }
    }

    $row->setSourceProperty('organisation_id',  $org_id);

    if (!$org_id) {
      echo PHP_EOL . 'Opportunity without org in D7: ' . $row->getSourceProperty('nid') . PHP_EOL;
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields()
  {
    $fields = parent::fields();
    $fields['organisation_id'] = $this->t('Organisation ID');
    return $fields;
  }
}
