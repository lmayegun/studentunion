<?php

namespace Drupal\su_migration\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\menu_link_content\Plugin\migrate\source\MenuLink;

/**
 * Drupal menu link source from database.
 *
 * @MigrateSource(
 *   id = "su_menu_link",
 *   source_module = "menu"
 * )
 */
class SuMenuLink extends MenuLink
{

    /**
     * {@inheritdoc}
     */
    public function query()
    {
        $query = parent::query();
        $query->condition('ml.hidden', 1, '<>');
        if (isset($this->configuration['menu_names'])) {
            $query->condition('ml.menu_name', $this->configuration['menu_names'], 'IN');
        }
        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function prepareRow(Row $row)
    {
        parent::prepareRow($row);
        $path = $row->getSourceProperty('link_path');

        if ($row->getSourceProperty('router_path') == 'node/%') {
            $id = explode('/', $path);
            $nodes = $this->select('node', 'n')
                ->fields('n', ['nid'])
                ->condition('nid', $id[1])
                ->condition('status', 1)
                ->execute()
                ->fetchAll();
            if (count($nodes) == 0) {
                return false;
            }
        }

        if (!\Drupal::pathValidator()->isValid($path)) {
            $row->setSourceProperty('external', 1);
            $row->setSourceProperty('customized', 1);
            $row->setSourceProperty('link_path', 'https://studentsunionucl.org/' . $path);
        }

        switch ($row->getSourceProperty('menu_name')) {
            case 'menu-new-menu':
                $row->setSourceProperty('menu_name', 'main');
                break;

            case 'menu-new-footer-menu':
                $row->setSourceProperty('menu_name', 'footer');
                if ($row->getSourceProperty('plid') == 0) {
                    $row->setSourceProperty('expanded', 1);
                }
                break;
        }

        return $row;
    }
}
