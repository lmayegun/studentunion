<?php

namespace Drupal\su_migration\Plugin\migrate\source\og;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 user roles within ogs from database.
 *
 * @MigrateSource(
 *   id = "d7_og_users_roles",
 *   source_module = "og"
 * )
 */
class OrganicGroupUsersRoles extends FieldableEntity
{
  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query()
  {
    $query = $this->select('og_users_roles', 'ogur')
      ->fields('ogur', [
        'uid',
        'rid',
        'gid',
        'group_type'
      ]);
    $query->condition('ogur.uid', '0', '>');
    $query->orderBy('gid', 'DESC');

    // FOR TESTING - Sikh Society 82262, vol opp 127777, abacus 82104
    // $query->condition('ogur.gid', '82104');
    // $query->condition('ogur.uid', '359461');

    if (isset($this->configuration['group_type'])) {
      $query->condition('ogur.type', $this->configuration['group_type']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   * 
   * Populate some additional information for things we won't be migrating directly (og roles, memberships)
   */
  public function prepareRow(Row $row)
  {
    $role_name = null;

    // Get role name from og_role table
    // rid, gid, name, group_type, group_bundle
    $roles = $this->select('og_role', 'ogr')
      ->fields('ogr', ['name'])
      ->condition('rid', $row->getSourceProperty('rid'))
      ->execute()
      ->fetchAll();
    foreach ($roles as $role) {
      $role_name = $role['name'];
      $row->setSourceProperty('role_name', $role_name);
    }

    // Get membership type from membership
    $memberships = $this->select('og_membership', 'ogm')
      ->fields('ogm', ['id', 'type', 'created'])
      ->condition('etid', $row->getSourceProperty('uid'))
      ->condition('entity_type', 'user')
      ->condition('gid', $row->getSourceProperty('gid'))
      ->execute()
      ->fetchAll();
    foreach ($memberships as $membership) {
      $row->setSourceProperty('og_membership_type', $membership['type']);
      $row->setSourceProperty('created', $membership['created']);
      $og_membership_id = $membership['id'];
    }

    // Get placement field info
    if ($role_name == 'placement' && isset($og_membership_id)) {
      $fields = ['field_start_date', 'field_end_date'];
      foreach ($fields as $field) {
        $query = $this->select('field_data_' . $field, 'f')
          ->fields('f', [$field . '_value'])
          ->condition('bundle', 'volunteering_opportunity')
          ->condition('entity_type', 'og_membership')
          ->condition('entity_id', $og_membership_id);
        $values = $query->execute()
          ->fetchAll();
        foreach ($values as $value) {
          $row->setSourceProperty($field, $value[$field . '_value']);
        }
      }
    }

    // Get node type
    $nodes = $this->select('node', 'n')
      ->fields('n', ['type'])
      ->condition('nid', $row->getSourceProperty('gid'))
      ->execute()
      ->fetchAll();
    foreach ($nodes as $node) {
      $row->setSourceProperty('og_node_type', $node['type']);
    }

    // Check if user also has 'ended' role
    $users_roles = $this->query();
    $ended = $users_roles->condition('ogur.rid', 50)
      ->execute()
      ->fetchAll();
    $row->setSourceProperty('placement_ended', count($ended) > 0);

    // Get memebership type
    // @todo get from product!? But that's not linked to the OG in any way
    // Probably not worth it for historical info

    // Get Field API field values.
    $id = $row->getSourceProperty('id');
    $type = $row->getSourceProperty('type');
    foreach ($this->getFields('og_membership', $type) as $field_name => $field) {
      $row->setSourceProperty($field_name, $this->getFieldValues('og_membership', $field_name, $id));
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields()
  {
    $fields = [
      'uid' => $this->t('User ID'),
      'rid' => $this->t('Role ID'),
      'gid' => $this->t('Group ID'),
      'group_type' => $this->t('Organic group type (e.g. node)'),
      'og_node_type' => $this->t('Organic group node type'),
      'role_name' => $this->t('Role name'),
      'og_membership_type' => $this->t('Organic group membership type'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds()
  {
    $ids['uid']['type'] = 'integer';
    $ids['rid']['type'] = 'integer';
    $ids['gid']['type'] = 'integer';
    return $ids;
  }
}
