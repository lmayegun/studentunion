<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use Drupal\Core\Render\Element\Page;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "convert_vol_contacts_to_contact_info_field"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_contacts:
 *   plugin: convert_vol_contacts_to_contact_info_field
 *   source: 
 * @endcode
 *
 */
class ConvertVolContactsToContactInfoField extends ProcessPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        $result = [];

        $names = ['vo_first', 'vo_second', 'vo_third', 'vop'];
        foreach ($names as $field_name) {
            if (!$row->getSourceProperty("field_" . $field_name . "_contact_name")) {
                continue;
            }
            $result[] = [
                'name' => $row->getSourceProperty("field_" . $field_name . "_contact_name")[0]['value'] ?? null,
                'email' => $row->getSourceProperty("field_" . $field_name . "_contact_email")[0]['email'] ?? null,
                'phone' => $row->getSourceProperty("field_" . $field_name . "_contact_phone")[0]['value'] ?? null
            ];
        }
        return $result;
    }
}
