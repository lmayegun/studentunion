<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use DOMDocument;
use DOMImplementation;
use Drupal\Core\Render\Element\Page;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "convert_address_to_geofield"
 * )
 *
 */
class ConvertAddressToGeofield extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($address, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return \Drupal::service('su_drupal_helper')->convertAddressArrayToGeofield($address);
  }
}
