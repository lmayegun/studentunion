<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use DateTime;
use Drupal\Core\Database\Database;
use Drupal\Core\Render\Element\Page;
use Drupal\group\Entity\Group;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "su_user_get_in_d8"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * uid:
 *   plugin: su_user_get_in_d8
 *   source: uid
 * @endcode
 *
 */
class GetUserIDInD8 extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $d7_uid = $value;
    Database::setActiveConnection('legacy');
    $db = \Drupal\Core\Database\Database::getConnection();

    if (!$db->schema()->fieldExists('users', 'name')) {
      $d7_results = reset($d7_results);
      return NULL;
    }

    $d7_results = $db->select('users', 'u')
      ->fields('u', ['uid', 'name', 'mail'])
      ->condition('uid', $d7_uid)
      ->execute()
      ->fetchAllAssoc('uid');

    Database::setActiveConnection('default');

    $d7_results = reset($d7_results);

    if (!$d7_results || !is_object($d7_results)) {
      return NULL;
    }

    $service = \Drupal::service('su_user.ucl_user');
    $account = $service->getUserForUclFieldInput($d7_results->name);
    if (!$account) {
      $account = $service->getUserForUclFieldInput($d7_results->mail);
    }
    return $account ? $account->id() : NULL;
  }
}
