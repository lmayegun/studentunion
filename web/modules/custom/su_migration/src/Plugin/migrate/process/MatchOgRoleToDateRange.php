<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use DateTime;
use Drupal\Core\Render\Element\Page;
use Drupal\group\Entity\Group;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "match_og_role_to_date_range"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_date_range:
 *   plugin: match_og_role_to_date_range
 *   source: text
 * @endcode
 *
 */
class MatchOgRoleToDateRange extends ProcessPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        $range = [
            'value' => null,
            'end_value' => null,
        ];

        // For "placement" check if "ended" role also applies and set end date from that too
        if ($row->getSourceProperty('role_name') == 'placement') {
            $range['value'] = Date('Y-m-d', strtotime($row->getSourceProperty('field_start_date')));
            if ($row->getSourceProperty('placement_ended')) {
                $range['end_value'] = Date('Y-m-d', strtotime($row->getSourceProperty('field_end_date')));
            }
        } else if ($row->getSourceProperty('role_name') == 'interest') {
            $range['value'] = Date('Y-m-d', $row->getSourceProperty('created'));
        } else {
            // Get academic year from role name
            $has_year = preg_match("/^(\D*)(\d+.*)\//", $value, $matches);
            if (!$has_year) {
                return $range;
            }

            $year = $matches[2];
            if (strlen($year) == 2) {
                $year = '20' + $year;
            }

            $og_node_type = $row->getSourceProperty("og_node_type");
            $socMember = strpos($row->getSourceProperty('role_name'), 'society member') !== FALSE;
            if ($og_node_type == 'club_or_society') {
                if ($socMember) {
                    $range = [
                        'value' => $year . '-09-01',
                        'end_value' => ($year + 1) . '-08-31',
                    ];
                } else {
                    $startDate = \Drupal::config('su_clubs_societies.settings')->get('date_committee_role_starts');

                    /** @var \Drupal\date_year_filter\Service\YearService $yearService */
                    $yearService = \Drupal::service('date_year_filter.year');
                    $year = $yearService->getYearForDate(DateTime::createFromFormat('Y-m-d', $year . '-09-01'), $startDate . ' 12:00:00');

                    $range = [
                        'value' => $year->start->format('Y-m-d'),
                        'end_value' => $year->end->format('Y-m-d'),
                    ];
                }
            } else {
                // Get academic year dates as a default
                $range = [
                    'value' => $year . '-09-01',
                    'end_value' => ($year + 1) . '-08-31',
                ];
            }
        }

        $range['value'] = $range['value'] ? $range['value'] . "T02:00:01" : null;
        $range['end_value'] = $range['end_value'] ? $range['end_value'] . "T22:59:59" : null;
        return $range;
    }
}
