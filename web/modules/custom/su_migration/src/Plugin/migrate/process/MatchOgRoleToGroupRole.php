<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use Drupal\Core\Render\Element\Page;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "match_og_role_to_group_role"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_date_range:
 *   plugin: match_og_role_to_group_role
 *   source: text
 * @endcode
 *
 */
class MatchOgRoleToGroupRole extends ProcessPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        $debug = false; // $row->getSourceProperty('uid') == 219703;

        /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
        $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
        $group = Group::load($row->getSourceProperty('gid'));
        if (!$group) {
            return false;
        }
        $groupType = $group->getGroupType();
        $roles = $gmrRepositoryService->getRolesForGroupType($groupType->id());

        // Get some candidate names, starting with the original value
        $candidates = [$groupType->id() . '-' . $value];
        // Strip out any numbers (e.g. years) at end of role
        $string_at_start = preg_match("/^(\D*)(\d+.*)/", $value, $matches);
        if ($matches) {
            $value = trim($matches[1]);
            $candidates[] = $groupType->id() . '-' . strtolower($value);
            $candidates[] = $groupType->id() . '-' . str_replace(' ', '_', strtolower($value));
        }
        echo $debug ? 'VALUE ' . $value . PHP_EOL : '';

        switch ($value) {
            case 'society member':
                $candidates[] = 'Member (club/society)';
                $candidates[] = 'club_society-member';
                break;
            case 'Welfare Officer':
                $candidates[] = 'Welfare Officer';
                $candidates[] = 'club_society-welfare_officer';
                break;
            case 'Volunteering & Outreach Officer':
                $candidates[] = 'club_society-volunteering';
                break;
            case 'leader':
                $candidates[] = $groupType->id() . '-organl';
                break;
        }
        $candidates[] = [$value];

        // Return role ID
        echo $debug ? 'Candidates ' . PHP_EOL . print_r($candidates, true) . PHP_EOL : NULL;
        foreach ($roles as $role) {
            echo $debug ? 'CHECKING ' . $role->id() . ' - ' . $role->label() . PHP_EOL : NULL;
            if (in_array($role->label(), $candidates) || in_array($role->id(), $candidates)) {
                echo $debug ? PHP_EOL . 'RETURNING ' . $role->id() . PHP_EOL : NULL;
                return $role;
            }
        }

        return false;
    }
}
