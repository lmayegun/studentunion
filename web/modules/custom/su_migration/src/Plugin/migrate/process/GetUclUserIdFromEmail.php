<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use Drupal\Core\Render\Element\Page;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "get_ucl_user_id_from_email"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: get_ucl_user_id_from_email
 *   source: text
 * @endcode
 *
 */
class GetUclUserIdFromEmail extends ProcessPluginBase {  
    
    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
        if(strpos($value, '@ucl.ac.uk') > 0) {
            if(strpos($value, '@') === 7) {
                return substr($value, 0, 7);
            }
        }
        return '';
   }

}