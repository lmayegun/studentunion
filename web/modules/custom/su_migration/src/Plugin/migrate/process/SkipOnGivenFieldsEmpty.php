<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\process\SkipOnEmpty;

/** 
 * @MigrateProcessPlugin(
 *   id = "skip_on_given_fields_empty",
 *   handle_multiples = TRUE
 * )
 */
class SkipOnGivenFieldsEmpty extends SkipOnEmpty
{

  protected $any_or_all_empty = 'all';
  protected $fields = [];

  function __construct($configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if (!empty($configuration['any_or_all_empty'])) {
      $this->any_or_all_empty = $configuration['any_or_all_empty'];
    }

    if (!empty($configuration['fields'])) {
      $this->fields = $configuration['fields'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $emptyCount = 0;
    $totalCount = count($this->fields);
    foreach ($this->fields as $field) {
      if (!$row->getSourceProperty($field)) {
        $emptyCount++;
      }
    }

    if (($this->any_or_all_empty == 'all' && $emptyCount == $totalCount) || ($this->any_or_all_empty == 'any' && $emptyCount > 0)) {
      $message = 'Empty fields: ' . $this->any_or_all_empty . ' are empty';
      throw new MigrateSkipRowException($message);
    }

    return $value;
  }
}
