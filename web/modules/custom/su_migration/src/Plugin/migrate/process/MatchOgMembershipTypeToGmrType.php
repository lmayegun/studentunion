<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use Drupal\Core\Render\Element\Page;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "match_og_membership_type_to_gmr_type"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_date_range:
 *   plugin: match_og_membership_type_to_gmr_type
 *   source: text
 * @endcode
 *
 */
class MatchOgMembershipTypeToGmrType extends ProcessPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {

        $og_node_type = $value;
        $og_membership_type = $row->getSourceProperty("og_membership_type");
        $role_name = $row->getSourceProperty("role_name");

        if ($og_node_type == 'club_or_society') {
            if (strpos($role_name, 'society member') === 0) {
                return 'club_society_member';
            } else {
                return 'club_society_committee';
            }
        } elseif ($og_node_type == 'volunteering_opportunity') {
            if ($role_name == 'interest' || $role_name == 'rejected') {
                return 'volunteering_interest';
            } else {
                return 'volunteering_placement';
            }
        } elseif ($og_node_type == 'volunteering_organisation') {
            return 'volunteering_leader';
        }
    }
}
