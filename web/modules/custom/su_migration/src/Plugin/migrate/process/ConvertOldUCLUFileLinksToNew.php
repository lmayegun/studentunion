<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use DOMDocument;
use DOMImplementation;
use Drupal\Core\Render\Element\Page;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Convert /sites/uclu.org/files to sites/default/files
 *
 *
 * @MigrateProcessPlugin(
 *   id = "convert_uclu_files_to_new"
 * )
 * 
 * Example use:
 *
 * @code
 * body/value:
 *   plugin: convert_uclu_files_to_new
 *   source: value
 * @endcode
 *
 */
class ConvertOldUCLUFileLinksToNew extends ProcessPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        return str_replace('sites/uclu.org/', 'sites/default/', $value);
    }
}
