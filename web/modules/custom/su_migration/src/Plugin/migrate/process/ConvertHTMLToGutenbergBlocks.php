<?php

namespace Drupal\su_migration\Plugin\migrate\process;

use DOMDocument;
use DOMImplementation;
use Drupal\Core\Render\Element\Page;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Convert a body HTML to a format Gutenberg will understand as blocks
 * 
 * Basically using DOMDocument to go through each element
 * wrapping it in the relevant HTML comments: * 
 * 
 * <!-- wp:heading {"level":3} -->
 * <h3>Heading 3.</h3>
 * <!-- /wp:heading -->
 *
 * @MigrateProcessPlugin(
 *   id = "convert_html_to_gutenberg_blocks"
 * )
 *
 * Example use:
 *
 * @code
 * body/value:
 *   plugin: convert_html_to_gutenberg_blocks
 *   source: value
 * @endcode
 *
 */
class ConvertHTMLToGutenbergBlocks extends ProcessPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        $tags = [
            'p' => 'wp:paragraph',

            'h1' => 'wp:heading {"level":1}',
            'h2' => 'wp:heading {"level":2}',
            'h3' => 'wp:heading {"level":3}',
            'h4' => 'wp:heading {"level":4}',

            'hr' => 'wp:separator',

            'ul' => 'wp:list',
            'ol' => 'wp:list {"ordered":true}',

            'div' => 'wp:html',

            'dl' => 'su-gutenberg-blocks/ckeditor-accordion'
        ];

        // Load the HTML as a DOM document to traverse through
        $source = new DOMDocument();
        $source->loadHTML(mb_convert_encoding($value, 'HTML-ENTITIES', 'UTF-8'));

        // Start new document to store new HTML
        $converted = (new DOMImplementation)->createDocument(null, 'html', (new DOMImplementation)->createDocumentType("html"));

        // Linebreaks to make sure the comments in the source are well formatted
        $lineBreak = $converted->createTextNode("\n");

        // Loop through all top level elements in the document 
        // (new DOMDocument automatically puts them under <body>)
        $nodes = $source->getElementsByTagName('body')->item(0);
        foreach ($nodes->childNodes as $node) {
            $wp_tag = null;

            // Deal with Video embeds
            if ($node->nodeName != '#text' && $node->tagName == 'p' && strpos($node->textContent, '[VIDEO::') >= 0) {
                $text = $node->textContent;
                preg_match('/(?<=\[VIDEO::).+?(?=\])/m', $text, $matches);
                if ($matches) {
                    $url = $matches[0];
                    // $converted->removeChild($node);

                    $comment = $converted->createComment(" " . 'wp:core-embed/youtube {"url":"' . $url . '","type":"rich","providerNameSlug":"","align":"full","className":""}' . " ");
                    $converted->documentElement->appendChild(clone $lineBreak);
                    $converted->documentElement->appendChild($comment);
                    $converted->documentElement->appendChild(clone $lineBreak);

                    $figure = $converted->createElement('figure', '');
                    $figure->setAttribute('class', "wp-block-embed-youtube alignfull wp-block-embed is-type-rich");
                    $div = $converted->createElement('div', $url);
                    $div->setAttribute('class', "wp-block-embed__wrapper");
                    $figure->appendChild($div);
                    $converted->appendChild($figure);

                    $comment = $converted->createComment(" " . '/wp:core-embed/youtube' . " ");
                    $converted->documentElement->appendChild(clone $lineBreak);
                    $converted->documentElement->appendChild($comment);
                    $converted->documentElement->appendChild(clone $lineBreak);

                    continue;
                }
            }

            // If it's an HTML element, and we have a Gutenberg version for the tag, 
            // Add that as a comment - e.g. <!-- wp:heading {"level":3} -->
            if ($node->nodeName != '#text' && isset($tags[$node->tagName])) {

                // Skip empty p tags people have put in for spacing
                if ($node->tagName == 'p' && $node->childNodes->length === 0) {
                    continue;
                }

                // Otherwise recreate with a Gutenberg comment
                $wp_tag = $tags[$node->tagName];

                $comment = $converted->createComment(" " . $wp_tag . " ");
                $converted->documentElement->appendChild($comment);

                // Accordions
                if ($node->tagName == 'dl') {
                    $newAccordion = [];
                    $newRow = [];
                    foreach ($node->childNodes as $accordionChild) {
                        if (!isset($accordionChild->tagName)) {
                            continue;
                        }
                        if ($accordionChild->tagName == 'dt') {
                            $newRow['title'] = $accordionChild->nodeValue;
                        }
                        if ($accordionChild->tagName == 'dd') {
                            $newRow['text'] = $accordionChild->nodeValue;
                            $newAccordion[] = $newRow;
                            $newRow = [];
                        }
                    }
                    foreach ($newAccordion as $section) {
                        if (!isset($newRow['title'])) {
                            continue;
                        }

                        $comment = $converted->createComment(" wp:su-gutenberg-blocks/ckeditor-accordion-section " . json_encode($section));
                        $converted->documentElement->appendChild($comment);

                        // <dt>fdg sdgfd </dt><dd><p>fdg dffdgd fg</p></dd>
                        $dt = $converted->createElement('dt', $newRow['title']);
                        $converted->appendChild($dt);

                        $dd = $converted->createElement('dd', $newRow['text']);
                        $converted->appendChild($dd);

                        $comment = $converted->createComment(" /wp:su-gutenberg-blocks/ckeditor-accordion-section ");
                        $converted->documentElement->appendChild($comment);
                    }
                }


                $converted->documentElement->appendChild(clone $lineBreak);
            }

            // Copy the element (after the comment if we inserted it)
            $node = $converted->importNode($node, true);
            if (isset($node->tagName) && isset($tags[$node->tagName]) && $node->tagName == 'hr') {
                $node->setAttribute('class', 'wp-block-separator');
            }
            $converted->documentElement->appendChild($node);

            // Add a closing comment - e.g. <!-- /wp:heading -->
            if ($wp_tag) {
                // Strip out options like {"level":3} from the tag
                // To construct the end tag
                $get_start = preg_match("/(.+)\s/", $wp_tag, $matches);
                $end_tag = "/" . ($matches[1] ?? $wp_tag);
                $comment = $converted->createComment(" " . $end_tag . " ");

                // Add comment end tag with linebreaks before and after
                $converted->documentElement->appendChild(clone $lineBreak);
                $converted->documentElement->appendChild($comment);
                $converted->documentElement->appendChild(clone $lineBreak);
            }
        };

        // Remove DOMDocument HTML cruft to get just the <body> contents equivalent
        $trim_off_front = strlen('<!DOCTYPE html><html>') + 1;
        $trim_off_end = (strrpos($converted->saveHTML(), '</html>')) - strlen($converted->saveHTML());
        $result = substr($converted->saveHTML(), $trim_off_front, $trim_off_end);
        return $result;
    }
}
