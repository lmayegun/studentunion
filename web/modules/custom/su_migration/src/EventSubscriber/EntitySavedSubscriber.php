<?php

declare(strict_types=1);

namespace Drupal\su_migration\EventSubscriber;

use DateTime;
use Drupal\commerce\Context;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Row;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntitySavedSubscriber.
 *
 * @package Drupal\su_migration\EventSubscriber
 */
class EntitySavedSubscriber implements EventSubscriberInterface {
  /**
   * Maps the existing redirects to the new node id.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The migrate post row save event.
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event): void {
    $migration = $event->getMigration();
    $source_configuration = $migration->getSourceConfiguration();
    $destination_configuration = $migration->getDestinationConfiguration();
    $row = $event->getRow();


    if ($destination_configuration['plugin'] === 'entity:commerce_product_variation') {
      $this->afterProductVariation($event, $row);
    }

    if ($destination_configuration['plugin'] === 'entity:commerce_product') {
      $this->afterProduct($event, $row);
    }

    if ($migration->id() == 'group_og_users_roles') {
      $this->afterOgRole($event, $row);
    }

    if ($migration->id() == 'd7_webform') {
      $this->afterWebform($event, $row);
    }
    if ($migration->id() == 'commerce_product_event' || $migration->id() == 'commerce_product_meeting') {
      $this->afterProductEvent($event, $row);
    }
    if ($migration->id() == 'commerce_product_variation_event') {
      $this->afterProductVariationEvent($event, $row);
    }

    if ($destination_configuration['plugin'] === 'entity:group') {
      if ($destination_configuration['default_bundle'] == 'volunteering_opp') {
        $this->afterVolunteeringOpportunity($event);
      }
      if ($destination_configuration['default_bundle'] == 'club_society') {
        $this->afterClubSociety($event);
      }
    }
  }

  public function afterWebform($event, $row) {
  }

  public function afterOgRole($event, $row) {
    $id = $event->getDestinationIdValues();
    $recordId = reset($id);
    $record = GroupMembershipRecord::load($recordId);
    $record->sync();
    //\Drupal::service('group_membership_record.group_role_sync')->syncGroupRolesForRecord($record);
  }

  public function afterVolunteeringOpportunity($event) {
    $row = $event->getRow();
    $source = $row->getSource();

    // Load new group (opp)
    $id = $event->getDestinationIdValues();
    $subgroup_id = reset($id);
    $subgroup = Group::load($subgroup_id);

    // Load parent group organisation
    if (!isset($source['organisation_id']) || !$source['organisation_id']) {
      echo 'No organisation ID for ' . $subgroup->label() . ' (' . $subgroup_id . ')' . PHP_EOL;
      return;
    }
    $org_id = $source['organisation_id'];
    $org = Group::load($org_id);
    if ($org) {
      if ($org->bundle() != 'volunteering_org') {
        echo PHP_EOL . $org_id . ' is actually a ' . $org->bundle() . ' so not going to add it as child.' . PHP_EOL;
        return;
      }
      $org->addContent($subgroup, 'subgroup:volunteering_opp');

      echo PHP_EOL . $org->label() . ' (' . $org->id() . ')' . ' with subgroup ' . $subgroup->label() . ' (' . $subgroup->id() . ')' . PHP_EOL;
    }
  }


  public function afterProduct($event, Row $row) {
    $id = $event->getDestinationIdValues();
    $product_id = reset($id);
    $product = Product::load($product_id);

    if (count($product->getStoreIds()) == 0) {
      /** @var \Drupal\commerce_store\CurrentStore $currentStore */
      $currentStore = \Drupal::service('commerce_store.current_store');
      $store = $currentStore->getStore();
      $product->setStoreIds([$store->id()]);
      $product->save();
    }
  }

  public function afterProductEvent($event, Row $row) {
    $id = $event->getDestinationIdValues();
    $product_id = reset($id);
    $product = Product::load($product_id);
    echo PHP_EOL . PHP_EOL . 'Event ' . $product_id . ' - variation count ' . count($product->getVariationIds()) . ' - d7 prods ' . $row->getSourceProperty('number_of_products') . PHP_EOL . PHP_EOL;

    $variations = $product->getVariations();
    if (count($variations) > 0) {
      // foreach ($variations as $variation) {
      //   $variation = ProductVariation::create([
      //     'type' => 'event',
      //     'sku' => 'EVENT_' . $product_id,
      //     'title' => $product->getTitle(),
      //     'price' => new Price('0.00', 'GBP'),
      //     'status' => 1,
      //   ]);
      // }
    } else if (count($variations) == 0 && $row->getSourceProperty('number_of_products') == 0) {
      // Create product variation with just dates
      $array = [
        'type' => 'event',
        'sku' => 'EVENT_' . $product_id,
        'title' => $product->getTitle(),
        'price' => new Price('0.00', 'GBP'),
        'status' => 1,
      ];
      if ($row->getSourceProperty('dates') && is_array($row->getSourceProperty('dates'))) {
        // $array['date_range'] = $row->getSourceProperty('dates');
      }
      $variation = ProductVariation::create($array);
      if ($row->getSourceProperty('dates') && is_array($row->getSourceProperty('dates'))) {
        $dates = [
          'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $row->getSourceProperty('dates')['value']),
          'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $row->getSourceProperty('dates')['end_value']),
        ];
        echo PHP_EOL . "WHAT WE'RE SAVING:" . PHP_EOL;
        print_r($dates);
        echo PHP_EOL . PHP_EOL;
        $variation->set('field_date_range', $dates);
      }
      $variation->save();
      // dd([$row->getSourceProperty('dates'), $variation->get('field_date_range')->value]);

      if ($row->getSourceProperty('dates')) {
        // print_r($row->getSourceProperty('dates'));
      }
      if ($row->getSourceProperty('dates') && is_array($row->getSourceProperty('dates'))) {
        // $dateRange = [
        //   'value' => $row->getSourceProperty('dates')['value'],
        //   'end_value' => $row->getSourceProperty('dates')['end_value'],
        // 'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $row->getSourceProperty('dates')['value']),
        // 'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $row->getSourceProperty('dates')['end_value']),
        // ];
        // dd($row->getSourceProperty('dates'), $variation->get('field_date_range'), $dateRange);
        // $variation->set('field_date_range', $dateRange);
        $variation->set('field_date_range', $row->getSourceProperty('dates'));
      }
      $variation->set('is_ticket', false);
      if ($product->bundle() == 'meeting') {
        $variation->set('commerce_stock_always_in_stock', true);
      }
      $variation->save();
      $product->addVariation($variation);
      echo PHP_EOL . PHP_EOL . 'Event had no products in D7 - saved product variation ' . $variation->id() . PHP_EOL . PHP_EOL;
    }
    $product->save();
  }

  public function afterProductVariationEvent($event, $row) {
  }

  public function setVariationStock($product_variation, $stock) {
    $purchasedEntity = $product_variation;
    $stockServiceManager = \Drupal::service('commerce_stock.service_manager');
    $stockServiceConfig = $stockServiceManager->getService($purchasedEntity)->getConfiguration();
    $current_user = \Drupal::currentUser();
    $current_store = \Drupal::service('commerce_store.current_store')->getStore();
    $context = new Context($current_user, $current_store);
    $locations = $stockServiceConfig->getAvailabilityLocations($context, $purchasedEntity);
    $unitPrice = $product_variation->getPrice();
    // $stockServiceManager->createTransaction($purchasedEntity, $locations[1]->getId(), '', $stock, $unitPrice->getNumber(), $unitPrice->getCurrencyCode(), StockTransactionsInterface::STOCK_IN, []);
  }

  public function afterProductVariation($event, Row $row) {
    $id = $event->getDestinationIdValues();
    $id = reset($id);
    $variation = ProductVariation::load($id);
    // if ($row->getSourceProperty('stock')) {
    //   $this->setVariationStock($variation, $row->getSourceProperty('stock'));
    // }

    if ($variation->getProduct() && $variation->list_price->first()->number > 0) {
      $realPrice = $variation->list_price->first()->toPrice();
      $salePrice = $variation->price->first()->toPrice();
      $variation->price = $realPrice;
      $variation->list_price = $salePrice;
      $variation->save();
    }
  }

  public function afterClubSociety($event) {
    $id = $event->getDestinationIdValues();
    $club_soc_id = reset($id);
    $group = Group::load($club_soc_id);

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyService $clubSocService */
    $clubSocService = \Drupal::service('su_clubs_societies.service');
    $clubSocService->createProduct($group);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[MigrateEvents::POST_ROW_SAVE] = ['onPostRowSave'];
    return $events;
  }
}
