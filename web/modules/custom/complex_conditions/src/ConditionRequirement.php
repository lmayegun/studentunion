<?php

namespace Drupal\complex_conditions;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Represents a requirement for a condition.
 */
final class ConditionRequirement {

  /**
   * Requirement id.
   *
   * @var string
   */
  protected $id;

  /**
   * Requirement label.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  protected $label;

  /**
   * Requirement description.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  protected $description;

  /**
   * Whether the requirement has been passed or not.
   *
   * @var bool
   */
  protected $pass;

  /**
   * The ID of the condition group the requirement is part of, if any.
   *
   * @var string
   */
  protected $group;


  /**
   * Prevents determining eligibility until user input provided at point of checking.
   *
   * @var bool
   */
  protected $requires_user_input = FALSE;

  /**
   * Constructs a new Requirement object.
   *
   * @param array $definition
   *   The definition.
   */
  public function __construct(array $definition) {
    foreach (['id', 'label', 'pass'] as $required_property) {
      if (!isset($definition[$required_property])) {
        throw new \InvalidArgumentException(sprintf('Missing required property %s.', $required_property));
      }
    }

    $this->id          = isset($definition['id'])          ? $definition['id'] : NULL;
    $this->label       = isset($definition['label'])       ? $definition['label'] : NULL;
    $this->pass        = isset($definition['pass'])        ? $definition['pass'] : NULL;
    $this->description = isset($definition['description']) ? $definition['description'] : NULL;
    $this->group       = isset($definition['group'])       ? $definition['group'] : NULL;
    $this->requires_user_input       = isset($definition['requires_user_input'])       ? $definition['requires_user_input'] : NULL;
  }

  public function id() {
    return $this->getId();
  }

  public function getId() {
    return $this->id;
  }

  public function setId(string $id) {
    $this->id = $id;
  }

  public function label() {
    return $this->getLabel();
  }

  public function getLabel() {
    return $this->label;
  }

  public function setLabel(TranslatableMarkup $label) {
    $this->label = $label;
  }

  public function labelAsString() {
    return $this->label->render();
  }

  public function getPass() {
    return $this->pass;
  }

  public function isPassed() {
    return $this->pass;
  }

  public function isFailed() {
    return !$this->pass;
  }

  public function setPass(bool $pass) {
    $this->pass = $pass;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription(TranslatableMarkup $description) {
    $this->description = $description;
  }

  public function getGroup() {
    return $this->group;
  }

  public function setGroup(string $group) {
    $this->group = $group;
  }

  public function getRequiresUserInput() {
    return $this->requires_user_input;
  }

  public function setRequiresUserInput(bool $requires_user_input) {
    $this->requires_user_input = $requires_user_input;
  }

  public static function anyPassed(array $requirements) {
    $result = static::getPassed($requirements);
    return count($result) > 0;
  }

  public static function allPassed(array $requirements, $ignoreInputRequired = FALSE) {
    $result = static::getPassed($requirements, $ignoreInputRequired);
    return count($result) === count($requirements);
  }

  public static function getPassed(array $requirements, $ignoreInputRequired = FALSE) {
    $return = [];
    foreach ($requirements as $requirement) {
      if ($requirement->pass || ($ignoreInputRequired && $requirement->getRequiresUserInput())) {
        $return[] = $requirement;
      }
    }
    return $return;
  }

  public static function dependsOnUserInput(array $requirements) {
    foreach ($requirements as $requirement) {
      if (!$requirement->pass && $requirement->getRequiresUserInput()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public static function anyFailed(array $requirements, $ignoreInputRequired = FALSE) {
    $result = static::getFailed($requirements, $ignoreInputRequired);
    return count($result) > 0;
  }

  public static function allFailed(array $requirements, $ignoreInputRequired = FALSE) {
    $result = static::getFailed($requirements, $ignoreInputRequired);
    return count($result) === count($requirements);
  }

  public static function getFailed(array $requirements, $ignoreInputRequired = FALSE) {
    $return = [];
    foreach ($requirements as $requirement) {
      if (!$requirement->pass && (!$ignoreInputRequired || !$requirement->getRequiresUserInput())) {
        $return[] = $requirement;
      }
    }
    return $return;
  }

  public function negate() {
    $negatedLabel = t('NOT ' . $this->labelAsString());
    $this->setLabel($negatedLabel);
    $this->setPass(!$this->isPassed());
  }

  public static function sortByGroup(array $requirements) {
    usort($requirements, function ($a, $b) {
      $groupA = $a->getGroup() ?: 0;
      $groupB = $b->getGroup() ?: 0;
      return $groupA - $groupB;
    });
    return $requirements;
  }

  public function getEmoji() {
    if ($this->isPassed()) {
      return '✔️';
    } elseif ($this->getRequiresUserInput()) {
      // return '✍️';
    }

    return '❌';
  }
}
