<?php

namespace Drupal\complex_conditions\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\ConditionRequirement;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "user_role",
 *   label = @Translation("User role(s)"),
 *   display_label = @Translation("User has specific user role(s)"),
 *   category = @Translation("Users"),
 *   weight = 0,
 * )
 */
class UserRole extends ComplexConditionBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'user_roles' => [],
      'user_roles_any_all' => '',
    ] + parent::defaultConfiguration();
  }

  public function getUserRoleOptions() {
    $roles = Role::loadMultiple();
    $roleOptions = [];
    foreach ($roles as $role) {
      $roleOptions[$role->id()] = $role->label();
    }
    return $roleOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $roleOptions = $this->getUserRoleOptions();
    $form['user_roles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Roles'),
      '#options' => $roleOptions,
      '#default_value' => $this->configuration['user_roles'],
    ];
    $form['user_roles_any_all'] = [
      '#type' => 'select',
      '#title' => $this->t('Require any or all of these roles'),
      '#options' => [
        'any' => 'Any of these role(s)',
        'all' => 'All of these role(s)',
      ],
      '#default_value' => $this->configuration['user_roles_any_all'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    // $role_ids = array_column($values['user_roles'], 'target_id');
    $this->configuration['user_roles'] = $values['user_roles'];
    $this->configuration['user_roles_any_all'] = $values['user_roles_any_all'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $requirements = parent::evaluateRequirements($entity, $account, $parameters);

    $rolesToCheck = $this->configuration['user_roles'];
    if (is_string($rolesToCheck)) {
      $rolesToCheck = [$rolesToCheck];
    }
    $rolesNames = [];
    foreach ($rolesToCheck as $roleId) {
      $rolesNames[] = Role::load($roleId)->label();
    }
    $userRoles = $account->getRoles();

    if ($this->configuration['user_roles_any_all'] == 'any') {
      // Any:
      $message = count($rolesNames) == 1 ? 'User must have this website role: @roles' : 'User has any of the following website roles: @roles';
      $requirements['has_any_roles'] = new ConditionRequirement([
        'id' => 'has_any_roles',
        'label' => t($message, [
          '@roles' => implode(', ', $rolesNames),
        ]),
        'description' => t('Roles are generally granted by the site administrator or based on your user type.'),
        'pass' => count(array_intersect($rolesToCheck, $userRoles)) > 0,
      ]);
    } else {
      // All:
      $message = count($rolesNames) == 1 ? 'User must have this website role: @roles' : 'User has all of the following website roles: @roles';
      $requirements['has_all_roles'] = new ConditionRequirement([
        'id' => 'has_all_roles',
        'label' => t($message, [
          '@roles' => implode(', ', $rolesNames),
        ]),
        'description' => t('Roles are generally granted by the site administrator.'),
        'pass' => count(array_intersect($rolesToCheck, $userRoles)) == count($rolesToCheck),
      ]);
    }

    return $requirements;
  }

  /**
   * {@inheritdoc}
   */
  public function generateRandomConditionConfig() {
    $randomRoles = array_rand($this->getUserRoleOptions(), random_int(1, 3));
    if (is_string($randomRoles)) {
      $randomRoles = [$randomRoles];
    }

    $config = parent::generateRandomConditionConfig();
    $config['target_plugin_configuration']['user_roles'] = $randomRoles;
    $config['target_plugin_configuration']['user_roles_any_all'] = random_int(0, 1) == 0 ? "any" : "all";

    return $config;
  }
}
