<?php

namespace Drupal\commerce_email\Form;

use Drupal\commerce_email\EmailSenderInterface;
use Drupal\commerce_email\Entity\EmailInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests emails.
 */
class TestEmailForm extends FormBase {

  /**
   * The email sender.
   *
   * @var \Drupal\commerce_email\EmailSenderInterface
   */
  protected $emailSender;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_email.email_sender'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EmailSenderInterface $email_sender, EntityTypeManagerInterface $entity_type_manager) {
    $this->emailSender = $email_sender;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_email_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EmailInterface $commerce_email = NULL) {
    $form_state->set('commerce_email', $commerce_email);
    $target_entity_type_id = 'commerce_order';

    $form['order_context'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'commerce_order',
      '#title' => $this->t('Order reference'),
      '#description' => $this->t('The order to use in context for the token replacement.'),
    ];

    $form['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From'),
      '#maxlength' => 255,
      '#default_value' => $commerce_email->getFrom(),
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [$target_entity_type_id],
    ];
    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#maxlength' => 255,
      '#default_value' => $commerce_email->getTo(),
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [$target_entity_type_id],
    ];
    $form['cc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cc'),
      '#maxlength' => 255,
      '#default_value' => $commerce_email->getCc(),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [$target_entity_type_id],
    ];
    $form['bcc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bcc'),
      '#maxlength' => 255,
      '#default_value' => $commerce_email->getBcc(),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [$target_entity_type_id],
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#default_value' => '[TEST]' . $commerce_email->getSubject(),
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [$target_entity_type_id],
    ];
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $commerce_email->getBody(),
      '#rows' => 10,
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [$target_entity_type_id],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Test email',
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getUserInput();

    /** @var \Drupal\commerce_email\Entity\EmailInterface $email */
    $email = $form_state->get('commerce_email');
    $email->setFrom($values['from']);
    $email->setTo($values['to']);
    $email->setCc($values['cc']);
    $email->setBcc($values['bcc']);
    $email->setSubject($values['subject']);
    $email->setBody($values['body']);

    $order = $this->entityTypeManager->getStorage('commerce_order')->load($form_state->getValue('order_context'));
    $this->emailSender->send($email, $order);
  }

}
