This module is a copy of a contributed module with some changes made.

This is likely because the current state of the module on the Drupal website required
a lot of patches and then some customisation and it was too convoluted to manage.

This should be regularly compared to the contributed module current release
to see if it can be deleted from here.
