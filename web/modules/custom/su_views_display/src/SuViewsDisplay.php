<?php

namespace Drupal\su_views_display;

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;
use Drupal\views_bootstrap\ViewsBootstrap;
/**
 * The primary class for the Views Bootstrap module.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class SuViewsDisplay extends ViewsBootstrap {

  /**
   * Returns the theme hook definition information.
   */
  public static function getThemeHooks() {
    $hooks['su_views_display_card_grid'] = [
      'preprocess functions' => [
        'template_preprocess_su_views_display_card_grid',
      ],
      'file' => 'su_views_display.theme.inc',
    ];
    return $hooks;
  }

  /**
   * Get unique element id.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueId(ViewExecutable $view) {
    $id = $view->storage->id() . '-' . $view->current_display;
    return Html::getUniqueId('su-views-display-' . $id);
  }

}
