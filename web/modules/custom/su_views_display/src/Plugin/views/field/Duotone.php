<?php

namespace Drupal\su_views_display\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @ViewsField("duotone")
 *
 * Duotone field - provides CSS class.
 */
class Duotone extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $cardValues = su_views_display_convert_entity_to_card_values($entity);

    $alias = $this->field_alias;
    $values->$alias = isset($cardValues['duotone']) && $cardValues['duotone'] != '' ? $cardValues['duotone'] : su_views_display_get_random_duotone();

    return parent::render($values);
  }
}
