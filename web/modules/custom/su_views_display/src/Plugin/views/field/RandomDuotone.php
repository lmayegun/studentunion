<?php

namespace Drupal\su_views_display\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @ViewsField("duotone_random")
 */
class RandomDuotone extends FieldPluginBase {
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $alias = $this->field_alias;
    $values->$alias = su_views_display_get_random_duotone();
    return parent::render($values);
  }
}
