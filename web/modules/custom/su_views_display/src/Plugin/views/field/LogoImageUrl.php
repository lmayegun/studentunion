<?php

namespace Drupal\su_views_display\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @ViewsField("logo_image_url")
 */
class LogoImageUrl extends FieldPluginBase {
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $cardValues = su_views_display_convert_entity_to_card_values($entity);

    $alias = $this->field_alias;
    $values->$alias = $cardValues['logo'] ?? '';

    return parent::render($values);
  }
}
