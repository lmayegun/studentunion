<?php

namespace Drupal\su_views_display\Plugin\views\field;

use DateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\views\Plugin\views\field\Custom;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ViewsField("card_image_url")
 */
class CardImageUrl extends FieldPluginBase {
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $cardValues = su_views_display_convert_entity_to_card_values($entity);

    $alias = $this->field_alias;
    $values->$alias = $cardValues['imageUrl'] ?? '';

    return parent::render($values);
  }
}
