<?php

namespace Drupal\su_views_display\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bootstrap\Plugin\views\style\ViewsBootstrapGrid;

/**
 * Style plugin to render a grid of cards using bootstrap template.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "su_views_display_card_grid",
 *   title = @Translation("Card Grid"),
 *   help = @Translation("Displays rows in a Bootstrap grid Card Group layout"),
 *   theme = "su_views_display_card_grid",
 *   theme_file = "../su_views_display.theme.inc",
 *   display_types = {"normal"}
 * )
 */
class SuViewsDisplayCardGrid extends ViewsBootstrapGrid {
  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = TRUE;

  /**
   * Options available to populate plugin fields value and details
   *
   * @var array
   */
  public static $fieldTypes = [
    'card_link' => [
      'title' => 'Link to content',
      'description' => 'You must select a plain text URL (output link as URL) for when card is clicked',
    ],
    'card_title_field' => [
      'title' => 'Card title',
      'description' => 'Please select a text field which will be displayed as card title, limit the characters to 60.'
    ],
    'card_image_field' => [
      'title' => 'Background image',
      'description' => 'Add to the view an image field with Formatter set to "URL to image".',
      'multiple' => TRUE,
    ],
    'duotone' => [
      'title' => 'Image duotone filter',
      'description' => 'Select a duotone field. It must be the Key not the Value.',
    ],
    'card_subheading_1' => [
      'title' => 'First subheading',
      'description' => 'Select a field as a subheading, this could be date/time, venue or text field. It will appear under the title.',
    ],
    'card_subheading_2' => [
      'title' => 'Second subheading',
      'description' => 'This will appear under the first subheading and can take similar fields.',
    ],
    'card_body_text_preview' => [
      'title' => 'Body text preview',
      'description' => 'Add a body field, limit to 40 characters. Can be left empty.',
    ],
    'host_icon' => [
      'title' => 'Logo of host/organiser',
      'description' => 'Must be an image field in Thumbnail format display. This will display a logo/icon in a top left corner.',
    ],
    'host_text' => [
      'title' => 'Name of host/organiser',
      'description' => 'Will appear next to the logo.',
    ],
    'price' => [
      'title' => 'Price',
      'description' => 'Please select for only ticked items and products',
    ],
    'card_button_text' => [
      'title' => 'Button text',
      'description' => 'Select a text field that displays on action button. If no button leave empty.',
    ],
    'image_box' => [
      'title' => 'Tag/label on the card image area',
      'description' => 'Text label appearing in the right bottom corner of the image part of the card.',
    ],
    'body_box' => [
      'title' => 'Tag/label on the card contentarea',
      'description' => 'Text label appearing in the left bottom corner of the card.',
    ],
    'button_links' => [
      'title' => 'Multiple links field',
      'description' => 'Select a field which allows multiple links',
    ],
  ];

  /**
   * Definition.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    foreach (self::$fieldTypes as $index => $type) {
      $options[$index] = ['default' => NULL];
    }
    $options['card_portrait_landscape'] = ['default' => 'card-portrait'];
    $options['card_size'] = ['default' => 'card-size-default'];
    $options['large_image'] = ['default' => FALSE];
    $options['clip_path'] = ['default' => ''];
    $options['make_horizontal_on_smaller_screens'] = ['default' => TRUE];
    $options['contain_background'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['card_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Card display mode'),
      '#options' => [
        'card-portrait' => $this->t('Portrait'),
        'card-portrait-small' => $this->t('Portrait - small'),
        'card-landscape' => $this->t('Landscape'),
        'card-square' => $this->t('Square'),
      ],
      '#required' => FALSE,
      '#default_value' => $this->options['card_mode'],
    ];

    $form['large_image'] = [
      '#title' => $this->t('Large image (e.g. for products)'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['large_image'],
      '#description' => $this->t('Displays card with image taking 2/3 of the height (only on portrait display).'),
    ];

    $form['make_horizontal_on_smaller_screens'] = [
      '#title' => $this->t('Display as horizontal on mobile screens'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['make_horizontal_on_smaller_screens'] ?? TRUE,
    ];

    $options = $this->displayHandler->getFieldLabels(TRUE);
    $options = ['' => 'None'] + $options;
    foreach (self::$fieldTypes as $index => $type) {
      $form[$index] = [
        '#type' => 'select',
        '#title' => t($type['title']),
        '#description' => $this->t($type['description']),
        '#options' => $options,
        '#required' => in_array($index, ['card_title_field', 'card_link'], TRUE),
        '#multiple' => isset($type['multiple']) ? $type['multiple'] : FALSE,
        '#default_value' => $this->options[$index],
      ];
    }

    $form['contain_background'] = [
      '#title' => $this->t('Contain background image within card image (rather than stretch to fill'),
      '#description' => $this->t('Only use this if the card image HAS to be a logo or similar.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['contain_background'] ?? FALSE,
    ];

    $form['clip_path'] = [
      '#type' => 'select',
      '#title' => t('Clip path'),
      '#description' => $this->t('Clip the image to a shape'),
      '#options' => [
        '' => 'None (recommended)',
        'clip-path-union-u' => 'Union U'
      ],
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#default_value' => $this->options['clip_path'],
    ];


    // Put responsive settings in a details (fieldset closed by default)
    // @todo provide option to use or not
    $form['responsive'] = [
      '#type' => 'details',
      '#title' => 'Responsive settings'
    ];
    foreach ($form as $key => $value) {
      if (strpos($key, 'col_') === 0) {
        $form['responsive'][$key] = $value;
        unset($form[$key]);
      }
    }
    // var_dump($form_state);
  }
}
