<?php

/**
 * Implements hook_views_data_alter().
 */
function su_views_display_views_data_alter(array &$data) {
  $data['views']['duotone_random'] = [
    'title' => t('Random duotone'),
    'help' => t('From the Portico Sunset theme'),
    'field' => [
      'id' => 'duotone_random',
      'click sortable' => TRUE,
    ],
  ];

  $types = ['groups', 'node', 'commerce_product'];
  foreach ($types as $type) {
    $data[$type]['card_image_url'] = [
      'title' => t('Cards defaults: card image URL'),
      'help' => t(''),
      'field' => [
        'id' => 'card_image_url',
        'click sortable' => TRUE,
      ],
    ];
    $data[$type]['logo_image_url'] = [
      'title' => t('Cards defaults: logo image URL'),
      'help' => t(''),
      'field' => [
        'id' => 'logo_image_url',
        'click sortable' => TRUE,
      ],
    ];
    $data[$type]['duotone'] = [
      'title' => t('Cards defaults: duotone'),
      'help' => t('Always returns a duotone (either the assigned, or a random one)'),
      'field' => [
        'id' => 'duotone',
        'click sortable' => TRUE,
      ],
    ];
  }
}
