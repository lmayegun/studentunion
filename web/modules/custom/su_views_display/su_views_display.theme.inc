<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

use Drupal\views_bootstrap\ViewsBootstrap;
use Drupal\su_views_display\SuViewsDisplay;
use Drupal\su_views_display\Plugin\views\style\SuViewsDisplayCardGrid;

/**
 * Prepares variables for views grid templates.
 *
 * Default template: su-views-display-card-grid.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_su_views_display_card_grid(array &$vars) {
  $view = $vars['view'];
  $vars['id'] = SuViewsDisplay::getUniqueId($view);
  $options = $view->style_plugin->options;

  $vars['cardClasses'] = '';
  $card_mode = $view->style_plugin->options['card_mode'];
  $card_large_image = $view->style_plugin->options['large_image'];

  $vars['card_mode'] = $card_mode;
  $vars['attributes']['class'][] = 'card-grid';
  $vars['attributes']['class'][] = $card_mode;

  $horizontalMobile = $view->style_plugin->options['make_horizontal_on_smaller_screens'] ?? true;
  $vars['cardClasses'] .= $horizontalMobile ? ' card-horizontal-sm ' : '';

  $vars['clip_path'] = $view->style_plugin->options['clip_path'];

  if ($card_mode == 'card-landscape') {
    $vars['cardClasses'] .= ' card-horizontal ';
  } else {
    $vars['cardClasses'] .= $card_mode . ' ' . ($card_large_image ? ' card-large-image ' : '');
  }
  if ($view->style_plugin->options['contain_background'] ?? false) {
    $vars['cardClasses'] .= ' card-background-contain ';
  }

  $fieldTypes = SuViewsDisplayCardGrid::$fieldTypes;
  foreach ($fieldTypes as $index => $type) {
    ${$index} = $view->style_plugin->options[$index];
  }

  foreach ($vars['rows'] as $id => $row) {
    $vars['rows'][$id] = [];
    foreach ($fieldTypes as $index => $type) {
      $row_field = ${$index};
      if (isset($type['multiple']) && $type['multiple'] && is_array($row_field)) {
        foreach ($row_field as $value) {
          $new_row_field = $value;
          break;
        }
        $row_field = $new_row_field ?? NULL;
      }
      $fieldValue = $view->style_plugin->getField($id, $row_field);
      if ($fieldValue) {
        $vars['rows'][$id][$index] = $fieldValue;
      }
    }
  }

  // Grid display
  $vars['row_classes'] = [];
  $vars['responsiveCardSize'] = FALSE;
  if ($vars['responsiveCardSize']) {
    foreach (ViewsBootstrap::getBreakpoints() as $breakpoint) {
      if ($options["col_$breakpoint"] == 'none') {
        continue;
      }
      $vars['row_classes'][] = $options["col_$breakpoint"];
    }
  } else {
    if ($card_mode == 'card-landscape') {
      $vars['row_classes'][] = 'cards-horizontal';
    } else {
      $vars['row_classes'][] = 'col_auto';
    }
    $vars['row_classes'][] = 'mr-3';
    $vars['row_classes'][] = 'ml-3';
    $vars['row_classes'][] = 'mb-4'; // mb-5
  }
  $vars['options'] = $options;
}
