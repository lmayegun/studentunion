<?php

namespace Drupal\su_election\Plugin\EntityStatistics;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_statistics_extensible\Plugin\EntityStatisticsGeneratorPluginBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "election_voted",
 *   label = @Translation("User voted in an election in last year"),
 *   category = @Translation("Student engagement"),
 *   weight = 10,
 * )
 **/
class ElectionVotedLastYear extends EntityStatisticsGeneratorPluginBase {

  public $entityTypeId = ['user'];

  /**
   */
  public function getPossibleDataValues() {
    return ['Yes', 'No'];
  }

  public function generateEntityData(EntityInterface $entity) {
    $cid = $this->getCacheId(implode(':', [
      __METHOD__,
      $entity->id(),
      $entity->getEntityTypeId(),
    ]));
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $since = strtotime('-1 year');

    $value = \Drupal::entityQuery('election_ballot')
      ->condition('confirmed', 1)
      ->condition('user_id', $entity->id())
      ->condition('created', $since, '>')
      ->count()
      ->execute() > 0 ? 'Yes' : 'No';

    \Drupal::cache()->set($cid, $value, Cache::PERMANENT, $entity->getCacheTags());

    return $value;
  }
}
