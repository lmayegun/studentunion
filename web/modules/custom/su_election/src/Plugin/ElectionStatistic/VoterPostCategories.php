<?php

namespace Drupal\su_election\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionBallot;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election_statistics\Plugin\ElectionStatistic\ElectionStatisticBase;
use Drupal\election_statistics\Plugin\ElectionStatistic\ElectionStatisticInterface;
use Drupal\election_statistics\Plugin\ElectionStatistic\VotersByPostType;

/**
 * @ElectionStatistic(
 *  id = "post_categories",
 *  label = @Translation("Voters by post categories voted for"),
 *  weight = -50,
 * )
 */
class VoterPostCategories extends VotersByPostType {

  public function getValueFromPost(ElectionPostInterface $election_post) {
    return $election_post->field_election_post_category->entity ? $election_post->field_election_post_category->entity->label() : 'No category';
  }

  public function getGroupedIntroText() {
    return t('<p>This shows the combinations of categories voted for by users. e.g. if one user voted for posts across three different categories, they will be counted in the "Category A + Category B + Category C" row. If they only voted for posts with Category B, they will be counted in the "Category B" row.</p>');
  }
}
