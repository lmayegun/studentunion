<?php

namespace Drupal\su_election\Plugin\MailoutType;

use DateTime;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Phase\NominationsPhase;
use Drupal\election\Phase\VotingPhase;
use Drupal\mailouts\Plugin\MailoutType\EntityMailoutBase;
use Drupal\user\Entity\User;
use InvalidArgumentException;

/**
 * Plugin implementation of the mailout_type.
 *
 * @MailoutType(
 *   id = "election_mailout",
 *   label = @Translation("Election mailouts"),
 *   description = @Translation("Send mail to e.g. eligible nominators.")
 * )
 */
class ElectionMailout extends EntityMailoutBase {
  public $entityTypeId = 'election';

  public function routesForActions() {
    return [
      'entity.election.canonical' => t('E-mail eligible users or candidates'),
    ];
  }

  public function routesForTasks() {
    return [
      'entity.election.canonical' => t('Mailouts'),
    ];
  }

  public function addFormOptions(&$form, FormStateInterface $form_state, $params = NULL) {
    parent::addFormOptions($form, $form_state, $params);

    $form['type'] = [
      '#type' => 'select',
      '#title' => "Category to e-mail",
      '#options' => [
        'nominate' => t('Eligible to nominate'),
        'vote' => t('Eligible to vote'),
        'candidates' => t('Hopeful candidates'),
      ],
      '#default_value' => 'vote',
      '#required' => TRUE,
    ];
  }

  public function getRecipientsFromForm($form, FormStateInterface $form_state, $params = NULL) {
    $emails = [];

    $entity_id = $params['entity_id'] ?? NULL;
    $election = Election::load($entity_id);
    if (!$election) {
      throw new InvalidArgumentException('No election found');
    }

    $type = $form_state->getValue('type');

    if ($type == 'nominate' || $type = 'vote') {
      $phase = $type == 'nominate' ? new NominationsPhase() : new VotingPhase();

      $query = \Drupal::entityQuery('user');
      $query->condition('status', 1);
      $uids = $query->execute();

      foreach ($uids as $uid) {
        $account = User::load($uid);
        $eligible = $election->getNextPostId($account, NULL, NULL, $phase, FALSE) ? TRUE : FALSE;
        if ($eligible) {
          $emails[] = $account->getEmail();
        }
      }
    } elseif ($type == 'candidates') {
      $candidate_ids = $election->getCandidateIds(['hopeful']);
      foreach ($candidate_ids as $candidate_id) {
        $candidate = ElectionCandidate::load($candidate_id);
        $emails[] = $candidate->getOwner()->getEmail();
      }
    }

    array_unique($emails);
    return $emails;
  }

  public function getConfirmationText() {
    return t('I confirm this e-mail relates to election activity only and is appropriate for its audience.');
  }
}
