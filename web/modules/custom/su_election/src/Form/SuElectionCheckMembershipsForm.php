<?php

namespace Drupal\su_election\Form;

use DateTime;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Phase\PhaseInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\su_clubs_societies\Plugin\ComplexConditions\Condition\ElectionClubSocMember;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Class ExportEligibleUsers.
 */
class SuElectionCheckMembershipsForm extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_election_check_membership_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    $date = \Drupal::service('date.formatter')->format(strtotime('now'), 'short');
    return "Check clubsoc memberships for nominees " . ($name ? ' - ' . $name : '') . ' - ' . $date;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {

    $form = parent::buildForm($form, $form_state);

    $form_state->set('election_id', $election->id());

    $form['date_test'] = [
      '#type' => 'datetime',
      '#date_date_format' => 'Y-m-d',
      '#date_time_format' => 'H:i:s',
      '#date_date_element' => 'date',
      '#date_time_element' => 'time',
      '#title' => 'Date to test 28 days from',
      '#default_value' => new DrupalDateTime(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $function = 'processBatch';
    $chunk_size = 200;
    $election = Election::load($form_state->get('election_id'));

    $candidates = $election->getCandidateIds([], FALSE);

    // Chunk the array
    $chunks = array_chunk($candidates, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, $function], [
        [
          'election_candidates' => $chunk,
          'election' => $election,
          'date_test' => $form_state->getValue('date_test')
        ],
      ]);
    }
  }

  public function processBatch(array $data, array &$context) {
    $election_candidates = $data['election_candidates'];
    $election = $data['election'];
    $date_test = $data['date_test'];
    foreach ($election_candidates as $election_candidate_id) {
      $election_candidate = ElectionCandidate::load($election_candidate_id);

      $election_post = $election_candidate->getElectionPost();
      $groups = su_election_get_clubsoc_from_post($election_post);

      foreach ($groups as $group_id) {
        $group = Group::load($group_id);
        $line = $this->generateLine($group, $election_candidate, $election_post);
        $this->addLine($line, $context);
      }
    }
  }

  public function generateLine($group, $election_candidate, $election_post) {
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $account = $election_candidate->getOwner();

    $memberSince = '';
    $memberDays = '';

    $records = $gmrRepositoryService->get($account, $group, GroupRole::load('club_society-member'), null, new DateTime(), true);
    if (count($records) > 0) {
      $record = reset($records);
      $memberSince = \Drupal::service('date.formatter')->format($record->getStartDate()->getTimestamp(), 'short');
      $memberDays = $record->getStartDate()->diff(new DateTime())->format("%r%a");
    } else {
      $record = FALSE;
    }

    $line = [
      'User ID' => $account->id(),
      'User name' => $account->getDisplayName(),
      'User e-mail' => $account->getEmail(),
      'Post' => $election_post->label(),
      'Club/Soc' => $group->label(),
      'Is member' => $record ? 'Yes' : 'No',
      'Member since' => $memberSince,
      'Member how many days' => $memberDays,
      'Current exemption' => ElectionClubSocMember::checkMembershipTimeExemption($account, $group) ? 'Yes' : 'No',
    ];
    return $line;
  }
}
