<?php

namespace Drupal\su_election\Form;

use Drupal;
use Drupal\election_import_csv\Form\ElectionPostImportForm;
use Drupal\su_students\Plugin\ComplexConditions\Condition\ElectionSelfDefinition;

/**
 * Implement Class BulkUserImport for import form.
 */
class SuElectionPostImportForm extends ElectionPostImportForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_election_post_import_form';
  }

  public static function getMapping(): array {
    $fields = parent::getMapping();
    $action = $fields['Action'];
    unset($fields['Action']);

    // Add custom fields
    $fields['Club/Society'] = [
      'description' => 'Club/society finance code, ID or name',
    ];

    $fields['Self-definition'] = [
      'description' => 'Self-definition category',
      'example' => 'One of: ' . implode(',', array_keys(ElectionSelfDefinition::SELF_DEFINITION_OPTIONS)),
    ];

    $fields['Category'] = [
      'description' => 'post category, e.g. Sabbatical Officers - (note a category will be created if it does not already exist)',
    ];

    // Add action back
    $fields['Action'] = $action;
    return $fields;
  }

  public static function validateRow(&$record, array &$context) {
    // Add club/soc condition
    if (isset($record['Club/Society']) && $record['Club/Society']) {
      $service = Drupal::service('su_clubs_societies.service');
      $club_society = $service->getClubSocFromUserInput($record['Club/Society']);
      if (!$club_society) {
        static::addError($record, $context, 'Cannot find club/society with ID/code: ' . $record['Club/Society']);
        return FALSE;
      }
      else {
        if (strpos($record['Name'], $club_society->label()) == FALSE) {
          $record['Name'] = $club_society->label() . ': ' . $record['Name'];
        }
      }
    }
    return TRUE;
  }

  public static function createPost($election, $record) {
    $post = parent::createPost($election, $record);
    $conditions = [];

    // Add club/soc condition
    if (isset($record['Club/Society']) && $record['Club/Society']) {
      $service = Drupal::service('su_clubs_societies.service');
      if ($club_society = $service->getClubSocFromUserInput($record['Club/Society'])) {
        $clubSocCondition = [
          'target_plugin_id' => 'election_clubsoc_member',
          'target_plugin_configuration' => [
            'groups' => [
              [
                'target_id' => $club_society->id(),
              ],
            ],
            "groups_any_or_all" => "any",
            "group_roles" => [
              [
                'target_id' => "club_society-member",
              ],
            ],
            "group_roles_any_or_all" => "any",
            "parent" => "",
            "depth" => 0,
            "sort_weight" => "1",
            "negate_condition" => 0,
          ],
        ];
        $conditions[] = $clubSocCondition;
      }
    }

    // Add self-define condition
    if (isset($record['Self-definition']) && $record['Self-definition']) {
      $service = Drupal::service('su_clubs_societies.service');
      $selfDefineCondition = [
        'target_plugin_id' => 'su_students_self_definition',
        'target_plugin_configuration' => [
          'field' => $record['Self-definition'],
          "parent" => "",
          "depth" => 0,
          "sort_weight" => "1",
          "negate_condition" => 0,
        ],
      ];
      $conditions[] = $selfDefineCondition;
    }
    if ($conditions) {
      $post->set('conditions_voting', $conditions);
    }

    // Add category
    if (isset($record['Category'])) {
      $terms = Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties([
          'name' => $record['Category'],
          'vid' => 'election_post_categories',
        ]);
      $term = $terms ? reset($terms) : FALSE;
      if (!$term) {
        // @todo fix
        // $term = Term::create([
        //    'vid' => 'election_post_categories',
        //   'name' => $record['Category'],
        // ])->save();
      }
      if ($term) {
        $post->field_election_post_category->entity = $term;
      }
    }

    $post->save();
  }

  public static function afterCreatePost($record, $post) {

    return $post;
  }

}
