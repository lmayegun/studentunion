<?php

namespace Drupal\su_election\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\VotingPhase;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\User;

/**
 * Class ExportEligibleUsers.
 */
class ExportEligibleClubSoc extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_eligible_clubsoc';
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    $date = \Drupal::service('date.formatter')->format(strtotime('now'), 'short');
    return "Get members of clubs and societies in election " . ($name ? ' - ' . $name : '') . ' - ' . $date;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {

    $form = parent::buildForm($form, $form_state);

    $form_state->set('election_id', $election->id());

    $form['markup'] = ['#markup' => 'Produces a list of any users who are club/society members for clubs/societies which are set as conditions on any published posts in this election. Note this does not factor in eligiblity other than club/society membership at this stage.'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $function = 'processBatch';

    $election = Election::load($form_state->get('election_id'));

    $election_post_ids = $election->getPostIds(TRUE, NULL, ['club_society']);

    $chunk_size = 100;

    // Chunk the array
    $chunks = array_chunk($election_post_ids, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, $function], [
        [
          'election_posts' => $chunk,
          'election' => $election,
        ],
      ]);
    }
  }

  public function processBatch(array $data, array &$context) {
    $election_posts = ElectionPost::loadMultiple($data['election_posts']);
    $election = $data['election'];

    foreach ($election_posts as $election_post) {
      $group_ids = su_election_get_clubsoc_from_post($election_post);

      foreach ($group_ids as $group_id) {
        // We load this with a direct query to avoid loading the membership entities.
        // (Slows performance).
        $connection = \Drupal::database();
        // Load memberships for this group:
        $query = $connection->select('group_content_field_data', 'gc');
        $query->condition('gc.type', 'club_society-group_membership');
        $query->condition('gc.gid', $group_id);
        $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
        $query->condition('r.group_roles_target_id', 'club_society-member');
        $query->fields('gc', ['entity_id']);
        $results = $query->distinct()->execute()->fetchCol();
        array_filter($results);
        $uids = $results;

        // $members = \Drupal::service('group_membership_record.repository')->get(NULL, Group::load($group_id), GroupRole::load('club_society-member'), NULL, new DateTime(), TRUE);
        foreach ($uids as $uid) {
          $line = [];
          $account = User::load($uid);
          if ($account->hasRole('member') && !$election_post->userHasCompletedPhase($account, new VotingPhase)) {
            $line['User ID'] = $account->id();
            $line['UPI'] = $account->field_upi->value;
            $line['E-mail'] = $account->getEmail();
            $this->addLine($line, $context);
          }
        }
      }
    }
  }

  public function filterResults($rows, $results) {
    $finalRows = [];
    $uids = [];
    foreach ($rows as $row) {
      if (!in_array($row['User ID'], $uids)) {
        $finalRows[$row['User ID']] = $row;
        $uids[] = $row['User ID'];
      }
    }
    return $finalRows;
  }

  public function addLine(array $line, &$context) {
    if (!isset($context['results']['lines'])) {
      $context['results']['lines'] = [];
    }

    $context['results']['lines'][$line['User ID']] = $line;
  }
}
