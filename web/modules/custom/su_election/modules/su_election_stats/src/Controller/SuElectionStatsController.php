<?php

namespace Drupal\su_election_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election_statistics\Plugin\ElectionStatistic\SummaryElectionStatistic;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidate;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SuElectionStatsController extends ControllerBase {

  public function getCacheTime() {
    return strtotime('+1 minute');
  }

  public function render(ElectionInterface $election) {
    $build = [];
    $build['#markup'] = '<div id="root" class="' . $election->id() . '"></div>';
    $build['#attached']['library'][] = 'su_election_stats/react';
    return $build;
  }

  public function getCacheableResponse($election, $type, $expiry = NULL) {
    $results = $this->loadStat($election, $type, $expiry);

    $response = new CacheableJsonResponse($results);

    if (!$expiry) {
      $expiry = isset($results['expiry']) ? $results['expiry'] : NULL;
    }

    if (!$expiry) {
      $expiry = $this->getCacheTime();
    }

    $response->setMaxAge($expiry)
      ->setSharedMaxAge($expiry);

    $response->getCacheableMetadata()
      ->addCacheTags($this->getCacheTags($election));

    return $response;
  }

  public static function getFilename(ElectionInterface $election, $type) {
    return 'public://election_stats/' . $election->id() . '_' . $type . '.json';
  }

  public static function getJsonUrl(ElectionInterface $election, $type) {
    return \Drupal::service('file_url_generator')->generateAbsoluteString(static::getFilename($election, $type));
  }

  public static function getJsonFile(ElectionInterface $election, $type) {
    if (!file_exists(static::getFilename($election, $type))) {
      return NULL;
    }

    $path = static::getJsonUrl($election, $type);

    try {
      $arrContextOptions = array(
        "ssl" => array(
          "verify_peer" => false,
          "verify_peer_name" => false,
        ),
      );

      $str = file_get_contents($path, false, stream_context_create($arrContextOptions));
    } catch (Exception $e) {
      return NULL;
    }
    $json = json_decode($str, TRUE);
    return $json;
  }

  public function ballotCount($election) {
    return \Drupal::entityQuery('election_ballot')
      ->condition('confirmed', 1)
      ->condition('election', $election->id())
      ->count()
      ->execute();
  }

  public function runBackgroundProcess($election, $type, $ballot_count, $expiry) {
    $handle = 'su_election_stats_' . $election->id() . '_' . $type;
    background_process_start_locked($handle, [get_class($this), 'calculate' . $type], $election->id(), $ballot_count, $expiry);
  }

  public function loadStat($election, $type, $expiry) {
    $ballot_count = $this->ballotCount($election);
    $json = static::getJsonFile($election, $type);

    $notGenerated = !$json || count($json) == 0;
    $oldEnough = $json && (!isset($json['expiry']) || $json['expiry'] == -1 || $json['expiry'] < strtotime('now')) && $json['ballot_count'] < $ballot_count;
    if ($notGenerated || $oldEnough) {
      $this->runBackgroundProcess($election, $type, $ballot_count, $expiry);
    }
    return $json;
  }

  public static function setJsonFile($election, $type, $ballot_count, $expiry, $data) {
    if (!file_exists('public://election_stats')) {
      \Drupal::service('file_system')->mkdir('public://election_stats', 0777);
    }

    $data['ballot_count'] = $ballot_count;
    $data['expiry'] = $expiry;

    \Drupal::service('file_system')->saveData(
      json_encode($data),
      static::getFilename($election, $type),
      FileSystemInterface::EXISTS_REPLACE,
    );
  }

  public function json(ElectionInterface $election) {
    return $this->getCacheableResponse($election, 'main', strtotime('+2 minutes'));
  }

  public function jsonMap(ElectionInterface $election) {
    return $this->getCacheableResponse($election, 'map', strtotime('+1 hour'));
  }

  public function jsonVotesOverTime(ElectionInterface $election) {
    return $this->getCacheableResponse($election, 'votes_over_time', strtotime('+1 hour'));
  }

  public function jsonLeaderboards(ElectionInterface $election) {
    return $this->getCacheableResponse($election, 'leaderboards', strtotime('+10 minutes'));
  }

  public function getCacheTags($election) {
    return [
      'election:' . $election->id() . ':votes:count',
      'election:' . $election->id() . ':posts:count',
      'election:' . $election->id() . ':candidates:count',
    ];
  }

  public static function calculatemain($election_id, $ballot_count, $expiry) {
    $election = Election::load($election_id);
    // Define the top level of the schema:
    $stats = [
      "election_id"           => $election->id(),
      "election_name"         => $election->label(),
      "all_votes"             => [],
      "total_voters"          => 0,
      "total_possible_voters" => 0,
      "total_turnout"         => 0,
    ];

    $pluginManager = \Drupal::service('plugin.manager.election_statistics');
    $definitions = $pluginManager->getDefinitions();

    $summaryPlugin = $pluginManager->createInstance('summary', $definitions['summary']);
    $summary = $summaryPlugin->generate($election);

    $stats['all_votes'] = ['ballots' => $summary['ballots']];
    $stats['total_voters'] = $summary['voters'];

    $stats['total_possible_voters'] = max(static::getTotalPossibleVoters($election), $stats['total_voters']);

    $_SESSION['uclu_total_possible_voters'] = $stats['total_possible_voters'];

    $stats['total_turnout'] = ($stats['total_voters'] / $stats['total_possible_voters']) * 100;

    static::setJsonFile($election, 'main', $ballot_count, $expiry, $stats);
  }

  public static function getTotalPossibleVoters(ElectionInterface $election) {
    if ($election->hasField('field_stats_possible_voters') && $election->field_stats_possible_voters->value > 0) {
      return $election->field_stats_possible_voters->value;
    }

    return (int) \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', ['member'], 'IN')
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  public static function calculateleaderboards($election_id, $ballot_count, $expiry) {
    $election = Election::load($election_id);
    // Define the top level of the schema:
    $stats = [
      "department_turnout"  => [],
      "faculty_turnout"     => [],
      "hall_turnout"        => [],
      "club_soc_turnout"    => [],
      "study_mode_turnout"  => [],
    ];

    $pluginManager = \Drupal::service('plugin.manager.election_statistics');
    $definitions = $pluginManager->getDefinitions();

    $turnouts = ['club_soc_turnout', 'department_turnout', 'faculty_turnout', 'hall_turnout'];
    foreach ($turnouts as $turnout) {
      $plugin = $pluginManager->createInstance($turnout, $definitions[$turnout]);
      $statsForPlugin = $plugin->generate($election);
      foreach ($statsForPlugin as $stat) {
        if (!isset($stat['name']) || !$stat['name']) {
          continue;
        }
        $stats[$turnout][] = [
          'label'           => $stat['name'],
          'voters'          => $stat['count'],
          'possible_voters' => $stat['total'],
          'turnout'         => $stat['turnout'],
          'sort' => $stat['sort'],
        ];
      }
    }

    $pies = ['study_mode_turnout'];
    foreach ($pies as $turnout) {
      $plugin = $pluginManager->createInstance($turnout, $definitions[$turnout]);
      $statsForPlugin = $plugin->generate($election);

      $labels = [];
      $data = [];
      foreach ($statsForPlugin as $stat) {
        $labels[] = $stat['name'];
        $data[] = $stat['count'];
      }

      $stats[$turnout] = [
        'datasets' => [
          [
            'label' => '# of voters',
            'data' => $data,
            'backgroundColor' => [
              '#f26640',
              '#082244',
              '#fec340',
              '#2aaa9e',
            ],
            'borderWidth' => 0,
          ]
        ],
        'labels' => $labels,
      ];
    }
    static::setJsonFile($election, 'leaderboards', $ballot_count, $expiry, $stats, $expiry);
  }

  public static function calculatemap($election_id, $ballot_count, $expiry) {
    $election = Election::load($election_id);

    // Define the top level of the schema
    $stats = [
      "voters_per_countries"  => []
    ];
    //IPs to countries
    $query_ips = SummaryElectionStatistic::getTopBallotFieldValues($election, 'ip', -1);

    $records = [];
    if (count($query_ips)) {
      $ips = static::getJsonFile($election, 'ip_country');
      if (!$ips) {
        $ips = [];
      }

      foreach ($query_ips as $row) {
        if ($row && $row->ip != '') {
          // Get IP from cached JSON data...
          $countryCode = isset($ips[$row->ip]) ? $ips[$row->ip] : NULL;

          // Otherwise load it via ip2country...
          if (!$countryCode) {
            $countryCode = \Drupal::service('ip2country.lookup')->getCountry($row->ip);
            $ips[$row->ip] = $countryCode;
            static::setJsonFile($election, 'ip_country', $ballot_count, -1, $ips);
          }

          if (!$countryCode) {
            continue;
          }

          if (!isset($records[$countryCode])) {
            $records[$countryCode] = 0;
          }
          $records[$countryCode] += $row->voters;
        }
      }

      foreach ($records as $key => $record)
        if ($key !== 0) {
          $stats['voters_per_countries'][] = [
            'country' => $key,
            'value' => $record,
          ];
        }
    }

    static::setJsonFile($election, 'map', $ballot_count, $expiry, $stats);
  }

  public static function calculatevotes_over_time($election_id, $ballot_count, $expiry) {
    $election = Election::load($election_id);
    $pluginManager = \Drupal::service('plugin.manager.election_statistics');
    $definitions = $pluginManager->getDefinitions();
    $plugin = $pluginManager->createInstance('votes_over_time', $definitions['votes_over_time']);
    $stats = $plugin->generate($election);

    static::setJsonFile($election, 'votes_over_time', $ballot_count, $expiry, ['votes' => $stats['Votes']]);
  }

  public static function studentDemographicsVoters(ElectionInterface $election) {
    $uids = $election->getVoterUserIds();
    return static::studentDemographics($election, $uids);
  }

  public static function studentDemographicsCandidates(ElectionInterface $election) {
    $candidate_ids = $election->getCandidateIds();
    $uids = [];
    foreach ($candidate_ids as $candidate_id) {
      $candidate = ElectionCandidate::load($candidate_id);
      $uids[] = $candidate->getOwner()->getEmail();
      unset($candidate);
    }
    return static::studentDemographics($election, $uids);
  }

  public static function studentDemographics(ElectionInterface $election, array $uids) {
    foreach ($uids as $uid) {
      $entity_ids[] = ['user', $uid];
    }
    $entity_ids = array_filter($entity_ids);

    $plugins_to_include = NULL;

    $tempstore = \Drupal::service('tempstore.private')->get('entity_statistics_extensible');
    $unique_id = 'election_stats' . $election->id() . serialize($entity_ids);
    $set_id = md5($unique_id);

    $tempstore->set($set_id, [
      'entity_ids' => $entity_ids,
      'plugins_to_include' => $plugins_to_include,
      'url' => Url::fromRoute('su_statistics.student_quick_analysis_result', ['set_id' => $set_id])->toString(),
    ]);

    $url = Url::fromRoute('entity_statistics_extensible.batch', ['entity_statistics_set' => $set_id])->toString();
    $response = new RedirectResponse($url);
    $response->send();
    return NULL;
  }
}
