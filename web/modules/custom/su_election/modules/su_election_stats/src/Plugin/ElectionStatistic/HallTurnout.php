<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

/**
 * @ElectionStatistic(
 *  id = "hall_turnout",
 *  label = @Translation("Halls of Residence turnout"),
 * )
 */
class HallTurnout extends StudentTaxonomyTurnoutBase {
  const VALUE_LABEL = 'Hall of Residence';
  const STUDENT_FIELD = 'field_student_hall_of_residence';
  const STUDENT_FIELD_VOCABULARY = 'students_halls_of_residence';
}
