<?php

namespace Drupal\su_election_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\election\Phase\VotingPhase;

/**
 * @Block(
 *   id = "su_election_stats_block",
 *   admin_label = @Translation("Union election statistics block"),
 *   category = @Translation("Elections"),
 * )
 */
class SuElectionStatsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $election = election_get_election_from_context();
    if (!$election) {
      $election = \Drupal::service('su_drupal_helper')->getRouteEntity();
    }
    if (!$election) {
      return [];
    }

    $voter_count = number_format($election->countVoters(), 0, ".", ",");
    $open = $election->isOpenOrPartiallyOpen(new VotingPhase());

    if ($voter_count > 0) {
      $count = [
        '#markup' => '<div class="election_stats__big_counter">' . $voter_count . '</div><div class="election_stats__big_counter_label">' . ($open ? 'voters so far' : 'voters') . '</div>',
        '#cache' => array(
          'contexts' => array(
            'url.path',
          )
        )
      ];
      $more = [
        '#markup' => t('<a href="@url" class="btn button">View live stats</a>', ['@url' => Url::fromRoute('su_election_stats.dashboard', ['election' => $election->id()])->toString()]),
        '#cache' => array(
          'contexts' => array(
            'url.path',
          )
        )
      ];

      return [
        $count,
        $more,
      ];
    }

    return [];
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 60 * 3; // 3 minutes
  }
}
