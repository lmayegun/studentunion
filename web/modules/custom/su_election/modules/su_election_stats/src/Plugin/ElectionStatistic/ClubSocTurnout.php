<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

/**
 * @ElectionStatistic(
 *  id = "club_soc_turnout",
 *  label = @Translation("Clubs and Societies turnout"),
 * )
 */
class ClubSocTurnout extends GroupTurnoutBase {
  const VALUE_LABEL = 'Club/Society';
  const GROUP_TYPE = 'club_society';
  const GROUP_ROLE = 'club_society-member';
  const GROUP_LABEL = 'Club/Society';
  const MINIMUM_MEMBERS = 30;
}
