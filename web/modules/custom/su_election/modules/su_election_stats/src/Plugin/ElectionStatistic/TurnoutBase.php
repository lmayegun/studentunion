<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionBallot;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election_statistics\Plugin\ElectionStatistic\ElectionStatisticBase;
use Drupal\election_statistics\Plugin\ElectionStatistic\ElectionStatisticInterface;

abstract class TurnoutBase extends ElectionStatisticBase implements ElectionStatisticInterface {
  const MINIMUM_MEMBERS = 30;
  const VALUE_LABEL = 'Club/Society';
  const EXCLUSIVE_VALUES = FALSE;
  const SORT_BY = 'turnout';

  const RECALCULATE_EVERY_TIME = TRUE;

  const ROWS_TO_LOAD = 30;
  public $recordsToLoad = NULL;

  public function getRecordsToLoad() {
    if ($this->recordsToLoad) {
      return $this->recordsToLoad;
    }
    return static::ROWS_TO_LOAD;
  }

  public function setRecordsToLoad($value) {
    $this->recordsToLoad = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    $result = [];

    $uids = $entity->getVoterUserIds();
    $uids = array_unique($uids);

    $overall_total = 0;

    $statProfile = $this->getStatProfile();
    foreach ($uids as $uid) {
      $userProfile = $this->getUserProfile($uid);
      $possible_values = $userProfile['possible_values'];
      foreach ($possible_values as $possible_value) {
        if (!isset($result[$possible_value])) {
          if (!isset($statProfile['names'][$possible_value]) || !isset($statProfile['totals'][$possible_value])) {
            // Could be unpublished, at which point we don't show it.
            continue;
          }
          $result[$possible_value] = [
            'name' => $statProfile['names'][$possible_value],
            'total' => $statProfile['totals'][$possible_value],
            'overall_total' => $statProfile['overall_total'] ?? 0,
            'total_population' => $statProfile['total_population'] ?? 0,
            'count' => 0,
          ];
        }
        $result[$possible_value]['count']++;
        $overall_total++;
        // dd($result);
      }
    }

    foreach ($result as $possible_value => $data) {
      if (static::MINIMUM_MEMBERS && $data['total'] && $data['total'] < static::MINIMUM_MEMBERS) {
        unset($result[$possible_value]);
        continue;
      }

      if (!$data['total']) {
        $result[$possible_value]['turnout'] = 0;
      } else {
        $result[$possible_value]['turnout'] = round(($data['count'] / $data['total']) * 100, 2);
      }
      $result[$possible_value]['uids']    = $uids;
      if (static::EXCLUSIVE_VALUES) {
        // dd($data['name'], $data['count'], $statProfile['overall_total'], ($data['count'] / $statProfile['overall_total']));
        $result[$possible_value]['proportion'] = round(($data['count'] / $overall_total) * 100, 2);
        $result[$possible_value]['proportion_of_total_population'] = round(($data['count'] / $statProfile['total_population']) * 100, 2);
      }

      $sortValue = isset($result[$possible_value][static::SORT_BY]) ? $result[$possible_value][static::SORT_BY] : $result[$possible_value]['turnout'];
      $result[$possible_value]['sort'] = $sortValue * 1000;
    }

    usort($result, function ($first, $second) {
      if (!isset($first['sort'])) {
        return 1;
      }
      if (!isset($second['sort'])) {
        return -1;
      }
      return $second['sort'] > $first['sort'];
    });

    if ($this->getRecordsToLoad() > 0 && count($result) > $this->getRecordsToLoad()) {
      $result = array_slice($result, 0, $this->getRecordsToLoad(), TRUE);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function build(ElectionEntityInterface $entity) {
    $raw = $this->get($entity);

    $stats = [];
    $count = 0;
    foreach ($raw as $row) {
      $count++;
      if ($this->getRecordsToLoad() > 0 && $count > $this->getRecordsToLoad()) {
        break;
      }

      if (static::MINIMUM_MEMBERS && $row['total'] < static::MINIMUM_MEMBERS) {
        continue;
      }

      $stat = [
        'soc' => $row['name'],
        'voters_members' => number_format($row['count'], 0, ".", ",") . ' / ' . number_format($row['total'], 0, ".", ","),
        'turnout' => $row['turnout'] . '%',
      ];

      if (static::EXCLUSIVE_VALUES) {
        $stat['proportion'] = $row['proportion'] . '%';
        $stat['proportion_of_total_population'] = $row['proportion_of_total_population'] . '%';
      }

      $stats[] = $stat;
    }

    $header = [
      t(static::VALUE_LABEL),
      t('Voters / Members'),
      t('Turnout'),
    ];

    if (static::EXCLUSIVE_VALUES) {
      $header[] = t('Proportion of voters');
      $header[] = t('Proportion of possible voters');
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $stats,
    ];
  }
}
