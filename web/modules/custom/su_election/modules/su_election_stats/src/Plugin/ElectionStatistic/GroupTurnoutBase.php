<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;

abstract class GroupTurnoutBase extends TurnoutBase {
  const MINIMUM_MEMBERS = 30;
  const GROUP_TYPE = 'club_society';
  const GROUP_ROLE = 'club_society-member';
  const VALUE_LABEL = 'Club/Society';

  public function getGroupIdsForUser($uid) {
    $group_ids = [];

    // We load this with a direct query to avoid loading the membership entities.
    // (Slows performance).
    $connection = \Drupal::database();
    // Load memberships for this group:
    $query = $connection->select('group_content_field_data', 'gc');
    $query->condition('gc.type', 'club_society-group_membership');
    $query->condition('gc.entity_id', $uid);
    // Restrict by role:
    $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
    $query->condition('r.group_roles_target_id', 'club_society-member');
    // Get user IDs (entity_id)
    $query->fields('gc', ['gid']);
    $results = $query->distinct()->execute()->fetchCol();
    array_filter($results);

    $group_ids = $results;

    // $membershipService = \Drupal::service('group.membership_loader');
    // $memberships = $membershipService->loadByUser(User::load($uid), [static::GROUP_ROLE]);
    // foreach ($memberships as $membership) {
    //   $group = $membership->getGroup();
    //   if ($group->bundle() == static::GROUP_TYPE) {
    //     $group_ids[] = $group->id();
    //   }
    // }

    return $group_ids;
  }

  /**
   * Get member UIDs, cached daily.
   *
   * @param GroupInterface $group
   *
   * @return [type]
   */
  public function getMemberIdsCount(GroupInterface $group) {
    $cid = 'election_statistics:' . static::GROUP_TYPE . ':' . static::GROUP_ROLE . ':' . $group->id();
    if ($cache = \Drupal::cache('election')->get($cid)) {
      return $cache;
    } else {
      // We load this with a direct query to avoid loading the membership entities.
      // (Slows performance).
      $connection = \Drupal::database();
      // Load memberships for this group:
      $query = $connection->select('group_content_field_data', 'gc');
      $query->condition('gc.type', 'club_society-group_membership');
      $query->condition('gc.gid', $group->id());
      // Restrict by role:
      $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
      $query->condition('r.group_roles_target_id', 'club_society-member');
      // Get user IDs (entity_id)
      $query->fields('gc', ['entity_id']);
      $results = $query->distinct()->execute()->fetchCol();
      array_filter($results);
      $total = count($results);

      // $membershipService = \Drupal::service('group.membership_loader');
      // $memberships = $membershipService->loadByGroup($group, [static::GROUP_ROLE]);
      // $uids = [];
      // foreach ($memberships as $membership) {
      //   $uids[] = $membership->getUser()->id();
      // }

      \Drupal::cache('election')->set($cid, $total, 60 * 60 * 24);
    }
    return $total;
  }

  public function generateStatProfile() {
    $totals = [];
    $names = [];
    $overall_total = 0;

    $group_ids = \Drupal::entityQuery('group')
      ->condition('type', static::GROUP_TYPE)
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();
    $groups = Group::loadMultiple($group_ids);
    foreach ($groups as $group) {
      $count = $this->getMemberIdsCount($group);
      $totals[$group->id()] = $count;
      $overall_total += $count;

      $names[$group->id()] = $group->label();
    }
    return [
      'totals' => $totals,
      'names' => $names,
      'overall_total' => $overall_total,
    ];
  }

  public function generateUserProfile($account) {
    return [
      'possible_values' => $this->getGroupIdsForUser($account->id()),
    ];
  }

  public function getCacheTagsUserProfile(AccountInterface $account) {
    return [
      // 'group_list:' . static::GROUP_TYPE,
      'group:membership_records:user:' . $account->id()
    ];
  }

  public function getCacheTagsStatProfile() {
    return [
      'group_list:' . static::GROUP_TYPE,
    ];
  }
}
