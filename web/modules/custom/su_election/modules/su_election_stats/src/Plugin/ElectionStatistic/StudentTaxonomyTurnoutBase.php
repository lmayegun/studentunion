<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\taxonomy\Entity\Term;

abstract class StudentTaxonomyTurnoutBase extends TurnoutBase {
  const EXCLUSIVE_VALUES = TRUE;

  const STUDENT_FIELD = '';
  const STUDENT_FIELD_VOCABULARY = '';

  /**
   * Get member UIDs, cached daily.
   *
   * @param GroupInterface $group
   *
   * @return [type]
   */
  public function getMemberIdsCount($entity) {
    $cid = 'election_statistics:count:' . static::STUDENT_FIELD . ':' . $entity->id();
    if ($cache = \Drupal::cache('election')->get($cid)) {
      return $cache;
    } else {
      // @todo get students with that term on this field
      $uids = [];
      $ids = \Drupal::entityQuery('profile')
        ->condition('type', 'student')
        ->condition('status', 1)
        ->condition(static::STUDENT_FIELD . '.entity:taxonomy_term.tid', $entity->id())
        ->accessCheck(FALSE)
        ->execute();
      $total = count($ids);

      \Drupal::cache('election')->set($cid, $total, 60 * 60 * 24);
    }
    return $total;
  }

  public function generateStatProfile() {
    $totals = [];
    $names = [];
    $total_population = 0;

    $ids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', static::STUDENT_FIELD_VOCABULARY)
      ->condition('status', 1)
      ->execute();
    $entities = Term::loadMultiple($ids);
    foreach ($entities as $entity) {
      $count = $this->getMemberIdsCount($entity);
      $totals[$entity->id()] = $count;
      $total_population += $count;

      $names[$entity->id()] = $entity->label();
    }
    $result = [
      'totals' => $totals,
      'names' => $names,
      'total_population' => $total_population,
    ];
    return $result;
  }

  public function generateUserProfile($account) {
    $studentService = \Drupal::service('su_students.student_services');
    $profile = $studentService::getStudentProfile($account);
    // @todo just get ID to avoid loading the term:
    $term = NULL;
    if ($profile) {
      $term = $profile->get(static::STUDENT_FIELD)->entity;
    }
    return [
      'possible_values' => $term ? [$term->id()] : [],
    ];
  }

  public function getCacheTagsUserProfile(AccountInterface $account) {
    $studentService = \Drupal::service('su_students.student_services');
    $profile = $studentService::getStudentProfile($account);
    if ($profile) {
      return $profile->getCacheTags();
    } else {
      return [];
    }
  }

  public function getCacheTagsStatProfile() {
    return [
      'taxonomy_term_list:' . static::STUDENT_FIELD_VOCABULARY,
    ];
  }
}
