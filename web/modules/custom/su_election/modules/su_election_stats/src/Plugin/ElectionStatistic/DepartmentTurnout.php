<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

/**
 * @ElectionStatistic(
 *  id = "department_turnout",
 *  label = @Translation("Department turnout"),
 * )
 */
class DepartmentTurnout extends StudentTaxonomyTurnoutBase {
  const VALUE_LABEL = 'Department';
  const STUDENT_FIELD = 'field_student_department';
  const STUDENT_FIELD_VOCABULARY = 'ucl_faculties_and_departments';
}
