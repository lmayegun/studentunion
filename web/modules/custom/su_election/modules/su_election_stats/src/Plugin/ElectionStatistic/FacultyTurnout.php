<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

/**
 * @ElectionStatistic(
 *  id = "faculty_turnout",
 *  label = @Translation("Faculty turnout"),
 * )
 */
class FacultyTurnout extends StudentTaxonomyTurnoutBase {
  const VALUE_LABEL = 'Faculty';
  const STUDENT_FIELD = 'field_student_faculty';
  const STUDENT_FIELD_VOCABULARY = 'ucl_faculties_and_departments';
}
