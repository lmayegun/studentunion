<?php

namespace Drupal\su_election_stats\Plugin\ElectionStatistic;

/**
 * @ElectionStatistic(
 *  id = "study_mode_turnout",
 *  label = @Translation("Study mode turnout"),
 * )
 */
class StudyModeTurnout extends StudentTaxonomyTurnoutBase {
  const VALUE_LABEL = 'Study mode';
  const STUDENT_FIELD = 'field_student_study_mode';
  const STUDENT_FIELD_VOCABULARY = 'students_study_modes';
}
