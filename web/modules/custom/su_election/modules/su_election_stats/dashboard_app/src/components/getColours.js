// Data generation

export function getRandomColours(numItems, type, shade) {
  let colors = [];

  for (let i = 0; i < numItems; i++) {
    colors.push("#" + type + Math.round(50 + i + 8) + shade);
  }
  return colors;
}


let fakeData = [
  {
    label: "Cancer Charities Alliance Society",
    possible_voters: "57",
    turnout: "19.3",
    voters: "11",
  },
  {
    label: "Caledonian Society",
    possible_voters: "36",
    turnout: "22.2",
    voters: "8",
  },
  {
    label: "Austrian Society",
    possible_voters: "33",
    turnout: "21.2",
    voters: "7",
  },
  {
    label: "European Law Students&#039; Association",
    possible_voters: "58",
    turnout: "20.7",
    voters: "12",
  },
  {
    label: "Ex-Yugoslav Society",
    possible_voters: "35",
    turnout: "20.0",
    voters: "7",
  },
];

  // function getData() {
  //   const sorted = fakeData.sort(() => 0.5 - Math.random());
  //   // console.log("getdata", this.state.fata, sorted);
  //   // this.setState({ fata: sorted });
  //   // console.log("getsorted", this.state.fata);
  //   //fakeData.sort();
  // }