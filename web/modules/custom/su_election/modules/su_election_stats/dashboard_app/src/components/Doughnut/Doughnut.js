import React from "react";
import { Doughnut } from 'react-chartjs-2';
import "./Doughnut.css";

export default class Donut extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.data) {
      return (<></>);
    }

    return <Doughnut
      data={this.props.data}
    />;
  }
}
