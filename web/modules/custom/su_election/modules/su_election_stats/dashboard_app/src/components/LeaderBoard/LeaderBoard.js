import React from "react";
import Button from "../Button/Button";
// import FlipMove from "react-flip-move";
import Label from "../Label/Label";
import BoardRow from "./BoardRow";
import HTMLReactParser from "html-react-parser";
import NoResultMessage from "../NoResultMessage/NoResultMessage";
//data changes (sorted/unsorted)
//display only first 5

export default function LeaderBoard(props) {
  if (!props.data) {
    return (<></>);
  }

  const shortAmount = 5;

  var rows = props.expanded ? props.data : props.data.slice(0, shortAmount);

  return (
    <div className={props.className}>
      <Label name={props.labelName} />
      <table cellSpacing="1" cellPadding="1">
        {rows.length > 0 ? rows.map((row, index) => (
          <BoardRow
            color={props.color}
            key={row.label}
            index={index}
            title={HTMLReactParser(row.label)}
            voters={row.voters}
            allVoters={row.possible_voters}
            turnout={row.turnout}
          />
        )) : (
          <NoResultMessage message="No data available yet." />
        )}
      </table>
      {rows.length > 0 && props.data.length > shortAmount ? (
        <Button onClick={() => { props.setExpanded(!props.expanded) }} text={props.expanded ? 'hide more rankings' : 'see more rankings'} />
      ) : (
        <></>
      )}
    </div>
  );
}
