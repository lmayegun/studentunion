import React from "react";
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS } from 'chart.js/auto'
import { Chart } from 'react-chartjs-2'
import { useMediaQuery } from 'react-responsive';

export default function LineChart(props) {
  const isTabletOrMobile = useMediaQuery({ maxWidth: 1224 })

  if (!props.data) {
    return (<></>);
  }

  var options = {
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: true,
      }
    },
    scales: {
      x: {
        ticks: {
          display: !isTabletOrMobile,
        },
        grid: {
          display: !isTabletOrMobile
        },
      },
      y: {
        grid: {
          display: !isTabletOrMobile
        },
      }
    }
  };

  var data = {
    labels: Object.keys(props.data),
    datasets: [
      {
        data: Object.values(props.data),

        pointRadius: 0,

        borderWidth: 2,
        borderColor: props.color,
        tension: 0.4,
      }
    ]
  };

  return (<Line options={options} data={data} />);
}
