import React from "react";
import CountUp from "react-countup";

export default function BoardRow(props) {
  return (
    // <div className="board-row"
    <tr className="leaderboard-row">
      <td className={`${props.color} place`}> {(props.index += 1)}</td>
      <td className="leaderboard-row-cel title">  <strong>{props.title}</strong>
        <br />
        <span className="subtitle">
          <CountUp
            decimals={0}
            preserveValue={true}
            end={props.voters}
            separator=","
            duration={5}
          /> voter{props.voters == 1 ? '' : 's'} out of {props.allVoters}</span>
      </td>
      <td className="leaderboard-row-cel turnout"><strong><CountUp
        decimals={2}
        preserveValue={true}
        end={Math.min(props.turnout, 100)}
        separator=","
        duration={5}
      />{"%"}</strong></td>
    </tr>
    // </div>
  );
}
