import React from "react";

export default function NoResultMessage(props) {

  return (
    <div className="no-result">
      {props.message}
    </div>
  );
}
