import React from "react";
import Doughnut from "./components/Doughnut/Doughnut";
import Label from "./components/Label/Label";
import BigCounter from "./components/BigCounter/BigCounter";
import NoResultMessage from "./components/NoResultMessage/NoResultMessage";
import "./App.css";
import LeaderBoard from "./components/LeaderBoard/LeaderBoard";
import LineChart from "./components/LineChart/LineChart";
import { ThreeCircles } from "react-loader-spinner";

const WorldMap = require('react-svg-worldmap').WorldMap;

const electionId = document.getElementById("root").className;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      main: this.getJson(),
      leaderboards: this.getLeaderboardsJson(),
      mapData: this.getMapJson(),
      votesOverTimeData: this.getVotesOverTimeJson(),
      leaderboardsExpanded: {
        'hall_turnout': false,
        'club_soc_turnout': false,
        'faculty_turnout': false,
        'department_turnout': false,
      },
    };
  }

  async getJson() {
    const apiStatsUrl = "/election/" +
      electionId +
      "/statistics/main/json";
    await
      fetch(apiStatsUrl, { cache: "no-cache" })
        .then((response) => response.json())
        .then((data) => this.setState({ main: data }));
  }
  async getLeaderboardsJson() {
    const apiStatsUrl = "/election/" +
      electionId +
      "/statistics/leaderboards/json";
    await
      fetch(apiStatsUrl, { cache: "no-cache" })
        .then((response) => response.json())
        .then((data) => this.setState({ leaderboards: data }));
  }

  async getMapJson() {
    const apiStatsMapUrl = "/election/" +
      electionId +
      "/statistics/map/json";
    await
      fetch(apiStatsMapUrl, { cache: "no-cache" })
        .then((response) => response.json())
        .then((mapData) => this.setState({ mapData: mapData }));
  }

  async getVotesOverTimeJson() {
    const apiStatsMapUrl = "/election/" +
      electionId +
      "/statistics/votes_over_time/json";
    await
      fetch(apiStatsMapUrl, { cache: "no-cache" })
        .then((response) => response.json())
        .then((votesOverTimeData) => this.setState({ votesOverTimeData: votesOverTimeData }));
  }

  componentDidCatch(error, info) {
    console.log(error, info);
    this.setState({
      error: true,
    });
  }

  async componentDidMount() {
    this.intervalMain = setInterval(() => this.getJson(), 1000 * 60 * 1); // minute
    this.intervalLeaderboards = setInterval(() => this.getLeaderboardsJson(), 1000 * 60 * 5); // five minutes
    this.intervalMap = setInterval(() => this.getMapJson(), 1000 * 60 * 60); // hour
    this.intervalVotesOverTime = setInterval(() => this.getVotesOverTimeJson(), 1000 * 60 * 60); // hour
  }

  componentWillUnmount() {
    clearInterval(this.intervalMain);
    clearInterval(this.intervalMap);
    clearInterval(this.intervalLeaderboards);
    clearInterval(this.intervalVotesOverTime);
  }

  render() {
    const { main, error } = this.state;
    const message = "";
    return error ? (
      <div className="error-message">
        <h1>Something went wrong loading the data. Please refresh the page or come back later.</h1>
      </div>
    ) : (
      <div className="App" name="Load JSON file">
        {main && main.total_voters >= 0 ? (
          <div className="dashboard-container" name="Load JSON file">
            <BigCounter
              className="top1 total-voters"
              data={this.state.main.total_voters}
              item="voters"
              duration="5"
            />

            <BigCounter
              className="top2 total-turnout"
              data={this.state.main.total_turnout.toFixed(2)}
              decimals={2}
              unit="%"
              item="turnout"
              duration="7"
            />

            <BigCounter
              className="top3 total-turnout"
              data={this.state.main.all_votes.ballots}
              item="ballots"
              duration="5"
            />

            <div className='map'>
              <Label name='Voters across the world' />
              {this.state.mapData.voters_per_countries ? (
                // console.log('map data', this.state.mapData) &&
                <WorldMap styleFunction={(context) => {
                  // console.log(this.state.mapData.voters_per_countries);
                  // Skew the data so the fact most votes are UK doesn't make the shading meaningless
                  var opacityLevel = 0;
                  if (context.countryValue == context.maxValue) {
                    opacityLevel = 1;
                  } else if (context.countryValue) {
                    opacityLevel = 0.1 + Math.pow((context.countryValue - context.minValue) / (context.maxValue - context.minValue), 0.3);
                  }
                  // console.log(context, opacityLevel);
                  return {
                    fill: context.color,
                    fillOpacity: opacityLevel,
                    stroke: "var(--su-navy)",
                    strokeWidth: 1,
                    strokeOpacity: 0.2,
                    cursor: "pointer"
                  };
                }}
                  borderColor="var(--su-navy)"
                  tooltipBgColor="var(--su-navy)"
                  frameColor="var(--su-navy)"
                  color="#004f6f"
                  size="xxl"
                  valueSuffix="voters"
                  data={this.state.mapData.voters_per_countries}
                />
              ) :
                (<NoResultMessage message="Apologies, there is no world map data available for this election yet." />)}
            </div>

            <LeaderBoard
              className="board1"
              labelName="Top turnout per club or society"
              data={this.state.leaderboards.club_soc_turnout ? this.state.leaderboards.club_soc_turnout : null}
              expanded={this.state.leaderboardsExpanded['club_soc_turnout']}
              setExpanded={(value) => {
                var expanded = this.state.leaderboardsExpanded;
                expanded['club_soc_turnout'] = value;
                this.setState({ leaderboardsExpanded: expanded });
              }}
              buttonUrl={`/election/${electionId}/statistics/club_soc_turnout`}
              buttonText="see full ranking"
              color='green'
            />

            <LeaderBoard
              className="board2"
              labelName="Top turnout per department"
              data={this.state.leaderboards.department_turnout ? this.state.leaderboards.department_turnout : null}
              expanded={this.state.leaderboardsExpanded['department_turnout']}
              setExpanded={(value) => {
                var expanded = this.state.leaderboardsExpanded;
                expanded['department_turnout'] = value;
                this.setState({ leaderboardsExpanded: expanded });
              }}
              buttonUrl={`/election/${electionId}/statistics/department_turnout`}
              buttonText="see full ranking"
              color='blue'
            />

            <LeaderBoard
              className="board3"
              labelName="Top turnout per faculty"
              data={this.state.leaderboards.faculty_turnout ? this.state.leaderboards.faculty_turnout : null}
              expanded={this.state.leaderboardsExpanded['faculty_turnout']}
              setExpanded={(value) => {
                var expanded = this.state.leaderboardsExpanded;
                expanded['faculty_turnout'] = value;
                this.setState({ leaderboardsExpanded: expanded });
              }}
              buttonUrl={`/election/${electionId}/statistics/faculty_turnout`}
              buttonText="See full ranking"
              color='orange'
            />

            <LeaderBoard
              className="board4"
              labelName="Top turnout per hall of residence"
              data={this.state.leaderboards.hall_turnout ? this.state.leaderboards.hall_turnout : null}
              expanded={this.state.leaderboardsExpanded['hall_turnout']}
              setExpanded={(value) => {
                var expanded = this.state.leaderboardsExpanded;
                expanded['hall_turnout'] = value;
                this.setState({ leaderboardsExpanded: expanded });
              }}
              buttonUrl={`/election/${electionId}/statistics/hall_turnout`}
              buttonText="See full ranking"
              color='pink'
            />

            {this.state.leaderboards.study_mode_turnout ? (
              <div className="pie">
                <Label name="Voters by mode of study" />
                <div className="chart-wrapper" style={{ "position": "relative" }}>
                  <Doughnut
                    data={this.state.leaderboards.study_mode_turnout ? this.state.leaderboards.study_mode_turnout : null}
                    title={main.election_name}
                  />
                </div>
              </div>
            ) : (
              // <NoResultMessage message={"No study mode"} />
              <></>
            )}

            {this.state.votesOverTimeData ? (
              <div className="votes_over_time">
                <Label name={"Votes over time"} />
                <div className="chart-wrapper" style={{ "position": "relative" }}>
                  <LineChart
                    labelName="Votes over time"
                    data={this.state.votesOverTimeData ? this.state.votesOverTimeData.votes : null}
                    color='orange'
                  />
                </div>
              </div>) : (<></>)}

            {/*
            {data.votes_per_post.length > 0 ? (
              <div className="sub chart-wrapper">
                <Label name="Votes per post" />
                <HorizontalBar
                  className="chart-per-post"
                  data={data.votes_per_post.slice(0, 10)}
                  color={getRandomColours(
                    data.club_soc_turnout.length,
                    "00",
                    "9F"
                  )}
                />
              </div>
            ) : (
              <NoResultMessage message={message} />
            )} */}
          </div>
        ) : (
          <div className="center-flex-child">
            <ThreeCircles

              color="#082244"
              middleCircleColor="#f26640"
              outerCircleColor="#fec340"
              height={110}
              width={110}
              style={{ margin: "0 auto" }}
              ariaLabel="three-circles-rotating"
            />
            <NoResultMessage message="Loading... If there is no data, please come back when voting has opened!" />
          </div>
        )
        }
      </div>
    );
  }
}

export default App;
