import React from "react";
import "./BigCounter.css";
import CountUp from "react-countup";
// import { getElection} from './data/getData';

export default function BigCounter(props) {
  return (
    <div className={props.className}>
      <div className="bigCounter">
        <CountUp
          decimals={props.decimals}
          preserveValue={true}
          end={props.data}
          separator=","
          duration={props.duration}
        />
        {props.unit}
      </div>
      <div className="counterLabel label">{props.item}</div>
      {props.description ? (<p>{props.description}</p>) : <></>}
    </div>
  );
}
