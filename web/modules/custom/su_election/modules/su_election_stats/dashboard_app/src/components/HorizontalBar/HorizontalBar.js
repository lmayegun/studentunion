import React from "react";
import { HorizontalBar } from 'react-chartjs-2';

export default class HorizontalBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var data = {
      type: "horizontalBar",
      options: {
        maintainAspectRatio: true,
        scales: {
          yAxes: [
            {
              ticks: {
                min: 0,
                max: 100
              }
            }
          ]
        }
      },
      data: {
        labels: this.props.data.map(d => d.label),
        datasets: [
          {
            label: "% turnout",
            data: this.props.data.map(d => d.turnout),
            backgroundColor: this.props.color
          }
        ]
      }
    };
    return <HorizontalBar data={data} />;
  }
}
