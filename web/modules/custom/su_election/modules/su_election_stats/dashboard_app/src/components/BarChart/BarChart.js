import React from "react";
import { BarChart } from 'react-chartjs-2';

export default class BarChart extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var data = {
      type: "bar",
      options: {
        maintainAspectRatio: true,
        scales: {
          xAxes: [
            {
              ticks: {
                min: 0,
                max: 100
              }
            }
          ]
        }
      },
      data: {
        labels: this.props.data.map(d => d.label),
        datasets: [
          {
            label: "% turnout",
            data: this.props.data.map(d => d.turnout),
            backgroundColor: this.props.color
          }
        ]
      }
    };
    return (<BarChart data={data} />);
  }
}
