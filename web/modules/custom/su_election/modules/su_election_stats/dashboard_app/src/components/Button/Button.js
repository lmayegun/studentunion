import React from "react";

export default function Button(props) {
  return <a onClick={props.onClick ? props.onClick : () => { }} className="union-button" href={props.link}>{props.text}</a>;
}
