/**
 * @file
 *
 * Handle posts page with views_infinite_scroll
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.loadMorePostsViaInfiniteScroll = {
    attach: function (context, settings) {
      'use strict';

      // Create an observer to watch for new loads via AJAX
      const targetNodes = document.getElementsByClassName('views-infinite-scroll-content-wrapper');
      const targetNode = targetNodes[0];
      const config = { attributes: true, childList: true, subtree: true };

      // Callback to merge tables based on the caption
      const callback = function (mutationsList, observer) {
        var container = $('.views-infinite-scroll-content-wrapper');

        container.children('table').each(function (index) {
          var eligibility = $(this).children('caption').text().trim();
          if (!$(this).attr('eligibility')) {
            $(this).attr('eligibility', eligibility);
          }

          // Merge into previous matching if exists
          const previousMatching = $(this).prevAll('table[eligibility="' + eligibility + '"]');
          if (previousMatching.length > 0) {
            var previousTable = previousMatching.first();
            var currentTableRows = $(this).find('tbody').html();
            previousTable.find('tbody').append(currentTableRows)
            $(this).remove();
          } else {
            if (!$(this).attr('eligibility-order')) {
              var order = drupalSettings.election_eligibility_order.indexOf(eligibility);
              order = order == -1 ? 10000 : order;
              // $(this).attr('eligibility-order', order);
              $(this).css('order', order);
            }
          }
        });
      };

      // Initiate observer
      if (targetNode) {
        const observer = new MutationObserver(callback);
        observer.observe(targetNode, config);

        // If there's a "Load more" button in the view, click it!
        setInterval(function () {
          if ($('[rel=next]')) {
            $('[rel=next]').click();
          }
        }, 300);
      }
    }
  };
}(jQuery, Drupal, drupalSettings));
