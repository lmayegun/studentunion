import React from 'react';
import { render } from 'react-dom';
import Timeline from  "./components/Timeline";
import "react-calendar-timeline/lib/Timeline.css";
 
const Root = () => {
 return (
    <div>
      <Timeline />
    </div>
 )
}
 
render(<Root/>, document.querySelector('#simplybook-timeline-app'));
