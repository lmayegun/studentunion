import moment from "moment";
import axios from "axios";

const api_url = 'https://user-api.simplybook.me';
const api_login = 'uclstudentsunion';
const api_key = '4babac427256def684f46829078ae2e86917b4db3ac049b49a5b9cb815cf6206';

// Only once token found
async function apiCall(token, method, params) {
    const call = await axios.post(api_url, {
        jsonrpc: '2.0',
        withCredentials: true,
        id: + new Date(),
        method: method,
        params: params
    }, {
        headers: {
            'X-Company-Login': api_login,
            'X-Token': token
        }
    });
    console.log('simplybook', 'apiCall', call);
    return call.data;
}

export async function getSimplyBookToken() {
    const tokenCall = await axios.post(api_url + '/login', {
        jsonrpc: '2.0',
        id: + new Date(),
        method: 'getToken',
        params: {
            'companyLogin': api_login,
            'apiKey': api_key
        },
    });
    const token = tokenCall.data.result;
    // console.log('simplybook', 'tokenCall', tokenCall);
    // console.log('simplybook', 'token', token);
    return token;
}

export async function getEvents(token) {
    const events = apiCall(token, 'getEventList', {
        'isVisibleOnly': true,
        'asArray': true,
        'handleClasses': null,
        'searchString': null
    });
}
export async function getBookings(token) {
    const events = await apiCall(token, 'getClientBookings ', {
        'isVisibleOnly': true,
        'asArray': true,
        'handleClasses': null,
        'searchString': null
    });
    console.log(events);
    return [
        {
            id: 1,
            group: 1,
            title: 'item 1',
            start_time: moment(),
            end_time: moment().add(1, 'hour')
        },
        {
            id: 2,
            group: 2,
            title: 'item 2',
            start_time: moment().add(-0.5, 'hour'),
            end_time: moment().add(0.5, 'hour')
        },
        {
            id: 3,
            group: 1,
            title: 'item 3',
            start_time: moment().add(2, 'hour'),
            end_time: moment().add(3, 'hour')
        }
    ];
}

export async function getGroups(token) {
    const serviceProvidersCall = await apiCall(token, 'getUnitList', {
        'isVisibleOnly': true,
        'asArray': true,
        'handleClasses': null,
        'searchString': null
    });
    const serviceProviders = serviceProvidersCall.result;
    console.log('simplybook', 'serviceProviders', serviceProviders);
    var groups = [];
    serviceProviders.forEach(function (item, index) {
        groups.push(
            {
                id: parseInt(item.id),
                title: item.name
            }
        );
      });
    console.log('simplybook', 'final serviceProviders groups', groups);
    return groups;
    // return [{ id: 1, title: 'group 1' }, { id: 2, title: 'group 2' }];
}
