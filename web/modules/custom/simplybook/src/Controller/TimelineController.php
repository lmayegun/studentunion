<?php

namespace Drupal\simplybook\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\feeds\Entity\FeedType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TimelineController.
 */
class TimelineController extends ControllerBase implements ContainerInjectionInterface {

    public function render() {
        $api = \Drupal::service('simplybook.api');

        return array(
            '#markup' => '<div id="simplybook-timeline-app"></div>',
            '#attached' => [
                'library' => 'simplybook/timeline'
              ],
          );
    }
}
