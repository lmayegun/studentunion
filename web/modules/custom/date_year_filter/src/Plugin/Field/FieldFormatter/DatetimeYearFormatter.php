<?php

namespace Drupal\date_year_filter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase;

/**
 * Plugin implementation of the 'datetime_year' formatter
 *
 * @FieldFormatter(
 *   id = "datetime_year",
 *   label = @Translation("Date as year"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DatetimeYearFormatter extends DateTimeFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'start_of_year' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['start_of_year'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start of year'),
      '#description' => $this->t('Format Y-m-d, e.g. 2025-12-31. Defaults to Jan 1st. The actual year you select here does not matter, just the day and month. You could vary it to filter by e.g. academic or tax years.'),
      '#default_value' => $this->getSetting('start_of_year') ? $this->getSetting('start_of_year') : (Date('Y') . '-01-01'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date) {
    $yearService = \Drupal::getContainer()->get('date_year_filter.year');
    $year = $yearService->getYearForDate($date, $this->getSetting('start_of_year') ? $this->getSetting('start_of_year') : (Date('Y') . '-01-01'));
    return $year->name;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // @todo Evaluate removing this method in
    // https://www.drupal.org/node/2793143 to determine if the behavior and
    // markup in the base class implementation can be used instead.
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->date)) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
        $date = $item->date;

        $elements[$delta] = $this->buildDate($date);
      }
    }

    return $elements;
  }
}
