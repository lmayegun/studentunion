<?php

namespace Drupal\date_year_filter\Plugin\Field\FieldFormatter;

use DateTime;
use DateTimeZone;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangePlainFormatter;
use Drupal\datetime_range\DateTimeRangeTrait;

/**
 * Plugin implementation of the 'daterange_year' formatter
 *
 * @FieldFormatter(
 *   id = "daterange_year",
 *   label = @Translation("Date range as year"),
 *   field_types = {
 *     "daterange",
 *   }
 * )
 */
class DateRangeYearFormatter extends DateRangePlainFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'start_of_year' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['start_of_year'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start of year'),
      '#description' => $this->t('Format Y-m-d, e.g. 2025-12-31. Defaults to Jan 1st. The actual year you select here does not matter, just the day and month. You could vary it to filter by e.g. academic or tax years.'),
      '#default_value' => $this->getSetting('start_of_year') ? $this->getSetting('start_of_year') : (Date('Y') . '-01-01'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $separator = $this->getSetting('separator');

    foreach ($items as $delta => $item) {
      if (!empty($item->start_date) && !empty($item->end_date)) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $start_date */
        $start_date = $item->start_date;
        $start_date->setTimezone(new DateTimeZone(date_default_timezone_get()));
        /** @var \Drupal\Core\Datetime\DrupalDateTime $end_date */
        $end_date = $item->end_date;
        $end_date->setTimezone(new DateTimeZone(date_default_timezone_get()));

        $yearService = \Drupal::getContainer()->get('date_year_filter.year');
        $startOfYear = $this->getSetting('start_of_year') ? $this->getSetting('start_of_year') : (Date('Y') . '-01-01');
        $yearStart = $yearService->getYearForDate($start_date, $startOfYear);
        $yearEnd = $yearService->getYearForDate($end_date, $startOfYear);

        if ($yearStart && $yearEnd && $yearStart->name != $yearEnd->name) {
          $elements[$delta] = [
            'start_date' => [
              '#markup' => $yearStart->name,
            ],
            'separator' => [
              '#plain_text' => ' ' . $separator . ' ',
            ],
            'end_date' => [
              '#markup' => $yearEnd->name,
            ],
          ];
        } elseif ($yearStart) {
          $elements[$delta] = ['#markup' => $yearStart->name];
        } else {
          $elements[$delta] = ['#markup' => 'Unknown'];
        }
      }
    }

    return $elements;
  }
}
