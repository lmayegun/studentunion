<?php

namespace Drupal\date_year_filter\Plugin\views\filter;

use DateInterval;
use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;

/**
 * Filters by year non-standard or not
 *
 * https://git.drupalcode.org/project/custom_view_filters/-/blob/2.x/src/Plugin/views/filter/DateRangePickerFilter.php
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("in_year")
 */
class YearFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function canBuildGroup() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    $form['value'] = !empty($form['value']) ? $form['value'] : [];
    parent::buildExposedForm($form, $form_state);
    $filter_id = $this->getFilterId();
    // Field which really filters.
    $form[$filter_id] = [
      '#type' => 'hidden',
      '#value' => '',
    ];

    // Select

    $yearService = \Drupal::getContainer()->get('date_year_filter.year');

    $form['exposed_year'] = $this->getSelectForYear($this->options['exposed_year'] ?? null);

    // Auxiliary fields.
    // $form['exposed_from_date'] = [
    //   '#type' => 'date',
    //   '#title' => $this->t('Since'),
    //   '#default_value' => isset($this->options['exposed_from_date']) ? $this->options['exposed_from_date'] : NULL,
    // ];

    // $form['exposed_to_date'] = [
    //   '#type' => 'date',
    //   '#title' => $this->t('Until'),
    //   '#default_value' => isset($this->options['exposed_to_date']) ? $this->options['exposed_to_date'] : NULL,
    // ];
  }

  /**
   * {@inheritdoc}
   */
  public function acceptExposedInput($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    $input[$this->options['expose']['identifier']] = $input['exposed_year']; // $input['exposed_from_date'] . '_' . $input['exposed_to_date'];

    $rc = parent::acceptExposedInput($input);

    return $rc;
  }

  /**
   * This method returns the ID of the fake field which contains this plugin.
   *
   * It is important to put this ID to the exposed field of this plugin for the
   * following reasons: a) To avoid problems with
   * FilterPluginBase::acceptExposedInput function b) To allow this filter to
   * be printed on twig templates with {{ form.date_range_picker_filter }}
   *
   * @return string
   *   ID of the field which contains this plugin.
   */
  private function getFilterId() {
    return $this->options['expose']['identifier'];
  }


  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    $field = $this->realField;

    if (!$this->options['exposed']) {
      // Administrative value.
      $this->queryFilter($field, $this->options['admin_year']);
    } else {
      // Exposed value.
      if (empty($this->value) || empty($this->value[0])) {
        return;
      }

      $this->queryFilter($field, $this->value[0]);
    }
  }


  /**
   * Filters by given year and month.
   *
   * @param $fieldName
   *   Machine name of the field.
   * @param $dates
   *   Date from and date to.
   */
  private function queryFilter($fieldName, $year) {
    $yearService = \Drupal::getContainer()->get('date_year_filter.year');

    $conditions = [];

    if ($this->hasEndDate()) {
      $start_field_name = "$this->tableAlias" . '.' . $fieldName . "__value";
      $end_field_name = "$this->tableAlias" . '.' . $fieldName . "__end_value";

      $timestamp_start = $this->query->getDateField($start_field_name, TRUE);
      $timestamp_end = $this->query->getDateField($end_field_name, TRUE);
    } else {
      $start_field_name = "$this->tableAlias" . '.' . $fieldName . "";
      $timestamp_start = $this->query->getDateField($start_field_name, FALSE);
    }

    $date_start = $this->query->getDateFormat($timestamp_start, 'Y-m-d H:i:s', FALSE);
    if ($this->hasEndDate()) {
      $date_end = $this->query->getDateFormat($timestamp_end, 'Y-m-d H:i:s', FALSE);
    }
    $date_now = $this->query->getDateFormat('FROM_UNIXTIME(***CURRENT_TIME***)', 'Y-m-d H:i:s', FALSE);

    switch ($year) {
      case 'all':
        break;

      case 'past':
        $conditions[] = $this->hasEndDate() ? "($date_now > $date_end)" : "($date_now > $date_start)";
        break;

      case 'future':
        $conditions[] = "($date_now < $date_start)";
        break;

      case 'current':
        if ($this->hasEndDate()) {
          $conditions[] = "$date_now BETWEEN $date_start AND $date_end";
          $conditions[] = "$date_end IS NULL AND $date_start < $date_now";
        }
        break;

      default:
        // Numeric year
        $year = $yearService->getYearFromString($year, $this->options['start_of_year']);
        $year_start = "CAST('" . $year->start->format('Y-m-d H:i:s') . "' AS DATE)";
        $year_end = "CAST('" . $year->end->format('Y-m-d H:i:s') . "' AS DATE)";

        if ($this->hasEndDate()) {
          $conditions[] = "$date_start BETWEEN $year_start AND $year_end";
          $conditions[] = "$date_end IS NOT NULL AND $date_end BETWEEN $year_start AND $year_end";
          $conditions[] = "$date_end IS NULL AND $date_start < $year_start";
        } else {
          $conditions[] = "$date_start BETWEEN $year_start AND $year_end";
        }
        break;
    }

    $conditions = array_filter($conditions);
    if (count($conditions) > 0) {
      $this->query->addWhereExpression($this->options['group'], implode(" OR ", $conditions));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    if (!$this->options['exposed']) {
      $form['admin_year'] = $this->getSelectForYear($this->options['admin_year']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['start_of_year'] = ['default' => ''];
    $options['years_range_start'] = ['default' => ''];
    $options['years_range_end'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['start_of_year'] = [
      '#type' => 'date',
      '#title' => $this->t('Start of year'),
      '#description' => $this->t('Defaults to Jan 1st. The actual year you select here does not matter, just the day and month. You could vary it to filter by e.g. academic or tax years.'),
      '#default_value' => isset($this->options['start_of_year']) ? $this->options['start_of_year'] : Date('Y') . '-01-01',
    ];

    // @todo years_range_start and years_range_end

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $attributes = [];
    // Exposed filter.
    if ($this->options['exposed']) {
      $attributes[] = 'exposed';
    }

    // Administrative filter.
    $start = new DateTime($this->options['start_of_year']);
    $variables = [
      '@start_of_year' => 'day ' . $start->format('d') . ', month ' . $start->format('n'),
    ];
    $attributes[] = 'year starts @start_of_year';

    return $this->t(implode(', ', $attributes), $variables);
  }

  private function hasEndDate() {
    return $this->definition['field_type'] == 'daterange';
  }

  private function getSelectForYear($currentValue) {
    $yearService = \Drupal::getContainer()->get('date_year_filter.year');

    $options = [
      'all' => $this->t('All'),
    ];

    if ($this->hasEndDate()) {
      $options['current'] = $this->t('Current (regardless of year)');
    }

    $current = new DateTime('now');

    $yearsBehind = 3; // @todo make configurable
    $yearsAhead = 3;

    $date = new DateTime('-' . $yearsBehind . ' years');

    $i = 0;
    while ($i <= ($yearsBehind + $yearsAhead)) {
      $options[$date->format('Y')] = $yearService->formatDateAsYear($date, $this->options['start_of_year']);
      $date->add(new DateInterval('P1Y'));
      $i++;
    }

    $options['past'] = $this->t('Past');
    $options['future'] = $this->t('Future');

    return [
      '#type' => 'select',
      '#title' => $this->t('Year'),
      '#options' => $options,
      '#default_value' => $currentValue ?? $yearService->getYearForDate($current, $this->options['start_of_year'])->start->format('Y'),

      '#other' => t('Other (enter the year, or the year in which a non-standard year starts)'),
      '#other_unknown_defaults' => 'append',
      '#select_type' => 'select'
    ];
  }

  public function isTimestamp($string) {
    return (1 === preg_match('~^[1-9][0-9]*$~', $string));
  }
}
