<?php

namespace Drupal\date_year_filter\Service;

use DateTime;
use DateInterval;
use DateTimeImmutable;

/**
 * Class YearService.
 */
class YearService {

  private $date;
  private $year;

  /**
   * Constructs a new YearService object.
   */
  public function __construct() {
  }

  /**
   * Produce string non-standard year from date
   *
   * @param   DateTime  $date
   *
   * @return  string              e.g. '2021-22'
   */
  public function formatDateAsYear(DateTime $date, $startOfYear) {
    if (gettype($startOfYear) == 'string') {
      $startOfYear = new DateTime($startOfYear);
    }

    // PRevent this function from modifying the existing dates (WEIRD DateTime behaviour for add and sub)
    $date = new DateTimeImmutable($date->format('Y-m-d'));

    if ($startOfYear->format('n') == 1 && $startOfYear->format('d') == 1) {
      return $date->format('Y');
    } else {
      $nextYear = new DateTimeImmutable($date->add(new DateInterval('P1Y'))->format('Y-m-d'));
      return $date->format('Y') . '-' . $nextYear->format('y'); // @todo optional formatting settings
    }
  }

  /**
   * Convert string non-standard year to Year object with dates
   *
   * @param   string  $textYear  e.g. '2021-22'
   * @param   string|DateTime $startOfYear
   *
   * @return  object             Year object
   */
  public function getYearFromString(string $textYear, $startOfYear) {
    if (gettype($startOfYear) == 'string') {
      $startOfYear = new DateTime($startOfYear);
    }
    $date = new DateTime(substr($textYear, 0, 4) . '-' . $startOfYear->format('m') . '-' . $startOfYear->format('d')) ?: new DateTime(substr($textYear, 0, 2) . '-' . $startOfYear->format('m') . '-' . $startOfYear->format('d'));
    return $this->getYearForDate($date, $startOfYear);
  }

  /**
   * Returns array start and end DateTime objects, and formatted name
   *
   * @param   String or DateTime  $date  date to process
   *
   * @return  Object          [return description]
   */
  public function getYearForDate($date, $startOfYear) {
    if (gettype($date) == 'string') {
      $date = new DateTime($date);
    }

    if (!$date) {
      \Drupal::logger('date_year_filter')->error('getYearForDate - valid date not provided.');
      return FALSE;
    }

    if (gettype($startOfYear) == 'string') {
      $startOfYear = new DateTime($startOfYear);
    }

    if (!$startOfYear) {
      \Drupal::logger('date_year_filter')->error('getYearForDate - valid start of year not provided: @date.', [
        '@date' => $startOfYear,
      ]);
      return FALSE;
    }

    // Ensure start of year is the one in this year
    $startOfYear = new \DateTime($date->format('Y') . '-' . $startOfYear->format('m') . "-" . $startOfYear->format('d') . ' 12:00:00');

    // e.g. 2024-03-01 -> 2024-02-29
    $lastDayOfPreviousYear = clone $startOfYear;
    $lastDayOfPreviousYear->sub(new DateInterval('P1D'));
    // $lastDayOfPreviousYear->add(new DateInterval('P364D'));

    $year = new \stdClass();

    // If this day is earlier than the start of the year:
    // Year starts in previous year, ends in current year.
    if ($date->format('z') < $startOfYear->format('z')) {
      $year->start = new DateTime(($date->format('Y') - 1) . '-' . $startOfYear->format('m') . '-' . $startOfYear->format('d'));
      $year->end = new DateTime($date->format('Y') . '-' . $lastDayOfPreviousYear->format('m') . '-' . $lastDayOfPreviousYear->format('d'));
    } else {
      // Year starts in current year, ends in new year:
      $year->start = new DateTime(($date->format('Y')) . '-' . $startOfYear->format('m') . '-' . $startOfYear->format('d'));
      $year->end = new DateTime(($date->format('Y') + 1) . '-' . ($lastDayOfPreviousYear->format('m')) . '-' . $lastDayOfPreviousYear->format('d'));
    }

    $year->end->setTime(23, 59, 59);

    $year->name = $this->formatDateAsYear($year->start, $startOfYear);
    // dd($year);

    return $year;
  }

  /**
   * Test if date is in the year described by a formatted string
   *
   * @param   String or DateTime   $date          date to check
   * @param   String or Year object        $year  e.g. '2021', '2021-22'
   * @param   Integer        $graceDaysBefore  expand the year for the purposes of this check
   * @param   Integer        $graceDaysAfter  expand the year for the purposes of this check
   *
   * @return  boolean
   */
  public function checkDateInYear($date, $year, $startOfYear, $graceDaysBefore = 0, $graceDaysAfter = 0) {
    if ($year == 'all') {
      return true;
    }
    if ($year == 'current') {
      $now = new DateTime();
      $year = $now->format('Y');
    }

    if (gettype($date) == 'string') {
      $date = new DateTime($date);
    }

    if (gettype($year) == 'string') {
      $year = $this->getYearFromString($year, $startOfYear);
    }

    // Factor in grace days
    $year->start = $year->start->sub(new DateInterval('P' . $graceDaysBefore . 'D'));
    $year->end = $year->end->add(new DateInterval('P' . $graceDaysAfter . 'D'));

    return $date >= $year->start && $date <= $year->end;
  }

  /**
   * Return and year array relative to now
   * adjusted by the number of years given
   *
   * @param   Integer  $years  e.g. -1 last year, 0 this year, 1 next year
   *
   * @return  object   Year object
   */
  public function getYearFromNow(int $years, $startOfYear) {
    $date = new DateTime();

    $interval = new DateInterval('P' . $years . 'Y');

    if ($years > 0) {
      $date = $date->add($interval);
    } else if ($years < 0) {
      $date = $date->sub($interval);
    }

    return $this->getYearForDate($date, $startOfYear);
  }

  /**
   * Return formatted year array relative to now
   * adjusted by the number of years given
   *
   * @param   Integer  $years  e.g. -1 last year, 0 this year, 1 next year
   *
   * @return  array   Year array
   */
  public function getFormattedYearFromNow(int $years, $startOfYear) {
    $year = $this->getYearFromNow($years, $startOfYear);
    return $this->formatDateAsYear($year->start, $startOfYear);
  }
}
