Allows for filtering by year on a view, optionally with a non-standard start date. Also provides a service with many year-related functions.

Functionality:
- Select dropdown on views for any date or date range field to filter by year
- A non-standard start date for your years allows you to e.g. filter by tax year, academic year, etc.
