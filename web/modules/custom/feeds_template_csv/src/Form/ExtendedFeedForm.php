<?php

namespace Drupal\feeds_template_csv\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\group\Entity\Form\GroupRoleForm;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Url;
use Drupal\feeds\FeedForm;

class ExtendedFeedForm extends FeedForm
{
    /**
     * {@inheritdoc}
     */
    public function form(array $form, FormStateInterface $form_state) {
        $feed = $this->entity;    
        $feed_type = $feed->getType();
        $form = parent::form($form, $form_state);
        
        foreach ($feed_type->getPlugins() as $type => $plugin) {
            if($type == 'fetcher') {
                $url = Url::fromRoute('feeds_template_csv.download', ['feeds_feed_type' => $feed_type->id()]);
                $form['template'] = array(
                    '#type' => 'link',
                    '#attributes' => ['class' => ['button']],
                    '#url' => $url,
                    '#title' => $this->t('Download template'),
                    '#weight' => -50,
                  );
            }
        }

        return $form;
    }

}
