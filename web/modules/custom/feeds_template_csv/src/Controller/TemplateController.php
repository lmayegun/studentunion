<?php

namespace Drupal\feeds_template_csv\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\feeds\Entity\FeedType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TemplateController.
 *
 *  Returns responses for Committee record routes.
 */
class TemplateController extends ControllerBase implements ContainerInjectionInterface {

  public function download($feeds_feed_type) {
    return $this->generateTemplate($feeds_feed_type);
  }

  public function generateTemplate($feeds_feed_type) {
    $title = 'Template for ' . $feeds_feed_type;

    $fields = $this->getMapping($feeds_feed_type);

    $content = implode(",", array_values(array_column($fields, "column_name"))) . PHP_EOL;
    // $content .= implode(",", array_values(array_column($fields, "description"))).PHP_EOL;
    $content .= implode(",", array_fill(0, count($fields), "")) . PHP_EOL;
    return static::generateCSV($content, $title);
  }

  public function getMapping($feeds_feed_type) {
    $fields = [];

    $feed_type = FeedType::load($feeds_feed_type);

    $entity_type = $feed_type->getProcessor()->entityType();
    $bundle = $feed_type->getProcessor()->bundleKey();

    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);

    $sources = $feed_type->getMappingSources();
    foreach ($sources as $key => $info) {
      $fields[$info['machine_name']] = [
        'column_name' => $info['label'],
      ];
    }

    $mappings = $feed_type->getMappings();
    foreach ($mappings as $delta => $mapping) {
      foreach ($mapping['map'] as $target_subvalue => $source_machine_name) {
        $fields[$source_machine_name]['target'] = $mapping['target'];
        $fields[$source_machine_name]['target_subvalue'] = $target_subvalue;
        $fields[$source_machine_name]['field_type'] = $field_definitions[$mapping['target']]->getType();
      }
    }

    return $fields;
  }

  public static function generateCSV($content, $filename) {
    $response = new Response($content);
    $response->headers->set('Content-Type', 'text/csv; utf-8');
    $response->headers->set('Content-Disposition', 'attachment; filename = ' . $filename . '.csv');
    return $response;
  }
}
