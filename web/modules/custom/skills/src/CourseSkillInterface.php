<?php

namespace Drupal\skills;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a course - skill entity type.
 */
interface CourseSkillInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the course - skill creation timestamp.
   *
   * @return int
   *   Creation timestamp of the course - skill.
   */
  public function getCreatedTime();

  /**
   * Sets the course - skill creation timestamp.
   *
   * @param int $timestamp
   *   The course - skill creation timestamp.
   *
   * @return \Drupal\skills\CourseSkillInterface
   *   The called course - skill entity.
   */
  public function setCreatedTime($timestamp);
}
