<?php

namespace Drupal\skills;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the course - skill entity type.
 */
class CourseSkillAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view skill record');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit skill record', 'administer skill record'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete skill record', 'administer skill record'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create skill record', 'administer skill record'], 'OR');
  }
}
