<?php

namespace Drupal\skills;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the skill entity type.
 */
class SkillAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view skill');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit skill', 'administer skill'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete skill', 'administer skill'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create skill', 'administer skill'], 'OR');
  }

}
