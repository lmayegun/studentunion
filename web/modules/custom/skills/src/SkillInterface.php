<?php

namespace Drupal\skills;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a skill entity type.
 */
interface SkillInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the skill title.
   *
   * @return string
   *   Title of the skill.
   */
  public function getTitle();

  /**
   * Sets the skill title.
   *
   * @param string $title
   *   The skill title.
   *
   * @return \Drupal\skills\SkillInterface
   *   The called skill entity.
   */
  public function setTitle($title);

  /**
   * Gets the skill creation timestamp.
   *
   * @return int
   *   Creation timestamp of the skill.
   */
  public function getCreatedTime();

  /**
   * Sets the skill creation timestamp.
   *
   * @param int $timestamp
   *   The skill creation timestamp.
   *
   * @return \Drupal\skills\SkillInterface
   *   The called skill entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the skill status.
   *
   * @return bool
   *   TRUE if the skill is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the skill status.
   *
   * @param bool $status
   *   TRUE to enable this skill, FALSE to disable.
   *
   * @return \Drupal\skills\SkillInterface
   *   The called skill entity.
   */
  public function setStatus($status);

}
