<?php

namespace Drupal\skills;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for a course - skill entity type.
 */
class CourseSkillViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    // The course - skill has no entity template itself.
    unset($build['#theme']);
    return $build;
  }
}
