<?php

namespace Drupal\skills\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the course - skill entity edit forms.
 */
class SkillRecordForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New course - skill %label has been created.', $message_arguments));
      $this->logger('skills')->notice('Created new course - skill %label', $logger_arguments);
    } else {
      $this->messenger()->addStatus($this->t('The course - skill %label has been updated.', $message_arguments));
      $this->logger('skills')->notice('Updated new course - skill %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.training_skill.canonical', ['training_skill' => $entity->id()]);
  }
}
