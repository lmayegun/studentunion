<?php

namespace Drupal\skills\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\skills\SkillRecordInterface;
use Drupal\user\UserInterface;

/**
 * Defines the course - skill entity class.
 *
 * @ContentEntityType(
 *   id = "skill_record",
 *   label = @Translation("course - Skill"),
 *   label_collection = @Translation("course - Skills"),
 *   handlers = {
 *     "view_builder" = "Drupal\skills\SkillRecordViewBuilder",
 *     "list_builder" = "Drupal\skills\SkillRecordListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\skills\SkillRecordAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\skills\Form\SkillRecordForm",
 *       "edit" = "Drupal\skills\Form\SkillRecordForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "skill_record",
 *   admin_permission = "access skill record overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/course/skills/add",
 *     "canonical" = "/course/skills/{skill_record}",
 *     "edit-form" = "/course/skills/{skill_record}/edit",
 *     "delete-form" = "/course/skills/{skill_record}/delete",
 *     "collection" = "/course/skills"
 *   },
 * )
 */
class SkillRecord extends ContentEntityBase implements SkillRecordInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new course - skill entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('Counts towards user skills.'))
      ->setDefaultValue(FALSE);

    $fields['earnt_date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Date earnt'))
      ->setDescription(t('The time that the user earnt this skill.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user who earnt the skill.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the course - skill was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the course - skill was last edited.'));

    $fields['entity_skill_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('ID of entity that provides skill'))
      ->setDescription(t('e.g. course, record, etc.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    return $fields;
  }
}
