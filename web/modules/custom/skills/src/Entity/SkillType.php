<?php

namespace Drupal\skills\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Skill type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "skill_type",
 *   label = @Translation("Skill type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\skills\Form\SkillTypeForm",
 *       "edit" = "Drupal\skills\Form\SkillTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\skills\SkillTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer skill types",
 *   bundle_of = "skill",
 *   config_prefix = "skill_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/skill_types/add",
 *     "edit-form" = "/admin/structure/skill_types/manage/{skill_type}",
 *     "delete-form" = "/admin/structure/skill_types/manage/{skill_type}/delete",
 *     "collection" = "/admin/structure/skill_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class SkillType extends ConfigEntityBundleBase {

  /**
   * The machine name of this skill type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the skill type.
   *
   * @var string
   */
  protected $label;

}
