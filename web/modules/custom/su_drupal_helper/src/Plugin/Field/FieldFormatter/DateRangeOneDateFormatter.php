<?php

namespace Drupal\su_drupal_helper\Plugin\Field\FieldFormatter;

use DateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;
use Drupal\smart_date\Entity\SmartDateFormat;
use Drupal\smart_date\SmartDateTrait;
use Drupal\smart_date\Plugin\Field\FieldFormatter\SmartDateDefaultFormatter;

/**
 * Plugin implementation of the 'Default' formatter for 'smartdate' fields.
 *
 * This formatter renders the time range using <time> elements, with
 * configurable date formats (from the list of configured formats) and a
 * separator.
 *
 * @FieldFormatter(
 *   id = "date_range_split",
 *   label = @Translation("Date range - one date"),
 *   field_types = {
 *     "smartdate",
 *     "daterange",
 *   }
 * )
 */
class DateRangeOneDateFormatter extends SmartDateDefaultFormatter
{

  use SmartDateTrait {
    viewElements as protected traitViewElements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'start_or_end' => 'start',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form = ['start_or_end' => [
        '#type' => 'select',
        '#title' => $this->t('Start or end'),
        '#default_value' => $this->getSetting('start_or_end'),
        '#options' => ['start' => 'Start', 'end' => 'End'],
      ]
    ] + $form;

    return $form;
  }
 
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $field_type = $this->fieldDefinition->getType();

    // We need to provide end dates (even though they're optional)
    // Because the core SmartDate module doesn't consider them optional
    // We remove the date later by storing the delta here
    $emptyEnds = [];
    foreach ($items as $delta => $item) {
      if(empty($item->end_value)) {
        $item->end_value = $item->value;
        $emptyEnds[$delta] = $delta;
      }
    }

    $original_elements = $this->traitViewElements($items, $langcode);
    foreach($original_elements AS $delta => $item) {
      unset($original_elements[$delta]['separator']);
      unset($original_elements[$delta][$this->getSetting('start_or_end') == 'end' ? 'start' : 'end']);
      // Here we blank out the end date if it was empty to begin with
      if($this->getSetting('start_or_end') == 'end' && in_array($delta, $emptyEnds, true)) {
        $original_elements[$delta]['end'] = ['#markup' => ''];
      }
    }

    return $original_elements;
  }
}
