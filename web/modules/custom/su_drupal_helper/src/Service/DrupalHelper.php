<?php

namespace Drupal\su_drupal_helper\Service;

use Drupal;
use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\Core\Language\LanguageInterface;
use Drupal\group\Entity\Group;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Exception;
use PDO;

class DrupalHelper {

  /**
   * Returns the entity of the current route.
   *
   * @return Drupal\Core\Entity\EntityInterface
   *   The entity or NULL if this is not an entity route.
   */
  public static function getRouteEntity() {
    $route_match = Drupal::routeMatch();
    $route_name = $route_match->getRouteName();

    // Views
    if ($route_match->getParameter('view_id') && $route_match->getParameter('group')) {
      $group = $route_match->getParameter('group');
      if ($group instanceof Group) {
        return $route_match->getParameter('group');
      }
      elseif (gettype($group) == 'string') {
        return Group::load($route_match->getParameter('group'));
      }
    }

    if ($route_match->getParameter('view_id') && $route_match->getParameter('node')) {
      $node = $route_match->getParameter('node');
      if ($node instanceof Node) {
        return $route_match->getParameter('node');
      }
      elseif (gettype($node) == 'string') {
        return Node::load($route_match->getParameter('node'));
      }
    }

    if ($route_match->getParameter('view_id') && $route_match->getParameter('taxonomy_term')) {
      $term = $route_match->getParameter('taxonomy_term');
      if ($term instanceof Term) {
        return $route_match->getParameter('taxonomy_term');
      }
      elseif (gettype($term) == 'string') {
        // Try to load by ID:
        $loadedTerm = Term::load($route_match->getParameter('taxonomy_term'));
        if ($loadedTerm) {
          return $loadedTerm;
        }

        // Try to load by path alias (just for tags):
        if (Drupal::moduleHandler()->moduleExists('pathauto')) {
          $loadedTerm = Term::load(static::getTermByUrlPath($term, ['tags']));
          if ($loadedTerm) {
            return $loadedTerm;
          }
        }
      }
    }

    // Look for a canonical entity view page, e.g. node/{nid}, user/{uid}, etc.
    $matches = [];
    preg_match('/entity\.(.*)\.(latest[_-]version|canonical)/', $route_name, $matches);
    if (!empty($matches[1])) {
      $entity_type = $matches[1];
      return $route_match->getParameter($entity_type);
    }

    // Look for a rest entity view page, e.g. "node/{nid}?_format=json", etc.
    $matches = [];
    // Matches e.g. "rest.entity.node.GET.json".
    preg_match('/rest\.entity\.(.*)\.(.*)\.(.*)/', $route_name, $matches);
    if (!empty($matches[1])) {
      $entity_type = $matches[1];
      return $route_match->getParameter($entity_type);
    }

    // Look for entity object 'add' pages, e.g. "node/add/{bundle}".
    $route_name_matches = [];
    preg_match('/(entity\.)?(.*)\.add(_form)?/', $route_name, $route_name_matches);
    if (!empty($route_name_matches[2])) {
      $entity_type = $route_name_matches[2];
      $definition = Drupal::entityTypeManager()
        ->getDefinition($entity_type, FALSE);
      if (!empty($definition)) {
        $type = $route_match->getRawParameter($definition->get('bundle_entity_type'));
        if (!empty($type)) {
          return Drupal::entityTypeManager()
            ->getStorage($entity_type)
            ->create([
              $definition->get('entity_keys')['bundle'] => $type,
            ]);
        }
      }
    }

    // Look for entity object 'edit' pages, e.g. "node/{entity_id}/edit".
    $route_name_matches = [];
    preg_match('/entity\.(.*)\.edit_form/', $route_name, $route_name_matches);
    if (!empty($route_name_matches[1])) {
      $entity_type = $route_name_matches[1];
      $entity_id = $route_match->getRawParameter($entity_type);

      if (!empty($entity_id)) {
        return Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->load($entity_id);
      }
    }

    // Look for entity object 'add content translation' pages, e.g.
    // "node/{nid}/translations/add/{source_lang}/{translation_lang}".
    $route_name_matches = [];
    preg_match('/(entity\.)?(.*)\.content_translation_add/', $route_name, $route_name_matches);
    if (!empty($route_name_matches[2])) {
      $entity_type = $route_name_matches[2];
      $definition = Drupal::entityTypeManager()
        ->getDefinition($entity_type, FALSE);
      if (!empty($definition)) {
        $node = $route_match->getParameter($entity_type);
        $type = $node->bundle();
        if (!empty($type)) {
          return Drupal::entityTypeManager()
            ->getStorage($entity_type)
            ->create([
              $definition->get('entity_keys')['bundle'] => $type,
            ]);
        }
      }
    }

    // Special handling for the admin user_create page. In this case, there's only
    // one bundle and it's named the same as the entity type, so some shortcuts
    // can be used.
    if ($route_name == 'user.admin_create') {
      $entity_type = $type = 'user';
      $definition = Drupal::entityTypeManager()->getDefinition($entity_type);
      if (!empty($type)) {
        return Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->create([
            $definition->get('entity_keys')['bundle'] => $type,
          ]);
      }
    }

    // Entity will be found in the route parameters.


    return NULL;
  }

  public static function getMachineName($string) {
    $transliterated = Drupal::transliteration()
      ->transliterate($string, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = mb_strtolower($transliterated);

    $transliterated = preg_replace('@[^a-z0-9_.]+@', '_', $transliterated);

    return substr($transliterated, 0, 22);
  }

  public static function getTermByUrlPath(string $term_name, array $vocabularies) {

    $query = Drupal::database()->select('taxonomy_term_field_data', 't')
      ->fields('t', ['tid', 'name']);

    // Filter by vocabulary ID if one or more are provided.
    if (!empty($vocabularies)) {
      $query->condition('t.vid', $vocabularies, 'IN');
    }

    $results = $query->execute()->fetchAll(PDO::FETCH_OBJ);

    // Iterate results.
    $pathautoAliasCleaner = Drupal::service('pathauto.alias_cleaner');
    foreach ($results as $row) {
      if ($pathautoAliasCleaner->cleanString($row->name) == $pathautoAliasCleaner->cleanString($term_name)) {
        return $row->tid;
      }
    }
    return NULL;
  }

  public function convertAddressToGeofield(AddressItem $address) {

    if (!$address->getPostalCode() || $address->getPostalCode() == '' || $address->getPostalCode() == 'N/A') {
      return NULL;
    }
    $string_address = $address->getPostalCode() . ', ' . $address->getCountryCode();

    $googlemaps = Drupal::entityTypeManager()
      ->getStorage('geocoder_provider')
      ->load('googlemaps');
    $geocoder = Drupal::service('geocoder');
    try {
      $geocodeAddressCollection = $geocoder->geocode($string_address, [$googlemaps]);
    } catch (Exception $e) {
    }
    if (!$geocodeAddressCollection) {
      return NULL;
    }
    foreach ($geocodeAddressCollection->all() as $location) {
      $coords = $location->getCoordinates();
      $lon = $coords->getLongitude();
      $lat = $coords->getLatitude();
      return Drupal::service('geofield.wkt_generator')->wktBuildPoint(
        [
          $lon,
          $lat,
        ]
      );
    }
  }

  public function convertAddressArrayToGeofield(array $address) {
    if (!isset($address['postal_code']) || $address['postal_code'] == '' || $address['postal_code'] == 'N/A') {
      return NULL;
    }
    $string_address = $address['postal_code'] . ', ' . $address['country'];

    $googlemaps = Drupal::entityTypeManager()
      ->getStorage('geocoder_provider')
      ->load('googlemaps');
    $geocoder = Drupal::service('geocoder');
    try {
      $geocodeAddressCollection = $geocoder->geocode($string_address, [$googlemaps]);
    } catch (Exception $e) {
    }
    if (!$geocodeAddressCollection) {
      return NULL;
    }
    foreach ($geocodeAddressCollection->all() as $location) {
      $coords = $location->getCoordinates();
      $lon = $coords->getLongitude();
      $lat = $coords->getLatitude();
      return Drupal::service('geofield.wkt_generator')->wktBuildPoint([
        $lon,
        $lat,
      ]);
    }
  }

}
