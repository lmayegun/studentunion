<?php

namespace Drupal\webform_group_extended\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;
use Drupal\group\Entity\GroupRole;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a webform group roles element.
 *
 * @FormElement("webform_group_permissions")
 */
class WebformGroupPermissions extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    $info = parent::getInfo();
    $info['#element_validate'] = [
      [$class, 'validateWebformGroupPermissions'],
    ];
    $info['#multiple'] = TRUE;
    return $info;
  }

  /**
   * Processes a webform roles (checkboxes) element.
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#options'] = static::getGroupPermissionsOptions($element);
    $element['#select2'] = TRUE;

    // Must convert this element['#type'] to a 'select' to prevent
    // "Illegal choice %choice in %name element" validation error.
    // @see \Drupal\Core\Form\FormValidator::performRequiredValidation
    $element['#type'] = 'select';

    WebformElementHelper::process($element);
    return parent::processSelect($element, $form_state, $complete_form);
  }

  /**
   * Webform element validation handler for webform roles (checkboxes) element.
   */
  public static function validateWebformGroupPermissions(&$element, FormStateInterface $form_state, &$complete_form) {
    if (!empty($element['#multiple'])) {
      $value = array_values($form_state->getValue($element['#parents'], []));
      $element['#value'] = $value;
      $form_state->setValueForElement($element, $value);
    }
  }

  /**
   * Get group roles options for an element.
   *
   * @param array $element
   *   An element.
   *
   * @return array
   *   Group roles options for an element.
   */
  public static function getGroupPermissionsOptions(array $element) {
    $element += [];

    $group_permissions = \Drupal::service('group.permissions')->getPermissions(FALSE);
    $group_permissions_names = [];

    $options = [];
    foreach ($group_permissions as $group_permission_id => $group_permission) {
      $group_permission_label = $group_permission['title'];

      $section_id = $group_permission['section'];
      $section_label = $group_permission['section'];

      $t_args = [
        '@section' => $section_label,
        '@group_permission' => $group_permission_label,
      ];

      $options[$section_label][$group_permission_id] = t('@section: @group_permission', $t_args);

      $group_permission_name = preg_replace("/^$section_id-/", "", $group_permission_id);
      $group_permission_names[$group_permission_name] = $group_permission_label;
    }
    ksort($options);

    return [(string) t('Group permissions') => $group_permissions_names] + $options;
  }
}
