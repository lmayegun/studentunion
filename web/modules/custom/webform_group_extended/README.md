A drop-in replacement for the webform_group module that is included in the Webform module, to improve the ability to
restrict access to webform forms, submissions and elements based on group role and/or group permission, and work within
other group contexts.

This provides additional functionality:

You can load the group that determines access from context or query string, rather than needing the form to be a node
that is group content. This means you can e.g. prepopulate the form with a group ID, and thereby restrict access to
users who have certain roles or permissions within the group that has that ID.

You can make the user select the group from one of the groups they are a member of, before proceeding to the form, so
you can then restrict access to the group, or otherwise use the group to populate an element, control form behaviour and
store the reference.

You can restrict access by group permissions. This allows finer-grained access control, and you could e.g. define custom
group permissions just to allow access to webform functionality.

If you want a webform_access_group element but don't want to require group membership for the form,
enter custom property require_group_membership: false in the Advanced tab of the element.

---

Current issues:

- Tests are not working
- Need to make the "getCurrentGroup" approach more generic and/or configurable per form or element
