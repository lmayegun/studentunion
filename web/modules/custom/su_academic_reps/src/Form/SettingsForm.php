<?php

namespace Drupal\su_academic_reps\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_academic_reps.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('su_academic_reps.settings');
    $form['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End of academic year role'),
      '#default_value' => $config->get('end_date'),
      '#description' => $this->t('The year part of this date is ignored; this does not need to be updated each year.')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('su_academic_reps.settings')
      ->set('end_date', $form_state->getValue('end_date'))
      ->save();
  }
}
