<?php

namespace Drupal\su_academic_reps\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\multivalue_form_element\Element\MultiValue;

/**
 * Allow creating multiple reps (group membership records type academic_rep).
 */
class AcademicRepForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'su_academic_reps_add_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['info'] = [
      '#markup' => 'This form allows UCL staff to register <a href="academic-reps">Academic Representatives</a>. Press <b>Add another rep</b> to add multiples. When ready, press <b>Submit</b>. Please take note of any messages as there may be errors or actions required.<br><hr><br><a class="union-button" target="_blank" href="/academic-representatives">View current academic representatives</a>',
      '#suffix' => '<hr>',
    ];

    $options = [];
    $ids = \Drupal::entityQuery('group')->condition('type', 'academic_group')->condition('status', 1)->execute();
    foreach ($ids as $id) {
      $group = Group::load($id);
      $options[$id] = $group->label();
    }

    $type_options = [];
    /** @var \Drupal\group\Entity\GroupRoleInterface[] $group_roles */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $group_roles = $gmrRepositoryService->getRolesForInstanceType('academic_representative');
    foreach ($group_roles as $role) {
      $type_options[$role->id()] = $role->label();
    }

    $form['reps'] = [
      '#type' => 'multivalue',
      '#title' => $this->t('Student details'),
      '#cardinality' => MultiValue::CARDINALITY_UNLIMITED,
      '#add_more_label' => $this->t('Add another rep'),
      '#nodrag' => TRUE,
      'sscc' => [
        '#type' => 'select',
        '#title' => $this->t('SSCC'),
        '#options' => $options,
        '#required' => TRUE,
      ],
      'type' => [
        '#type' => 'select',
        '#title' => $this->t('Rep type'),
        '#options' => $type_options,
      ],
      // 'name' => [
      //   '#type' => 'textfield',
      //   '#title' => $this->t('Full name'),
      //   '#required' => TRUE,
      // ],
      'mail' => [
        '#type' => 'email',
        '#title' => $this->t('UCL e-mail address'),
        '#required' => TRUE,
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // foreach ($form_state['values']['container'] as $key => $row) {
    //   $email_field = 'container][' . $key . '][email';
    //   if (strpos($row['email'], '@ucl.ac.uk') <= 0) {
    //     form_set_error($email_field, t('E-mail must be a UCL e-mail.'));
    //     drupal_set_message(t($row['email'] . ' is not a UCL e-mail address.'), 'warning');
    //   } else {
    //     /*if(!uclu_student_load_by_email($row['email'])) {
    //     drupal_set_message(t($row['email'] . ' is not opted in as a member of Students\' Union UCL.'), 'warning');
    //   }*/
    //   }
    // }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $reps = $form_state->getValue('reps');

    $failed = [];


    /** @var \Drupal\date_year_filter\Service\YearService $yearService */
    $yearService = \Drupal::service('date_year_filter.year');
    $start = new \DateTime();
    $endDate = \Drupal::config('su_academic_reps.settings')->get('end_date') ?? '2021-08-31';
    $year = $yearService->getYearForDate($start, $endDate);
    $end = $year->end;

    $service = \Drupal::service('su_user.ucl_user');
    $studentService = \Drupal::service('su_students.student_services');

    $added_successfully = '';
    $duplicates = '';
    foreach ($reps as $delta => $rep) {
      if (strpos($rep['mail'], '@ucl.ac.uk', 0) === FALSE) {
        $rep['error'] = 'E-mail address not UCL.';
        $failed[] = $rep;
        continue;
      }

      $account = $service->getUserForUclFieldInput($rep['mail']);
      if (!$account) {
        $rep['error'] = 'Cannot find account with that email address.';
        $failed[] = $rep;
        continue;
      }
      if (!$studentService->isStudent($account)) {
        $rep['error'] = 'Account is not a student.';
        $failed[] = $rep;
        continue;
      }

      /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
      $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
      if ($gmrRepositoryService->currentEnabledInstanceExists($account, Group::load($rep['sscc']), GroupRole::load($rep['type']), NULL, new \DateTime())) {
        $duplicates .= $rep['mail'] . "<br>";
        continue;
      }

      $gmr = GroupMembershipRecord::create([
        'type' => 'academic_representative',
        'group_id' => $rep['sscc'],
        'group_role_id' => $rep['type'],
        'user_id' => $account->id(),
        'date_range' => [
          'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $start->getTimestamp()),
          'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $end->getTimestamp()),
        ]
      ]);
      $gmr->save();
      $added_successfully .= $rep['mail'] . "<br>";
    }

    $not_opted_in = '';
    $not_ucl = '';
    foreach ($failed as $failedRep) {
      if ($failedRep['error'] == 'E-mail address not UCL.') {
        $not_ucl .= $failedRep['mail'] . "<br>";
      } elseif ($failedRep['error'] == 'Account is not a student.' || 'Cannot find account with that email address.') {
        $not_opted_in .= $failedRep['mail'] . "<br>";
      }
    }

    if ($added_successfully != '') {
      \Drupal::messenger()->addMessage(['#markup' => t('The following students were successfully added as academic representatives: <br>' . $added_successfully)]);
    }

    if ($duplicates != '') {
      \Drupal::messenger()->addMessage(
        [
          '#markup' =>
          t('The following students were already listed as academic reps. They have been updated if any details changed. <br>' . $duplicates)
        ],
        'status'
      );
    }

    if ($not_opted_in != '' || $not_ucl != '') {
      \Drupal::messenger()->addMessage(
        [
          '#markup' =>
          t('There have been some errors, detailed below. You will need to resubmit for each.') // The form has been prepopulated with the reps that caused these issues.
        ],
        'warning'
      );
    }

    if ($not_opted_in != '') {
      \Drupal::messenger()->addMessage(
        [
          '#markup' =>
          t('The following students do not appear to be a current student, or opted in as a member of Students\' Union UCL. Please contact them and advise they visit https://studentsunionucl.org/optin to join the Union before they are added as an academic representative.<br> ' . $not_opted_in)
        ],
        'warning'
      );
    }

    if ($not_ucl != '') {
      \Drupal::messenger()->addMessage(
        [
          '#markup' => t('The following e-mail addresses are not UCL addresses.<br>'  . PHP_EOL . $not_ucl)
        ],
        'warning'
      );
    }
  }
}
