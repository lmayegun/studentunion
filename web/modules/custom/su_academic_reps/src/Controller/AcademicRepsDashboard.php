<?php

namespace Drupal\su_academic_reps\Controller;

use DateTime;
use Drupal\Core\Controller\ControllerBase;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;

class AcademicRepsDashboard extends ControllerBase {
  public function generateDashboard() {
    $page = [];

    $repRoles = GroupRole::loadMultiple([
      'academic_group-faculty_represent',
      'academic_group-lead_department_r',
      'academic_group-course_representa',
      'academic_group-research_student_',
    ]);
    $repTypes = [];
    foreach ($repRoles as $role) {
      $repTypes[$role->id()] = $role->label();
    }

    // Loop through reps?
    $totalReps = 0;
    $fieldResults = [];
    $fieldResults['sscc'] = [];

    $taxonomyFields = ['field_student_study_mode', 'field_student_year_of_study', 'field_student_faculty', 'field_student_department'];

    // Set up field results
    foreach ($repTypes as $repLabel) {
      foreach ($taxonomyFields as $field) {
        $fieldResults[$field][$repLabel] = [];
      }
      $fieldResults['sscc'][$repLabel] = [];
    }

    // Populate SSCCs
    $ids = \Drupal::entityQuery('group')->condition('type', 'academic_group')->condition('status', 1)->execute();
    foreach ($ids as $id) {
      $group = Group::load($id);
      foreach ($repTypes as $repLabel) {
        $fieldResults['sscc'][$repLabel][$group->label()] = 0;
      }
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    foreach ($repRoles as $role) {
      $repsRecords = $gmrRepositoryService->get(NULL, NULL, $role, NULL, new DateTime(), TRUE);
      foreach ($repsRecords as $record) {
        $totalReps++;

        $studentService = \Drupal::service('su_students.student_services');
        $profile = $studentService::getStudentProfile($record->getOwner());

        foreach ($taxonomyFields as $taxonomyField) {
          if ($taxonomyField == 'field_student_year_of_study') {
            $value = $profile->get($taxonomyField)->value ?: 'Unknown';
          } else {
            $value = $profile->get($taxonomyField)->entity ? $profile->get($taxonomyField)->entity->getName() : 'Unknown';
          }

          if (!isset($fieldResults[$taxonomyField])) {
            $fieldResults[$taxonomyField] = [];
          }
          if (!isset($fieldResults[$taxonomyField][$role->label()])) {
            $fieldResults[$taxonomyField][$role->label()] = [];
          }
          if (!isset($fieldResults[$taxonomyField][$role->label()][$value])) {
            $fieldResults[$taxonomyField][$role->label()][$value] = 0;
          }
          $fieldResults[$taxonomyField][$role->label()][$value]++;
        }

        // SSCCs
        $groupLabel = $record->getGroup()->label();
        if (!isset($fieldResults['sscc'][$role->label()][$groupLabel])) {
          $fieldResults['sscc'][$role->label()][$groupLabel] = 0;
        }
        $fieldResults['sscc'][$role->label()][$groupLabel]++;
      }
    }

    // dd($fieldResults['field_student_study_mode'], $ssccs);

    $studentCount = 46000;

    // Generate totals after processing
    $totals = [
      [
        'Stat' => 'Number of academic representatives',
        'Value' => $totalReps,
      ],
      [
        'Stat' => 'Expected students',
        'Value' => $studentCount,
      ],
      [
        'Stat' => 'Ratio of reps to students',
        'Value' => '1 : ' . round($studentCount / $totalReps),
      ],
      'no_reps' => [
        'Stat' => 'SSCCs without reps',
        'Value' => 0,
      ],
    ];

    // Present tables
    $page['totals'] = [
      '#type' => 'table',
      '#caption' => $this->t('Key information'),
      '#header' => ['Stat', 'Value'],
      '#rows' => $totals,
      '#cache' => array_merge(
        $this->getCacheTags(),
        ['profile_list:student']
      ),
    ];

    foreach ($taxonomyFields as $taxonomyField) {
      $fieldName = $profile->get($taxonomyField)->getFieldDefinition()->getLabel();

      $page[$taxonomyField] = [
        '#type' => 'details',
        '#title' => $fieldName,
        '#open' => FALSE,
        'table' => $this->getTableForRoleValues($fieldResults[$taxonomyField], $fieldName, $repTypes),
      ];
    }

    $ssccs = $this->getTableForRoleValues($fieldResults['sscc'], 'SSCC', $repTypes);

    $page['sscc'] = [
      '#type' => 'details',
      '#title' => 'Student-Staff Consultative Committees',
      '#open' => TRUE,
      'table' => $ssccs,
      '#cache' => array_merge(
        $this->getCacheTags(),
        ['group_list:academic_group']
      ),
    ];

    unset($ssccs['#rows']['totals']);
    $ssccs_empty = [];
    foreach ($ssccs['#rows'] as $sscc) {
      if ($sscc['Total (all rep types)']['data'] == 0) {
        $ssccs_empty[] = $sscc;
      }
    }

    $ssccs['#rows'] = $ssccs_empty;

    $page['ssccs_empty'] = [
      '#type' => 'details',
      '#title' => 'Student-Staff Consultative Committees with no reps',
      '#open' => FALSE,
      'table' => $ssccs,
      '#cache' => array_merge(
        $this->getCacheTags(),
        ['group_list:academic_group']
      ),
    ];

    $page['totals']['#rows']['no_reps']['Value'] = count($ssccs_empty);

    return $page;
  }

  public function getTableForRoleValues(
    $values,
    $labelForTable,
    $repTypes
  ) {
    $headers = array_merge([$labelForTable, 'Total (all rep types)'], array_keys($values));

    $rows = [];
    $overallTotal = 0;

    $totals = [
      $labelForTable => 'Total',
      'Total (all rep types)' => 0,
    ];
    foreach ($repTypes as $repLabel) {
      $totals[$repLabel] = 0;
    }

    foreach ($values as $repTypeLabel => $stats) {
      foreach ($stats as $valueLabel => $valueTotal) {
        $overallTotal += $valueTotal;

        if (!isset($rows[$valueLabel])) {
          $rows[$valueLabel] = [
            $labelForTable => [
              'data' => $valueLabel,
              'class' => 'bold',
            ],
            'Total (all rep types)' => [
              'data' => 0,
              'class' => 'bold',
            ]
          ];
          foreach ($repTypes as $repLabel) {
            $rows[$valueLabel][$repLabel] = [
              'data' => 0,
              'class' => '',
            ];
          }
        }
        $rows[$valueLabel]['Total (all rep types)']['data'] += $valueTotal;

        $rows[$valueLabel][$repTypeLabel]['data'] += $valueTotal;

        if (!isset($totals[$repTypeLabel])) {
          $totals[$repTypeLabel] = 0;
        }
        $totals[$repTypeLabel] += $valueTotal;
      }
    }

    if ($labelForTable == 'SSCC') {
      foreach ($rows as $valueLabel => $valueData) {
        foreach ($valueData as $repType => $repData) {
          if (isset($repData['data']) && $repData['data'] === 0) {
            $rows[$valueLabel][$repType]['class'] .= ' error';
          }
        }
      }
      // dd($rows);
    }

    ksort($rows);

    $totals['Total (all rep types)'] = $overallTotal;
    $rows['totals'] = $totals;

    return [
      '#type' => 'table',
      // '#caption' => $this->t('Key information'),
      '#header' => $headers,
      '#rows' => $rows,
      '#cache' => $this->getCacheTags(),
    ];
  }

  public function getCacheTags() {
    return [
      'max-age' => 60 * 60 * 24 * 7,
      'tags' => [
        'group_membership_record_list:academic_representative',
      ]
    ];
  }
}
