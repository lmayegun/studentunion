const { blocks, data, element, components, editor } = wp;
const { registerBlockType } = blocks;
const { dispatch, select } = data;
const { Fragment } = element;
const { PanelBody, BaseControl, Icon, RangeControl, IconButton, Toolbar, SelectControl } = components;
const { InnerBlocks, RichText, InspectorControls, PanelColorSettings, MediaUpload, BlockControls } = editor;
const __ = Drupal.t;

const settings = {
  title: __('Table of contents'),
  description: __('Table of contents based on headers in page from H2.'),
  icon: 'editor-ol-rtl',
  attributes: {
    title: {
      type: 'string',
    },
    levels: {
      type: 'number',
    },
  },
};

const edit = ({ className, attributes, setAttributes, isSelected }) => {
  const { title, levels } = attributes;

  return (<Fragment>
      <div className={className}>
          <RichText
            identifier="title"
            tagName="h2"
            value={title}
            placeholder={__('Table of contents')}
            onChange={nextTitle => {
              setAttributes({
                title: nextTitle,
              });
            }}
            onSplit={() => null}
            unstableOnSplit={() => null}
          />
          <div>
            Table of contents will display here.
          </div>
      </div>
      <InspectorControls>
        <PanelBody title={ __('Block Settings') }>
          <div>{title}</div>
        </PanelBody>
      </InspectorControls>
    </Fragment>
  );
};

const save = ({ className, attributes }) => {
  const { title, subtitle, text } = attributes;

  return (
    <div className={className}>
        {title && (
          <h2>{title}</h2>
        )}
      <div>
        Table of contents will display here.
      </div>
    </div>
  );
};

const category = {
  slug: 'toc',
  title: __('Navigation'),
};

const currentCategories = select('core/blocks').getCategories().filter(item => item.slug !== category.slug);
dispatch('core/blocks').setCategories([ category, ...currentCategories ]);

registerBlockType(`${category.slug}/toc`, { category: category.slug, ...settings, edit: edit, save: save });
