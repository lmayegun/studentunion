<?php

namespace Drupal\su_login\Service;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements Class SuLoginOauth2Service Controller.
 */
class DestinationCookieService {

  public const cookie_name = 'su_login_destination';

  /**
   * Constructs a new SuLoginOauth2Service object.
   */
  public function __construct() {
  }

  public static function hasCookie() {
    return isset($_COOKIE[static::cookie_name]) && $_COOKIE[static::cookie_name] != '';
  }

  /**
   * @param $destination
   * @return Cookie
   */
  public static function setCookie($destination = NULL, $sendResponse = false) {
    $destination = !is_null($destination) ? $destination : '/';
    if ($destination == '/user/accept-conditions') {
      $destination = '/user';
    }
    $cookie = new Cookie(static::cookie_name, $destination, 0, '/', NULL, FALSE);

    if ($sendResponse) {
      $response = new Response();
      $response->headers->setCookie($cookie);
      $response->send();
    }
    return $cookie;
  }

  /**
   * @param $request
   * @return mixed|string
   */
  public static function getCookie($request) {
    $cookies = $request->cookies;
    return $cookies->has(static::cookie_name) ? $cookies->get(static::cookie_name) : FALSE;
  }

  /**
   * Removing a cookie https://stackoverflow.com/a/14001301
   */
  public static function deleteCookie() {
    unset($_COOKIE[static::cookie_name]);
    setcookie(static::cookie_name, '', time() - 3600, '/');
  }

  /**
   * Redirect from a form to the path saved in the cookie
   *
   * @param $request
   * @param $form_state
   */
  public static function getRedirectDestination(Request $request) {
    $cookie = self::getCookie($request);
    $destination = $cookie && $cookie != '/' ? $cookie : '/user';
    return $destination;
  }
}
