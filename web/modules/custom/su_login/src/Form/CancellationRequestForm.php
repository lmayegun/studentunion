<?php

/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */

namespace Drupal\su_login\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CancellationRequestForm extends FormBase {


  /**
   * @return string
   */
  public function getFormId() {
    return 'su_login_cancellation_request_form';
  }

  /**
   * Weight; Used to sort the list of form elements
   * before being output; lower numbers appear before higher numbers.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|RedirectResponse
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $user = User::load($user->id());
    if ($user->isAuthenticated()) {
      $config = $this->config('su_login.suloginconfiguration');
      $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
      $plugins = $pluginManager->getForUser($user);
      foreach ($plugins as $plugin) {
        $plugin->alterRevokeForm($form, $user, $config);
      }

      $form['actions']['save'] = array(
        '#type'  => 'submit',
        '#value' => t('Save'),
      );
    }

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array                                $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $user = User::load($user->id());

    $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
    $plugins = $pluginManager->getForUser($user);
    foreach ($plugins as $plugin) {
      $plugin->alterRevokeFormSubmit($form, $form_state, $user);
      if ($plugin->isConditionRevoked($form, $form_state, $user)) {
        $plugin->redirect($plugin, $form_state);
      }
    }
    \Drupal::messenger()->addMessage('Thanks! We have updated your details.', 'status');
  }
}
