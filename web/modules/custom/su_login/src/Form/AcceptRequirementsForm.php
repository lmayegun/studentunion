<?php

/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */

namespace Drupal\su_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AcceptRequirementsForm extends FormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'su_login_accept_conditions_form';
  }

  /**
   * Weight; Used to sort the list of form elements
   * before being output; lower numbers appear before higher numbers.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|RedirectResponse
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $user = User::load($user->id());
    if ($user->isAuthenticated()) {
      $config = $this->config('su_login.suloginconfiguration');
      $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
      $plugins = $pluginManager->getForUser($user);
      foreach ($plugins as $plugin) {
        if ($plugin->appliesToUser($user)) {
          $plugin->alterAcceptForm($form, $user, $config);
        }
      }

      $form['actions']['save'] = array(
        '#type'  => 'submit',
        '#value' => t('Save'),
      );
    }

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array                                $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $user = User::load($user->id());

    $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
    $plugins = $pluginManager->getForUser($user);
    foreach ($plugins as $plugin) {
      if ($plugin->appliesToUser($user)) {
        $plugin->alterAcceptFormSubmit($form, $form_state, $user);
        if ($plugin->isConditionAccepted($form, $form_state, $user)) {
          $plugin->redirect($plugin, $form_state);
        }
      }
    }
    \Drupal::messenger()->addMessage('Thanks! We have updated your details.', 'status');
  }
}
