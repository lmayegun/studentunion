<?php

namespace Drupal\su_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SuLoginConfigurationForm.
 */
class SuLoginConfigurationForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'su_login.suloginconfiguration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'su_login_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('su_login.suloginconfiguration');
    $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
    $plugins = $pluginManager->getAll();
    foreach ($plugins as $plugin) {
      $plugin->alterAdminConfigForm($form, $form_state, $config);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);
    $config = $this->config('su_login.suloginconfiguration');
    $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
    $plugins = $pluginManager->getAll();
    foreach ($plugins as $plugin) {
      $plugin->alterAdminConfigFormSubmit($form, $form_state, $config);
    }
    $config->save();
  }

}
