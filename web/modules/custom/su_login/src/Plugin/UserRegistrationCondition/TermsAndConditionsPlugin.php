<?php

namespace Drupal\su_login\Plugin\UserRegistrationCondition;

use DateTimeZone;
use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\su_login\Plugin\UserRegistrationConditionPluginBase;
use Drupal\su_login\Plugin\UserRegistrationConditionPluginInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @see \Drupal\su_login\Annotation\UserRegistrationConditionPlugin
 * @see \Drupal\su_login\Plugin\UserRegistrationConditionPluginInterface
 *
 * @UserRegistrationConditionPlugin(
 *   id = "terms_and_conditions",
 *   description = @Translation("User must agree to terms and conditions."),
 *   userRoles = {}
 * )
 */
class TermsAndConditionsPlugin extends UserRegistrationConditionPluginBase implements UserRegistrationConditionPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Check if this plugin applies to this user
   */
  public function appliesToUser(User $user)
  {
    return true;
  }

  /**
   * Default redirect condition. Must return true or user will be redirected to accept terms and conditions
   *
   * @param User $user
   * @return bool
   */
  public function condition(User $user)
  {
    return $user->get('field_accept_terms_and_condition')->value == 1;
  }

  /**
   * Check terms and conditions is accepted
   *
   * @param $form
   * @param $form_state
   * @param $user
   * @return mixed
   */
  public function isConditionAccepted($form, $form_state, $user)
  {
    return $form_state->getValue('terms_and_conditions');
  }

  /**
   * Get the value from the custom condition form element
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function isConditionRevoked($form, $form_state, $user)
  {
    return $form_state->getValue('revoke_terms_and_conditions');
  }

  /**
   * Default form for accept terms and conditions
   *
   * @param $form
   * @param $user
   * @param $config
   */
  public function alterAcceptForm(&$form, $user, &$config)
  {
    $form['terms_and_conditions_info'] = [
      '#type'   => 'item',
      '#markup' => $config->get('terms_and_conditions_info.value'),
      '#title'  => t('Terms and conditions'),
      '#weight' => -15,
    ];

    $form['terms_and_conditions'] = [
      '#type'          => 'checkbox',
      '#title'         => t('I agree with the terms and conditions.'),
      '#description'   => $config->get('terms_and_conditions_description.value'),
      '#weight'        => -15,
      '#default_value' => $user->get('field_accept_terms_and_condition')->value,
      '#required'      => TRUE,
    ];

    return $form;
  }

  /**
   * Default submit form accept terms and conditions
   *
   * IF they accept:
   *  check terms and conditions
   *  add data_protection_granted date
   *
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterAcceptFormSubmit($form, $form_state, $user)
  {
    if ($accept = $this->isConditionAccepted($form, $form_state, $user)) {
      $user->set('field_accept_terms_and_condition', $accept);
      $datetime = new \DateTime(date('Y-m-d\TH:i:s', \Drupal::time()->getRequestTime()), new \DateTimeZone(date_default_timezone_get()));
      $datetime = DrupalDateTime::createFromDateTime($datetime);
      $user->set('field_data_protection_granted', $datetime
        ->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
        ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
      $user->save();
    }
  }

  /**
   * Default revoke form
   *
   * @param $form
   * @param $user
   * @param $config
   */
  public function alterRevokeForm(&$form, $user, &$config)
  {

    $form['revoke_terms_and_conditions_info'] = [
      '#type'   => 'item',
      '#markup' => $config->get('cancel_info.value'),
      '#title'  => t('Cancellation request'),
      '#weight' => -15,
    ];

    $form['revoke_terms_and_conditions'] = [
      '#type'          => 'checkbox',
      '#title'         => t('I would like to delete my account.'),
      '#description'   => $config->get('revoke_terms_and_conditions_description.value'),
      '#weight'        => -15,
      '#default_value' => FALSE,
    ];
  }

  /**
   * Default revoke submit form
   *
   * If they revoke:
   *  uncheck terms and conditions
   *  add field_data_protection_revoked date
   *
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterRevokeFormSubmit($form, $form_state, $user)
  {
    if ($accept = $this->isConditionRevoked($form, $form_state, $user)) {
      $user->set('field_accept_terms_and_condition', FALSE);
      $datetime = new \DateTime(date('Y-m-d\TH:i:s', \Drupal::time()->getRequestTime()), new \DateTimeZone(date_default_timezone_get()));
      $datetime = DrupalDateTime::createFromDateTime($datetime);
      $user->set('field_data_protection_revoked', $datetime
        ->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
        ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
      $user->save();
    }
  }

  /**
   * Default admin configuration form
   *
   * @param $form
   * @param $form_state
   * @param $config
   */
  public function alterAdminConfigForm(&$form, $form_state, &$config)
  {
    $route_provider = \Drupal::service('router.route_provider');
    $link = $route_provider->getRouteByName('su_login.accept_required')->getPath();

    $form['user_condition'] = [
      '#type'  => 'details',
      '#title' => t('User condition'),
      '#open'  => TRUE,
    ];

    $form['user_condition']['terms_and_conditions'] = [
      '#type'  => 'details',
      '#title' => t('Accept form: Terms and conditions'),
      '#open'  => FALSE,
    ];

    $form['user_condition']['terms_and_conditions']['terms_and_conditions_info'] = array(
      '#title'         => t('Terms and conditions info'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('terms_and_conditions_info.value'),
      '#format'        => $config->get('terms_and_conditions_info.format'),
    );

    $form['user_condition']['terms_and_conditions']['terms_and_conditions_description'] = array(
      '#title'         => t('Terms and conditions description'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('terms_and_conditions_description.value'),
      '#format'        => $config->get('terms_and_conditions_description.format'),
    );

    $form['user_condition']['revoke_terms_and_conditions'] = [
      '#type'  => 'details',
      '#title' => t('Revoke form: Terms and conditions'),
      '#open'  => FALSE,
    ];

    $form['user_condition']['revoke_terms_and_conditions']['revoke_terms_and_conditions_info'] = array(
      '#title'         => t('Cancel info'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('revoke_terms_and_conditions_info.value'),
      '#format'        => $config->get('revoke_terms_and_conditions_info.format'),
    );

    $form['user_condition']['revoke_terms_and_conditions']['revoke_terms_and_conditions_description'] = array(
      '#title'         => t('Cancel description'),
      '#type'          => 'text_format',
      '#description'   => t('The comment will be display on <a href="@link" target="_blank">terms and conditions form</a> page.', ['@link' => $link]),
      '#default_value' => $config->get('revoke_terms_and_conditions_description.value'),
      '#format'        => $config->get('revoke_terms_and_conditions_description.format'),
    );
  }

  /**
   * Default admin configuration submit form
   *
   * @param $form
   * @param $form_state
   * @param $config
   */
  public function alterAdminConfigFormSubmit($form, $form_state, &$config)
  {
    $values = $form_state->getValues();
    $config->set('terms_and_conditions_info.value', $values['terms_and_conditions_info']['value'])
      ->set('terms_and_conditions_info.format', $values['terms_and_conditions_info']['format'])
      ->set('terms_and_conditions_description.value', $values['terms_and_conditions_description']['value'])
      ->set('terms_and_conditions_description.format', $values['terms_and_conditions_description']['format'])
      ->set('revoke_terms_and_conditions_info.value', $values['revoke_terms_and_conditions_info']['value'])
      ->set('revoke_terms_and_conditions_info.format', $values['revoke_terms_and_conditions_info']['format'])
      ->set('revoke_terms_and_conditions_description.value', $values['revoke_terms_and_conditions_description']['value'])
      ->set('revoke_terms_and_conditions_description.format', $values['revoke_terms_and_conditions_description']['format']);
  }
}
