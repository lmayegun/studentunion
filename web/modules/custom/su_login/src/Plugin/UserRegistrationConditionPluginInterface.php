<?php

namespace Drupal\su_login\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\user\Entity\User;

/**
 * An interface for enabled determiner plugins.
 *
 */
interface UserRegistrationConditionPluginInterface extends PluginInspectionInterface
{
  /**
   * Provide a description of the plugin.
   *
   * @return string
   *   A string description of the plugin.
   */
  public function description();

  /**
   *
   *
   * @param $role
   * @return mixed
   */
  public function forUserRole($role);

  /**
   * Redirect
   *
   * @param $form
   * @param $form_state
   */
  public function getRedirectDestination();

  /**
   * Condition necessary to not go to accept and conditions form
   *
   * @param User $user
   * @return mixed
   */
  public function condition(User $user);

  /**
   * @param $plugin
   * @param $form_state
   * @return mixed
   */
  public function redirect($plugin, $form_state);

  /**
   * @param User $user
   * @return mixed
   */
  public function appliesToUser(User $user);
}
