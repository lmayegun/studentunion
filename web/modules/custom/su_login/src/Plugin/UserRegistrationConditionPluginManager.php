<?php

namespace Drupal\su_login\Plugin;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\su_login\Annotation\UserRegistrationConditionPlugin;
use Drupal\user\Entity\User;

/**
 * A plugin manager.
 *
 * https://www.drupal.org/docs/drupal-apis/plugin-api/creating-your-own-plugin-manager
 * https://medium.com/@uditrawat/custom-plugin-type-in-drupal-8-5c243b4ed152
 *
 * @see \Drupal\su_login\Annotation\UserRegistrationConditionPlugin
 * @see \Drupal\su_login\Plugin\UserRegistrationConditionPluginManager
 * @see plugin_api
 */
class UserRegistrationConditionPluginManager extends DefaultPluginManager
{
  /**
   * Constructs a UserRegistrationConditionPluginManager object.
   *
   * @param \Traversable                                  $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface      $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
  {
    parent::__construct(
      'Plugin/UserRegistrationCondition',
      $namespaces,
      $module_handler,
      'Drupal\su_login\Plugin\UserRegistrationConditionPluginInterface',
      'Drupal\su_login\Annotation\UserRegistrationConditionPlugin'
    );
    $this->alterInfo('su_login_user_registration_condition_info');
    $this->setCacheBackend($cache_backend, 'su_login_user_registration_condition_plugins');
  }

  /**
   * Only return plugins that are either for all users (no role specified)
   * or specified for one of the user's roles
   *
   * @param User $user
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getForUser(User $user)
  {
    $plugins = [];
    $roles = $user->getRoles();
    foreach ($this->getDefinitions() as $plugin_id => $plugin) {
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->isForAnyRole() || $plugin->forUserRoles($roles)) {
        $plugins[] = $plugin;
      }
    }
    return $plugins;
  }

  /**
   * Returns all plugins
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getAll()
  {
    $plugins = [];
    foreach ($this->getDefinitions() as $plugin_id => $plugin) {
      $plugins[] = $this->createInstance($plugin_id);
    }
    return $plugins;
  }

  /**
   * Return plugin if role specified
   *
   * @param string $role
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getForRole(string $role)
  {
    $plugins = [];
    foreach ($this->getDefinitions() as $plugin_id => $plugin) {
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->forUserRole($role)) {
        $plugins[] = $plugin;
      }
    }
    return $plugins;
  }
}
