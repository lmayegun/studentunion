<?php

namespace Drupal\su_login\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\Entity\User;

/**
 * A base class to help developers implement their own plugins.
 *
 * @see \Drupal\su_login\Annotation\UserRegistrationConditionPlugin
 * @see \Drupal\su_login\Plugin\UserRegistrationConditionPluginInterface
 */
abstract class UserRegistrationConditionPluginBase extends PluginBase implements UserRegistrationConditionPluginInterface, ContainerFactoryPluginInterface
{

  public function id()
  {
    return $this->pluginId;
  }

  /**
   * Retrieve the @description property from the annotation and return it.
   *
   * {@inheritdoc}
   */
  public function description()
  {
    return $this->pluginDefinition['description'];
  }

  /**
   * Based on the 'userRoles' defined in the plugin annotation
   *
   * @param $role
   * @return bool|mixed
   */
  public function forUserRole($role)
  {
    return in_array($role, $this->pluginDefinition['userRoles']);
  }

  /**
   * If role defined in the plugin annotation 'userRoles' is empty
   *
   * @return bool
   */
  public function isForAnyRole()
  {
    return count($this->pluginDefinition['userRoles']) == 0;
  }

  /**
   * Returns an array containing all of
   * the roles in $roles whose values
   * exist in in the plugin annotation 'userRoles'.
   *
   * @param array $roles
   * @return array
   */
  public function forUserRoles(array $roles)
  {
    return array_intersect($roles, $this->pluginDefinition['userRoles']);
  }

  /**
   * Condition to redirect to accept and conditions form
   * if the condition is passed (is true) we do not redirect
   * if the condition is false we do redirect to terms and conditions form
   *
   * @param User $user
   */
  public function condition(User $user)
  {
  }

  /**
   * Get the value from the checkbox condition form element
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function isConditionAccepted($form, $form_state, $user)
  {
  }

  /**
   * Get the value from the custom condition form element
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function isConditionRevoked($form, $form_state, $user)
  {
  }

  /**
   * Redirect
   *
   * @param $form
   * @param $form_state
   */
  public function getRedirectDestination()
  {
    $cookieService = \Drupal::service('su_login.destination_cookie_service');
    return $cookieService->getRedirectDestination(\Drupal::request());
  }

  /**
   * Check if this plugin applies to this user
   */
  public function appliesToUser(User $user)
  {
    return true;
  }

  /**
   * Alter the terms and conditions form to add more conditions
   *
   * @param $form
   * @param $user
   * @param $config
   */
  public function alterAcceptForm(&$form, $user, &$config)
  {
  }

  /**
   * Alter the terms and conditions form submit
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterAcceptFormSubmit($form, $form_state, $user)
  {
  }

  /**
   * Alter the revoke form
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterRevokeForm(&$form, $user, &$config)
  {
  }

  /**
   * Alter the revoke form submit
   *
   * @param $form
   * @param $form_state
   * @param $user
   */
  public function alterRevokeFormSubmit($form, $form_state, $user)
  {
  }

  /**
   * Alter admin panel settings form
   *
   * @param $form
   * @param $form_state
   * @param $config
   */
  public function alterAdminConfigForm(&$form, $form_state, &$config)
  {
  }

  /**
   * Alter admin panel settings form submit
   *
   * @param $form
   * @param $form_state
   * @param $config
   */
  public function alterAdminConfigFormSubmit($form, $form_state, &$config)
  {
  }

  /**
   * @param $plugin
   * @param $form_state
   * @return mixed|void
   */
  public function redirect($plugin, $form_state)
  {
  }
}
