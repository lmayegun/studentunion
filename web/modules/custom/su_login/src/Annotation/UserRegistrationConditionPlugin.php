<?php

namespace Drupal\su_login\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a UserRegistrationConditionPlugin annotation object.
 *
 * This allows other modules to add requirements to a new user's first sign in,
 * e.g. terms and conditions, data protection consent, etc.
 *
 * Note that the "@ Annotation" line below is required and should be the last
 * line in the docblock. It's used for discovery of Annotation definitions.
 *
 * @see \Drupal\su_login\Plugin\UserRegistrationConditionPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class UserRegistrationConditionPlugin extends Plugin
{
  /**
   * Description of plugin
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Filter by roles
   *
   * @var array
   */
  public $userRoles = [];
}
