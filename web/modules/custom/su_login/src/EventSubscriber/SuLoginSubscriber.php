<?php

namespace Drupal\su_login\EventSubscriber;

use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class SuLoginSubscriber implements EventSubscriberInterface {
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForRedirection');
    return $events;
  }

  /**
   * @param GetResponseEvent $event
   */
  public function checkForRedirection(RequestEvent $event) {
    $user = \Drupal::currentUser();

    // Conditions
    // 1. User is neither anonymous nor administrator
    if ($user->isAnonymous() || $user->id() === '1' || isset($_SESSION['election_only'])) {
      return $event;
    }

    $current_path = \Drupal::service('path.current')->getPath();

    $route_provider = \Drupal::service('router.route_provider');
    $destination = $route_provider->getRouteByName('su_login.accept_required')->getPath();

    $user = User::load($user->id());

    // Prevent redirecting to registration for some routes
    $immuneRoutes = [
      'user.logout',               // 2. User can go to logout page
      'su_login.accept_required',  // 3. User can stay in accept terms and conditions page
      'masquerade.unmasquerade'    // 4. User can go to unmasquerade
    ];
    foreach ($immuneRoutes as $route) {
      if ($current_path === $route_provider->getRouteByName($route)->getPath()) {
        return $event;
      }
    }

    // Load any conditions for this user and redirect if required
    $pluginManager = \Drupal::service('plugin.manager.su_login.user_registration_condition');
    $plugins = $pluginManager->getForUser($user);
    $loginService = \Drupal::service('su_login.destination_cookie_service');
    foreach ($plugins as $plugin) {
      if ($plugin->appliesTouser($user)) {
        if (!$plugin->condition($user)) {
          if (!$loginService->hasCookie()) {
            $loginService->setCookie(\Drupal::service('path.current')->getPath());
          }
          $event->setResponse(new RedirectResponse($destination));
          return $event;
        }
      }
    }

    return $event;
  }
}
