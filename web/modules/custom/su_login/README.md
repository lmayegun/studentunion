CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


# Introduction

This module includes two main functionalities:

* A new ucl login page
* A force redirect to accept terms and conditions for all users

Most of our users login into our website through their UCL account. However, some users do still need to create local accounts,
and log in with them.

su_login module creates a new route _/user/login-ucl_ which is the main hub for log in. When the user clicks log in, it redirects to this custom page.
Also, this page contains the default links (in a sidebar) to log in with a local account, creates a new local account or reset your local account password.

This login-ucl page renders a custom form that other modules can add, or edit (e.g. adding extra information, or adding other buttons).
This form by default contains also information for new students.

After a user has logged in, it forces the user to accept the terms and conditions (user 1(administrator account) is excluded).
Otherwise, the only thing they can do until then is log out. Other modules can add more conditions that users needs to accept before they can use
the website.


# Requirements

# Recommended modules

# Installation

# Configuration

# Troubleshooting

# FAQ

# Maintainers
