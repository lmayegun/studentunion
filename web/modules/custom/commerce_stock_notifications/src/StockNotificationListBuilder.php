<?php

namespace Drupal\commerce_stock_notifications;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Stock notification entities.
 *
 * @ingroup commerce_stock_notifications
 */
class StockNotificationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['email'] = $this->t('User E-Mail');
    $header['product'] = $this->t('Product');
    $header['submit'] = $this->t('Submitted on');
    $header['sent'] = $this->t('Sent on');
    $header['action'] = $this->t('Unsubscribe link');
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    try {
      /* @var $entity \Drupal\commerce_stock_notifications\Entity\StockNotification */
      $row['id'] = $entity->id();
      $row['email'] = $entity->getEmail();
      $row['product'] = ProductVariation::load($entity->get('product_id')->getString())->getTitle();
      $row['submit'] = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $entity->get('submit_time')->getString());
      if (!empty($entity->get('sent_time')->getString())) {
        $row['sent'] = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $entity->get('sent_time')
          ->getString());
      } else {
        $row['sent'] = t('Not sent yet');
      }
      $row['action'] = \Drupal::request()->getSchemeAndHttpHost() .
        '/user/' . $entity->get('user_id')->getString() .
        '/stock-notifications/unsubscribe/' . $entity->id();
      return $row;
    }
    catch (\Error $e) {
      return [];
    }
  }

}
