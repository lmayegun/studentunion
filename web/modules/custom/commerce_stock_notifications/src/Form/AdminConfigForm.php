<?php

namespace Drupal\commerce_stock_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminConfigForm.
 */
class AdminConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_stock_notifications_admin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_stock_notifications.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = self::getValuesFromConfig();

    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail subject'),
      '#required' => TRUE,
      '#default_value' => $config['email_subject'],
    ];

    $form['email_subject_token_ui'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user', 'commerce_product_variation'],
    ];

    $form['email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('HTML Body'),
      '#required' => TRUE,
      '#default_value' => $config['email_body'],
    ];

    $form['email_body_token_ui'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user', 'commerce_product_variation'],
    ];

    $form['oos_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Out of stock message'),
      '#description' => $this->t('A message that the users will see when the product is out of stock.'),
      '#required' => TRUE,
      '#default_value' => $config['oos_message'],
    ];

    $form['oos_token_ui'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user', 'commerce_product_variation'],
    ];

    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Success message'),
      '#description' => $this->t('A message that the users will see when they successfully submit their email for notifications.'),
      '#required' => TRUE,
      '#default_value' => $config['success_message'],
    ];

    $form['success_token_ui'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user', 'commerce_product_variation'],
    ];

    $form['duplicate_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Duplicate message'),
      '#description' => $this->t('A message that the users will see when they submit a duplicate email address for notifications.'),
      '#required' => TRUE,
      '#default_value' => $config['duplicate_message'],
    ];

    $form['duplicate_token_ui'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user', 'commerce_product_variation'],
    ];

    $form['purge_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Purge interval'),
      '#description' => $this->t('Sent notifications will be kept in the table for X days.'),
      '#field_suffix' => $this->t('days'),
      '#required' => TRUE,
      '#default_value' => $config['purge_interval'],
      '#attributes' => [
        'min' => 1,
      ],
    ];

    $form['allow_anonymous'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow anonymous signup'),
      '#default_value' => $config['allow_anonymous'] ?? false
    ];

    $form['enter_again_to_unsubscribe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Unsubscribe if email entered again'),
      '#default_value' => $config['enter_again_to_unsubscribe'] ?? false
    ];

    $form['unsubscribe_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Unsubscribe message'),
      '#description' => $this->t('A message that the users will see when they submit a duplicate email address for notifications and it has been unsubscribed (enable with the setting above).'),
      '#required' => TRUE,
      '#default_value' => $config['unsubscribe_message'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $days = $form_state->getValue('purge_interval');
    if ((int) $days <= 0) {
      $form_state->setErrorByName('purge_interval', $this->t('Purge interval must be a positive number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->cleanValues();
    $config = $form_state->getValues();

    $this->config('commerce_stock_notifications.config')
      ->setData($config)
      ->save();
  }

  /**
   * Returns the configuration values.
   *
   * @return array
   *   Associative array containing the configuration.
   */
  public static function getValuesFromConfig() {
    // Fetch config.
    $config = \Drupal::configFactory()->get('commerce_stock_notifications.config')->getRawData();
    $defaults = self::getDefaults();
    $values = [];
    // OOS Message.
    $values['oos_message'] = $config['oos_message'] ?? $defaults['oos_message'];
    $values['success_message'] = $config['success_message'] ?? $defaults['success_message'];
    $values['duplicate_message'] = $config['duplicate_message'] ?? $defaults['duplicate_message'];
    $values['purge_interval'] = $config['purge_interval'] ?? $defaults['purge_interval'];
    $values['email_subject'] = $config['email_subject'] ?? $defaults['email_subject'];
    $values['email_body'] = $config['email_body'] ?? $defaults['email_body'];
    $values['allow_anonymous'] = $config['allow_anonymous'] ?? $defaults['allow_anonymous'];
    $values['enter_again_to_unsubscribe'] = $config['enter_again_to_unsubscribe'] ?? $defaults['enter_again_to_unsubscribe'];
    $values['unsubscribe_message'] = $config['unsubscribe_message'] ?? $defaults['unsubscribe_message'];
    
    return $values;
  }

  /**
   * Get list of default values.
   *
   * @return array
   *   Default values.
   */
  public static function getDefaults() {
    return [
      'email_subject' => t('[commerce_product_variation:title] is back in stock | [site:name]'),
      'email_body' => t('[commerce_product_variation:title] is back in stock | [site:name]'),
      'oos_message' => t('Please notify me when this product is back in stock.'),
      'success_message' => t('You will be notified when this product is back in stock.'),
      'duplicate_message' => t('A request for notification has already been made for this product with this email address.'),
      'purge_interval' => 30,
      'allow_anonymous' => false,
      'enter_again_to_unsubscribe' => false,
      'unsubscribe_message' => t('E-mail address already registered, so it has been unsubscribed.')
    ];
  }

}
