<?php

namespace Drupal\commerce_stock_notifications\Controller;

use Drupal\commerce_stock_notifications\Entity\StockNotification;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class StockNotificationController.
 *
 * @package Drupal\commerce_stock_notifications\Controller
 */
class StockNotificationController extends ControllerBase {

  /**
   * Unsubscribe the user from the notification.
   *
   * @param string $user
   *   User ID.
   * @param string $notification_id
   *   Notification ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function unsubscribe(string $user, string $notification_id) {
    /** @var \Drupal\commerce_stock_notifications\Entity\StockNotification $notification */
    $notification = $this->entityTypeManager()->getStorage('commerce_stock_notification')->load($notification_id);
    if ($notification === NULL) {
      return new RedirectResponse('/system/404');
    }
    // Delete if you have the access to.
    if ($notification->access('delete')) {
      $notification->delete();
      $this->messenger()->addStatus($this->t('Successfully unsubscribed.'), $this->entityTypeManager()->getStorage('user')->load($user));
      return new RedirectResponse('/');
    }
    else {
      return new RedirectResponse('/system/403');
    }
  }

}
