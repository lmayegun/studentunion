<?php

namespace Drupal\commerce_stock_notifications\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Stock notification entities.
 */
class StockNotificationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    return parent::getViewsData();
  }

}
