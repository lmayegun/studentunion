<?php

namespace Drupal\commerce_stock_notifications\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Stock notification entity.
 *
 * @ingroup commerce_stock_notifications
 *
 * @ContentEntityType(
 *   id = "commerce_stock_notification",
 *   label = @Translation("Stock notification"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_stock_notifications\StockNotificationListBuilder",
 *     "views_data" = "Drupal\commerce_stock_notifications\Entity\StockNotificationViewsData",
 *
 *     "access" = "Drupal\commerce_stock_notifications\StockNotificationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_stock_notifications\StockNotificationHtmlRouteProvider",
 *     },
 *   },
 *   form = {
 *    "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *   },
 *   base_table = "commerce_stock_notification",
 *   admin_permission = "administer stock notification entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" ="product_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/structure/commerce_stock_notification",
 *     "delete-form" = "/admin/structure/commerce_stock_notification/{commerce_stock_notification}/delete",
 *     "delete-multiple-form" = "/admin/structure/commerce_stock_notification/delete",
 *   },
 *   field_ui_base_route = "commerce_stock_notifications.admin_config_form"
 * )
 */
class StockNotification extends ContentEntityBase implements StockNotificationInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['langcode']->setDisplayConfigurable('form', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user who subscribed to this notification'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['email'] = BaseFieldDefinition::create('string')
      ->setLabel(t('E-Mail'))
      ->setDescription(t('A email where to send the notification'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['product_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product variation'))
      ->setDescription(t('The product to which the user subscribed.'))
      ->setSetting('target_type', 'commerce_product_variation')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['submit_time'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Submitted on'))
      ->setDescription(t('The time that the notification request was submitted.'))
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['sent_time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Sent on'))
      ->setDescription(t('The time that the notification was sent to the user.'))
      ->setSetting('datetime_type', 'datetime')
      ->setDefaultValue(NULL)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 5,
      ]);
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('email')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($email) {
    $this->set('email', $email);
    return $this;
  }
}
