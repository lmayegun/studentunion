<?php

namespace Drupal\commerce_stock_notifications\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Stock notification entities.
 *
 * @ingroup commerce_stock_notifications
 */
interface StockNotificationInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the email address that the email will be sent to.
   *
   * @return string
   *   The email address.
   */
  public function getEmail();

  /**
   * Set an email address that the email will be sent to.
   *
   * @param string $email
   *
   * @return StockNotificationInterface
   */
  public function setEmail($email);

}
