<?php

namespace Drupal\commerce_stock_notifications\Plugin\Commerce\InlineForm;

use Drupal\commerce\Plugin\Commerce\InlineForm\InlineFormBase;
use Drupal\commerce_stock_notifications\Form\AdminConfigForm;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for back in-stock notification.
 *
 * @CommerceInlineForm(
 *   id = "commerce_stock_notifications",
 *   label = @Translation("Back in-stock notification"),
 * )
 */
class Notification extends InlineFormBase {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The e-mail validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Notification object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The e-mail validator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user, EmailValidatorInterface $email_validator, EntityTypeManagerInterface $entity_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->config = $config_factory->get('commerce_stock_notifications.config');
    $this->currentUser = $current_user;
    $this->emailValidator = $email_validator;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('email.validator'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The variation_id is passed via configuration to avoid serializing the
      // variation.
      'variation_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function requiredConfiguration() {
    return ['variation_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface|null $variation */
    $variation = $this->entityTypeManager->getStorage('commerce_product_variation')->load($this->configuration['variation_id']);
    if (!$variation) {
      throw new \RuntimeException('Invalid variation_id given to the commerce_stock_notifications inline form.');
    }

    $inline_form['notification'] = [
      '#type' => 'fieldset',
      '#title' => $this->config->get('oos_message') ?? $this->t('Please notify me when this product is back in stock.'),
      'notify_email' => [
        '#type' => 'textfield',
        '#title' => $this->t('E-Mail'),
        '#required' => TRUE,
        '#default_value' => $this->currentUser->getEmail(),
      ],
      'notify_submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Notify me'),
        '#limit_validation_errors' => [
          $inline_form['#parents'],
        ],
        '#submit' => [
          [get_called_class(), 'preventParentSubmit'],
        ],
      ],
    ];

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::validateInlineForm($inline_form, $form_state);

    $notify_email_parents = array_merge($inline_form['#parents'], ['notification', 'notify_email']);
    $notify_email = $form_state->getValue($notify_email_parents);
    $notify_email_path = implode('][', $notify_email_parents);
    $variation_id = $this->configuration['variation_id'];

    // Check if user is logged in.
    if ($this->currentUser->isAnonymous()) {
      $form_state->setErrorByName($notify_email_path, $this->t('You must be authenticated to do that.'));
    }

    // Check if e-mail is valid.
    if (!$this->emailValidator->isValid($notify_email)) {
      $form_state->setErrorByName($notify_email_path, $this->t('Invalid e-mail'));
    }

    // Check if already subscribed.
    $notification_count_query = $this->entityTypeManager
      ->getStorage('commerce_stock_notification')
      ->getQuery()
      ->condition('email', $notify_email)
      ->condition('product_id', $variation_id)
      ->condition('sent_time', NULL, 'IS')
      ->count();

    if ($notification_count_query->execute() > 0) {
      $duplicate_message = AdminConfigForm::getValuesFromConfig()['duplicate_message'];
      $form_state->setErrorByName($notify_email_path, $duplicate_message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);

    $notify_email_parents = array_merge($inline_form['#parents'], ['notification', 'notify_email']);
    $variation_id = $this->configuration['variation_id'];

    $this->entityTypeManager
      ->getStorage('commerce_stock_notification')
      ->create([
        'user_id' => $this->currentUser->id(),
        'product_id' => $variation_id,
        'submit_time' => $this->time->getRequestTime(),
        'email' => $form_state->getValue($notify_email_parents),
      ])
      ->save();

    $this->messenger()->addStatus(AdminConfigForm::getValuesFromConfig()['success_message']);
  }

  /**
   * Empty submit callback to prevent add to cart submit handler to be called.
   */
  public static function preventParentSubmit(array $form, FormStateInterface $form_state) {
  }

}
