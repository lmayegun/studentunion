<?php

namespace Drupal\commerce_stock_notifications\Plugin\QueueWorker;

use Drupal\commerce_stock_notifications\Form\AdminConfigForm;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "commerce_stock_notifications",
 *   title = @Translation("Commerce Stock Notification Queue"),
 *   cron = {"time" = 120}
 * )
 */
class CommerceStockNotifyQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Mail service for sending out the mail.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  private $mailManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  private $token;

  /**
   * CommerceStockNotifyQueue constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param MailManagerInterface $mailManager
   *   Mail manager.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param Token $token
   *   Token.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailManagerInterface $mailManager, EntityTypeManagerInterface $entityTypeManager, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->mailManager = $mailManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->token = $token;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail'),
      $container->get('entity_type.manager'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data->get('sent_time')->getValue())) {
      $user = user_load_by_mail($data->getEmail());
      $product = $this->entityTypeManager->getStorage('commerce_product_variation')->load($data->get('product_id')->getString());

      /** @var \Drupal\commerce_stock\StockServiceManagerInterface $stockManager */
      $stockServiceManager = \Drupal::service('commerce_stock.service_manager');
      $checker = $stockServiceManager->getService($product)->getStockChecker();
      $locations = $this->entityTypeManager->getStorage('commerce_stock_location')->loadMultiple();
      if (!$checker->getIsInStock($product, $locations)) {
        $time = new DrupalDateTime();
        $data->set('sent_time', $time->getTimestamp())->save();
        return;
      }

      if (!$product->isPublished() || !$product->getProduct()->isPublished()) {
        // Product is inactive, ignore it.
        // throw new \Exception($this->t('Product "@sku" (or it\'s parent product) is unpublished. Skipping for now.', [
        //   '@sku' => $product->getSku(),
        // ]));
        return;
      }
      // Parameters.
      $params = [
        'mail_title' => $this->token->replace(AdminConfigForm::getValuesFromConfig()['email_subject'], [
          'commerce_product_variation' => $product,
          // 'user' => $user,
        ]),
        'message' => $this->token->replace(AdminConfigForm::getValuesFromConfig()['email_body'], [
          'commerce_product_variation' => $product,
          // 'user' => $user,
        ]),
      ];
      // Send a message.
      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $this->mailManager->mail('commerce_stock_notifications', 'commerce_stock_notifications.notify', $data->getEmail(), $lang, $params, NULL, TRUE);
      $time = new DrupalDateTime();

      $data->set('sent_time', $time->getTimestamp())->save();
    }
  }
}
