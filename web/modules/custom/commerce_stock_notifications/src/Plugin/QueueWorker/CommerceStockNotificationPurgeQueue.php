<?php

namespace Drupal\commerce_stock_notifications\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "commerce_stock_notifications_cleanup",
 *   title = @Translation("Commerce Stock Notification Purge Queue"),
 *   cron = {"time" = 240}
 * )
 */
class CommerceStockNotificationPurgeQueue extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $data->delete();
  }

}
