INTRODUCTION
------------

The Commerce Stock Notifications module modifies the "Add to Cart" 
form on products that are out of stock, allowing the users to enter an 
e-mail address to be notified when the product is back in stock.

 * For a full description of the module, visit the project page: 
   https://www.drupal.org/project/commerce_stock_notifications
   
 * To submit bugs and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_stock_notifications
   
REQUIREMENTS
------------

Commerce Stock Notifications requires the following modules:

 * Commerce (https://www.drupal.org/project/commerce)
 * Commerce Product (https://www.drupal.org/project/commerce)
 * Commerce Stock (https://www.drupal.org/project/commerce_stock)
 * Commerce Stock Field (https://www.drupal.org/project/commerce_stock)
 
RECOMMENDED MODULES
-------------------

 * Swift Mailer (https://www.drupal.org/project/swiftmailer)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. 
   Visit https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.
 
CONFIGURATION
-------------

 * Configure the notification form messages in Commerce » Configuration » Stock 
 Notification.

MAINTAINERS
-----------

Current maintainers:
 * Zvonimir Rudinski (zvonimirr00) - https://www.drupal.org/u/zvonimirr00
 * Bojan M. (bojan.m) - https://www.drupal.org/u/bojanm

This project has been sponsored by:
 * Studio Present (https://www.studiopresent.com)
