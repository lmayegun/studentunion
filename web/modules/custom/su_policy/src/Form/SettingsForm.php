<?php

namespace Drupal\su_policy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'su_policy.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_policy_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('su_policy.settings');
    $form['policy_page_header'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text for top of policy page'),
      '#default_value' => $config->get('policy_page_header'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('su_policy.settings')
      ->set('policy_page_header', $form_state->getValue('policy_page_header')['value'])
      ->save();
  }

}
