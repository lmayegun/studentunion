<?php

namespace Drupal\su_content\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the NoHasInTagName constraint.
 */
class NoHasInTagNameValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    foreach ($value as $item) {
      if (strstr($item->value, '#')) {
        $this->context->addViolation($constraint->hasHash, ['%value' => $item->value]);
      }
    }
  }

}
