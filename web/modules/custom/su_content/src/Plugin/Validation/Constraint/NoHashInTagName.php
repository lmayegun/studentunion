<?php

namespace Drupal\su_content\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value contains a hash sign.
 *
 * @Constraint(
 *   id = "NoHashInTagName",
 *   label = @Translation("No hash in tag name", context = "Validation"),
 *   type = "string"
 * )
 */
class NoHashInTagName extends Constraint {

  public string $hasHash = 'Tag names are not allowed to contain a hash sign (#). They are used as headings links, so they should be normal sentence case headings, e.g. "Animal rights", not "#animalrights';

}
