<?php

namespace Drupal\su_content\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;

/**
 * Show a message (customisable in settings) for org leaders.
 *
 * @Block(
 *   id = "embed_poll",
 *   admin_label = @Translation("Poll embed"),
 *   category = @Translation("Blocks"),
 * )
 */
class EmbedPoll extends BlockBase {

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access polls');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $poll_lookup = isset($config['poll_lookup']) ? $config['poll_lookup'] : '';
    $poll_title = isset($config['poll_title']) ? $config['poll_title'] : 'Poll';

    $query = \Drupal::entityQuery('poll');
    $query->condition('id', $poll_lookup, '=');
    $polls = \Drupal::entityTypeManager()->getStorage('poll')->loadMultiple($query->execute());

    $output = array();
    if ($polls) {
      $poll = reset($polls);
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('poll');
      $output = $view_builder->view($poll, 'block');
      $output['#title'] = $poll_title;
      // $output['#prefix'] = "<h3 class='poll-question'>" . $poll->label() . "</h3>";
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    // Set a default to be most recent poll in block.
    $default_poll = \Drupal::entityTypeManager()
      ->getStorage('poll')
      ->getMostRecentPoll();

    // If there is already a poll set, ensure it appears as default.
    $current_poll_id = isset($config['poll_lookup']) ? $config['poll_lookup'] : FALSE;
    if ($current_poll_id) {
      $form_query = \Drupal::entityQuery('poll');
      $form_query->condition('id', $current_poll_id, '=')->pager(1);

      $default_poll = \Drupal::entityTypeManager()
        ->getStorage('poll')
        ->loadMultiple($form_query->execute());
    }

    // Run a reset() to ensure arr set to proper index.
    $default_poll_obj = reset($default_poll);

    $form['poll_lookup'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'poll',
      '#title' => $this->t('Poll to display'),
      '#description' => $this->t('Begin typing to search for an existing poll'),
      '#default_value' => $default_poll_obj,
    );

    // Allow content editors to override the poll block title.
    $form['poll_title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Block title override'),
      '#description' => $this->t('Override the block title (leave blank to omit)'),
      '#default_value' => 'Poll',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('poll_lookup', $form_state->getValue('poll_lookup'));
    $this->setConfigurationValue('poll_title', $form_state->getValue('poll_title'));
  }
}
