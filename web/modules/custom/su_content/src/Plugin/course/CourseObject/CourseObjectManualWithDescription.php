<?php

namespace Drupal\su_content\Plugin\course\CourseObject;

use Drupal\Core\Form\FormStateInterface;
use Drupal;
use Drupal\course\Entity\CourseObject;
use Drupal\course_object_manual\Plugin\course\CourseObject\CourseObjectManual;

/**
 * @CourseObject(
 *   id = "manual_description",
 *   label = "Manual step with description",
 *   handlers = {
 *     "fulfillment" = "\Drupal\course\Entity\CourseObjectFulfillment"
 *   }
 * )
 */
class CourseObjectManualWithDescription extends CourseObjectManual {

  /**
   * Display status message as course content.
   */
  public function take() {
    $body = [];
    if ($this->getDescription()) {
      $body[] = ['#markup' => $this->getDescription()];
    }
    $body[] = ['#markup' => '<div class="course__manual_step__status">' . $this->getStatus() . '</div>'];
    return $body;
  }

  /**
   * Return description
   */
  public function getDescription() {
    $config = $this->getOptions();
    if ($config['step_description'] && $config['step_description']['value']) {
      return $config['step_description']['value'];
    }
    return NULL;
  }

  public function optionsDefinition() {
    $defaults = parent::optionsDefinition();

    $defaults['step_description'] = [];

    return $defaults;
  }

  public function optionsForm(&$form, FormStateInterface $form_state) {
    parent::optionsForm($form, $form_state);

    $config = $this->getOptions();

    $form['step_description'] = [
      '#type' => 'text_format',
      '#title' => t('Description for manual step'),
      '#format' => $config['step_description']['format'] ?: 'basic_html',
      '#default_value' => $config['step_description']['value'] ?: '',
    ];
  }
}
