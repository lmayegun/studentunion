<?php

namespace Drupal\su_content\Plugin\Filter;

use DOMDocument;
use DOMImplementation;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_align_full_add_container",
 *   title = @Translation("Add containers for non-alignfull items"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class AlignFullContainer extends FilterBase
{
    public function process($text, $langcode)
    {
        $dom = new DOMDocument();
	$text = mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8');
        $dom->loadHTML($text, LIBXML_NOWARNING | LIBXML_NOERROR);

        $containerDiv = $dom->createElement('div');
        $containerDiv->setAttribute('class', 'alignfull-container');

        $replacements = [];

        $nodes = $dom->getElementsByTagName('body')->item(0);
        foreach ($nodes->childNodes as $node) {
            if (!$node) {
                continue;
            }

            if (!isset($node->attributes)) {
                continue;
            }

            $class = $node->attributes->getNamedItem('class') ? $node->attributes->getNamedItem('class')->value : false;
            $noClass = !$class;
            $notAlignfull = strpos($class, 'alignfull') === FALSE;
            $notContainer = strpos($class, 'container') === FALSE;

            if ($noClass || ($notAlignfull && $notContainer)) {
                $clone = $containerDiv->cloneNode();
                $replacements[] = [$clone, $node];
            }
        }
        foreach ($replacements as $objects) {
            $clone = $objects[0];
            $node = $objects[1];
            $node->parentNode->replaceChild($clone, $node);
            $clone->appendChild($node);
        }

        //   $('#page.page-node-type-page_full .field--name-body').addClass('g-2');
        //   $('#page.page-node-type-page_full .field--name-body > *:not(.alignfull):not(.container)').wrap("<div class='container'></div>");
        $html = $dom->saveHTML();
        $trim_off_front = strlen('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">') + 1;
        $trim_off_end = (strrpos($html, '</html>')) - strlen($dom->saveHTML());
        $text = substr($html, $trim_off_front, $trim_off_end);

        return new FilterProcessResult($text);
    }
}
