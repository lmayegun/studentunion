<?php

namespace Drupal\su_content\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\typed_data\Form\SubformState;
use Drupal\typed_data\Plugin\TypedDataFormWidget\SelectWidget;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Plugin implementation of the 'options_buttons' widget.
 *
 * @FieldWidget(
 *   id = "boolean_dropdown",
 *   label = @Translation("Boolean dropdown"),
 *   field_types = {
 *     "boolean",
 *   },
 *   multiple_values = FALSE
 * )
 */
class BooleanDropdown extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => NULL,
      'description' => NULL,
      'empty_option' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $data = $items->getDataDefinition();

    $options = [];
    $options[0] = $data->getSetting('off_label') ?? 'No';
    $options[1] = $data->getSetting('on_label') ?? 'Yes';


    $element = array_merge($element, [
      '#type' => 'select',
      '#default_value' => $items[$delta]->value,
      '#multiple' => FALSE,
      '#options' => $options,
    ]);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $items->setValue($form_state->getValue($field_name)[0] == '1' ? 1 : 0);
    $items->filterEmptyItems();
    // dd([
    //   $form_state->getValue($field_name),
    //   $form_state->getValue($field_name)[0],
    //   $form_state->getValue($field_name)[0] == '1' ? 1 : 0,
    //   $items,
    // ]);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    return $elements;
  }
}
