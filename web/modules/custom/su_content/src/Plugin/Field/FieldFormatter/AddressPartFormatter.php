<?php

namespace Drupal\su_content\Plugin\Field\FieldFormatter;

use Drupal\address\AddressInterface;
use Drupal\address\Plugin\Field\FieldFormatter\AddressPlainFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'address_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "address_part_formatter",
 *   label = @Translation("Select what to include"),
 *   field_types = {
 *     "address",
 *   },
 * )
 */
class AddressPartFormatter extends AddressPlainFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'parts' => [],
    ] + parent::defaultSettings();
  }

  public static function getParts() {
    return [
      '#given_name' => t('Given name'),
      '#additional_name' => t('Additional name'),
      '#family_name' => t('Family name'),
      '#organization' => t('Organization'),
      '#address_line1' => t('Address line 1'),
      '#address_line2' => t('Address line 2'),
      '#postal_code' => t('Postal code'),
      '#sorting_code' => t('Sorting code'),
      '#administrative_area' => t('Administrative area'),
      '#locality' => t('Locality'),
      '#dependent_locality' => t('Dependent locality'),
      '#country' => t('Country'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $parts = static::getParts();

    $elements['parts'] = [
      '#type' => 'select',
      '#title' => t('Parts to include'),
      '#options' => $parts,
      '#multiple' => TRUE,
      '#default_value' => $this->getSetting('parts'),
    ];

    return $elements;
  }

  /**
   * Builds a renderable array for a single address item.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array.
   */
  protected function viewElement(AddressInterface $address, $langcode) {
    $allParts = array_keys(static::getParts());
    $toInclude = array_keys($this->getSetting('parts'));

    $element = parent::viewElement($address, $langcode);

    foreach ($allParts as $key) {
      if (in_array($key, $allParts) && !in_array($key, $toInclude)) {
        unset($element[$key]);
      }
    }

    return $element;
  }
}
