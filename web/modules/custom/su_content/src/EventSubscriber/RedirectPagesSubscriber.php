<?php

/**
 * @file
 * Contains \Drupal\su_content\EventSubscriber\RedirectPagesSubscriber
 */

namespace Drupal\su_content\EventSubscriber;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RedirectPagesSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectPage'];
    return $events;
  }

  /**
   * Redirect requests if the node has a redirect set up on it and we don't have edit access.
   *
   * @param GetResponseEvent $event
   * @return void
   */
  public function redirectPage(Event $event) {
    // \Drupal::logger('su_content')->debug('Redirect page su_content.');

    return $event;

    $request = $event->getRequest();

    if ($request->attributes->get('_route') !== 'entity.node.canonical') {
      return;
    }

    $node = \Drupal::routeMatch()->getParameter('node');
    if (!$node->hasField('field_redirect_to')) {
      return;
    }

    if (!$node->get('field_redirect_to') || !$node->get('field_redirect_to')->first()) {
      return;
    }

    $linkItem = $node->get('field_redirect_to')->first();
    if (!$linkItem->getUrl() || !$linkItem->getUrl()->toString()) {
      return;
    }

    $redirect_url = $linkItem->getUrl()->toString();
    if ($node->access('update', \Drupal::currentUser())) {
      \Drupal::messenger()->addWarning(t(
        'This page has a redirect set up on it (under "Administration" -> "Redirect to other page").<br><br>You have edit access so you are not being redirected; any visitors who do not have edit access would not see this page and instead be redirected to <a href="@url" target="_blank">@url</a>',
        ['@url' => $redirect_url]
      ));
      return;
    }

  //    $response = new TrustedRedirectResponse($redirect_url, 301);
  //  $event->setResponse($response);
  }
}
