/**
 * @file
 *
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.runCountdown = {
    attach: function (context, settings) {
      'use strict';

      $('#score_all_and_complete').on('click', function (event) {
        event.preventDefault();

        $('.quiz-report-score[value=\'\']').each(function () {
          $(this).val($(this).attr('max'));
        });
        
        $('#edit-submit').trigger('click');
      })

    }
  };
}(jQuery, Drupal));

