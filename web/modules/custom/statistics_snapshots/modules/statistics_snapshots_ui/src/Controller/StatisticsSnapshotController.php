<?php

namespace Drupal\statistics_snapshots_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface;

class StatisticsSnapshotController extends ControllerBase {

  public function render(StatisticsSnapshotInterface $statistics_snapshot) {
    $build = [];

    $build['info'] = [
      '#type' => 'table',
      '#weight' => -50,
      '#header' => [
        t('Start of period'),
        t('End of period'),
        t('Generated'),
      ],
      '#rows' => [
        [
          \Drupal::service('date.formatter')->format($statistics_snapshot->start->value),
          \Drupal::service('date.formatter')->format($statistics_snapshot->end->value),
          \Drupal::service('date.formatter')->format($statistics_snapshot->created->value),
        ],
      ],
    ];

    $metadata = [];
    $contents = [];

    // Get a previous one from at least a before this one.
    $previous = $statistics_snapshot->getPreviousSnapshot($statistics_snapshot->end->value - (24 * 60 * 60));

    $pluginManager = \Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $plugins = $pluginManager->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $statsProvider = $pluginManager->createInstance($plugin_id);

      if (isset($plugin['metadata']) && $plugin['metadata']) {
        $metadata[$plugin_id] = $statsProvider;
        continue;
      }

      $rows = [];
      $fields = $statsProvider->getFields();
      foreach ($fields as $field_name => $field) {
        $value = $statistics_snapshot->getStatValue($statsProvider, $field_name);

        $change = '-';
        if ($previous) {
          $old = $previous->getStatValue($statsProvider, $field_name);
          if (($value || $value === "0") && ($old || $old === "0")) {
            if ($value > $old) {
              $change = '🔺' . ($value - $old);
            } elseif ($value < $old) {
              $change = '🔻 ' . ($old - $value);
            } else {
              $change = '=';
            }
          }
        }

        $rows[] = [
          'Statistic' => $field->getLabel(),
          'Value' => $value || $value === "0" ? $value : '-',
          'Change' => $change,
          'Tools' => '',
        ];
      }

      $build[$plugin_id] = [
        '#prefix' => '<a name="' . $plugin_id . '"></a>',
        'table' => [
          '#type' => 'table',
          '#caption' => $plugin['label'],
          '#header' => [
            t('Statistic'),
            t('Value'),
            t('Change'),
            t('Tools'),
          ],
          '#rows' => $rows,
        ]
      ];
      $contents[$plugin_id] = $plugin['label'];
    }

    $contents_links = [];
    foreach ($contents as $id => $label) {
      $contents_links[] = '<a href="#' . $id . '">' . $label . '</a>';
    }
    $build['contents'] = [
      '#weight' => -46,
      '#markup' => '<ul><li>' . implode('</li><li>', $contents_links) . '</li></ul>',
    ];

    foreach ($metadata as $plugin_id => $statsProvider) {
      $rows = [];
      $fields = $statsProvider->getFields();
      foreach ($fields as $field_name => $field) {
        $value = $statistics_snapshot->getStatValue($statsProvider, $field_name);
        if ($value && $field->getType() == 'datetime') {
          $value = \Drupal::service('date.formatter')->format($value);
        }

        $rows[] = [
          'Metadata' => $field->getLabel(),
          'Value' => $value || $value === "0" ? $value : '-',
        ];
      }
      $build[$plugin_id] = [
        '#weight' => -48,
        '#caption' => $statsProvider->getLabel(),
        '#type' => 'table',
        '#header' => [
          t('Metadata'),
          t('Value'),
        ],
        '#rows' => $rows,
      ];
    }

    return $build;
  }
}
