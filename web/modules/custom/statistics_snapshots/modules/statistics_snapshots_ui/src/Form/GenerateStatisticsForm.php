<?php

namespace Drupal\statistics_snapshots_ui\Form;

use DateInterval;
use DatePeriod;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchForm;

/**
 * Class GenerateStatisticsForm.
 */
class GenerateStatisticsForm extends BatchForm {

  /**
   * {@inheritdoc}
   */
  public static function processOperation(array $data, array &$context) {
    statistics_snapshots_start_snapshot($data['start'], $data['end']);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'generate_statistics_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start'),
      '#weight' => '0',
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime('-2 years'))
    ];

    $form['end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End'),
      '#weight' => '0',
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime('now'))
    ];

    $form['granularity'] = [
      '#type' => 'select',
      '#title' => $this->t('Snapshot interval / length'),
      '#options' => [
        'H' => $this->t('Hourly'),
        'D' => $this->t('Daily'),
        'W' => $this->t('Weekly'),
        'M' => $this->t('Monthly'),
        'Y' => $this->t('Monthly')
      ],
      '#size' => 5,
      '#default_value' => 'D',
      '#weight' => '0',
    ];

    $form = parent::buildForm($form, $form_state);

    $form['batch_chunk_size']['#default_value'] = 1;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $interval = new DateInterval('P1' . $form_state->getValue('granularity'));
    $daterange = new DatePeriod($form_state->getValue('start')
      ->getPhpDateTime(), $interval, $form_state->getValue('end')
      ->getPhpDateTime());
    foreach ($daterange as $date) {
      $data[] = [
        'start' => $date->getTimestamp(),
        'end' => $date->add($interval)->getTimestamp() - 1
      ];
    }

    // Chunk the array
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 1;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [$chunk]);
    }
  }

}
