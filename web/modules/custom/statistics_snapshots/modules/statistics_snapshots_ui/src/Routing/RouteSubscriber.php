<?php

namespace Drupal\statistics_snapshots_ui\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {
  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.statistics_snapshot.canonical')) {
      $route->addDefaults(['_controller' => '\Drupal\statistics_snapshots_ui\Controller\StatisticsSnapshotController::render']);
    }
  }
}
