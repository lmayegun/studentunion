/**
 * @file
 *
 */

(function ($, Drupal) {
  'use strict';
  $(document).ready(function () {
    console.log('OVERVIEW');
  });

  function getSelected() {
    const checkboxes = $('.history_checkbox:checked');
    let names = [];
    checkboxes.each(function (index) {
      names.push(checkboxes[index].name);
    });
    return names;
  }

  $('#create_dashboard').on('click', function () {
    const names = getSelected();
    if (names.length > 0) {
      // post('/admin/reports/statistics/snapshots/history', names);
    }
    else {
      alert(Drupal.t("No statistics fields selected. Check the box next to at least one."));
    }
  });

  $('#view_history').on('click', function () {
    const names = getSelected();
    if (names.length > 0) {
      post('/admin/reports/statistics/snapshots/history', names);
    }
    else {
      alert(Drupal.t("No statistics fields selected. Check the box next to at least one."));
    }
  });

// Post to the provided URL with the specified parameters.
  function post(path, parameters) {
    let form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", path);

    $.each(parameters, function (key, value) {
      let field = $('<input></input>');

      field.attr("type", "hidden");
      field.attr("name", key);
      field.attr("value", value);

      form.append(field);
    });

    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);
    form.submit();
  }
}(jQuery, Drupal));
