<?php

namespace Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "node",
 *   label = "Content statistics (nodes)",
 * )
 */
class NodeStatsCalculator extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];

    // Count number of published nodes in total.
    $fields['nodes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total published nodes'))
      ->setSetting('period', 'snapshot');

    $fields['nodes_up'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total unpublished nodes'))
      ->setSetting('period', 'snapshot');

    // Count number of nodes per type.
    $types = NodeType::loadMultiple();
    foreach ($types as $type) {
      $fields['nodes_' . $type->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Nodes published of type: ' . $type->label()))
        ->setSetting('period', 'snapshot');

      $fields['nodes_up_' . $type->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Nodes unpublished of type: ' . $type->label()))
        ->setSetting('period', 'snapshot');
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    // We collect the statistics values together, then set them at the end.
    $values = [];

    // @todo deal with content moderation?

    // Count number of published nodes in total.
    $values['nodes'] = Drupal::entityQuery('node')
      ->condition('status', NodeInterface::PUBLISHED)
      ->count()
      ->execute();

    $values['nodes_up'] = Drupal::entityQuery('node')
      ->condition('status', 0)
      ->count()
      ->execute();

    // Count number of nodes per type.
    $types = NodeType::loadMultiple();
    foreach ($types as $type) {
      $values['nodes_' . $type->id()] = Drupal::entityQuery('node')
        ->condition('status', NodeInterface::PUBLISHED)
        ->condition('type', $type->id())
        ->count()
        ->execute();

      $values['nodes_up_' . $type->id()] = Drupal::entityQuery('node')
        ->condition('status', NodeInterface::NOT_PUBLISHED)
        ->condition('type', $type->id())
        ->count()
        ->execute();
    }

    return $values;
  }

}
