<?php

namespace Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;
use Drupal\user\Entity\Role;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "user",
 *   label = "User statistics",
 * )
 */
class UserStatsCalculator extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];

    // Count number of active users in total.
    $fields['users'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total active users'))
      ->setDefaultValue(0)
      ->setSetting('period', 'snapshot');

    $fields['users_active_in_last_year'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Active users during current year'))
      ->setDefaultValue(0)
      ->setSetting('period', 'year');

    // Count number of users who have logged in during period.
    $fields['users_active_in_last_month'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Active users during current month'))
      ->setDefaultValue(0)
      ->setSetting('period', 'month');

    // Count number of users per role.
    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      if (in_array($role->id(), $this->excludedRoles())) {
        continue;
      }

      $fields['role' . $role->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Users with role: ' . $role->label()))
        ->setDefaultValue(0)
        ->setSetting('period', 'snapshot');
    }

    return $fields;
  }

  /**
   * Roles we don't generate stats for.
   *
   * @return array
   *   Role IDs to exclude.
   */
  public function excludedRoles(): array {
    return ['anonymous', 'authenticated'];
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    // We collect the statistics values together, then set them at the end.
    $values = [];

    // Count number of active users in total.
    $values['users'] = Drupal::entityQuery('user')
      ->condition('status', 1)
      ->count()
      ->execute();

    // Count number of users who have logged in, in last year.
    $startOfYear = $this->calculateStartFromPeriod('users_active_in_last_year', $start, $end, 'year');
    $endOfYear = $this->calculateEndFromPeriod('users_active_in_last_year', $start, $end, 'year');
    $values['users_active_in_last_year'] = Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('access', $startOfYear, '>=')
      ->condition('access', $endOfYear, '<=')
      ->count()
      ->execute();

    // Count number of users who have logged in, in last month.
    $startOfMonth = $this->calculateStartFromPeriod('users_active_in_last_month', $start, $end, 'month');
    $endOfMonth = $this->calculateEndFromPeriod('users_active_in_last_month', $start, $end, 'month');
    $values['users_active_in_last_month'] = Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('access', $startOfMonth, '>=')
      ->condition('access', $endOfMonth, '<=')
      ->count()
      ->execute();

    // Count number of users per role.
    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      if (in_array($role->id(), $this->excludedRoles())) {
        continue;
      }

      $values['role' . $role->id()] = Drupal::entityQuery('user')
        ->condition('status', 1)
        ->condition('roles', $role->id())
        ->count()
        ->execute();
    }

    return $values;
  }

}
