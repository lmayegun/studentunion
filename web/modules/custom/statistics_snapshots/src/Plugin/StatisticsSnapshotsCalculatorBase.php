<?php

namespace Drupal\statistics_snapshots\Plugin;

use DateTime;
use Drupal;
use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for SU Statistics provider plugins.
 */
abstract class StatisticsSnapshotsCalculatorBase extends PluginBase implements StatisticsSnapshotsCalculatorInterface {

  public const SPLIT_CHUNK_SIZE = 1000;

  public const SPLITS_INTO_QUEUES = FALSE;

  /**
   * {@inheritdoc}
   */
  public function runCalculation(array $segment_data = NULL, int $start = NULL, int $end = NULL) {
    Drupal::logger('statistics_snapshots')
      ->debug('Running calculation ' . $this->getPluginId());

    if (!$start) {
      $start = strtotime('now');
    }

    if (!$end) {
      $end = $start;
    }

    // Plugins may want to override dates at the point of calculation and setting values.
    // Note this can be done per-field using "moment_in_time" setting.
    $start = $this->setStart($segment_data, $start, $end);
    $end = $this->setEnd($segment_data, $start, $end);

    Drupal::logger('statistics_snapshots')
      ->debug('Run calculation @plugin @segment', [
        '@plugin' => $this->getPluginId(),
        '@segment' => $segment_data ? ' with segment data (' . count($segment_data) . ' records)' : '',
      ]);

    $values = $this->calculate($segment_data, $start, $end);

    foreach ($this->getFields() as $fieldName => $fieldDefinition) {
      if (!isset($values[$fieldName]) && $fieldDefinition->getDefaultValueLiteral()) {
        $defaultValue = $fieldDefinition->getDefaultValueLiteral();
        if (is_int($defaultValue) || is_float($defaultValue)) {
          $values[$fieldName] = $defaultValue;
        }
      }
    }

    if ($values) {
      $this->setValues($segment_data, $start, $end, $values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setStart(array $segment_data = NULL, int $start = NULL, int $end = NULL): ?int {
    return $start;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnd(array $segment_data = NULL, int $start = NULL, int $end = NULL): ?int {
    return $end;
  }

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    // We collect the statistics values together, then set them at the end.
    // We add to the values by e.g. running queries.

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    // Define Base Field Definitions, e.g.

    // $fields['users'] = BaseFieldDefinition::create('integer')
    // ->setLabel(t('Total active users'));

    return [];
  }

  function setValues($segment_data = NULL, $start, $end, $values = []) {
    $repository = Drupal::service('statistics_snapshots.repository');

    // Set values on snapshot entity.
    foreach ($values as $fieldName => $value) {
      if ($fieldName == 'statistics_snapshots_increment') {
        continue;
      }

      if (isset($values['statistics_snapshots_increment']) && $values['statistics_snapshots_increment']) {
        $repository->incrementStatValue($this, $fieldName, $start, $end, $value);
      }
      else {
        $repository->setStatValue($this, $fieldName, $start, $end, $value);
      }
    }
  }

  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function splitIntoQueues(array $dataset, int $start, int $end) {
    $service = Drupal::service('statistics_snapshots.service');
    $segments = array_chunk($dataset, static::SPLIT_CHUNK_SIZE);
    foreach ($segments as $segment) {
      $service->addPluginToQueue($this->getPluginId(), $segment, $start, $end);
    }
  }

  /**
   * @param string $unique_id
   * @param int|NULL $start
   * @param int|NULL $end
   * @param string $period
   *
   * @return int|null
   * @throws \Exception
   */
  public function calculateStartFromPeriod(string $unique_id, int $start = NULL, int $end = NULL, string $period = 'snapshot'): ?int {
    if (!$start) {
      return $start;
    }

    $startDateTime = new DateTime('@' . $start);
    switch ($period) {
      case 'month':
        return $startDateTime->modify('00:00:00 first day of this month')
          ->getTimestamp();
        break;

      case 'year':
        return $startDateTime->modify('00:00:00 first day of January')
          ->getTimestamp();
        break;

      case 'snapshot':
      default:
        return $start;
    }
  }

  /**
   * @param string $unique_id
   * @param int|NULL $start
   * @param int|NULL $end
   * @param string $period
   *
   * @return int|null
   * @throws \Exception
   */
  public function calculateEndFromPeriod(string $unique_id, int $start = NULL, int $end = NULL, string $period = 'snapshot') {
    if (!$end) {
      return $end;
    }

    $endDateTime = new DateTime('@' . $end);
    switch ($period) {
      case 'month':
        return $endDateTime->modify('23:59:59 last day of this month')
          ->getTimestamp();
        break;

      case 'year':
        return $endDateTime->modify('23:59:59 last day of December')
          ->getTimestamp();
        break;

      // Make end the same as start if it's a snapshot / moment in time:
      case 'snapshot':
      default:
        return $start;
    }
  }

  /**
   * @return array
   */
  public function getDashboardElements(): array {
    $elements = [
      'list' => [],
      'table' => [],
      'pieChart' => [],
      'barChart' => [],
    ];

    $fields = $this->getFields();
    foreach ($fields as $field_id => $field) {
      foreach ($elements as $element_type => $types) {
        if ($element_id = $field->getSetting($element_type)) {
          if (!isset($charts[$element_type][$element_id])) {
            $elements[$element_type][$element_id] = [];
          }

          $elements[$element_type][$element_id][] = $field_id;
        }
      }
    }

    return $elements;
  }

}
