<?php

namespace Drupal\statistics_snapshots\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for SU Statistics provider plugins.
 */
interface StatisticsSnapshotsCalculatorInterface extends PluginInspectionInterface {

  /**
   * Define Base Field Definitions for statistics.
   *
   * e.g.
   *
   * $fields['users'] = BaseFieldDefinition::create('integer')
   *  ->setLabel(t('Total active users'));
   *
   * @return array
   *   Array of base field definitions.
   */
  public function getFields();

  /**
   * Calculate the field values and add them to the snapshot.
   *
   * This is a private function - all calculations should be initiated via
   * runCalculation()
   *
   * @param array $segment_data
   *   Data to process if we are doing a split/segmented calculation.
   *   See splitIntoQueues.
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array;

  /**
   * Split the processing of a calculation into multiple queue items.
   *
   * By chunking the dataset. A bit like the Batch API.
   *
   * @param array $dataset
   */
  public function splitIntoQueues(array $dataset, int $start, int $end);

  /**
   * Initiate the calculation.
   *
   * Always do this instead of calculate to ensure fields etc.
   *
   * @param array|null $segment_data
   *   Data to process if we are doing a split/segmented calculation.
   *   See splitIntoQueues.
   * @param null $start
   * @param null $end
   */
  public function runCalculation(array $segment_data = NULL, int $start = NULL, int $end = NULL);

  /**
   * @param array|NULL $segment_data
   * @param int|NULL $start
   * @param int|NULL $end
   *
   * @return int|null
   *   Timestamp.
   */
  public function setStart(array $segment_data = NULL, int $start = NULL, int $end = NULL): ?int;

  /**
   * @param array|NULL $segment_data
   * @param int|NULL $start
   * @param int|NULL $end
   *
   * @return int|null
   *   Timestamp.
   */
  public function setEnd(array $segment_data = NULL, int $start = NULL, int $end = NULL): ?int;

}
