<?php

namespace Drupal\statistics_snapshots\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "statistics_snapshots_queue",
 *   title = @Translation("Statistics Snapshots Queue"),
 *   cron = {"time" = 240}
 * )
 */
class StatisticsRunPluginQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * CommerceStockNotifyQueue constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!isset($data['plugin_id']) || !$data['plugin_id']) {
      return;
    }
    if (isset($data['finish']) && $data['finish']) {
      return;
    }
    statistics_snapshots_process_item($data['plugin_id'], $data['segment_data'] ?? NULL, $data['start'], $data['end']);
  }

}
