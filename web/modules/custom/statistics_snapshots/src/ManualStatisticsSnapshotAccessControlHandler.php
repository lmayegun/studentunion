<?php

namespace Drupal\statistics_snapshots;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the manual statistics snapshot entity
 * type.
 */
class ManualStatisticsSnapshotAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view manual statistics snapshot');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, [
          'edit manual statistics snapshot',
          'administer manual statistics snapshot',
        ], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, [
          'delete manual statistics snapshot',
          'administer manual statistics snapshot',
        ], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'create manual statistics snapshot',
      'administer manual statistics snapshot',
    ], 'OR');
  }

}
