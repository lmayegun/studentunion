<?php

namespace Drupal\statistics_snapshots;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for a manual statistics snapshot entity type.
 */
class ManualStatisticsSnapshotViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    // The manual statistics snapshot has no entity template itself.
    unset($build['#theme']);
    return $build;
  }

}
