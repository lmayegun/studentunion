<?php

namespace Drupal\statistics_snapshots\Service;

use Drupal;

/**
 * Implements Class StatisticsSnapshotService Controller.
 */
class StatisticsSnapshotService {

  /**
   * Constructs a new StatisticsSnapshotService object.
   */
  public function __construct() {
  }

  /**
   * Initiate stats calculation - called by queue.
   *
   * @param string $plugin_id
   *   Plugin ID.
   * @param array|null $segment_data
   */
  public function calculateStatsForPlugin(string $plugin_id, array $segment_data = NULL, int $start = NULL, int $end = NULL) {
    $pluginManager = Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $statsProvider = $pluginManager->createInstance($plugin_id);
    $statsProvider->runCalculation($segment_data, $start, $end);
  }

  /**
   * Add calculations to queues to run.
   */
  public function calculateStats($start, $end) {
    $pluginManager = Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $plugins = $pluginManager->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $this->addPluginToQueue($plugin_id, NULL, $start, $end);
    }
    $this->addFinishToQueue();
  }

  /**
   * Add a plugin to queue to run the calculation.
   *
   * @param string $plugin_id
   *   Plugin ID.
   */
  public function addPluginToQueue(string $plugin_id, $segment_data = NULL, int $start = NULL, int $end = NULL) {
    Drupal::logger('statistics_snapshots')
      ->debug('Adding plugin to queue @plugin', ['@plugin' => $plugin_id]);
    $type = statistics_snapshots_get_processing_type();
    if ($type == 'queue') {
      $queue = Drupal::queue('statistics_snapshots_queue');
      $queue->createItem([
        'plugin_id' => $plugin_id,
        'segment_data' => $segment_data,
        'start' => $start,
        'end' => $end,
      ]);
    }
    else {
      background_process_start('statistics_snapshots_process_item', $plugin_id, $segment_data, $start, $end);
    }
  }

  /**
   * Add a queue item to wrap up after all statistics calculations.
   */
  public function addFinishToQueue() {
    $type = statistics_snapshots_get_processing_type();
    if ($type == 'queue') {
      $queue = Drupal::queue('statistics_snapshots_queue');
      $queue->createItem([
        'finish' => TRUE,
      ]);
    }
    else {
      background_process_start('statistics_snapshots_finish_queue');
    }
  }

  /**
   * Convert snapshot's period to a nice date range.
   *
   * @param string $format
   *   Date format ID.
   * @param string $separator
   *   How to separate the two dates.
   *
   * @return string
   *   Formatted date range.
   */
  public function getFormattedDateRange(int $start, int $end, string $format = 'medium', string $separator = ' - '): string {
    $dateFormatter = Drupal::service('date.formatter');

    $start = $dateFormatter->format($start, $format, $format);
    $end = $dateFormatter->format($end, $format, $format);
    return $start . $separator . $end;
  }

}
