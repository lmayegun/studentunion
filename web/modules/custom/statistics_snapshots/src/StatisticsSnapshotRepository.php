<?php

namespace Drupal\statistics_snapshots;

use Drupal;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface;
use Exception;

/**
 * Repository for database-related helper methods .
 *
 * This repository is a service named 'dbtng_example.repository'. You can see
 * how the service is defined in dbtng_example/dbtng_example.services.yml.
 *
 * For projects where there are many specialized queries, it can be useful to
 * group them into 'repositories' of queries. We can also architect this
 * repository to be a service, so that it gathers the database connections it
 * needs. This way other classes which use the repository don't need to concern
 * themselves with database connections, only with business logic.
 *
 * This repository demonstrates basic CRUD behaviors, and also has an advanced
 * query which performs a join with the user table.
 *
 * @ingroup dbtng_example
 */
class StatisticsSnapshotRepository {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The translation service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Connection $connection, TranslationInterface $translation, MessengerInterface $messenger) {
    $this->connection = $connection;
    $this->setStringTranslation($translation);
    $this->setMessenger($messenger);
  }

  /**
   * Save an entry in the database.
   *
   * Exception handling is shown in this example. It could be simplified
   * without the try/catch blocks, but since an insert will throw an exception
   * and terminate your application if the exception is not handled, it is best
   * to employ try/catch.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert(array $entry): ?int {
    try {
      $return_value = $this->connection->insert('statistics_snapshot')
        ->fields($entry)
        ->execute();
    } catch (Exception $e) {
      $this->messenger()
        ->addMessage($this->t('Insert failed. Message = %message', [
          '%message' => $e->getMessage(),
        ]), 'error');
    }
    return $return_value ?? NULL;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public function update(array $entry): ?int {
    try {
      // Connection->update()...->execute() returns the number of rows updated.
      $count = $this->connection->update('statistics_snapshot')
        ->fields($entry)
        ->condition('id', $entry['id'])
        ->execute();
    } catch (Exception $e) {
      $this->messenger()
        ->addMessage($this->t('Update failed. Message = %message, query= %query', [
            '%message' => $e->getMessage(),
            '%query' => $e->query_string,
          ]
        ), 'error');
    }
    return $count ?? 0;
  }

  /**
   * Delete an entry from the database.
   *
   * @param array $entry
   *   An array containing at least the person identifier 'pid' element of the
   *   entry to delete.
   *
   * @see Drupal\Core\Database\Connection::delete()
   */
  public function delete(array $entry) {
    $this->connection->delete('statistics_snapshot')
      ->condition('id', $entry['id'])
      ->execute();
  }

  /**
   * Read from the database using a filter array.
   *
   * The standard function to perform reads for static queries is
   * Connection::query().
   *
   * Connection::query() uses an SQL query with placeholders and arguments as
   * parameters.
   *
   * Drupal DBTNG provides an abstracted interface that will work with a wide
   * variety of database engines.
   *
   * The following is a query which uses a string literal SQL query. The
   * placeholders will be substituted with the values in the array.
   * Placeholders
   * are marked with a colon ':'. Table names are marked with braces, so that
   * Drupal's' multisite feature can add prefixes as needed.
   *
   * @code
   *   // SELECT * FROM {dbtng_example} WHERE uid = 0 AND name = 'John'
   *   \Drupal::database()->query(
   *     "SELECT * FROM {dbtng_example} WHERE uid = :uid and name = :name",
   *     [':uid' => 0, ':name' => 'John']
   *   )->execute();
   * @endcode
   *
   * For more dynamic queries, Drupal provides Connection::select() API method,
   * so there are several ways to perform the same SQL query. See the
   * @link http://drupal.org/node/310075 handbook page on dynamic queries.
   * @endlink
   * @code
   *   // SELECT * FROM {dbtng_example} WHERE uid = 0 AND name = 'John'
   *   \Drupal::database()->select('dbtng_example')
   *     ->fields('dbtng_example')
   *     ->condition('uid', 0)
   *     ->condition('name', 'John')
   *     ->execute();
   * @endcode
   *
   * Here is select() with named placeholders:
   * @code
   *   // SELECT * FROM {dbtng_example} WHERE uid = 0 AND name = 'John'
   *   $arguments = array(':name' => 'John', ':uid' => 0);
   *   \Drupal::database()->select('dbtng_example')
   *     ->fields('dbtng_example')
   *     ->where('uid = :uid AND name = :name', $arguments)
   *     ->execute();
   * @endcode
   *
   * Conditions are stacked and evaluated as AND and OR depending on the type
   *   of
   * query. For more information, read the conditional queries handbook page
   *   at:
   * http://drupal.org/node/310086
   *
   * The condition argument is an 'equal' evaluation by default, but this can
   *   be
   * altered:
   * @code
   *   // SELECT * FROM {dbtng_example} WHERE age > 18
   *   \Drupal::database()->select('dbtng_example')
   *     ->fields('dbtng_example')
   *     ->condition('age', 18, '>')
   *     ->execute();
   * @endcode
   *
   * @param array $entry
   *   An array containing all the fields used to search the entries in the
   *   table.
   *
   * @return object
   *   An object containing the loaded entries if found.
   *
   * @see Drupal\Core\Database\Connection::select()
   */
  public function load(array $entry = []): object {
    // Read all the fields from the statistics_snapshot table.
    $select = $this->connection
      ->select('statistics_snapshot')
      // Add all the fields into our select query.
      ->fields('statistics_snapshot');

    // Add each field and value as a condition to this query.
    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    // Return the result in object format.
    return $select->execute()->fetchAll();
  }

  /**
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   * @param string $unique_id
   * @param int|NULL $start
   * @param int|NULL $end
   *
   * @return float
   */
  public function getRefreshedStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $unique_id, int $start = NULL, int $end = NULL) {
    return $this->getStatValue($plugin, $unique_id, $start, $end, TRUE);
  }

  /**
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   * @param string
   * $unique_id
   * @param int|NULL $start
   * @param int|NULL $end
   * @param bool $refreshIfPossible
   *
   * @return float
   */
  public function getStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $unique_id, int $start = NULL, int $end = NULL, bool $refreshIfPossible = FALSE, $returnAllColumns = FALSE) {
    $query = Drupal::database()->select('statistics_snapshot', 'ss');
    $query->fields('ss');
    $query->condition('ss.plugin_id', $plugin->getPluginId());
    $query->condition('ss.unique_id', $unique_id);

    if ($start) {
      $query->condition('ss.time_start', $start);
    }

    if ($end) {
      $query->condition('ss.time_end', $end);
    }

    // If we want to refresh, we load the value only if it expires after now.
    // This means we only get a value if it's still within the minimum age.
    // If we don't get a value, we know to recalculate it below.
    if ($refreshIfPossible) {
      $expiryOfADay = 86400;
      $query->having('(ss.generated + IFNULL(ss.minimum_age, ' . $expiryOfADay . ')) > :now', [':now' => strtotime('now')]);
    }

    $query->range(0, 1);
    $query->orderBy("generated", 'DESC');
    if ($returnAllColumns) {
      $value = $query->execute()->fetchAssoc();
    }
    else {
      $value = isset($query->execute()->fetchCol()[0]) ? $query->execute()
        ->fetchCol()[0] : NULL;
    }

    if (!$value && $refreshIfPossible) {
      $service = Drupal::service('statistics_snapshots.service');
      $service->calculateStatsForPlugin($plugin->getPluginId(), NULL, $start, $end);
      return $this->getStatValue($plugin, $unique_id, $start, $end);
    }

    return $value;
  }

  /**
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   * @param string $unique_id
   * @param int|NULL $start
   * @param int|NULL $end
   *
   * @return float|null
   */
  public function getStatValueAllColumns(StatisticsSnapshotsCalculatorInterface $plugin, string $unique_id, int $start = NULL, int $end = NULL) {
    return $this->getStatValue($plugin, $unique_id, $start, $end, FALSE, TRUE);
  }

  /**
   * @param string $plugin_id
   * @param string $unique_id
   * @param int|null $start
   * @param int|null $end
   * @param float|null $value
   * @param string $narration
   */
  public function setManualStatValue(string $plugin_id, string $unique_id, int $start = NULL, int $end = NULL, float $value = NULL, string $narration = '') {
    $keys = $this->getKeys($plugin_id, $unique_id, $start, $end);

    $this->connection->merge('statistics_snapshot')
      ->key($keys)
      ->fields($keys + [
          'stat_value' => $value,
          'narration' => $narration,
          'manual' => 1,
          'uid' => Drupal::currentUser()->id(),
          'generated' => Drupal::time()->getCurrentTime(),
        ])
      ->execute();
  }

  /**
   * @param $plugin
   * @param string $unique_id
   * @param $start
   * @param $end
   *
   * @return array
   */
  public function getKeys($plugin, string $unique_id, $start, $end): array {
    if ($plugin && !is_string($plugin)) {
      $fields = $plugin->getFields();
      $field = $fields[$unique_id] ?? NULL;
      if ($field && $field->getSetting('period')) {
        $period = $fields[$unique_id]->getSetting('period');
        $start = $plugin->calculateStartFromPeriod($unique_id, $start, $end, $period);
        $end = $plugin->calculateEndFromPeriod($unique_id, $start, $end, $period);
      }
    }

    return [
      'plugin_id' => is_string($plugin) ? $plugin : $plugin->getPluginId(),
      'unique_id' => $unique_id,
      'time_start' => $start,
      'time_end' => $end,
    ];
  }

  /**
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   * @param string $unique_id
   * @param int|null $start
   * @param int|null $end
   * @param float|null $value
   */
  public function setStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $unique_id, int $start = NULL, int $end = NULL, float $value = NULL) {
    $keys = $this->getKeys($plugin, $unique_id, $start, $end);

    $this->connection->merge('statistics_snapshot')
      ->key($keys)
      ->fields($keys + [
          'stat_value' => $value,
          'uid' => Drupal::currentUser()->id(),
          'generated' => Drupal::time()->getCurrentTime(),
        ])
      ->execute();
  }

  /**
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   * @param string $unique_id
   * @param $start
   * @param $end
   * @param $increment_value
   */
  public function incrementStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $unique_id, $start, $end, $increment_value) {
    $keys = $this->getKeys($plugin, $unique_id, $start, $end);

    $this->connection->merge('statistics_snapshot')
      ->key($keys)
      ->expression('stat_value', 'stat_value + :inc', [':inc' => $increment_value])
      ->execute();
  }

}
