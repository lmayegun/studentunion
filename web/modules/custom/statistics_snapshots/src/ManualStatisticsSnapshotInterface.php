<?php

namespace Drupal\statistics_snapshots;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a manual statistics snapshot entity type.
 */
interface ManualStatisticsSnapshotInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the manual statistics snapshot title.
   *
   * @return string
   *   Title of the manual statistics snapshot.
   */
  public function getTitle();

  /**
   * Sets the manual statistics snapshot title.
   *
   * @param string $title
   *   The manual statistics snapshot title.
   *
   * @return \Drupal\statistics_snapshots\ManualStatisticsSnapshotInterface
   *   The called manual statistics snapshot entity.
   */
  public function setTitle($title);

  /**
   * Gets the manual statistics snapshot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the manual statistics snapshot.
   */
  public function getCreatedTime();

  /**
   * Sets the manual statistics snapshot creation timestamp.
   *
   * @param int $timestamp
   *   The manual statistics snapshot creation timestamp.
   *
   * @return \Drupal\statistics_snapshots\ManualStatisticsSnapshotInterface
   *   The called manual statistics snapshot entity.
   */
  public function setCreatedTime($timestamp);

}
