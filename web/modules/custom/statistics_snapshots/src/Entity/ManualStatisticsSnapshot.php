<?php

namespace Drupal\statistics_snapshots\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\statistics_snapshots\ManualStatisticsSnapshotInterface;
use Drupal\user\UserInterface;

/**
 * Defines the manual statistics snapshot entity class.
 *
 * @ContentEntityType(
 *   id = "manual_statistics_snapshot",
 *   label = @Translation("Manual statistics snapshot"),
 *   label_collection = @Translation("Manual statistics snapshots"),
 *   handlers = {
 *     "view_builder" =
 *   "Drupal\statistics_snapshots\ManualStatisticsSnapshotViewBuilder",
 *     "list_builder" =
 *   "Drupal\statistics_snapshots\ManualStatisticsSnapshotListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" =
 *   "Drupal\statistics_snapshots\ManualStatisticsSnapshotAccessControlHandler",
 *     "form" = {
 *       "add" =
 *   "Drupal\statistics_snapshots\Form\ManualStatisticsSnapshotForm",
 *       "edit" =
 *   "Drupal\statistics_snapshots\Form\ManualStatisticsSnapshotForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "manual_statistics_snapshot",
 *   admin_permission = "administer manual statistics snapshot",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/reports/statistics/snapshots/manual/add",
 *     "canonical" =
 *   "/manual_statistics_snapshot/{manual_statistics_snapshot}",
 *     "edit-form" =
 *   "/admin/reports/statistics/snapshots/manual/{manual_statistics_snapshot}/edit",
 *     "delete-form" =
 *   "/admin/reports/statistics/snapshots/manual/{manual_statistics_snapshot}/delete",
 *     "collection" = "/admin/reports/statistics/snapshots/manual"
 *   },
 *   field_ui_base_route = "entity.manual_statistics_snapshot.settings"
 * )
 */
class ManualStatisticsSnapshot extends ContentEntityBase implements ManualStatisticsSnapshotInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new manual statistics snapshot entity is created, set the uid
   * entity reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the manual statistics snapshot entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the statistic.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['calculation_explanation'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('How to calculate'))
      ->setDescription(t('Explain how to calculate the statistic for others to follow.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);#

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the manual statistics snapshot author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the manual statistics snapshot was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the manual statistics snapshot was last edited.'));

    $fields['period'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Time period'))
      ->setSettings([
        'allowed_values' => [
          'snapshot' => 'Snapshot (moment in time)',
          'day' => 'Day',
          //          'week' => 'Week',
          'month' => 'Month',
          'quarter_calendar' => 'Quarter (calendar year)',
          //          'quarter_custom' => 'Quarter (custom year)',
          'year' => 'Calendar year',
          //          'year_custom' => 'Custom year',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('inherit')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entry_users'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User(s) who can enter statistics'))
      ->setSetting('target_type', 'user')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['entry_roles'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Website user roles who can enter statistics'))
      ->setSetting('target_type', 'user_role')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['plugin_id'] = BaseFieldDefinition::create('plugin_reference')
      ->setLabel(t('Category for plugin'))->setLabel(t('Category'))
      ->setSettings([
        'target_type' => 'statistics_snapshots_calculator',
      ])
      ->setDisplayOptions('form', [
        'type' => 'plugin_reference_select',
        'configuration_form' => 'full',
        'provider_grouping' => FALSE,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['category_if_other'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Other category name if not in list above'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['grouping'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Grouping within category (if relevant)'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['cumulative'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Cumulative?'))
      ->setSetting('on_label', t('Yes'))
      ->setSetting('off_label', t('No'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 16,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['proportion_of_group'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Display as proportion of group stat is in?'))
      ->setSetting('on_label', t('Yes'))
      ->setSetting('off_label', t('No'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 16,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['proportion_of'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Display as proportion of other statistic (enter statistic ID)'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPeriod() {
    return $this->get('period')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    $plugins = $this->get('plugin_id') ? $this->get('plugin_id')
      ->referencedPlugins() : NULL;
    if ($plugins && count($plugins) > 0) {
      return $plugins[0]->getPluginId();
    }
    return 'manual';
  }

}
