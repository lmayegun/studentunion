<?php

namespace Drupal\statistics_snapshots\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an entity.
 */
interface StatisticsSnapshotDashboardInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

