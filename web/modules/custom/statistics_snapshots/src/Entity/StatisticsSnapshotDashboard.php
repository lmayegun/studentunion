<?php

namespace Drupal\statistics_snapshots\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Example entity.
 *
 * @ConfigEntityType(
 *   id = "statistics_snapshots_dashboard",
 *   label = @Translation("Statistics Snapshots Dashboard"),
 *   handlers = {
 *     "list_builder" = "Drupal\example\Controller\ExampleListBuilder",
 *     "form" = {
 *       "add" = "Drupal\example\Form\ExampleForm",
 *       "edit" = "Drupal\example\Form\ExampleForm",
 *       "delete" = "Drupal\example\Form\ExampleDeleteForm",
 *     }
 *   },
 *   config_prefix = "statistics_snapshots",
 *   admin_permission = "administer statistics snapshots dashboards",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "dashboard_elements",
 *   },
 *   links = {
 *     "edit-form" =
 *   "/admin/statistics/dashboards/{statistics_snapshots_dashboard}/edit",
 *     "delete-form" =
 *   "/admin/statistics/dashboards/{statistics_snapshots_dashboard}/delete",
 *   }
 * )
 */
class StatisticsSnapshotDashboard extends ConfigEntityBase implements StatisticsSnapshotDashboardInterface {

  /**
   * The ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The elements in order.
   *
   * @var array
   */
  protected array $dashboard_elements;

}

