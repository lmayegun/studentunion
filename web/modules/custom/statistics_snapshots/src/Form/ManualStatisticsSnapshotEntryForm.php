<?php

namespace Drupal\statistics_snapshots\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\statistics_snapshots\Controller\StatisticsOverviewController;
use Drupal\statistics_snapshots\Entity\ManualStatisticsSnapshot;

/**
 * Class SettingsForm.
 */
class ManualStatisticsSnapshotEntryForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'manual_statistics_snapshot_entry_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $manual_statistics_snapshot = NULL): array {
    if ($manual_statistics_snapshot) {
      $snapshots = [$manual_statistics_snapshot];
    }
    else {
      $snapshots = $this->loadSnapshotsForUser();
    }

    foreach ($snapshots as $snapshot_id => $snapshot) {
      $form[$snapshot_id] = $this->createEntryFormForManualSnapshot($snapshot);
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  private function loadSnapshotsForUser(): array {
    $user = Drupal::currentUser();

    $query = Drupal::entityQuery('manual_statistics_snapshot');
    $roleOrUser = $query->orConditionGroup()
      ->condition('entry_users.entity:user.uid', $user->id())
      ->condition('entry_roles', $user->getRoles(), 'IN');
    $query->condition($roleOrUser);
    $snapshot_ids = $query->execute();
    $snapshots = Drupal\statistics_snapshots\Entity\ManualStatisticsSnapshot::loadMultiple($snapshot_ids);

    if (!$snapshots) {
      // @todo load all if they have permission.
      return [];
    }

    return $snapshots;
  }

  public function createEntryFormForManualSnapshot($manualSnapshot): array {
    if (!$manualSnapshot) {
      return [];
    }

    $entryRows = [];

    if ($manualSnapshot->getPeriod() == 'snapshot') {
      $start = strtotime('now');
      $end = strtotime('now');
      $entryRows[] = $this->getEntryRow($manualSnapshot, $start, $end);
    }
    else {
      $startsAndEnds = [];

      switch ($manualSnapshot->getPeriod()) {
        case 'day':
          $startOfPeriods = strtotime('midnight -31 days');
          $periodLength = '1 day';
          break;
        case 'month':
          $startOfPeriods = strtotime('midnight first day of -12 months');
          $periodLength = '1 month';
          break;
        case 'quarter_calendar':
          $startOfPeriods = strtotime('midnight first day of -3 years');
          $periodLength = '3 months';
          break;
        case 'year':
          $startOfPeriods = strtotime('midnight first day of -10 years');
          $periodLength = '1 year';
          break;

        // @todo:
        //        case 'week':
        //          $startOfPeriods = strtotime('midnight first day of -12 weeks');
        //          $periodLength = '1 week';
        //          break;
        //  'quarter_calendar' => 'Quarter (calendar year)',
        //  'quarter_custom' => 'Quarter (custom year)',
        //  'year_custom' => 'Custom year',

        default:
          $startOfPeriods = strtotime('midnight first day of -12 weeks');
          $periodLength = '1 week';
      }


      $pointerTime = $startOfPeriods;
      while ($pointerTime <= strtotime('now')) {
        $nextStart = strtotime('+' . $periodLength, $pointerTime);
        $startsAndEnds[$pointerTime] = $nextStart - 1;
        $pointerTime = $nextStart;
      }
      foreach ($startsAndEnds as $start => $end) {
        $entryRows[$start . ':' . $end . ':' . $manualSnapshot->id()] = $this->getEntryRow($manualSnapshot, $start, $end);
      }
    }

    $entryTable = [
        '#type' => 'table',
        '#header' => [
          t('Period'),
          t('Value'),
          t('Narration'),
        ],
      ] + $entryRows;

    return [
      '#type' => 'details',
      '#title' => $manualSnapshot->label(),
      '#id' => $manualSnapshot->id(),
      'snapshot:' . $manualSnapshot->id() => $entryTable,
    ];
  }

  private function getEntryRow($manualSnapshot, int $start, int $end): array {
    $period = $manualSnapshot->getPeriod();
    if ($period == 'snapshot') {
      $label = t('As of now');
    }
    else {
      $label = StatisticsOverviewController::formatDatesForPeriod($period, $start, $end);
    }

    return [
      'label' => [
        '#markup' => $label,
      ],
      'value' => [
        '#type' => 'textfield',
        '#title' => t('Value'),
        '#title_display' => 'invisible',
      ],
      'narration' => [
        '#type' => 'textfield',
        '#title' => t('Narration'),
        '#title_display' => 'invisible',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $repository = Drupal::service('statistics_snapshots.repository');

    foreach ($form_state->getValues() as $elementId => $periods) {
      if (!strstr($elementId, 'snapshot:')) {
        continue;
      }
      $snapshotId = explode(':', $elementId)[1];
      $snapshot = ManualStatisticsSnapshot::load($snapshotId);
      foreach ($periods as $startEnd => $formValues) {
        if ($formValues['value'] == '') {
          continue;
        }

        $timestampsAndId = explode(':', $startEnd);
        $start = $timestampsAndId[0];
        $end = $timestampsAndId[1];
        $unique_id = $timestampsAndId[2];

        $value = $formValues['value'];
        $narration = $formValues['narration'];

        $repository->setManualStatValue($snapshot->getPluginId(), $unique_id, $start, $end, $value, $narration);
      }
    }
  }

}
