<?php

namespace Drupal\statistics_snapshots\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the manual statistics snapshot entity edit forms.
 */
class ManualStatisticsSnapshotForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New manual statistics snapshot %label has been created.', $message_arguments));
      $this->logger('statistics_snapshots_modules')
        ->notice('Created new manual statistics snapshot %label', $logger_arguments);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('The manual statistics snapshot %label has been updated.', $message_arguments));
      $this->logger('statistics_snapshots_modules')
        ->notice('Updated new manual statistics snapshot %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.manual_statistics_snapshot.canonical', ['manual_statistics_snapshot' => $entity->id()]);
  }

}
