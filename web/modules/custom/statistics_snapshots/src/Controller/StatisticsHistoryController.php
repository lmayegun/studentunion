<?php

namespace Drupal\statistics_snapshots\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Controller for statistics snapshots.
 */
class StatisticsHistoryController extends ControllerBase implements ContainerInjectionInterface {

  public function render() {
    $combined_field_ids = $_POST;
    $field_ids = [];
    foreach ($combined_field_ids as $combined_field_id) {
      $split = explode(':', $combined_field_id, 2);
      $plugin_id = $split[0];
      $field_id = $split[1];
      if (!isset($plugin_ids[$plugin_id])) {
        $plugin_ids[$plugin_id] = [];
      }
      $plugin_ids[$plugin_id][] = $field_id;
    }

    return [];
  }

}
