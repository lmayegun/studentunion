<?php

namespace Drupal\statistics_snapshots\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use const Date;

/**
 * Controller for statistics snapshots.
 */
class StatisticsOverviewController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @return array
   */
  public function renderOverview(): array {
    $render = [];

    $render['#attached']['library'][] = 'statistics_snapshots/overview';

    $repository = Drupal::service('statistics_snapshots.repository');
    $pluginManager = Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $plugins = $pluginManager->getDefinitions();

    foreach ($plugins as $plugin_id => $plugin) {
      $plugin = $pluginManager->createInstance($plugin_id);
      $render += static::renderPlugin($plugin);
    }

    $render[] = [
      '#type' => 'button',
      '#value' => t('View history for selected'),
      '#id' => 'view_history',
    ];

    $render[] = [
      '#type' => 'button',
      '#value' => t('Create dashboard for selected'),
      '#id' => 'create_dashboard',
    ];

    return $render;
  }

  /**
   * @param $plugin
   */
  public static function renderPlugin($plugin) {
    $render = [];
    $plugin_id = $plugin->getPluginId();
    $fields = $plugin->getFields();

    // Set up each plugin as a tab:
    $render['plugin_' . $plugin_id] = [
      '#group' => 'stats',
      '#type' => 'details',
      '#title' => t($plugin->getPluginDefinition()['label']),
      'run_button' => [
        '#markup' => t('<p><a class="btn button" href="@url" target="_blank">Run calculations again</a></p>', [
          '@url' => Drupal\Core\Url::fromRoute('statistics_snapshots.run_plugin', ['plugin_id' => $plugin_id])
            ->toString(),
        ]),
      ],
    ];

    //    $allowCharts = Drupal::moduleHandler()->moduleExists('charts');
    $allowCharts = FALSE;
    $renderables = static::getRenderablesForPlugin($plugin, $allowCharts);

    // Render any normal stat rows that aren't rendered as proportions or other fancy ways:
    $rows = [];
    foreach ($renderables['ungrouped'] as $field_id) {
      $fieldDefinition = $fields[$field_id];
      $columns = static::getStatValue($plugin, $field_id, NULL, NULL, TRUE);
      if (!$columns) {
        // @todo throw error
        continue;
      }

      $label = static::getFieldLabel($fieldDefinition, $columns);
      $value = $columns['stat_value'];

      $previousValue = static::getStatValue($plugin, $field_id, $columns['time_start'], $columns['time_end']);

      $row = [
        'field_id' => $field_id,
        'label' => $label,
        'period' => static::getPeriodLabel($fieldDefinition, $columns),
        'value' => static::formatStatValue($plugin, $field_id, $value),
        'previous_value' => $previousValue ? static::formatStatValue($plugin, $field_id, $previousValue) : NULL,
      ];
      $rows[$field_id] = $row;
    }
    if (count($rows) > 0) {
      $render['plugin_' . $plugin_id]['ungrouped_stats'] = static::renderStatsTable(t('General statistics'), $rows, $plugin_id);
    }

    // Render any groups:
    foreach ($renderables['group'] as $groupName => $field_ids) {
      $rows = [];

      foreach ($field_ids as $field_id) {
        $fieldDefinition = $fields[$field_id];
        $columns = static::getStatValue($plugin, $field_id, NULL, NULL, TRUE);
        if (!$columns) {
          // @todo throw error
          continue;
        }
        $label = static::getFieldLabel($fieldDefinition, $columns);
        $value = $columns['stat_value'];

        $row = [
          'field_id' => $field_id,
          'label' => $label,
          'period' => static::getPeriodLabel($fieldDefinition, $columns),
          'value' => static::formatStatValue($plugin, $field_id, $value),
        ];

        if ($proportionOf = $fieldDefinition->getSetting('proportion_of')) {
          $proportionValue = ($value / static::getStatValue($plugin, $proportionOf)) * 100;
          $formattedProportion = round($proportionValue, 2) . '%';
          $row['proportion_of'] = $formattedProportion;
        }

        $rows[$field_id] = $row;
      }
      if (array_column($rows, 'proportion_of')) {
        array_walk($rows, function (&$value) {
          if (!isset($value['proportion_of'])) {
            $value['proportion_of'] = '';
          }
        });
      }

      if (count($rows) > 0) {
        $render['plugin_' . $plugin_id]['group_' . $groupName] = static::renderStatsTable($groupName, $rows, $plugin_id);
      }
    }

    // Work out proportions:
    $proportionTotals = [];
    foreach ($renderables['proportion'] as $proportionName => $field_ids) {
      if (!isset($proportionTotals[$proportionName])) {
        $proportionTotals[$proportionName] = 0;
      }
      foreach ($field_ids as $field_id) {
        $value = static::getStatValue($plugin, $field_id);
        $proportionTotals[$proportionName] += $value;
      }
    }
    foreach ($proportionTotals as $proportionName => $proportionTotal) {
      $proportionRows = [];
      foreach ($renderables['proportion'][$proportionName] as $field_id) {
        $fieldDefinition = $fields[$field_id];
        $columns = static::getStatValue($plugin, $field_id, NULL, NULL, TRUE);
        if (!$columns) {
          // @todo throw error
          continue;
        }
        $label = static::getFieldLabel($fieldDefinition, $columns);
        if ($fieldDefinition->getSetting('proportion') == $proportionName) {
          $value = $columns['stat_value'];

          if ($proportionTotal) {
            $proportionValue = ($value / $proportionTotal) * 100;
            $formattedProportion = round($proportionValue, 2) . '%';
          }
          else {
            $proportionValue = 0;
            $formattedProportion = '???';
          }

          $proportionRows[] = [
            'field_id' => $field_id,
            'label' => $label,
            'period' => static::getPeriodLabel($fieldDefinition, $columns),
            'value' => static::formatStatValue($plugin, $field_id, $value),
            'proportion' => $formattedProportion,
            'sort' => $proportionValue,
          ];
        }
      }

      if (count($proportionRows) > 0) {
        $render['plugin_' . $plugin_id]['proportion_' . $proportionName] = static::renderProportionsTable($proportionName, $proportionRows, $plugin_id);
      }
    }

    return $render;
  }

  /**
   * @param $plugin
   * @param bool $allowCharts
   *
   * @return array|array[]
   */
  public static function getRenderablesForPlugin($plugin, bool $allowCharts = TRUE): array {
    $renderables = [
      'proportion' => [],
      'chart' => [],
      'group' => [],
      'ungrouped' => [],
    ];

    $possibleGroupingTypes = array_keys($renderables);
    unset($possibleGroupingTypes['ungrouped']);
    if (!$allowCharts) {
      unset($possibleGroupingTypes['chart']);
    }

    $fields = $plugin->getFields();
    foreach ($fields as $fieldName => $fieldDefinition) {
      $fieldIsInGrouping = FALSE;
      foreach ($possibleGroupingTypes as $groupingType) {
        if ($grouping = $fieldDefinition->getSetting($groupingType)) {
          if (!isset($renderables[$groupingType][$grouping])) {
            $renderables[$groupingType][$grouping] = [];
          }
          $renderables[$groupingType][$grouping][] = $fieldName;
          $fieldIsInGrouping = TRUE;
          break;
        }
      }

      if (!$fieldIsInGrouping) {
        $renderables['ungrouped'][] = $fieldName;
      }
    }

    return $renderables;
  }

  /**
   * @param $plugin
   * @param $field_id
   * @param $returnAllColumns
   *
   * @return array|float|int
   */
  public static function getStatValue($plugin, string $field_id, int $start = NULL, int $end = NULL, bool $returnAllColumns = FALSE) {
    $repository = Drupal::service('statistics_snapshots.repository');
    $columns = $repository->getStatValueAllColumns($plugin, $field_id);
    if (!$columns) {
      return NULL;
    }
    if ($returnAllColumns) {
      return $columns;
    }
    else {
      return $columns['stat_value'];
    }
  }

  private static function getFieldLabel($fieldDefinition, array $snapshot_row) {
    $label = $fieldDefinition->getSetting('valueLabel') ?: $fieldDefinition->getLabel();
    return $label;
  }

  private static function getPeriodLabel($fieldDefinition, array $snapshot_row) {
    if ($period = $fieldDefinition->getSetting('period')) {
      $formatter = Drupal::service('date.formatter');
      $start = (int) $snapshot_row['time_start'];
      $end = (int) $snapshot_row['time_end'];

      if ($period == 'snapshot') {
        return t('As at @date', [
          '@date' => Drupal::service('date.formatter')->format($start, 'short'),
        ]);
      }

      return static::formatDatesForPeriod($period, $start, $end);
    }
    return NULL;
  }

  public static function formatDatesForPeriod($period, int $start, int $end) {
    if ($period == 'snapshot') {
      return t('As at @date', [
        '@date' => Drupal::service('date.formatter')->format($start, 'short'),
      ]);
    }

    $format = 'Y-m-d H:i:s';
    if ($period == 'month') {
      $format = 'Y-m';
    }
    elseif (strstr($period, 'year')) {
      $format = 'Y';
    }
    elseif ($period == 'day') {
      $format = 'D-m-y';
    }

    $startDateTime = new DrupalDateTime();
    $startDateTime->setTimestamp($start);
    $startFormatted = $startDateTime->format($format);

    $endDateTime = new DrupalDateTime();
    $endDateTime->setTimestamp($end);
    $endFormatted = $endDateTime->format($format);

    if ($startFormatted == $endFormatted) {
      $label = $startFormatted;
    }
    else {
      $label = $startFormatted . ' - ' . $endFormatted;
    }
    return $label;
  }

  /**
   * @param $plugin
   * @param $field_id
   * @param $value
   *
   * @return string
   */
  public static function formatStatValue($plugin, $field_id, $value): string {
    if (!$value && $value !== 0 && $value !== '0') {
      return '{ not calculated }';
    }

    $field_type = $plugin->getFields()[$field_id]->getType();
    if ($field_type == 'integer') {
      return number_format($value, 0);
    }
    elseif ($field_type == 'float') {
      return number_format($value, 2);
    }

    return $value;
  }

  /**
   * @param $tableName
   * @param $rows
   *
   * @return array
   */
  public static function renderStatsTable(string $tableName, array $rows, string $plugin_id): array {
    $headers = [
      'stat' => t('Statistic'),
      'period' => t('Time/date range'),
      'value' => t('Value'),
    ];

    if (count(array_column($rows, 'proportion_of')) > 0) {
      $headers[] = t('%');
    }

    $headers[] = t('Select');

    static::addCheckboxToRows($rows, $plugin_id);

    static::removeColumn($rows, 'field_id');

    $table = [
      '#type' => 'table',
      '#prefix' => '<h2>' . $tableName . '</h2>',
      '#header' => $headers,
      '#rows' => $rows,
    ];

    $table = static::removePeriodIfTheSame($table);

    return $table;
  }

  private static function addCheckboxToRows(array &$rows, string $plugin_id) {
    $finalRows = [];
    foreach ($rows as $row) {
      $checkbox = [
        '#type' => 'checkbox',
        '#name' => $plugin_id . ':' . $row['field_id'],
        '#attributes' => [
          'class' => [
            'history_checkbox',
          ],
        ],
      ];
      $row['history'] = Drupal::service('renderer')->render($checkbox);
      $finalRows[] = $row;
    }
    $rows = $finalRows;
  }

  private static function removeColumn(array &$array, string $column) {
    array_walk($array, function (&$value) use ($column) {
      unset($value[$column]);
    });
  }

  private static function removePeriodIfTheSame(array $table): array {
    $periods = [];
    $rows = [];
    foreach ($table['#rows'] as $id => $row) {
      if ($row['period']) {
        if (!in_array($row['period'], $periods)) {
          $periods[] = $row['period'];
        }
      }
    }

    // If the whole table only contains data with one period, move it to the caption:
    if (count($periods) == 1) {
      unset($table['#header']['period']);
      $table['#caption'] = $periods[0];
      array_walk($table['#rows'], function (&$value) {
        unset($value['period']);
      });
    }
    return $table;
  }

  /**
   * @param $proportionName
   * @param $proportionRows
   *
   * @return array
   */
  public static function renderProportionsTable(string $proportionName, array $proportionRows, string $plugin_id): array {
    // Sort:
    usort($proportionRows, function ($first, $second) {
      if (!isset($first['sort'])) {
        return 1;
      }
      if (!isset($second['sort'])) {
        return -1;
      }
      return $second['sort'] > $first['sort'];
    });

    // Remove sort column:
    static::removeColumn($proportionRows, 'sort');

    static::addCheckboxToRows($proportionRows, $plugin_id);

    static::removeColumn($proportionRows, 'field_id');

    $header = [
      'stat' => t('Statistic'),
      'period' => t('Time/date range'),
      'value' => t('Value'),
      'percentage' => t('%'),
      'history' => t('View history'),
    ];

    $table = [
      '#type' => 'table',
      '#prefix' => '<h2>' . $proportionName . '</h2>',
      '#header' => $header,
      '#rows' => $proportionRows,
    ];
    $table = static::removePeriodIfTheSame($table);
    return $table;
  }

  /**
   * @param $plugin_id
   *
   * @return array
   */
  public function runPlugin($plugin_id): array {
    $start = Drupal::time()->getCurrentTime();

    $service = Drupal::service('statistics_snapshots.service');
    $service->calculateStatsForPlugin($plugin_id);

    $end = Drupal::time()->getCurrentTime();

    $render = [];

    $pluginManager = Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $plugin = $pluginManager->createInstance($plugin_id);
    if (isset($plugin->SPLITS_INTO_QUEUES) && $plugin->SPLITS_INTO_QUEUES) {
      $render[] = [
        '#markup' => t('Added calculations to queue for "@plugin". This may mean the numbers are not currently up to date until the queue is fully run.', ['@plugin' => $plugin_id]),
      ];
    }
    else {
      $rendered = static::renderPlugin($plugin);

      $render[] = [
        '#markup' => t('<p>Calculated for plugin ID "@plugin".</p>', ['@plugin' => $plugin_id]),
      ];
      $render[] = [
        '#markup' => t('<p><b>Started:</b> @startTime.</p>', [
          '@startTime' => Drupal::service('date.formatter')
            ->format($start, 'custom', 'Y-m-d H:i:s'),
        ]),
      ];
      $render[] = [
        '#markup' => t('<p><b>Ended:</b> @endTime.</p>', [
          '@endTime' => Drupal::service('date.formatter')
            ->format($end, 'custom', 'Y-m-d H:i:s'),
        ]),
      ];
      $render += $rendered;
    }

    return $render;
  }

}
