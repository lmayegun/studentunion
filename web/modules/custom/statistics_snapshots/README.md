Statistics Snapshots generates snapshots of site statistics for defined periods, based on custom plugins you program,
extending an easy-to-use plugin base.

Each statistic is stored as a field against a Snapshot entity, which means they can be used in views and Charts like
other fields.

By default, a daily snapshot is generated just after midnight each day.

For better snapshot browsing, and a form to bulk generate snapshots, enable the statistics_snapshots_ui module.

# Using in views

https://www.drupal.org/project/view_custom_table

# Making your own plugins

Have a look at UserStatsProvider as an example.

Basically, calculators define some fields (which get added to the Snapshot entity type), and then defines a calculate
function that calculates those fields.

If you can run the entire calculation in one timeout, do as normal by running your calculations in the calculate()
function of your plugin, and using $repository->setStatValue().

If you need multiple timeouts, in calculate() check if $segment_data is set. If it isn't, do the initial load of
whatever you need to process in your calculations, e.g. entities, and then run splitIntoQueues with an array of that as
your data set, and return. (This is a bit like in the Batch API). Then, after that return call, use $segment_data as
your data, and use incrementStatValue instead of setStatValue. The plugin manager will loop through the overall dataset
as chunks.
