<?php

namespace Drupal\announcement_block\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'announcement_block.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('announcement_block.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Include [count_date] to include countdown/countup date'),
      '#default_value' => str_replace('<span id="announcement_block_countdown"></span>', '[count_date]', $config->get('text')),
    ];

    $form['date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Countdown/Countup target'),
      '#default_value' => $config->get('date') && $config->get('date') > 0 ? DrupalDateTime::createFromTimestamp($config->get('date')) : null,
    ];

    $form['background_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background color'),
      '#default_value' => $config->get('background_color') ?? '#FFFFFF',
    ];

    $form['text_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Text color'),
      '#default_value' => $config->get('text_color') ?? '#000000',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // dd($form_state->getValue('date'));

    $text = $form_state->getValue('text')['value'];
    $text = str_replace('[count_date]', '<span id="announcement_block_countdown"></span>', $text);

    $this->config('announcement_block.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('text', $text)
      ->set('date', $form_state->getValue('date') ? $form_state->getValue('date')->getTimestamp() : null)
      ->set('background_color', $form_state->getValue('background_color'))
      ->set('text_color', $form_state->getValue('text_color'))
      ->save();
  }
}
