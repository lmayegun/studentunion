<?php

namespace Drupal\announcement_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;

/**
 * Show a message (customisable in settings) for org leaders.
 *
 * @Block(
 *   id = "announcement_block",
 *   admin_label = @Translation("Announcement Block"),
 *   category = @Translation("Blocks"),
 * )
 */
class AnnouncementBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('announcement_block.settings');
    if ($config->get('enabled') == 1) {
      $libraries = [];
      $text = $config->get('text');
      if ($config->get('date') > 0) {
        $libraries[] = 'announcement_block/announcement_block.countdown';

        $now = strtotime('now');
        $date_formatter = \Drupal::service('date.formatter');
        // $time_string = $date_formatter->formatDiff($now, $config->get('date'), ['granularity' => 3]);
        $time_string = '...';

        $text = str_replace(
          '<span id="announcement_block_countdown"></span>',
          '<span id="announcement_block_countdown">' . $time_string . '</span>',
          $text
        );
      }

      return [
        '#theme' => 'announcement_block',
        '#text' => $text,
        '#date' => $config->get('date'),
        '#background_color' => $config->get('background_color'),
        '#text_color' => $config->get('text_color'),
        '#attached' => [
          'library' => $libraries,
        ],
      ];
    } else {
      return [];
    }
  }

  public function getCacheTags() {
    $fields = [
      'config:announcement_block',
      'config:announcement_block.settings',
      'config:announcement_block.enabled',
      'config:announcement_block.text',
      'config:announcement_block.date',
      'config:announcement_block.background_color',
      'config:announcement_block.text_color',
    ];
    return Cache::mergeTags(parent::getCacheTags(), $fields);
  }
}
