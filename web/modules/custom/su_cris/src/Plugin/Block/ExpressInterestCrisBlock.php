<?php

namespace Drupal\su_cris\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Entity\EntityFormMode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupRole;
use Drupal\group\Plugin\GroupContentEnabler\GroupMembership;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Plugin\Block\UserCreateGroupMembershipRecord;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "cris_express_interest",
 *   admin_label = @Translation("Join CRIS idea with expression of interest"),
 * )
 */
class ExpressInterestCrisBlock extends UserCreateGroupMembershipRecord {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = parent::build();

    if (!su_cris_expression_of_interest_open()) {
      $setting_value = \Drupal::config('su_cris.settings')->get('expressions_of_interest_closed_message');
      if ($setting_value) {
        $text = $setting_value['value'];
      } else {
        $text = '<p>Expressions of interest are currently closed.</p>';
      }

      $button = '<p><a href="mailto:su.cris@ucl.ac.uk" class="button">Contact the CRIS team</a></p>';

      return [
        '#markup' => $text . $button,
      ];
    }


    $group = group_membership_record_get_route_entity();
    if ($group->getEntityTypeId() !== 'group') {
      return [];
    }
    $groupRole = GroupRole::load($this->configuration['role_to_grant']);
    $account = User::load(\Drupal::currentUser()->id());

    if (!su_cris_user_get_registration($account)) {
      $text = '<p>You\'re not currently registered for the Community Research Initiative for Students (CRIS). Learn more and sign-up on the CRIS website.</p>';

      $link = "https://studentsunionucl.org/volunteering/cris";
      $button = '<p><a href="' . $link . '" class="button">Visit the CRIS website</a></p>';

      return [
        '#markup' => $text . $button,
      ];
    }

    return $markup;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form = parent::blockForm($form, $form_state);

    $form['not_cris_registered'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice for users who do not have not registered for CRIS.'),
      '#default_value' => $this->configuration['not_cris_registered'] ?? '',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['not_cris_registered'] = $form_state->getValue('not_cris_registered');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }
}
