<?php

namespace Drupal\group_membership_record\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Entity\EntityFormMode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupRole;
use Drupal\group\Plugin\GroupContentEnabler\GroupMembership;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "su_cris_org_block",
 *   admin_label = @Translation("CRIS - block for organisations working with CRIS"),
 * )
 */
class OrganisationCrisBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $group = group_membership_record_get_route_entity();
    if ($group->getEntityTypeId() !== 'group') {
      return [];
    }

    if ($group->bundle() !== 'volunteering_org') {
      return [];
    }

    if ($group->field_vol_org_work_with_cr->value != TRUE) {
      return [];
    }

    $text = isset($this->configuration['text']['value']) && $this->configuration['text']['value'] != '' ? '<p>' . $this->configuration['text']['value'] . '</p>' : '';

    $link = $this->configuration['button_link'];
    if ($link) {
      $button = '<p><a href="' . $link . '" class="button">' . $this->configuration['button_text'] . '</a></p>';
    }

    return [
      '#markup' => $text . $button,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['text'] = [
      '#type' => 'text_format',
      '#format' => $this->configuration['text']['format'],
      '#default_value' => $this->configuration['text']['value'],
      '#title' => $this->t('Text to show user on orgs working with CRIS'),
      '#required' => FALSE,
      '#allowed_formats' => ['basic_html'],
    ];

    $form['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text for button, e.g. Contact us'),
      '#default_value' => $this->configuration['button_text'] ?? 'Get in contact',
      '#required' => TRUE,
    ];

    $form['button_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link for button'),
      '#default_value' => $this->configuration['button_url'] ?? '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['text'] = $form_state->getValue('text');
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['button_link'] = $form_state->getValue('button_link');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), []);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route.group'));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 60 * 60; // 1 hour
  }
}
