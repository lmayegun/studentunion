<?php

namespace Drupal\su_cris\Controller;

use DateTime;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

class CrisProfileTabController extends ControllerBase implements ContainerInjectionInterface {

  function renderTab($user) {
    $showRegister = TRUE;

    $build = [];

    // Orgs list
    $records = \Drupal::service('group_membership_record.repository')->get($user, NULL, GroupRole::load('volunteering_org-cris_leader'), NULL, new DateTime(), TRUE);
    $orgs = [];
    foreach ($records as $record) {
      $orgs[] = [
        'Organisation' => $record->getGroup()->toLink()->toString(),
        'Role' => $record->getRole()->label(),
        'View' => $record->getGroup()->toLink('view', 'canonical')->toString(),
      ];
    }

    if (count($orgs) > 0) {
      $build['orgs'] = [
        '#type' => 'table',
        '#caption' => 'CRIS organisations you have access to',
        '#header' => array_keys($orgs[0]),
        '#rows' => $orgs,
        '#empty' => t('No organisations.'),
      ];
      $showRegister = FALSE;
    }

    if ($showRegister) {
      // Submission summary OR link to form
      $crisRegistration = su_cris_user_get_registration($user);
      if ($crisRegistration) {
        $url = $submission->toUrl()->toString();
        $build['registration'] = [
          '#type' => 'markup',
          '#markup' => '<h2>Registration</h2><a href="' . $url . '">View your CRIS registration.</a>',
          '#weight' => 100,
        ];
      } else {
        $build['register'] = [
          '#type' => 'markup',
          '#markup' => '<h2>Registration</h2> You\'re not currently registered for the Community Research Initiative for Students (CRIS). <a href="https://studentsunionucl.org/volunteering/cris">Learn more and sign-up on the CRIS website.</a>',
          '#weight' => 100,
        ];
      }
    }

    // Reflections
    $render = su_cris_get_rendered_reflections_block($user);
    if ($render) {
      $build['reflections'] = [
        '#type' => 'markup',
        '#markup' => '<h2>Progress logs</h2><div><a href="https://studentsunionucl.org/forms/cris-reflection" class="union-button">Add progress log</a></div>' . $render,
        '#weight' => 100,
      ];
    }

    return $build;
  }
}
