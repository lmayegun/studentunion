<?php

namespace Drupal\su_cris\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SU Community Research Initiative / CRIS settings for this site.
 */
class CrisSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'su_cris_cris_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['su_cris.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['expressions_of_interest_open'] = [
      '#type' => 'select',
      '#title' => $this->t('Expressions of interest status'),
      '#options' => [
        'closed' => 'Closed',
        'open' => 'Open',
      ],
      '#default_value' => $this->config('su_cris.settings')->get('expressions_of_interest_open'),
    ];

    $form['expressions_of_interest_closed_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message to show if expressions of interest closed'),
      '#default_value' => $this->config('su_cris.settings')->get('expressions_of_interest_closed_message')['value'],
      '#format' => $this->config('su_cris.settings')->get('expressions_of_interest_closed_message')['format'],
      '#allowed_formats' => ['basic_html']
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('su_cris.settings')
      ->set('expressions_of_interest_open', $form_state->getValue('expressions_of_interest_open'))
      ->set('expressions_of_interest_closed_message', $form_state->getValue('expressions_of_interest_closed_message'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
