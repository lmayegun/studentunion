<?php

namespace Drupal\su_cris\Form;

use Drupal\su_volunteering\Form\AddPlacementForm;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class AddCrisLeaderForm extends AddPlacementForm {
  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added CRIS contact.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved CRIS contact.';

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = ['volunteering_org-cris_leader'];


  /**
   * {@inheritdoc}
   */
  protected $fieldsToAllow = ['user_id', 'date_range', 'enabled'];

  /**
   * {@inheritdoc}
   */
  // protected $fieldsToHide = ['enabled'];

}
