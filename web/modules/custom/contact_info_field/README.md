CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Contact Info Field introduces a new field type ("Contact Info") which
can be added to content types and other entities to collect information like:
name, phone, e-mail, role, etc.

This allows collecting multiple people's contact information in a single field.

What elements are collected and displayed is entirely configurable.

These can then be displayed in a list or table, and exported individually.

 * For a full description of the module visit:
   https://www.drupal.org/project/contact_info_field

 * To submit bug reports and feature suggestions, or to track changes
   visit: https://www.drupal.org/project/issues/contact_info_field


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Contact Info Field as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Contact Info Field module.
    2. Manage the fields for your content type or entity.
    3. Add a field with "Contact info" type.
    4. In Form display, control what elements are collected.
    5. In View display, control what elements are displayed
       and select whether to show a table etc.


MAINTAINERS
-----------

 * Maxwell Keeble (maxwellkeeble) - https://www.drupal.org/u/maxwellkeeble
