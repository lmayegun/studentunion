<?php

namespace Drupal\contact_info_field\Plugin\Field\FieldFormatter;

use Drupal\contact_info_field\Plugin\Field\FieldFormatter\ContactInfoFieldFormatter as FieldFormatterContactInfoFieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the 'dice' formatter.
 *
 * @FieldFormatter (
 *   id = "contact_info_single_field",
 *   label = @Translation("Single field value of a specific contact"),
 *   field_types = {
 *     "contact_info_field"
 *   }
 * )
 */
class ContactInfoSingleValueFormatter extends FormatterBase
{
    use ContactInfoFormatterTrait;

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
            'field' => '',
        ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $elements['field'] = [
            '#type' => 'select',
            '#title' => 'Select field to show',
            '#options' => [
                'Name' => 'Name',
                'Position' => 'Position',
                'E-mail' => 'E-mail',
                'Phone' => 'Phone',
                'User account' => 'User account',
                'Link' => 'Link',
                'Notes' => 'Notes',
            ],
            '#default_value' => $this->getSetting('field'),
        ];

        return $elements;
    }


    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode = NULL)
    {
        $elements = array();

        foreach ($items as $delta => $item) {
            $values = $this->getItemValues($item);

            $markup = isset($values[$this->getSetting('field')]) ? $values[$this->getSetting('field')] : '';

            $elements[$delta] = array(
                '#type' => 'markup',
                '#markup' => $markup,
            );
        }

        return $elements;
    }
}
