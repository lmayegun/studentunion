<?php

namespace Drupal\contact_info_field\Plugin\Field\FieldFormatter;

use Drupal\contact_info_field\Plugin\Field\FieldFormatter\ContactInfoFieldFormatter as FieldFormatterContactInfoFieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the 'dice' formatter.
 *
 * @FieldFormatter (
 *   id = "contact_info_field_table",
 *   label = @Translation("Table of contacts"),
 *   field_types = {
 *     "contact_info_field"
 *   }
 * )
 */
class ContactInfoTableFieldFormatter extends FormatterBase {
  use ContactInfoFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    return $elements;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    $rows = [];
    foreach ($items as $delta => $item) {
      $rows[] = $this->getItemValues($item);
    }

    if (count($rows) == 0) {
      return [];
    }

    // Hide empty columns.
    $headers = array_keys($rows[0]);
    foreach ($headers as $index => $key) {
      $keepHeader = FALSE;
      foreach ($rows as $row) {
        if ($row[$key]) {
          $keepHeader = TRUE;
        }
      }
      if (!$keepHeader) {
        unset($headers[$index]);
        foreach ($rows as $rowIndex => $row) {
          unset($rows[$rowIndex][$key]);
        }
      }
    }

    // Render table.
    $build['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No contact information.'),
    ];

    $elements[$delta] = [
      '#type' => '#markup',
      '#markup' =>  \Drupal::service('renderer')->render($build)
    ];

    return $elements;
  }
}
