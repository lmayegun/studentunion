<?php

namespace Drupal\contact_info_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the 'dice' formatter.
 *
 * @FieldFormatter (
 *   id = "contact_info_field",
 *   label = @Translation("One contact per line"),
 *   field_types = {
 *     "contact_info_field"
 *   }
 * )
 */
class ContactInfoFieldFormatter extends FormatterBase {
  use ContactInfoFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'seperator' => '•',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['seperator'] = [
      '#type' => 'textfield',
      '#title' => 'Seperator between fields',
      '#default_value' => $this->getSetting('seperator'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $values = $this->getItemValues($item);
      $values = array_filter($values);

      $markup = implode(' ' . $this->getSetting('seperator') . ' ', $values);

      $elements[$delta] = array(
        '#type' => 'markup',
        '#markup' => $markup,
      );
    }

    return $elements;
  }
}
