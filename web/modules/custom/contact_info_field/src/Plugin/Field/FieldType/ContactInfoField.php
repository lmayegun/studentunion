<?php

namespace Drupal\contact_info_field\Plugin\Field\FieldType;

use Drupal\Core\Render\Element\Email;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'dice' field type.
 *
 * @FieldType (
 *   id = "contact_info_field",
 *   label = @Translation("Contact info"),
 *   description = @Translation("Collects name, email and phone."),
 *   default_widget = "contact_info_field",
 *   default_formatter = "contact_info_field"
 * )
 */
class ContactInfoField extends FieldItemBase
{
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition)
  {
    return array(
      'columns' => array(
        'name' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
        'email' => array(
          'type' => 'varchar',
          'length' => Email::EMAIL_MAX_LENGTH,
        ),
        'url' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
        'phone' => array(
          'type' => 'varchar',
          'length' => 50,
          'not null' => FALSE,
        ),
        'notes' => array(
          'type' => 'blob',
          'size' => 'big',
        ),
        'position' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
        'uid' => array(
          'type' => 'int',
          'not null' => FALSE,
          'unsigned' => TRUE,
          'default' => 0,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty()
  {
    $value1 = $this->get('name')->getValue();
    return empty($value1);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
  {
    // Add our properties.
    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(FALSE);

    $properties['email'] = DataDefinition::create('email')
      ->setLabel(t('Email'))
      ->setRequired(FALSE);

    $properties['url'] = DataDefinition::create('uri')
      ->setLabel(t('URL'))
      ->setRequired(FALSE);

    $properties['phone'] = DataDefinition::create('string')
      ->setLabel(t('Phone number'))
      ->setRequired(FALSE);

    $properties['notes'] = DataDefinition::create('string')
      ->setLabel(t('Notes'))
      ->setRequired(FALSE);

    $properties['uid'] = DataDefinition::create('integer')
      ->setLabel(t('User ID'))
      ->setRequired(FALSE);

    $properties['position'] = DataDefinition::create('string')
      ->setLabel(t('Position/role'))
      ->setRequired(FALSE);

    return $properties;
  }
}
