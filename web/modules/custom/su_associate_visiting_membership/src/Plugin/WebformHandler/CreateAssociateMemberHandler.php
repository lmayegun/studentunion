<?php

namespace Drupal\su_associate_visiting_membership\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileType;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Create a new node entity from a webform submission.
 *
 * @WebformHandler(
 *   id = "create_associate_member_handler",
 *   label = @Translation("Create an associate/visiting member if the submission is confirmed"),
 *   category = @Translation("Students' Union UCL"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */

class CreateAssociateMemberHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // 'profile_type' => '',
      // 'entity_values' => [],
      // 'skip_empty_webform_fields' => true
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    // $list = ProfileType::loadMultiple();
    // foreach ($list as $profileType) {
    //     $options[$profileType->id()] = $profileType->label();
    // }
    // $form['profile_type'] = [
    //     '#type' => 'select',
    //     '#title' => $this->t('Profile type'),
    //     '#options' => $options,
    //     '#required' => TRUE,
    //     '#default_value' => $this->configuration['profile_type']
    // ];
    // $form['skip_empty_webform_fields'] = [
    //     '#type' => 'checkbox',
    //     '#title' => $this->t('Don\'t update profile fields if webform field empty'),
    //     '#default_value' => $this->configuration['skip_empty_webform_fields']
    // ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->applyFormStateToConfiguration($form_state);
    // $this->configuration['profile_type'] = $form_state->getValue('profile_type');
    // $this->configuration['skip_empty_webform_fields'] = $form_state->getValue('skip_empty_webform_fields');
  }

  // Function to be fired after submitting the Webform.
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $workflowElement = 'workflow';
    $workflowValues = $webform_submission->getElementData($workflowElement);
    $targetWorkflowStates = ['confirmed', 'approved_and_paid'];
    $state = $workflowValues['workflow_state'];
    $create = in_array($state, $targetWorkflowStates);
    if ($create) {
      su_associate_visiting_membership_create_from_submission($webform_submission);
    }
  }
}
