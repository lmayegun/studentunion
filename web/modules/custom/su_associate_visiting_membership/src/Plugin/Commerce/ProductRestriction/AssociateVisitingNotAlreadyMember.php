<?php

namespace Drupal\su_associate_visiting_membership\Plugin\Commerce\ProductRestriction;

use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginBase;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;

/**
 * Provides product restriction by user role.
 *
 * @ProductRestrictionPlugin(
 *   id = "associate_visiting_not_already_member",
 *   label = @Translation("Restrict to users who are not Union members"),
 *   category = @Translation("Union"),
 *   entity_type = "commerce_product",
 *   entity_bundles = {},
 *   weight = -10
 * )
 */
class AssociateVisitingNotAlreadyMember extends ProductRestrictionPluginBase implements ProductRestrictionPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->hasRole('associate_visiting') || $account->hasRole('member')) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function accessErrorMessage($product_or_variation) {
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->hasRole('associate_visiting')) {
      return new TranslatableMarkup(
        'You already have a current associate/visiting membership.',
        []
      );
    }
    if ($account->hasRole('member')) {
      return new TranslatableMarkup(
        'You already have a current Union membership.',
        []
      );
    }
  }
}
