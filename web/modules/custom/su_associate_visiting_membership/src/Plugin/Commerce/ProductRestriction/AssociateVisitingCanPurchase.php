<?php

namespace Drupal\su_associate_visiting_membership\Plugin\Commerce\ProductRestriction;

use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginBase;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;
use Drupal\webform\Entity\Webform;

/**
 * Provides product restriction by user role.
 *
 * @ProductRestrictionPlugin(
 *   id = "associate_visiting_can_purchase",
 *   label = @Translation("User has approved Associate/Visiting Membership form request"),
 *   category = @Translation("Union"),
 *   entity_type = "commerce_product",
 *   entity_bundles = {},
 *   weight = -10
 * )
 */
class AssociateVisitingCanPurchase extends ProductRestrictionPluginBase implements ProductRestrictionPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->hasRole('associate_visiting') || $account->hasRole('member')) {
      return false;
    }

    $webform = Webform::load('webform_122466');
    $approvedSubmissions = webform_workflows_element_queries_get_submission_with_state($webform, 'workflow', 'approved_but_unpaid');
    if (count($approvedSubmissions) == 0) {
      return false;
    }

    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function accessErrorMessage($product_or_variation) {
    $message = [];

    $account = User::load(\Drupal::currentUser()->id());
    if ($account->hasRole('associate_visiting')) {
      return new TranslatableMarkup(
        'You already have a current associate/visiting membership.',
        []
      );
    }

    if ($account->hasRole('member')) {
      return new TranslatableMarkup(
        'You already have a current Union membership.',
        []
      );
    }

    $webform = Webform::load('webform_122466');
    $approvedSubmissions = webform_workflows_element_queries_get_submission_with_state($webform, 'workflow', 'approved_but_unpaid', \Drupal::currentUser()->id());
    if (count($approvedSubmissions) == 0) {
      return new TranslatableMarkup(
        'You need to be logged in as a user that has submitted and had approved a request via the <a href="https://studentsunionucl.org/forms/request-visiting-or-associate-membership-for-clubs-and-societies">Request Visiting or Associate Membership for Clubs and Societies form</a>.',
        []
      );
    }
  }
}
