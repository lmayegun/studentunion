<?php

namespace Drupal\su_associate_visiting_membership\EventSubscriber;

use DateTime;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderItemEvent;
use Drupal\commerce_refund_order_item\Event\OrderRefundedEvent;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderRefundedSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.refund.order_item' => ['onOrderRefund', 100],

      'commerce_order.commerce_order.predelete' => ['onOrderCancelOrDelete', 100],
      'commerce_order.cancel.post_transition' => ['onOrderCancelOrDeleteWorkflow', 100],

      'commerce_order.commerce_order_item.predelete' => ['onOrderItemDelete', 100]
    ];
    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param OrderRefundedEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderRefund(OrderRefundedEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $itemsWithQuantity = $event->getItemsWithQuantity();

    foreach ($itemsWithQuantity as $orderItemId => $quantityRefunded) {
      if ($quantityRefunded == 0) {
        continue;
      }
      $orderItem = OrderItem::load($orderItemId);
      $this->removeMembershipForOrderItem($orderItem);
    }
  }

  public function onOrderCancelOrDelete(OrderEvent $event) {
    $items = $event->getOrder()->getItems();
    foreach ($items as $orderItem) {
      $this->removeMembershipForOrderItem($orderItem);
    }
  }

  public function onOrderCancelOrDeleteWorkflow(WorkflowTransitionEvent $event) {
    $items = $event->getEntity()->getItems();
    foreach ($items as $orderItem) {
      $this->removeMembershipForOrderItem($orderItem);
    }
  }

  /**
   * @param OrderItemEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderItemDelete(OrderItemEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $orderItem = $event->getOrderItem();
    $this->removeMembershipForOrderItem($orderItem);
  }

  public function removeMembershipForOrderItem($orderItem) {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = $orderItem->getPurchasedEntity();
    if (!$variation) {
      return;
    }

    if ($variation->bundle() !== 'associate_visiting_membership') {
      return;
    }

    $account = $orderItem->getOrder()->getCustomer();

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $records = $gmrRepositoryService->get($account, Group::load(1), GroupRole::load('student_record-record_member'), null, new DateTime(), true);

    foreach ($records as $record) {
      $record->setEndDate(strtotime('yesterday'));
      $record->save();
      // Site roles will automatically sync on saving the record - don't need to do it here
    }
  }
}
