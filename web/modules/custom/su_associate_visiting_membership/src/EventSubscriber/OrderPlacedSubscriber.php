<?php

namespace Drupal\su_associate_visiting_membership\EventSubscriber;

use DateTime;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\Core\Form\FormState;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\webform\Entity\Webform;
use Drupal\workflow\Entity\Workflow;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderPlacedSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['onOrderPlace', 100],
    ];
    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param WorkflowTransitionEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onOrderPlace(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $account = $order->getCustomer();

    $orderItems = $order->getItems();
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $orderItem */
    foreach ($orderItems as $orderItem) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
      $variation = $orderItem->getPurchasedEntity();
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      if ($variation && $product = $variation->getProduct()) {
        if ($product->bundle() == 'associate_visiting_membership') {
          // Find webform submission
          $webform = Webform::load('webform_122466');
          $approvedSubmissions = webform_workflows_element_queries_get_submission_with_state($webform, 'workflow', 'approved_but_unpaid', \Drupal::currentUser()->id());
          if (count($approvedSubmissions) > 0) {
            foreach ($approvedSubmissions as $submission) {
              if ($submission->getOwnerId() !== \Drupal::currentUser()->id()) {
                continue;
              }
              $element_id = 'workflow';
              $workflowId = 'basic_approval_with_payment_step';
              $workflowsManager = \Drupal::service('webform_workflows_element.manager');

              $workflowType = $workflowsManager->getWorkflowType($workflowId);
              $transition = $workflowType->getTransition('confirm_after_payment');

              $newData = $submission->getElementData($element_id);
              $newData['workflow_state'] = $transition->to()->id();
              $newData['transition'] = $transition->id();
              $submission->setElementData($element_id, $newData);
              $submission->save();

              // Get a handler.
              $handlers = $webform()->getHandlers();
              foreach ($handlers as $handler) {
                $handler->postSave($submission);
              }

              su_associate_visiting_membership_create_from_submission($submission);
            }
          }
        }
      }
    }
  }
}
