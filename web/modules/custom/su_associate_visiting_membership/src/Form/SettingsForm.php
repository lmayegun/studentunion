<?php

namespace Drupal\su_associate_visiting_membership\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'su_associate_visiting_membership.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('su_associate_visiting_membership.settings');
    $form['end_of_membership'] = [
      '#type' => 'date',
      '#title' => $this->t('End of academic year for associate/visiting membership'),
      '#description' => $this->t('Note for this field the day and month are used, but the year is ignored.'),
      '#default_value' => $config->get('end_of_membership'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    $this->config('su_associate_visiting_membership.settings')
      ->set('end_of_membership', $form_state->getValue('end_of_membership'))
      ->save();
  }
}
