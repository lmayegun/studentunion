<?php

namespace Drupal\su_associate_visiting_membership\Form;

use DateTime;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Form\GroupMembershipRecordForm;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for committee members.
 *
 * @ingroup Students' Union UCL
 */
class AssociateVisitingRecordForm extends GroupMembershipRecordForm {

  /**
   * {@inheritdoc}
   */
  protected $groupTypes = ['student_record'];

  /**
   * {@inheritdoc}
   */
  protected $messageAdded = 'Added associate/visiting member.';

  /**
   * {@inheritdoc}
   */
  protected $messageUpdated = 'Saved associate/visiting member.';

  /**
   * {@inheritdoc}
   */
  protected $allowedGroups = [1];

  /**
   * {@inheritdoc}
   */
  protected $allowedRoles = [];

  /**
   * {@inheritdoc}
   */
  protected $allowedMembershipRecordTypes = ['associate_visiting_membership'];

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Set default daterange
    $endDate = \Drupal::config('su_associate_visiting_membership.settings')->get('end_of_membership');
    $now = new DateTime();
    $yearService = \Drupal::getContainer()->get('date_year_filter.year');
    $year = $yearService->getYearForDate($now, $endDate);
    $this->defaultStartDate = $now->format("Y-m-d\TH:i:s");
    $this->defaultEndDate = $year->end->format("Y-m-d\TH:i:s");

    $this->customFormInfo = 'Search by any user details to find an existing account if possible. Otherwise, enter the full e-mail and name to have an account created.';

    $form = parent::buildForm($form, $form_state);

    $form['new_user_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email (if new user)'),
      '#description' => $this->t('Only needed if the user cannot be found via the User field above.'),
      '#default_value' => '',
      '#required' => FALSE,
      '#maxlength' => 64,
      '#size' => 30,
      '#weight' => 3,
      '#states' => [
        'visible' => [
          'input[name="user_id[0][target_id]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['new_user_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full name (if new user)'),
      '#description' => $this->t('Only needed if the user cannot be found via the User field above.'),
      '#default_value' => '',
      '#required' => FALSE,
      '#weight' => 3,
      '#states' => [
        'visible' => [
          'input[name="user_id[0][target_id]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['field_order']['#states'] = [
      'visible' => [
        'select[name="field_associate_visiting_origin"]' => ['value' => 'Online'],
      ],
    ];

    $form['user_id']['widget'][0]['target_id']['#required'] = FALSE;
    $form['user_id']['widget'][0]['target_id']['#validated'] = FALSE;

    return $form;
  }


  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Re-add all but the user_id error
    $form_errors = $form_state->getErrors();
    $form_state->clearErrors();
    unset($form_errors['user_id']);
    foreach ($form_errors as $name => $error_message) {
      $form_state->setErrorByName($name, $error_message);
    }

    // Check if already has role
    $userId = $form_state->getValue('user_id')[0]['target_id'];
    if ($userId) {
      $user = User::load($userId);
      if ($user->hasRole('associate_visiting')) {
        $form_state->setErrorByName('user_id', 'User is already an associate/visiting member.');
      }
    }

    // $form_state->setValueForElement($form['id'], $this->targetEntityTypeId . '.' . $form_state->getValue('id'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // If user not specified, try to find from e-mail; if not, create
    if (!$form_state->getValue('user_id') || !$form_state->getValue('user_id')[0]['target_id']) {
      $mail = $form_state->getValue('new_user_email');

      $user = su_associate_visiting_membership_get_or_create_user($mail, $form_state->getValue('new_user_name'));
      $form_state->setValue('user_id', [0 => ['target_id' => $user->id()]]);
      $this->entity->user_id->entity = $user;
    }

    parent::save($form, $form_state);
  }
}
