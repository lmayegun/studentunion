<?php

namespace Drupal\su_associate_visiting_membership\Form;

use DateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchImportCSVForm;

/**
 * Implement Class BulkUserImport for import form.
 */
class BulkImportAssociateVisitingMembers extends BatchImportCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'su_associate_visiting_membership_bulk_membership';
  }

  public static function getMapping(): array {
    $fields = [];
    $fields['Start'] = [
      'description' => 'date to start membership',
      'dataFormat' => 'd/m/Y',
      'example' => '15/06/2025',
    ];
    $fields['End'] = [
      'description' => 'date to end membership',
      'dataFormat' => 'd/m/Y',
      'example' => '14/06/2026',
    ];
    $fields['E-mail'] = [
      'description' => '',
      'example' => 'm.keeble.21@ucl.ac.uk',
    ];
    $fields['Full name'] = [
      'description' => '',
      'example' => '',
    ];
    $fields['Action'] = [
      'description' => 'leave off to create, put in "end" to end any existing memberships that match the row using the provided e-mail and end date, put in "update" to try to update the existing record matching those dates if found',
    ];

    return $fields;
  }

  public static function getMappingAsList() {
    $t = '';
    $fields = static::getMapping();
    foreach ($fields as $name => $data) {
      $t .= '<li><b>' . $name . '</b>: ' . ($data['description'] ?? '') . (isset($data['example']) ? ' (e.g. ' . $data['example'] . ')' : '') . '</li>';
    }
    return $t;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $action = $row['Action'] ?? 'create';

    if (isset($row['Email'])) {
      $row['E-mail'] = $row['Email'];
    }

    if (isset($row['Full Name'])) {
      $row['Full name'] = $row['Full Name'];
    }

    $required = ['Start', 'End', 'E-mail', 'Full name'];
    foreach ($required as $field) {
      if (!isset($row[$field])) {
        static::addError($row, $context, 'Required field not provided: ' . $field);
        return FALSE;
      }
    }

    $start = DateTime::createFromFormat('d/m/Y', $row['Start']);
    if (!$start) {
      static::addError($row, $context, 'Couldnt work start date out from ' . $row['Start'] . ' - must be d/m/Y like 01/12/2021');
      return FALSE;
    }

    $end = DateTime::createFromFormat('d/m/Y', $row['End']);
    if (!$end) {
      static::addError($row, $context, 'Couldnt work end date out from ' . $row['End'] . ' - must be d/m/Y like 01/12/2021');
      return FALSE;
    }

    $user = su_associate_visiting_membership_get_or_create_user($row['E-mail'], $row['Full name']);

    $gmr = NULL;
    $existingRecords = su_associate_visiting_membership_get($user, $start);

    if (count($existingRecords) > 0) {
      if ($action == 'create') {
        static::addError($row, $context, 'User already has ' . count($existingRecords) . ' record(s) with these details for the start date.');
        return FALSE;
      }
      elseif ($action == 'end') {
        foreach ($existingRecords as $gmr) {
          $gmr->setEndDate($end);
          $gmr->save();
        }
        return TRUE;
      }

      if (!isset($row['Action']) || $row['Action'] == '') {
        static::addError($row, $context, 'Identical existing record appears to exist; not creating a new one unless told to by setting Action to "create".');
        return FALSE;
      }

      if ($action == 'update' && count($existingRecords) > 1) {
        static::addError($row, $context, 'Multiple existing identical records (' . count($existingRecords) . '), not sure which to update.');
        return FALSE;
      }
      $gmr = reset($existingRecords);
    }

    if (!$gmr && $action == 'update') {
      static::addError($row, $context, 'Cannot find existing record to update.');
      return FALSE;
    }

    if (!$gmr && $action == 'end') {
      static::addError($row, $context, 'Cannot find existing record to end.');
      return FALSE;
    }

    if (!isset($gmr) || !$gmr) {
      /** @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $gmr */
      $gmr = su_associate_visiting_membership_create($user, NULL, $end);
    }
    $gmr->field_associate_visiting_origin->value = 'Bulk';
    $gmr->save();

    return TRUE;
  }

}
