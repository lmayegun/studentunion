<?php

/**
 * @file
 * Contains fair_video.page.inc.
 *
 * Page callback for Fair video entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Fair video templates.
 *
 * Default template: fair_video.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_fair_video(array &$variables) {
  // Fetch FairVideo Entity Object.
  $fair_video = $variables['elements']['#fair_video'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
