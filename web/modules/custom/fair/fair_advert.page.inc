<?php

/**
 * @file
 * Contains fair_advert.page.inc.
 *
 * Page callback for Fair advert entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Fair advert templates.
 *
 * Default template: fair_advert.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_fair_advert(array &$variables) {
  // Fetch FairAdvert Entity Object.
  $fair_advert = $variables['elements']['#fair_advert'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
