<?php

/**
 * @file
 * Contains fair.page.inc.
 *
 * Page callback for Fair entities.
 */

use Drupal\Core\Render\Element;
use Drupal\user\Entity\User;

/**
 * Prepares variables for Fair templates.
 *
 * Default template: fair.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_fair(array &$variables) {
  // Fetch Fair Entity Object.
  $fair = $variables['elements']['#fair'];
  $fair->setFairVariables($variables);
}

function collapseArrayField($array, $field) {
  $values = array_column($array, $field);
  $result = [];
  foreach ($values as $value) {
    if (!is_countable($value)) {
      return null;
    }
    if (count($value) > 0) {
      $result = array_merge($result, $value);
    }
  }
  if (!$result) {
    return null;
  }
  $result = array_filter(array_unique($result));
  if (!$result) {
    return null;
  }
  return array_values($result);
}

function fair_page_convert_election_candidate_fields_to_stall_fields($asArray) {
  // Fair field -> group field(s) - enter in order of preference
  $definitions = [
    // Required fields - app may break without them
    'name' => ['label', 'name'],
    'fair_stall_category' => ['field_fair_stall_category'],
    'fair_stall_card' => ['field_candidate_photo'],
    'fair_stall_tagline' => ['field_fair_tagline'],
    'field_fair_stall_website' => ['field_fair_stall_website', 'field_website'],
    'field_fair_stall_facebook' => ['field_fair_stall_facebook', 'field_facebook'],
    'field_fair_stall_twitter' => ['field_fair_stall_twitter', 'field_twitter'],
    'field_fair_stall_instagram' => ['field_fair_stall_instagram', 'field_instagram'],

    // Optional fields
    // 'field_fair_stall_contact_email' => ['field_email', 'field_vol_org_email'],
    // 'field_fair_stall_description' => ['field_clubsoc_body', 'field_vol_org_description'],
    // 'field_fair_stall_description_summary' => ['field_clubsoc_body_summary'],

    // To test
    'field_fair_stall_slide_images' => ['field_fair_stall_slide_images'],
    'fair_stall_tags' => ['field_fair_stall_tags'],
    'field_fair_stall_videos' => ['field_fair_stall_videos', 'field_videos'],
    'field_fair_stall_comments' => ['field_fair_comments'],
  ];

  foreach ($definitions as $fairField => $groupFields) {
    foreach ($groupFields as $groupField) {
      if (isset($asArray[$groupField])) {
        $asArray[$fairField] = $asArray[$groupField];
        break;
      }
    }
  }

  return $asArray;
}

function fair_page_convert_group_fields_to_stall_fields($asArray) {
  // Fair field -> group field(s) - enter in order of preference
  $definitions = [
    // Required fields - app may break without them
    'name' => ['label'],
    'fair_stall_category' => ['field_fair_stall_category'],
    'fair_stall_card' => ['field_card_image'],
    'fair_stall_tagline' => ['field_tagline'],
    'field_fair_stall_website' => ['field_website'],
    'field_fair_stall_facebook' => ['field_facebook'],
    'field_fair_stall_twitter' => ['field_twitter'],
    'field_fair_stall_instagram' => ['field_instagram'],

    // Optional fields
    'field_fair_stall_advert_image' => ['field_fair_stall_advert_image'],
    'field_fair_stall_contact_email' => ['field_email', 'field_vol_org_email'],
    'field_fair_stall_description' => ['field_clubsoc_body', 'field_vol_org_description'],
    'field_fair_stall_description_summary' => ['field_clubsoc_body_summary'],
    'field_fair_stall_logo' => ['field_logo', 'field_vol_org_logo'],
    'field_fair_stall_advert_url' => ['field_fair_stall_advert_url'],

    // To test
    'field_fair_stall_slide_images' => ['field_fair_stall_slide_images'],
    'fair_stall_tags' => ['field_fair_stall_tags'],
    'fair_stall_reference' => ["field_fairs"],
    'field_fair_stall_events' => ['field_fair_stall_events'],
    'field_fair_stall_comments' => ['field_club_society_comments'],
    'field_fair_stall_videos' => ['field_fair_stall_videos', 'field_videos'],
    'field_fair_stall_comments' => ['field_fair_stall_comments'],

    // field_fair_stall_online: "0"
    // field_fair_stall_qanda_enabled: "1"
    // field_fair_stall_comments: ""

    // Not to move across
    // 'field_enable_joining' => [''],
    // fair_stall_managers: []
    // field_users_joined: []

  ];

  foreach ($definitions as $fairField => $groupFields) {
    foreach ($groupFields as $groupField) {
      if (isset($asArray[$groupField])) {
        $asArray[$fairField] = $asArray[$groupField];
        break;
      }
    }
  }

  return $asArray;
}

function fair_page_convert_entity_for_json($entity) {
  $asArray = $entity->toArray();

  if (isset($asArray['users_joined'])) {
    unset($asArray['users_joined']);
  }

  if (isset($asArray['field_mailing_list_emails'])) {
    unset($asArray['field_mailing_list_emails']);
  }

  foreach ($asArray as $key => $values) {
    $fieldType = $entity->getFieldDefinition($key)->getType();
    // echo $fieldType . '(' . $key . ') ';
    switch ($fieldType) {
      case 'string':
      case 'string_long':
      case 'uuid':
      case 'timestamp':
      case 'boolean':
      case 'email':
        $asArray[$key] = $entity->$key->getString();
        break;

      case 'color_field_type':
        $asArray[$key] = isset($entity->$key->getValue()[0]['color']) ? $entity->$key->getValue()[0]['color'] : '';
        break;

      case 'entity_reference':
        $array = [];
        foreach ($entity->$key as $entityRef) {
          if ($entityRef->entity) {
            if (in_array($key, ['fair_stall_category', 'fair_stall_tags', 'field_fair_stall_category', 'field_fair_stall_tags', 'field_fair_ranking_reference'])) {
              $array[] = $entityRef->entity->getName();
            } else if (in_array($key, ['field_fair_stall_events'])) {
              // print_r($entityRef->entity);
              if ($entityRef->entity && count($entityRef->entity->field_url) > 0) {
                $events = [
                  'title' => $entityRef->entity->getName(),
                  'url' => $entityRef->entity->field_url[0]->uri,
                  'dates' => []
                ];
                $i = 0;
                foreach ($entityRef->entity->field_date_range as $date) {
                  $date = $date->toArray();
                  $events['dates'][] = [
                    'date_start' =>  $date['value'],
                    'date_end' => $date['end_value']
                  ];
                  $i++;
                }
                $array[] = $events;
              }
            } else {
              $array[] = $entityRef->entity->id();
            }
          }
        }
        if ($key == 'fair_stall_tags' || $key == 'field_fair_stall_tags') {
          $array = array_slice($array, 0, 10);
        }
        $asArray[$key] = $array;
        break;

      case 'integer':
        $asArray[$key] = $entity->$key->first() ? $entity->$key->first()->value : '';
        break;

      case 'text_with_summary':
        $asArray[$key . '_summary'] = isset($entity->$key[0]) ? $entity->$key[0]->summary : '';
        $asArray[$key] = isset($entity->$key[0]) ? $entity->$key[0]->value : '';
        break;

      case 'text_long':
        $asArray[$key] = isset($entity->$key[0]) ? $entity->$key[0]->value : '';
        break;

      case 'video_embed_field':
        $array = [];
        foreach ($entity->$key as $index => $value) {
          $array[] = ['url' => $entity->$key[$index]->getValue()['value'], 'title' => ''];
        }
        $asArray[$key] = $array;
        break;

      case 'link':
        $array = [];
        foreach ($entity->$key as $index => $value) {
          if (isset($entity->$key[$index])) {
            $array[] = ['url' => $entity->$key[$index]->uri, 'title' => $entity->$key[$index]->title];
          }
        }
        $asArray[$key] = $array;
        break;

      case 'image':
        $array = [];
        foreach ($entity->$key as $index => $value) {
          if (isset($entity->$key[$index]) && isset($entity->$key[$index]->entity)) {
            $originalUri = $entity->$key[$index]->entity->getFileUri();
            $fieldsToStyles = [
              'fair_stall_card' => 'fair_card',
              'field_fair_stall_card' => 'fair_card',
              'field_card_image' => 'fair_card'
            ];
            if (isset($fieldsToStyles[$key])) {
              // Load the image style configuration entity.
              $style = \Drupal\image\Entity\ImageStyle::load($fieldsToStyles[$key]);
              $uri = $style->buildUri($originalUri);
              if (!file_exists($uri)) {
                $style->createDerivative($originalUri, $uri);
              }
            } else {
              $uri = $originalUri;
            }
            $array[] = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
          }
        }
        $asArray[$key] = $array;
        break;

      case 'comment':
      case 'changed':
      case 'created':
      case 'language':
      case 'path':
        $asArray[$key] = "";
        break;

      default:
        // echo $fieldType . '(' . $key . ') ';
        break;
    }
  }

  if ($entity->getEntityTypeId() == 'group') {
    $asArray = fair_page_convert_group_fields_to_stall_fields($asArray);
  } elseif ($entity->getEntityTypeId() == 'election_candidate') {
    $asArray = fair_page_convert_election_candidate_fields_to_stall_fields($asArray);
  }

  $asArray['type'] = $entity->bundle();

  return $asArray;
}
