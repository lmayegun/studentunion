<?php

namespace Drupal\fair;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\fair\Entity\FairInterface;

/**
 * Defines the storage handler class for Fair entities.
 *
 * This extends the base storage class, adding required special handling for
 * Fair entities.
 *
 * @ingroup fair
 */
class FairStorage extends SqlContentEntityStorage implements FairStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(FairInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {fair_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {fair_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(FairInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {fair_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('fair_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
