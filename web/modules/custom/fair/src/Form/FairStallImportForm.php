<?php

namespace Drupal\fair\Form;

use Drupal\fair\Form\ImportFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

use Drupal\fair\Entity\Fair;
use Drupal\fair\Controller\FairStallImportController;

/**
 * Implement Class BulkUserImport for import form.
 */
class FairStallImportForm extends ImportFormBase {

  public const importController = 'FairStallImportController';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fair_stall_import';
  }

}
