<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FairStallTypeForm.
 */
class FairStallTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $fair_stall_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $fair_stall_type->label(),
      '#description' => $this->t("Label for the Fair stall type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $fair_stall_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\fair\Entity\FairStallType::load',
      ],
      '#disabled' => !$fair_stall_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $fair_stall_type = $this->entity;
    $status = $fair_stall_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Fair stall type.', [
          '%label' => $fair_stall_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Fair stall type.', [
          '%label' => $fair_stall_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($fair_stall_type->toUrl('collection'));
  }

}
