<?php

namespace Drupal\fair\Form;

use Drupal\fair\Form\ImportFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

use Drupal\fair\Entity\Fair;
use Drupal\fair\Controller\FairStallImportController;

/**
 * Implement Class BulkUserImport for import form.
 */
class ScheduleItemImportForm extends ImportFormBase {

  public const importController = 'ScheduleItemImportController';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fair_schedule_import';
  }

}
