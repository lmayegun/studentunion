<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Fair stall edit forms.
 *
 * @ingroup fair
 */
class FairStallForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\fair\Entity\FairStall $entity */
    $form = parent::buildForm($form, $form_state);

    // Protect some fields from editing from e.g. stall managers
    // See also fair_form_field_config_edit_form_alter
    if(!$this->account->hasPermission('edit fair stall protected fields')) 
    {
      $fields = $this->entity->getFieldDefinitions();
      foreach($fields AS $field_name => $field) {
        if(isset($form[$field_name]) && method_exists($field, 'getThirdPartySettings')) {
            // echo $field_name.'<hr>'.$field->getThirdPartySetting('fair', 'limit_to_fairs_admin').'<hr>';
            $form[$field_name]['#disabled'] = $field->getThirdPartySetting('fair', 'limit_to_fairs_admin');
        }
      }

      // Base fields to disable for non-admin
      $baseFields = ['name', 'user_id', 'fair_stall_reference'];
      if($this->entity->bundle() == 'election_candidate') {
        $baseFields[] = 'fair_stall_category';
        $baseFields[] = 'fair_stall_card';
        $form['fair_stall_card']['#prefix'] = $this->t('The card image will be imported from your nomination via the elections platform after all nominations are submitted. You can add images to your image slideshow however!');
      }      
      foreach($baseFields AS $field) {
        $form[$field]['#disabled'] = true;
      }
    }

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Fair stall.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Fair stall.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.fair_stall.canonical', ['fair_stall' => $entity->id()]);
  }

}
