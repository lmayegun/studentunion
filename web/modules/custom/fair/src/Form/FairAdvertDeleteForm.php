<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fair advert entities.
 *
 * @ingroup fair
 */
class FairAdvertDeleteForm extends ContentEntityDeleteForm {


}
