<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Schedule item entities.
 *
 * @ingroup fair
 */
class ScheduleItemDeleteForm extends ContentEntityDeleteForm {


}
