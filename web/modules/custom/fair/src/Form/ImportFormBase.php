<?php

namespace Drupal\fair\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

use Drupal\fair\Entity\Fair;

use Drupal\fair\Controller\FairStallImportController;
use Drupal\fair\Controller\ScheduleItemImportForm;

/**
 * Implement Class BulkUserImport for import form.
 */
abstract class ImportFormBase extends FormBase {
  public const importController = ''; 

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fair_import';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fair.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Fair $fair = null) {
    if(!$fair) {
      $this->messenger()->addError('No valid fair ID provided in the URL.');
      unset($form['submit']);
      return $form;
    }

    $form_state->set("fair", $fair);

    $form['explanation'] = array(
        '#prefix' => '<h1>How to use</h1>',
        '#markup' => '<ol><li>Download the template using the button above.</li><li>Complete according to the instructions in the second row.</li><li>Before uploading the CSV remove the second (explanations) row.</li><li>Upload here and import.</li><li>Note any errors in the error report and reupload if needed.</li></ul>',
        '#suffix' => '',
    ); 

    $form['file_upload'] = [
      '#prefix' => '<h1>Upload and import</h1>',
      '#type' => 'file',
      '#title' => $this->t('Import CSV File'),
      '#size' => 40,
      '#description' => $this->t('Select the CSV file to be imported.'),
      '#required' => false,
      '#autoupload' => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations = [];

    $controllerName = '\Drupal\fair\Controller\\' . static::importController;
    $controller = new $controllerName;

    $validators = ['file_validate_extensions' => ['csv']];
    $file = file_save_upload('file_upload', $validators)[0];
    $csv_array = $this->convertCSVtoArray($file->getFileUri());

    if(!$csv_array) {
      $this->messenger()->addError('Invalid CSV file.');
      return false;
    }

    $first = true;
    foreach($csv_array AS $row) {
      // Add to batch to process
      $operations[] = [$controllerName . '::runOperation', ['importRow', $row, $form_state->get("fair")->id()]];
    }

    // print_r($operations); die;

    $controller::start($operations);    
  }

  public function convertCSVtoArray($filename = '', $delimiter = ','){

    if(!file_exists($filename) || !is_readable($filename)) return FALSE;
    $header = NULL;
    $data = array();

    if (($handle = fopen($filename, 'r')) !== FALSE ) {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
      {
        if(!$header){
          $header = $row;
        }else{
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }

    return $data;
  }

}
