<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fair stall entities.
 *
 * @ingroup fair
 */
class FairStallDeleteForm extends ContentEntityDeleteForm {


}
