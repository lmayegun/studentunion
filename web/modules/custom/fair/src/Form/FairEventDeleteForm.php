<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fair event entities.
 *
 * @ingroup fair
 */
class FairEventDeleteForm extends ContentEntityDeleteForm {


}
