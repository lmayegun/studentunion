<?php

namespace Drupal\fair\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

use Drupal\fair\Entity\Fair;

use Drupal\fair\Controller\FairStallImportController;
use Drupal\fair\Controller\FairStallResetController;
use Drupal\fair\Controller\ScheduleItemImportForm;

/**
 * Implement Class 
 */
class FairStallResetForm extends FormBase {
    const module = 'fair';
    const controller = 'FairStallResetController';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fair_reset';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fair.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Fair $fair = null) {
    if(!$fair) {
      $this->messenger()->addError('No valid fair ID provided in the URL.');
      unset($form['submit']);
      return $form;
    }

    $form_state->set("fair", $fair);

    $form['explanation'] = array(
        '#prefix' => '<h1>Note!</h1>',
        '#markup' => '<p>This will clear all mailing list sign-ups and club/society joins for all stalls in this fair, published or unpublished. This data will cleared for the stall wherever it appears, eg. if it appears in more than one fair, this will reset the sign-ups for the stall in all fairs it is in.</p',
        '#suffix' => '',
    ); 

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset stalls'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations = [];

    $fair = $form_state->get('fair');

    $controllerName = '\Drupal\fair\Controller\\' . static::controller;
    $controller = new $controllerName;

    // Load stalls and process
    $query = \Drupal::entityQuery('fair_stall')
      ->condition('fair_stall_reference.target_id', $fair->id());
    $ids = $query->execute();
    $entity_storage = \Drupal::entityTypeManager()->getStorage('fair_stall')->loadMultiple($ids);
    foreach ($entity_storage as $stall) {
      // Add to batch to process
      $operations[] = [$controllerName . '::runOperation', [$stall]];
    }

    $controller::start($operations);    
  }

}
