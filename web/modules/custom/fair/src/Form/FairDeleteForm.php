<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fair entities.
 *
 * @ingroup fair
 */
class FairDeleteForm extends ContentEntityDeleteForm {


}
