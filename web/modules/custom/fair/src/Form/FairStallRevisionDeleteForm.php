<?php

namespace Drupal\fair\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Fair stall revision.
 *
 * @ingroup fair
 */
class FairStallRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Fair stall revision.
   *
   * @var \Drupal\fair\Entity\FairStallInterface
   */
  protected $revision;

  /**
   * The Fair stall storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fairStallStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->fairStallStorage = $container->get('entity_type.manager')->getStorage('fair_stall');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fair_stall_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.fair_stall.version_history', ['fair_stall' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $fair_stall_revision = NULL) {
    $this->revision = $this->FairStallStorage->loadRevision($fair_stall_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->FairStallStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Fair stall: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Fair stall %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.fair_stall.canonical',
       ['fair_stall' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {fair_stall_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.fair_stall.version_history',
         ['fair_stall' => $this->revision->id()]
      );
    }
  }

}
