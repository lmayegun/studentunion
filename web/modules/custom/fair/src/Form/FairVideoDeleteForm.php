<?php

namespace Drupal\fair\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fair video entities.
 *
 * @ingroup fair
 */
class FairVideoDeleteForm extends ContentEntityDeleteForm {


}
