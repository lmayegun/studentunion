<?php

namespace Drupal\fair\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Fair video revision.
 *
 * @ingroup fair
 */
class FairVideoRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Fair video revision.
   *
   * @var \Drupal\fair\Entity\FairVideoInterface
   */
  protected $revision;

  /**
   * The Fair video storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fairVideoStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->fairVideoStorage = $container->get('entity_type.manager')->getStorage('fair_video');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fair_video_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.fair_video.version_history', ['fair_video' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $fair_video_revision = NULL) {
    $this->revision = $this->FairVideoStorage->loadRevision($fair_video_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->FairVideoStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Fair video: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Fair video %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.fair_video.canonical',
       ['fair_video' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {fair_video_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.fair_video.version_history',
         ['fair_video' => $this->revision->id()]
      );
    }
  }

}
