<?php

namespace Drupal\fair\Controller;

use DateTime;
use Drupal\comment\Entity\Comment;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_store\Entity\Store;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\fair\Entity\FairStall;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\image\Entity\ImageStyle;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonAPIController extends ControllerBase {

  /**
   * @param $stall
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserStallActions($stall) {
    $data = NULL;

    $stall = $this->getStallEntity($stall);
    $type = $stall->getEntityTypeId();

    $data['stall_type']        = $type . '--' . $stall->bundle();
    $data['stall_uuid']        = $stall->uuid();
    $data['stall_entity_type'] = $stall->getEntityTypeId();

    $account = User::load($this->currentUser()->id());

    // VISITOR STATS
    // Log visitor details
    fair_log_stall_visit($stall->id(), $account->id(), $account->uuid());

    // Load stats
    // $minutes = "15"; // minutes to include as "now"
    $data['visitors']['total'] = 0; // fair_log_count_stall_visitors($stall->id());
    $data['visitors']['now']   = 0; // fair_log_count_stall_visitors($stall->id(), strtotime("-" . $minutes . " minutes"));

    if ($this->isCurrentUserAnonymous($account)) {
      return new JsonResponse($data);
    }

    // FAVOURITES
    // If the user does not have a fairs' profile create it
    $profile    = $this->getCurrentUserFairProfile($account);
    $favourites = [];
    if (!empty($profile)) {
      $favourites                  = $profile->field_fair_stalls_reference;
      $data['favourite']['bundle'] = $profile->uuid();
    }
    if ($type == 'group') {
      $data['favourite']['field'] = 'field_fair_groups_reference';
    } else {
      $data['favourite']['field'] = 'field_fair_stalls_reference';
    }
    $data['favourite']['favourited'] = FALSE;
    if ($favourites) {
      foreach ($favourites as $favourite) {
        if ($favourite->entity->id() == $stall->id()) {
          $data['favourite']['favourited'] = TRUE;
          break;
        }
      }
    }

    // JOINED
    if ($stall->hasField('field_users_joined')) {
      $data['join']['field']     = 'field_users_joined';
      $data['join']['user_type'] = 'user--user';
      $data['join']['user_uuid'] = $account->uuid();

      // Check if user is joined or not
      $data['join']['joined']     = FALSE;
      $usersJoined                = $stall->get('field_users_joined')
        ->referencedEntities();
      $data['visitors']['joined'] = count($usersJoined);
      foreach ($usersJoined as $userJoined) {
        if ($this->getCurrentUserUUID($account) == $userJoined->uuid()) {
          $data['join']['joined'] = TRUE;
          break;
        }
      }
    } elseif ($stall->getEntityTypeId() == 'group') {
      $data['join']['user_type'] = 'user--user';
      $data['join']['user_uuid'] = $account->uuid();

      /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
      $clubSocMembershipsService = \Drupal::service('su_clubs_societies.memberships');
      if ($clubSocMembershipsService->userIsCurrentMember(\Drupal::currentUser(), $stall)) {
        $data['join']['joined'] = TRUE;
      } else {
        $itemsInCart = $this->getCartVariationIds();
        $variationIds = [];
        if (count($itemsInCart) > 0) {
          if ($stall->hasField('field_product') && $stall->field_product && $stall->field_product->entity) {
            $variations = $stall->field_product->entity->getVariations();
            foreach ($variations as $variation) {
              $variationIds[] = $variation->id();
            }
          }
        }

        if (count(array_intersect($itemsInCart, $variationIds)) > 0) {
          $data['join']['in_cart'] = TRUE;
          $data['join']['joined'] = FALSE;
        } else {
          $data['join']['in_cart'] = FALSE;
          $data['join']['joined'] = FALSE;
        }
      }
    }

    // SUBSCRIBED
    $data['subscribe']['user_email'] = $account->getEmail();
    $data['subscribe']['subscribed'] = $this->checkEmailSubscribed($stall, $account->getEmail());

    // COMMENTS
    if ($stall->getEntityTypeId() == 'group') {
      $data['comment']['comment_type'] = 'comment--group_q_a';
      $data['comment']['field_name']   = 'field_' . $stall->bundle() . '_comments';
    } else {
      $data['comment']['comment_type'] = 'comment--fair_stall_comment';
      $data['comment']['field_name']   = 'field_fair_stall_comments';
    }

    return new JsonResponse($data);
  }

  public function getCartVariationIds() {
    $cartProvider = \Drupal::service('commerce_cart.cart_provider');
    $cart = $cartProvider->getCart('default', Store::load(1));
    $itemsInCart = [];
    if ($cart) {
      $items = $cart->getItems();
      foreach ($items as $item) {
        $itemsInCart[] = $item->getPurchasedEntityId();
      }
    }
    return $itemsInCart;
  }

  public function addToCart(ProductVariation $commerce_product_variation) {
    if (!in_array($commerce_product_variation->id(), $this->getCartVariationIds())) {
      $cartManager = \Drupal::service('commerce_cart.cart_manager');
      $cartProvider = \Drupal::service('commerce_cart.cart_provider');
      $store = Store::load(1);
      $cart = $cartProvider->getCart('default', $store);

      if (!$cart) {
        $cart = $cartProvider->createCart('default', $store);
      } else {
        foreach ($cart->getItems() as $item) {
          if ($item->getPurchasedEntity()->getProduct()->id() == $commerce_product_variation->getProductId()) {
            $cart->removeItem($item);
            $cart->save();
          }
        }
      }

      $line_item_type_storage = \Drupal::entityTypeManager()
        ->getStorage('commerce_order_item_type');
      $cart_manager = \Drupal::service('commerce_cart.cart_manager');
      $line_item = $cart_manager->addEntity($cart, $commerce_product_variation);
    }
    $data           = [];
    $data['result'] = 'added';
    return new JsonResponse($data);
  }

  public function subscribeToStall($stall, $email) {
    $stall = $this->getStallEntity($stall);
    $type = $stall->getEntityTypeId();

    $data           = [];
    $data['result'] = 'nothing';

    $data['email_valid'] = \Drupal::service('email.validator')->isValid($email);
    if (!$data['email_valid']) {
      return new JsonResponse($data);
    }

    if ($type == 'group') {
      if ($this->checkEmailSubscribed($stall, $email)) {
        $query = \Drupal::entityQuery('group_membership_record')
          ->condition('type', 'sign_up')
          ->condition('group_id', $stall->id())
          ->condition('field_signup_email', $email);
        $records = $query->execute();
        if (count($records) > 0) {
          $id = reset($records);
          $gmr = GroupMembershipRecord::load($id);
          $gmr->setEndDate(strtotime('midnight yesterday'));
          $gmr->save();
        }
        $data['result'] = "removed";
      } else {
        $record = GroupMembershipRecord::create([
          'type' => 'sign_up',
          'group_id' => $stall->id(),
          'group_role_id' => '',
          'user_id' => \Drupal::currentUser()->id(),
          'date_range' => [
            'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, \Drupal::time()->getRequestTime()),
            'end_value' => ''
          ],
          'field_signup_email' => $email,
          'field_ip' => $_SERVER['REMOTE_ADDR']
        ]);
        $record->save();
        $data['result'] = "added";
      }
    } else {
      $current_emails = array_column($stall->get('field_mailing_list_emails')
        ->getValue(), 'value');
      if (in_array($email, $current_emails)) {
        $data['result'] = "removed";
        //      $current_emails = array_diff($current_emails, [$email]);
      } else {
        $data['result']   = "added";
        $current_emails[] = $email;
      }
      $stall->set('field_mailing_list_emails', $current_emails);
      $stall->save();
    }

    return new JsonResponse($data);
  }

  public function joinToStall($stall) {
    $stall = $this->getStallEntity($stall);
    $type = $stall->getEntityTypeId();

    $account        = User::load($this->currentUser()->id());
    $data           = [];
    $data['result'] = 'nothing';
    if (!$this->isCurrentUserAnonymous($account)) {
      $email = $account->getEmail();

      $data['email_valid'] = strpos($email, '@ucl.ac.uk') > 0;
      if (!$data['email_valid']) {
        return new JsonResponse($data);
      }

      if ($type == 'group') {
      } else {
        $usersJoined = $stall->get('field_users_joined')->getValue();
        $alreadyJoined = false;
        foreach ($usersJoined as $userJoined) {
          if ($account->id() == $userJoined['target_id']) {
            $alreadyJoined = true;
            break;
          }
        }

        if ($alreadyJoined) {
          $data['result'] = "joined";
          //        $usersJoined = array_diff($userJoined, ['target_id' => $account->id()]);
        } else {
          $data['result']   = "joined";
          $usersJoined[] = ['target_id' => $account->id()];
        }
        $stall->set('field_users_joined', $usersJoined);
        $stall->save();
      }
      Cache::invalidateTags(['fair_stall:joined_for_user:' . $account->id()]);
    }

    return new JsonResponse($data);
  }

  /**
   * @param $stall
   * @param $email
   *
   * @return array
   */
  public function checkEmailSubscribed($stall, $email) {
    if ($stall->getEntityTypeId() == 'group') {
      $query = \Drupal::entityQuery('group_membership_record')
        ->condition('type', 'sign_up')
        ->condition('group_id', $stall->id())
        ->condition('field_signup_email', $email);
      $records = $query->execute();
      return count($records) > 0;
    } else {
      if (!$stall->hasField('field_mailing_list_emails')) {
        return false;
      }

      $current_emails = array_column($stall->get('field_mailing_list_emails')
        ->getValue(), 'value');
    }
    return in_array($email, $current_emails);
  }

  public function getStallEntity($stall) {
    if (substr($stall, 0, 1) == 'g') {
      $stall = \Drupal::entityTypeManager()
        ->getStorage('group')
        ->load(substr($stall, 1));
    } else if (substr($stall, 0, 1) == 'c') {
      $stall = \Drupal::entityTypeManager()
        ->getStorage('election_candidate')
        ->load(substr($stall, 1));
    } else {
      $stall = \Drupal::entityTypeManager()
        ->getStorage('fair_stall')
        ->load($stall);
    }
    return $stall;
  }

  /**
   * @param $stall
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCommentsForStall($stall, $timesince = NULL) {
    $account = User::load($this->currentUser()->id());

    $stall = $this->getStallEntity($stall);
    $type = $stall->getEntityTypeId();

    // Load comments:
    $query = \Drupal::entityQuery('comment')
      ->condition('entity_id', $stall->id())
      ->condition('entity_type', $type)
      ->sort('cid', 'DESC');
    if ($timesince) {
      $query->condition('created', $timesince - 60, '>=');
    }
    $cids = $query->execute();

    // Convert for use:
    $comments = [];
    foreach ($cids as $cid) {
      $comment = Comment::load($cid);
      $ownerAccount = User::load($comment->getOwnerId());
      $isReply = $comment->pid->count() > 0;

      // Check access to comment
      // Show if owner or stall manager or the comment has a reply
      $showComment         = FALSE;
      $commentOwner        = $ownerAccount->id() == $account->id();
      $adminOrStallManager = $this->checkIfUserAdminOrStallManager($stall, $account);
      if ($commentOwner || $adminOrStallManager) {
        $showComment = TRUE;
      } elseif ($isReply) {
        $showComment = TRUE;
      } else {
        $countReplies = \Drupal::entityQuery('comment')
          ->condition('entity_id', $stall->id())
          ->condition('entity_type', $type)
          ->condition('pid', $cid);
        $count = $countReplies->count()->execute();
        $showComment  = $countReplies->count()->execute() > 0;
      }

      if ($showComment) {
        $picture = !$ownerAccount->user_picture->isEmpty() ?
          ImageStyle::load('thumbnail')->buildUrl($ownerAccount->user_picture->entity->getFileUri())
          : '';

        $comments[] = [
          'cid'                     => $cid,
          'uuid'                    => $comment->uuid(),
          'pid'                     => $isReply ? $comment->pid->referencedEntities()[0]->id() : NULL,
          'comment_body'            => $comment->get('comment_body')->value,
          'created'                 => $comment->get('created')->value,
          'uid'                     => $comment->getOwnerId(),
          'user_picture'            => $picture,
          'field_first_name'        => $ownerAccount->field_first_name ? $ownerAccount->field_first_name->getString() : '',
          'field_last_name'         => $ownerAccount->field_last_name ? $ownerAccount->field_last_name->getString() : '',
          'field_comment_user_role' => $comment->field_comment_user_role ? $comment->field_comment_user_role->getString() : '',
          'field_comment_category'  => $comment->field_comment_category ? $comment->field_comment_category->getString() : '',
        ];
      }
    }
    return new JsonResponse($comments);
  }

  public function checkIfUserAdminOrStallManager(ContentEntityInterface $stall, User $account = NULL) {
    if (isset($_SESSION['stall_manager_' . $stall->id()])) {
      // return $_SESSION['stall_manager_' . $stall->id()];
    }

    if ($this->isCurrentUserAnonymous($account)) {
      $_SESSION['stall_manager_' . $stall->id()] = FALSE;
      return $_SESSION['stall_manager_' . $stall->id()];
    }

    if (!$account) {
      $account = User::load($this->currentUser()->id());
    }
    $roles = $account->getRoles();
    $admin = in_array('administrator', $roles) || in_array('fairs_administrator', $roles);
    if ($admin) {
      $_SESSION['stall_manager_' . $stall->id()] = TRUE;
      return $_SESSION['stall_manager_' . $stall->id()];
    }

    if ($stall->hasField('fair_stall_managers')) {
      $managers = $stall->fair_stall_managers;
      $ids      = [];
      foreach ($managers as $manager) {
        $ids[] = $manager->target_id;
      }
    }

    if ($stall->getEntityTypeId() == 'election_candidate') {
      $managers = $stall->user_id;
      $ids      = [];
      foreach ($managers as $manager) {
        $ids[] = $manager->target_id;
      }
    }

    if ($stall->getEntityTypeId() == 'group') {
      /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
      $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
      $roles = [
        'club_society-president',
        'club_society-treasurer',
        'volunteering_org-leader',
      ];
      $ids = [];
      foreach ($roles as $role) {
        $records = $gmrRepositoryService->get(NULL, $stall, GroupRole::load($role), NULL, new DateTime(), TRUE);
        foreach ($records as $record) {
          $ids[] = $record->getUser()->id();
        }
      }
    }
    $stallManager = in_array($account->id(), $ids);

    $_SESSION['stall_manager_' . $stall->id()] = $stallManager;
    return $_SESSION['stall_manager_' . $stall->id()];
  }

  /**
   * Delete a comment from a stall
   *
   * @param $stall
   * @param $id
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteCommentForStall($stall, $id) {

    $data           = [];
    $data['result'] = 'nothing';

    $account = User::load($this->currentUser()->id());
    if (substr($stall, 0, 1) == 'g') {
      $stall = Group::load(substr($stall, 1));
    } elseif (substr($stall, 0, 1) == 'c') {
      $stall = ElectionCandidate::load(substr($stall, 1));
    } else {
      $stall = \Drupal::entityTypeManager()
        ->getStorage('fair_stall')
        ->load($stall);
    }
    if ($this->checkIfUserAdminOrStallManager($stall, $account)) {
      try {
        $comment = Comment::load($id);
        if (!$comment) {
          $data['result'] = 'deleted';
        } else {
          $comment->delete();
          $data['result'] = 'deleted';
        }
      } catch (Exception $e) {
        $data['result'] = 'error';
      }
    }

    return new JsonResponse($data);
  }

  /**
   * @var \Drupal\user\Entity\User $account
   *
   * @return string
   */
  public function formatBasicAuth($account) {
    $basicAuthCredential = $account->getAccountName() . ':' . $account->getPassword();
    $base64              = base64_encode($basicAuthCredential);
    return 'Basic ' . $base64;
  }

  /**
   * @param $account
   *
   * @return bool
   */
  public function isCurrentUserAnonymous($account) {
    $roles     = $account->getRoles();
    $anonymous = TRUE;
    if (!in_array('anonymous', $roles)) {
      $anonymous = FALSE;
    }
    return $anonymous;
  }

  /**
   * @return string|null
   */
  public function getCurrentUserUUID($account) {
    if (!$this->isCurrentUserAnonymous($account)) {
      return $account->uuid();
    }
  }

  /**
   * @param $account
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getCurrentUserFairProfile($account) {

    $profile = \Drupal::entityTypeManager()
      ->getStorage('profile')
      ->loadByUser($account, 'fairs');

    if (empty($profile)) {
      $profile = Profile::create([
        'type' => 'fairs',
        'uid'  => $account->id(),
      ]);

      $profile->setDefault(TRUE);
      $profile->save();
    }

    return $profile;
  }
}
