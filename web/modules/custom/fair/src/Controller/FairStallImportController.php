<?php

namespace Drupal\fair\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fair\Entity\FairStall;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements Class StudentImportBatchController Controller.
 */
class FairStallImportController extends ImportControllerBase
{
    const module = 'fair';
    const controller = 'FairStallImportController';
    const title = 'Fair stall import';
    const batchTitle = 'Importing stalls...';
    const batchInitMessage = 'Starting import...';
    const importProcessedOne = 'One stall imported.';
    const importProcessedMany = '@count fair stalls processed.';

    const fields = [
        'Stall name' => [
            'description' => 'Text only.',
            'database' => 'name',
            'required' => true
        ],
        'Type' => [
            'description' => 'Either "general", "election_candidate" or "club_society".',
            'required' => true,
            'options' => ['general', 'club_society', 'election_candidate']
        ],
        'Users to add as stall managers' => [
            'description' => 'Semi-colon separated e-mails or user IDs.',
            'required' => false
        ],
        'Category' => [
            'description' => 'Must match existing category exactly.',
            'required' => false
        ],
        'Stall ID' => [
            'description' => 'Existing stall ID to update if needed.',
        ],
    ];

    public static function importRow($row, $fair, &$context)
    {
        $context['sandbox']['current_item'] = $row;
        $errors = [];
        $fields = static::fields;
        
        $errors = parent::getFieldErrors($fields, $row);    

        // Check if we're updating an existing stall
        $stall = false;
        if(isset($row['Stall ID'])) {
            try {
                $stall = FairStall::load($row['Stall ID']);
            } catch(Exception $e) {

            }
            if(!$stall) {
                $row['Stall ID'] = false;
            }
        }

        foreach($fields AS $column => $field) {
            // Unique fields (e.g. name)
            if(isset($field['unique']) && $field['unique']) {
                $query = \Drupal::entityQuery('fair_stall')
                ->condition($field['database'], $row[$column])
                ->condition('fair_stall_reference.target_id', $fair->id());
                if($stall) {
                    $query = $query->condition('id', $stall->id(), '<>');
                }
                $count = $query->count()->execute(); 
                if($count > 0) {
                    $errors[] = "Stall with that ".$column." already exists for this fair. To update existing stall add existing ID in Stall ID column.";
                }
            }
        }

        // Category
        $category = false;
        $categoryName = $row['Category'];
        if($categoryName != '') {            
            $terms = \Drupal::entityTypeManager()
                ->getStorage('taxonomy_term')
                ->loadByProperties(['name' => $categoryName, 'vid' => 'fair_stall_category']);
            if($terms) {
                $term = reset($terms);
                $category = $term->id();
            }
        }

        // Stall managers
        $userEntitiesToAdd = [];
        $userIdentifiersNotToAdd = [];
        $userIdentifiers = explode(";", $row['Users to add as stall managers']);
        foreach($userIdentifiers AS $userIdentifier) {
            if(strlen($userIdentifier) == 0) {
                continue;
            }
            $validUser = false; 
            if(strlen($userIdentifier) === 7) {
                $validUser = user_load_by_name($userIdentifier . '@ucl.ac.uk');
            } elseif(filter_var($userIdentifier, FILTER_VALIDATE_EMAIL)) {
                $validUser = user_load_by_mail($userIdentifier);
                if(!$validUser) {
                    $validUser = user_load_by_name($userIdentifier);
                }
            }
            if($validUser) {
                $userEntitiesToAdd[] = ['target_id' => $validUser->id()];
            } else {
                $userIdentifiersNotToAdd[] = $userIdentifier;
            }
        }
        if(count($userIdentifiersNotToAdd) > 0) {
            $errors[] = "Not all users to add as stall managers found - correct: ".implode(";", $userIdentifiersNotToAdd);
        }

        if(count($errors) == 0) { 
            if(!$stall) {
                $stall = FairStall::create([
                    'name' => utf8_encode($row['Stall name']),
                    'type' => $row['Type'],
                ]);
                $stall->enforceIsNew();
            }      
            if($category) {
                $stall->fair_stall_category = ['target_id' => $category];
            }
            $stall->fair_stall_reference[] = ['target_id' => $fair->id()];
            if($userEntitiesToAdd && count($userEntitiesToAdd) > 0) {
                // print_r($stall->fair_stall_managers); die;
                $stall->fair_stall_managers = array_merge($stall->fair_stall_managers, $userEntitiesToAdd);
            }

            $stall->save();
            $message = "Importing ".$row['Stall name'] .' with ID '. $stall->id();
        } else {            
            $row = parent::addErrorRow($errors, $row);
            $message = "Error for ".$row['Stall name'];
        }

        $context['results'][] = $row;
        $context['message'] = $message;
    }
}
