<?php

namespace Drupal\fair\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\fair\Entity\FairVideoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FairVideoController.
 *
 *  Returns responses for Fair video routes.
 */
class FairVideoController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Fair video revision.
   *
   * @param int $fair_video_revision
   *   The Fair video revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($fair_video_revision) {
    $fair_video = $this->entityTypeManager()->getStorage('fair_video')
      ->loadRevision($fair_video_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('fair_video');

    return $view_builder->view($fair_video);
  }

  /**
   * Page title callback for a Fair video revision.
   *
   * @param int $fair_video_revision
   *   The Fair video revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($fair_video_revision) {
    $fair_video = $this->entityTypeManager()->getStorage('fair_video')
      ->loadRevision($fair_video_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $fair_video->label(),
      '%date' => $this->dateFormatter->format($fair_video->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Fair video.
   *
   * @param \Drupal\fair\Entity\FairVideoInterface $fair_video
   *   A Fair video object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(FairVideoInterface $fair_video) {
    $account = $this->currentUser();
    $fair_video_storage = $this->entityTypeManager()->getStorage('fair_video');

    $langcode = $fair_video->language()->getId();
    $langname = $fair_video->language()->getName();
    $languages = $fair_video->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $fair_video->label()]) : $this->t('Revisions for %title', ['%title' => $fair_video->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all fair video revisions") || $account->hasPermission('administer fair video entities')));
    $delete_permission = (($account->hasPermission("delete all fair video revisions") || $account->hasPermission('administer fair video entities')));

    $rows = [];

    $vids = $fair_video_storage->revisionIds($fair_video);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\fair\FairVideoInterface $revision */
      $revision = $fair_video_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $fair_video->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.fair_video.revision', [
            'fair_video' => $fair_video->id(),
            'fair_video_revision' => $vid,
          ]));
        } else {
          $link = $fair_video->toLink($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link->toString(),
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        } else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
                Url::fromRoute('entity.fair_video.translation_revert', [
                  'fair_video' => $fair_video->id(),
                  'fair_video_revision' => $vid,
                  'langcode' => $langcode,
                ]) :
                Url::fromRoute('entity.fair_video.revision_revert', [
                  'fair_video' => $fair_video->id(),
                  'fair_video_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.fair_video.revision_delete', [
                'fair_video' => $fair_video->id(),
                'fair_video_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['fair_video_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }
}
