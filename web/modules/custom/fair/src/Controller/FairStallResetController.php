<?php

namespace Drupal\fair\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fair\Entity\FairStall;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements Class StudentImportBatchController Controller.
 */
class FairStallResetController extends ControllerBase
{
    const module = 'fair';
    const controller = 'FairStallResetController';
    const title = 'Fair stall reset';
    const batchTitle = 'Resetting stalls...';
    const batchInitMessage = 'Starting reset...';
    const importProcessedOne = 'One stall reset.';
    const importProcessedMany = '@count fair stalls processed.';

  public static function start($operations)
  {
      $_SESSION[static::controller.'errorRows'] = [];
      $batch = array(
          'title' => t(static::batchTitle),
          'operations' => $operations,
          'init_message'     => t(static::batchInitMessage),
          'progress_message' => t('Processed @current out of @total.'),
          'error_message'    => t('An error occurred during processing'),
          'finished' => '\Drupal\\' . static::module . '\Controller\\' . static::controller . '::finished',
      );

      batch_set($batch);
  }

  public static function runOperation($stall, &$context)
  {
     $message = static::resetStall($stall, $context);

      $context['results'][] = $message;
      $context['message'] = $message;
  }


  public static function resetStall($stall, &$context) {
    // Signups and joins
    $fields = ['field_mailing_list_emails', 'field_users_joined'];
    foreach($fields AS $field) {
      if($stall->hasField($field)) {
        $stall->set($field, []);  
      }
    }  
    $stall->save();

    // Visits
    $query = \Drupal::entityQuery('fair_log_entry');
    $query = $query->condition('stall', $stall->id(), '=');
    $ids = $query->execute();
    $entity_storage = \Drupal::entityTypeManager()->getStorage('fair_log_entry')->loadMultiple($ids);
    foreach ($entity_storage as $log) {
      $log->delete();
    }

    $message = 'Reset stall '.$stall->title; 
    return $message;
  }

  public static function finished($success, $results, $operations)
  {
      $batch = &batch_get();
      $controllerName = '\Drupal\fair\Controller\\' . static::controller;
      $controller = new $controllerName;
      if ($success) {
          $message = \Drupal::translation()->formatPlural(count($results),  static::importProcessedOne, static::importProcessedMany);
          \Drupal::messenger()->addMessage($message);
      } else {
        \Drupal::messenger()->addError('Finished with an error.');
      }
  }    
}
