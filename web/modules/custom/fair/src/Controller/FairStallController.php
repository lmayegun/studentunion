<?php

namespace Drupal\fair\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\fair\Entity\FairStallInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FairStallController.
 *
 *  Returns responses for Fair stall routes.
 */
class FairStallController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Fair stall revision.
   *
   * @param int $fair_stall_revision
   *   The Fair stall revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($fair_stall_revision) {
    $fair_stall = $this->entityTypeManager()->getStorage('fair_stall')
      ->loadRevision($fair_stall_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('fair_stall');

    return $view_builder->view($fair_stall);
  }

  /**
   * Page title callback for a Fair stall revision.
   *
   * @param int $fair_stall_revision
   *   The Fair stall revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($fair_stall_revision) {
    $fair_stall = $this->entityTypeManager()->getStorage('fair_stall')
      ->loadRevision($fair_stall_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $fair_stall->label(),
      '%date' => $this->dateFormatter->format($fair_stall->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Fair stall.
   *
   * @param \Drupal\fair\Entity\FairStallInterface $fair_stall
   *   A Fair stall object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(FairStallInterface $fair_stall) {
    $account = $this->currentUser();
    $fair_stall_storage = $this->entityTypeManager()->getStorage('fair_stall');

    $langcode = $fair_stall->language()->getId();
    $langname = $fair_stall->language()->getName();
    $languages = $fair_stall->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $fair_stall->label()]) : $this->t('Revisions for %title', ['%title' => $fair_stall->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all fair stall revisions") || $account->hasPermission('administer fair stall entities')));
    $delete_permission = (($account->hasPermission("delete all fair stall revisions") || $account->hasPermission('administer fair stall entities')));

    $rows = [];

    $vids = $fair_stall_storage->revisionIds($fair_stall);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\fair\FairStallInterface $revision */
      $revision = $fair_stall_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $fair_stall->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.fair_stall.revision', [
            'fair_stall' => $fair_stall->id(),
            'fair_stall_revision' => $vid,
          ]));
        } else {
          $link = $fair_stall->toLink($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link->toString(),
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        } else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
                Url::fromRoute('entity.fair_stall.translation_revert', [
                  'fair_stall' => $fair_stall->id(),
                  'fair_stall_revision' => $vid,
                  'langcode' => $langcode,
                ]) :
                Url::fromRoute('entity.fair_stall.revision_revert', [
                  'fair_stall' => $fair_stall->id(),
                  'fair_stall_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.fair_stall.revision_delete', [
                'fair_stall' => $fair_stall->id(),
                'fair_stall_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['fair_stall_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }
}
