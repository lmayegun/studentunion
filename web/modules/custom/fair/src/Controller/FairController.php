<?php

namespace Drupal\fair\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\fair\Entity\FairInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FairController.
 *
 *  Returns responses for Fair routes.
 */
class FairController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Fair revision.
   *
   * @param int $fair_revision
   *   The Fair revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($fair_revision) {
    $fair = $this->entityTypeManager()->getStorage('fair')
      ->loadRevision($fair_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('fair');

    return $view_builder->view($fair);
  }

  /**
   * Page title callback for a Fair revision.
   *
   * @param int $fair_revision
   *   The Fair revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($fair_revision) {
    $fair = $this->entityTypeManager()->getStorage('fair')
      ->loadRevision($fair_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $fair->label(),
      '%date' => $this->dateFormatter->format($fair->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Fair.
   *
   * @param \Drupal\fair\Entity\FairInterface $fair
   *   A Fair object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(FairInterface $fair) {
    $account = $this->currentUser();
    $fair_storage = $this->entityTypeManager()->getStorage('fair');

    $langcode = $fair->language()->getId();
    $langname = $fair->language()->getName();
    $languages = $fair->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $fair->label()]) : $this->t('Revisions for %title', ['%title' => $fair->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all fair revisions") || $account->hasPermission('administer fair entities')));
    $delete_permission = (($account->hasPermission("delete all fair revisions") || $account->hasPermission('administer fair entities')));

    $rows = [];

    $vids = $fair_storage->revisionIds($fair);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\fair\FairInterface $revision */
      $revision = $fair_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $fair->getRevisionId()) {
          $link = \Drupal\Core\Link::fromTextAndUrl($date, new Url('entity.fair.revision', [
            'fair' => $fair->id(),
            'fair_revision' => $vid,
          ]));
        } else {
          $link = $fair->toLink($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link->toString(),
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        } else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
                Url::fromRoute('entity.fair.translation_revert', [
                  'fair' => $fair->id(),
                  'fair_revision' => $vid,
                  'langcode' => $langcode,
                ]) :
                Url::fromRoute('entity.fair.revision_revert', [
                  'fair' => $fair->id(),
                  'fair_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.fair.revision_delete', [
                'fair' => $fair->id(),
                'fair_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['fair_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }
}
