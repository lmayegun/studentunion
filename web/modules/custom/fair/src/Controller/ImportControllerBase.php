<?php

namespace Drupal\fair\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fair\Entity\Fair;
use Drupal\fair\Entity\FairStall;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements Class StudentImportBatchController Controller.
 */
abstract class ImportControllerBase extends ControllerBase {
  const module = 'fair';
  const controller = 'ImportControllerBase';
  const title = 'Import';
  const batchTitle = 'Importing...';
  const batchInitMessage = 'Starting import...';
  const importProcessedOne = 'One row imported.';
  const importProcessedMany = '@count rows processed.';

  const fields = [];

  public function generateTemplate() {
    $content = implode(",", array_keys(static::fields)) . PHP_EOL;
    $content .= implode(",", array_values(array_column(static::fields, "description"))) . PHP_EOL;
    $content .= implode(",", array_fill(0, count(static::fields), "")) . PHP_EOL;
    return static::generateCSV($content, static::title . ' template');
  }

  public static function generateCSV($content, $filename) {
    $response = new Response($content);
    $response->headers->set('Content-Type', 'text/csv; utf-8');
    $response->headers->set('Content-Disposition', 'attachment; filename = ' . $filename . '.csv');
    return $response;
  }

  public static function start($operations) {
    $_SESSION[static::controller . 'errorRows'] = [];
    $batch = array(
      'title' => t(static::batchTitle),
      'operations' => $operations,
      'init_message'     => t(static::batchInitMessage),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
      'finished' => '\Drupal\\' . static::module . '\Controller\\' . static::controller . '::finished',
    );

    batch_set($batch);
  }

  public static function runOperation($operationType, $row, $fairId, &$context) {
    $firstKey = array_keys(static::fields)[0];
    if ($row[$firstKey] === static::fields[$firstKey]['description']) {
      $message = "Skipping headers and description rows";
    } else {
      $fair = Fair::load($fairId);
      $message = static::$operationType($row, $fair, $context);
    }

    $context['results'][] = $message;
    $context['message'] = $message;
  }

  public static function finished($success, $results, $operations) {
    $batch = &batch_get();
    $controllerName = '\Drupal\fair\Controller\\' . static::controller;
    $controller = new $controllerName;
    if (count($_SESSION[static::controller . 'errorRows']) > 0) {
      $url = static::generateErrorReport($_SESSION[static::controller . 'errorRows'], $batch['id']);
      $link = \Drupal\Core\Link::fromTextAndUrl('Click here for a list of errors for you to fix and reupload.', \Drupal\Core\Url::fromUri($url))->toString();
      $text = new TranslatableMarkup("There were errors processing your file. @link (You can make your changes to the error file directly and upload it, rather than re-uploading the original file.) The rest of the rows in your original spreadsheet were successful.", ["@link" => $link]);
      $controller->messenger()->addError($text);
    } else if ($success) {
      $controller->messenger()->addMessage(\Drupal::translation()->formatPlural(count($results),  static::importProcessedOne, static::importProcessedMany));
    } else {
      $controller->messenger()->addError('Finished with an error.');
    }
  }

  public static function generateErrorReport($errors, $batchId) {
    $content = implode(",", array_keys($errors[0])) . PHP_EOL;
    foreach ($errors as $row) {
      $content .= implode(",", array_values($row)) . PHP_EOL;
    }
    return static::downloadCSV($content, static::title . ' errors ' . $batchId, true);
  }

  public static function downloadCSV($content, $filename) {
    if (!file_exists('private://' . static::controller)) {
      mkdir('private://' . static::controller, 0777, true);
    }
    $destination = 'private://' . static::controller . '/' . $filename . '.csv';
    $file = file_save_data('', $destination, FILE_EXISTS_REPLACE);
    $file->setTemporary();
    $file->save();

    $uri = $file->getFileUri();

    file_put_contents($uri, $content);

    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    return $url;
  }

  public static function getFieldErrors($fields, $row) {
    $errors = [];
    foreach ($fields as $column => $field) {
      if (isset($field['required']) && $field['required']) {
        if (!isset($row[$column]) || $row[$column] === '') {
          $errors[] = $column . " column empty or not provided.";
        }
      }
      if (isset($field['options']) && count($field['options']) > 0) {
        if (!in_array($row[$column], $field['options'])) {
          $errors[] = $column . " must be one of: " . implode(" or ", $field['options']);
        }
      }
    }
    return $errors;
  }

  public static function addErrorRow($errors, $row) {
    $row = ['Error' => implode(" ", $errors)] + $row;
    $_SESSION[static::controller . 'errorRows'][] = $row;
    return $row;
  }
}
