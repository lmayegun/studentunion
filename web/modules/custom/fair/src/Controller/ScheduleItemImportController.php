<?php

namespace Drupal\fair\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fair\Entity\ScheduleItem;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements Class ScheduleItemImportController Controller.
 */
class ScheduleItemImportController extends ImportControllerBase
{
    const module = 'fair';
    const controller = 'ScheduleItemImportController';
    const title = 'Fair schedule item import';
    const batchTitle = 'Importing items...';
    const batchInitMessage = 'Starting import...';
    const importProcessedOne = 'One imported.';
    const importProcessedMany = '@count processed.';

    const fields = [
        'Name' => [
            'description' => 'Text only.',
            'database' => 'name',
            'required' => true,
            'unique' => true
        ],
        'Start' => [
            'description' => 'Date and time.',
            'required' => true,
        ],
        'End' => [
            'description' => 'Date and time.',
            'required' => true,
        ]
    ];
        
    public static function importRow($row, $fair, &$context)
    { 
        $context['sandbox']['current_item'] = $row;
        $errors = [];
        $fields = static::fields;
        $schedule_item = false; // Always create a new one

        $errors = parent::getFieldErrors($fields, $row);        

        foreach($fields AS $column => $field) {

        }

        if(count($errors) == 0) { 
            if(!$schedule_item) {
                $schedule_item = ScheduleItem::create([
                    'name' => substr(utf8_encode($row['Name']), 0, 50),
                    'field_fair_schedule_start' => strtotime($row['Start']),
                    'field_fair_schedule_end' => strtotime($row['End'])
                ]);
                $schedule_item->enforceIsNew();
            }      
            $schedule_item->field_fair_reference[] = ['target_id' => $fair->id()];

            $schedule_item->save();
            $message = "Importing ".$row['Name'] .' with ID '. $schedule_item->id();
        } else {            
            $row = parent::addErrorRow($errors, $row);
            $message = "Errors importing ".$row['Name'];
        }

        $context['results'][] = $row;
        $context['message'] = $message;
    }
}
