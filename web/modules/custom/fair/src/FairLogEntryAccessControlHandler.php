<?php

namespace Drupal\fair;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Fair log entry entity.
 *
 * @see \Drupal\fair\Entity\FairLogEntry.
 */
class FairLogEntryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\fair\Entity\FairLogEntryInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished fair log entry entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published fair log entry entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit fair log entry entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete fair log entry entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add fair log entry entities');
  }


}
