<?php

namespace Drupal\fair;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\fair\Entity\FairVideoInterface;

/**
 * Defines the storage handler class for Fair video entities.
 *
 * This extends the base storage class, adding required special handling for
 * Fair video entities.
 *
 * @ingroup fair
 */
interface FairVideoStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Fair video revision IDs for a specific Fair video.
   *
   * @param \Drupal\fair\Entity\FairVideoInterface $entity
   *   The Fair video entity.
   *
   * @return int[]
   *   Fair video revision IDs (in ascending order).
   */
  public function revisionIds(FairVideoInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Fair video author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Fair video revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\fair\Entity\FairVideoInterface $entity
   *   The Fair video entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(FairVideoInterface $entity);

  /**
   * Unsets the language for all Fair video with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
