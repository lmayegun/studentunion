<?php

namespace Drupal\fair;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Fair stall config entity.
 *
 * @see \Drupal\fair\Entity\FairStallType.
 */
class FairStallTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\fair\Entity\FairStallTypeInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view published fair stall entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'administer site configuration');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer site configuration');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer site configuration');
  }

}
