<?php

namespace Drupal\fair;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\fair\Entity\FairInterface;

/**
 * Defines the storage handler class for Fair entities.
 *
 * This extends the base storage class, adding required special handling for
 * Fair entities.
 *
 * @ingroup fair
 */
interface FairStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Fair revision IDs for a specific Fair.
   *
   * @param \Drupal\fair\Entity\FairInterface $entity
   *   The Fair entity.
   *
   * @return int[]
   *   Fair revision IDs (in ascending order).
   */
  public function revisionIds(FairInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Fair author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Fair revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\fair\Entity\FairInterface $entity
   *   The Fair entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(FairInterface $entity);

  /**
   * Unsets the language for all Fair with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
