<?php

namespace Drupal\fair;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\fair\Entity\FairVideoInterface;

/**
 * Defines the storage handler class for Fair video entities.
 *
 * This extends the base storage class, adding required special handling for
 * Fair video entities.
 *
 * @ingroup fair
 */
class FairVideoStorage extends SqlContentEntityStorage implements FairVideoStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(FairVideoInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {fair_video_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {fair_video_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(FairVideoInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {fair_video_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('fair_video_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
