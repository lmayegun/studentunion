<?php

namespace Drupal\fair;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for fair.
 */
class FairTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
