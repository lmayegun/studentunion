<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fair schedule item entities.
 *
 * @ingroup fair
 */
interface ScheduleItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair schedule item name.
   *
   * @return string
   *   Name of the Fair schedule item.
   */
  public function getName();

  /**
   * Sets the Fair schedule item name.
   *
   * @param string $name
   *   The Fair schedule item name.
   *
   * @return \Drupal\fair\Entity\ScheduleItemInterface
   *   The called Fair schedule item entity.
   */
  public function setName($name);

  /**
   * Gets the Fair schedule item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair schedule item.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair schedule item creation timestamp.
   *
   * @param int $timestamp
   *   The Fair schedule item creation timestamp.
   *
   * @return \Drupal\fair\Entity\ScheduleItemInterface
   *   The called Fair schedule item entity.
   */
  public function setCreatedTime($timestamp);

}
