<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Fair event entities.
 *
 * @ingroup fair
 */
interface FairEventInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair event name.
   *
   * @return string
   *   Name of the Fair event.
   */
  public function getName();

  /**
   * Sets the Fair event name.
   *
   * @param string $name
   *   The Fair event name.
   *
   * @return \Drupal\fair\Entity\FairEventInterface
   *   The called Fair event entity.
   */
  public function setName($name);

  /**
   * Gets the Fair event creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair event.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair event creation timestamp.
   *
   * @param int $timestamp
   *   The Fair event creation timestamp.
   *
   * @return \Drupal\fair\Entity\FairEventInterface
   *   The called Fair event entity.
   */
  public function setCreatedTime($timestamp);

}
