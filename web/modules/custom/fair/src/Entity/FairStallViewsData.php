<?php

namespace Drupal\fair\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Fair stall entities.
 */
class FairStallViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
