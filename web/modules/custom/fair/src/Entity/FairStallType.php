<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Fair stall type entity.
 *
 * @ConfigEntityType(
 *   id = "fair_stall_type",
 *   label = @Translation("Fair stall type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fair\FairStallTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\fair\Form\FairStallTypeForm",
 *       "edit" = "Drupal\fair\Form\FairStallTypeForm",
 *       "delete" = "Drupal\fair\Form\FairStallTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\fair\FairStallTypeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\fair\FairStallTypeAccessControlHandler",
 *   },
 *   config_prefix = "fair_stall_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "fair_stall",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/fair_stall_type/{fair_stall_type}",
 *     "add-form" = "/admin/structure/fair_stall_type/add",
 *     "edit-form" = "/admin/structure/fair_stall_type/{fair_stall_type}/edit",
 *     "delete-form" = "/admin/structure/fair_stall_type/{fair_stall_type}/delete",
 *     "collection" = "/admin/structure/fair_stall_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   }
 * )
 */
class FairStallType extends ConfigEntityBundleBase implements FairStallTypeInterface {

  /**
   * The Fair stall type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Fair stall type label.
   *
   * @var string
   */
  protected $label;

}
