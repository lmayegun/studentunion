<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fair video entities.
 *
 * @ingroup fair
 */
interface FairVideoInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair video name.
   *
   * @return string
   *   Name of the Fair video.
   */
  public function getName();

  /**
   * Sets the Fair video name.
   *
   * @param string $name
   *   The Fair video name.
   *
   * @return \Drupal\fair\Entity\FairVideoInterface
   *   The called Fair video entity.
   */
  public function setName($name);

  /**
   * Gets the Fair video creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair video.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair video creation timestamp.
   *
   * @param int $timestamp
   *   The Fair video creation timestamp.
   *
   * @return \Drupal\fair\Entity\FairVideoInterface
   *   The called Fair video entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Fair video revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Fair video revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\fair\Entity\FairVideoInterface
   *   The called Fair video entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Fair video revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Fair video revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\fair\Entity\FairVideoInterface
   *   The called Fair video entity.
   */
  public function setRevisionUserId($uid);

}
