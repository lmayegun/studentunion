<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fair stall entities.
 *
 * @ingroup fair
 */
interface FairStallInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair stall name.
   *
   * @return string
   *   Name of the Fair stall.
   */
  public function getName();

  /**
   * Sets the Fair stall name.
   *
   * @param string $name
   *   The Fair stall name.
   *
   * @return \Drupal\fair\Entity\FairStallInterface
   *   The called Fair stall entity.
   */
  public function setName($name);

  /**
   * Gets the Fair stall creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair stall.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair stall creation timestamp.
   *
   * @param int $timestamp
   *   The Fair stall creation timestamp.
   *
   * @return \Drupal\fair\Entity\FairStallInterface
   *   The called Fair stall entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Fair stall revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Fair stall revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\fair\Entity\FairStallInterface
   *   The called Fair stall entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Fair stall revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Fair stall revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\fair\Entity\FairStallInterface
   *   The called Fair stall entity.
   */
  public function setRevisionUserId($uid);

}
