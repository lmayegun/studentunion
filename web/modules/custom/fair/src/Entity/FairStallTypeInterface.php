<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Fair stall type entities.
 */
interface FairStallTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
