<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fair log entry entities.
 *
 * @ingroup fair
 */
interface FairLogEntryInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair log entry name.
   *
   * @return string
   *   Name of the Fair log entry.
   */
  public function getName();

  /**
   * Sets the Fair log entry name.
   *
   * @param string $name
   *   The Fair log entry name.
   *
   * @return \Drupal\fair\Entity\FairLogEntryInterface
   *   The called Fair log entry entity.
   */
  public function setName($name);

  /**
   * Gets the Fair log entry creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair log entry.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair log entry creation timestamp.
   *
   * @param int $timestamp
   *   The Fair log entry creation timestamp.
   *
   * @return \Drupal\fair\Entity\FairLogEntryInterface
   *   The called Fair log entry entity.
   */
  public function setCreatedTime($timestamp);

}
