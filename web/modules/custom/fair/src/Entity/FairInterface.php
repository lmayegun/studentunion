<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fair entities.
 *
 * @ingroup fair
 */
interface FairInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair name.
   *
   * @return string
   *   Name of the Fair.
   */
  public function getName();

  /**
   * Sets the Fair name.
   *
   * @param string $name
   *   The Fair name.
   *
   * @return \Drupal\fair\Entity\FairInterface
   *   The called Fair entity.
   */
  public function setName($name);

  /**
   * Gets the Fair creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair creation timestamp.
   *
   * @param int $timestamp
   *   The Fair creation timestamp.
   *
   * @return \Drupal\fair\Entity\FairInterface
   *   The called Fair entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Fair revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Fair revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\fair\Entity\FairInterface
   *   The called Fair entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Fair revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Fair revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\fair\Entity\FairInterface
   *   The called Fair entity.
   */
  public function setRevisionUserId($uid);

}
