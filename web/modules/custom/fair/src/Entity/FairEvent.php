<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Fair event entity.
 *
 * @ingroup fair
 *
 * @ContentEntityType(
 *   id = "fair_event",
 *   label = @Translation("Fair event"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fair\FairEventListBuilder",
 *     "views_data" = "Drupal\fair\Entity\FairEventViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\fair\Form\FairEventForm",
 *       "add" = "Drupal\fair\Form\FairEventForm",
 *       "edit" = "Drupal\fair\Form\FairEventForm",
 *       "delete" = "Drupal\fair\Form\FairEventDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\fair\FairEventHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\fair\FairEventAccessControlHandler",
 *   },
 *   base_table = "fair_event",
 *   translatable = FALSE,
 *   admin_permission = "administer fair event entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/fair_event/{fair_event}",
 *     "add-form" = "/admin/content/fair_event/add",
 *     "edit-form" = "/admin/content/fair_event/{fair_event}/edit",
 *     "delete-form" = "/admin/content/fair_event/{fair_event}/delete",
 *     "collection" = "/admin/content/fair_event",
 *   },
 *   field_ui_base_route = "fair_event.settings"
 * )
 */
class FairEvent extends ContentEntityBase implements FairEventInterface
{

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name)
  {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime()
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp)
  {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Event name / brief description'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }
}
