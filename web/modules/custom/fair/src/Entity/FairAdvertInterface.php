<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fair advert entities.
 *
 * @ingroup fair
 */
interface FairAdvertInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Fair advert name.
   *
   * @return string
   *   Name of the Fair advert.
   */
  public function getName();

  /**
   * Sets the Fair advert name.
   *
   * @param string $name
   *   The Fair advert name.
   *
   * @return \Drupal\fair\Entity\FairAdvertInterface
   *   The called Fair advert entity.
   */
  public function setName($name);

  /**
   * Gets the Fair advert creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Fair advert.
   */
  public function getCreatedTime();

  /**
   * Sets the Fair advert creation timestamp.
   *
   * @param int $timestamp
   *   The Fair advert creation timestamp.
   *
   * @return \Drupal\fair\Entity\FairAdvertInterface
   *   The called Fair advert entity.
   */
  public function setCreatedTime($timestamp);

}
