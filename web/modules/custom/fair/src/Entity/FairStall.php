<?php

namespace Drupal\fair\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Fair stall entity.
 *
 * @ingroup fair
 *
 * @ContentEntityType(
 *   id = "fair_stall",
 *   label = @Translation("Fair stall"),
 *   bundle_label = @Translation("Fair stall type"),
 *   handlers = {
 *     "storage" = "Drupal\fair\FairStallStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fair\FairStallListBuilder",
 *     "views_data" = "Drupal\fair\Entity\FairStallViewsData",
 *     "translation" = "Drupal\fair\FairStallTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\fair\Form\FairStallForm",
 *       "add" = "Drupal\fair\Form\FairStallForm",
 *       "edit" = "Drupal\fair\Form\FairStallForm",
 *       "delete" = "Drupal\fair\Form\FairStallDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\fair\FairStallHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\fair\FairStallAccessControlHandler",
 *   },
 *   base_table = "fair_stall",
 *   data_table = "fair_stall_field_data",
 *   revision_table = "fair_stall_revision",
 *   revision_data_table = "fair_stall_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer fair stall entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/fair/stall/{fair_stall}",
 *     "add-page" = "/admin/content/fair/stall/add",
 *     "add-form" = "/admin/content/fair/stall/add/{fair_stall_type}",
 *     "edit-form" = "/admin/content/fair/stall/{fair_stall}/edit",
 *     "delete-form" = "/admin/content/fair/stall/{fair_stall}/delete",
 *     "version-history" = "/admin/content/fair/stall/{fair_stall}/revisions",
 *     "revision" =
 *     "/admin/content/fair/stall/{fair_stall}/revisions/{fair_stall_revision}/view",
 *     "revision_revert" =
 *     "/admin/content/fair/stall/{fair_stall}/revisions/{fair_stall_revision}/revert",
 *     "revision_delete" =
 *     "/admin/content/fair/stall/{fair_stall}/revisions/{fair_stall_revision}/delete",
 *     "translation_revert" =
 *     "/admin/content/fair/stall/{fair_stall}/revisions/{fair_stall_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/fair/stall",
 *   },
 *   bundle_entity_type = "fair_stall_type",
 *   field_ui_base_route = "entity.fair_stall_type.edit_form"
 * )
 */
class FairStall extends EditorialContentEntityBase implements FairStallInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    } elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the fair_stall owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Fair stall entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',

        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Stall name, e.g. the name of the organisation or service.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length'      => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label'  => 'above',
        'type'   => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    /**
     * Start custom fields
     * Tagline
     * Card image
     * Category
     * Tags
     * Fair reference
     * Managers
     */

    $fields['fair_stall_tagline'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tagline'))
      ->setDescription(t('A short catchy sentence describing the stall, e.g. a slogan.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type'  => 'string',
      ])
      ->setDisplayOptions('form', [
        'type'   => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['fair_stall_card'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Card image'))
      ->setSettings([
        'alt_field_required' => FALSE,
        'max_filesize'       => '700 KB',
        'file_extensions'    => 'png jpg jpeg',
        'min_resolution'     => '215x350',
      ])
      ->setDisplayOptions('view', [
        'type'     => 'image',
        'weight'   => 5,
        'label'    => 'hidden',
        'settings' => [
          'image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('form', [
        'label'  => 'hidden',
        'type'   => 'image_image',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    // ->setRequired(TRUE);

    $fields['fair_stall_category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel((t('Category')))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting(
        'handler_settings',
        [
          'target_bundles' => [
            'fair_stall_category' => 'fair_stall_category',
          ],
        ]
      )
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 13,
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '10',
          'autocomplete_type' => 'tags',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['fair_stall_tags'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel((t('Tags')))
      ->setDescription(t('Select up to ten (10) tags.'))
      ->setSetting('target_type', 'taxonomy_term')
      // ->setSetting('handler', 'views')
      // ->setSetting('handler_settings', [
      //   'view_name' => 'fairs_stalls_tag_selection_',
      //   'display_name' => 'entity_reference_1',
      //   'arguments' => [],
      // ])
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting(
        'handler_settings',
        [
          'target_bundles' => [
            'fair_stall_tags' => 'fair_stall_tags',
          ],
        ]
      )
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 13,
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '10',
          'autocomplete_type' => 'tags',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(10);

    $fields['fair_stall_reference'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Fair(s) the stall is part of'))
      ->setSetting('target_type', 'fair')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '200',
          'autocomplete_type' => 'tags',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fair_stall_managers'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Managers'))
      ->setSetting('target_type', 'user')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '200',
          'autocomplete_type' => 'tags',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    /**
     * End custom fields
     */

    $fields['status']->setDescription(t('Whether the stall is published.'))
      ->setDisplayOptions('form', [
        'type'   => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the stall was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the stall was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }
}
