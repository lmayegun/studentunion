<?php

namespace Drupal\fair\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Fair event entities.
 */
class FairEventViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
