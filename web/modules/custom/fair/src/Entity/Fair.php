<?php

namespace Drupal\fair\Entity;

use DateTime;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\user\UserInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Defines the Fair entity.
 *
 * @ingroup fair
 *
 * @ContentEntityType(
 *   id = "fair",
 *   label = @Translation("Fair"),
 *   handlers = {
 *     "storage" = "Drupal\fair\FairStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fair\FairListBuilder",
 *     "views_data" = "Drupal\fair\Entity\FairViewsData",
 *     "translation" = "Drupal\fair\FairTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\fair\Form\FairForm",
 *       "add" = "Drupal\fair\Form\FairForm",
 *       "edit" = "Drupal\fair\Form\FairForm",
 *       "delete" = "Drupal\fair\Form\FairDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\fair\FairHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\fair\FairAccessControlHandler",
 *   },
 *   base_table = "fair",
 *   data_table = "fair_field_data",
 *   revision_table = "fair_revision",
 *   revision_data_table = "fair_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   translatable = TRUE,
 *   admin_permission = "administer fair entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/fair/{fair}",
 *     "add-form" = "/admin/content/fair/add",
 *     "edit-form" = "/admin/content/fair/{fair}/edit",
 *     "delete-form" = "/admin/content/fair/{fair}/delete",
 *     "version-history" = "/admin/content/fair/{fair}/revisions",
 *     "revision" =
 *     "/admin/content/fair/{fair}/revisions/{fair_revision}/view",
 *     "revision_revert" =
 *     "/admin/content/fair/{fair}/revisions/{fair_revision}/revert",
 *     "revision_delete" =
 *     "/admin/content/fair/{fair}/revisions/{fair_revision}/delete",
 *     "translation_revert" =
 *     "/admin/content/fair/{fair}/revisions/{fair_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/fair",
 *   },
 *   field_ui_base_route = "fair.settings"
 * )
 */
class Fair extends EditorialContentEntityBase implements FairInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    } elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the fair owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Fair entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the fair.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length'      => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Fair is published.'))
      ->setDisplayOptions('form', [
        'type'   => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_with_summary')
      ->setLabel(t('Description of fair'))
      ->setDescription(t('e.g. on list of fairs'))
      ->setDisplayOptions('view', [
        'label'  => 'visible',
        'type'   => 'text_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'text_textarea',
        'weight' => 6,
        'rows'   => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);


    return $fields;
  }


  public function setFairVariables(&$variables) {
    // Drupal path
    $drupalPath = \Drupal::service('path.current')->getPath();
    $variables['drupalPath'] = $drupalPath;

    // Get client id from UCL API
    $config = \Drupal::config('su_login_oauth2.adminsettings');
    $variables['clientId'] = $config->get('client_id');

    // Load user data
    $uid = \Drupal::currentUser()->id();
    $account = User::load($uid);
    if ($account) {
      $variables = array_merge($this->getFairVariablesUser($account), $variables);
    } else {
      $variables['userDetails'] = [];
    }

    // Load various data, all cached within itself
    $variables = array_merge($variables, $this->getFairData());
    $variables = array_merge($variables, $this->getCategories());
    $variables = array_merge($variables, $this->getAdverts());
    $variables = array_merge($variables, $this->getVideos());

    $variables['stalls'] = [];
    $variables['stalls'] += $this->getStalls();
    $variables['stalls'] += $this->getGroups();
    $variables['stalls'] += $this->getCandidates();

    // Calculate tags
    $tags = collapseArrayField($variables['stalls'], 'fair_stall_tags');
    if (!$tags) {
      $tags = [];
    }
    shuffle($tags);
    $variables['stallsInfo'] = [
      'categoriesInUse' => collapseArrayField($variables['stalls'], 'fair_stall_category'),
      'tagsInUse' => $tags
    ];

    // Helpful $content variable for templates.
    foreach (Element::children($variables['elements']) as $key) {
      $variables['content'][$key] = $variables['elements'][$key];
    }
  }

  public function getCategories() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $variables = [];
    $variables['fairCategories'] = [];
    $vid = 'fair_stall_category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $variables['fairCategories'][] = array(
        'id' => $term->tid,
        'name' => $term->name,
        'description' => $term->description__value,
      );
    }

    \Drupal::cache()->set($cid, $variables, Cache::PERMANENT, ['taxonomy_term_list:' . $vid]);

    return $variables;
  }

  public function getAdverts() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $variables = [];
    $variables['adverts'] = [];

    $entity_storage = \Drupal::entityTypeManager()->getStorage('fair_advert')->loadMultiple();
    foreach ($entity_storage as $ad) {
      if ($ad->isPublished()) {
        $variables['adverts'][] = fair_page_convert_entity_for_json($ad);
      }
    }

    \Drupal::cache()->set($cid, $variables, Cache::PERMANENT, ['fair_advert_list']);

    return $variables;
  }

  public function getFairData() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      // return $cache->data;
    }

    $variables = [];

    $variables['fairId'] = $this->id();
    $variables['fairData'] = fair_page_convert_entity_for_json($this);

    if (count($this->getCandidateList()) > 0) {
      $postSorting = [];
      $term_ids = \Drupal::entityQuery('taxonomy_term')
        ->condition('status', 1)
        ->condition('vid', 'election_post_categories')
        ->execute();
      $terms = Term::loadMultiple($term_ids);
      foreach ($terms as $term) {
        $postSorting[$term->getWeight()] = $term->label();
      }
      ksort($postSorting);

      $variables['fairData']['field_fair_ranking_reference'] = array_merge($variables['fairData']['field_fair_ranking_reference'], $postSorting);
    }

    \Drupal::cache()->set($cid, $variables, Cache::PERMANENT, $this->getCacheTags());

    return $variables;
  }

  public function getVideos() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $variables = [];
    $variables['videos'] = [];

    $query = \Drupal::entityQuery('fair_video')
      ->condition('status', 1)
      ->condition('field_fair_reference.target_id', $this->id());
    $ids = $query->execute();
    $entity_storage = \Drupal::entityTypeManager()->getStorage('fair_video')->loadMultiple($ids);
    foreach ($entity_storage as $entity) {
      $entityAsArray = fair_page_convert_entity_for_json($entity);
      $entityAsArray['entity_type'] = 'fair_video';
      $entityAsArray['group'] = 'Videos';
      $variables['videos'][$entity->id()] = $entityAsArray;
    }

    $variables['videosInfo'] = [
      'categoriesInUse' => [], // collapseArrayField($variables['videos'], 'fair_stall_category'))
      'tagsInUse' => [] // shuffle(collapseArrayField($variables['videos'], 'fair_stall_tags'))
    ];

    \Drupal::cache()->set($cid, $variables, Cache::PERMANENT, ['fair_video_list']);

    return $variables;
  }

  public function getStallList() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $query = \Drupal::entityQuery('fair_stall')
      ->condition('status', 1)
      ->condition('fair_stall_reference.target_id', $this->id());
    $ids = $query->execute();

    \Drupal::cache()->set($cid, $ids, Cache::PERMANENT, ['fair_stall_list']);

    return $ids;
  }

  public function getStall($id) {
    $cid = __METHOD__ . $id;
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $entity = FairStall::load($id);
    $entityAsArray = fair_page_convert_entity_for_json($entity);
    $entityAsArray['entity_type'] = 'fair_stall';
    $entityAsArray['group'] = 'Stalls';

    \Drupal::cache()->set($cid, $entityAsArray, Cache::PERMANENT, $entity->getCacheTags());

    return $entityAsArray;
  }

  public function getStalls() {
    $stalls = [];

    $ids = $this->getStallList();
    foreach ($ids as $id) {
      $stalls[$id] = $this->getStall($id);
    }

    // This is cached in both of the sub functions so no need to cache here
    return $stalls;
  }


  public function getCandidates() {
    $stalls = [];

    $ids = $this->getCandidateList();
    foreach ($ids as $id) {
      $stalls['c' . $id] = $this->getCandidate($id);
    }

    // This is cached in both of the sub functions so no need to cache here
    return $stalls;
  }

  public function getGroups() {
    $stalls = [];

    $ids = $this->getGroupList();
    foreach ($ids as $id) {
      $stalls['g' . $id] = $this->getGroup($id);
    }

    // This is cached in both of the sub functions so no need to cache here
    return $stalls;
  }

  public function getElectionIds() {
    $elections = $this->field_fair_election->referencedEntities();
    $ids = [];
    foreach ($elections as $election) {
      $ids[] = $election->id();
    }
    return $ids;
  }

  public function getCandidateList() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $election_ids = $this->getElectionIds();
    if (count($election_ids) == 0) {
      return [];
    }

    $query = \Drupal::entityQuery('election_post')
      ->condition('status', 1)
      ->condition('election', $election_ids, 'IN')
      ->condition('type', 'club_society', '!=');
    $post_ids = $query->execute();
    // dd($post_ids);

    $query = \Drupal::entityQuery('election_candidate');
    if (!\Drupal::currentUser()->hasPermission('view unpublished election candidate entities')) {
      $query->condition('status', 1);
    }
    $query->condition('candidate_status', 'hopeful')
      ->condition('election_post', $post_ids, 'IN');
    $ids = $query->execute();

    \Drupal::cache()->set($cid, $ids, Cache::PERMANENT, ['election_candidate_list']);
    return $ids;
  }

  public function getGroupList() {
    $cid = __METHOD__ . $this->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $query = \Drupal::entityQuery('group')
      ->condition('status', 1)
      ->condition('field_fairs.target_id', $this->id());
    $ids = $query->execute();

    \Drupal::cache()->set($cid, $ids, Cache::PERMANENT, ['group_list']);
    return $ids;
  }

  public function getCandidate($id) {
    $cid = __METHOD__ . $id;
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $entity = ElectionCandidate::load($id);
    $entityAsArray = fair_page_convert_entity_for_json($entity);
    $entityAsArray['entity_type'] = 'election_candidate';
    $entityAsArray['group'] = 'Stalls';
    $entityAsArray['type'] = 'election_candidate';
    $entityAsArray['election_candidate_id'] = $entityAsArray['id'];
    $entityAsArray['id'] = 'c' . $entityAsArray['id'];
    $entityAsArray['field_enable_joining'] = "1";
    $entityAsArray['field_fair_stall_qanda_enabled'] = "1";

    $url = Url::fromRoute('entity.election_candidate.edit_form', ['election_candidate' => $id]);
    $entityAsArray['edit_link'] = $url->toString();

    $url = Url::fromRoute('entity.election_post.voting', ['election_post' => $entity->getElectionPost()->id()]);
    $entityAsArray['field_fair_stall_to_action_url'] = [['url' => $url->toString(), 'title' => 'Vote now!']];

    $entityAsArray['fair_stall_category'] = $entity->getElectionPost()->label();

    $cardValues = su_views_display_convert_entity_to_card_values($entity);
    $entityAsArray['fair_stall_card'] = isset($cardValues['imageUrl']) && $cardValues['imageUrl'] && $cardValues['imageUrl'] != '/' ? [$cardValues['imageUrl']] : $entityAsArray['fair_stall_card'];

    \Drupal::cache()->set($cid, $entityAsArray, Cache::PERMANENT, Cache::mergeTags($entity->getCacheTags(), ['election_candidate:' . $id]));

    return $entityAsArray;
  }

  public function getGroup($id) {
    $cid = __METHOD__ . $id;
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $entity = Group::load($id);
    $entityAsArray = fair_page_convert_entity_for_json($entity);
    $entityAsArray['entity_type'] = 'group';
    $entityAsArray['group'] = 'Stalls';
    $entityAsArray['type'] = $entity->bundle() == 'club_society' ? 'club_society' : 'general';
    $entityAsArray['group_id'] = $entityAsArray['id'];
    $entityAsArray['id'] = 'g' . $entityAsArray['id'];
    $entityAsArray['field_enable_joining'] = "1";
    $entityAsArray['field_fair_stall_qanda_enabled'] = "1";

    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $roles = ['club_society-president', 'club_society-treasurer', 'volunteering_org-leader'];
    $entityAsArray['fair_stall_managers'] = [];
    foreach ($roles as $role) {
      $records = $gmrRepositoryService->get(NULL, $entity, GroupRole::load($role), NULL, new DateTime(), TRUE);
      foreach ($records as $record) {
        $entityAsArray['fair_stall_managers'][] = $record->getUser()->id();
      }
    }

    if ($entity->bundle() == 'volunteering_org') {
      $entityAsArray['field_fair_stall_to_action_url'] = [['url' => "https://studentsunionucl.org/group/" . $entityAsArray['group_id'], 'title' => 'View our opportunities!']];
      $entityAsArray['field_fair_stall_website'] =
        "https://studentsunionucl.org/group/" . $entityAsArray['group_id']; // ['url' => "https://studentsunionucl.org/group/" . $entityAsArray['group_id'], 'title' => ''];
      $entityAsArray['field_fair_stall_mailing_list_on'] = "1";
    }

    // Membership products:
    if ($entity->bundle() == 'club_society') {
      $entityAsArray['products'] = [];
      // 'type', 'variation_id', 'price'
      $product = $entity->field_product->entity;
      if ($product) {
        $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
        $variations = $product->getVariations();
        foreach ($variations as $variation) {
          if (!$variation->isActive()) {
            continue;
          }
          $price = $variation->getPrice();

          $entityAsArray['products'][] = [
            'type' => $variation->attribute_club_society_membershi->entity->label(),
            'variation_id' => $variation->id(),
            'price' => $currency_formatter->format($price->getNumber(), $price->getCurrencyCode()),
          ];
        }
      }
    }

    $cardValues = su_views_display_convert_entity_to_card_values($entity);
    $entityAsArray['fair_stall_card'] = isset($cardValues['imageUrl']) && $cardValues['imageUrl'] && $cardValues['imageUrl'] != '/' ? [$cardValues['imageUrl']] : $entityAsArray['fair_stall_card'];

    \Drupal::cache()->set($cid, $entityAsArray, Cache::PERMANENT, Cache::mergeTags($entity->getCacheTags(), ['group_membership_record:group_roles:' . $id]));

    return $entityAsArray;
  }

  public function getFairVariablesUser($account) {
    if (!$account) {
      return [];
    }
    $cid = __METHOD__ . $account->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $variables = [];

    $profile = \Drupal::entityTypeManager()
      ->getStorage('profile')
      ->loadByUser($account, 'fairs');

    $favs = [];

    if (!empty($profile)) {
      $favourites = $profile->field_fair_stalls_reference;
      foreach ($favourites as $favourite) {
        array_push($favs, $favourite->entity->id());
      }
      $favourites = $profile->field_fair_groups_reference;
      foreach ($favourites as $favourite) {
        array_push($favs, 'g' . $favourite->entity->id());
      }
    }

    $uclEmail = "ucl.ac.uk";
    $domain = explode('@', $account->getEmail());
    $ucl_mail = FALSE;
    if (isset($domain[1])) {
      $ucl_mail = $uclEmail === $domain[1] ?: $ucl_mail;
    }

    $picture = !$account->user_picture->isEmpty() ? \Drupal\image\Entity\ImageStyle::load('thumbnail')->buildUrl($account->user_picture->entity->getFileUri()) : '';

    $variables = array_merge($variables, $this->getUserJoined($account));

    $variables['userDetails'] = [
      'uid' => $account->id(),
      'mail' => $account->getEmail(),
      'ucl_mail' => $ucl_mail,
      'user_picture' => $picture,
      'field_first_name' => $account->field_first_name->getString(),
      'field_last_name' => $account->field_last_name->getString(),
      'fair_admin' => $account->hasRole('fairs_administrator') || $account->hasRole('administrator'),
      'user_student' => $account->hasRole('student'),
      'stalls_joined' => [],
      'stalls_favourited' => $favs,
    ];

    // Cache
    $cacheTags = $account->getCacheTags();
    if ($profile) {
      $cacheTags = Cache::mergeTags($cacheTags, $profile->getCacheTags());
    }
    \Drupal::cache()->set($cid, $variables, Cache::PERMANENT, $cacheTags);

    return $variables;
  }

  public function getUserJoined($account) {
    $cid = __METHOD__ . $account->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $variables = [];
    $variables['stalls'] = [];
    $query = \Drupal::entityQuery('fair_stall')
      ->condition('status', 1)
      ->condition('fair_stall_reference.target_id', $this->id())
      ->condition('field_users_joined.target_id', $account->id());
    $ids = $query->execute();

    foreach ($ids as $id) {
      $variables['userDetails']['stalls_joined'][] = $id;
    }

    \Drupal::cache()->set($cid, $variables, Cache::PERMANENT, ['fair_stall:joined_for_user:' . $account->id()]);

    return $variables;
  }
}
