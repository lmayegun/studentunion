<?php

namespace Drupal\fair;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Fair video entity.
 *
 * @see \Drupal\fair\Entity\FairVideo.
 */
class FairVideoAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\fair\Entity\FairVideoInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished fair video entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published fair video entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit fair video entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete fair video entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add fair video entities');
  }


}
