<?php

namespace Drupal\fair;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\fair\Entity\FairStallInterface;

/**
 * Defines the storage handler class for Fair stall entities.
 *
 * This extends the base storage class, adding required special handling for
 * Fair stall entities.
 *
 * @ingroup fair
 */
class FairStallStorage extends SqlContentEntityStorage implements FairStallStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(FairStallInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {fair_stall_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {fair_stall_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(FairStallInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {fair_stall_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('fair_stall_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
