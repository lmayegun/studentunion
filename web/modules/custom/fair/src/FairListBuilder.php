<?php

namespace Drupal\fair;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Fair entities.
 *
 * @ingroup fair
 */
class FairListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Fair ID');
    $header['name'] = $this->t('Fair name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\fair\Entity\Fair $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.fair.canonical',
      ['fair' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
