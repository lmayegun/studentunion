<?php

namespace Drupal\fair\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

class FairProcessor implements InboundPathProcessorInterface {


  /**
   * Processes the inbound path.
   *
   * Implementations may make changes to the request object passed in but should
   * avoid all other side effects. This method can be called to process requests
   * other than the current request.
   *
   * @param string                                    $path
   *   The path to process, with a leading slash.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HttpRequest object representing the request to process. Note, if this
   *   method is being called via the path_processor_manager service and is not
   *   part of routing, the current request object must be cloned before being
   *   passed in.
   *
   * @return string
   *   The processed path.
   */
  public function processInbound($path, Request $request) {

    if (strpos($path, '/fair/') === 0) {
      $names = preg_replace('|^\/fair\/|', '', $path);
      $names = str_replace('/', ':', $names);
      watchdog_exception('fair', $names);
      return "/fair/$names";
    }

    return $path;
  }

}
