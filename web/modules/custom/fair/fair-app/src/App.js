import React, { useState, useEffect, useRef, useCallback } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import moment from "moment";

import {
  fetchUserToken,
  fetchUserStallActions,
  fetchScheduleItems,
  fetchStallComments,
  fetchStallsLive,
  // fetchVideos,
  fetchStallEvents,
  fetchStallProducts,
  postStallFavourite,
  postAddToCart,
  postStallComment,
  postStallDeleteComment,
  postStallJoin,
  postStallSubscribe,
  syncJoinFavWithDB,
} from "./utils/ApiCalls";
import { useStateWithStorage } from "./utils/Hooks";
import { PageView, initGA, Event } from "./utils/Tracking";
import { theme } from "./utils/Theme";

/* Material-UI theming */
import { ThemeProvider } from "@material-ui/core/styles";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";

/* Our UI components */
import Debug from "./components/debug/Debug";
import TestingToolbar from "./components/debug/TestingToolbar";
import Banner from "./components/banner/Banner";
import SecondaryHeader from "./components/header/SecondaryHeader";
import Stall from "./components/stall/Stall/Stall";
import {
  getRandom,
  getMostSimilarStall,
} from "./components/directory/search/SearchFunctions";

/* Pages */
import StallsPage from "./components/pages/StallsPage";
import StagePage from "./components/pages/StagePage";
import SavedPage from "./components/pages/SavedPage";
import TabsBar from "./components/banner/TabsBar";
import HelloDialog from "./components/dialogs/HelloDialog";

import { runSearch } from "./components/directory/search/SearchFunctions";
import { Grow, CircularProgress, Container } from "@material-ui/core";
import ShuffleIcon from "@material-ui/icons/Shuffle";
import GroupIcon from "@material-ui/icons/Group";
import CompareIcon from "@material-ui/icons/Compare";
import EventsPage from "./components/pages/EventsPage";

const preloadImages = (filenames) => {
  filenames.forEach((path) => {
    if (path && path.length > 0) {
      new Image().src = path;
    }
  });
};

const getBaseUrl = () => {
  var baseUrl = "/";
  if (document.getElementById("root").getAttribute("fair-id")) {
    baseUrl =
      "/fair/" + document.getElementById("root").getAttribute("fair-id");
  }
  return baseUrl;
};

export default function App() {
  // Google analytics
  initGA("UA-829873-1");
  // PageView();

  // Defining state for the app
  const [userToken, setUserToken] = useState(false);
  const [userIsStallManager, setUserIsStallManager] = useState(false);
  const [stallUserActions, setStallUserActions] = useState({});
  const [currentStallComments, setCurrentStallComments] = useState([]);

  // Deal with path and parameters
  const searchParams = new URLSearchParams(window.location.search);
  const testing = searchParams.has("testing");
  const paramsTimeMode = searchParams.has("timeMode")
    ? searchParams.get("timeMode")
    : "present"; // TODO change to working this out based on fair start time

  const [timeMode, setTimeMode] = useState(paramsTimeMode);

  const [events, setEvents] = useState({});

  // Data from path
  const [fairId, setFairId] = useState(
    document.getElementById("root").getAttribute("fair-id")
      ? document.getElementById("root").getAttribute("fair-id")
      : 1
  );
  const [drupalPath, setDrupalPath] = useState(
    document.getElementById("root").getAttribute("fair-id")
      ? document.getElementById("root").getAttribute("drupal-path")
      : "/"
  );
  const [loadedFromStall, setLoadedFromStall] = useState(false);
  const [baseUrl, setBaseUrl] = useState(getBaseUrl(fairId));
  const [originaUrl, setOriginaUrl] = useState(window.location.pathname);

  // Data from API
  const [stalls, setStalls] = useState(window.stalls);
  const [stallsFullData, setStallsFullData] = useState([]);
  const [videos, setVideos] = useState([]);
  const [scheduleItems, setScheduleItems] = useState([]);

  // user generated data
  const [saved, setSaved] = useStateWithStorage([], "saved" + fairId);
  const [cardHistory, setCardHistory] = useStateWithStorage(
    [],
    "cardHistory" + fairId
  );

  // Track whatever stall is open in a dialog
  const [currentStallData, setCurrentStallData] = useState(null);
  const [currentStallEvents, setCurrentStallEvents] = useState([]);
  const [currentStallProducts, setCurrentStallProducts] = useState([]);
  const [
    currentStallSuggestedStalls,
    setCurrentStallSuggestedStalls,
  ] = useState([]);
  const [currentStallAdvert, setCurrentStallAdvert] = useState(null);

  const [currentVideoData, setCurrentVideoData] = useState(null);

  // Track filters in directories
  const [directoryFiltersStalls, setDirectoryFiltersStalls] = useState({
    textSearch: "",
    categories: [],
    tags: [],
  });
  const [directoryFiltersVideos, setDirectoryFiltersVideos] = useState({
    textSearch: "",
  });
  const [directoryFiltersEvents, setDirectoryFiltersEvents] = useState({
    textSearch: "",
  });
  const [
    directoryCurrentCategoriesStalls,
    setDirectoryCurrentCategoriesStalls,
  ] = useState([]);
  const [
    directoryCurrentCategoriesVideos,
    setDirectoryCurrentCategoriesVideos,
  ] = useState([]);
  const [
    directoryCurrentCategoriesEvents,
    setDirectoryCurrentCategoriesEvents,
  ] = useState([]);

  const [helloDialogOpen, setHelloDialogOpen] = useStateWithStorage(
    true,
    "helloDialog" + fairId,
    testing
  );
  const [cardLoading, setCardLoading] = useState(null);

  const [stageScheduleItems, setStageScheduleItems] = useState([]);

  /**
   * One function to route API save requests
   *
   * Kept as one function to make it simpler to pass as a prop to subcomponents
   *
   * See the switch for the options
   *
   * @param {string} action
   * @param {object} params
   */
  const saveDataViaAPI = useCallback(
    async (action, params) => {
      //console.log("saveDataViaAPI", action, params);

      let success = false;

      switch (action) {
        case "add_to_cart":
          success = await postAddToCart(
            window.userToken,
            window.stallUserActions,
            params.variation_id,
          );
          if (success) {
            const inCart = true;
            window.stallUserActions.join.in_cart = inCart;
            setStallUserActions(window.stallUserActions);
            syncJoinFavWithDB(
              currentStallData.id,
              window.stallUserActions.join.in_cart,
              window.userDetails.stalls_joined
            );
          }
          return success;

        case "favourite":
          success = await postStallFavourite(
            window.userToken,
            window.stallUserActions,
          );

          if (success) {
            const stallId = currentStallData.id;
            const favourites = window.userDetails.stalls_favourited;
            window.stallUserActions.favourite.favourited = !window
              .stallUserActions.favourite.favourited;
            setStallUserActions(window.stallUserActions);
            syncJoinFavWithDB(
              stallId,
              window.stallUserActions.favourite.favourited,
              favourites
            );
          }
          return success;

        case "comment":
          success = await postStallComment(
            window.userToken,
            window.stallUserActions,
            params.comment,
            params.parentUuid,
            params.category ? params.category : "",
            params.user_role ? params.user_role : ""
          );
          return success;

        case "delete comment":
          const stallId = currentStallData.id;
          success = await postStallDeleteComment(
            stallId,
            params.commentId
          );
          return success;

        case "join":
          success = await postStallJoin(
            currentStallData.id,
            window.userToken,
            window.stallUserActions
          );
          if (success) {
            const joined = (success.result == "joined");
            window.stallUserActions.join.joined = joined;
            setStallUserActions(window.stallUserActions);
            syncJoinFavWithDB(
              currentStallData.id,
              window.stallUserActions.join.joined,
              window.userDetails.stalls_joined
            );
          }

          //console.log(
          //   "window.userDetails.stalls_joined",
          //   window.userDetails.stalls_joined
          // );

          return success;

        case "subscribe":
          success = await postStallSubscribe(currentStallData.id, params.email);
          if (success) {
            window.stallUserActions.subscribe.subscribed =
              success.result == "added";
            setStallUserActions(window.stallUserActions);
          }
          return success;

        default:
          break;
      }
    },
    [currentStallData, stallUserActions]
  );

  // Run once on page load
  useEffect(() => {
    setDirectoryCurrentCategoriesStalls(
      runSearch(
        "stalls",
        window.stalls,
        directoryFiltersStalls,
        'random',
        window.fairData && window.fairData.field_fair_ranking_reference
          ? window.fairData.field_fair_ranking_reference
          : []
      )
    );

    setDirectoryCurrentCategoriesVideos(
      runSearch("videos", window.videos, directoryFiltersVideos, 'random', [])
    );

    const newEvents = getEventsFromStalls(window.stalls);
    setEvents(newEvents);
    setDirectoryCurrentCategoriesEvents(
      runSearch("events", newEvents, directoryFiltersEvents, 'random', [])
    );

    // Timer to load live info every so often
    // const timer = setInterval(() => {
    //   fetchStallsLive(fairId).then((response) => {
    //     updateStallsFullDataFromAPIdata(response);
    //   });
    // }, 60000);
  }, []);
  // Run once on page load - async
  useEffect(async () => {
    async function runInitialAPIcalls() {
      // Get session info
      const token = await fetchUserToken();
      window.userToken = token;
    }
    runInitialAPIcalls();
  }, []);

  useEffect(async () => {
    //Timer to refresh schedule for stage
    const loadedScheduleItems = await fetchScheduleItems(fairId);
    // setScheduleItems(loadedScheduleItems);
    setStageScheduleItems(filterScheduleItems(loadedScheduleItems));

    const reschedule = setInterval(
      () => setStageScheduleItems(filterScheduleItems(loadedScheduleItems)),
      10000
    );
  }, [scheduleItems]);


  const getEventsFromStalls = (stalls) => {
    var events = {};
    const stallIds = Object.keys(stalls);

    for (let i = 0; i < stallIds.length; i++) {
      const stall = stalls[stallIds[i]];
      if (stall.field_fair_stall_events && stall.field_fair_stall_events.length > 0) {
        // console.log(stall.field_fair_stall_events);
        for (let i = 0; i < stall.field_fair_stall_events.length; i++) {
          var event = stall.field_fair_stall_events[i];
          event.stallId = stall.id;
          event.backgroundImageUrl = '';
          event.logoUrl = stall.field_fair_stall_logo ? stall.field_fair_stall_logo : '';
          event.subtitle = stall.name;
          event.earliestDate = null;

          // console.log(event);

          // Exclude ended events
          // console.log('event.dates.length', event.dates.length);
          // console.log('event.dates.find(date => new Date(date.date_end) > new Date())', event.dates.find(date => new Date(date.date_end) > new Date()));
          if (event.dates.length === 0 || event.dates.find(date => new Date(date.date_end) > new Date())) {
            for (let j = 0; j < event.dates.length; j++) {
              const date = event.dates[j];
              // console.log('loop j', date, event.earliestDate);
              const thisDateEarlier = event.earliestDate > new Date(date.date_start) && new Date(date.date_end) > new Date();
              event.earliestDate = !event.earliestDate || thisDateEarlier ? new Date(date.date_start) : event.earliestDate;
            }
            if (event.earliestDate) {
              event.category = moment(event.earliestDate).calendar({
                sameDay: '[Today]',
                nextDay: '[Tomorrow]',
                nextWeek: 'dddd',
                lastDay: '[Yesterday]',
                lastWeek: '[Last] dddd',
                sameElse: 'DD-MM-YYYY'
              });
              event.categoryOrder = event.earliestDate;
            } else {
              event.category = "Other links and ways to join in";
              event.categoryOrder = new Date(2055, 1, 1);
            }
            event.id = stall.id + '-' + i;
            events[event.id] = event;
          }
        }
      }
    };

    // events = events.sort((a, b) => (a.earliestDate < b.earliestDate) ? 1 : -1);

    // console.log('getEventsFromStalls', events);

    return events;
  };

  const deleteComment = async (commentId) => {
    // console.log("deleteComment", commentId);
    const success = await saveDataViaAPI("delete comment", {
      commentId: commentId,
    });
    if (success.result === "deleted") {
      // Delete from currentStallComments
      var newStallComments = [...currentStallComments];
      newStallComments = newStallComments.filter((comment) => { return comment.cid != commentId; })
      setCurrentStallComments(newStallComments);
      return true;
    } else {
      return false;
    }
  };

  const addDummyComment = useCallback(
    (message, date, parentId) => {
      let newStallComments = [...currentStallComments];

      const newComment = {
        cid: date.getTime(),
        comment_body: message,
        created: moment(date).format(),
        field_comment_category: "",
        name: "",
        pid: parentId,
        children: [],
        uid: window.userDetails.uid,
        user_picture: window.userDetails.user_picture,
        field_first_name: window.userDetails.field_first_name,
        field_last_name: window.userDetails.field_last_name,
        uuid: "dummyComment" + date.getTime(),
        dummy: true,
      };
      // console.log("addDummyComment", newComment);

      if (parentId) {
        const parentIndex = newStallComments.find(
          (comment) => (comment.cid = parentId)
        );
        newStallComments[parentIndex].children.unshift(newComment);
      } else {
        newStallComments.unshift(newComment);
      }

      //console.log("currentStallComments", currentStallComments);
      //console.log("newStallComments", newStallComments);
      setCurrentStallComments(newStallComments);
    },
    [currentStallComments]
  );

  const generateSuggestedStalls = (stall) => {
    if (window.fairData.field_fair_suggested_enabled !== "1") {
      return false;
    }
    //console.log("generateSuggestedStalls", stall);
    // Need at least a certain number of stalls to generate this sort of thing
    // and for similar to be interesting at all
    if (Object.keys(window.stalls).length < 10) {
      //console.log("generateSuggestedStalls too few", window.stalls);
      return false;
    }

    let suggested = [];

    let stallsToSearch = { ...window.stalls };

    const similar = getMostSimilarStall(stall, stallsToSearch);
    const similarId = similar.id;
    //console.log("similar", similarId);

    // Remove the used one
    delete stallsToSearch[similarId];

    // Random value (make sure diferent than similar)
    let randomId = getRandom(Object.keys(stallsToSearch), 1)[0];
    //console.log("random", randomId);

    delete stallsToSearch[randomId];

    // Union stalls
    const unionStalls = Object.values(window.stalls).filter(
      (e) =>
        (e.fair_stall_category[0] === "Students' Union UCL services" ||
          e.fair_stall_category[0] === "Students&#39; Uni on UCL services" ||
          e.fair_stall_category[0] === "Union services") &&
        e.id != randomId &&
        e.id != similarId
    );

    //console.log("unionStalls", unionStalls);
    const randomUnionId =
      unionStalls.length > 0 ? getRandom(unionStalls, 1)[0].id : false;

    suggested["Something similar (based on tags)"] = [
      similarId,
      <CompareIcon />,
    ];
    suggested["Something random"] = [randomId, <ShuffleIcon />];
    suggested["Something more from your Students' Union"] = [
      randomUnionId,
      <GroupIcon />,
    ];

    //console.log("generateSuggestedStalls suggested", suggested);
    setCurrentStallSuggestedStalls(suggested);
  };

  const filterScheduleItems = (items) => {
    //console.log("filterScheduleItems start", items);
    const now = moment().unix();
    var slicedItems = [];
    for (let i = 0; i < items.length; i++) {
      //console.log(
      //   "filterScheduleItems item",
      //   items[i].id,
      //   items[i].field_fair_schedule_end,
      //   now
      // );
      if (items[i].field_fair_schedule_end >= now) {
        //console.log("filterScheduleItems item", items[i].id, "keep");
        slicedItems.push(items[i]);
      }
    }
    //console.log("filterScheduleItems filtered items", slicedItems);
    slicedItems = slicedItems.sort(
      (a, b) => a.field_fair_schedule_start - b.field_fair_schedule_start
    );
    return slicedItems.slice(0, 5);
  };

  const selectAdvert = async (stallToLoad) => {
    // console.log("selectAdvert");
    if (window.fairData.field_fair_stall_ads !== '1') {
      return false;
    }

    if ((!stallToLoad.field_fair_stall_advert_url ||
      stallToLoad.field_fair_stall_advert_url.length == 0) ||
      (!stallToLoad.field_fair_stall_advert_image ||
        stallToLoad.field_fair_stall_advert_image.length === 0)
    ) {
      if (window.adverts && window.adverts.length > 0) {
        const randomAdFromAPI =
          window.adverts[Math.floor(Math.random() * window.adverts.length)];
        const randomAd = {
          name: randomAdFromAPI.name,
          url: randomAdFromAPI.field_fair_advert_url[0].url,
          image: randomAdFromAPI.field_fair_advert_image[0],
        };
        // console.log("Advert displayed", randomAd.name);
        Event("Adverts", "advert viewed", randomAd.name);
        setCurrentStallAdvert(randomAd);
      }
    } else {
      const stallAd = {
        name: stallToLoad.field_fair_stall_advert_url[0].title,
        image: stallToLoad.field_fair_stall_advert_image,
        url: stallToLoad.field_fair_stall_advert_url[0].url,
      };
      setCurrentStallAdvert(stallAd);
    }
  };

  const runStallSearchFromChild = useCallback(
    (field, value) => {
      // TODO
    },
    [currentStallData]
  );

  // Handle saved data
  const getCardForSaved = (type, id) => {
    return type === "video"
      ? videos.find((card) => card.id === id)
      : stalls.find((card) => card.id === id);
  };

  const getClickHandlerForCardType = (type, id) => {
    return type === "video" ? openVideo : openStall;
  };

  const addToCardHistory = (type, id) => {
    cardHistory.push([
      type,
      id,
      getCardForSaved(type, id),
      getClickHandlerForCardType(type),
    ]);
    setCardHistory(cardHistory);
  };

  const addToSaved = (type, id) => {
    var newSaved = saved;
    newSaved.push([
      type,
      id,
      getCardForSaved(type, id),
      getClickHandlerForCardType(type),
    ]);
    setSaved(saved);
  };

  const removeFromSaved = (type, id) => {
    var newSaved = saved;
    newSaved.filter((item) => item[0] !== type && item[1] !== id);
    setSaved(newSaved);
  };

  const mergeComments = (currentStallComments, newComments) => {
    var mergedComments = [];
    mergedComments = [].concat(newComments, currentStallComments);

    // Filter duplicates &
    // Remove dummy comments, these should be loaded now
    mergedComments = mergedComments.filter((v, i, a) => {
      const isUnique = a.findIndex((t) => t.uuid === v.uuid) === i;
      const isDummy = v.dummy;
      // console.log(v.cid, "isUnique", isUnique);
      // console.log(v.cid, "isDummy", isDummy);
      return isUnique && !isDummy;
    });

    return mergedComments;
  };

  const updateStallsFullDataFromAPIdata = (newStalls) => {
    const existingStallsFull = stallsFullData;

    // Update any existing stalls
    for (let i = 0; i < existingStallsFull.length; i++) {
      const newData = newStalls.find(
        (stall) => stall.id === existingStallsFull[i].id
      );
      // console.log("updateStallsFromAPIdata newData", newData);
      if (newData) {
        updateStallFullDataFromAPIdata(existingStallsFull[i].id, newData);
      }
    }

    // Add any non-existent stalls
    // Is this something we need?
  };

  const updateStallFullDataFromAPIdata = (id, fullData) => {
    const existingStallsFull = stallsFullData;
    var updated = false;

    // Merge basic stall information into whatever we're updating in Full
    let basicStall = stalls.find((stall) => stall.id === id);
    if (!basicStall) {
      basicStall = {};
    }

    for (let i = 0; i < existingStallsFull.length; i++) {
      if (existingStallsFull[i] && existingStallsFull[i].id === id) {
        const newStall = Object.assign(existingStallsFull[i], fullData);
        existingStallsFull[i] = Object.assign(basicStall, newStall);
        updated = true;
        i = existingStallsFull.length;
      }
    }

    if (!updated && fullData) {
      existingStallsFull.push(Object.assign(basicStall, fullData));
    }

    if (currentStallData.id === fullData.id) {
      // console.log("setCurrentStallData", Object.assign(basicStall, fullData));
      setCurrentStallData(Object.assign(basicStall, fullData));
      setCurrentStallComments([]);
    }
    // console.log("fullData", fullData);
    setStallsFullData(existingStallsFull);
    return Object.assign(basicStall, fullData);
  };

  var timerComments = null;

  const openStall = useCallback(
    (id) => {
      //console.log("openStall", id);
      if (id) {
        if (!window.userToken) {
          fetchUserToken().then((token) => {
            window.userToken = token;
          });
        }

        // Load API information for use later
        fetchUserStallActions(id).then((loadedStallActions) => {
          // console.log("loadedStallActions", loadedStallActions);
          setStallUserActions(loadedStallActions);
          window.stallUserActions = loadedStallActions;
        });

        var stallToLoad = window.stalls[id];
        //console.log("openStall stallToLoad", stallToLoad);
        setCurrentStallData(stallToLoad);

        // Generate from other data
        //console.log("openStall selectAdvert", id);
        selectAdvert(stallToLoad);
        //console.log("openStall generateSuggestedStalls", id);
        generateSuggestedStalls(stallToLoad);

        const stallManager =
          stallToLoad.fair_stall_managers &&
          stallToLoad.fair_stall_managers.indexOf(window.userDetails.uid) > -1;
        const admin = window.userDetails.fair_admin;
        //console.log("openStall setUserIsStallManager", id);
        setUserIsStallManager(stallManager || admin);

        // Fetch from external
        fetchStallEvents(stallToLoad).then((stallEvents) => {
          console.log("openStall fetchStallEvents", id);
          setCurrentStallEvents(stallEvents);
        });

        if (stallToLoad.type === "club_society") {
          //console.log("openStall fetchStallProducts", id);
          if (stallToLoad.entity_type == 'group') {
            console.log('stallToLoad.products', stallToLoad.products);
            setCurrentStallProducts(stallToLoad.products);
          } else {
            // fetchStallProducts(stallToLoad).then((stallProducts) => {
            //   stallProducts = stallProducts.filter(productContainer => {
            //     // console.log(productContainer);
            //     return productContainer.product.Title.indexOf('(Remote)') < 0;
            //   });
            //   setCurrentStallProducts(stallProducts);
            // });
          }
        }

        // Load comments and set timer to keep doing it
        // console.log("openStall updateCurrentStallComments", id);
        setCurrentStallComments([]);
        updateCurrentStallComments(id, false);

        PageView(baseUrl + "/stall/" + id);
      } else {
        setCurrentStallData(null);
        setCurrentStallComments([]);
        updateCurrentStallComments(false, false);
        setStallUserActions(null);
        window.stallUserActions = null;
        if (timerComments) {
          clearInterval(timerComments);
        }
      }
    },
    [stallsFullData, currentStallComments]
  );

  var updateCurrentStallComments = useCallback(
    (stallId, useTime) => {
      if (!stallId) {
        setCurrentStallComments([]);
      }
      // console.log(
      //   "updateCurrentStallComments currentStallComments",
      //   currentStallComments
      // );
      fetchStallComments(stallId, useTime ? window.timeCommentsLastLoaded : null).then(
        (loadedNewComments) => {
          // console.log("fetchStallComments", stallId, currentStallComments, loadedNewComments);
          window.timeCommentsLastLoaded = Math.round(
            new Date().getTime() / 1000
          );

          var mergedComments = loadedNewComments;
          if (useTime) {
            // Merge with old comments if we're adding to existing stall's comments
            mergedComments = mergeComments(
              currentStallComments,
              loadedNewComments
            );
          }

          console.log('loadedNewComments', loadedNewComments);
          console.log('mergedComments', mergedComments);

          setCurrentStallComments((current) => mergedComments);
        }
      );
    },
    [currentStallData, currentStallComments]
  );

  useEffect(() => {
    timerComments = setInterval(() => {
      if (currentStallData) {
        updateCurrentStallComments(currentStallData.id, true);
      }
    }, 15000);
    return () => clearInterval(timerComments);
  }, [currentStallData, currentStallComments]);

  const openVideo = (id) => {
    // console.log("openVideo", id);
    if (id) {
      setCurrentVideoData(window.videos[id]);
      addToCardHistory("video", id);
    } else {
      setCurrentVideoData(null);
    }
    PageView(baseUrl + "/video/" + id);
  };

  const checkForVideo =
    window.fairData.field_fair_stream.length > 0
      ? window.fairData.field_fair_stream[0].url
      : window.fairData.field_fair_trailer.length > 0
        ? window.fairData.field_fair_trailer[0].url
        : null;

  return (
    <ThemeProvider theme={theme}>
      <Grow in={true}>
        <Router>
          <Banner
            title={window.fairData.name}
            tagline={window.fairData.field_fair_tagline}
            logoUrls={window.fairData.field_fair_logo}
            logobarUrl={window.fairData.field_fair_logo_bar[0]}
            bannerFullWidthUrl={window.fairData.field_fair_banner_full_width[0]}
            bannerHalfWidthOverlap={window.fairData.field_fair_banner_overlap}
            bannerHalfWidthUrl={window.fairData.field_fair_banner_half_width[0]}
            baseUrl={baseUrl}
            openStallHandler={openStall}
            videoUrl={checkForVideo}
            events={events}
          />
          <Switch>
            {window.userDetails !== null ? (
              <Route
                path={baseUrl + "/group/:id"}
                render={(props) => {
                  if (!loadedFromStall) {
                    openStall('g' + props.match.params.id);
                    setLoadedFromStall('g' + props.match.params.id);
                  }
                  return (
                    <Container>
                      <CircularProgress size={"20rem"} />
                    </Container>
                  );
                }}
              />
            ) : (
              <></>
            )}
            {window.userDetails !== null ? (
              <Route
                path={baseUrl + "/candidate/:id"}
                render={(props) => {
                  if (!loadedFromStall) {
                    openStall('c' + props.match.params.id);
                    setLoadedFromStall('c' + props.match.params.id);
                  }
                  return (
                    <Container>
                      <CircularProgress size={"20rem"} />
                    </Container>
                  );
                }}
              />
            ) : (
              <></>
            )}
            {window.userDetails !== null ? (
              <Route
                path={baseUrl + "/stall/:id"}
                render={(props) => {
                  if (!loadedFromStall) {
                    openStall(props.match.params.id);
                    setLoadedFromStall(props.match.params.id);
                  }
                  return (
                    <Container>
                      <CircularProgress size={"20rem"} />
                    </Container>
                  );
                }}
              />
            ) : (
              <></>
            )}
            <Route
              exact
              path={baseUrl}
              render={() => (
                <div role="main">
                  <StallsPage
                    filters={directoryFiltersStalls}
                    filtersHandler={setDirectoryFiltersStalls}
                    currentCategories={directoryCurrentCategoriesStalls}
                    currentCategoriesHandler={
                      setDirectoryCurrentCategoriesStalls
                    }
                    openHandler={openStall}
                    cardLoading={cardLoading}
                    timeMode={timeMode}
                    baseUrl={baseUrl}
                    scheduleItems={scheduleItems}
                    fair={window.fairData}
                    videoUrl={checkForVideo}
                  />
                </div>
              )}
            />
            <Route
              path={baseUrl + "/saved"}
              render={(props) => (
                <SavedPage
                  title={window.fairData.field_fair_saved_title}
                  titleHistory={window.fairData.field_fair_saved_title_history}
                  openHandler={openStall}
                  cardsSaved={saved}
                  cardsHistory={cardHistory}
                  joinEnabledForFair={
                    window.fairData.field_joining_club_societies_ena && window.fairData.field_joining_club_societies_ena === "1"
                  }
                />
              )}
            />
            <Route
              path={baseUrl + "/events"}
              render={(props) => (
                <EventsPage
                  events={events}
                  openHandler={openStall}
                  currentCategories={directoryCurrentCategoriesEvents}
                  currentCategoriesHandler={
                    setDirectoryCurrentCategoriesEvents
                  }
                />
              )}
            />
            {window.fairData.field_fair_stage_enable === "1" ? (
              <Route
                path={baseUrl + "/stage"}
                render={() => (
                  <StagePage
                    data-tour="stage"
                    url={checkForVideo}
                    videos={videos}
                    resetToLivestream={() => setCurrentVideoData(null)}
                    currentVideoData={currentVideoData}
                    openHandler={openVideo}
                    header={window.fairData.field_fair_stage_title}
                    headerVideos={window.fairData.field_fair_stage_title_videos}
                    scheduleItems={stageScheduleItems}
                    currentCategories={directoryCurrentCategoriesVideos}
                    currentCategoriesHandler={
                      setDirectoryCurrentCategoriesVideos
                    }
                    stageButtons={
                      window.fairData.field_fair_stage_action_button
                    }
                  />
                )}
              />
            ) : (
              <></>
            )}
            <Route render={(props) => {
              // console.log("ROUTE Not found", props);
              return (
                <Container>
                  Not found.
                </Container>
              );
            }} />
          </Switch>
          <Hidden smUp>
            <TabsBar
              bottomBar={true}
              baseUrl={baseUrl}
              openStallHandler={openStall}
              events={events}
              fairData={window.fairData}
            />
          </Hidden>
          <Stall
            fairId={fairId}
            stallData={currentStallData}
            stallsBannerImageUrl={window.fairData.field_fair_stall_banner[0]}
            stallsBannerImageHalfWidthUrl={
              window.fairData.field_stall_banner_half_width[0]
            }
            stallsBannerImageHalfWidthOverlap={
              window.fairData.field_stall_banner_overlap
            }
            openHandler={openStall}
            runSearchHandler={runStallSearchFromChild}
            setCurrentStallDataHandler={setCurrentStallData}
            saveDataViaAPIhandler={saveDataViaAPI}
            suggestedStalls={currentStallSuggestedStalls}
            currentStallEvents={currentStallEvents}
            currentStallProducts={currentStallProducts}
            stallUserActions={stallUserActions}
            currentStallAdvert={currentStallAdvert}
            addDummyComment={addDummyComment}
            loadedFromStall={loadedFromStall}
            baseUrl={baseUrl}
            originalUrl={window.location.pathname}
            userIsStallManager={userIsStallManager}
            userDetails={window.userDetails}
            userLoggedIn={window.userDetails.uid > 0 ? true : false}
            joinEnabledForFair={
              window.fairData.field_joining_club_societies_ena && window.fairData.field_joining_club_societies_ena === "1"
            }
            showCategoryInHeader={window.fairData.field_fair_stall_header_category && window.fairData.field_fair_stall_header_category === "1"}
            currentStallComments={currentStallComments}
            deleteCommentHandler={deleteComment}
          />
          <HelloDialog
            open={helloDialogOpen}
            handleCancel={() => {
              setHelloDialogOpen(false);
            }}
            dialogTitle={window.fairData.field_fair_hello_dialog_title}
            dialogContent={window.fairData.field_fair_hello_dialog_text}
          />
        </Router>
      </Grow>
    </ThemeProvider>
  );
}
