import React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Cookies from 'universal-cookie';

export default function LoginDialog(props) {
  const handleCancel = () => {
    props.cancelLoginDialog();
  };

  const login = (ucl, action) => {
    const cookies = new Cookies();
    cookies.set('su_login_destination', window.location.pathname + "?" + action, { path: '/' });
    let uclApi = "https://uclapi.com/oauth/authorise?client_id=" + window.clientId + "&state=1";
    window.location.assign(uclApi);
  };

  let actionDescription = "proceed";
  switch (props.action) {
    case "save":
      actionDescription = "save this stall";
      break;

    case "join":
      actionDescription = "join this club/society";
      break;

    case "comment":
      actionDescription = "comment";
      break;

    case "ask":
      actionDescription = "ask a question";
      break;
  }

  return (
    <Dialog
      open={props.open}
      onClose={handleCancel}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Log in"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          To {actionDescription} you'll need to log in.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel} color="secondary">
          Cancel
        </Button>
        {props.action !== 'comment' ? (
          <Button onClick={props.handleNoUcl} color="primary" autoFocus>
            I don't have a UCL account
          </Button>
        ) : (
          <></>
        )}
        <Button
          onClick={() => login(true, props.action)}
          color="primary"
          autoFocus
        >
          Log in with UCL account
        </Button>
      </DialogActions>
    </Dialog>
  );
}
