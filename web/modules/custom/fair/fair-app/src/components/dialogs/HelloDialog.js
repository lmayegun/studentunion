import React, { useState, useEffect } from "react";
import ReactHtmlParser from "react-html-parser";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { themeAccessible } from "./../../utils/Theme";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Tour from "reactour";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(4),
    marginTop: theme.spacing(1),
    marginBottom: -theme.spacing(1),
  },
  button: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  bar: {},
}));

const tourSteps = [];

const name = window.fairData && window.fairData.name ? window.fairData.name : 'the fair';

tourSteps.push({
    selector: '[data-tour="tab_stalls"]',
    content: window.fairData.field_fair_tour_1_stalls || `Just like a real fair, ${name} has lots of (virtual) stalls to visit. Get browsing.`,
  });

if(window.fairData.field_fair_stage_enable === "1") {
  tourSteps.push({
    selector: '[data-tour="tab_stage"]',
    content: window.fairData.field_fair_tour_2_stage || `During ${name}, we'll be live broadcasting. We'll have live performances, interviews, competitions and lots more. Tune in.`,
  });
}

  tourSteps.push({
    selector: '[data-tour="tab_saved"]',
    content: window.fairData.field_fair_tour_3_saved || `You can save stalls to look at later. Handy.`,
    action: () => {
      // Go to StagePage
    },
  });

  
if(window.fairData.field_fair_stage_enable === "1") {
  
  // tourSteps.push({
  //   selector: '[data-tour="stream"]',
  //   content: `Our live broadcast from the Student Centre will showcase the best of our clubs, societies and services. You can also browse our video archive and watch performances, demonstrations and introductions from our clubs and societies.`,
  // });
  // {
  //   selector: '[data-tour="videos"]',
  //   content: `You can browse our video archive and watch performances, demonstrations and introductions from our clubs and societies.`
  // },

}
  
tourSteps.push({
    selector: '[data-tour="search"]',
    content: window.fairData.field_fair_tour_4_search || `Know what you're looking for? You can find a stall by searching the name, key words or tags.`,
    action: () => {
      // Go to StallsPage
    },
  });
  
  tourSteps.push({
    selector: '[data-tour="tags"]',
    content: window.fairData.field_fair_tour_5_tags_ || `Tags will help you find stalls based on what they offer, and discover stalls you might not have considered.`,
  });
  
  tourSteps.push({
    selector: '[data-tour="stalls_category_first"]',
    content: window.fairData.field_fair_tour_6_categories || `Stalls are displayed in categories, randomly sorted so you can find something interesting. Click on a stall to open it.`,
    action: () => {
      // Go to open stall
    },
  });
  
  tourSteps.push({
    selector: '[data-tour="toolbar"]',
    content: window.fairData.field_fair_tour_7_stall || `On the stall, find out more about a stallholder, save the stall to visit later, sign up, and find links to stallholder websites and social media. To read more about the stall, click 'Read more'.`,
    action: () => {
      // Close stall
    },
  });
  
  tourSteps.push({
    selector: '[data-tour="tab_stalls"]',
    content: `You're all set!`,
  });

export default function LoginDialog(props) {
  const classes = useStyles();
  const [isTourOpen, setTourIsOpen] = useState(false);
  const handleCancel = () => {
    props.handleCancel();
  };

  const handleTour = () => {
    setTourIsOpen(true);
    props.handleCancel();
  };

  return (
    <>
      <Dialog
        fullWidth={true}
        maxWidth="sm"
        open={props.open}
        onClose={handleCancel}
        // aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {props.dialogTitle ? ReactHtmlParser(props.dialogTitle) : "Hello"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.dialogContent
              ? ReactHtmlParser(props.dialogContent)
              : "Welcome to the fair!"}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            className={classes.button}
            onClick={handleCancel}
            color="secondary"
          >
            Get started
          </Button>
          <ThemeProvider theme={themeAccessible}>
            <Button
              className={classes.button}
              onClick={handleTour}
              color="primary"
              autoFocus
            >
              Find out how it all works
            </Button>
          </ThemeProvider>
        </DialogActions>
      </Dialog>
      <Tour
        steps={tourSteps}
        isOpen={isTourOpen}
        onRequestClose={() => setTourIsOpen(false)}
        accentColor={"#F26640"}
      />
    </>
  );
}
