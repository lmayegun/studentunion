import React from "react";
import ReactPlayer from "react-player";

/* Material UI */
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import Box from "@material-ui/core/Box";
import Fab from "@material-ui/core/Fab";

/* Subcomponents */
import CloseIcon from "@material-ui/icons/Close";

/* Styles */
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  dialog: {
    overflow: "hidden",
  },
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  fab: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  playerWrapper: {
    position: "relative",
    padding: theme.spacing(1),
    paddingTop: "56.25%" /* Player ratio: 100 / (1280 / 720) */,
    maxHeight: "100vh",
  },
  reactPlayer: {
    position: "absolute",
    top: 0,
    left: 0,
    padding: theme.spacing(1),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function VideoPlayer(props) {
  const classes = useStyles();

  const handleClose = () => {
    props.setCurrentVideoDataHandler(null);
  };

  if (props.videoUrl) {
    return (
      <Dialog
        fullScreen
        open={true}
        onClose={handleClose}
        TransitionComponent={Transition}
        className={classes.dialog}
      >
        <Box className={classes.playerWrapper}>
          <ReactPlayer
            url={props.videoUrl}
            className={classes.reactPlayer}
            width="100%"
            height="100%"
          />
        </Box>
        <Fab
          className={classes.fab}
          onClick={handleClose}
          color="primary"
          aria-label="close"
        >
          <CloseIcon />
        </Fab>
      </Dialog>
    );
  } else {
    return <></>;
  }
}
