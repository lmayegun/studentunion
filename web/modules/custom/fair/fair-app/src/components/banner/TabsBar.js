import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link, withRouter } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";

/* Icons */
import StorefrontIcon from "@material-ui/icons/Storefront";
import HelpIcon from "@material-ui/icons/Help";
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import LiveTvIcon from "@material-ui/icons/LiveTv";
import StarIcon from "@material-ui/icons/Star";

const useStyles = makeStyles((theme) => ({
  stickToBottom: {
    width: "100%",
    maxWidth: "100vw",
    position: "fixed",
    bottom: 0,
    zIndex: 1300,
  },
}));

function TabsBar(props) {
  const classes = useStyles();
  const handleCallToRouter = (value) => {
    props.history.push(value);
  };

  const tabs = [];
  tabs.push(
    {
      label: props.fairData.field_fair_tab_name_stalls && props.fairData.field_fair_tab_name_stalls != '' ? props.fairData.field_fair_tab_name_stalls : "Stalls",
      value: props.baseUrl,
      dataTour: "tab_stalls",
      icon: <StorefrontIcon />,
    }
  );
  if (window.fairData.field_fair_stage_enable === "1") {
    tabs.push({
      label: props.fairData.field_fair_stage_title && props.fairData.field_fair_stage_title != '' ? props.fairData.field_fair_stage_title : "Live",
      value: props.baseUrl + "/stage",
      dataTour: "tab_stage",
      icon: <LiveTvIcon />,
    });
  }
  if (window.fairData.field_fair_events_enable === "1" && Object.keys(props.events).length > 0) {
    tabs.push({
      label: props.fairData.field_fair_stage_title && props.fairData.field_fair_tab_name_events != '' ? props.fairData.field_fair_tab_name_events : "Join in",
      value: props.baseUrl + "/events",
      dataTour: "tab_events",
      icon: <EmojiPeopleIcon />,
    });
  }
  tabs.push(
    {
      label: "Saved",
      value: props.baseUrl + "/saved",
      dataTour: "tab_saved",
      icon: <StarIcon />,
    });

  if (props.bottomBar) {
    return (
      <BottomNavigation
        value={props.history.location.pathname}
        // onChange={(event, newValue) => {
        //   setValue(newValue);
        // }}
        showLabels
        className={classes.stickToBottom}
      >
        {tabs.map((tab) => {
          return (
            <BottomNavigationAction
              key={tab.value}
              value={tab.value}
              label={tab.label}
              component={Link}
              to={tab.value}
              icon={tab.icon}
              data-tour={tab.dataTour}
            />
          );
        })}
      </BottomNavigation>
    );
  } else {
    return (
      <AppBar position="static" color="default" elevation={2}>
        <Tabs
          value={props.history.location.pathname}
          indicatorColor="secondary"
          textColor="secondary"
          centered
        >
          {tabs.map((tab) => {
            return (
              <Tab
                key={tab.value}
                value={tab.value}
                label={tab.label}
                component={Link}
                to={tab.value}
                icon={tab.icon}
                data-tour={tab.dataTour}
              />
            );
          })}

          {/* <Tab
            // value={props.baseUrl + "/tour"}
            label="Tour"
            onClick={props.openTourHandler}
            // component={Link}
            // to={props.baseUrl + "/tour"}
            icon={<HelpIcon />}
          /> */}
        </Tabs>
      </AppBar>
    );
  }
}

export default withRouter(TabsBar);
