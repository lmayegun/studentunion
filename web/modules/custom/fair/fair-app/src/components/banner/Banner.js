import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Link, withRouter } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Grow from "@material-ui/core/Grow";
import Hidden from "@material-ui/core/Hidden";

import Mainstage from "./../mainstage/Mainstage";
import YouTubeIcon from "@material-ui/icons/YouTube";
import TabsBar from "./TabsBar";

const logoHeight = 40;

const useStyles = makeStyles((theme) => ({
  header: {
    position: "relative",
    display: "flex",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    backgroundSize: "cover",
    backgroundAttachment: "fixed",
  },
  content: {
    padding: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    height: "100%",
    justifyContent: "center",
  },
  logobar: {
    width: "100%",
    zIndex: 2,
    // position: "absolute",
    // top: 0,
  },
  logobarBox: {
    padding: theme.spacing(2),
    display: "flex",
    justifyContent: "space-between",
  },
  logobarHeading: {
    padding: theme.spacing(2),
    display: "inline",
  },
  logo: {
    height: logoHeight,
  },
  fairPageTitle: {},
  banner: {
    // transition: "1s",
    position: "relative",
    zIndex: 1,
    height: "400px",
    [theme.breakpoints.down("md")]: {
      height: "auto",
    },
    flexDirection: "column",
    display: "flex",
    justifyContent: "space-between",
    // paddingTop: 40 + theme.spacing(4),
    // "&:before": {
    //   content: " ",
    //   position: "absolute",
    //   top: 0,
    //   width: "100%",
    //   height: "100%",
    //   zIndex: -1,
    // },
  },
  bannerHalfWidthImage: {
    position: "absolute",
    objectFit: "cover",
    top: 0,
    width: "100%",
    height: "100%",
  },
  playerWrapper: {
    position: "relative",
    width: "100%",
    height: "100%",
    minHeight: "231px",
    backgroundColor: "#000000"
  },
  playerOverlay: {
    position: "absolute",
    width: "100%",
    height: "100%",
    cursor: "pointer",
    left: 0,
    top: 0,
    zIndex: 1,
  },
  playIcon: {
    fontSize: 110,
    color: "#2aaa9e",
  },
  video: {
    // transition: "1s",
  },
  videoHidden: {
    position: "absolute",
    left: "100%",
  },
  videoOverlay: {
    position: "absolute",
    left: 0,
    top: 0,
  },
}));

function Banner(props) {
  const theme = useTheme();
  const classes = useStyles();

  const showVideo = props.videoUrl;
  const onStage = props.history.location.pathname === props.baseUrl + "/stage";

  const bannerVideoHidden = onStage;
  const bigBanner = !onStage;

  const LogobarImageStyle = props.logobarUrl
    ? { backgroundImage: "url(" + props.logobarUrl + ")" }
    : {};
  const BannerImageStyle = props.bannerFullWidthUrl
    ? { backgroundImage: "url(" + props.bannerFullWidthUrl + ")" }
    : {};
  const HeaderStyle = bigBanner ? {} : {};

  const TransitionStyle = {
    transition: theme.transitions.create("all", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  };

  const logoUrlsArray = props.logoUrls ? props.logoUrls : [];

  return (
    <Grid container className={classes.header} style={HeaderStyle}>
      {/* <Grid
        item
        xs={12}
        className={`banner-logobar ${classes.logobar}`}
        style={LogobarImageStyle}
      >
        <Box className={classes.logobarBox} boxShadow={2}>
          {logoUrlsArray &&
            logoUrlsArray.map((url) => {
              return <img
                key={url}
                alt={"Logo linking to "+url}
                className={classes.logo}
                src={url} />;
            })}
        </Box>
      </Grid> */}
      {bigBanner ? (
        <>
          <Grid
            item
            xs={12}
            md={showVideo && !bannerVideoHidden ? 6 : 12}
            className={classes.banner}
            style={Object.assign(TransitionStyle, BannerImageStyle)}
          >
            <Hidden smDown>
              {showVideo && props.bannerHalfWidthUrl ? (
                <img
                  className={`${classes.bannerHalfWidthImage} banner-background-image`}
                  style={{
                    minWidth:
                      "calc(100% + " + props.bannerHalfWidthOverlap + ")",
                  }}
                  src={props.bannerHalfWidthUrl}
                  alt="Banner background image"
                />
              ) : (
                <></>
              )}
            </Hidden>
            <div className={classes.content}>
              <h1
                className={`fair-page-title ${classes.fairPageTitle}`}
                style={{ zIndex: 2, color: window.fairData.field_heading_colour ? window.fairData.field_heading_colour : '#FFFFFF' }}
              >
                {props.title}
              </h1>
              <h2 style={{ zIndex: 2, color: window.fairData.field_heading_colour ? window.fairData.field_heading_colour : '#FFFFFF' }} className={"fair-page-tagline"}>
                {props.tagline}
              </h2>
            </div>
          </Grid>

          {showVideo ? (
            <>
              <Grid
                item
                xs={12}
                md={!bannerVideoHidden ? 6 : 0}
                className={`MuiPaper-elevation4 banner-video ${classes.video} ${bannerVideoHidden ? classes.videoHidden : ""
                  }`}
                style={TransitionStyle}
              >
                <Hidden smDown>
                  <img
                    className={classes.videoOverlay}
                    src={props.videoOverlayUrl}
                    alt=""
                  // Alt is empty to avoid confusing as it's not a content element
                  />
                </Hidden>
                <div className={classes.playerWrapper}>
                  {/* <Link
                    to={props.baseUrl + "/stage"}
                    className={classes.playerOverlay}
                  ></Link> */}
                  <Mainstage
                    // light={true}
                    width="100%"
                    height="100%"
                    url={props.videoUrl}
                    playIcon={<YouTubeIcon className={classes.playIcon} />}
                  />
                </div>
              </Grid>
            </>
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
      <Hidden xsDown>
        <TabsBar
          bottomBar={false}
          baseUrl={props.baseUrl}
          openStallHandler={props.openStall}
          openTourHandler={props.openTourHandler}
          showVideo={showVideo}
          bannerVideoHidden={bannerVideoHidden}
          events={props.events}
          fairData={window.fairData}
        />
      </Hidden>
    </Grid>
  );
}

export default withRouter(Banner);
