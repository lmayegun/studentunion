import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    padding: theme.spacing(2),
  },
}));

export default function TestingToolbar(props) {
  const classes = useStyles();
  return (
    <FormControl className={classes.root}>
      <FormLabel htmlFor="timeMode">
        <b>Testing:</b> time travel
      </FormLabel>
      <FormGroup row>
        <ToggleButtonGroup id="timeMode" value={props.timeMode} exclusive>
          <ToggleButton
            component="a"
            href={props.baseUrl + "?timeMode=future"}
            value="future"
          >
            Fair is coming up
          </ToggleButton>
          <ToggleButton
            component="a"
            href={props.baseUrl + "?timeMode=present"}
            value="present"
          >
            Fair is on now
          </ToggleButton>
          <ToggleButton
            component="a"
            href={props.baseUrl + "?timeMode=past"}
            value="past"
          >
            Fair is over
          </ToggleButton>
        </ToggleButtonGroup>
      </FormGroup>
    </FormControl>
  );
}
