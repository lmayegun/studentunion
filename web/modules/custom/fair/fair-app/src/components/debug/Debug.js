import React, { useState, useEffect } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";

export default function Debug(props) {
  const vals = [];
  return (
    <List
      style={{
        overflow: "hidden",
        maxWidth: "80vw",
      }}
    >
      {props &&
        Object.keys(props).map((keyName, i) => (
          <div key={i}>
            <ListItem alignItems="flex-start">
              <ListItemText primary={keyName} secondary={props[keyName]} />
            </ListItem>
            <Divider component="li" />
          </div>
        ))}
    </List>
  );
}
