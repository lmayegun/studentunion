import React from "react";
import { unix } from "moment";
// will need moment and other
import moment from "moment";
import Moment from "react-moment";
import _ from "lodash";
import chunk from "lodash/chunk";

//materialUI
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

//styles from material
const useStyles = makeStyles((theme) => ({
  paper: {
    margin: "auto",
    fontSize: "1.5 rem",
    margin: "2% 4%",
  },
  date: {
    backgroundColor: theme.palette.secondary.main,
    borderRadius: "3px 0 0 3px",
    color: "white",
    width: "20%",
  },
  h1: {
    padding: "0 6%",
  },
  items: {
    padding: "2% 6%",
    backgroundColor: theme.palette.background.paper,
    fontSize: "1.2rem",
    width: "80%",
  },
  item: {
    margin: "0 0 2% 0",
  },
  itemName: {
    color: theme.palette.primary.main,
    fontWeight: 600,
  },
  timeSpan: {
    color: theme.palette.secondary.main,
    fontWeight: 500,
  },
}));

//some logic that will be sorting and listing all schedule items
// 1) by date
// 2) by time

const FairSchedule = (props) => {
  const classes = useStyles();
  //creates a new aray instead of modyfinig an existing one, which is good
  const sortedScheduleItems = props.items
    .slice()
    .sort((a, b) => a.field_fair_schedule_start - b.field_fair_schedule_start);
  const groupItems = _.groupBy(
    sortedScheduleItems,
    "field_fair_schedule_start_1"
  );
  const sortedGroupedItems = _.sortBy(groupItems, "field_fair_schedule_start");
  // would like to highlight a card if the date is today -
  //compare this with the field_fair_schedule_start and could
  //highlight the paper border for example
  const dateToday = moment().unix();

  //console.log(
  //   "sorted grouped",
  //   sortedGroupedItems,
  //   "groupsrt",
  //   groupItems,
  //   "grnotsrt",
  //   _.groupBy(props.items, "field_fair_schedule_start_1")
  // );

  return sortedGroupedItems.length > 0 ? (
    sortedGroupedItems.map((data, i) => {
      return (
        <>
          <Paper className={classes.paper}>
            <Grid container>
              <Grid item className={classes.date}>
                <div className={classes.h1}>
                  <h1>{data[0]["field_fair_schedule_start_1"]}</h1>
                </div>
              </Grid>
              <>
                <div className={classes.items}>
                  {data.map((item) => (
                    <>
                      <div className={classes.item}>
                        <div className={classes.itemName}>
                          {item.name}
                          {"   "}
                          <span className={classes.timeSpan}>
                            {moment
                              .unix(item.field_fair_schedule_start)
                              .format("H:mm")}{" "}
                            -{" "}
                            {moment
                              .unix(item.field_fair_schedule_end)
                              .format("H:mm")}
                          </span>
                        </div>
                        <div>{item.field_description}</div>
                        <div>
                          {item.field_fair_schedule_url !== "" ? (
                            <>
                              <a href={item.field_fair_schedule_url}>Link</a>
                            </>
                          ) : (
                            <></>
                          )}
                        </div>
                      </div>
                    </>
                  ))}
                </div>
              </>
            </Grid>
          </Paper>
        </>
      );
    })
  ) : (
    <></>
  );
};

export default FairSchedule;

const ItemsPerDay = () => {};
