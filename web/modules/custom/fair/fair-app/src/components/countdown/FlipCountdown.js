import React from "react";
import "./FlipCountdown.css";
import moment from "moment";

// https://codepen.io/liborgabrhel/pen/JyJzjb

// function component
const AnimatedCard = ({ animation, digit }) => {
  return (
    <div className={`flipCard ${animation}`}>
      <span>{digit}</span>
    </div>
  );
};

// function component
const StaticCard = ({ position, digit }) => {
  return (
    <div className={position}>
      <span>{digit}</span>
    </div>
  );
};
const Subtitle = ({ text }) => {
  return (
    <div className={"subtitle"}>
      <h3>{text}</h3>
    </div>
  );
};

const direction = 1; // -1 is counting up
// Could calculate that from props but not bothered

// function component
const FlipUnitContainer = ({ digit, shuffle, unit, subtitle }) => {
  // assign digit values
  let currentDigit = digit;
  let previousDigit = digit - direction;

  // to prevent a negative value
  if (unit !== "hours") {
    previousDigit = previousDigit === -1 ? 59 : previousDigit;
    previousDigit = previousDigit === 60 ? 0 : previousDigit;
  } else {
    previousDigit = previousDigit === -1 ? 23 : previousDigit;
    previousDigit = previousDigit === 24 ? 0 : previousDigit;
  }

  // add zero
  if (currentDigit < 10) {
    currentDigit = `0${currentDigit}`;
  }
  if (previousDigit < 10) {
    previousDigit = `0${previousDigit}`;
  }

  // shuffle digits
  const digit1 = shuffle ? previousDigit : currentDigit;
  const digit2 = !shuffle ? previousDigit : currentDigit;

  // shuffle animations
  const animation1 = shuffle ? "fold" : "unfold";
  const animation2 = !shuffle ? "fold" : "unfold";

  return (
    <div className={"flipUnitContainer"}>
      <StaticCard position={"upperCard"} digit={currentDigit} />
      <StaticCard position={"lowerCard"} digit={previousDigit} />
      <AnimatedCard digit={digit1} animation={animation1} />
      <AnimatedCard digit={digit2} animation={animation2} />
      <Subtitle text={subtitle} />
    </div>
  );
};

export default class FlipCountdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      days: 0,
      daysShuffle: true,
      hours: 0,
      hoursShuffle: true,
      minutes: 0,
      minutesShuffle: true,
      seconds: 0,
      secondsShuffle: true,
    };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.updateTime(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  updateTime() {
    // get new date
    var now = moment();
    var end = moment.unix(this.props.timestamp);
    var duration = now.isAfter(end)
      ? moment.duration(now.diff(end))
      : moment.duration(end.diff(now));
    // console.log("duration", duration);
    var days = Math.round(Math.abs(duration.asDays()));
    // console.log("days", days);
    duration.subtract(moment.duration(days, "days"));
    var hours = Math.abs(duration.hours());
    // console.log("hours", hours);
    duration.subtract(moment.duration(hours, "hours"));
    var minutes = Math.abs(duration.minutes());
    duration.subtract(moment.duration(minutes, "minutes"));
    var seconds = Math.abs(duration.seconds());

    // on hour chanage, update hours and shuffle state
    if (days !== this.state.days) {
      const daysShuffle = !this.state.daysShuffle;
      this.setState({
        days,
        daysShuffle,
      });
    }
    if (hours !== this.state.hours) {
      const hoursShuffle = !this.state.hoursShuffle;
      this.setState({
        hours,
        hoursShuffle,
      });
    }
    if (minutes !== this.state.minutes) {
      const minutesShuffle = !this.state.minutesShuffle;
      this.setState({
        minutes,
        minutesShuffle,
      });
    }
    if (seconds !== this.state.seconds) {
      const secondsShuffle = !this.state.secondsShuffle;
      this.setState({
        seconds,
        secondsShuffle,
      });
    }
  }

  render() {
    // state object destructuring
    const {
      days,
      hours,
      minutes,
      seconds,
      daysShuffle,
      hoursShuffle,
      minutesShuffle,
      secondsShuffle,
    } = this.state;

    return (
      <div className={"flipClock"}>
        <FlipUnitContainer
          unit={"days"}
          digit={days}
          shuffle={daysShuffle}
          subtitle="days"
        />
        <FlipUnitContainer
          unit={"hours"}
          digit={hours}
          shuffle={hoursShuffle}
          subtitle="hours"
        />
        <FlipUnitContainer
          unit={"minutes"}
          digit={minutes}
          shuffle={minutesShuffle}
          subtitle="minutes"
        />
        <FlipUnitContainer
          unit={"seconds"}
          digit={seconds}
          shuffle={secondsShuffle}
          subtitle="seconds"
        />
      </div>
    );
  }
}
