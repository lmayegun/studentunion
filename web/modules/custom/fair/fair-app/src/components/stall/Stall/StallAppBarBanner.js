import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import ReactHtmlParser from "react-html-parser";

/* Material-UI components */
import AppBar from "@material-ui/core/AppBar";
import StallVideo from "./StallVideo";
import StallSlideshow from "./StallSlideshow";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { Box, Fab, Hidden } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import StarIcon from "@material-ui/icons/Star";
import StallCategoriesAndTags from "./StallCategoriesAndTags";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    minHeight: "fit-content",
  },
  rootWithSlideshow: {
    [theme.breakpoints.up("md")]: {
      // minHeight: "400px"
    },
  },
  toolbar: { width: "100%" },
  headingBlock: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    backgroundSize: "cover",
  },
  bannerHalfWidthImage: {
    position: "absolute",
    zIndex: 2,
    objectFit: "cover",
    top: 0,
    width: "100%",
    height: "100%",
    pointerEvents: 'none'
  },
  textBlock: {
    padding: theme.spacing(2),
    paddingLeft: "3vw",
    paddingRight: "3vw",
    paddingTop: "60px",
    zIndex: 3,
  },
  title: {
    marginBottom: 0,
  },
  CloseButton: {
    position: "absolute",
    right: theme.spacing(2),
    top: theme.spacing(2),
  },
  logo: {
    width: "8em",
    minWidth: "8em",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  leaveFab: {
    position: "fixed",
    zIndex: 6,
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

export default function StallAppBarBanner(props) {
  const classes = useStyles();
  const BackgroundImageStyle = props.stallsBannerImageUrl
    ? { backgroundImage: "url(" + props.stallsBannerImageUrl + ")" }
    : {};

  var bannerType = "fullwidth";

  // if (
  //   props.stallData.field_fair_stall_slide_images &&
  //   props.stallData.field_fair_stall_slide_images.length > 0
  // ) {
  //   bannerType = "images";
  // }

  if (
    props.stallData.field_fair_stall_videos &&
    props.stallData.field_fair_stall_videos.length > 0
  ) {
    bannerType = "video";
  }

  return (
    <Grid
      container
      className={`${classes.root} ${bannerType === "images" ? classes.rootWithSlideshow : ""
        }`}
      style={BackgroundImageStyle}
    >
      {/* <Hidden smDown> */}
      <Fab
        size={"medium"}
        onClick={props.handleClose}
        color="secondary"
        className={classes.leaveFab}
        aria-label="close"
        variant="extended"
      >
        <ArrowBackIcon className={classes.extendedIcon} /> Leave stall
      </Fab>
      {/* </Hidden> */}

      <Grid
        xs={12}
        md={bannerType === "fullwidth" ? 12 : 6}
        container
        className={classes.headingBlock}
        justify="space-between"
      >
        {bannerType != "fullwidth" && props.stallsBannerImageHalfWidthUrl ? (
          <Hidden smDown>
            <img
              className={`${classes.bannerHalfWidthImage} banner-background-image`}
              style={{
                minWidth:
                  "calc(100% + " +
                  props.stallsBannerImageHalfWidthOverlap +
                  ")",
              }}
              src={props.stallsBannerImageHalfWidthUrl}
              alt="banner background"
            />
          </Hidden>
        ) : (
          <></>
        )}
        <Grid className={classes.textBlock} item>
          {props.showCategoryInHeader && (
            <h4 className="stall-page-category" style={{ color: window.fairData.field_heading_colour ? window.fairData.field_heading_colour : '#FFFFFF' }}>
              {props.stallData.type == 'election_candidate' ? 'Candidate for ' : ''}{ReactHtmlParser(props.stallData.fair_stall_category)}
            </h4>
          )}
          <h2 className="stall-page-title" style={{ color: window.fairData.field_heading_colour ? window.fairData.field_heading_colour : '#FFFFFF' }}>
            {ReactHtmlParser(props.stallData.name)}
          </h2>
          {props.stallData.fair_stall_tagline != "..." ? (
            <h3 className="stall-page-tagline" style={{ color: window.fairData.field_heading_colour ? window.fairData.field_heading_colour : '#FFFFFF' }}>
              {ReactHtmlParser(props.stallData.fair_stall_tagline)}
            </h3>
          ) : (
            <></>
          )}

          {/* {props.userIsStallManager && <StallCategoriesAndTags
            runSearchHandler={props.runSearchHandler}
            category={props.stallData.fair_stall_category}
            tags={
              props.stallData.fair_stall_tags
                ? props.stallData.fair_stall_tags
                : []
            }
          />} */}
          {/* TODO show these and notthe ones in the toolbar if not scrolled; hide these and show the toolbar ones on scroll past the banner */}
          {/* <StallButtonsJoinAndSave
            saved={props.saved}
            handleSave={props.handleSave}
            stallData={props.stallData}
          /> */}
        </Grid>
      </Grid>
      {bannerType != "fullwidth" ? (
        <Grid xs={12} md={6} container item>
          {bannerType === "video" ? (
            <StallVideo
              url={props.stallData.field_fair_stall_videos[0].url}
              parentClasses={props.parentClasses}
              stallData={props.stallData}
            />
          ) : (
            <></>
          )}
          {bannerType === "images" ? (
            <>
              <Hidden mdUp>
                <StallSlideshow
                  card={false}
                  slideshowImages={
                    props.stallData.field_fair_stall_slide_images
                  }
                  sliderHeight={"400px"}
                  sliderWidth={"100%"}
                />
              </Hidden>
              <Hidden smDown>
                <StallSlideshow
                  card={false}
                  slideshowImages={
                    props.stallData.field_fair_stall_slide_images
                  }
                  sliderHeight={"100%"}
                  sliderWidth={"50%"}
                />
              </Hidden>
            </>
          ) : (
            <></>
          )}
        </Grid>
      ) : (
        <></>
      )}
    </Grid>
  );
}
