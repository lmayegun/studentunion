import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import PhotoLibraryIcon from "@material-ui/icons/PhotoLibrary";

import SimpleImageSlider from "react-simple-image-slider";

import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";

import { CardActionArea } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {
    "& .MuiCardContent-root:last-child": {
      paddingBottom: 0,
    },
  },
  playerWrapper: {
    padding: 0,
    backgroundColor: "#000000",
  },
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
}));

const StallSlideshowModal = (props) => {
  return (
    <Lightbox
      mainSrc={props.slideshowImages[props.currentSlide]}
      nextSrc={
        props.slideshowImages[
          (props.currentSlide + 1) % props.slideshowImages.length
        ]
      }
      prevSrc={
        props.slideshowImages[
          (props.currentSlide + props.slideshowImages.length - 1) %
            props.slideshowImages.length
        ]
      }
      onCloseRequest={props.onClose}
      onMovePrevRequest={() => {
        props.setCurrentSlide(
          (props.currentSlide + props.slideshowImages.length - 1) %
            props.slideshowImages.length
        );
      }}
      onMoveNextRequest={() => {
        props.setCurrentSlide(
          (props.currentSlide + 1) % props.slideshowImages.length
        );
      }}
    />
  );
};

export default function StallSlideshow(props) {
  const classes = useStyles();

  const [images, setImages] = useState(
    props.slideshowImages &&
      props.slideshowImages.map((el) => {
        return {
          backgroundColor: "#FFFFFF",
          url: el,
          caption: "",
        };
      })
  );

  const [modalOpen, setModalOpen] = useState(false);

  const [modalSlides, setModalSlides] = useState(
    props.slideshowImages &&
      props.slideshowImages.map((el) => {
        return {
          media: <img src={el} />,
          content: "",
        };
      })
  );

  const [currentSlide, setCurrentSlide] = useState();

  if (images) {
    var sliderStyle = {};
    if(props.card ) {
      sliderStyle = props.sliderHeight !== "100%" ? { position: "relative" } : {};
      sliderStyle.backgroundColor = "#FFFFFF";
    } else {
      sliderStyle = props.sliderHeight !== "100%"
      ? { position: "relative", backgroundColor: "#000000" }
      : {
          backgroundColor: "#000000",
        };
    }
    return (
      <>
        {props.card ? (
          <Card className={`${classes.card} ${props.classes}`}>
            <CardContent className={classes.playerWrapper}>
              <SimpleImageSlider
                navStyle={2}
                width={props.sliderWidth}
                height={props.sliderHeight}
                style={sliderStyle}
                images={images}
                onClick={(slide, event) => {
                  setCurrentSlide(slide);
                  setModalOpen(true);
                }}
              />
            </CardContent>
          </Card>
        ) : (
          <SimpleImageSlider
            navStyle={2}
            width={props.sliderWidth}
            height={props.sliderHeight}
            style={sliderStyle}
            images={images}
            onClick={(slide, event) => {
              //console.log("Click slideshow");
              setCurrentSlide(slide);
              setModalOpen(true);
            }}
          />
        )}
        {modalOpen && (
          <StallSlideshowModal
            slideshowImages={props.slideshowImages}
            open={modalOpen}
            currentSlide={currentSlide}
            setCurrentSlide={setCurrentSlide}
            onClose={() => {
              setModalOpen(false);
            }}
          />
        )}
      </>
    );
  } else {
    return <></>;
  }
}
