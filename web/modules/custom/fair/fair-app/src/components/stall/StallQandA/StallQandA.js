import React, { useCallback } from "react";
import StallQandACard from "./StallQandACard";
import StallQandAForm from "./StallQandAForm";
import SecondaryHeader from "../../header/SecondaryHeader";
import StallQandACategories from "./StallQandACategories";

import { customMemoLog } from "../../../utils/Hooks";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */

import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Avatar from "@material-ui/core/Avatar";

import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";

import "./StallQandA.css";

import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import StallQandACards from "./StallQandACards";

/* Styling */
const useStyles = makeStyles((theme) => ({
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
  root: {
    minWidth: 275,
  },
  CardContent: {
    // minHeight: '10vw',
    // maxHeight: '10vw'
  },
  list: { maxHeight: "70vh", overflowY: "scroll" },
}));

// import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
// import CheckBoxIcon from '@material-ui/icons/CheckBox';

function StallQandA(props) {
  const classes = useStyles();

  //console.log('StallQandA', props);

  const addDummyCommentFromForm = (message, parentId) => {
    return props.addDummyComment(message, new Date(), parentId);
  };

  return (
    <Card className={`${props.parentClasses.card}`}>
      <CardHeader title={props.title} />
      <CardContent>
        <StallQandAForm
          saveCommentHandler={props.saveCommentHandler}
          currentStallComments={props.currentStallComments}
          addDummyCommentFromForm={addDummyCommentFromForm}
        />
        <StallQandACategories
          clickHandler={props.changeQandACategory}
          selected={props.selectedCategory}
          data={props.categories}
        />
        <StallQandACards 
          filteredComments={props.filteredComments}
          stallData={props.stallData}
          userIsStallManager={props.userIsStallManager}
          deleteCommentHandler={props.deleteCommentHandler}
          saveCommentHandler={props.saveCommentHandler}
          addDummyCommentFromForm={addDummyCommentFromForm}
        />
      </CardContent>
    </Card>
  );
}

// export default StallQandA;

export default React.memo(StallQandA, (prevProps, nextProps) => {
  const propsMatch = prevProps.filteredComments === nextProps.filteredComments;
  customMemoLog("StallQandA", prevProps, nextProps, propsMatch, false);
  return propsMatch;
});
