import React, { useRef, useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";

import { customMemoLog } from "../../../utils/Hooks";

import _ from "lodash";

/* Material UI */
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";

/* Subcomponents */
import StallAppBarBanner from "./StallAppBarBanner";
import StallDialogBody from "./StallDialogBody";
import StallToolbar from "./StallToolbar";
import LoginDialog from "../../dialogs/LoginDialog";
import DialogTitle from '@material-ui/core/DialogTitle';

/* Styles */
import { makeStyles, useTheme } from "@material-ui/core/styles";
import axios from "axios";
import { Fab, Hidden } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import StallBrowseToolbar from "./StallBrowseToolbar";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  fab: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  dialog: {
    "& .MuiDialog-paper": {
      backgroundColor: "#F0F0F0",
    },
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

const Stall = (props) => {
  //console.log("RENDER Stall", props);

  const classes = useStyles();
  const dialog = useRef();
  const dialogContent = useRef();
  const history = useHistory();
  let location = useLocation();

  window.addEventListener("popstate", function (event) {
    handleClose();
  });

  const scrolled = true;
  // TODO:
  //
  // The following should work but it doesn't like passing the dialog as the
  // target And still only observes the overall window scroll  const scrolled =
  // useScrollTrigger({ target: dialog.current ? dialog.current : undefined, //
  // target: props.window ? props.window() : undefined, disableHysteresis:
  // true, threshold: 400, }); console.log("dialog.current", dialog.current);
  // console.log("scrolled", scrolled);

  const handleClose = () => {
    props.setCurrentStallDataHandler(null);
    if (props.originalUrl != props.baseUrl) {
      history.push(props.baseUrl);
    } else {
      window.history.pushState({ stall: null }, "", props.baseUrl);
    }
  };

  const [loginDialogOpen, setLoginDialogOpen] = useState(false);
  const [loginDialogAction, setLoginDialogAction] = useState("");

  const handleSave = async () => {
    if (!window.userDetails) {
      setLoginDialogAction("save");
      setLoginDialogOpen(true);
    } else {
      setLoginDialogOpen(false);
      props.saveDataViaAPIhandler("favourite");
    }
  };

  const saveComment = async (message, parentUuid, category, user_role) => {
    //console.log("saveComment", message);
    if (!window.userDetails) {
      setLoginDialogAction("ask");
      setLoginDialogOpen(true);
      return false;
    } else {
      setLoginDialogOpen(false);
      const success = await props.saveDataViaAPIhandler("comment", {
        comment: message,
        parentUuid: parentUuid,
        category: category,
        user_role: user_role
      });
      return success;
    }
  };

  if (props.stallData) {
    if (props.stallData.id) {
      var stallUrl;
      if (props.stallData.entity_type && props.stallData.entity_type == 'group') {
        stallUrl = props.baseUrl + "/group/" + props.stallData.id.substring(1);
      } else if (props.stallData.entity_type && props.stallData.entity_type == 'election_candidate') {
        stallUrl = props.baseUrl + "/candidate/" + props.stallData.id.substring(1);
      } else {
        stallUrl = props.baseUrl + "/stall/" + props.stallData.id;
      }
      if (location.pathname != stallUrl) {
        window.history.pushState({ stall: props.stallData.id }, "", stallUrl);
        // history.push(stallUrl);
      }
    }
  }

  return (
    <Dialog
      fullScreen
      open={props.stallData}
      ref={dialog}
      onClose={handleClose}
      TransitionComponent={Transition}
      className={classes.dialog}
      aria-labelledby="stall-dialog"
    >
      <DialogTitle id="stall-dialog" style={{ display: "none" }} />
      {props.stallData ? (
        <>
          <StallAppBarBanner
            handleClose={handleClose}
            saved={props.stallFavourite}
            handleSave={handleSave}
            runSearchHandler={props.runSearchHandler}
            stallsBannerImageUrl={props.stallsBannerImageUrl}
            stallsBannerImageHalfWidthUrl={props.stallsBannerImageHalfWidthUrl}
            stallsBannerImageHalfWidthOverlap={
              props.stallsBannerImageHalfWidthOverlap
            }
            stallData={props.stallData}
            userIsStallManager={props.userIsStallManager}
            showCategoryInHeader={props.showCategoryInHeader}
          />
          <StallToolbar
            data-tour="toolbar"
            userLoggedIn={props.userLoggedIn}
            userDetails={props.userDetails}
            dialog={dialog.current}
            handleClose={handleClose}
            scrolled={scrolled}
            handleSave={handleSave}
            stallData={props.stallData}
            loginDialogOpen={loginDialogOpen}
            stallUserActions={props.stallUserActions}
            joinEnabledForFair={props.joinEnabledForFair}
            saveDataViaAPIhandler={props.saveDataViaAPIhandler}
            userIsStallManager={props.userIsStallManager}
            currentStallProducts={props.currentStallProducts}
          ></StallToolbar>
          <StallDialogBody
            handleClose={handleClose}
            currentStallComments={props.currentStallComments}
            dialog={dialog.current}
            stallData={props.stallData}
            saveCommentHandler={saveComment}
            deleteCommentHandler={props.deleteCommentHandler}
            addDummyComment={props.addDummyComment}
            userIsStallManager={props.userIsStallManager}
            openHandler={props.openHandler}
            currentStallEvents={props.currentStallEvents}
            currentStallAdvert={props.currentStallAdvert}
            suggestedStalls={props.suggestedStalls}
            setLoginDialogOpen={setLoginDialogOpen}
            loginDialogOpen={loginDialogOpen}
            setLoginDialogAction={setLoginDialogAction}
            loginDialogAction={loginDialogAction}
            stallUserActions={props.stallUserActions}
          />
          {/* <StallBrowseToolbar /> */}
          <LoginDialog
            open={loginDialogOpen}
            action={loginDialogAction}
            cancelLoginDialog={() => {
              setLoginDialogOpen(false);
            }}
            handleNoUcl={() => {
              setLoginDialogOpen(false);
            }}
          />
        </>
      ) : (
        <>No stall.</>
      )}
    </Dialog>
  );
};

export default Stall;

// export default React.memo(Stall, (prevProps, nextProps) => {
//   const propsMatch = prevProps.stallData === nextProps.stallData &&
//         prevProps.currentStallComments === nextProps.currentStallComments;
//   customMemoLog("Stall", prevProps, nextProps, propsMatch);
//   return propsMatch;
// });
