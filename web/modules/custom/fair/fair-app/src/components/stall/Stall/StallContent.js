import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import ReactHtmlParser from "react-html-parser";

import clsx from "clsx";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Chip from "@material-ui/core/Chip";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Collapse from "@material-ui/core/Collapse";

/* Icons */
import InfoIcon from "@material-ui/icons/Info";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { CircularProgress } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
    "& p.MuiTypography-paragraph:last-of-type": {
      paddingBottom: 0,
    },
  },
  summary: {
    flexGrow: "1",
  },
  logo: {
    maxWidth: "7em",
    maxHeight: "7em",
    width: "7em",
    height: "7em",
    [theme.breakpoints.up("md")]: {
      marginLeft: "1em",
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: "1em",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
    },
    order: 2,
  },
  expand: {},
  expandIcon: {
    transform: "rotate(0deg)",
    // marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  fullDescription: {
    "& a": {
      paddingLeft: "0.4rem",
      paddingRight: "0.4rem",
      backgroundColor: "rgba(242, 102, 64, 0.08)",
      borderBottom: "2px solid #f26640",
      textDecoration: "none",
      color: "#082244",
    },
    "& a:hover": {
      borderBottom: "2px solid #082244",
      textDecoration: "none",
    },
  },
  title: {
    color: "#CE4321",
    fontSize: "1.5 rem",
  },
}));

export default function StallContent(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  // console.log(
  //   "text stall content",
  //   props.stallData.field_fair_stall_description
  // );
  return (
    <Card className={`${props.parentClasses.card}`}>
      <CardContent className={classes.card}>
        {props.stallData.field_fair_stall_logo &&
          props.stallData.field_fair_stall_logo != "" ? (
          <img
            className={classes.logo}
            src={props.stallData.field_fair_stall_logo}
            alt="logo"
          ></img>
        ) : (
          <></>
        )}

        {props.stallData.field_fair_stall_description_summary ? (
          <div className={classes.summary}>
            {ReactHtmlParser(
              props.stallData.field_fair_stall_description_summary
            )}
          </div>
        ) : (
          <div className={classes.summary}>
            {ReactHtmlParser(
              props.stallData.field_fair_stall_description
            )}
          </div>
        )}

        {props.stallData.type == "election_candidate" ? (
          <div>
            {props.stallData.field_candidate_goals && props.stallData.field_candidate_goals != "" ? (
              <div>
                <h3 className={classes.title}>
                  What I plan to do if elected:
                </h3>
                <p>{ReactHtmlParser(props.stallData.field_candidate_goals)}</p>
              </div>
            ) : (
              <></>
            )}

            {props.stallData.field_candidate_skills && props.stallData.field_candidate_skills != "" ? (
              <div>
                <h3 className={classes.title}>
                  My skills and experience:
                </h3>
                <p>
                  {ReactHtmlParser(props.stallData.field_candidate_skills)}
                </p>
              </div>
            ) : (
              <></>
            )}

            {props.stallData.field_candidate_summary && props.stallData.field_candidate_summary != "" ? (
              <div>
                <h3 className={classes.title}>Why you should vote for me:</h3>
                <p>{ReactHtmlParser(props.stallData.field_candidate_summary)}</p>
              </div>
            ) : (
              <></>
            )}
          </div>
        ) : (
          <></>
        )}
      </CardContent>
      {props.stallData.field_fair_stall_description != "" && props.stallData.field_fair_stall_description_summary ? (
        <>
          <CardActions disableSpacing>
            <Button
              data-tour="read_more"
              className={classes.expand}
              onClick={handleExpandClick}
              startIcon={
                <ExpandMoreIcon
                  className={clsx(classes.expandIcon, {
                    [classes.expandOpen]: expanded,
                  })}
                />
              }
            >
              {expanded ? "Read less" : "Read more"}
            </Button>
          </CardActions>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent className={classes.fullDescription}>
              {ReactHtmlParser(props.stallData.field_fair_stall_description)}
            </CardContent>
          </Collapse>
        </>
      ) : (
        <></>
      )}
    </Card>
  );
}
