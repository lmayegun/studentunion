import React, { useState } from "react";

import { customMemoLog } from "../../../utils/Hooks";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */

import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Avatar from "@material-ui/core/Avatar";
import ExtensionIcon from "@material-ui/icons/Extension";

import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import StallSuggestedStall from "./StallSuggestedStall";

/* Styling */
const useStyles = makeStyles((theme) => ({
  card: {},
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
  root: {
    minWidth: 275,
  },
  CardContentForList: {
    padding: 0,
  },
  CardContent: {
    // minHeight: '10vw',
    // maxHeight: '10vw'
  },
}));

// import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
// import CheckBoxIcon from '@material-ui/icons/CheckBox';

function StallSuggested(props) {
  // console.log("RENDER StallSuggested", props);

  const classes = useStyles();

  if (props.stallData && Object.keys(props.suggestedStalls).length > 0) {
    return (
      <Card className={`${props.parentClasses.card}`}>
        <CardHeader title="Suggestions" />
        <CardContent className={classes.CardContentForList}>
          <List>
            {Object.keys(props.suggestedStalls).map((source, i) => {
              //console.log(
              //   "Object.keys(props.suggestedStalls)",
              //   source,
              //   props.suggestedStalls[source]
              // );
              const stall = window.stalls[props.suggestedStalls[source][0]];
              return (
                stall &&
                props.stallData.id != stall.id && (
                  <>
                    {/* <ListItem disableGutters={true}> */}
                    <StallSuggestedStall
                      name={stall.name}
                      source={source}
                      id={stall.id}
                      openHandler={props.openHandler}
                      scrollToTopHandler={props.scrollToTopHandler}
                      imgUrl={stall.field_fair_stall_logo}
                      icon={props.suggestedStalls[source][1]}
                    />
                    {/* </ListItem> */}
                    {i < Object.keys(props.suggestedStalls).length - 1 ? (
                      <Divider component="li" />
                    ) : (
                      <></>
                    )}
                  </>
                )
              );
            })}
          </List>
        </CardContent>
      </Card>
    );
  } else {
    return <></>;
  }
}

export default React.memo(StallSuggested, (prevProps, nextProps) => {
  const propsMatch = prevProps.suggestedStalls === nextProps.suggestedStalls;
  customMemoLog("StallSuggested", prevProps, nextProps, propsMatch);
  return propsMatch;
});
