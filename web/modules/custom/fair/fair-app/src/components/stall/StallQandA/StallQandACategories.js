import React from "react";

import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(1),
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(0.5),
    },
  },
}));

export default function StallQandACategories(props) {
  const classes = useStyles();

  if (props.data.length === 1) {
    return <></>;
  } else {
    return (
      <div className={classes.root}>
        {props.data.map((category) => (
          <Chip
            label={category}
            color={
              props.selected && props.selected === category
                ? "primary"
                : "default"
            }
            onClick={() => props.clickHandler(category)}
          />
        ))}
      </div>
    );
  }
}
