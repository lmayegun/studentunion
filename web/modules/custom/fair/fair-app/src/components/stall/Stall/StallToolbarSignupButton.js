import React from "react";

import StallToolbarSignupForm from "./StallToolbarSignupForm";

/* Material-UI components */
import Button from "@material-ui/core/Button";

/* Icons */
import ContactMailIcon from "@material-ui/icons/ContactMail";

export default function StallToolbarSignupButton(props) {
  const [formOpen, setFormOpen] = React.useState(false);

  const handleFormClose = () => {
    setFormOpen(false);
  };

  const subscribed = props.stallUserActions && props.stallUserActions.subscribe ? props.stallUserActions.subscribe.subscribed : false;

  return (
    <span>
      <Button
        variant="contained"
        className={props.classes.menuButton}
        color="secondary"
        onClick={() => {
          setFormOpen(true);
        }}
        startIcon={<ContactMailIcon />}
      >
        { subscribed ? "Remove signup" : "Sign up" }
      </Button>

      <StallToolbarSignupForm
        open={formOpen}
        subscribed={subscribed}
        handleClose={handleFormClose}
        saveDataViaAPIhandler={props.saveDataViaAPIhandler}
      />
    </span>
  );
}
