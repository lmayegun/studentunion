import React, { useState } from "react";

import ReactHtmlParser from "react-html-parser";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
// import CardMedia from "@material-ui/core/CardMedia";
import Avatar from "@material-ui/core/Avatar";
import RecordVoiceOverIcon from "@material-ui/icons/RecordVoiceOver";
import { CardActions, Button } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  card: {},
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
}));

export default function StallLinkedStall(props) {
  const classes = useStyles();

  var id = 0;
  var title = "";
  var description = "";

  const useFairClubSocInfo =
    props.stallType === "club_society" &&
    window.fairData.field_cs_associated_stall &&
    window.fairData.field_cs_associated_stall[0];

  const useStallSpecificInfo =
    props.stallData.field_associated_stall &&
    props.stallData.field_associated_stall.length > 0;

  if (useFairClubSocInfo) {
    id = window.fairData.field_cs_associated_stall[0];
    title = window.fairData.field_cs_associated_stall_title;
    description = window.fairData.field_cs_associated_stall_desc;
  } else if (useStallSpecificInfo) {
    id = props.stallData.field_associated_stall[0];
    const stall = window.stalls[id];
    title = props.stallData.field_associated_stall_title
      ? props.stallData.field_associated_stall_title
      : stall.name;
    description = props.stallData.field_associated_stall_descripti
      ? props.stallData.field_associated_stall_descripti
      : stall.fair_stall_tagline;
  }

  if (id > 0 && title != "") {
    return (
      <Card className={`${props.parentClasses.card}`}>
        <CardHeader title={title}></CardHeader>
        {/* <CardMedia></CardMedia> */}
        <CardContent>{ReactHtmlParser(description)}</CardContent>
        <CardActions>
          <Button
            onClick={() => {
              props.openHandler(null);
              const pause = setTimeout(() => {
                props.openHandler(id);
                props.scrollToTopHandler();
              }, 500);
            }}
            color="primary"
          >
            Visit stall
          </Button>
        </CardActions>
      </Card>
    );
  } else {
    return <></>;
  }
}
