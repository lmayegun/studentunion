import React from "react";

import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  counter: {
    textAlign: "center",
    width: "33%",
    // marginRight: theme.spacing(2),
  },
  countNumber: {
    fontSize: "1.5em",
    fontWeight: "bold",
  },
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
}));

export default function StallVisitorStats(props) {
  const classes = useStyles();

  if (props.visitorsTotal || props.membersJoined || props.visitorsOnline) {
    return (
      <TableContainer>
        {/* component={Card} */}
        <Table>
          <TableBody>
            <TableRow>
              {props.visitorsTotal && props.visitorsTotal >= 5 ? (
                <TableCell className={classes.counter}>
                  <div className={classes.countNumber}>
                    {props.visitorsTotal}
                  </div>
                  {" visitors so far"}
                </TableCell>
              ) : (
                <></>
              )}

              {props.membersJoined && props.membersJoined >= 3 ? (
                <TableCell className={classes.counter}>
                  <div className={classes.countNumber}>
                    {props.membersJoined}
                  </div>
                  {" taster members joined"}
                </TableCell>
              ) : (
                <></>
              )}

              {props.visitorsOnline && props.visitorsOnline >= 3 ? (
                <TableCell className={classes.counter}>
                  <div className={classes.countNumber}>
                    {props.visitorsOnline}
                  </div>
                  {" visitors right now"}
                </TableCell>
              ) : (
                <></>
              )}
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  } else {
    return <></>;
  }
}
