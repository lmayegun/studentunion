import React, { useState } from "react";

import StallToolbarClubsSocJoinForm from "./StallToolbarClubsSocJoinForm";
import StallToolbarClubsSocJoinFreeForm from "./StallToolbarClubsSocJoinFreeForm";

/* Material-UI components */
import Button from "@material-ui/core/Button";

/* Icons */
import CardMembershipIcon from "@material-ui/icons/CardMembership";
import LoginDialog from "../../dialogs/LoginDialog";
import StallToolbarSignupForm from "./StallToolbarSignupForm";

export default function StallToolbarCallToActionButton(props) {
  const searchParams = new URLSearchParams(window.location.search);
  const [formJoinOpen, setFormJoinOpen] = React.useState(
    searchParams.has("join") && props.currentStallProducts.length > 0
  );
  const [formJoinFreeOpen, setFormJoinFreeOpen] = React.useState(
    searchParams.has("join") && props.currentStallProducts.length === 0
  );
  const [formSignupOpen, setFormSignupOpen] = React.useState(
    searchParams.has("signup")
  );

  const [loginDialogOpen, setLoginDialogOpen] = useState(false);

  const handleFormJoinClose = () => {
    setFormJoinOpen(false);
  };
  const handleFormJoinFreeClose = () => {
    setFormJoinFreeOpen(false);
  };
  const handleFormSignupClose = () => {
    setFormSignupOpen(false);
  };

  const joined =
    props.stallUserActions && props.stallUserActions.join
      ? props.stallUserActions.join.joined
      : false;

  const inCart =
    props.stallUserActions && props.stallUserActions.join && props.stallUserActions.join.in_cart
      ? props.stallUserActions.join.in_cart
      : false;

  const openFreeDialog = () => {
    setFormJoinOpen(false);
    setFormJoinFreeOpen(true);
  };

  return (
    <span>
      <Button
        variant="contained"
        className={props.classes.menuButton}
        color="secondary"
        onClick={() => {
          if (props.userLoggedIn) {
            if (inCart) {
              window.open('/cart');
            } else if (props.currentStallProducts.length > 0) {
              setFormJoinOpen(true);
            }
          } else {
            setLoginDialogOpen(true);
          }
        }}
        startIcon={<CardMembershipIcon />}
      >
        {inCart ? 'Go to basket to join now' : "Join"}
      </Button>

      <LoginDialog
        open={loginDialogOpen}
        action={"join"}
        cancelLoginDialog={() => {
          setLoginDialogOpen(false);
        }}
        handleNoUcl={() => {
          setLoginDialogOpen(false);
          setFormSignupOpen(true);
        }}
      />

      <StallToolbarClubsSocJoinForm
        open={formJoinOpen}
        handleClose={handleFormJoinClose}
        saveDataViaAPIhandler={props.saveDataViaAPIhandler}
        joined={joined}
        currentStallProducts={props.currentStallProducts}
        openFreeDialog={openFreeDialog}
        stallData={props.stallData ? props.stallData : {}}
        inCart={inCart}
      />

      <StallToolbarClubsSocJoinFreeForm
        open={formJoinFreeOpen}
        handleClose={handleFormJoinFreeClose}
        saveDataViaAPIhandler={props.saveDataViaAPIhandler}
        joined={joined}
        currentStallProducts={props.currentStallProducts}
      />

      <StallToolbarSignupForm
        open={formSignupOpen}
        handleClose={handleFormSignupClose}
        saveDataViaAPIhandler={props.saveDataViaAPIhandler}
      />
    </span >
  );
}
