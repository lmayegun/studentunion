import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import StallToolbarCallToActionButton from "./StallToolbarCallToActionButton";
import StallToolbarClubsSocJoinButton from "./StallToolbarClubsSocJoinButton";
import StallToolbarSignupButton from "./StallToolbarSignupButton";
import StallToolbarSaveButton from "./StallToolbarSaveButton";

import EditIcon from "@material-ui/icons/Edit";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    margin: theme.spacing(1),
  },
}));

function StallToolbarButtons(props) {
  const isStudent = props.userDetails.user_student;
  const classes = useStyles();
  //console.log("StallToolbarButtons", props);

  const showEdit = props.userIsStallManager;

  // console.log('showJoin', props.stallData.type, props.joinEnabledForFair, props.stallData.field_enable_joining);
  const showJoin =
    props.stallData.type === "club_society" &&
    props.joinEnabledForFair &&
    props.stallData.field_enable_joining === "1" &&
    (!props.userLoggedIn ||
      (props.userLoggedIn && (isStudent || props.userIsStallManager)));

  const showMailingList =
    !showJoin &&
    props.stallData.field_fair_stall_mailing_list_on &&
    props.stallData.field_fair_stall_mailing_list_on === "1";

  const showCallToAction =
    props.stallData.field_fair_stall_to_action_url &&
    props.stallData.field_fair_stall_to_action_url[0] &&
    props.stallData.field_fair_stall_to_action_url[0].title != "";

  return (
    <>
      {showEdit ? (
        <Button
          variant="contained"
          className={classes.menuButton}
          color="secondary"
          onClick={() => {
            if (props.stallData.entity_type == 'group') {
              window.open(
                "/group/" + props.stallData.group_id + "/edit"
              );
            } else if (props.stallData.entity_type == 'election_candidate') {
              window.open(
                props.stallData.edit_link
              );

            } else {
              window.open(
                "/admin/content/fair/stall/" + props.stallData.id + "/edit"
              );
            }
          }}
          startIcon={<EditIcon />}
        >
          Edit stall
        </Button>
      ) : (
        <></>
      )
      }
      {
        showJoin ? (
          <StallToolbarClubsSocJoinButton
            stallData={props.stallData ? props.stallData : {}}
            userLoggedIn={props.userLoggedIn}
            stallUserActions={props.stallUserActions}
            classes={classes}
            saveDataViaAPIhandler={props.saveDataViaAPIhandler}
            currentStallProducts={props.currentStallProducts}
          />
        ) : (
          <></>
        )
      }
      {
        showMailingList ? (
          <StallToolbarSignupButton
            stallData={props.stallData ? props.stallData : {}}
            userLoggedIn={props.userLoggedIn}
            classes={classes}
            stallUserActions={props.stallUserActions}
            saveDataViaAPIhandler={props.saveDataViaAPIhandler}
          />
        ) : (
          <></>
        )
      }
      {
        showCallToAction ? (
          <StallToolbarCallToActionButton
            stallData={props.stallData ? props.stallData : {}}
            classes={classes}
          />
        ) : (
          <></>
        )
      }
      <StallToolbarSaveButton
        classes={classes}
        handleSave={props.handleSave}
        stallUserActions={props.stallUserActions}
        userLoggedIn={props.userLoggedIn}
      />
    </>
  );
}

export default StallToolbarButtons;

// export default React.memo(StallToolbarButtons, (prevProps, nextProps) => {
//   return (
//     prevProps.stallUserActions === nextProps.stallUserActions &&
//     prevProps.userIsStallManager === nextProps.userIsStallManager
//   );
// });
