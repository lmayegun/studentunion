import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { PageView, initGA, Event } from "../../../utils/Tracking";

import { customMemoLog } from "../../../utils/Hooks";

/* Material-UI components */
import Card from "@material-ui/core/Card";
import Link from "@material-ui/core/Link";
import { CardMedia, CardActionArea, CardHeader } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {},
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
  media: {
    height: 500,
  },
}));

function StallCardImage(props) {
  const classes = useStyles();

  if (props.image) {
    return (
      <Card className={`${props.parentClasses.card}`}>
        <CardActionArea>
            <CardMedia
              className={classes.media}
              title="Candidate image"
              image={props.image}
            />
        </CardActionArea>
      </Card>
    );
  } else {
    return <></>;
  }
}

export default React.memo(StallCardImage, (prevProps, nextProps) => {
  const propsMatch =
    prevProps.image === nextProps.image;
  customMemoLog("StallCardImage", prevProps, nextProps, propsMatch);
  return propsMatch;
});
