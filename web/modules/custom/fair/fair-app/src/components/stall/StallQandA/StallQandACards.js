import React, { useState, useCallback, useRef, useLayoutEffect } from "react";
import StallQandACard from "./StallQandACard";

import { customMemoLog } from "../../../utils/Hooks";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

import useStayScrolled from "react-stay-scrolled";

/* Material-UI components */

import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Avatar from "@material-ui/core/Avatar";

import "./StallQandA.css";

import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import { ListItemText, Grow } from "@material-ui/core";

/* Styling */
const useStyles = makeStyles((theme) => ({
  list: { maxHeight: "70vh", overflowY: "scroll" },
}));

function StallQandACards(props) {
  const [notifyNewMessage, setNotifyNewMessage] = useState(false);
  const classes = useStyles();
  const ref = useRef();

  const { stayScrolled, isScrolled } = useStayScrolled(ref);

  // The element just scrolled up to top - remove new messages notification, if any
  const onScroll = useCallback(() => {
    if (isScrolled()) setNotifyNewMessage(true);
  }, []);

  useLayoutEffect(() => {
    // Tell the user to scroll up to see the newest messages if the element was scrolled
    setNotifyNewMessage(!stayScrolled());
  }, [props.filteredComments.length]);

  //console.log("StallQandACards", props);

  return (
    <List ref={ref} onScroll={onScroll} className={classes.list}>
      {!props.filteredComments || props.filteredComments.length === 0 ? (
        "No questions yet - why not ask one?"
      ) : (
        <></>
      )}
      {props.filteredComments &&
        Object.keys(props.filteredComments).map((commentId) => (
          <>
            <StallQandACard
              key={commentId}
              stallId={props.stallData.id}
              userIsStallManager={props.userIsStallManager}
              comment={props.filteredComments[commentId]}
              saveCommentHandler={props.saveCommentHandler}
              deleteCommentHandler={props.deleteCommentHandler}
              addDummyCommentFromForm={props.addDummyCommentFromForm}
            />
            <Divider component="li" />
          </>
        ))}
      <Grow in={notifyNewMessage}>
        <ListItem dense={true}>
          <ListItemText>Scroll up to see new comments</ListItemText>
        </ListItem>
      </Grow>
    </List>
  );
}

// export default StallQandACards;

export default React.memo(StallQandACards, (prevProps, nextProps) => {
  const propsMatch = prevProps.filteredComments === nextProps.filteredComments;
  customMemoLog("StallQandACards", prevProps, nextProps, propsMatch, true);
  return propsMatch;
});
