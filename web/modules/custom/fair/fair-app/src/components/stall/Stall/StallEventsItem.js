import React from "react";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import { Typography } from "@material-ui/core";
import Moment from "react-moment";
import ReactHtmlParser from "react-html-parser";

export default function StallEventsItem(props) {
  return (
    <ListItem
      button
      onClick={() => {
        window.open(props.url);
      }}>
      <ListItemAvatar>
        <Avatar src={props.img} alt="avatar" color="secondary">{props.icon}</Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={ReactHtmlParser(props.name)}
        secondary={props.date}
      />
    </ListItem>
  );
}
