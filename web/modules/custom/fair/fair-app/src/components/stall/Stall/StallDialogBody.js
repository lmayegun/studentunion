import React, { useRef, useCallback, useEffect, useState } from "react";

import _ from "lodash";

import { makeStyles, useTheme } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

import { customMemoLog } from "../../../utils/Hooks";

import StallLiveEvent from "./StallLiveEvent";
import StallVisitorStats from "./StallVisitorStats";
import StallContent from "./StallContent";
import StallQandA from "../StallQandA/StallQandA";
import StallSuggested from "./StallSuggested";
import StallLinkedStall from "./StallLinkedStall";
import StallAd from "./StallAd";
import StallSlideshow from "./StallSlideshow";
import StallCategoriesAndTags from "./StallCategoriesAndTags";
import StallEvents from "./StallEvents";
import CloseIcon from "@material-ui/icons/Close";
import StallVideo from "./StallVideo";
import StallCardImage from "./StallCardImage";

const useStyles = makeStyles((theme) => ({
  Container: {
    marginTop: theme.spacing(3),
  },
  MainColumn: {
    // paddingTop: theme.spacing(3),
  },
  card: {
    marginBottom: theme.spacing(2),
    borderRadius: 0,
    borderStyle: "solid",
    borderWidth: "1px",
    borderBottomWidth: "2px",
    borderColor: theme.palette.primary.main,
  },
  cardHeader: {
    padding: 0,
    paddingBottom: theme.spacing(1),
  },
}));

const StallDialogBody = (props) => {
  //console.log("StallDialogBody", props);

  const classes = useStyles();
  const theme = useTheme();

  const [QandACategories, setQandACategories] = useState(["All categories"]);
  const [QandASelectedCategory, setQandASelectedCategory] = useState(
    "All categories"
  );
  const [QandAFilteredComments, setQandAFilteredComments] = useState([]);

  const changeQandACategory = useCallback(
    (newCategory) => {
      //console.log(
      //   "changeQandACategory",
      //   newCategory,
      //   props.currentStallComments
      // );

      if (!props.stallData) {
        return false;
      }

      if (!props.currentStallComments) {
        return false;
      }

      let newComments = {};
      let newCategories = ["All categories"];

      // Get comments without parents
      //console.log("props.currentStallComments", props.currentStallComments);
      props.currentStallComments.forEach((comment) => {
        if (comment.pid === "" || !comment.pid) {
          comment.children = [];
          comment.id = comment.cid;
          comment.sortTime = comment.created;
          newComments[comment.cid] = comment;
          // console.log("keep ", comment);
        } else {
          // console.log("dont keep ", comment);
        }
      });
      // console.log("Top level comments", newComments);

      // Add children to parents
      props.currentStallComments.forEach((comment) => {
        if (comment.pid != "" && newComments[comment.pid]) {
          //console.log("comment.pid", comment.pid);
          if (!newComments[comment.pid].children) {
            newComments[comment.pid].children = [];
          }
          newComments[comment.pid].children.push(comment);
          newComments[comment.pid].sortTime = comment.created;
          if (comment.field_comment_category != "") {
            newCategories.push(comment.field_comment_category);
          }
        }
      });
      // console.log("Top level comments with children", newComments);

      // Unique AND NON-EMPTY categories
      newCategories = newCategories.filter(function (el, index, arr) {
        return el != "" && index === arr.indexOf(el);
      });
      setQandACategories((oldCategories) => newCategories);

      // Filter top level by category given to the child (which is how the stall
      // manager categories the Q and A)
      if (newCategory !== "All categories") {
        Object.keys(newComments).forEach((cid) => {
          const comment = newComments[cid];
          const categoryMatch =
            comment.children.length > 0 &&
            comment.children[0].field_comment_category === newCategory;
          if (!categoryMatch) {
            delete newComments[cid];
          }
        });
      }

      if (!props.userIsStallManager) {
        //console.log(
        //   "If not a stall manager or author, hide comments without a reply."
        // );
        Object.keys(newComments).forEach((cid) => {
          const comment = newComments[cid];
          const hasChildren = comment.children.length > 0;
          const userIsAuthor =
            window.userDetails && comment.uid === window.userDetails.uid;

          const keep = hasChildren || userIsAuthor;
          if (!keep) {
            delete newComments[cid];
          }
        });
      }

      //console.log("setQandAFilteredComments", newComments);
      newComments = _.sortBy(newComments, "sortTime").reverse();
      setQandAFilteredComments((oldComments) => newComments);
      setQandASelectedCategory((oldCategory) => newCategory);
    },
    [props.currentStallComments]
  );

  useEffect(() => {
    changeQandACategory(QandASelectedCategory);
  }, [props.currentStallComments]);

  const scrollToTop = () => {
    //console.log("scrollToTop", props.dialog);
    props.dialog.scrollIntoView({
      behavior: "smooth",
      block: "start",
    });
  };

  if (props.stallData) {
    // console.log("stallData", stallData);
    const showVisitors = false;
    // props.stallUserActions &&
    // props.stallUserActions.visitors &&
    // (props.stallUserActions.visitors.total ||
    //   props.stallUserActions.visitors.now ||
    //   props.stallUserActions.visitors.joined);

    return (
      <Container className={classes.Container}>
        <StallLiveEvent stallData={props.stallData}></StallLiveEvent>
        {showVisitors ? (
          <Grid className={classes.MainColumn} container spacing={2}>
            {/* <Grid item xs={12} md={8}></Grid> */}
            {/* <Grid item xs={12} md={4}> */}
            <StallVisitorStats
              parentClasses={classes}
              visitorsTotal={props.stallUserActions.visitors.total}
              visitorsOnline={props.stallUserActions.visitors.now}
              membersJoined={props.stallUserActions.visitors.joined}
              usersOnline={[]}
            />
            {/* </Grid> */}
          </Grid>
        ) : (
          <></>
        )}
        <Grid container spacing={2}>
          <Grid item xs={12} md={8}>
            <StallContent
              stallData={props.stallData}
              parentClasses={classes}
            ></StallContent>
            {props.stallData.field_fair_stall_qanda_enabled === "1" &&
            window.fairData.field_fair_qanda_enabled === "1" ? (
              <StallQandA
                title={
                  window.fairData.field_fair_q_a_title &&
                  window.fairData.field_fair_q_a_title != ""
                    ? window.fairData.field_fair_q_a_title
                    : "Ask us anything"
                }
                stallData={props.stallData}
                parentClasses={classes}
                saveCommentHandler={props.saveCommentHandler}
                deleteCommentHandler={props.deleteCommentHandler}
                changeQandACategory={changeQandACategory}
                addDummyComment={props.addDummyComment}
                categories={QandACategories}
                selectedCategory={QandASelectedCategory}
                filteredComments={QandAFilteredComments}
                currentStallComments={props.currentStallComments}
                userIsStallManager={props.userIsStallManager}
              ></StallQandA>
            ) : (
              <></>
            )}
            {props.stallData.field_fair_stall_videos &&
            props.stallData.field_fair_stall_videos.length > 1 ? (
              <StallVideo
                card={true}
                parentClasses={classes}
                stallData={props.stallData}
              />
            ) : (
              <></>
            )}
            {props.stallData.field_fair_stall_slide_images &&
            props.stallData.field_fair_stall_slide_images.length > 0 ? (
              <StallSlideshow
                card={true}
                slideshowImages={props.stallData.field_fair_stall_slide_images}
                sliderHeight="600px"
                sliderWidth="100%"
              />
            ) : (
              <></>
            )}
          </Grid>
          <Grid item xs={12} md={4}>
            <StallEvents
              parentClasses={classes}
              currentStallEvents={props.currentStallEvents}
            />
            <StallLinkedStall
              stallData={props.stallData}
              parentClasses={classes}
              scrollToTopHandler={scrollToTop}
              openHandler={props.openHandler}
            />
            {window.fairData.field_fair_stall_ads === "1" ? (
              <StallAd
                currentStallAdvert={props.currentStallAdvert}
                parentClasses={classes}
                fairId={props.fairId}
              ></StallAd>
            ) : (
              <></>
            )}
            {props.stallData.type == "election_candidate" ? (
              <StallCardImage 
                parentClasses={classes} 
                image={props.stallData.fair_stall_card[0]} 
              />
            ) : (
              <></>
            )}
            <StallSuggested
              stallData={props.stallData}
              suggestedStalls={props.suggestedStalls}
              openHandler={props.openHandler}
              scrollToTopHandler={scrollToTop}
              parentClasses={classes}
            ></StallSuggested>
          </Grid>
        </Grid>
      </Container>
    );
  } else {
    return <></>;
  }
};

export default StallDialogBody;

// export default React.memo(StallDialogBody, (prevProps, nextProps) => {
//   const propsMatch = prevProps.stallData === nextProps.stallData;
//   customMemoLog("StallDialogBody", prevProps, nextProps, propsMatch);
//   return propsMatch;
// });
