import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

/* Material-UI components */
import Button from "@material-ui/core/Button";
import StarIcon from "@material-ui/icons/Star";
import StarBorderOutlinedIcon from "@material-ui/icons/StarBorderOutlined";

import LoginDialog from "../../dialogs/LoginDialog";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    // position: "relative",
  },
  buttonProgress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  buttonLoading: {
    "& .MuiButton-startIcon": {
      animationName: "spin",
      animationDuration: "2000ms",
      animationIterationCount: "infinite",
      animationTimingFunction: "linear",
    },
  },
}));

export default function StallToolbarSaveButton(props) {
  //console.log("StallToolbarSaveButton", props);
  const classes = useStyles();

  const [loading, setLoading] = React.useState(false);

  const [loginDialogOpen, setLoginDialogOpen] = useState(false);

  const save = async () => {
    await props.handleSave();
    setLoading(false);
  };

  const favourited =
    props.stallUserActions && props.stallUserActions.favourite
      ? props.stallUserActions.favourite.favourited
      : false;

  //console.log("favourited", props.stallUserActions, favourited);

  return (
    <span className={classes.wrapper}>
      <Button
        variant="contained"
        className={clsx(props.classes.menuButton, {
          [classes.buttonLoading]: loading,
        })}
        color="primary"
        onClick={async () => {
          setLoading(true);
          if (props.userLoggedIn) {
            await save();
          } else {
            setLoginDialogOpen(true);
            setLoading(false);
          }
        }}
        disabled={loading}
        startIcon={favourited ? <StarIcon /> : <StarBorderOutlinedIcon />}
      >
        {favourited ? "Unsave" : "Save"}
      </Button>

      <LoginDialog
        open={loginDialogOpen}
        action={"save"}
        cancelLoginDialog={() => {
          setLoginDialogOpen(false);
        }}
        handleNoUcl={() => {
          setLoginDialogOpen(false);
        }}
      />
    </span>
  );
}
