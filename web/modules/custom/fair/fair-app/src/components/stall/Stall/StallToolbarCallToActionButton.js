import React from "react";

/* Material-UI components */
import Button from "@material-ui/core/Button";

/* Icons */
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

export default function StallToolbarCallToActionButton(props) {
  const openLink = (url) => {
    window.open(url);
  };

  if (props.stallData.field_fair_stall_to_action_url &&
    props.stallData.field_fair_stall_to_action_url[0] &&
    props.stallData.field_fair_stall_to_action_url[0].title != "") {
    return (
      <span>
        <Button
          variant="contained"
          className={props.classes.menuButton}
          color="secondary"
          onClick={() => {
            openLink(props.stallData.field_fair_stall_to_action_url[0].url);
          }}
          startIcon={<ArrowForwardIcon />}
        >
          {props.stallData.field_fair_stall_to_action_url[0].title}
        </Button>
      </span>
    );
  } else {
    return <></>;
  }
}