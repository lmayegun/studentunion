import React, { useState } from "react";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";

import ReactHtmlParser from "react-html-parser";

/* Styles */
import { makeStyles, useTheme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(2),
    // paddingTop: theme.spacing(3),
  },
}));

export default function StallCategoriesAndTags(props) {
  const classes = useStyles();

  // console.log("cats", props);

  const runSearch = (field, value) => {
    this.props.runSearchHandler(field, value);
  };

  if (
    !props.category ||
    (props.category.length === 0 && props.tags.length === 0)
  ) {
    return <></>;
  } else {
    return (
      <Grid className={classes.root} container spacing={1} alignItems="center">
        {props.category.length > 0 ? (
          <Grid item>
            <Chip
              color="secondary"
              label={ReactHtmlParser(props.category)}
              onClick={() => runSearch("category", props.category)}
            />
          </Grid>
        ) : (
          <></>
        )}
        {props.tags.length > 0 &&
          props.tags.map((tag) => {
            return (
              <Grid item>
                <Chip
                  color="secondary"
                  label={ReactHtmlParser(tag)}
                  size="small"
                  onClick={() => runSearch("tags", tag)}
                />
              </Grid>
            );
          })}
      </Grid>
    );
  }
}
