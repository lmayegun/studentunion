import React, { useState } from "react";

import Moment from "react-moment";
import moment from "moment";

import ReactHtmlParser from "react-html-parser";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import Avatar from "@material-ui/core/Avatar";
import LockIcon from "@material-ui/icons/Lock";
import { Link } from "react-router-dom";
import {
  ListItemSecondaryAction,
  IconButton,
  Button,
  Grow,
} from "@material-ui/core";

/* Other components */
import StallQandAReplyDialog from "./StallQandAReplyDialog";
import StallQandACardMessageBox from "./StallQandACardMessageBox";

/* Styling */
const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 300,
  },
  CardContent: {
    // minHeight: '20vw',
    // maxHeight: '20vw'
  },
  coral: {
    color: "#FFFFFF",
    backgroundColor: "#f26640",
  },
  navy: {
    color: "#FFFFFF",
    backgroundColor: "#082244",
  },
  message: {
    "& .MuiListItemText-primary p:last-of-type": {
      paddingBottom: "3px",
    },
    "& .MuiListItemText-secondary": {
      paddingBottom: "0px",
    },
  },
}));

export default function StallQandACard(props) {
  const classes = useStyles();

  const [replyDialogOpen, setReplyDialogOpen] = useState(false);

  const goToUrl = (url) => {
    window.open(url);
  };

  console.log("props.comment", props.comment);

  return (
    <>
      <StallQandACardMessageBox
        id={props.comment.cid}
        firstName={props.comment.field_first_name}
        lastName={props.comment.field_last_name}
        userRole={props.comment.field_comment_user_role}
        dummy={props.comment.dummy}
        text={props.comment.comment_body}
        time={props.comment.created}
        color={classes.navy}
        userIsStallManager={props.userIsStallManager}
        avatarImageUrl={props.comment.user_picture}
        stallId={props.stallId}
        child={false}
        setReplyDialogOpen={setReplyDialogOpen}
        deleteCommentHandler={props.deleteCommentHandler}
      />
      {props.comment.children &&
        props.comment.children.length > 0 &&
        props.comment.children.map((comment) => {
          return (
            <StallQandACardMessageBox
              id={comment.cid}
              firstName={comment.field_first_name}
              lastName={comment.field_last_name}
              userRole={props.comment.field_comment_user_role}
              text={comment.comment_body}
              time={comment.created}
              color={classes.coral}
              child={true}
              userIsStallManager={props.userIsStallManager}
              avatarImageUrl={comment.user_picture}
              setReplyDialogOpen={setReplyDialogOpen}
              deleteCommentHandler={props.deleteCommentHandler}
            />
          );
        })}
      {props.userIsStallManager ? (
        <StallQandAReplyDialog
          open={replyDialogOpen}
          parentId={props.comment.uuid}
          commentContent={props.comment.comment_body}
          saveCommentHandler={props.saveCommentHandler}
          addDummyCommentFromForm={props.addDummyCommentFromForm}
          cancelReplyDialog={() => {
            setReplyDialogOpen(false);
          }}
        />
      ) : (
        <></>
      )}
    </>
  );
}
