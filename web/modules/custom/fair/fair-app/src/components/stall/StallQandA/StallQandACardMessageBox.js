import React, { useState } from "react";

import Moment from "react-moment";

import ReactHtmlParser from "react-html-parser";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

import StallQandACardDeleteButton from "./StallQandACardDeleteButton";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import Avatar from "@material-ui/core/Avatar";
import LockIcon from "@material-ui/icons/Lock";
import { Link } from "react-router-dom";
import {
  ListItemSecondaryAction,
  IconButton,
  Button,
  Grow,
} from "@material-ui/core";

import ReplyIcon from "@material-ui/icons/Reply";

/* Styling */
const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 300,
  },
  CardContent: {
    // minHeight: '20vw',
    // maxHeight: '20vw'
  },
  coral: {
    color: "#FFFFFF",
    backgroundColor: "#f26640",
  },
  navy: {
    color: "#FFFFFF",
    backgroundColor: "#082244",
  },
  message: {
    whiteSpace: "pre-line",
    "& .MuiListItemText-primary p:last-of-type": {
      paddingBottom: "3px",
    },
    "& .MuiListItemText-secondary": {
      paddingBottom: "0px",
    },
  },
  messagePrimary: {
    fontWeight: "bold"
  },
  actions: {
    display: 'flex',
    flexDirection: 'row-reverse',
    padding: theme.spacing(1)
  }
}));

export default function StallQandACardMessageBox(props) {
  const classes = useStyles();

  const [deleted, setDeleted] = useState(false);

  const deleteComment = async (commentId) => {
    console.log("deleteComment", commentId);
    const success = await props.deleteCommentHandler(commentId);
    if(success) {
      setDeleted(true);
    }
  };

  const showDelete = props.userIsStallManager;
  const showReply = props.userIsStallManager && !props.dummy && !props.child;
  
  return (
    <Grow in={!deleted}>
      <>
      <ListItem dense={true}>
        <ListItemAvatar>
          <Avatar className={props.color} src={props.avatarImageUrl}>
            {props.firstName ? props.firstName.charAt(0) : ""}
            {props.lastName ? props.lastName.charAt(0) : ""}
          </Avatar>
        </ListItemAvatar>
        <ListItemText 
          className={`${classes.message} ${!props.child ? classes.messagePrimary : ''}`}
          primary={ReactHtmlParser(props.text)}
          secondary={
            <>
              {props.firstName ? (
                <span>
                  {ReactHtmlParser(props.firstName)}
                  {", "}
                </span>
              ) : (
                <></>
              )}
              {props.userRole ? (
                <span>
                  {ReactHtmlParser(props.userRole)}
                  {", "}
                </span>
              ) : (
                <></>
              )}
              {props.time ? <Moment unix fromNow>{props.time}</Moment> : <></>}
            </>
          }
        ></ListItemText>
        
      </ListItem>
      {showDelete || showReply ? (
          <div className={classes.actions}>
            {showDelete ? (
              <StallQandACardDeleteButton
                id={props.id}
                deleteCommentHandler={deleteComment}
              />
            ) : (<></>)}
            {showReply ? (
              <Button
                target="_blank"
                rel="noopener noreferrer"
                onClick={() =>
                  // goToUrl(
                  //   `/comment/reply/fair_stall/${props.stallId}/field_fair_stall_comments/${props.id}`
                  // )
                  props.setReplyDialogOpen(true)
                }
                color="inherit"
                edge="end"
                startIcon={<ReplyIcon />}
              >
                Reply
              </Button>
            ) : (<></>)}
          </div>
        ) : (
          <></>
        )}
        </>
    </Grow>
  );
}
