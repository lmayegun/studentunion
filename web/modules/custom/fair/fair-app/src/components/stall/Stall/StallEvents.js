import React, { useState } from "react";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */

import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import Avatar from "@material-ui/core/Avatar";
import EventIcon from "@material-ui/icons/Event";

import StallEventsItem from "./StallEventsItem";
import { ListItem } from "@material-ui/core";

/* Styling */
const useStyles = makeStyles((theme) => ({
  card: {},
  cardIcon: {
    color: "#CE4321",
    backgroundColor: theme.palette.primary.contrastText,
  },
  root: {
    minWidth: 275,
  },
  CardContent: {
    // minHeight: '10vw',
    // maxHeight: '10vw'
  },
  title: {
    color: "#CE4321",
    fontSize: "1.5 rem",
  },
}));

export default function StallEvents(props) {
  const classes = useStyles();

  console.log("StallEvents", props);

  if (props.currentStallEvents && props.currentStallEvents.length > 0) {
    return (
      <Card className={`${props.parentClasses.card}`}>
        <CardHeader className={classes.title} title="Upcoming events" />
        <List>
          {props.currentStallEvents.map((event, i) => (
            <>
              {/* <ListItem disableGutters={true}> */}
              <StallEventsItem
                key={"event" + i}
                name={event.title}
                location={""}
                timestamp={""}
                date={event.field_date_range}
                url={event.path}
                img={event.image}
                icon={<EventIcon />}
              ></StallEventsItem>
              {/* </ListItem> */}
              {i < props.currentStallEvents.length - 1 ? (
                <Divider component="li" />
              ) : (
                <></>
              )}
            </>
          ))}
        </List>
      </Card>
    );
  } else {
    return <></>;
  }
}
