import React, { useState, useEffect } from "react";
/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Alert from "@material-ui/lab/Alert";

import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import { List, ListItem, ListItemText } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  list: {
    display: "flex",
    justifyContent: "center",
  },
  buttonLoading: {
    "& .MuiButton-startIcon": {
      animationName: "spin",
      animationDuration: "2000ms",
      animationIterationCount: "infinite",
      animationTimingFunction: "linear",
    },
  },
  FormGroup: {
    width: "100%",
  },
}));

export default function StallToolbarClubsSocJoinForm(props) {
  const classes = useStyles();
  //console.log("StallToolbarClubsSocJoinForm", props);

  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);

  const StallToolbarClubsSocJoinFormContent = (props) => {
    return (
      <>
        <DialogTitle id="form-dialog-title">Join</DialogTitle>
        <DialogContent>
          <DialogContentText>
            This club or society has both a free Taster Membership and paid
            membership{props.currentStallProducts.length > 1 ? "s" : ""}. If you choose
            the free membership you can upgrade to the paid one at any time.
          </DialogContentText>
          <List>
            {props.currentStallProducts.map((productVariation) => {
              return (
                <ListItem
                  button
                  onClick={async () => {
                    setError("");
                    // if (validate()) {
                    setLoading(true);
                    const success = await props.saveDataViaAPIhandler("add_to_cart", {
                      'variation_id': productVariation.variation_id
                    });
                    console.log("Add to cart success", success);
                    if (success) {
                      setOpenAlert(true);
                      // if (props.handleClose) {
                      //   props.handleClose();
                      // }
                    } else {
                      setError("Unable to add to cart right now.");
                    }
                    setLoading(false);
                    // }
                  }}
                // onClick={() => {
                //   const basePath =
                //     "https://studentsunionucl.org/user/login-options?destination=";
                //   const destinationPath =
                //     "commerce/add-to-cart/" +
                //     productVariation.variation_id;
                //   window.open(basePath + destinationPath);
                // }}
                >
                  <ListItemText
                    primary={productVariation.type + ' - ' + productVariation.price}
                  />
                </ListItem>
              );
            })}
          </List>
          {error ? <Alert severity="error">{error}</Alert> : <></>}
        </DialogContent>{" "}
        <DialogActions>
          <Button onClick={props.handleClose} color="secondary">
            Cancel
          </Button>
        </DialogActions>

        <Dialog
          open={openAlert}
          onClose={props.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Added to cart</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Successfully added to your cart. You can keep browsing, and check out whenever you're ready, or check out now.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => {
              window.open('/cart');
            }} color="primary">
              Check out now
            </Button>
            <Button onClick={props.handleClose} color="secondary">
              Continue browsing
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  };

  useEffect(() => {
    setError("");
  }, [props.open]);

  const validate = () => {
    return true;
  };

  return (
    <>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <StallToolbarClubsSocJoinFormContent {...props} />
      </Dialog>
    </>
  );
}
