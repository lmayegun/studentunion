import React, { useState, useEffect } from "react";
/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Alert from "@material-ui/lab/Alert";

import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";

const useStyles = makeStyles((theme) => ({
  list: {
    display: "flex",
    justifyContent: "center",
  },
  buttonLoading: {
    "& .MuiButton-startIcon": {
      animationName: "spin",
      animationDuration: "2000ms",
      animationIterationCount: "infinite",
      animationTimingFunction: "linear",
    },
  },
  FormGroup: {
    width: "100%",
  },
}));

export default function StallToolbarClubsSocJoinFreeForm(props) {
  const classes = useStyles();
  //console.log("StallToolbarClubsSocJoinFreeForm", props);

  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);

  const StallToolbarClubsSocJoinFreeFormContent = (props) => {
    if (props.joined) {
      return (
        <>
          <DialogTitle id="form-dialog-title">Cancel Taster Membership</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to cancel your Taster Membership?
            </DialogContentText>
            {error ? <Alert severity="error">{error}</Alert> : <></>}
          </DialogContent>
        </>
      );
    } else {
      return (
        <>
          <DialogTitle id="form-dialog-title">Join</DialogTitle>
          <DialogContent>
            <DialogContentText>
              If you can’t attend club or society activities in person, join for free with a Taster Membership.
            </DialogContentText>
            <DialogContentText>
              With a Taster Membership you’ll be able to take part in all of the online events run by the club or society, you’ll also be added to their mailing list.
            </DialogContentText>
            <DialogContentText>
              When you are able to join in-person, you can upgrade to a full-membership for a small joining fee.
            </DialogContentText>
            <DialogContentText>
              By joining, you agree to our{" "}
              <a
                href="https://studentsunionucl.org/shop/online-shop-terms-and-conditions"
                target="_blank"
                rel="noopener noreferrer"
              >
                Online Shop Terms and Conditions
              </a>
              .
            </DialogContentText>
            {error ? <Alert severity="error">{error}</Alert> : <></>}
          </DialogContent>
        </>
      );
    }
  };

  useEffect(() => {
    setError("");
  }, [props.open]);

  const validate = () => {
    return true;
  };

  return (
    <>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <StallToolbarClubsSocJoinFreeFormContent {...props} />
        <DialogActions>
          <Button onClick={props.handleClose} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={async () => {
              setError("");
              if (validate()) {
                setLoading(true);
                const success = await props.saveDataViaAPIhandler("join");
                //console.log("Join success", success);
                if (success) {
                  setOpenAlert(true);
                  if (props.handleClose) {
                    props.handleClose();
                  }
                } else {
                  setError("Unable to save right now.");
                }
                setLoading(false);
              }
            }}
            className={loading ? classes.buttonLoading : null}
            disabled={loading}
            startIcon={
              loading ? (
                <HourglassEmptyIcon />
              ) : props.joined ? (
                <RemoveCircleIcon />
              ) : (
                <CheckCircleIcon />
              )
            }
            color="primary"
          >
            {props.joined ? "Remove membership" : "Join"}
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={openAlert}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogContent>
          <DialogContentText>
            {props.joined
              ? "It worked! You've joined successfully."
              : "You are no longer a taster member of this group."}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setOpenAlert(false);
              if (props.handleClose) {
                props.handleClose();
              }
            }}
            color="primary"
          >
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
