import React, { useState } from "react";

import { customMemoLog } from "../../../utils/Hooks";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import ListItem from "@material-ui/core/ListItem";
import FormGroup from "@material-ui/core/FormGroup";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import SendIcon from "@material-ui/icons/Send";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import { Select, MenuItem, InputLabel } from "@material-ui/core";
import LoginDialog from "../../dialogs/LoginDialog";
import Cookies from 'universal-cookie';

const useStyles = makeStyles((theme) => ({
  list: {
    display: "flex",
    justifyContent: "center",
  },
  buttonLoading: {
    "& .MuiButton-startIcon": {
      animationName: "spin",
      animationDuration: "2000ms",
      animationIterationCount: "infinite",
      animationTimingFunction: "linear",
    },
  },
  FormGroup: {
    width: "100%",
  },
}));

function StallQandAForm(props) {
  const classes = useStyles();

  const cookies = new Cookies();
  const [message, setMessage] = useState(cookies.get('user_comment') && cookies.get('user_comment') != '' ? cookies.get('user_comment') : '');
  const [category, setCategory] = useState("");
  const [userClubSocRole, setUserClubSocRole] = useState(cookies.get('user_comment_role') && cookies.get('user_comment_role') != '' ? cookies.get('user_comment_role') : '');
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  const validate = () => {
    const minLength = 20;
    const maxLength = 500;
    const maxLines = 3;

    if (message.length < minLength) {
      setError("Needs to be at least " + minLength + " characters");
      return false;
    }

    if (message.length > maxLength) {
      setError("Can't be longer than " + maxLength + " characters");
      return false;
    }

    if (message.split(/\r\n|\r|\n/).length > maxLines) {
      setError("Can't be longer than " + maxLines + " lines/paragraphs");
      return false;      
    }

    // if (window.userDetails.uid !== '0' && window.userDetails.ucl_mail === false){
    //   setError("To ask a question you need to login with a UCL account.");
    //   return false;
    // }

    setError(false);
    return true;
  };

  const submit = async () => {
    if (!validate()) {
      return;
    }

    // if this user is anonymous
    if (window.userDetails.uid === '0'){
      // Save message in a cookie
      cookies.set('user_comment', message, { path: '/' });
      // Open modal to login
      setLoginDialogOpen(true);
    } else {
      cookies.remove('user_comment');
      setLoading(true);
      cookies.set('user_comment_role', userClubSocRole, { path: '/' });
      const success = await props.saveCommentHandler(
          message,
          props.parentId,
          category,
          userClubSocRole
      );
      setLoading(false);
      if (success) {
        if (props.handleClose) {
          props.handleClose();
        }
        props.addDummyCommentFromForm(message, props.parentId);
        setMessage("");
      } else {
        setError("Unable to post right now.");
      }
    }
  };

  const [loginDialogOpen, setLoginDialogOpen] = useState(false);

  const categoryOptions = ["", "Events", "Representation", "Volunteering", "Skills", "COVID-19", "Liberation"];
  const userClubSocRoleOptions = [
    "",
    "Candidate", 
    "President",
    "Treasurer",
    "Welfare Officer",
    "Committee member",
    "UCL staff member",
    "Students' Union UCL staff member",
    "elected student representative",
  ];

  return (
    <ListItem className={classes.list}>
      <FormGroup className={classes.FormGroup}>
        <TextField
          id={
            props.parentId
              ? "Type your response here"
              : "Type your question here"
          }
          label={
            props.parentId
              ? "Type your response here"
              : "Type your question here"
          }
          placeholder={props.parentId ? "" : "What about..."}
          multiline
          value={message}
          error={error}
          helperText={error}
          onChange={(e) => {
            setMessage(e.target.value);
          }}
          autoFocus={props.parentId}
        />
        {props.parentId ? (
          <>
            <br />
            <InputLabel id="category-label">Category</InputLabel>
            <Select
              labelId="category-label"
              id="category"
              value={category}
              onChange={(e) => {
                setCategory(e.target.value);
              }}
            >
              <MenuItem value={""}>None</MenuItem>
              {categoryOptions.map((categoryOption) => {
                return (
                  <MenuItem value={categoryOption}>{categoryOption}</MenuItem>
                );
              })}
            </Select>
            <br />
            <InputLabel id="role-label" role="">Your role</InputLabel>
            <Select
              labelId="role-label"
              id="role"
              value={userClubSocRole}
              onChange={(e) => {
                setUserClubSocRole(e.target.value);
              }}
            >
              <MenuItem value={""}>None</MenuItem>
              {userClubSocRoleOptions.map((option) => {
                return <MenuItem value={option}>{option}</MenuItem>;
              })}
            </Select>
          </>
        ) : (
          <></>
        )}
        <span>
          <Button
            color="primary"
            onClick={submit}
            className={loading ? classes.buttonLoading : null}
            disabled={loading}
            startIcon={loading ? <HourglassEmptyIcon /> : <SendIcon />}
          >
            {props.parentId ? "Send reply" : "Ask question"}
          </Button>

          <LoginDialog
              open={loginDialogOpen}
              action={"comment"}
              disableNonUcl={true}
              cancelLoginDialog={() => {
                setLoginDialogOpen(false);
              }}
              handleNoUcl={() => {
                setLoginDialogOpen(false);
              }}
          />
        </span>
      </FormGroup>
    </ListItem>
  );
}

export default StallQandAForm;

// export default React.memo(StallQandAForm, (prevProps, nextProps) => {
//   const propsMatch = prevProps.parentId === nextProps.parentId;
//   customMemoLog("StallQandAForm", prevProps, nextProps, propsMatch, true);
//   return propsMatch;
// });
