import React from "react";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";

import ReactHtmlParser from "react-html-parser";

export default function StallSuggestedStall(props) {
  return (
    <ListItem
      button
      onClick={() => {
        props.openHandler(null);
        const pause = setTimeout(() => {
          props.openHandler(props.id);
          props.scrollToTopHandler();
        }, 300);
      }}
    >
      <ListItemAvatar>
        <Avatar src={props.imgUrl ? props.imgUrl : ''} alt="avatar" color="primary">{props.icon}</Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={ReactHtmlParser(props.name)}
        secondary={props.source}
      />
    </ListItem>
  );
}
