import React from "react";
import ReactHtmlParser from "react-html-parser";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import StallQandAForm from "./StallQandAForm";

export default function StallQandAReplyDialog(props) {
  const handleClose = () => {
    props.cancelReplyDialog();
  };

  return (
    <Dialog
      open={props.open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      fullWidth={true}
      maxWidth="sm"
    >
      <DialogTitle id="form-dialog-title">Reply to comment</DialogTitle>
      <DialogContent>
        {ReactHtmlParser(props.commentContent)}
        <StallQandAForm
          handleClose={handleClose}
          saveCommentHandler={props.saveCommentHandler}
          addDummyCommentFromForm={props.addDummyCommentFromForm}
          parentId={props.parentId}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}
