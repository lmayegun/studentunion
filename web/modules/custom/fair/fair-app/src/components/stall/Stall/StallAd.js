import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { PageView, initGA, Event } from "../../../utils/Tracking";

import { customMemoLog } from "../../../utils/Hooks";

/* Material-UI components */
import Card from "@material-ui/core/Card";
import Link from "@material-ui/core/Link";
import { CardMedia, CardActionArea, CardHeader } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {},
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
  media: {
    height: 300,
  },
}));

function StallAd(props) {
  const classes = useStyles();

  if (props.currentStallAdvert) {
    return (
      <Card className={`${props.parentClasses.card}`}>
        <CardHeader title="Sponsored content" />
        <CardActionArea>
          <Link href={props.currentStallAdvert.url} target="_blank" rel="noopener noreferrer">
            <CardMedia
              className={classes.media}
              title="Sponsored content"
              image={props.currentStallAdvert.image}
            />
          </Link>
        </CardActionArea>
      </Card>
    );
  } else {
    return <></>;
  }
}

export default React.memo(StallAd, (prevProps, nextProps) => {
  const propsMatch =
    prevProps.currentStallAdvert === nextProps.currentStallAdvert;
  customMemoLog("StallAd", prevProps, nextProps, propsMatch);
  return propsMatch;
});
