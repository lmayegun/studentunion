import React, { useState } from "react";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Alert from "@material-ui/lab/Alert";

import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";

const useStyles = makeStyles((theme) => ({
  list: {
    display: "flex",
    justifyContent: "center",
  },
  buttonLoading: {
    "& .MuiButton-startIcon": {
      animationName: "spin",
      animationDuration: "2000ms",
      animationIterationCount: "infinite",
      animationTimingFunction: "linear",
    },
  },
  FormGroup: {
    width: "100%",
  },
}));

export default function StallToolbarSignupForm(props) {
  const classes = useStyles();

  const [email, setEmail] = useState(
    window.userDetails && window.userDetails.mail ? window.userDetails.mail : ""
  );

  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);

  const validate = () => {
    const minLength = 20;
    const maxLength = 2000;

    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const emailValid = re.test(String(email).toLowerCase());

    if (!emailValid) {
      setError("Not a valid e-mail address.");
      return false;
    }

    setError(false);
    return true;
  };

  const StallToolbarSignupFormContent = (props) => {
    if (props.subscribed) {
      return (
        <>
          <DialogTitle id="form-dialog-title">Remove signup</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to remove your signup?
            </DialogContentText>
            {error ? <Alert severity="error">{error}</Alert> : <></>}
          </DialogContent>
        </>
      );
    } else {
      return (
        <>
          <DialogTitle id="form-dialog-title">
            Sign up to the mailing list
          </DialogTitle>
          <DialogContent>
            <DialogContentText color={"textPrimary"}>
              Enter your e-mail address to be added to the mailing list. Your e-mail address will only be shared with the stallholder and the relevant Union service.
            </DialogContentText>
            <DialogContentText color={"textPrimary"}>
              By signing up, you agree to our&nbsp;
              <a
                href="http://studentsunionucl.org/data-protection-and-privacy-policy"
                target="_blank"
                rel="noopener noreferrer"
              >
                Data Protection and Privacy Policy
              </a>
              .
            </DialogContentText>
            <TextField
              autoFocus
              id="name"
              label="Email Address"
              type="email"
              value={email}
              disabled={
                window.userDetails &&
                window.userDetails.mail &&
                email === window.userDetails.mail
              }
              error={error}
              helperText={error}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              fullWidth
            />
          </DialogContent>
        </>
      );
    }
  };

  return (
    <>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <StallToolbarSignupFormContent
          subscribed={props.subscribed}
          {...props}
        ></StallToolbarSignupFormContent>

        <DialogActions>
          <Button onClick={props.handleClose} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={async () => {
              if (validate()) {
                setLoading(true);
                const success = await props.saveDataViaAPIhandler("subscribe", {
                  email: email,
                });
                setLoading(false);
                //console.log("saveDataViaAPIhandler", success);
                if(success.result) {
                  if (!window.userDetails.mail || window.userDetails.mail == "") {
                    window.userDetails.mail = email;
                  }
                  setOpenAlert(true);
                  if (props.handleClose) {
                    props.handleClose();
                  }
                } else {
                  setError("Unable to save right now.");
                }
              }
            }}
            color="primary"
            className={loading ? classes.buttonLoading : null}
            disabled={loading}
            startIcon={
              loading ? (
                <HourglassEmptyIcon />
              ) : props.subscribed ? (
                <RemoveCircleIcon />
              ) : (
                <ArrowForwardIcon />
              )
            }
          >
            {props.subscribed ? "Remove sign up" : "Sign up"}
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openAlert}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogContent>
          <DialogContentText>
            {props.subscribed
              ? "Success! You've been signed up and the stall holder will contact you with more information."
              : "Your sign-up has been removed."}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setOpenAlert(false);
              if (props.handleClose) {
                props.handleClose();
              }
            }}
            color="primary"
          >
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
