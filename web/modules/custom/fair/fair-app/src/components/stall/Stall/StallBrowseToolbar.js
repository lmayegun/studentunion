import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import ReactHtmlParser from "react-html-parser";

/* Material-UI components */
import AppBar from "@material-ui/core/AppBar";
import StallVideo from "./StallVideo";
import StallSlideshow from "./StallSlideshow";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { Box, Fab, Hidden } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import StarIcon from "@material-ui/icons/Star";
import StallCategoriesAndTags from "./StallCategoriesAndTags";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MoreIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles((theme) => ({
    appBar: {
      top: 'auto',
      bottom: 0,
    },
}));

export default function StallBrowseToolbar(props) {
  const classes = useStyles();

  return ( 
  <AppBar position="fixed" color="primary" className={classes.appBar}>
    <Toolbar>
        <IconButton edge="end" color="inherit">
        <MoreIcon />
        </IconButton>
    </Toolbar>
</AppBar>);
}
