import React from "react";

/* Material-UI theming */
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import { themePurpleYellow } from "./../../../utils/Theme";

/* Material-UI components */
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import LaunchIcon from '@material-ui/icons/Launch';
/* Icons */
import LinkIcon from "@material-ui/icons/Link";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import CloseIcon from "@material-ui/icons/Close";
import EmailIcon from "@material-ui/icons/Email";
// import StarOutlineIcon from "@material-ui/icons/StarOutline";
import StarIcon from "@material-ui/icons/Star";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column"
    }
  },
  button: {
    margin: theme.spacing(1),
  },
}));

export default function StallLiveEvent(props) {
  const classes = useStyles();

  const goToUrl = (url) => {
    window.open(url);
  };

  if (
    props.stallData &&
    props.stallData.field_fair_stall_events &&
    props.stallData.field_fair_stall_events.length > 0
  ) {
    return (
      <ThemeProvider theme={themePurpleYellow}>
        <div className={classes.root}>
          {props.stallData.field_fair_stall_events.map((link) => (<Button
            size="large"
            variant="contained"
            color="primary"
            onClick={() => goToUrl(link.url)}
            startIcon={<LaunchIcon />}
            className={`animate-pulse-slow ${classes.button}`}
          >
            {link.title}
          </Button>))}          
        </div>
      </ThemeProvider>
    );
  } else {
    return <></>;
  }
}
