
import React, { useState } from "react";

import Moment from "react-moment";
import moment from "moment";

import ReactHtmlParser from "react-html-parser";

/* Material-UI theming */
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import Avatar from "@material-ui/core/Avatar";
import LockIcon from "@material-ui/icons/Lock";
import { Link } from "react-router-dom";
import {
  ListItemSecondaryAction,
  IconButton,
  Button,
  Grow,
} from "@material-ui/core";

import DeleteIcon from "@material-ui/icons/Delete";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckIcon from "@material-ui/icons/Check";

export default function StallQandACardDeleteButton(props) {
    const [confirmingDelete, setConfirmingDelete] = useState(false);
    if (confirmingDelete) {
      return (
        <>
          <Button
            onClick={() => {
              props.deleteCommentHandler(props.id);
            }}
            color="primary"
            edge="end"
            startIcon={<CheckIcon />}
          >
            Confirm deletion
          </Button>
          <Button
            onClick={() => {
              setConfirmingDelete(false);
            }}
            color="inherit"
            edge="end"
            startIcon={<CancelIcon />}
          >
            Cancel
          </Button>
        </>
      );
    } else {
      return (
        <Button
          onClick={() => {
            setConfirmingDelete(true);
          }}
          color="inherit"
          edge="end"
          startIcon={<DeleteIcon />}
        >
          Delete
        </Button>
      );
    }
  };