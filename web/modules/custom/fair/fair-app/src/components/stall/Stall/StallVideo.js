import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

/* Material-UI components */
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import MovieIcon from "@material-ui/icons/Movie";
import ReactPlayer from "react-player";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: theme.spacing(2),
    borderStyle: "none",
    "& .MuiCardContent-root:last-child": {
      paddingBottom: 0,
    },
  },
  cardIcon: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.contrastText,
  },
  playerWrapper: {
    position: "relative",
    padding: theme.spacing(1),
    paddingTop: "56.25%" /* Player ratio: 100 / (1280 / 720) */,
    width: "100%",
    backgroundColor: "#000000"
  },
  playerWrapperPortrait: {
    position: "relative",
    padding: theme.spacing(1),
    paddingLeft: "30%",
    paddingRight: "30%",
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "20%",
      paddingRight: "20%",
    },
    width: "100%",
    backgroundColor: "#000000"
  },
  reactPlayer: {
    position: "absolute",
    top: 0,
    left: 0,
    " iframe": {
      borderWidth: 0,
      borderRadius: 0
    },
  },
  reactPlayerPortrait: {
    " iframe": {
      borderWidth: 0,
      borderRadius: 0
    },
  },
}));

const RenderPlayer = (props) => {
  const classes = useStyles();
  return (
    <ReactPlayer
      url={props.videoUrl}
      className={props.portrait ? classes.reactPlayerPortrait : classes.reactPlayer}
      width="100%"
      height="100%"
      controls={true}
      playsinline={true}
    />
  );
};

export default function StallVideo(props) {
  const classes = useStyles();

  if (props.card) {
    return props.stallData.field_fair_stall_videos.map((video, i) => {
      if (i > 0) {
        return (
          <Card className={`${classes.card}`}>
            <CardContent className={classes.playerWrapper}>
              <RenderPlayer videoUrl={video.url} />
            </CardContent>
          </Card>
        );
      }
    });
  } else {
    const portrait = props.stallData.field_fair_stall_portrait_video === "1";
    return (
      <div className={portrait ? classes.playerWrapperPortrait : classes.playerWrapper}>
        <RenderPlayer portrait={portrait} videoUrl={props.url} />
      </div>
    );
  }
}
