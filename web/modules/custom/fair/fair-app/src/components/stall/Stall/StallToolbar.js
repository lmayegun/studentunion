import React from "react";

/* Material-UI components */
import { makeStyles, useTheme } from "@material-ui/core/styles";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

/* Icons */
import HomeIcon from '@material-ui/icons/Home';
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import CloseIcon from "@material-ui/icons/Close";
import EmailIcon from "@material-ui/icons/Email";

import StallToolbarButtons from "./StallToolbarButtons";
import { Hidden } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    '& .MuiButton-root': {
      margin: theme.spacing(1),
    },
  },
  menuButton: {
    "& .MuiButton-startIcon": {
      [theme.breakpoints.down("sm")]: {
        marginRight: "0",
        marginLeft: "0",
      },
    },
  },
  gridItem: {
    [theme.breakpoints.down("sm")]: {
      display: "contents",
    },
  },
}));

function StallToolbar(props) {
  const theme = useTheme();
  const classes = useStyles();
  const mediaQueryDownSm = useMediaQuery(theme.breakpoints.down('sm'));

  const goToUrl = (url) => {
    window.open(url);
  };

  const buttons = [
    {
      text: "E-mail",
      url: props.stallData.field_fair_stall_contact_email
        ? "mailto:" + props.stallData.field_fair_stall_contact_email
        : false,
      icon: <EmailIcon />,
    },
    {
      text: "Website",
      url: props.stallData.field_fair_stall_website && props.stallData.field_fair_stall_website[0]
        ? props.stallData.field_fair_stall_website[0].url
        : false,
      icon: <HomeIcon />,
    },
    {
      text: "Facebook",
      url: props.stallData.field_fair_stall_facebook && props.stallData.field_fair_stall_facebook[0]
        ? props.stallData.field_fair_stall_facebook[0].url
        : false,
      icon: <FacebookIcon />,
    },
    {
      text: "Instagram",
      url: props.stallData.field_fair_stall_instagram && props.stallData.field_fair_stall_instagram[0]
        ? props.stallData.field_fair_stall_instagram[0].url
        : false,
      icon: <InstagramIcon />,
    },
    {
      text: "Twitter",
      url: props.stallData.field_fair_stall_twitter && props.stallData.field_fair_stall_twitter[0]
        ? props.stallData.field_fair_stall_twitter[0].url
        : false,
      icon: <TwitterIcon />,
    },
  ];

  return (
    <AppBar position="sticky" color="default">
      <Toolbar className={classes.toolbar} disableGutters={mediaQueryDownSm} color="transparent">
        <Grid
          className={classes.toolgrid}
          justify="space-between"
          alignItems="center" // Add it here :)
          container
          spacing={24}
        >
          <Grid item className={classes.gridItem}>
            <StallToolbarButtons
              stallData={props.stallData}
              stallUserActions={props.stallUserActions}
              userIsStallManager={props.userIsStallManager}
              handleSave={props.handleSave}
              userLoggedIn={props.userLoggedIn}
              joinEnabledForFair={props.joinEnabledForFair}
              saveDataViaAPIhandler={props.saveDataViaAPIhandler}
              userDetails={props.userDetails}
              currentStallProducts={props.currentStallProducts}
            />
            {buttons.map((button) => {
              if (button.url && button.url.length > 0) {
                return (
                  <Button
                    target="_blank"
                    rel="noopener noreferrer"
                    onClick={() => goToUrl(button.url)}
                    className={classes.menuButton}
                    color="inherit"
                    startIcon={button.icon}
                  >
                    <Box display={{ xs: "none", sm: "none", md: "block" }}>
                      {button.text}
                    </Box>
                  </Button>
                );
              } else {
                return <></>;
              }
            })}
          </Grid>
          <Hidden smDown>
            <Grid item className={classes.gridItem}>
              <Button
                variant="contained"
                className={classes.menuButton}
                color="secondary"
                onClick={props.handleClose}
                startIcon={<CloseIcon />}
              >
                Leave stall
              </Button>
            </Grid>
          </Hidden>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

export default StallToolbar;

// export default React.memo(StallToolbar, (prevProps, nextProps) => {
//   return (
//     prevProps.stallUserActions === nextProps.stallUserActions &&
//     prevProps.userIsStallManager === nextProps.userIsStallManager
//   );
// });
