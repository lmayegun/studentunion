import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { customMemoLog } from "../../../utils/Hooks";
import Box from "@material-ui/core/Box";
import Badge from "@material-ui/core/Badge";
import { Grow } from "@material-ui/core";

import DirectoryCardStall from "./DirectoryCardStall";
import DirectoryCardEvent from "./DirectoryCardEvent";
import DirectoryCardVideo from "./DirectoryCardVideo";

const useStyles = makeStyles((theme) => ({
  cardView: {
    margin: theme.spacing(3),
    height: "100%",
  },
  listView: {
    margin: theme.spacing(2),
  },
  badge: {
    cursor: "pointer",
    transition: "0.5s",
    "&:hover": {
      transform: "scale(1.05, 1.05)",
    },
  },
}));

const DirectoryCard = (props) => {
  const classes = useStyles();
  // console.log('DirectoryCard', props);

  const RenderSwitch = (props) => {
    switch (props.type) {
      case 'videos':
        return (<DirectoryCardVideo {...props} />);

      case 'events': 
        return (<DirectoryCardEvent {...props} />);

      default:
        return (<DirectoryCardStall {...props} />);
    }
  }

  return (
    <Grow in={true}>
      <Box className={props.view == 'list' ? classes.listView : classes.cardView}>
        <Badge
          badgeContent={"online"}
          invisible={!props.online}
          color="primary"
          className={`MuiBadge-online ${classes.badge}`}
        >
          <RenderSwitch {...props} />
        </Badge>
      </Box>
    </Grow>
  );
};

export default React.memo(DirectoryCard, (prevProps, nextProps) => {
  const propsMatch = prevProps.id === nextProps.id && prevProps.view === nextProps.view;
  customMemoLog("DirectoryCard", prevProps, nextProps, propsMatch);
  return propsMatch;
});
