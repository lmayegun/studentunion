import React, { useState, useRef } from "react";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import { fade } from '@material-ui/core/styles/colorManipulator';

import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { themeAccessible } from "../../../utils/Theme";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";

import Button from "@material-ui/core/Button";

import SortIcon from '@material-ui/icons/Sort';
import FlareIcon from '@material-ui/icons/Flare';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ListIcon from '@material-ui/icons/List';
import ToggleButton from "@material-ui/lab/ToggleButton";

const useStyles = makeStyles((theme) => ({
    MuiToggleButtonGroup: {
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
       verticalAlign: "middle"
    },
    MuiToggleButton: {
        color: theme.palette.secondary.main,
        borderColor: theme.palette.secondary.main,
        "&:hover" : {
            backgroundColor: fade(theme.palette.secondary.main, 0.2),
        },
        "&.Mui-selected": {
            color: theme.palette.secondary.contrastText,
            backgroundColor: theme.palette.secondary.main,
            "&:hover" : {
                backgroundColor: theme.palette.secondary.dark,
            }
        },
    }
}));

export default function ViewSortToggleButton(props) {
    // console.log("<ViewSortToggleButton />", props);

    const classes = useStyles();
    var options = [];
    switch (props.type) {
        case 'sort':
            options = [
                ['random', 'Discover', <FlareIcon fontSize="small" />],
                ['alphabetical', 'A-Z', <SortByAlphaIcon fontSize="small" />]
            ];
            break;

        case 'view':
            options = [
                ['cards', 'Cards', <ViewModuleIcon fontSize="small" />],
                ['list', 'List', <ListIcon fontSize="small" />]
            ];
            break;

        default:
            return (<></>);
    }

    // console.log("options", options);

    return (
        <ToggleButtonGroup exclusive size="small" className={classes.MuiToggleButtonGroup} value={props.value} onChange={props.sortViewHandler} aria-label={props.type}>
            {options && options.map((button) => {
                // console.log('button', button);
                if (button) {
                    const value = button[0];
                    const text = button[1];
                    const icon = button[2];
                    return (
                        <ToggleButton className={classes.MuiToggleButton} value={value} aria-label={text}>
                            {icon}&nbsp;{text}
                        </ToggleButton>)
                } else {
                    return <></>;
                }
            })}
        </ToggleButtonGroup>
    );
}