import React from "react";
import Category from "./Category";

const SavedJoinedCategory = (props) => {
  const cardsProp = props.cardsProp;
  const title = props.title;

  return props.ids.length > 0 ? (
    <Category
      title={title}
      items={props.items}
      windowVariable={props.directoryType}
      openHandler={props.openHandler}
      nonCategoryCategory={props.nonCategoryCategory}
    >
      {props.ids}
    </Category>
  ) : (
    <Category
      view={props.view}
      title={title}
      items={props.items}
      emptyMessage={
        cardsProp === "cardsJoined"
          ? "Once you've joined clubs and societies, you can keep track of them here."
          : "Use the star icon to save content to your list."
      }
      nonCategoryCategory={props.nonCategoryCategory}
    >
      {[]}
    </Category>
  );
};

export default SavedJoinedCategory;
