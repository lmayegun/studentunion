import React, { useState, useEffect } from "react";
import LazyLoad from "react-lazyload";
import { customMemoLog } from "../../utils/Hooks";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import ReactHtmlParser from "react-html-parser";

import { Fab, Box, List } from "@material-ui/core";
import DirectoryCard from "./card/DirectoryCard";
import EmblaCarousel from "./EmblaCarousel";

import { Grow } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(4),
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
  },
  emptyMessage: {
    padding: theme.spacing(2),
  },
  media: {
    height: 140,
  },
  dummy: {
    width: 100,
    height: 100,
    margin: 10,
    backgroundColor: "red",
  },
}));

function Category(props) {
  const theme = useTheme();
  const classes = useStyles();

  const RenderDirectoryCard = (props) => {
    // console.log("RenderDirectoryCard", props);
    // Standardise the various data sources into the card fields
    const data = props.items[props.cardId];
    const cardProps = {
      type: props.directoryType,
      id: data.id,
      online: data.online,
      openHandler: props.openHandler,
      scrollHandler: props.scrollHandler,
      cardLoading: props.cardLoading,
      view: props.view
    };
    if (props.windowVariable === "stalls") {
      cardProps.title = ReactHtmlParser(data.name);
      cardProps.tagline = ReactHtmlParser(data.fair_stall_tagline);
      cardProps.image = data.fair_stall_card ? data.fair_stall_card[0] : '';
      if (props.nonCategoryCategory) {
        cardProps.category = ReactHtmlParser(data.fair_stall_category);
      }
    } else if (props.windowVariable === "videos") {
      cardProps.videoUrl = data.field_fair_video_url[0].url;
      cardProps.title = ReactHtmlParser(data.name);
      cardProps.stallReference = ReactHtmlParser(
        data.field_fair_video_stall_reference
      );
      cardProps.tagline = data.field_fair_video_tagline;
    } else if (props.windowVariable === "events") {
      cardProps.title = ReactHtmlParser(data.title);
      // cardProps.image = data.backgroundImageUrl;
      cardProps.image = data.logoUrl;
      cardProps.tagline = data.subtitle;
      cardProps.url = data.url;
      cardProps.earliestDate = data.earliestDate;
      cardProps.stallId = data.stallId;
    }
    return <DirectoryCard key={cardProps.id} {...cardProps} />;
  };

  const RenderEmpty = (props) => {
    return (
      <>
        {props.emptyMessage ? (
          <p className={classes.emptyMessage}>{props.emptyMessage}</p>
        ) : (
          <>
            {/* <Skeleton variant="rect" width={210} height={118} />
      <Skeleton variant="rect" width={210} height={118} />
      <Skeleton variant="rect" width={210} height={118} /> */}
          </>
        )}
      </>
    );
  };

  const RenderList = (props) => {
    // console.log('RenderList', props);
    const children = props.children.length > 0 ? props.children.filter(cardId => props.items[cardId]) : false;
    return children && children.length > 0 ? (
      <List>
        {children.map((cardId) => <RenderCard cardId={cardId} {...props} />)}
      </List>
    ) : (
      <RenderEmpty emptyMessage={props.emptyMessage} />
    );
  };

  const RenderCard = (props) => {
    return (<div key={"render" + props.cardId}>
      <RenderDirectoryCard
        cardId={props.cardId}
        items={props.items}
        windowVariable={props.windowVariable}
        view={props.view}
        openHandler={props.openHandler}
        scrollHandler={props.scrollHandler}
        directoryType={props.directoryType}
        cardLoading={props.cardLoading}
        nonCategoryCategory={props.nonCategoryCategory}
      />
    </div>);
  }

  const RenderCarousel = (props) => {
    // console.log("RenderCarousel", props);
    const children = props.children.length > 0 ? props.children.filter(cardId => props.items[cardId]) : false;
    return children && children.length > 0 ? (
      <EmblaCarousel>
        {children.map((cardId) => {
          return (<RenderCard cardId={cardId} {...props} />);
        })}
      </EmblaCarousel>
    ) : (
      <RenderEmpty emptyMessage={props.emptyMessage} />
    );
  };

  return (
    <LazyLoad offset={200} dataTour={props.firstCategory ? props.windowVariable + '_category_first' : ''}>
      <Grow in={true}>
        <>
          {props.title !== "" ? (
            <Box className={classes.root}>
              <h2 tabindex="0" className={"category-header"}>
                {ReactHtmlParser(props.title)}
              </h2>
              {props.subtitle && props.subtitle != "" ? (
                <h3 tabindex="0" className={"category-subheader"}>
                  {ReactHtmlParser(props.subtitle)}
                </h3>
              ) : (
                <></>
              )}
            </Box>
          ) : (
            <></>
          )}
          {props.view == 'list' ? (
            <RenderList {...props} />
          ) : (
            <RenderCarousel {...props} />
          )}
        </>
      </Grow>
    </LazyLoad>
  );
}

export default React.memo(Category, (prevProps, nextProps) => {
  const propsMatch =
    prevProps.children === nextProps.children &&
    prevProps.title === nextProps.title
    && prevProps.view === nextProps.view;
  customMemoLog("Category", prevProps, nextProps, propsMatch);
  return propsMatch;
});
