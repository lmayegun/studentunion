import React, { useState, useRef } from "react";

import _ from "lodash";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { themeAccessible } from "../../../utils/Theme";

import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Grow from "@material-ui/core/Grow";

import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import ClearIcon from "@material-ui/icons/Clear";
import AddBoxIcon from "@material-ui/icons/AddBox";
import DirectoryToolsTags from "./DirectoryToolsTags";
import ViewSortToggleButton from "./ViewSortToggleButton";
import DirectoryAutocomplete from "./DirectoryAutocomplete";
import {
  Chip,
  Grid,
  Input,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(4),
    marginTop: theme.spacing(1),
    marginBottom: -theme.spacing(1),
  },
  button: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  bar: {},
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: theme.spacing(1),
  },
  Select: {
    minWidth: 120,
    maxWidth: 300,
  },
}));

// Settings
export default function DirectoryTools(props) {
  // console.log("DirectoryTools", props);
  // Imports and state
  const classes = useStyles();

  return (
    <FormGroup className={classes.root} row>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Button
            className={classes.button}
            color="secondary"
            variant={"contained"}
            onClick={(event) => {
              props.randomCardHandler();
            }}
          >
            I'm feeling lucky!
          </Button>
          <DirectoryToolsTags {...props} />
          <ViewSortToggleButton
            type="sort"
            value={props.sort}
            classesButton={classes.button}
            sortViewHandler={props.sortViewHandler}
          />
          <ViewSortToggleButton
            type="view"
            value={props.view}
            classesButton={classes.button}
            sortViewHandler={props.sortViewHandler}
          />
          {/* <DirectoryAutocomplete
            data-tour="search"
            autocompleteOptions={props.autocompleteOptions}
            filters={props.filters}
            setFilterHandler={props.setFilterValue}
            openHandler={props.openHandler}
            role="search"
          /> */}
        </Grid>
        {/* <Grid item xs={12}>
         
        </Grid> */}
      </Grid>
    </FormGroup>
  );
}
