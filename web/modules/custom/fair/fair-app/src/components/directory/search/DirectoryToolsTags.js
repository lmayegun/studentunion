import React, { useState, useCallback, useRef, useEffect } from "react";

import _ from "lodash";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { themeAccessible } from "../../../utils/Theme";

import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Grow from "@material-ui/core/Grow";

import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import ClearIcon from "@material-ui/icons/Clear";
import AddBoxIcon from "@material-ui/icons/AddBox";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";
import ViewSortToggleButton from "./ViewSortToggleButton";
import {
  Chip,
  Grid,
  Input,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(4),
    marginTop: theme.spacing(1),
    marginBottom: -theme.spacing(1),
  },
  button: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  bar: {},
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: theme.spacing(1),
  },
  Select: {
    minWidth: 120,
    maxWidth: 300,
  },
}));

// Settings
const totalTagsToShow = 6;

export default function DirectoryToolsTags(props) {
  // console.log("DirectoryToolsTags", props);

  const getTagsToShow = (showMore) => {
    // console.log('getTagsToShow', 'props.showMoreTags', props.showMoreTags, showMore);
    if (props.showMoreTags) {
      return props.valuesInUse;
    } else {
      // Merge tags checked (to make sure they're at the start) with all other tags
      const checkedTags = props.filters[props.filterBy];
      const tags = _.uniq(checkedTags.concat(props.valuesInUse));
      // Reduce to the number of tags we've decided to show by default, or the number of checked tags
      return tags.slice(0, Math.max(checkedTags.length, totalTagsToShow));
    }
  };

  // Imports and state
  const classes = useStyles();
  const [tagsToShow, setTagsToShow] = useState(getTagsToShow());

  useEffect(() => {
    setTagsToShow(getTagsToShow(props.showMoreTags));
  }, [props]);

  const showMoreTags = () => {
    props.setShowMoreTagsHandler(true);
    setTagsToShow(getTagsToShow(true));
  };

  const showFewerTags = () => {
    props.setShowMoreTagsHandler(false);
    setTagsToShow(getTagsToShow(false));
  };

  //another function
  const updateLocalAndParentState = (event, checked, tag) => {
    var newChecked = [...checked];
    if (!checked || !checked.includes(tag)) {
      newChecked.push(tag);
    } else {
      newChecked = checked.filter((e) => e !== tag);
    }
    // console.log("updateLocalAndParentState checked", checked);
    props.setFilterHandler("tags", newChecked);
  };

  const resetLocalAndParentState = () => {
    props.setFilterHandler("tags", []);
  };

  return (
    <>
      <ThemeProvider theme={themeAccessible}>
        {tagsToShow && tagsToShow.map((tag, i) => {
          const checked =
            props.filters[props.filterBy] &&
            props.filters[props.filterBy].includes(tag);
          // console.log(tag, checked);
          return (
            <Button
              className={classes.button}
              color="primary"
              variant={checked ? "contained" : "outlined"}
              onClick={(event) => {
                updateLocalAndParentState(
                  event,
                  props.filters[props.filterBy],
                  tag
                );
              }}
              // value={checked}
              startIcon={
                checked ? <CheckBoxIcon /> : <CheckBoxOutlineBlankIcon />
              }
            >
              {tag}
            </Button>
          );
        })}
      </ThemeProvider>
      {tagsToShow && props.valuesInUse && tagsToShow.length < props.valuesInUse.length ? (
        <Button
          className={classes.button}
          onClick={(event) => {
            showMoreTags();
          }}
          color="secondary"
          startIcon={<AddBoxIcon />}
        >
          More tags
        </Button>
      ) : (
        <Button
          className={classes.button}
          onClick={(event) => {
            showFewerTags();
          }}
          color="secondary"
          startIcon={<IndeterminateCheckBoxIcon />}
        >
          Fewer tags
        </Button>
      )}
      {props.filters[props.filterBy] &&
        props.filters[props.filterBy].length > 0 ? (
        <Button
          className={classes.button}
          onClick={(event) => {
            resetLocalAndParentState();
          }}
          color="secondary"
          startIcon={<ClearIcon />}
        >
          Reset tags
        </Button>
      ) : (
        <></>
      )}
    </>
  );
}
