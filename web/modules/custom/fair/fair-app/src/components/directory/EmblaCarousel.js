import React, { useEffect, useCallback, useState } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useEmblaCarousel } from "embla-carousel/react";

import { Fab, Box, Grow } from "@material-ui/core";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";

const useStyles = makeStyles((theme) => ({
  arrow: {
    position: "absolute",
    touchAction: "manipulation",
    zIndex: 5,
    top: "50%",
    transform: "translateY(-50%)",
  },
  rightArrow: {
    right: theme.spacing(1),
    // right: -theme.spacing(2),
  },
  leftArrow: {
    left: theme.spacing(1),
    // left: -theme.spacing(2),
  },
}));

const CarouselButton = (props) => {
  const classes = useStyles();
  return (
    <Grow in={props.visible}>
      <Fab
        className={`${classes.arrow} ${props.buttonClass}`}
        color="primary"
        onClick={() => props.clickHandler()}
        aria-label="arrow next"
      >
        {props.icon}
      </Fab>
    </Grow>
  );
};

const EmblaCarousel = (props) => {
  const classes = useStyles();
  const [EmblaCarouselReact, embla] = useEmblaCarousel({
    loop: false,
    dragFree: true,
    containScroll: "trimSnaps",
    align: "start",
  });
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [scrollSnaps, setScrollSnaps] = useState([]);

  const scrollTo = useCallback((index) => embla.scrollTo(index), [embla]);
  const scrollPrev = useCallback(() => embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla.scrollNext(), [embla]);
  const onSelect = useCallback(() => {
    if (!embla) return;
    setSelectedIndex(embla.selectedScrollSnap());
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  useEffect(() => {
    if (!embla) return;
    setScrollSnaps(embla.scrollSnapList());
    embla.on("select", onSelect);
    onSelect();
  }, [embla, onSelect]);

  return (
    <div style={{ position: "relative" }}>
      <EmblaCarouselReact>
        <div style={{ display: "flex" }}>{props.children}</div>
      </EmblaCarouselReact>
      <CarouselButton
        clickHandler={embla && embla.scrollPrev}
        visible={prevBtnEnabled}
        buttonClass={classes.leftArrow}
        icon={<NavigateBeforeIcon />}
      />
      <CarouselButton
        clickHandler={embla && embla.scrollNext}
        visible={nextBtnEnabled}
        buttonClass={classes.rightArrow}
        icon={<NavigateNextIcon />}
      />
    </div>
  );
};

export default React.memo(EmblaCarousel);
