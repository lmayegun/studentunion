import React, { useState, useEffect, useRef, useCallback } from "react";

import _ from "lodash";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { customMemoLog, useStateWithStorage } from "../../utils/Hooks";
import Paper from "@material-ui/core/Paper";
import Skeleton from "@material-ui/lab/Skeleton";
import TextSearch from "./search/TextSearch";
import DirectoryTools from "./search/DirectoryTools";
import Category from "./Category";
import AppBar from '@material-ui/core/AppBar';

import {
  runSearch,
  getCategoriesInUse,
  getTagsInUse,
  getRandom,
  shuffleArray,
} from "./search/SearchFunctions";
import { CircularProgress, Box } from "@material-ui/core";
import DirectoryAutocomplete from "./search/DirectoryAutocomplete";

const useStyles = makeStyles((theme) => ({
  searchBar: {
  },
  searchBarBackground: {
    backgroundColor: "#FFFFFF",
    padding: theme.spacing(2),
  },
  loadingContainer: {
    padding: theme.spacing(2),
    // display: "flex",
    overflow: "hidden",
  },
  skeletonRow: {
    width: "100%",
    display: "flex",
  },
  SkeletonHeading: {
    margin: "1em",
  },
  SkeletonRect: {
    margin: theme.spacing(2),
    borderRadius: "3px",
    minWidth: "225px",
  },
}));

function RandomCategory(props) {
  const numberOfCards = 10;
  var randomCards = getRandom(Object.keys(props.items), numberOfCards);
  return (
    <Category
      title={props.title}
      subtitle={props.subtitle}
      openHandler={props.openHandler}
      scrollHandler={props.scrollHandler}
      directoryType={props.type}
      cardLoading={props.cardLoading}
      windowVariable={props.windowVariable}
      view={props.view}
      items={props.items}
      nonCategoryCategory={props.nonCategoryCategory}
    >
      {randomCards}
    </Category>
  );
}

function SkeletonCategory(props) {
  const { classes } = props;
  return [0, 1, 2].map((el, i) => {
    return (
      <div className={classes.loadingContainer}>
        <div className={classes.skeletonRow}>
          <Skeleton
            variant="rect"
            className={classes.SkeletonHeading}
            width={"30vw"}
            height={"3em"}
          />
        </div>
        <div className={classes.skeletonRow}>
          <Skeleton
            variant="rect"
            className={classes.SkeletonHeading}
            width={"20vw"}
            height={"1.5em"}
          />
        </div>
        <div className={classes.skeletonRow}>
          {[0, 1, 2, 3, 4].map((el, i) => {
            return (
              <Skeleton
                className={classes.SkeletonRect}
                variant="rect"
                width={225}
                height={225}
              />
            );
          })}
        </div>
      </div>
    );
  });
}

const NoResults = (props) => {
  if (!props.firstDirectoryLoad && props.items && Object.keys(props.items).length > 0) {
    return (
      <>
        {/* <Paper>No results!</Paper> */}
        <RandomCategory
          title="No results!"
          subtitle="Try some random suggestions:"
          nonCategoryCategory={true}
          {...props}
        />
      </>
    );
  } else {
    return <SkeletonCategory classes={props.classes} />;
  }
};

const Directory = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const mediaQueryMobile = useMediaQuery(theme.breakpoints.down('md'));

  // Current categories
  const [tagsInUse, setTagsInUse] = useState(
    window[props.windowVariable + 'Info'] ? window[props.windowVariable + 'Info']['tagsInUse'] : []
  );
  const [categoriesInUse, setCategoriesInUse] = useState(
    window[props.windowVariable + 'Info'] ? window[props.windowVariable + 'Info']['categoriesInUse'] : []
  );

  const [view, setView] = useStateWithStorage(props.windowVariable == 'events' ? 'list' : 'cards', 'settingView' + props.windowVariable);
  const [sort, setSort] = useStateWithStorage('random', 'settingSort' + props.windowVariable);

  const [
    showMoreTags,
    setShowMoreTags,
  ] = useState(false);

  const sortViewHandler = (event, value) => {
    // console.log('sortViewHandler', value);
    switch (value) {
      case 'random':
      case 'alphabetical':
        setSort(value);
        break;

      case 'cards':
      case 'list':
        setView(value);
        break;
    }
  }

  const generateAutocompleteOptions = useCallback(
    (items, windowVariable, tags, categories) => {
      // console.time("TIMER generateAutocompleteOptions");
      var autocompleteOptions = [];
      const ids = Object.keys(items);
      ids.forEach((id, index) => {
        const card = items[id];
        autocompleteOptions.push({
          // filterName: "tags",
          id: card.id,
          title: card.name,
          // filterValue: tags[k],
          group: card.entity_type && card.entity_type == 'election_candidate' ? 'Candidates' : "Stalls",
        });
      })

      if (tags) {
        for (var k = 0; k < tags.length; k++) {
          autocompleteOptions.push({
            filterName: "tags",
            title: tags[k],
            filterValue: tags[k],
            group: "Tags",
          });
        }
      }

      // for (k = 0; k < categories.length; k++) {
      //   autocompleteOptions.push({
      //     filterName: "categories",
      //     title: categories[k],
      //     filterValue: categories[k],
      //     group: "Categories",
      //   });
      // }

      // console.log("generate autocompleteOptions", autocompleteOptions);
      // console.timeEnd("TIMER generateAutocompleteOptions");
      return autocompleteOptions;
    }
  );

  const [autocompleteOptions] = useState(
    generateAutocompleteOptions(
      props.items,
      props.windowVariable,
      tagsInUse,
      categoriesInUse
    )
  );

  // Currently set filters
  const setFilters = (updatedFilters) => {
    props.filtersHandler(updatedFilters);
    //console.log("set filters", updatedFilters);
  };

  const setDirectoryCategories = useCallback(
    (categories) => {
      props.currentCategoriesHandler(categories);
      // console.log("set categories", categories);
    },
    [props.currentCategoriesHandler]
  );

  // Callback for child filters
  const setFilterValue = useCallback(
    (name, value, valueToBlank) => {
      // Overwrite existing filters state
      const existingFilters = { ...props.filters };
      const updatedFilters = { ...props.filters };
      updatedFilters[name] = value;
      if (valueToBlank) {
        updatedFilters[valueToBlank] = null;
      }
      // console.log("Old versus new filters: ", existingFilters, updatedFilters);
      if (!_.isEqual(existingFilters, updatedFilters)) {
        setFilters(updatedFilters);

        // console.log("Setting filters: ", name, value);

        // Runs search based on new filters
        setDirectoryCategories(
          runSearch(props.windowVariable, props.items, updatedFilters, sort, props.categoriesRanking)
        );
      }
    },
    [props, sort]
  );

  useEffect(() => {
    setDirectoryCategories(
      runSearch(props.windowVariable, props.items, props.filters, sort, props.categoriesRanking)
    );
  }, [sort]);

  // Render
  // console.log("filters", props.filters, "props", props);

  // console.log(
  //   "RENDER Directory",
  //   "view",
  //   view
  // );
  return (
    <>
      {props.search ? (
        <AppBar elevation={0} color="transparent" className={classes.searchBar} position={!mediaQueryMobile ? "sticky" : 'relative'}>
          <Paper square className={classes.searchBarBackground}>
            {/* <div className={classes.searchBarBackground}> */}
            {/* <TextSearch
            dataTour="search"
            autocompleteOptions={autocompleteOptions}
            filters={props.filters}
            setFilterHandler={setFilterValue}
            openHandler={props.openHandler}
            role="search"
          /> */}
            <DirectoryAutocomplete
              data-tour="search"
              autocompleteOptions={autocompleteOptions}
              filters={props.filters}
              setFilterHandler={setFilterValue}
              openHandler={props.openHandler}
              role="search"
            />
            <DirectoryTools
              data-tour="tags"
              autocompleteOptions={autocompleteOptions}
              filters={props.filters}
              showMoreTags={showMoreTags}
              setShowMoreTagsHandler={setShowMoreTags}
              filterBy={"tags"}
              valuesInUse={tagsInUse}
              setFilterHandler={setFilterValue}
              sortViewHandler={sortViewHandler}
              view={view}
              sort={sort}
              randomCardHandler={() => {
                const randomId = getRandom(Object.keys(props.items), 1)[0];
                props.openHandler(randomId);
              }}
            />
          </Paper>
          {/* </div> */}
        </AppBar>
      ) : (
        <></>
      )}
      {props.currentCategories.length > 0 ? (
        props.currentCategories.map((category, index) => {
          if (category.cards.length > 0) {
            props.setFirstDirectoryLoad(false);
            return (
              <Category
                items={props.items}
                firstCategory={index === 0}
                title={category.name}
                subtitle={category.description}
                openHandler={props.openHandler}
                scrollHandler={props.scrollHandler}
                directoryType={props.type}
                cardLoading={props.cardLoading}
                windowVariable={props.windowVariable}
                view={view}
                nonCategoryCategory={sort == 'alphabetical'}
              >
                {category.cards}
              </Category>
            );
          }
        })
      ) : (
        <NoResults classes={classes} view={view} {...props}></NoResults>
      )}
    </>
  );
};

export default React.memo(Directory, (prevProps, nextProps) => {
  const propsMatch =
    prevProps.currentCategories === nextProps.currentCategories && prevProps.view === nextProps.view;
  customMemoLog("Directory", prevProps, nextProps, propsMatch, true);
  return propsMatch;
});
