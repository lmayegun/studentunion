import React, { memo, useState } from "react";

import LazyLoad from "react-lazyload";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import StorefrontIcon from "@material-ui/icons/Storefront";
import {
  Avatar,
  Grow,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";

import ImageIcon from "@material-ui/icons/Image";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import { CircularProgress } from "@material-ui/core";

const cardWidth = 215;
const mediaHeight = 350;

const useStyles = makeStyles((theme) => ({
  card: {
    position: "relative",
    minWidth: cardWidth,
    width: cardWidth,
    "&:after": {
      content: '""',
      display: "block",
      position: "absolute",
      width: "100%",
      height: "64%",
      bottom: 0,
      zIndex: 1,
      background:
        "linear-gradient(to top, rgba(0,0,0,0.8) 10%, rgba(0,0,0,0.6) 20%, rgba(0,0,0,0))",
    },
  },
  media: {
    height: mediaHeight,
  },
  circularLoading: {
    position: "absolute",
    left: "calc(50% - 4rem)",
    top: "calc(50% - 4rem)",
    zIndex: 2,
  },
  actionAreaLoading: {},
  cardContent: {},
  headerOverlay: {
    position: "absolute",
    boxSizing: "border-box",
    zIndex: 2,
    bottom: 0,
    width: "100%",
  },
}));

const DirectoryCardStall = memo((props) => {
  const classes = useStyles();
  // console.log("RENDER DirectoryCardStall");

  const defaultImages = window.fairData.field_default_card_images
    ? window.fairData.field_default_card_images
    : [];

  // console.log("cardloading and id", props.cardLoading, props.id);

  const [loading, setLoading] = useState(false);

  if (props.view == "list") {
    return (
      <ListItem
        onClick={async () => {
          await props.openHandler(props.id);
        }}
      >
        <ListItemAvatar>
          <Avatar
            src={
              props.image && props.image != ''
                ? props.image
                : defaultImages[
                Math.floor(Math.random() * defaultImages.length)
                ]
            }
            color={"primary"}
          />
        </ListItemAvatar>
        <ListItemText
          primary={props.title}
          secondary={props.tagline != "..." ? props.tagline : false}
        />
      </ListItem>
    );
  } else {
    return (
      // <LazyLoad height={mediaHeight} offset={300}>
      // <Grow in={true}>
      <Card
        onClick={async () => {
          //console.log("Card clicked", props.id);
          // setLoading(true);
          await props.openHandler(props.id);
          // setLoading(false);
          // props.scrollHandler();
        }}
        className={`${classes.card}`}
      >
        <CardActionArea className={classes.actionArea}>
          <CardMedia
            className={classes.media}
            image={
              props.image
                ? props.image
                : defaultImages[
                Math.floor(Math.random() * defaultImages.length)
                ]
            }
            title=""
          >
            <Box py={2} px={2} className={classes.headerOverlay}>
              {props.category ? (
                <Typography className="tagline" variant="body2" component="p">
                  <i>{props.category}</i>
                </Typography>
              ) : (
                <></>
              )}
              <Typography gutterBottom variant="h5" component="h2">
                {props.title}
              </Typography>
              {props.tagline != "..." ? (
                <Typography className="tagline" variant="body2" component="p">
                  {props.tagline}
                </Typography>
              ) : (
                <></>
              )}
            </Box>
            {loading ? (
              <CircularProgress
                className={classes.circularLoading}
                size={"8rem"}
              />
            ) : (
              <></>
            )}
          </CardMedia>
        </CardActionArea>
      </Card>
      // </Grow>
      // </LazyLoad>
    );
  }
});

export default DirectoryCardStall;
