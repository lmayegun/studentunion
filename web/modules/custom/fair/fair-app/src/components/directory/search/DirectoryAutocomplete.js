/* eslint-disable no-use-before-define */
import React, { useState, useRef } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { withStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(4),
  },
  bar: {},
}));

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop - 16);

export default function DirectoryAutocomplete(props) {
  const classes = useStyles();
  const timeOut = useRef(null);
  const [inputValue, setInputValue] = useState('');
  const [loading, setLoading] = useState(false);
  const autocompleteRef = useRef(null);

  //console.log("input value", inputValue);
  //console.log("textsearch props", props);
  //console.log("props test on top", props.filters.textSearch);
  return (
    <Autocomplete
      freeSolo
      fullWidth
      inputValue={inputValue}
      options={props.autocompleteOptions}
      groupBy={(option) => option.group}
      loading={loading}
      loadingText={"Loading search results below..."}
      onChange={(event, newValue, reason) => {
        console.log("onChange newValue of autocomplete", newValue, reason);
        if (reason === "clear") {
          // props.setFilterHandler("textSearch", "");
        } else if(reason == 'select-option'){
          setInputValue('');
          if (newValue.id) {
            props.openHandler(newValue.id);
          } else if (newValue.filterName) {
            props.setFilterHandler(
              newValue.filterName,
              [newValue.filterValue],
              "textSearch"
            );        
          }
        } else {
          // props.setFilterHandler("textSearch", newValue);
        }      
      }}
      onInputChange={(event, newInputValue, reason) => {
        console.log("onInputChange newValue of autocomplete", newInputValue, reason);
        setInputValue(newInputValue);

        if (newInputValue.id) {
        } else if (newInputValue.filterName) {
        } else {
          if (reason === "clear" || (reason == "reset")) {
            setInputValue("");
            props.setFilterHandler("textSearch", "");
          } else if(newInputValue.length > 2) {
            setLoading(true);
            clearTimeout(timeOut.current);
            timeOut.current = setTimeout(
              () => {
                props.setFilterHandler("textSearch", newInputValue);
                setLoading(false);
              },
              600
            );
          }
        }
      }}
      ref={autocompleteRef}
      onFocus={(event) => {
        scrollToRef(autocompleteRef);
      }}
      className={classes.bar}
      getOptionLabel={(option) => {
        return option.title ? option.title : option.name ? option.name : option;
      }}
      renderInput={(params) => (
        <TextField  
        label="Search" 
        // variant="outlined" 
         {...params}
        />
      )}
    />
  );
}
