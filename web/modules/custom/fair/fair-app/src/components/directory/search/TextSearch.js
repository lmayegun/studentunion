/* eslint-disable no-use-before-define */
import React, { useState, useRef } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import { IconButton, InputBase } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: theme.spacing(4),
  },
  bar: {},
}));

export default function TextSearch(props) {
  const classes = useStyles();
  const [value, setValue] = useState(props.filters.textSearch);
  const [inputValue, setInputValue] = useState(props.filters.textSearch);
  const timeOut = useRef(null);
  //console.log("input value", inputValue);
  //console.log("textsearch props", props);
  //console.log("props test on top", props.filters.textSearch);
  return (
    <>
      <TextField
        className={classes.input}
        placeholder="Search"
        inputProps={{ "aria-label": "search directory" }}
        value={value}
        onChange={() => {}}
      />
      <IconButton
        type="submit"
        className={classes.iconButton}
        aria-label="search"
      >
        <SearchIcon />
      </IconButton>
    </>
  );
}
