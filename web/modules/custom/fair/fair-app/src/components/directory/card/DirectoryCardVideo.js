import React from "react";
import LazyLoad from "react-lazyload";
import { makeStyles } from "@material-ui/core/styles";
import ReactPlayer from "react-player";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Badge from "@material-ui/core/Badge";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { Grow } from "@material-ui/core";
import PlayCircleFilledIcon from "@material-ui/icons/PlayCircleFilled";
import YouTubeIcon from "@material-ui/icons/YouTube";

const cardWidth = 320;
const mediaHeight = 210;

const useStyles = makeStyles((theme) => ({
  card: {
    position: "relative",
    width: cardWidth,
    minHeight: 250,
  },
  media: {
    height: mediaHeight,
  },
  contentArea: { padding: theme.spacing(1) },
  headerOverlay: {
    position: "absolute",
    zIndex: 2,
    bottom: 0,
    width: "100%",
  },
  playIcon: {
    fontSize: 110,
  },

  playerWrapper: {
    position: "relative",
    width: "100%",
    height: "100%",
  },
  playerOverlay: {
    position: "absolute",
    width: "100%",
    height: "100%",
    cursor: "pointer",
    left: 0,
    top: 0,
    zIndex: 1,
  },
}));

export default function DirectoryCardVideo(props) {
  const classes = useStyles();

  return (
    // <LazyLoad height={mediaHeight} offset={300}>
      //<Grow in={true}>
        <Box className={classes.root}>
          <Badge
            badgeContent={"online"}
            invisible={true}
            color="primary"
            className={classes.badge}
          >
            <Card
              onClick={async () => {
                //console.log("Card clicked video", props.id);
                await props.openHandler(props.id);
                props.scrollHandler();
              }}
              className={`${classes.card}`}
            >
              <CardActionArea className={classes.actionArea}>
                <CardMedia className={classes.media} title="">
                  <div className={classes.playerWrapper}>
                    <div className={classes.playerOverlay}></div>
                    <ReactPlayer
                      width="100%"
                      height="100%"
                      url={props.videoUrl}
                      light={true}
                      playIcon={
                        <YouTubeIcon
                          color="primary"
                          className={classes.playIcon}
                        />
                      }
                    />
                  </div>
                </CardMedia>
                <CardContent className={classes.contentArea}>
                  <Typography gutterBottom variant="h5" component="h2">
                    {props.title}
                  </Typography>
                  <Typography className="tagline" variant="body2" component="p">
                    {props.tagline}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Badge>
        </Box>
      //</Grow>
    // </LazyLoad>
  );
}
