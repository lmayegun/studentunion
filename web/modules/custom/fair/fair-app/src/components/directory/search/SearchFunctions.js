import Fuse from "fuse.js";
import stringSimilarity from "string-similarity";
import { PageView, initGA, Event } from "../../../utils/Tracking";

const runSearch = (itemType, items, filters, sort, categoriesRanking) => {
  // console.log("Run search", itemType, items, filters, sort, categoriesRanking);

  PageView(window.location.pathname + "/search?" + serialize(filters));

  document.body.style.cursor = 'wait';

  // If items not given, we check if they're provided in fair.page.html as a window variable (stalls, videos)
  if (!items || items.length == 0) {
    if (
      !window[itemType] ||
      Object.keys(window[itemType]).length === 0
    ) {
      return [];
    } else {
      items = window[itemType];
    }
  }

  // console.log("runSearch unfiltered", itemType, items);

  // console.time("TIMER filteredIds");
  const filteredIds = filterCards(items, itemType, filters);
  // console.timeEnd("TIMER filteredIds");
  // console.log("runSearch filtered ids", filteredIds);

  // console.time("TIMER categoriseCards");
  const newCategories = categoriseCards(items, itemType, filteredIds, filters, sort);
  // console.timeEnd("TIMER categoriseCards");
  // console.log("runSearch categorised cards from search", newCategories);

  // console.time("TIMER sortCategories");
  const sortedCategories = sortCategories(
    items,
    itemType,
    sort,
    newCategories,
    categoriesRanking
  );
  // console.timeEnd("TIMER sortCategories");
  // console.log("runSearch sorted categories", sortedCategories);

  document.body.style.cursor = 'default';

  return sortedCategories;
};

const filterCards = (items, itemType, filters) => {
  if (!filters) {
    return Object.keys(items);
  }
  // console.log("vars in filterCards", items, itemType, filters);

  // Filter settings
  const tagsExclusive = false;

  var cardsBeingFiltered = { ...items };
  var finalIds = [];

  // TODO use changedFilterProperty to only reprocess based on the changed filter
  // console.log(
  //   "cards being filtered",
  //   cardsBeingFiltered,
  //   "textsearch",
  //   filters.textSearch
  // );
  // Free text search with external library
  if (filters && filters.textSearch && filters.textSearch !== "") {
    cardsBeingFiltered = filterCardsByText(
      cardsBeingFiltered,
      filters.textSearch
    );
  }

  // Loop through the cards and see if they match the other filters
  // Add to categories
  Object.keys(cardsBeingFiltered).forEach(function (key, index) {
    const card = cardsBeingFiltered[key];
    // console.log('Add to categories', card, key);
    if (!card) {
      console.log("!card", card);
      return;
    }
    var categoryMatch = true;
    const cardCategory = card.fair_stall_category
      ? card.fair_stall_category[0]
      : card.category;

    if (filters.categories) {
      categoryMatch = cardCategory
        ? filters.categories.includes(cardCategory)
        : false;
    }

    var tagsMatch = true;
    const cardTags = card.fair_stall_tags ? card.fair_stall_tags : card.tags;
    if (filters.tags) {
      const matchingTags = cardTags
        ? cardTags.filter((value) => filters.tags.includes(value))
        : [];
      tagsMatch = tagsExclusive
        ? matchingTags.length === filters.tags.length
        : matchingTags.length > 0;
    }

    var filtersEmpty = true;

    if (filters.categories && filters.tags) {
      filtersEmpty =
        filters.categories.length === 0 && filters.tags.length === 0;
    }

    if (filtersEmpty || categoryMatch || tagsMatch) {
      finalIds.push(card.id);
    }
  });
  // console.log("final cards", finalCards);
  return finalIds;
};

const filterCardsByText = (cards, text) => {
  const newCards = Object.values(cards);
  const options = {
    isCaseSensitive: false,
    includeScore: true,
    minMatchCharLength: 2,
    // threshold: 0.6,
    // shouldSort: true,
    // includeMatches: false,
    // findAllMatches: false,
    // minMatchCharLength: 1,
    // location: 0,
    // distance: 100,
    // useExtendedSearch: false,
    // ignoreLocation: false,
    // ignoreFieldNorm: false,
    keys: [
      { name: "name", weight: 1 },
      { name: "fair_stall_tagline", weight: 4 },
      { name: "field_fair_stall_description_summary", weight: 6 },
    ],
  };

  // console.log("Fuse", newCards, options, text);
  const fuse = new Fuse(newCards, options);
  // console.log("new fuse", fuse);
  const fuseResults = fuse.search(text);
  console.log("fuse results", fuseResults);

  const cardsFilteredByScore = fuseResults.filter((a) => {
    return a.score < 0.1;
  });
  console.log("fuse cardsBeingFiltered after score test", cardsFilteredByScore);

  const cardsFilteredAndMapped = cardsFilteredByScore.map((a) => {
    //console.log("cardsBeingFiltered", a);
    a.item.refIndex = a.refIndex;
    return a.item;
  });
  return cardsFilteredAndMapped;
};

const categoriseCards = (items, itemType, ids, filters, sort) => {
  // Settings
  const maxTagCategories = 3;
  var minCardsInCategory = 1;
  var fieldsToCategoriseBy = [];

  if (sort === 'alphabetical') {
    fieldsToCategoriseBy = ["name"];
    minCardsInCategory = -1;
  } else if (itemType === 'events') {
    fieldsToCategoriseBy = ['category'];
    minCardsInCategory = -1;
  } else {
    fieldsToCategoriseBy = ['fair_stall_category'];
  }

  var cardsNotUsedYet = ids;
  // console.log("categoriseCards", itemType, ids, sort);

  var categoriesToUse = [];
  var finalCategories = [];

  // Create target categories based on fields
  // console.time(
  //   "TIMER categoriseCards - Create target categories based on fields"
  // );
  for (let i = 0; i < fieldsToCategoriseBy.length; i++) {
    const field = fieldsToCategoriseBy[i];
    var fieldsToLoop = 0;

    const fieldFrequencies = [];

    fieldFrequencies = getValueFrequencies(items, itemType, field, ids);
    if (field === "tags") {
      fieldsToLoop =
        fieldFrequencies.length > maxTagCategories
          ? maxTagCategories
          : fieldFrequencies.length;
    } else {
      fieldsToLoop = fieldFrequencies.length;
    }
    // console.log("fieldFrequencies", field, fieldFrequencies);

    for (var j = 0; j < fieldsToLoop; j++) {
      if (fieldFrequencies[j]) {
        var include = false;
        if (field == "tags") {
          if (filters[field].includes(fieldFrequencies[i].value)) {
            include = true;
          }
        } else {
          include = true;
        }

        if (include) {
          categoriesToUse.push({
            key: field,
            value: fieldFrequencies[j].value,
          });
        }
      }
    }
  }
  // console.timeEnd(
  //   "TIMER categoriseCards - Create target categories based on fields"
  // );

  // Populate those categories in order added (tags then category field), removing any cards allocated to a category from the overall list
  // console.log("categoriesToUse", categoriesToUse);
  // console.time(
  //   "TIMER categoriseCards - Populate those categories in order added"
  // );
  categoriesToUse.forEach((categoryToUse) => {
    var claimedCards = [];
    claimedCards = cardsNotUsedYet.filter((card) => {
      const value = items[card][categoryToUse.key];
      if (categoryToUse.key == 'name') {
        return getAlphaKeyFromTitle(value) == categoryToUse.value;
      } else {
        return Array.isArray(value)
          ? value.includes(categoryToUse.value)
          : value === categoryToUse.value;
      }
    });

    cardsNotUsedYet = cardsNotUsedYet.filter((card) => {
      const claimedAsPartOfThisCategory = claimedCards.includes(card);
      return !claimedAsPartOfThisCategory;
    });

    finalCategories.push({
      name: categoryToUse.value,
      description: getDescriptionForCategory(
        categoryToUse.value,
        window.fairCategories
      ),
      cards: claimedCards,
    });
  });
  // console.timeEnd(
  //   "TIMER categoriseCards - Populate those categories in order added"
  // );

  // Sort or shuffle here to make sure other category is always at the end
  if (sort == 'alphabetical') {

  } else if (itemType == 'events') {
    finalCategories = finalCategories.sort(function (a, b) {
      const aCard = items[a.cards[0]];
      const bCard = items[b.cards[0]];
      return bCard.categoryOrder - aCard.categoryOrder;
    });
  } else {
    shuffleArray(finalCategories);
  }

  // console.log("SEARCH finalCategories before searching", finalCategories);

  // Remaining cards go in an Other category
  var otherCategoryCards = cardsNotUsedYet;

  finalCategories.forEach((category) => {
    if (sort == 'alphabetical' || itemType == 'events') {

    } else {
      // Don't allow a category with fewer than 3 cards - move to the 'Other' category
      // console.log("other sorting", category);
      if (minCardsInCategory > 0 && category.cards.length < minCardsInCategory) {
        // Collect together
        otherCategoryCards.concat(category.cards);
        // Remove from main list
        finalCategories = finalCategories.filter(
          (categoryToRemove) => categoryToRemove.name != category.name
        );
      }
    }
  });

  if (otherCategoryCards.length > 0) {
    finalCategories.push({
      name: itemType == "videos" ? "Watch again" : "Assorted",
      cards: otherCategoryCards,
    });
  }

  // console.log("SEARCH finalCategories", finalCategories);
  return finalCategories;
};

function getAlphaKeyFromTitle(title) {
  const firstLetter = title.charAt(0);
  if (/(?![×÷])[A-Za-zÀ-ÿ]/.test(firstLetter)) {
    return firstLetter.toUpperCase();
  } else {
    return '#';
  }
}

function shuffleArray(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

function getRandom(arr, n) {
  var result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len) {
    n = len;
  }
  while (n--) {
    var x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}

// get card categories currently being used by cards
const getCategoriesInUse = (cards, fieldName) => {
  let category = [];
  if (!cards) {
    return [];
  }
  for (let i = 0; i < cards.length; i++) {
    category.push(cards[i][fieldName]);
  }
  var result = Array.from(new Set(category));
  return result.filter(Boolean);
};

const getTagsInUse = (cards, fieldName) => {
  let tags = [];
  if (!cards) {
    return [];
  }
  for (let i = 0; i < cards.length; i++) {
    tags = tags.concat(cards[i][fieldName]);
  }
  var result = Array.from(new Set(tags));
  return result.filter(Boolean);
};

/**
 * Get a count of all unique values in an array property of objects
 *
 * e.g. get the count each tag is used on an object
 *
 * @param {*} key
 * @param {*} objects
 */
function getValueFrequencies(items, itemType, key, ids) {
  if (ids.length == 0) {
    return false;
  }

  var alphabetical = false;
  if (key == 'name') {
    alphabetical = true;
  }

  // console.log(items, itemType, key, ids);

  const cacheKey =
    "getValueFrequences" + key + itemType;
  if (window[cacheKey]) {
    return window[cacheKey];
  }
  //console.log('cacheKey', cacheKey)

  //console.time("TIMER getValueFrequencies");
  var tagFrequencies = [];
  var unsortedTagFrequencies = [];
  var sortedTagFrequencies = [];

  ids.forEach((id) => {
    // console.log(itemType, id, key);
    const value = items[id][key];
    const values = !Array.isArray(value) ? [value] : value;
    values.forEach((tag) => {
      if (alphabetical) {
        tag = getAlphaKeyFromTitle(tag);
      }
      if (tag) {
        if (tagFrequencies[tag]) {
          tagFrequencies[tag] = tagFrequencies[tag] + 1;
        } else {
          tagFrequencies[tag] = 1;
        }
      }
    });
  });

  // Populate array of objects
  Object.keys(tagFrequencies).forEach((key) => {
    unsortedTagFrequencies.push({
      value: key,
      count: tagFrequencies[key],
    });
  });

  // Sort descending
  sortedTagFrequencies = unsortedTagFrequencies.sort(function (a, b) {
    return b.count - a.count;
  });

  window[cacheKey] = sortedTagFrequencies;
  // console.timeEnd("TIMER getValueFrequencies");
  return sortedTagFrequencies;
}

const sortCategories = (items, itemType, sort, categories, fairRankings) => {
  var finalCategories = [];

  console.log(items, itemType, sort, categories, fairRankings);

  categories.forEach((category) => {
    var ranking = categories.length;
    if (fairRankings && fairRankings.length > 0) {
      ranking = fairRankings.findIndex((rank) => rank === category.name);
    }
    category.ranking = ranking >= 0 ? ranking : categories.length;

    // Sort cards within each category either by name or randomly
    if (sort == 'alphabetical') {
      const field = 'name';
      // console.log("Sort cards within category alphabetically");
      category.cards = category.cards.sort((a, b) => {
        // console.log("sort == 'alphabetical", a,b);
        var textA = items[a][field].toUpperCase();
        var textB = items[b][field].toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
      });
    } else {
      category.cards = shuffleArray(category.cards);
    }

    category.cards.sort((a, b) => (items[a].pinned ? 1 : -1));

    finalCategories.push(category);
  });

  console.log("finalCategories presort", finalCategories);
  if (sort == 'alphabetical') {
    finalCategories = finalCategories.sort((a, b) =>
      a.name > b.name ? 1 : -1
    );
  } else {
    finalCategories = finalCategories.sort((a, b) =>
      a.ranking > b.ranking ? 1 : -1
    );
  }
  console.log("finalCategories", finalCategories);

  return finalCategories;
};

function serialize(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push(
        v !== null && typeof v === "object"
          ? serialize(v, k)
          : encodeURIComponent(k) + "=" + encodeURIComponent(v)
      );
    }
  }
  return str.join("&");
}

function getMostSimilarStall(stall, stalls) {
  // console.log(stalls);
  //TODO
  const stallText = stall.fair_stall_category + " " + stall.fair_stall_tags;

  let stallTexts = [];
  Object.keys(stalls).forEach((key, index) => {
    const e = stalls[key];
    if (e.id === stall.id) {
      stallTexts.push("");
    } else {
      stallTexts.push(e.fair_stall_category + " " + e.fair_stall_tags);
    }
  });
  const bestMatch = stringSimilarity.findBestMatch(stallText, stallTexts);
  //console.log(bestMatch);

  const index = bestMatch.bestMatchIndex;
  return stalls[Object.keys(stalls)[index]];
}

const getDescriptionForCategory = (category, categories) => {
  const loadedCategory = categories.find((element) => element.name == category);
  // console.log(
  //   "getDescriptionForCategory",
  //   category,
  //   categories,
  //   loadedCategory
  // );
  return loadedCategory ? loadedCategory.description : "";
};

export {
  runSearch,
  filterCards,
  categoriseCards,
  getCategoriesInUse,
  getTagsInUse,
  getRandom,
  getMostSimilarStall,
  shuffleArray,
};
