import React, { Component } from "react";

/* Material-UI components */
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import { makeStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    width: "100%",
    justifyContent: "center",
    backgroundColor: "none",
    textAlign:"center",
    padding: "1% 0",
    borderRadius: 0,
  },
});

const SecondaryHeader = (props) => {
  const classes = useStyles();
  return (
    <AppBar position="static" elevation={0}>
      <Paper className={classes.root}>
        <Typography variant="h6" color="textPrimary" gutterBottom>
          {props.title}
        </Typography>
      </Paper>
    </AppBar>
  );
};
export default SecondaryHeader;
