import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  header: {
    textAlign: "center",
    minHeight: "40vh",
    justifyContent: "center",
    display: "flex",
    alignItems: "center",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    backgroundSize: "cover",
    backgroundAttachment: "fixed",
  },
  text: {},
}));
export default function PrimaryHeader(props) {
  const classes = useStyles();

  const HeaderStyle = props.imageUrl
    ? { backgroundImage: "url(" + props.imageUrl + ")" }
    : {};
  return (
    <div className={classes.header} style={HeaderStyle}>
      <div>
        <div>
          <h1 className="js-quickedit-page-title title page-title">
            {props.title}
          </h1>
        </div>
        <div>
          <h2>{props.tagline}</h2>
        </div>
      </div>
    </div>
  );
}
