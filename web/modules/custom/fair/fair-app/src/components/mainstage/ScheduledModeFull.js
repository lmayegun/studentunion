import React from "react";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "react-moment";
import moment from "moment";
import 'moment-timezone';

const useStyles = makeStyles((theme) => ({
  root: {
    fontSize: "1.5 rem"
  },
  nowPlaying: {
    backgroundColor: theme.palette.background.paper,
    color: "#E23013"
  },
  nowIcon: {
    height: "100%",
    padding: "0"
  },
  scheduleItem: {
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.secondary.main
  }

}));
export default function ScheduledModeFull(props) {
  const classes = useStyles();
  const date = moment();
  const start = props.start;
  const finish = props.finish;

  function display() {
    return (
      <div className={classes.scheduleItem}>
        <Moment format="H:mm" tz={"Etc/UTC"}>{start}</Moment>
        <span> (</span>
        <Moment fromNow>{start}</Moment>
        <span>)</span>
      </div>
    );
  }

  return ( start <= date && finish >= date ? (
    <ListItem className={classes.root} dense={true}> 
      <ListItemAvatar className={[classes.nowIcon, classes.nowPlaying].join(" ")}>
        {/* LIVE */}
        <Avatar className={classes.nowPlaying}>{props.icon}</Avatar>
      </ListItemAvatar>
      <ListItemText className={classes.nowPlaying}
        primary={props.primary}
        secondary={display(props.start, props.finish)}
      />
    </ListItem>
  ) : (    <ListItem className={classes.root} dense={true}> 
      <ListItemAvatar className={classes.nowIcon}>
        <Avatar className={classes.scheduleItem}>{props.icon}</Avatar>
      </ListItemAvatar>
      <ListItemText className={classes.scheduleItem}
        primary={props.primary}
        secondary={display(props.start, props.finish)}
      />
    </ListItem>) );
}
