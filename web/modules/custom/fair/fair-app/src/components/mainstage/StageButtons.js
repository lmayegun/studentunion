import React from "react";

/* Material-UI components */
import {
  makeStyles,
  createMuiTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { themePurpleYellow } from "./../../utils/Theme";

const useStyles = makeStyles((theme) => ({
  wrapper: { margin: theme.spacing(1) },
  button: {
    width: "100%",
  },
}));

export default function StageButtons(props) {
  const classes = useStyles();
  //console.log("props.buttons", props.buttons);

  const goToUrl = (url) => {
    window.open(url);
  };

  return (
    <ThemeProvider theme={themePurpleYellow}>
      {props.buttons.map((button) => {
        const link = button.url;
        const text = button.title;
        if (!link || !text) {
          return <></>;
        }
        return (
          <div className={classes.wrapper}>
            <Button
              size="large"
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={() => goToUrl(link)}
              //   startIcon={<LiveHelpIcon />}
            >
              {text}
            </Button>
            <br />
          </div>
        );
      })}
    </ThemeProvider>
  );
}
