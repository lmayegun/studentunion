import React from "react";
import ReactPlayer from "react-player";

export default function Mainstage(props) {
  return (
    <ReactPlayer
      url={props.url}
      width={props.width}
      height={props.height}
      playing={props.autoplay}
      muted={props.mute}
      volume={null}
      light={props.light}
      playIcon={props.playIcon ? props.playIcon : null}
    />
  );
}
