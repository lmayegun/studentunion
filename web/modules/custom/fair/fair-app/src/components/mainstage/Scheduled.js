import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ScheduledMode from "./ScheduledMode";
import StageButtons from "./StageButtons";
import Paper from "@material-ui/core/Paper";
import "moment-timezone";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    color: theme.palette.secondary.main,
    textAlign: "center",
  },
  paper: {
    overflow: "auto",
    flexGrow: "1"
    // height: "550px",
  },
}));

const Scheduled = (props) => {
  const classes = useStyles();
  const mode = props.mode;
  const date = moment().unix();

    return (
      <>
        <Paper className={classes.paper}>
          <StageButtons buttons={props.stageButtons} />
          {/* {mode === "full" && <h3 className={classes.header}>Schedule</h3>} */}
          <List className={classes.root} dense={mode === "full"}>
            {props.items.map((data, i) => {
              return (
                date <= data.field_fair_schedule_end && (
                  <ScheduledMode
                    title={data.name}
                    start={moment.unix(data.field_fair_schedule_start)}
                    finish={moment.unix(data.field_fair_schedule_end)}
                    live={
                      data.field_fair_schedule_start <= date &&
                      data.field_fair_schedule_end >= date
                        ? "true"
                        : "false"
                    }
                    i={i}
                    mode={mode === "full" ? "full" : "reduced"}
                  />
                )
              );
            })}
          </List>
        </Paper>
      </>
    );
};

export default Scheduled;
