import React from "react";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "react-moment";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScheduledModeReduced(props) {
  const classes = useStyles();

  function display(title, start) {
    return (
      <div>
        {title}&nbsp;<Moment fromNow>{start}</Moment>
      </div>
    );
  }

  switch (props.i) {
    case 0:
      return (
        <div>
          <List
            subheader={<ListSubheader>NOW SHOWING</ListSubheader>}
            className={classes.root}
          >
            <ListItem>
              <ListItemText primary={props.primary} />
            </ListItem>
          </List>
        </div>
      );
    case 1:
      return (
        <div>
          <List
            subheader={<ListSubheader>NEXT</ListSubheader>}
            className={classes.root}
          >
            <ListItem>
              <ListItemText primary={display(props.primary, props.start)} />
            </ListItem>
          </List>
        </div>
      );
    default:
      return (
        <div>
          <ListItem>
            <ListItemText primary={display(props.primary, props.start)} />
          </ListItem>
        </div>
      );
  }
}
