import React from "react";
import PlayCircleFilledOutlinedIcon from "@material-ui/icons/PlayCircleFilledOutlined";
import AccessTimeOutlinedIcon from "@material-ui/icons/AccessTimeOutlined";
import ScheduledModeFull from "./ScheduledModeFull";
import ScheduledModeReduced from "./ScheduledModeReduced";

export default function ScheduledMode(props) {
  const title = props.title;
  const start = props.start;
  const finish = props.finish;
  const live = props.live;
  const i = props.i;
  const mode = props.mode;

  if (mode === "full") {
    return (
      <ScheduledModeFull
        icon={
          live === "true" ? (
            <PlayCircleFilledOutlinedIcon />
          ) : (
            <AccessTimeOutlinedIcon />
          )
        }
        i={i}
        primary={title}
        start={start}
        finish={finish}
      />
    );
  } else {
    return (
      <ScheduledModeReduced
        primary={title}
        i={i}
        start={start}
        finish={finish}
      />
    );
  }
}
