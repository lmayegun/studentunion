import React, { useState, useEffect, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Directory from "../directory/Directory";
import ScrollToTopOnMount from "./ScrollToTopOnMount";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
    boxShadow: "none",
  },
  control: {
    padding: theme.spacing(0),
  },
}));

const EventsPage = (props) => {
  const classes = useStyles();
  const [firstDirectoryLoad, setFirstDirectoryLoad] = useState(true);

  return (<>
    <ScrollToTopOnMount />
    <Directory
      items={props.events}
      windowVariable={"events"}
      data-tour="events"
      scrollHandler={() => { }}
      type="events"
      firstDirectoryLoad={firstDirectoryLoad}
      setFirstDirectoryLoad={setFirstDirectoryLoad}
      currentCategories={props.currentCategories}
      currentCategoriesHandler={props.currentCategoriesHandler}
      {...props}
    /></>);
};

export default EventsPage;
