import React, { useState } from "react";
import {
  makeStyles,
  createMuiTheme,
  ThemeProvider
} from "@material-ui/core/styles";
import { customMemoLog } from "../../utils/Hooks";

import { themePurpleYellow } from "./../../utils/Theme";

import { Link } from "react-router-dom";
import FlipCountdown from "../countdown/FlipCountdown";
import LiveTvIcon from "@material-ui/icons/LiveTv";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";

import LaunchIcon from '@material-ui/icons/Launch';

import SecondaryHeader from "./../header/SecondaryHeader";
import Mainstage from "./../mainstage/Mainstage";
import Scheduled from "./../mainstage/Scheduled";
import Directory from "./../directory/Directory";
import FairSchedule from "./../fairInfo/FairSchedule";
import ScrollToTopOnMount from "./ScrollToTopOnMount";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  centerContainer: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  container: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
    boxShadow: "none",
  },
  control: {
    padding: theme.spacing(0),
  },
  buttonContainer: {
    margin: theme.spacing(1),
    display: "flex",
    justifyContent: "center",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column"
    }
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function StallsPage(props) {
  const classes = useStyles();

  const widthDefault = "100%";
  const heightDefault = "350px";
  const styleIframe = {
    float: "right",
    padding: "1em",
  };

  const [firstDirectoryLoad, setFirstDirectoryLoad] = useState(true);

  const categoriesRanking = window.fairData.field_fair_ranking_reference
    ? window.fairData.field_fair_ranking_reference
    : [];

  function RenderButtons() {

    const goToUrl = (url) => {
      window.open(url);
    };
    // console.log('props.fairData.field_fair_buttons_over_categori', window.fairData.field_fair_buttons_over_categori);

    if (
      window.fairData &&
      window.fairData.field_fair_buttons_over_categori &&
      window.fairData.field_fair_buttons_over_categori.length > 0
    ) {
      console.log('HERE');
      return (
        <ThemeProvider theme={themePurpleYellow}>
          <div className={classes.buttonContainer}>
            {window.fairData.field_fair_buttons_over_categori.map((link) => (<Button
              size="large"
              variant="contained"
              color="primary"
              onClick={() => goToUrl(link.url)}
              // startIcon={<LaunchIcon />}
              className={`animate-pulse-slow ${classes.button}`}
            >
              {link.title}
            </Button>))}
          </div>
        </ThemeProvider>
      );
    } else {
      return <></>;
    }
  }

  function RenderFuturePage() {
    //console.log"FairComingUp", props);
    // Render
    if (props.timeMode === "future") {
      return (
        <>
          <Grid container className={classes.centerContainer}>
            {window.fairData.field_fair_countdown_heading ? (
              <Grid item>
                <h2 class="countdown-header">
                  {window.fairData.field_fair_countdown_heading}
                </h2>
              </Grid>
            ) : (
              <></>
            )}
            <Grid item>
              <FlipCountdown
                timestamp={window.fairData.field_fair_start}
              ></FlipCountdown>
            </Grid>
            {window.fairData.field_fair_countdown_subtitle ? (
              <Grid item>
                <h2 class="countdown-header">
                  {window.fairData.field_fair_countdown_subtitle}
                </h2>
              </Grid>
            ) : (
              <></>
            )}
          </Grid>

          {props.scheduleItems.length > 0 ? (
            <>
              <SecondaryHeader
                title={window.fairData.field_fair_future_schedule}
              />
              <Grid item>
                <FairSchedule items={props.scheduleItems}></FairSchedule>
              </Grid>
            </>
          ) : (
            <></>
          )}
          <SecondaryHeader title={window.fairData.field_fair_stalls_title} />
        </>
      );
    } else {
      return <></>;
    }
  }

  function RenderOverMessage() {
    if (props.timeMode === "past") {
      return (
        <>
          <SecondaryHeader title={window.fairData.field_fair_header_over} />
          <Container className={classes.container}>
            {window.fairData.field_fair_message_over}
          </Container>
        </>
      );
    } else {
      return <></>;
    }
  }
  function RenderMainstage() {
    if (props.timeMode === "present") {
      return (
        <>
          <SecondaryHeader
            title={window.fairData.field_fair_stall_now_playing}
          />
          <Container className={classes.container}>
            <Grid container spacing={2}>
              <Grid item sm={6} xs={12}>
                <Mainstage
                  width={widthDefault}
                  height={heightDefault}
                  url={props.videoUrl}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <Scheduled items={props.scheduleItems} />
                <Button
                  component={Link}
                  to={props.baseUrl + "/stage"}
                  startIcon={<LiveTvIcon />}
                  variant="contained"
                  color="secondary"
                >
                  Watch live
                </Button>
              </Grid>
            </Grid>
          </Container>
        </>
      );
    } else {
      return <></>;
    }
  }

  return (
    <Grid>
      {/* <RenderMainstage /> */}
      <ScrollToTopOnMount />
      <RenderFuturePage />
      <RenderOverMessage />
      <RenderButtons />
      <Directory
        items={window['stalls']}
        windowVariable={'stalls'}
        cardLoading={props.cardLoading}
        type="stalls"
        search={true}
        currentCategories={props.currentCategories}
        currentCategoriesHandler={props.currentCategoriesHandler}
        filters={props.filters}
        openHandler={props.openHandler}
        filtersHandler={props.filtersHandler}
        scrollHandler={null}
        firstDirectoryLoad={firstDirectoryLoad}
        setFirstDirectoryLoad={setFirstDirectoryLoad}
        categoriesRanking={categoriesRanking}
      // {...props}
      />
    </Grid>
  );
}

export default React.memo(StallsPage, (prevProps, nextProps) => {
  const propsMatch =
    prevProps.currentCategories === nextProps.currentCategories;
  customMemoLog("StallsPage", prevProps, nextProps, propsMatch);
  return propsMatch;
});
