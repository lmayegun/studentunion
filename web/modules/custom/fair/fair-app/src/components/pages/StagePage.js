import React, { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import Mainstage from "./../mainstage/Mainstage";
import Scheduled from "./../mainstage/Scheduled";
//import StageButtons from "./../mainstage/StageButtons";
import Directory from "./../directory/Directory";
import Collapse from "@material-ui/core/Collapse";
import ScrollToTopOnMount from "./ScrollToTopOnMount";

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop);
// console.log("scroll behaviour", scrollToRef);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  playerWrapper: {
    position: "relative",
  },
  overlay: {
    position: "absolute",
    width: "100%",
    height: "100%",
    cursor: "pointer",
    left: 0,
    top: 0,
    zIndex: 1,
  },
  paper: {
    padding: 0,
    color: theme.palette.text.primary,
    boxShadow: "none",
  },
  sidebar: {
    display: "flex",
    flexDirection: "column",
    maxHeight: "550px"
  },
  control: {
    padding: theme.spacing(0),
  },
}));

export default function StagePage(props) {
  const classes = useStyles();

  const stageScrollRef = useRef(null);

  const [firstDirectoryLoad, setFirstDirectoryLoad] = useState(true);

  const widthDefault = "100%";
  const heightDefault = "550px";
  const scheduledMode = "full";
  const autoPlay = "true";
  console.log('StagePage', props.url, "all", props);
  return (
    <div className={classes.root} ref={stageScrollRef}>
      <ScrollToTopOnMount />
      <Grid container alignItems="stretch" spacing={0}>
        {props.url !== null || props.currentVideoData !== null ? (
          <>
            <Grid item xs={12} sm={10}>
              <Mainstage
                data-tour="stream"
                width={widthDefault}
                height={heightDefault}
                url={
                  props.currentVideoData
                    ? props.currentVideoData.field_fair_video_url[0].url
                    : props.url
                }
                autoplay={autoPlay}
              />
            </Grid>
            <Grid item xs={12} sm={2} dataTour="schedule" className={classes.sidebar}>
              <Collapse in={props.currentVideoData}>
                <div className={classes.playerWrapper}>
                  <div
                    className={classes.overlay}
                    onClick={() => {
                      scrollToRef(stageScrollRef);
                      props.resetToLivestream();
                    }}
                  ></div>
                  <Mainstage
                    width={"100%"}
                    height={133}
                    url={props.url}
                    autoplay={true}
                    playing={true}
                    mute={true}
                  />
                </div>
              </Collapse>
              {/* <StageButtons buttons={props.stageButtons} /> */}
              <Scheduled items={props.scheduleItems} stageButtons={props.stageButtons} mode={scheduledMode} />
            </Grid>
          </>
        ) : (
          <></>
        )}
        {Object.keys(window["videos"]).length > 0 ?
          <Grid item xs={12}>
            <Directory
              items={window["videos"]}
              windowVariable={"videos"}
              data-tour="videos"
              scrollHandler={() => scrollToRef(stageScrollRef)}
              type="videos"
              firstDirectoryLoad={firstDirectoryLoad}
              setFirstDirectoryLoad={setFirstDirectoryLoad}
              {...props}
            />
          </Grid> : <></>}
      </Grid>
    </div>
  );
}
