import React, { useState, useEffect, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

import Category from "./../directory/Category";
import Directory from "./../directory/Directory";
import DirectoryCard from "./../directory/card/DirectoryCard";
import LiveTvIcon from "@material-ui/icons/LiveTv";
import SavedJoinedCategory from "../directory/SavedJoinedCategory";
import ScrollToTopOnMount from "./ScrollToTopOnMount";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
    boxShadow: "none",
  },
  control: {
    padding: theme.spacing(0),
  },
}));

//here we get the stall Id and return data for directory
//const id = 12;return window.stalls.find((stall) => stall.id === id);
const convertToStallData = (stallArr) => {
  const fullStallsArray = [];
  for (let i = 0; i < stallArr.length; i++) {
    const fullStall = window.stalls[stallArr[i]];
    if (fullStall !== undefined) {
      fullStallsArray.push(fullStall);
    }
  }
  //console.log("stalls Array", fullStallsArray);
  return fullStallsArray;
};

const SavedPage = (props) => {
  console.log("SavedPage", props);
  const savedStalls = window.userDetails.stalls_favourited;
  const joinedStalls = window.userDetails.stalls_joined;
  const classes = useStyles();
  const savedCards = savedStalls; //convertToStallData(savedStalls);
  const joinedCards = joinedStalls; //convertToStallData(joinedStalls);

  const [saved, setSaved] = useState(savedCards);
  const [joined, setJoined] = useState(joinedCards);

  useEffect(() => {
    const checkSaved = (stallsArr) => {
      if (stallsArr.length !== savedCards.length) {
        setSaved(savedCards);
      }
    };
    const checkJoined = (stallsArr) => {
      if (stallsArr.length !== joinedCards.length) {
        setJoined(joinedCards);
      }
    };
    checkJoined(joinedCards);
    checkSaved(savedCards);
  }, []);

  //console.log("state", saved, joined);
  return (
    <div className={classes.root}>
      <ScrollToTopOnMount />
      <SavedJoinedCategory
        directoryType='stalls'
        items={window['stalls']}
        ids={saved}
        cardsProp="cardsSaved"
        title="Saved for later"
        openHandler={props.openHandler}
        view="cards"
        nonCategoryCategory={true}
      />
      {props.joinEnabledForFair ?
        <SavedJoinedCategory
          directoryType='stalls'
          items={window['stalls']}
          ids={joined}
          cardsProp="cardsJoined"
          title="Joined"
          openHandler={props.openHandler}
          view="cards"
          nonCategoryCategory={true}
        /> : <></>}
    </div>
  );
};

export default SavedPage;
