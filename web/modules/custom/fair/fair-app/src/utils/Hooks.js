import React from "react";

export function customMemoLog(functionName, prevProps, nextProps, match, log) {
  if (log) {
    let diff = Object.keys(nextProps).reduce((diff, key) => {
      if (prevProps[key] === nextProps[key]) return diff;
      return {
        ...diff,
        [key]: nextProps[key],
      };
    }, {});

    //console.log(
    //   "MEMO - ARE EQUAL",
    //   functionName,
    //   // prevProps,
    //   // nextProps,
    //   diff,
    //   match
    // );
  }
}

// https://joshwcomeau.com/react/persisting-react-state-in-localstorage/
export function useStateWithStorage(defaultValue, key, overwrite) {
  const [value, setValue] = React.useState(() => {
    const storageValue = window.localStorage.getItem(key);
    if (storageValue !== null && !overwrite) {
      // console.log("Loading " + key + " from localStorage");
      return JSON.parse(storageValue);
    } else {
      // console.log("Loading " + key + " from default Value");
      return defaultValue;
    }
  });
  React.useEffect(() => {
    // console.log("Setting " + key + " in localStorage");
    window.localStorage.setItem(key, JSON.stringify(value));
    window.localStorage.setItem(key + "_updated", new Date().getTime());
  }, [key, value]);
  return [value, setValue];
}
