import axios from "axios";

/**
 * For API calls via JSON api, see fair.routing.yml for the routes and how they are linked to the controller
 */

/* Getting initial data */
// https://www.robinwieruch.de/react-hooks-fetch-data

const apiRoot = "";

export const fetchUserToken = async () => {
  const token = await axios("/session/token/");
  return token.data;
};

export const fetchStalls = async (fairId) => {
  const result = await axios(apiRoot + "/fair/" + fairId + "/api/stalls/basic");
  //console.log("API FETCH fetchStalls", result);
  return result.data;
};

export const fetchStallsLive = async (fairId) => {
  const path = apiRoot + "/fair/" + fairId + "/api/stalls/live";
  const result = await axios(path);
  //console.log("API FETCH fetchStallsLive", path, result);
  return result.data;
};

export const fetchStallComments = async (id, timesince) => {
  const path =
    apiRoot + "/fair/stall/comments/" + id + (timesince ? "/" + timesince : "");
  const result = await axios(path);
  // console.log("API FETCH fetchStallComments", timesince, path, result);
  return result.data;
};

export const fetchScheduleItems = async (fairId) => {
  const result = await axios(apiRoot + "/fair/" + fairId + "/api/schedule");
  //console.log("API FETCH fetchScheduleItems", result);
  return result.data;
};

export const fetchVideos = async (fairId) => {
  const result = await axios(apiRoot + "/fair/" + fairId + "/api/videos/full");
  //console.log("API FETCH fetchVideos", result);
  return result.data;
};

export const fetchStallFull = async (fairId, id) => {
  const result = await axios(
    apiRoot + "/fair/" + fairId + "/api/stalls/full/" + id
  );
  var fullStall = result.data[0];
  fullStall.last_full_fetch = new Date();
  //console.log("API FETCH fetchStallFull", fullStall);
  return fullStall;
};

export const fetchStallEvents = async (fullStall) => {
  var path =
    "/whats-on/export/fair/";
  if (fullStall.entity_type == 'group') {
    path = path + fullStall.group_id;
  } else {
    if (!fullStall.field_fair_stall_group_reference) {
      return false;
    }
    path = path + fullStall.field_fair_stall_group_reference;
  }

  try {
    const result = await axios(path);
    console.log("API FETCH - fetchStallEvents", result);
    return result.data;
  } catch (error) {
    return [];
  }
};

export const fetchStallProducts = async (fullStall) => {
  const path =
    "https://studentsunionucl.org/clubs-societies/export/products/" +
    fullStall.field_fair_stall_group_reference;

  if (!fullStall.field_fair_stall_group_reference) {
    return false;
  }

  try {
    const result = await axios(path);
    //console.log("API FETCH Drupal 7 - fetchStallProducts", result);
    return result.data.products;
  } catch (error) {
    return [];
  }
};

export const fetchUserJoined = async (fairId, uid) => {
  try {
    const stallIds = await axios(
      apiRoot + "/fair/" + fairId + "/api/stalls/user/joined/" + uid
    );
    return Object.values(stallIds);
  } catch (error) {
    //console.log("fetchUserJoined error", error);
    return null;
  }
};

/* Getting JSON:API details */

export const fetchUserStallActions = async (stallId) => {
  if (stallId) {
    const response = await axios("/fair/stall/" + stallId + "/user_actions");
    //console.log("API FETCH fetchUserStallActions", response);
    return response.data;
  }
};

export const fetchStallCommentInfo = async (stallId) => {
  if (stallId) {
    const comment = await axios("/fair/stall/comment/" + stallId);
    //console.log("fetchStallCommentInfo", stallId, comment);
    return {
      type: comment.data.comment_type,
      entityType: comment.data.entity_type,
      field: comment.data.field_name,
      stallType: comment.data.stall_type,
      stallId: comment.data.stall,
    };
  }
};

/* Saving things */


export const postAddToCart = async (userToken, stallUserActions, variation_id) => {
  if (!userToken) {
    return false;
  }

  var endpoint = "/fair/add_to_cart/" + variation_id;

  var method = "POST";

  const body = JSON.stringify({
    data: [
      {
        type: stallUserActions.stall_type,
        id: stallUserActions.stall_uuid,
        variation_id: variation_id
      },
    ],
  });

  const response = await fetch(endpoint, {
    method: method,
    headers: {
      Accept: "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json",
      "X-CSRF-Token": userToken,
    },
    body: body,
  });

  console.log("postAddToCart body response", body, response);

  return response.ok;
};

export const postStallFavourite = async (userToken, stallUserActions) => {
  if (!userToken) {
    return false;
  }

  var endpoint =
    "/jsonapi/profile/fairs/" +
    stallUserActions.favourite.bundle +
    "/relationships/" +
    stallUserActions.favourite.field;

  var method = stallUserActions.favourite.favourited ? "DELETE" : "POST";

  //can we get a stall id here so it's easier to compare with the favourited array in js?
  const body = JSON.stringify({
    data: [
      {
        type: stallUserActions.stall_type,
        id: stallUserActions.stall_uuid,
      },
    ],
  });

  const response = await fetch(endpoint, {
    method: method,
    headers: {
      Accept: "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json",
      "X-CSRF-Token": userToken,
    },
    body: body,
  });

  //console.log("postStallFavourite body response", body, response);

  return response.ok;
};

export const postStallComment = async (
  userToken,
  stallUserActions,
  comment,
  parentUuid,
  category,
  user_role
) => {
  //console.log("postStallComment", stallUserActions, parentUuid);

  if (!userToken && !comment) {
    return false;
  }

  var endpoint;
  if (stallUserActions.stall_entity_type == 'group') {
    endpoint = "/jsonapi/comment/group_q_a/";
  } else if (stallUserActions.stall_entity_type == 'election_candidate') {
    endpoint = "/jsonapi/comment/candidate_q_a/";
  } else {
    endpoint = "/jsonapi/comment/fair_stall_comment/";
  }
  var method = "POST";

  let attributes = {
    entity_type: stallUserActions.stall_entity_type,
    field_name: stallUserActions.comment.field_name,
    comment_body: {
      value: comment,
    },
    field_comment_category: {
      value: category,
    },
    field_comment_user_role: {
      value: user_role,
    },
  };

  let relationships = {
    entity_id: {
      data: {
        type: stallUserActions.stall_type,
        id: stallUserActions.stall_uuid,
      },
    },
  };

  if (parentUuid) {
    relationships.pid = {
      data: {
        type: stallUserActions.comment.comment_type,
        id: parentUuid,
      },
    };
  }

  let body = JSON.stringify({
    data: {
      type: stallUserActions.comment.comment_type,
      attributes: attributes,
      relationships: relationships,
    },
  });

  //console.log("post comment body", body);

  const response = await fetch(endpoint, {
    method: method,
    headers: {
      Accept: "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json",
      "X-CSRF-Token": userToken,
    },
    body: body,
  });
  const json = await response.json();
  //console.log(json);
  if (response.ok) {
    //console.log("response ok");
    return true;
  } else {
    //console.log("response not ok");
    return false;
  }
};

export const postStallDeleteComment = async (stallId, commentId) => {
  console.log("postStallDeleteComment", stallId, commentId);
  if (commentId) {
    const deleted = await axios(
      "/fair/stall/" + stallId + "/comment/" + commentId + "/delete/"
    );
    return deleted.data;
  }
};

export const postStallSubscribe = async (stallId, email) => {
  if (stallId) {
    const subscribed = await axios(
      "/fair/stall/" + stallId + "/subscribe/" + email
    );
    return subscribed.data;
  }
};

export const postStallJoin = async (stallId, userToken, stallUserActions) => {
  if (!userToken) {
    return false;
  }
  if (stallId) {
    const joined = await axios(
      "/fair/stall/" + stallId + "/join"
    );
    return joined.data;
  }
};

export const syncJoinFavWithDB = (stall, action, userDetails) => {
  if (action) {
    if (!userDetails.includes(stall)) {
      userDetails.push(stall);
    }
  } else {
    if (userDetails.includes(stall)) {
      userDetails.pop(stall);
    }
  }
};
