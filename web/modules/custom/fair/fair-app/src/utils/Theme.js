import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";

// Defaults you can override here:
// https://material-ui.com/customization/default-theme/
export const theme = createMuiTheme({
  shape: {
    borderRadius: 3,
  },
  typography: {
    fontFamily: [
      "freight-sans-pro",
      "Roboto",
      "Helvetica Neue",
      "Arial",
      "Sans-serif",
    ].join(","),
    fontSize: 15,
  },
  palette: {
    primary: {
      light: "#F26640",
      main: "#F26640",
      dark: "#CE4321",
      contrastText: "#FFF",
    },
    secondary: {
      light: "#082244",
      main: "#082244",
      dark: "#0d366b",
      contrastText: "#FFF",
    },
    purple: {
      light: "#6C3F99",
      main: "#6C3F99",
      dark: "#6C3F99",
      contrastText: "#FFF",
    },
    yellow: {
      light: "#FEC340",
      main: "#FEC340",
      dark: "#FEC340",
      contrastText: "#082244",
    },
    text: {    
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.62)",
      disabled: "rgba(0, 0, 0, 0.48)",
      hint: "rgba(0, 0, 0, 0.48)",
    }
  },
});

export const themeAccessible = createMuiTheme({
  ...theme,
  palette: {
    primary: {
      light: "#E23013",
      main: "#E23013",
      dark: "#E23013",
      contrastText: "#FFF",
    },
  },
});

export const themePurpleYellow = createMuiTheme({
  ...theme,
  palette: {
    primary: {
      light: "#6c3f99",
      main: "#6c3f99",
      dark: "#6c3f99",
      contrastText: "#fff",
    },
    secondary: {
      light: "#FEC340",
      main: "#FEC340",
      dark: "#FEC340",
      contrastText: "#082244",
    },
  },
});