<?php

/**
 * @file
 * Contains fair_event.page.inc.
 *
 * Page callback for Fair event entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Fair event templates.
 *
 * Default template: fair_event.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_fair_event(array &$variables) {
  // Fetch FairEvent Entity Object.
  $fair_event = $variables['elements']['#fair_event'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
