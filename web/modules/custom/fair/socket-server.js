var http = require("http").createServer(handler);

http.listen(8083);
console.log("Chatserver listening on port 8083");

function handler(req, res) {
  res.writeHead(200);
  res.end("Example Chat Server");
}
