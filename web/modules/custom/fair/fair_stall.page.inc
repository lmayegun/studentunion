<?php

/**
 * @file
 * Contains fair_stall.page.inc.
 *
 * Page callback for Fair stall entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Fair stall templates.
 *
 * Default template: fair_stall.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_fair_stall(array &$variables) {
  // Fetch FairStall Entity Object.
  $fair_stall = $variables['elements']['#fair_stall'];
  $variables['fair_stall'] = $fair_stall;

  $fairs = $fair_stall->fair_stall_reference->referencedEntities();
  $variables['fairs'] = $fairs;

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
