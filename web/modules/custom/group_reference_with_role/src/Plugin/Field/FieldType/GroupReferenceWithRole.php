<?php

namespace Drupal\group_reference_with_role\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Provides a field type of group reference with role.
 * 
 * @FieldType(
 *   id = "group_reference_with_role",
 *   label = @Translation("Group reference with group role"),
*    description = @Translation("Stores a reference to a group and to a role of that group's type."),
 *   default_formatter = "group_reference_with_role_label_formatter",
 *   default_widget = "group_reference_with_role_autocomplete_widget"
 * )
 */
class GroupReferenceWithRole extends FieldItemBase {

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        return array(
            'columns' => array(
                'group' => array(
                    'type' => 'int',
                    'size' => 'normal',
                    'not null' => FALSE,
                ),
                'group_role' => array(
                    'type' => 'int',
                    'size' => 'normal',
                    'not null' => FALSE,
                )
            ),
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        $properties = [];
        $properties['group'] = DataDefinition::create('integer')
            ->setLabel(t('Group ID'))
            ->setDescription(t('The ID of the referenced group.'))
            ->setSetting('unsigned', TRUE);

        $properties['group_role'] = DataDefinition::create('integer')
            ->setLabel(t('Role ID'))
            ->setDescription(t('The ID of the referenced group role.'))
            ->setSetting('unsigned', TRUE);
    
        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
        $group = $this->get('group')->getValue();
        if($group === NULL || $group === '') {
            return true;
        }
    }
}