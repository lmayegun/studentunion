<?php

namespace Drupal\election\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

 /**
   * Plugin implementation of the 'group_reference_with_role_label' formatter.
   *
   * @FieldFormatter (
   *   id = "group_reference_with_role_label_formatter",
   *   label = @Translation("Group name with role"),
   *   field_types = {
   *     "group_reference_with_role"
   *   }
   * )
   */
  class GroupReferrenceWithRoleLabelFormatter extends FormatterBase {
    /**
 * {@inheritdoc}
 */
public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
  $elements = array();

  foreach ($items as $delta => $item) {
    $markup = '';
    
    $group = \Drupal::entityTypeManager()->getStorage('group')->load($item->group);
    $markup = 'Must be a member of "' . $group->get('label')->value.'"';
    if(!empty($item->group_role)) {
      $group_role = \Drupal::entityTypeManager()->getStorage('group_role')->load($item->group_role);
      $markup = ' with the group role of "' . $group_role->get('label')->value.'"';
    }

    $elements[$delta] = array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }

  return $elements;
}
  }