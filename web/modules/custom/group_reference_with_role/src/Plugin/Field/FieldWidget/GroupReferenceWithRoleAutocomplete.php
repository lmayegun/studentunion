<?php

namespace Drupal\group_reference_with_role\Plugin\Field\FieldWidget;
 
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
 
/**
 * Plugin implementation of the 'group_reference_with_role' widget.
 *
 * @FieldWidget(
 *   id = "group_reference_with_role_autocomplete_widget",
 *   label = @Translation("Group reference with role - autocomplete widget"),
 *   description = @Translation("Select both group and role with autocomplete"),
 *   field_types = {
 *     "group_reference_with_role",
 *   },
 *   multiple_values = TRUE,
 * )
 */
 
class GroupReferenceWithRoleAutocomplete extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    
    $field_name = $this->fieldDefinition->getName();

   $element['group'] = [
    '#type' => 'entity_autocomplete',
    '#title' => $this->t('Group'),
    '#target_type' => 'group',
    '#tags' => false,
    '#required' => false,
    '#maxlength' => NULL,
    ];
    $element['group_role'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('With this role'),
        '#target_type' => 'group_role',
        '#tags' => false,
        '#required' => false,
        '#maxlength' => NULL,
    ];

      //setting default value to all fields from above
      $childs = Element::children($element);
      foreach ($childs as $child) {
          $element[$child]['#default_value'] = isset($items[$delta]->{$child}) ? $items[$delta]->{$child} : NULL;
      }
     
      // Wrap in fieldset and make inline
      $element += array(
        '#type' => 'fieldset',
        //'#attributes' => array('class' => array('container-inline')),
      );

      return $element;
  }
}