<?php

namespace Drupal\commerce_product_restriction\Plugin\Commerce\ProductRestriction;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginBase;
use Drupal\commerce_product_restriction\Plugin\ProductRestrictionPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

use Drupal\user\Entity\User;

/**
 * Provides product restriction by user role.
 *
 * @ProductRestrictionPlugin(
 *   id = "purchase_quantity_restriction",
 *   label = @Translation("Restrict quantity of purchase for product (across all variations in all user's orders)"),
 *   category = @Translation("User"),
 *   entity_type = "commerce_product"
 * )
 */
class PurchaseQuantityRestriction extends ProductRestrictionPluginBase implements ProductRestrictionPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'uids' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['quantity'] = [
      '#type' => 'number',
      '#title' => 'Quantity customer can purchase (across all of their completed or processing orders)',
      '#default_value' => $this->configuration['quantity'] ?? 1,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['quantity'] = $values['quantity'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);

    $account = User::load(\Drupal::currentUser()->id());

    if ($entity->getEntityTypeId() == 'commerce_product') {
      $count = $this->countUserPurchasedVariationOfProduct($account, $entity);
    }
    else {
      $count = $this->countUserPurchasedVariationOfProduct($account, $entity->getProduct());
    }

    return $count < intval($this->configuration['quantity']);
  }

  /**
   * Get the existing purchased number of variations for this product.
   *
   * Or specified variation IDs.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account.
   * @param \Drupal\commerce_product\Entity\Product $product
   *   Product entity.
   * @param array|null $variation_ids
   *   Array of variation Ids or null.
   *
   * @return int
   *   Count.
   */
  public function countUserPurchasedVariationOfProduct(AccountInterface $account, Product $product, array $variation_ids = NULL) {
    if (!$variation_ids) {
      $variation_ids = $product->getVariationIds();
    }

    if (!$variation_ids || count($variation_ids) == 0) {
      return FALSE;
    }

    $query = \Drupal::database()->select('commerce_order_item', 'i');
    $query->join('commerce_order', 'o', 'i.order_id = o.order_id');

    // @todo allow selecting order states in config.
    $stateCondition = $query->orConditionGroup()
      ->condition('o.state', 'processing')
      ->condition('o.state', 'completed');

    $query->fields('i', ['order_item_id'])
      ->condition('i.purchased_entity', $variation_ids, 'IN')
      ->condition('o.uid', $account->id(), '=')
      ->condition('i.quantity', 0, ">")
      ->condition($stateCondition)
      ->distinct();

    $ids = $query->execute()->fetchCol();

    return count($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function accessErrorMessage($product_or_variation) {
    return new TranslatableMarkup(
      "You have already purchased the maximum quantity for this product (@quantity).",
      [
        '@quantity' => $this->configuration['quantity'],
      ]
    );
  }

}
