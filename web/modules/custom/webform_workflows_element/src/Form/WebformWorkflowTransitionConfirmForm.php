<?php

namespace Drupal\webform_workflows_element\Form;

use Drupal;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Defines a confirmation form to transition a submission.
 */
class WebformWorkflowTransitionConfirmForm extends ConfirmFormBase {

  /**
   * Webform.
   *
   * @var WebformInterface
   */
  protected WebformInterface $webform;

  /**
   * Webform submission.
   *
   * @var WebformSubmissionInterface
   */
  protected WebformSubmissionInterface $webform_submission;

  /**
   * Workflow.
   *
   * @var WorkflowInterface
   */
  protected WorkflowInterface $workflow;

  /**
   * Transition.
   *
   * @var TransitionInterface
   */
  protected TransitionInterface $transition;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?TranslatableMarkup {
    return $this->t('Do you want to perform "@transition" on submission @id?', [
      '@transition' => $this->transition->label(),
      '@id' => $this->webform_submission->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Yes');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText(): TranslatableMarkup {
    return $this->t('No');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, WebformSubmissionInterface $webform_submission = NULL, WorkflowInterface $workflow = NULL, string $transition = NULL): array {
    // Set variables:
    $this->webform_submission = $webform_submission;
    $this->webform = $webform;
    $this->workflow = $workflow;
    $this->transition = $this->workflow->getTypePlugin()
      ->getTransition($transition);

    // Create generic confirmation form:
    $form = parent::buildForm($form, $form_state);

    // Check access to transition, for better UX:
    /** @var \Drupal\webform_workflows_element\Service\WebformWorkflowsManager $workflows_manager */
    $workflows_manager = Drupal::service('webform_workflows_element.manager');
    $account = Drupal::currentUser();
    $access = $workflows_manager->checkAccessForSubmissionAndTransition($this->workflow, $account, $this->webform, $this->transition);
    if (!$access) {
      $form['actions']['submit']['#access'] = FALSE;
      $form['actions']['cancel']['#title'] = $this->t('Cancel');
      $form['description']['#markup'] = $this->t('Either the "@transition" transition is not possible or you do not have sufficient access to perform it on submission @id as part of the "@workflow" workflow.', [
        '@workflow' => $this->workflow->label(),
        '@transition' => $this->transition->label(),
        '@id' => $this->webform_submission->id(),
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submission = WebformSubmission::load($this->webform_submission->id());

    /** @var \Drupal\webform_workflows_element\Service\WebformWorkflowsManager $workflows_manager */
    $workflows_manager = Drupal::service('webform_workflows_element.manager');
    foreach ($workflows_manager->getWorkflowElementsForWebform($this->webform) as $element_id => $element) {
      if ($element['#workflow'] === $this->workflow->id()) {
        $data = $this->webform_submission->getElementData($element_id);
        $data['transition'] = $this->transition->id();
        $this->webform_submission->setElementData($element_id, $data);
        $workflows_manager->runTransitionOnElementValue($this->webform_submission, $element_id);
      }
    }

    $submission->save();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "webform_workflow_transition_confirm_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.webform.results_submissions', [
      'webform' => $this->webform->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Perform "@transition" on submission @id?', [
      '@transition' => $this->transition->label(),
      '@id' => $this->webform_submission->id(),
    ]);
  }

}
