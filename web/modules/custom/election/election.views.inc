<?php

/**
 * @file
 */

/**
 * Implements hook_views_data_alter().
 */
function election_views_data_alter(array &$data) {
  $data['election_post']['eligibility_for_post'] = [
    'title' => t("Current user's eligibility for election post"),
    'help' => t('Optionally show actions based on eligibility.'),
    'field' => [
      'id' => 'eligibility_for_post',
    ],
  ];
  // $data['election_post']['election_post_current_candidates'] = [
  //   'title' => t('Election post current candidates'),
  //   'help' => t(''),
  //   'field' => [
  //     'id' => 'election_post_current_candidates',
  //     'click sortable' => TRUE,
  //   ],
  // ];
  $data['election_post']['election_post_actions'] = [
    'title' => t("Actions relating to the post for the current user"),
    'help' => t('Optionally show actions based on phase.'),
    'field' => [
      'id' => 'election_post_actions',
    ],
  ];

  // Count.
  $data['election_post']['election_post_has_count'] = [
    'title' => t("Post has had count run and has result"),
    'help' => t(''),
    'field' => [
      'id' => 'election_post_has_count',
    ],
  ];
  $data['election_post_field_data']['election_post_has_count_filter'] = [
    'title' => t('Post has had count run and has result'),
    'group' => t('Elections'),
    'filter' => [
      'title' => t('Post has had count run and has result'),
      'help' => t(''),
      'field' => 'count_timestamp',
      'id' => 'election_post_has_count_filter',
      'type' => 'yes-no',
      'accept_null' => TRUE,
    ],
  ];
}
