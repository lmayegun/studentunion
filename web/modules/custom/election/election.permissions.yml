# Viewing

# View access for posts can be dependent on election publishing

view published election posts for published elections:
  title: "View published election posts for published elections"

view published election posts for unpublished elections:
  title: "View published election posts for unpublished elections"

view unpublished election post entities:
  title: "View unpublished election posts"

# View access for candidates can be dependent on post publishing

view published election candidates for unpublished posts:
  title: "View published election candidates for unpublished posts"

view published election candidates for published posts:
  title: "View published election candidates for published posts"
  description: "This makes them visible in all cases. To make visible only when voting is opened, do not provide this permission and provide the one below."

view published election candidate entities when voting open:
  title: "View published election candidates for published posts only when voting open"
  description: "'View published election candidates for published posts' would override this - make sure it is unchecked if using this permission."

view unpublished election candidate entities:
  title: "View any unpublished election candidates"

# Expressing interest

express interest in posts:
  title: "Express interest in posts (subject to eligibility)"

# Nominating / creating candidates

nominate for posts:
  title: "Nominate for posts (subject to eligibility)"

edit election candidate entities:
  title: "Edit election candidates (not subject to eligibility or being owner)"

add election candidate entities without eligibility:
  title: "Create new election candidates without being eligible (e.g. for admin)"

withdraw own nomination:
  title: "Withdraw own nominations"
  description: "(if the election or post settings allow it)"

# Voting

vote:
  title: "Vote for posts they are eligible to vote for"
  description: "i.e. create ballots and votes for those positions"

# Counting

run election counts:
  title: "Run election post counts"

view published count results:
  title: "View published count results"

view unpublished count results:
  title: "View unpublished count results"

edit count results:
  title: "Edit fields related to the result of a count"

# Election management

view published elections:
  title: "View published elections"

view unpublished elections:
  title: "View unpublished elections"

edit elections:
  title: "Edit elections"

delete elections:
  title: "Delete elections"

add elections:
  title: "Create new elections"

administer election:
  title: "Administer elections"
  description: "Allow to access the administration form to configure elections."
  restrict access: true

bypass running election lock:
  title: Bypass running election lock
  description: This permission allows the user to delete elections, edit posts, or view ballot results, even while voting is open (if they have the other relevant permissions).
  restrict access: true

view all election revisions:
  title: "View all Election revisions"

revert all election revisions:
  title: "Revert all Election revisions"
  description: "Role requires permission <em>view Election revisions</em> and <em>edit rights</em> for elections in question or <em>administer elections</em>."

delete all election revisions:
  title: "Delete all revisions"
  description: "Role requires permission to <em>view Election revisions</em> and <em>delete rights</em> for elections in question or <em>administer elections</em>."

# Posts management
# We do not have an independent permission for editing or deleting posts - if you can edit the election, you can edit its posts

add election posts:
  title: "Create new election posts"

delete posts without ballots:
  title: "Delete election posts without votes already cast for them"

delete posts with ballots:
  title: "Delete election posts even if votes already cast for them"
  restrict access: true

administer election posts:
  title: "Administer election posts"
  description: "Allow to access the administration form to configure election posts."
  restrict access: true

clone election posts:
  title: "Clone election posts"
  description: "Can include cloning ballots and votes."

view all election post revisions:
  title: "View all Election post revisions"

revert all election post revisions:
  title: "Revert all Election post revisions"
  description: "Role requires permission <em>view Election post revisions</em> and <em>edit rights</em> for election post entities in question or <em>administer election posts</em>."

delete all election post revisions:
  title: "Delete all revisions"
  description: "Role requires permission to <em>view Election post revisions</em> and <em>delete rights</em> for election post entities in question or <em>administer election posts</em>."

# Candidates management

delete candidates without votes:
  title: "Delete election candidates without votes already cast for them"

delete candidates with votes:
  title: "Delete candidates even if votes already cast for them"
  restrict access: true

administer election candidates:
  title: "Administer election candidates"
  description: "Allow to access the administration form to configure election candidates."
  restrict access: true

view own unpublished election candidates:
  title: "View own unpublished election candidates"

view all election candidate revisions:
  title: "View all Election candidate revisions"

revert all election candidate revisions:
  title: "Revert all Election candidate revisions"
  description: "Role requires permission <em>view Election candidate revisions</em> and <em>edit rights</em> for election candidate entities in question or <em>administer election candidate entities</em>."

delete all election candidate revisions:
  title: "Delete all revisions"
  description: "Role requires permission to <em>view Election candidate revisions</em> and <em>delete rights</em> for election candidate entities in question or <em>administer election candidate entities</em>."

# Ballots management

delete election ballots:
  title: "Delete election ballots"

# Callbacks

permission_callbacks:
  - \Drupal\election\Access\ElectionPermissions::generatePermissions
  - \Drupal\election\Access\ElectionPostPermissions::generatePermissions
  - \Drupal\election\Access\ElectionCandidatePermissions::generatePermissions
