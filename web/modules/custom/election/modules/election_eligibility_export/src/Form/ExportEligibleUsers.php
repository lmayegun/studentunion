<?php

namespace Drupal\election_eligibility_export\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionBallot;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionType;
use Drupal\user\Entity\User;

/**
 * Class ExportEligibleUsers.
 */
class ExportEligibleUsers extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_eligibility_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    if ($form_state->get('election_id')) {
      $election = Election::load($form_state->get('election_id'));
      $name = $election->label() . ' (' . $election->id() . ')';
    }
    $date = \Drupal::service('date.formatter')->format(strtotime('now'), 'short');
    return "Eligible users export " . ($name ? ' - ' . $name : '') . ' - ' . $date;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {

    $form = parent::buildForm($form, $form_state);

    $form_state->set('election_id', $election->id());

    if (!$election) {
      return;
    }

    $phases = [];
    foreach ($election->getEnabledPhases() as $phase_id => $phase) {
      $phases[$phase_id] = $phase->label();
    }

    $form['phase'] = [
      '#title' => 'Phase to test eligibility for',
      '#description' => 'Note this will ignore the phase status (open, closed, scheduled).',
      '#type' => 'select',
      '#options' => $phases,
      '#default_value' => $_SESSION[$this->getFormId() . '-phase'] ?? [],
    ];

    $form['category'] = [
      '#title' => 'Category to filter by',
      '#type' => 'select',
      '#options' => [
        'eligible' => 'Eligible users',
        'ineligible' => 'Ineligible users',
        'already' => 'Users who have already completed the phase (e.g. already voted)',
        'not_already' => 'Users who have NOT already completed the phase (e.g. have not already voted)',
      ],
      '#description' => t('Note the already completed / NOT already completed will ignore eligibility, and only filter by whether they\'ve done the action or not. However it will be quicker to run.'),
      '#default_value' => $_SESSION[$this->getFormId() . '-category'] ?? [],
    ];

    $post_types = [];
    $election_type = ElectionType::load($election->bundle());
    $types = $election_type->getAllowedPostTypes();
    foreach ($types as $type) {
      $post_types[$type->id()] = $type->label();
    }
    $form['post_type_ids'] = [
      '#title' => 'Position types to include for eligibility checking',
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $post_types,
      '#description' => t('Select none to include all post types.'),
      '#default_value' => $_SESSION[$this->getFormId() . '-post_types'] ?? [],
      '#states' => [
        'visible' => [
          [':input[name="category"]' => ['value' => 'eligible']],
          'or',
          [':input[name="category"]' => ['value' => 'ineligible']],
        ],
      ],
    ];

    $form['roles'] = [
      '#title' => 'Include users with these roles:',
      '#description' => 'The more roles / users in roles, the slower the export. Be as specific as possible.',
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#default_value' => $_SESSION[$this->getFormId() . '-roles'] ?? [],
    ];

    // Group module can also be used to filter for this.
    if (\Drupal::moduleHandler()->moduleExists('group')) {
      $this->alterFormGroup($form, $form_state);
    }

    // Actions:
    $form['actions'] = [
      '#type' => 'actions',
      '#tree' => TRUE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Download eligible users',
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $_SESSION[$this->getFormId() . '-phase'] = $form_state->getValue('phase');
    $_SESSION[$this->getFormId() . '-roles'] = $form_state->getValue('roles');
    $_SESSION[$this->getFormId() . '-category'] = $form_state->getValue('category');
    $_SESSION[$this->getFormId() . '-post_types'] = $form_state->getValue('post_types');

    $uids = $this->getUsers($form, $form_state);

    $phase = $form_state->getValue('phase');
    $phases = Election::getPhases([$phase]);

    $function = 'processBatch';
    if ($form_state->getValue('category') == 'eligible' || $form_state->getValue('category') == 'ineligible') {
      $chunk_size = 50;
    }
    else {
      $chunk_size = 250;
    }

    // Chunk the array.
    $chunks = array_chunk($uids, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, $function], [
        [
          'users' => $chunk,
          'election_id' => $form_state->get('election_id'),
          'phases' => $phases,
          'post_type_ids' => count($form_state->getValue('post_type_ids')) > 0 ? $form_state->getValue('post_type_ids') : [],
          'category' => $form_state->getValue('category'),
        ],
      ]);
    }
  }

  /**
   *
   */
  public function getUsers($form, $form_state) {
    $query = \Drupal::entityQuery('user')
      ->condition('status', 1);

    // @todo get from form state
    $roles = $form_state->getValue('roles');
    if ($roles) {
      $query->condition('roles', $roles, 'IN');
    }
    $uids = $query->execute();

    if (\Drupal::moduleHandler()->moduleExists('group')) {
      $_SESSION[$this->getFormId() . '-group_roles'] = $form_state->getValue('group_roles');

      if ($form_state->getValue('group_roles')) {
        $group_matching_uids = [];

        $connection = \Drupal::database();
        // Load memberships for this group:
        $query = $connection->select('group_content_field_data', 'gc');
        // Restrict by role:
        $query->join('group_content__group_roles', 'r', 'r.entity_id = gc.id');
        $query->condition('r.group_roles_target_id', $form_state->getValue('group_roles'), 'IN');
        // Get user IDs (entity_id)
        $query->fields('gc', ['entity_id']);
        $group_matching_uids = $query->distinct()->execute()->fetchCol();
        array_filter($group_matching_uids);

        $uids = array_intersect($uids, $group_matching_uids);
      }
    }

    if ($form_state->getValue('phase') == 'voting' && ($form_state->getValue('category') == 'already' || $form_state->getValue('category') == 'not_already')) {
      $election = Election::load($form_state->get('election_id'));
      $already = $election->getVoterUserIds();
      if ($form_state->getValue('category') == 'already') {
        return array_intersect($uids, $already);
      }
      else {
        return array_diff($uids, $already);
      }
    }

    return $uids;
  }

  /**
   *
   */
  public function generateLine($account, $election, array $phases, $category = NULL, $post_type_ids = NULL) {
    $line = [
      'User ID' => $account->id(),
      'User name' => $account->getDisplayName(),
      'User e-mail' => $account->getEmail(),
    ];
    $include = FALSE;
    foreach ($phases as $phase_id => $phase) {
      $phaseName = $phase->getAction()->__toString();

      if ($category == 'already' || $category == 'not_already') {
        $ballots = ElectionBallot::loadIdsByUserAndElection($account, $election);
        if ($category == 'already' && count($ballots) > 0) {
          $line[$phaseName] = 'Already completed';
          $include = TRUE;
        }
        if ($category == 'not_already' && count($ballots) == 0) {
          $line[$phaseName] = 'Not already completed';
          $include = TRUE;
        }
      }
      else {
        $eligible = $election->getNextPostId($account, NULL, NULL, $phase, FALSE, $post_type_ids) ? TRUE : FALSE;
        if ($eligible) {
          $line[$phaseName] = 'Eligible';
          $include = !$category || $category == 'eligible';
        }
        else {
          $line[$phaseName] = 'Not eligible';
          $include = $category == 'ineligible';
        }
      }
    }
    $line['include'] = $include;
    return $line;
  }

  /**
   *
   */
  public function processBatch(array $data, array &$context) {
    $users = $data['users'];
    $election = Election::load($data['election_id']);
    $phases = $data['phases'];
    $post_type_ids = $data['post_type_ids'];
    $category = $data['category'];
    foreach ($users as $user) {
      $user = User::load($user);
      $line = $this->generateLine($user, $election, $phases, $category, $post_type_ids);
      if ($line && $line['include']) {
        unset($line['include']);
        $this->addLine($line, $context);
      }
    }
  }

  /**
   *
   */
  public function getGroupRoles($group_types = NULL) {
    if (\Drupal::moduleHandler()->moduleExists('group')) {
      $query = \Drupal::entityQuery('group_role');
      $query = $query->condition('internal', 0, '=');
      if ($group_types && count($group_types) > 0) {
        $query = $query->condition('group_type', $group_types, 'IN');
      }
      $ids = $query->execute();
      return \Drupal::entityTypeManager()->getStorage('group_role')->loadMultiple($ids);
    }
    return NULL;
  }

  /**
   *
   */
  public function alterFormGroup(&$form, $form_state) {
    $roles = $this->getGroupRoles();
    $optionsRoles = [];
    foreach ($roles as $role) {
      $optionsRoles[$role->id()] = $role->label();
    }

    $form['group_roles'] = [
      '#title' => 'Include users with any of these roles in groups:',
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $optionsRoles,
      '#default_value' => $_SESSION[$this->getFormId() . '-group_roles'] ?? [],
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];
  }

}
