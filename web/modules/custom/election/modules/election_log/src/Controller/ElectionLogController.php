<?php

namespace Drupal\election_log\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for webform submission log routes.
 *
 * Copied from: \Drupal\dblog\Controller\DbLogController.
 */
class ElectionLogController extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The webform request handler.
   *
   * @var \Drupal\webform\WebformRequestInterface
   */
  protected $requestHandler;

  /**
   * The webform submission log manager.
   *
   * @var \Drupal\election_log\ElectionLogManagerInterface
   */
  protected $logManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\election_log\Controller\ElectionLogController $instance */
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->requestHandler = $container->get('webform.request');
    $instance->logManager = $container->get('election_log.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Displays a listing of webform submission log messages.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   A user account.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function overview(ElectionInterface $election = NULL, ElectionPostInterface $election_post = NULL, EntityInterface $election_candidate = NULL, AccountInterface $account = NULL) {
    // Entities.
    // Header.
    $header = [];
    $header['lid'] = ['data' => $this->t('#'), 'field' => 'log.lid', 'sort' => 'desc'];

    if (empty($election)) {
      $header['election_id'] = ['data' => $this->t('Election'), 'field' => 'log.election_id', 'class' => [RESPONSIVE_PRIORITY_MEDIUM]];
    }

    if (empty($election_post)) {
      $header['election_post_id'] = ['data' => $this->t('Election post'), 'field' => 'log.election_post_id', 'class' => [RESPONSIVE_PRIORITY_MEDIUM]];
    }

    if (empty($election_candidate)) {
      $header['election_candidate_id'] = ['data' => $this->t('Election candidate'), 'field' => 'log.election_candidate_id', 'class' => [RESPONSIVE_PRIORITY_MEDIUM]];
    }

    $header['operation'] = ['data' => $this->t('Operation'), 'field' => 'log.operation', 'class' => [RESPONSIVE_PRIORITY_MEDIUM]];
    $header['message'] = ['data' => $this->t('Message'), 'field' => 'log.message', 'class' => [RESPONSIVE_PRIORITY_LOW]];
    $header['uid'] = ['data' => $this->t('User'), 'field' => 'user.name', 'class' => [RESPONSIVE_PRIORITY_LOW]];
    $header['timestamp'] = ['data' => $this->t('Date'), 'field' => 'log.timestamp', 'sort' => 'desc', 'class' => [RESPONSIVE_PRIORITY_LOW]];

    // Query.
    $options = ['header' => $header, 'limit' => 50];
    $logs = $this->logManager->loadByEntities($election, $election_post, $election_candidate, $account, $options);

    // Rows.
    $rows = [];
    foreach ($logs as $log) {
      $row = [];
      $row['lid'] = $log->lid;
      if (empty($election)) {
        $election_for_row = Election::load($log->election_id);
        $row['election_id'] = $election_for_row->toLink($election_for_row->label(), 'canonical');
      }

      if (empty($election_post)) {
        if ($log->election_post_id && $election_post_for_row = ElectionPost::load($log->election_post_id)) {
          $row['election_post_id'] = $election_post_for_row->toLink($election_post_for_row->label(), 'canonical');
        }
        elseif (!$log->election_post_id) {
          $row['election_post_id'] = 'n/a';
        }
        else {
          $row['election_post_id'] = t('Unknown (ID @id)', [
            '@id' => $log->election_post_id,
          ]);
        }
      }
      if (empty($election_candidate)) {
        if ($log->election_candidate_id && $election_candidate_for_row = ElectionCandidate::load($log->election_candidate_id)) {
          $row['election_candidate_id'] = $election_candidate_for_row->toLink($election_candidate_for_row->label(), 'canonical');
        }
        elseif (!$log->election_candidate_id) {
          $row['election_candidate_id'] = 'n/a';
        }
        else {
          $row['election_candidate_id'] = t('Unknown (ID @id)', [
            '@id' => $log->election_candidate_id,
          ]);
        }
      }

      $row['operation'] = $log->operation;
      $row['message'] = [
        'data' => [
          '#markup' => $this->t($log->message, $log->variables),
        ],
      ];
      $row['uid'] = [
        'data' => [
          '#theme' => 'username',
          '#account' => User::load($log->uid),
        ],
      ];
      $row['timestamp'] = $this->dateFormatter->format($log->timestamp, 'short');

      $rows[] = $row;
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#sticky' => TRUE,
      '#empty' => $this->t('No log messages available.'),
    ];
    $build['pager'] = ['#type' => 'pager'];
    return $build;
  }

}
