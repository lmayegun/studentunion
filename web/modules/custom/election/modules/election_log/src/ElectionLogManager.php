<?php

namespace Drupal\election_log;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\election\Entity\ElectionCandidateInterface;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPostInterface;

/**
 * Election log manager.
 */
class ElectionLogManager implements ElectionLogManagerInterface {

  use DependencySerializationTrait;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * ElectionLogManager constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function insert(array $fields) {
    $fields += [
      'election_id' => '',
      'election_post_id' => '',
      'election_candidate_id' => '',
      'entity_type_target' => '',
      'operation' => '',
      'uid' => '',
      'message' => '',
      'variables' => serialize([]),
      'data' => serialize([]),
      'timestamp' => '',
    ];
    $this->database->insert(ElectionLogManagerInterface::TABLE)
      ->fields($fields)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery(EntityInterface $election = NULL, EntityInterface $election_post = NULL, EntityInterface $election_candidate = NULL, AccountInterface $account = NULL, array $options = []) {
    // Default options.
    $options += [
      'header' => NULL,
      'limit' => NULL,
    ];

    $query = $this->database->select(ElectionLogManagerInterface::TABLE, 'log');

    // Log fields.
    $query->fields('log', [
      'lid',
      'uid',
      'election_id',
      'election_post_id',
      'election_candidate_id',
      'entity_type_target',
      'operation',
      'message',
      'variables',
      'timestamp',
      'data',
    ]);

    // User fields.
    $query->leftJoin('users_field_data', 'user', 'log.uid = user.uid');

    // Load based on entity given.
    if ($election_candidate instanceof ElectionCandidateInterface) {
      $query->condition('log.election_candidate_id', $election_candidate->id());
    }
    elseif ($election_post instanceof ElectionPostInterface) {
      $query->condition('log.election_post_id', $election_post->id());
    }
    elseif ($election instanceof ElectionInterface) {
      $query->condition('log.election_id', $election->id());
    }

    // User account condition.
    if ($account) {
      $query->condition('log.uid', $account->id());
    }

    // Set header sorting.
    if ($options['header']) {
      $query = $query->extend('\Drupal\Core\Database\Query\TableSortExtender')
        ->orderByHeader($options['header']);
    }

    // Set limit pager.
    if ($options['limit']) {
      $query = $query->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit($options['limit']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByEntities(EntityInterface $election = NULL, EntityInterface $election_post = NULL, EntityInterface $election_candidate = NULL, AccountInterface $account = NULL, array $options = []) {
    $result = $this->getQuery($election, $election_post, $election_candidate, $account, $options)
      ->execute();
    $records = [];
    while ($record = $result->fetchObject()) {
      $record->variables = unserialize($record->variables);
      $record->data = unserialize($record->data);
      $records[] = $record;
    }
    return $records;
  }

}
