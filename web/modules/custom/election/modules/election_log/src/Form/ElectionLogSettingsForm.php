<?php

namespace Drupal\election_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ElectionSettingsForm.
 *
 * @ingroup election
 */
class ElectionLogSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'election_log.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'election_log_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('delete_after', $form_state->getValue('delete_after'))
      ->set('log_access', $form_state->getValue('log_access'))
      ->set('log_ballots', $form_state->getValue('log_ballots'))
      ->set('log_forms', $form_state->getValue('log_forms'))
      ->set('log_counts', $form_state->getValue('log_counts'))
      ->set('log_delete', $form_state->getValue('log_delete'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Election entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['delete_after'] = [
      '#type' => 'number',
      '#title' => $this->t('Delete logs after this many days'),
      '#description' => $this->t('Enter 0 to never delete logs automatically.'),
      '#default_value' => $config->get('delete_after') ?? 0,
    ];

    $form['log_forms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log all election candidate and post form submissions (create nomination, edit)'),
      '#default_value' => $config->get('log_actions') ?? 1,
    ];

    $form['log_counts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log counts'),
      '#default_value' => $config->get('log_counts') ?? 1,
    ];

    $form['log_ballots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log all submissions of ballot forms (not including any details of the votes themselves)'),
      '#default_value' => $config->get('log_ballots') ?? 1,
    ];

    $form['log_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log all deletions'),
      '#default_value' => $config->get('log_delete') ?? 1,
    ];

    return parent::buildForm($form, $form_state);
  }

}
