<?php

namespace Drupal\election_log;

use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;

/**
 * Logger that listens for 'election' channel.
 */
class ElectionLogLogger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * The election log manager.
   *
   * @var \Drupal\election_log\ElectionLogManagerInterface
   */
  protected $logManager;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * ElectionLog constructor.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The log message parser service.
   * @param \Drupal\election_log\ElectionLogManagerInterface $log_manager
   *   The election log manager.
   */
  public function __construct(LogMessageParserInterface $parser, ElectionLogManagerInterface $log_manager) {
    $this->parser = $parser;
    $this->logManager = $log_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    // Only log the 'election' channel.
    if ($context['channel'] !== 'election') {
      return;
    }

    // Make sure the context contains a election.
    $election_entity_types = [
      'election_candidate',
      'election_post',
      'election',
    ];

    $entity = NULL;
    $entity_type = NULL;
    foreach ($election_entity_types as $election_entity_type) {
      if (isset($context[$election_entity_type])) {
        $entity_type = $election_entity_type;
        $entity = $context[$entity_type];
        break;
      }
    }

    if (!$entity || !$entity_type) {
      return;
    }

    // Set default values.
    $context += [
      'operation' => '',
      'data' => [],
    ];

    // Cast message to string.
    $message = (string) $message;
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);

    $data = [
      'operation' => $context['operation'],
      'uid' => $context['uid'],
      'entity_type_target' => $entity_type,
      'message' => $message,
      'variables' => serialize($message_placeholders),
      'data' => serialize($context['data']),
      'timestamp' => $context['timestamp'],
    ];

    $data['election_post_id'] = NULL;
    $data['election_candidate_id'] = NULL;

    $data[$entity_type . '_id'] = $entity->id();

    if ($entity_type == 'election_candidate') {
      $data[$entity_type . '_id'] = $entity->id();
      $data['election_post_id'] = $entity->getElectionPost() ? $entity->getElectionPost()->id() : NULL;
      $data['election_id'] = $entity->getElectionPost() && $entity->getElectionPost()->getElection() ? $entity->getElectionPost()->getElection()->id() : 0;
    }
    elseif ($entity_type == 'election_post') {
      $data[$entity_type . '_id'] = $entity->id();
      $data['election_id'] = $entity->getElection() ? $entity->getElection()->id() : 0;
    }
    else {
      $data[$entity_type . '_id'] = $entity->id();
    }
    $this->logManager->insert($data);
  }

}
