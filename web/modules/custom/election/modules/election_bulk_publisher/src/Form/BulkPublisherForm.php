<?php

namespace Drupal\election_bulk_publisher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;

/**
 * Implement Class ElectionPostImportForm for import form.
 */
class BulkPublisherForm extends BatchImportCSVForm {

  /**
   * @var null
   */
  public $election = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'election_bulk_publisher_form';
  }

  /**
   *
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['Post ID'] = [
      'description' => 'Post name or ID',
    ];
    $fields['Candidate ID'] = [
      'description' => 'Candidate name or ID',
    ];
    $fields['Published'] = [
      'description' => 'Whether the post or candidate should be published or not: Yes or No',
      'required' => TRUE,
    ];
    $fields['Status'] = [
      'description' => 'Optionally set the status of a candidate to one of: Hopeful, Withdrawn, Rejected',
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {
    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<p>To publish/unpublish posts or candidates, upload a file with the following columns.<ul>' .
        static::getMappingAsList() .
        '</ul></p>'
        . '<p>* required column</p>',
    ];
    $form_state->set('election_id', $election ? $election->id() : NULL);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $election = Election::load($row['election_id']);

    $required = static::getRequiredFields();
    foreach ($required as $field => $data) {
      if (!isset($row[$field])) {
        static::addError($row, $context, 'Required field not provided: ' . $field);
        return FALSE;
      }
    }

    if (!isset($row['Post ID']) && !isset($row['Candidate ID'])) {
      static::addError($row, $context, 'Need to provide a post or candidate ID.');
      return FALSE;
    }

    if (isset($row['Post ID']) && $entity = ElectionPost::load($row['Post ID'])) {
      // We have a post.
    }
    elseif (isset($row['Candidate ID']) && $entity = ElectionCandidate::load($row['Candidate ID'])) {
      // We have a candidate.
    }
    else {
      static::addError($row, $context, 'Could not find a post or candidate with the given ID.');
      return FALSE;
    }

    // Set published.
    if (isset($row['Published'])) {
      $entity->set('status', $row['Published'] == 'Yes');
    }

    // Set candidate status.
    if (isset($row['Status']) && $row['Status'] && $entity->getEntityTypeId() == 'election_candidate') {
      $status = strtolower($row['Status']);
      if (in_array($status, $entity->getPossibleStatuses())) {
        $entity->set('candidate_status', $status);
      }
      else {
        static::addError($row, $context, $status . ' does not seem to be a valid status.');
        return FALSE;
      }
    }

    $entity->save();

    return TRUE;
  }

}
