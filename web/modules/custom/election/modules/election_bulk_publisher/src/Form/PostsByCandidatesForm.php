<?php

namespace Drupal\election_bulk_publisher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchForm;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;

/**
 * Implement Class ElectionPostImportForm for import form.
 */
class PostsByCandidatesForm extends BatchForm {

  /**
   * @var null
   */
  public $election = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'election_bulk_unpublish_no_candidates';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {
    $form_state->set('election_id', $election ? $election->id() : NULL);
    $form = parent::buildForm($form, $form_state);

    $form['unpublish'] = [
      '#type' => 'checkbox',
      '#title' => 'Unpublish all posts without any hopeful and published candidates',
      '#weight' => -25,
    ];

    $form['publish'] = [
      '#type' => 'checkbox',
      '#title' => 'Publish all posts with hopeful and published candidates',
      '#weight' => -25,
    ];

    $form['publish_elected_defeated'] = [
      '#type' => 'checkbox',
      '#title' => 'Publish all posts with elected/defeated, published candidates',
      '#weight' => -25,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $election_id = $form_state->get('election_id');

    $data = \Drupal::entityQuery('election_post')
      ->condition('election', $election_id)
      ->execute();

    // Chunk the array.
    $chunk_size = 100;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processOperation'], [[
        'election_posts' => $chunk,
        'unpublish' => $form_state->getValue('unpublish'),
        'publish' => $form_state->getValue('publish'),
        'publish_elected_defeated' => $form_state->getValue('publish_elected_defeated'),
      ],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function processOperation(array $data, array &$context) {
    $election_posts = $data['election_posts'];
    $unpublishIfNoCandidates = $data['unpublish'];
    $publishIfCandidates = $data['publish'];
    $publishIfElectedDefeated = $data['publish_elected_defeated'];

    foreach ($election_posts as $election_post_id) {
      $election_post = ElectionPost::load($election_post_id);
      $count = $election_post->countCandidates(['hopeful'], TRUE);
      if ($count == 0 && $unpublishIfNoCandidates && $election_post->status->value == 1) {
        $election_post->status->value = 0;
        $election_post->save();
        $context['message'] = t('Unpublished @post_name with 0 candidates.', [
          '@post_name'  => $election_post->label(),
        ]);
      }

      if ($count > 0 && $publishIfCandidates && $election_post->status->value == 0) {
        $election_post->status->value = 1;
        $election_post->save();
        $context['message'] = t('Published @post_name with @count candidates.', [
          '@post_name'  => $election_post->label(),
          '@count' => $count,
        ]);
      }

      $countElectedDefeated = $election_post->countCandidates(['elected', 'defeated'], TRUE);
      if ($count > 0 && $publishIfElectedDefeated && $election_post->status->value == 0) {
        $election_post->status->value = 1;
        $election_post->save();
        $context['message'] = t('Published @post_name with @count elected/defeated candidates.', [
          '@post_name'  => $election_post->label(),
          '@count' => $countElectedDefeated,
        ]);
      }
    }
  }

}
