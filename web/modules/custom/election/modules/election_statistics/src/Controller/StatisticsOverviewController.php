<?php

namespace Drupal\election_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPostInterface;

/**
 * Return a page to choose a plugin, and then rendered statistics plugins.
 */
class StatisticsOverviewController extends ControllerBase {

  /**
   * Render list for election to select which plugin to load.
   * 
   * @see election_statistics.routing.yml
   */
  public function render(ElectionInterface $election) {
    $context_provider = \Drupal::service('election.election_route_context');

    if (!$election) {
      $election = NULL;
      $contexts = $context_provider->getRuntimeContexts(['election']);
      if (isset($contexts['election']) && $contexts['election']) {
        $election = $contexts['election']->getContextValue();
      }
    }

    $build = [];
    if ($election) {
      $build = $this->processPlugins($election);
    }

    return $build;
  }

  /**
   * Process plugins list for rendering list.
   */
  public function processPlugins(ElectionEntityInterface $entity) {
    $build = [];
    $links_list = [];

    $pluginManager = \Drupal::service('plugin.manager.election_statistics');
    $plugins = $pluginManager->getFilteredDefinitions();
    foreach ($plugins as $plugin_id => $definition) {
      $plugin = $pluginManager->createInstance($plugin_id, $definition);

      if ($plugin->isSensitive() && !\Drupal::currentUser()->hasPermission('view sensitive selection statistics')) {
        continue;
      }

      if ($plugin->isQuick()) {
        $plugin_build = $plugin->build($entity);
        $plugin_build['#prefix'] = '<h2>' . $plugin->getLabel() . '</h2>';
        $build[] = $plugin_build;
      } else {
        $links_list[$plugin_id] = $plugin->getLabel();
      }
    }

    if (count($links_list) > 0) {
      $list = [];

      $html = '<ul>';

      foreach ($links_list as $plugin_id => $label) {
        if (!$plugin_id || !$label) {
          continue;
        }
        $url = Url::fromRoute(
          'election_statistics.' . $entity->getEntityTypeId() . '_plugin',
          [
            'election' => $entity->getElection()->id(),
            'election_post' => $entity->getEntityTypeId() == 'election_post' ? $entity->id() : NULL,
            'plugin_id' => $plugin_id,
          ],
        );
        $html .= '<li><a href="' . $url->toString() . '">' . $label . '</a></li>';
      }

      $html .= '</ul>';

      $build[] = [
        '#markup' => $html,
        '#prefix' => t('<h2>Available statistics</h2>'),
      ];
    }
    return $build;
  }

  /**
   * Render statistics intro page for post.
   * 
   * @see election_statistics.routing.yml
   */
  public function renderPost(ElectionPostInterface $election_post) {
    $context_provider = \Drupal::service('election.election_route_context');

    if (!$election_post) {
      $election_post = NULL;
      $contexts = $context_provider->getRuntimeContexts(['election_post']);
      if (isset($contexts['election_post']) && $contexts['election_post']) {
        $election_post = $contexts['election_post']->getContextValue();
      }
    }

    $build = [];
    if ($election_post) {
      $build = $this->processPlugins($election_post);
    }
    return $build;
  }

  /**
   * Render a single plugin.
   */
  public function renderForPlugin(ElectionEntityInterface $entity, string $plugin_id) {
    $build = [];
    $pluginManager = \Drupal::service('plugin.manager.election_statistics');

    $definitions = $pluginManager->getDefinitions();

    if (!isset($definitions[$plugin_id])) {
      return [];
    }

    $plugin = $pluginManager->createInstance($plugin_id, $definitions[$plugin_id]);

    if (method_exists($plugin, 'setRecordsToLoad')) {
      $plugin->setRecordsToLoad(-1);
    }

    $build = $plugin->build($entity);
    return $build;
  }

  /**
   * Render a single plugin for an election.
   * 
   * @see election_statistics.routing.yml
   */
  public function renderForPluginElection(ElectionInterface $election, string $plugin_id) {
    return $this->renderForPlugin($election, $plugin_id);
  }

  /**
   * Render a single plugin for an election post.
   * 
   * @see election_statistics.routing.yml
   */
  public function renderForPluginPost(ElectionInterface $election, ElectionPostInterface $election_post, string $plugin_id) {
    return $this->renderForPlugin($election_post, $plugin_id);
  }

  /**
   * Get a title for rendering a single plugin for a post.
   * 
   * @see election_statistics.routing.yml
   */
  public function renderForPluginTitlePost(ElectionInterface $election, ElectionPostInterface $election_post, string $plugin_id) {
    return $this->renderForPluginTitle($election_post, $plugin_id);
  }

  /**
   * Get a title for rendering a single plugin for an election.
   * 
   * @see election_statistics.routing.yml
   */
  public function renderForPluginTitleElection(ElectionInterface $election, string $plugin_id) {
    return $this->renderForPluginTitle($election, $plugin_id);
  }

  /**
   * Get a title for rendering a single plugin.
   */
  public function renderForPluginTitle(ElectionEntityInterface $entity, string $plugin_id) {
    $pluginManager = \Drupal::service('plugin.manager.election_statistics');

    $definitions = $pluginManager->getDefinitions();

    if (!isset($definitions[$plugin_id])) {
      return [];
    }

    return $definitions[$plugin_id]['label'];
  }
  
}
