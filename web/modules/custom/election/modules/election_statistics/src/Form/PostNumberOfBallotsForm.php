<?php

namespace Drupal\election_statistics\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionType;

/**
 * Class PostNumberOfBallotsForm.
 */
class PostNumberOfBallotsForm extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_ballot_counts';
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    if ($form_state->get('election_id')) {
      $election = Election::load($form_state->get('election_id'));
      $name = $election->label() . ' (' . $election->id() . ')';
    }
    $date = \Drupal::service('date.formatter')->format(strtotime('now'), 'short');
    return "Post ballot counts " . ($name ? ' - ' . $name : '') . ' - ' . $date;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {

    $form = parent::buildForm($form, $form_state);

    $form_state->set('election_id', $election->id());

    if (!$election) {
      return;
    }

    $post_types = [];
    $election_type = ElectionType::load($election->bundle());
    $types = $election_type->getAllowedPostTypes();
    foreach ($types as $type) {
      $post_types[$type->id()] = $type->label();
    }
    $form['post_type_ids'] = [
      '#title' => 'Position types to include',
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $post_types,
      '#description' => t('Select none to include all post types.'),
      '#default_value' => $_SESSION[$this->getFormId() . '-post_types'] ?? array_keys($post_types),
    ];

    // Actions:
    $form['actions'] = [
      '#type' => 'actions',
      '#tree' => TRUE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Download number of ballots export',
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $_SESSION[$this->getFormId() . '-post_types'] = $form_state->getValue('post_types');

    $election = Election::load($form_state->get('election_id'));
    $post_type_ids = count($form_state->getValue('post_type_ids')) > 0 ? $form_state->getValue('post_type_ids') : [];
    $post_ids = $election->getPostIds(TRUE, NULL, $post_type_ids);

    $function = 'processBatch';
    $chunk_size = 250;

    // Chunk the array.
    $chunks = array_chunk($post_ids, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, $function], [
        [
          'post_ids' => $chunk,
          'election_id' => $form_state->get('election_id'),
          'post_type_ids' => $post_type_ids,
        ],
      ]);
    }
  }

  /**
   *
   */
  public function processBatch(array $data, array &$context) {
    $post_ids = $data['post_ids'];

    foreach ($post_ids as $post_id) {
      $election_post = ElectionPost::load($post_id);
      $line = $this->generateLine($election_post);
      if ($line) {
        $this->addLine($line, $context);
      }
    }
  }

  /**
   *
   */
  public function generateLine($election_post) {
    $uri = Url::fromRoute(
      'entity.election_post.canonical',
      [
        'election_post' => $election_post->id(),
      ],
      [
        'absolute' => TRUE,
      ],
    )->toString();
    $line = [
      'Post ID' => $election_post->id(),
      'Post name' => $election_post->label(),
      'Post URL' => $uri,
      'Count' => $election_post->countBallots(TRUE),
    ];
    return $line;
  }

}
