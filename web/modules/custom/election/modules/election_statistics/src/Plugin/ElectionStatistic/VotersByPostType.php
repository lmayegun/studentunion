<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionBallot;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election\Entity\ElectionPostType;
use Drupal\election_statistics\Plugin\ElectionStatistic\ElectionStatisticBase;
use Drupal\election_statistics\Plugin\ElectionStatistic\ElectionStatisticInterface;

/**
 * @ElectionStatistic(
 *  id = "post_type",
 *  label = @Translation("Voters by post types voted for"),
 *  weight = -50,
 * )
 */
class VotersByPostType extends ElectionStatisticBase implements ElectionStatisticInterface {

  /**
   * Get value to group by
   *
   * @param ElectionPostInterface $election_post
   *
   * @return string
   */
  public function getValueFromPost(ElectionPostInterface $election_post) {
    $post_type = ElectionPostType::load($election_post->bundle());
    return $post_type->label();
  }

  public function getGroupedIntroText() {
    return t('<p>This shows the combinations of post types voted for by users. e.g. if one user voted for posts across three different post types, they will be counted in the "Post type A + Post type B + Post type C" row. If they only voted for posts with Post type B, they will be counted in the "Post type B" row.</p>');
  }

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    $results = [];

    $voter_values = [];
    $overall_values = [];

    // Loop through posts and get categories
    $post_ids = $entity->getEntityTypeId() == 'election_post' ? [$entity->id()] : $entity->getPostIds(FALSE);
    foreach ($post_ids as $post_id) {
      $post = ElectionPost::load($post_id);
      $value = $this->getValueFromPost($post);

      // Number of voters who voted for this category overall

      // Voters who voted for combinations
      $post_voters = $post->getVoterUserIds();
      foreach ($post_voters as $uid) {
        if (!isset($voter_values[$uid])) {
          $voter_values[$uid] = [];
        }

        if (!in_array($value, $voter_values[$uid])) {
          $voter_values[$uid][] = $value;
        }

        if (!isset($overall_values[$value])) {
          $overall_values[$value] = [];
        }

        if (!in_array($uid, $overall_values[$value])) {
          $overall_values[$value][] = $uid;
        }
      }
    }

    foreach ($overall_values as $value => $uids) {
      $overall_values[$value] = count($uids);
    }

    // Loop through voters
    foreach ($voter_values as $uid => $values) {
      asort($values);

      // Increment a value of the combination of categories they voted for
      $value_name = '"' . implode('" + "', $values) . '"';
      if (!isset($grouped[$value_name])) {
        $grouped[$value_name] = 0;
      }
      $grouped[$value_name]++;
    }

    ksort($overall_values);
    ksort($grouped);

    return [
      'grouped' => $grouped,
      'overall' => $overall_values,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(ElectionEntityInterface $entity) {
    $results = $this->get($entity);

    $total = $entity->countVoters();
    $grouped = $results['grouped'];
    $overall = $results['overall'];

    // Grouped
    $grouped_stats = [];
    foreach ($grouped as $values => $result) {
      $grouped_stats[] = [
        'values' => $values,
        'count' => $result,
        'proportion' => round(($result / $total) * 100, 2) . '%',
      ];
    }
    usort($grouped_stats, function ($a, $b) {
      return $b['count'] <=> $a['count'];
    });

    // Overall
    $overall_stats = [];
    foreach ($overall as $values => $result) {
      $overall_stats[] = [
        'values' => $values,
        'count' => $result,
        'proportion' => round(($result / $total) * 100, 2) . '%',
      ];
    }
    
    usort($overall_stats, function ($a, $b) {
      return $b['count'] <=> $a['count'];
    });

    return [
      'overall_heading' => [
        '#markup' => t('<h2>Overall</h2>'),
      ],
      'overall' => [
        '#type' => 'table',
        '#header' => [
          'values' => t('Voted for'),
          'count' => t('Voter count'),
          'proportion' => t('Out of total voters'),
        ],
        '#rows' => $overall_stats,
      ],
      'grouped_heading' => [
        '#markup' => t('<h2>Combinations</h2>'),
      ],
      'grouped' => [
        '#type' => 'table',
        '#header' => [
          'values' => t('Combinations user voted for'),
          'count' => t('Voter count'),
          'proportion' => t('Proportion of total'),
        ],
        '#rows' => $grouped_stats,
        '#prefix' => $this->getGroupedIntroText(),
      ],
    ];
  }
}
