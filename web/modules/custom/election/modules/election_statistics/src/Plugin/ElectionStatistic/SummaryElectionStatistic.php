<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionEntityInterface;

/**
 * @ElectionStatistic(
 *  id = "summary",
 *  label = @Translation("Quick statistics"),
 *  weight = -50,
 *  quick = TRUE,
 * )
 */
class SummaryElectionStatistic extends ElectionStatisticBase implements ElectionStatisticInterface {

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    $ballots = 0;
    $uids = [];
    $positions = 0;
    $positionsWithVotes = 0;
    $candidates = 0;

    if ($entity->getEntityTypeId() == 'election') {
      $election_post_ids = $entity->getPostIds(FALSE);
    }
    else {
      $election_post_ids = [$entity->id()];
    }

    $election = $entity->getElection();

    $results = [
      'positions' => count($election_post_ids),
      'candidates' => $election->countCandidatesForVoting($election_post_ids),
      'ballots' => $election->countBallots(TRUE),
      'voters' => $election->countVoters(),
    ];

    if ($entity->getEntityTypeId() == 'election_post') {
      unset($results['positions']);
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function build(ElectionEntityInterface $entity) {
    $stats = $this->get($entity);
    $header = [
      'positions' => t('@positions', ['@positions' => $entity->getPostTypesAsLabel()]),
      t('@candidates*', ['@candidates' => 'Candidates']),
      t('Ballots'),
      t('Voters'),
    ];

    if ($entity->getEntityTypeId() == 'election_post') {
      unset($header['positions']);
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => [$stats],
      '#suffix' => '<p>* Note candidates includes only published and hopeful candidates.</p>',
    ];
  }

}
