<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

/**
 * @ElectionStatistic(
 *  id = "ip_addresses",
 *  label = @Translation("Top IP addresses"),
 *  sensitive = TRUE,
 * )
 */
class TopIpAddressesElectionStatistic extends ElectionStatisticBase {
}
