<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\charts\Plugin\chart\Library\ChartBase;
use Drupal\election\Entity\ElectionEntityInterface;

/**
 *
 */
abstract class OverTimeBase extends ElectionStatisticBase {

  /**
   *
   */
  public function getCountingLabel() {
    return t('Votes');
  }

  /**
   *
   */
  public function getTitle() {
    return t('Votes over time');
  }

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    $result = [];
    // See generateFromDatabase as called in VotesOverTime for an example generate.
  }

  /**
   * Table must have created field and election_post field.
   */
  public function generateFromDatabase(ElectionEntityInterface $entity, $table, $valuesLabel, $usersLabel) {
    $result = [];
    $count_values = [$valuesLabel, $usersLabel];
    foreach ($count_values as $key) {
      $query = \Drupal::database()->select($table, 'ed');
      $query->addExpression("date_format( FROM_UNIXTIME(ed.created), '%Y-%m-%d %H' )", "hour");
      if ($entity->getEntityTypeId() == 'election') {
        $query->join('election_post_field_data', 'ep', 'ep.id = ed.election_post');
        $query->condition('ep.election', $entity->id());
      }
      else {
        $query->condition($entity->getEntityTypeId(), $entity->id());
      }
      $query->orderBy('hour', 'ASC');

      if ($key == $valuesLabel) {
        $query->addExpression('count(*)', "total");
        $query->groupBy('hour');
        $datesInUseResult = $query->execute()->fetchAllAssoc('hour');
        $result[$key] = $this->fillDates($datesInUseResult);
      }
      else {
        $query->addExpression('ed.user_id', "user_id");
        $datesPerUserId = $query->execute()->fetchAll();
        if (!$datesPerUserId) {
          $result[$key] = [];
          continue;
        }
        $countedUserIds = [];
        $countsPerHour = [];
        foreach ($datesPerUserId as $data) {
          $data = (array) $data;
          $user_id = $data['user_id'];

          if (in_array($user_id, $countedUserIds)) {
            continue;
          }

          $hour = $data['hour'];
          if (!isset($countsPerHour[$hour])) {
            $countsPerHour[$hour] = [
              'hour' => $hour,
              'total' => 0,
            ];
          }
          $countsPerHour[$hour]['total']++;
          $countedUserIds[] = $user_id;
        }
        $filledCountsPerHour = $this->fillDates($countsPerHour, TRUE, $key);
        $result[$key] = $filledCountsPerHour;
      }
    }
    return $result;
  }

  /**
   * Add in any empty hours to the dataset so it is complete.
   *
   * @param mixed $datesInUseResult
   *
   * @return [type]
   */
  public function fillDates($datesInUseResult, $sum = TRUE, $debug = '') {
    $datesInUse = array_column($datesInUseResult, 'hour');

    $result = [];
    $last = strtotime($datesInUse[count($datesInUse) - 1] . ':00');
    $first = strtotime(reset($datesInUse) . ':00');

    $total = 0;
    $current = $first;
    $stepVal = '+1 hour';
    while ($current <= $last) {
      $timeperiod = date('Y-m-d H', $current);
      $formatted = date('d/m/Y H:i', $current);
      $result[$formatted] = $sum ? $total : 0;
      if (isset($datesInUseResult[$timeperiod])) {
        $data = (array) $datesInUseResult[$timeperiod];
        $result[$formatted] += $data['total'];
        $total = $result[$formatted];
      }
      $current = strtotime($stepVal, $current);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function build(ElectionEntityInterface $entity) {
    $stats = $this->get($entity);

    if (\Drupal::moduleHandler()->moduleExists('charts')) {
      return $this->buildChart($entity, $stats);
    }
    else {
      return [
        '#type' => 'table',
        '#header' => [
          t('Time'),
          t('Votes'),
        ],
        '#rows' => $stats,
      ];
    }
  }

  /**
   *
   */
  public function buildChart(ElectionEntityInterface $entity, $stats) {
    $xaxis = [
      '#type' => 'chart_xaxis',
      '#title' => t('Time'),
    ];

    $yaxis = [
      '#type' => 'chart_yaxis',
      '#title' => $this->getCountingLabel(),
    ];

    $chart = [
      '#type' => 'chart',
      '#chart_type' => 'spline',
      '#title' => $this->getTitle(),
      '#spline' => TRUE,
      '#polar' => FALSE,
      '#tooltips' => [],

      'x_axis' => $xaxis,
      'y_axis' => $yaxis,

      '#data_labels' => FALSE,

      '#raw_options' => [
        'point' => [
          'show' => FALSE,
        ],
        'line' => [
          'point' => FALSE,
          'zerobased' => TRUE,
          'classes' => 'chart-thick-line',
        ],
        'axis' => [
          'x' => [
            'tick' => [
              'show' => FALSE,
              'count' => 15,
              'culling' => [
                'lines' => FALSE,
                'max' => 15,
              ],
            ],
          ],
        ],
      ],

      '#title_font_size' => 14,
      '#title_font_weight' => 'bold',

      '#legend' => TRUE,
      '#legend_position' => 'bottom',

      '#exporting_library' => FALSE,

      '#tooltips' => TRUE,
      '#tooltips_use_html' => TRUE,

      '#cache' => $this->getCacheTags($entity),
    ];

    $colours = ChartBase::getDefaultColors();
    $i = 0;

    foreach ($stats as $key => $data) {
      if (!isset($chart['x_axis']['#labels'])) {
        $chart['x_axis']['#labels'] = array_keys($data);
      }
      $chart[$key] = [
        '#type' => 'chart_data',
        '#title' => t($key),
        '#data' => $data,
        '#color' => $colours[$i],
        '#line_width' => 3,
      ];
      $i++;
    }

    return $chart;
  }

}
