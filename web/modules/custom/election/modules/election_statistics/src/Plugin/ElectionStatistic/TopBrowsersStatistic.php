<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionEntityInterface;

/**
 * @ElectionStatistic(
 *  id = "browsers",
 *  label = @Translation("Top browsers for ballots"),
 * )
 */
class TopBrowsersStatistic extends ElectionStatisticBase {

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    $result = [];

    $query = \Drupal::database()->select('election_ballot', 'eb')
      ->fields('eb', ['user_agent'])
      ->distinct();
    $query->addExpression('COUNT(*)', 'total');
    $query->condition($entity->getEntityTypeId(), $entity->id());
    $query->groupBy('user_agent');
    $user_agent_counts = $query->execute()->fetchAllAssoc('user_agent');

    $browsers = [];
    $total = 0;
    foreach ($user_agent_counts as $count) {
      // . ' ' . strtok($this->getBrowser($count->user_agent)['version'], '.');
      $browser = $this->getBrowser($count->user_agent)['name'];
      if (!isset($browsers[$browser])) {
        $browsers[$browser] = [
          'name' => $browser,
          'total' => 0,
          'proportion' => 0,
        ];
      }
      $browsers[$browser]['total'] += $count->total;
      $total += $count->total;
    }

    foreach ($browsers as $key => $browser) {
      $browsers[$key]['proportion'] = round(($browser['total'] / $total) * 100, 2);
    }

    usort($browsers, function ($first, $second) {
      return strcmp($second['proportion'], $first['proportion']);
    });

    return $browsers;
  }

  /**
   *
   */
  public function getBrowser($u_agent) {
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";
    $ub = '';

    // First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
      $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
      $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
      $platform = 'windows';
    }
    // Next get the name of the useragent yes seperately and for good reason.
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
      $bname = 'Internet Explorer';
      $ub = "MSIE";
    }
    elseif (preg_match('/Firefox/i', $u_agent)) {
      $bname = 'Mozilla Firefox';
      $ub = "Firefox";
    }
    elseif (preg_match('/Chrome/i', $u_agent)) {
      $bname = 'Google Chrome';
      $ub = "Chrome";
    }
    elseif (preg_match('/Safari/i', $u_agent)) {
      $bname = 'Apple Safari';
      $ub = "Safari";
    }
    elseif (preg_match('/Opera/i', $u_agent)) {
      $bname = 'Opera';
      $ub = "Opera";
    }
    elseif (preg_match('/Netscape/i', $u_agent)) {
      $bname = 'Netscape';
      $ub = "Netscape";
    }

    // Finally get the correct version number.
    $known = ['Version', $ub, 'other'];
    $pattern = '#(?<browser>' . join('|', $known) .
      ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
      // We have no matching number just continue.
    }

    // See how many we have.
    $i = count($matches['browser']);
    if ($i != 1) {
      // We will have two since we are not using 'other' argument yet
      // see if version is before or after the name.
      if (strripos($u_agent, "Version") < strripos($u_agent, (string) $ub)) {
        $version = $matches['version'][0];
      }
      elseif (isset($matches['version'][1])) {
        $version = $matches['version'][1];
      }
    }
    else {
      $version = $matches['version'][0];
    }

    // Check if we have a number.
    if ($version == NULL || $version == "") {
      $version = "?";
    }

    return [
      'userAgent' => $u_agent,
      'name'      => $bname,
      'version'   => $version,
      'platform'  => $platform,
      'pattern'    => $pattern,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(ElectionEntityInterface $entity) {
    $raw = $this->get($entity);

    $stats = $this->generate($entity);

    foreach ($stats as $browser => $data) {
      $stats[$browser]['proportion'] = $stats[$browser]['proportion'] . '%';
    }

    // If (\Drupal::moduleHandler()->moduleExists('charts')) {
    // return $this->buildChart($entity, $stats);
    // } else {.
    return [
      '#type' => 'table',
      '#header' => [
        t('Browser'),
        t('Votes'),
        t('Proportion'),
      ],
      '#rows' => $stats,
    ];
    // }
  }

  /**
   *
   */
  public function buildChart(ElectionEntityInterface $entity, $stats) {
    $values = array_values($stats);
    $datesBetweenFirstAndLast = array_keys($stats);

    // Define a series to be used in multiple examples.
    $series = [
      '#type' => 'chart_data',
      // '#title' => t('All votes'),
      '#data' => $values,
      '#color' => '#f26640',
      '#line_width' => 3,
    ];

    // Define an x-axis to be used in multiple examples.
    $xaxis = [
      '#type' => 'chart_xaxis',
      '#title' => t('Time'),
      '#labels' => $datesBetweenFirstAndLast,
    ];

    $yaxis = [
      '#type' => 'chart_yaxis',
      '#title' => t('Votes'),
    ];

    return [
      '#type' => 'chart',
      '#chart_type' => 'spline',
      '#title' => t('Votes over time'),
      '#spline' => TRUE,
      '#polar' => FALSE,
      '#tooltips' => [],

      'series' => $series,
      'x_axis' => $xaxis,
      'y_axis' => $yaxis,

      '#data_labels' => FALSE,

      '#raw_options' => [
        'point' => [
          'show' => FALSE,
        ],
        'line' => [
          'point' => FALSE,
          'zerobased' => TRUE,
          'classes' => 'chart-thick-line',
        ],
        'axis' => [
          'x' => [
            'tick' => [
              'show' => FALSE,
              'count' => 15,
              'culling' => [
                'lines' => FALSE,
                'max' => 15,
              ],
            ],
          ],
        ],
      ],

      '#title_font_size' => 14,
      '#title_font_weight' => 'bold',

      '#legend' => FALSE,
      '#legend_position' => 'right',

      '#exporting_library' => FALSE,

      '#tooltips' => TRUE,
      '#tooltips_use_html' => TRUE,

      '#cache' => $this->getCacheTags($entity),
    ];
  }

}
