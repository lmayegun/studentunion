<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionEntityInterface;

/**
 * @ElectionStatistic(
 *  id = "nominations_over_time",
 *  label = @Translation("Nominations over time"),
 *  sensitive = TRUE,
 *  quick = TRUE,
 * )
 */
class NominationsOverTime extends OverTimeBase {

  /**
   *
   */
  public function getCountingLabel() {
    return t('Nominations');
  }

  /**
   *
   */
  public function getTitle() {
    return t('Nominations over time');
  }

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    return $this->generateFromDatabase($entity, 'election_candidate_field_data', 'Nominations', 'Nominees');
  }

}
