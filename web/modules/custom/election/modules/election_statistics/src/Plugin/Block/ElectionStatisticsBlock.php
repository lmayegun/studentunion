<?php

namespace Drupal\election_statistics\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "election_statistics",
 *   admin_label = @Translation("Election statistics block"),
 * )
 */
class ElectionStatisticsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $context_provider = \Drupal::service('election.election_route_context');

    $contexts = $context_provider->getRuntimeContexts(['election', 'election_post']);

    $election = NULL;
    $election_post = NULL;

    if (isset($contexts['election']) && $contexts['election']) {
      $election = $contexts['election']->getContextValue();
      // $election_post = Election::load($contexts['election']->getContextValue());
    }
    elseif (isset($contexts['election_post']) && $contexts['election_post']) {
      $election_post = $contexts['election_post']->getContextValue();
      $election = $election_post->getElection();
    }

    if (!$election) {
      return [];
    }

    $render = [];
    if ($election) {

      $pluginManager = \Drupal::service('plugin.manager.election_statistics');
      $plugins = $pluginManager->getFilteredDefinitions();
      foreach ($plugins as $plugin_id => $definition) {
        $plugin = $pluginManager->createInstance($plugin_id, $definition);
        $render[] = $plugin->build($election);
      }
    }
    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['statistics_to_show'] = [
      '#type' => 'table',
      '#header' => [
        '',
        $this->t('Weight'),
        $this->t('Title'),
      ],
      '#attributes' => [
        'id' => 'stats_table',
      ],
      '#tabledrag' => [[
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'draggable-weight',
      ],
      ],
    ];

    $pluginManager = \Drupal::service('plugin.manager.election_statistics');
    $plugins = $pluginManager->getFilteredDefinitions();
    foreach ($plugins as $plugin_id => $definition) {
      $plugin = $pluginManager->createInstance($plugin_id, $definition);
      $weight = $plugin->weight;

      $form['table'][$plugin->getPluginId()] = [
        'data' => [],
      ];
      $form['table'][$plugin->getPluginId()]['#attributes']['class'] = ['draggable'];
      $form['table'][$plugin->getPluginId()]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes' => [
          'class' => [
            'draggable-weight',
          ],
        ],
      ];
      $form['table'][$plugin->getPluginId()]['label'] = [
        '#markup' => $plugin->getLabel(),
      ];
    }

    $form['show_election_or_post'] = [
      '#type' => 'select',
      '#title' => 'When block is displayed on an election post page / context, show election post stats or overall election stats?',
      '#options' => [
        'election_post' => 'Election post',
        'election' => 'Election',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['text'] = $form_state->getValue('text');
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['button_link'] = $form_state->getValue('button_link');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // @todo get tags from included plugins.
    return Cache::mergeTags(parent::getCacheTags(), []);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route.group']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // 1 hour
    return 60 * 60;
  }

}
