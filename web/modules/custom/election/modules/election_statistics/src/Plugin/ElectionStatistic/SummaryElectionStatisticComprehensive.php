<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionBallot;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election\Entity\ElectionPost;

/**
 * @ElectionStatistic(
 *  id = "summary_comprehensive",
 *  label = @Translation("General statistics"),
 *  weight = -50,
 * )
 */
class SummaryElectionStatisticComprehensive extends ElectionStatisticBase implements ElectionStatisticInterface {

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    $ballots = 0;
    $uids = [];
    $positions = 0;
    $positionsWithVotes = 0;
    $candidates = 0;
    $abstentions = 0;
    $votes = 0;

    if ($entity->getEntityTypeId() == 'election') {
      $election_posts = $entity->getPostIds(FALSE);
    }
    else {
      $election_posts = [$entity->id()];
    }

    foreach ($election_posts as $election_post) {
      $positions++;
      $election_post = ElectionPost::load($election_post);
      $post_candidates = $election_post->countCandidatesForVoting();
      $candidates += $post_candidates;

      $ballotIds = \Drupal::entityQuery('election_ballot')
        ->condition('confirmed', 1)
        ->condition('election_post', $election_post->id())
        ->execute();

      $post_ballots = ElectionBallot::loadMultiple($ballotIds);
      if (count($post_ballots) > 0) {
        $positionsWithVotes++;
      }
      foreach ($post_ballots as $ballot) {
        $uids[] = $ballot->getOwnerId();
        if ($ballot->abstained->value) {
          $abstentions++;
        }
        else {
          $votes++;
        }
        $ballots++;
      }
    }

    $uids = array_unique($uids);
    $voters = count($uids);

    $results = [
      'positions' => $positions,
      'candidates' => $candidates,
      'ballots' => $ballots,
      'votes' => $votes,
      'absentions' => $abstentions,
      'voters' => $voters,
      'positions_with_votes' => $positionsWithVotes,
    ];

    if ($entity->getEntityTypeId() == 'election_post') {
      unset($results['positions']);
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function build(ElectionEntityInterface $entity) {
    $stats = $this->get($entity);

    $header = [
      'positions' => t('@positions', ['@positions' => $entity->getPostTypesAsLabel()]),
      t('@candidates*', ['@candidates' => 'Candidates']),
      t('Total ballots'),
      t('Vote ballots'),
      t('Abstention ballots'),
      t('Voters'),
      t('Positions with votes'),
    ];

    if ($entity->getEntityTypeId() == 'election_post') {
      unset($header['positions']);
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => [$stats],
      '#suffix' => '<p>* Note candidates includes only published and hopeful candidates.</p>',
    ];
  }

}
