<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election\Entity\ElectionInterface;
use Drupal\user\Entity\User;

/**
 *
 */
abstract class ElectionStatisticBase extends PluginBase implements ElectionStatisticInterface {

  const USE_CACHE = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   *
   */
  public function getCacheId(ElectionEntityInterface $entity) {
    $cid_components = [
      $this->getPluginId(),
      $entity->getEntityTypeId(),
      $entity->id(),
    ];
    $cid = implode(':', $cid_components);
    return $cid;
  }

  /**
   *
   */
  public function getNewBallotsSinceLastUpdate(ElectionEntityInterface $entity) {
    $cid = $this->getCacheId($entity);
    if (static::USE_CACHE && $cache = \Drupal::cache('election_statistics')->get($cid)) {
      $ballotIds = \Drupal::entityQuery('election_ballot')
        ->condition('created', $cache->created, '>')
        ->condition($entity->getEntityTypeId(), $entity->id())
        ->execute();
      return $ballotIds;
    }
    return [];
  }

  /**
   *
   */
  public function getCacheData(ElectionEntityInterface $entity) {
    $cid = $this->getCacheId($entity);
    $cache = \Drupal::cache('election_statistics')->get($cid);
    if (static::USE_CACHE && $cache) {
      return $cache->data;
    }
    else {
      return NULL;
    }
  }

  /**
   * Get the data for the statistic, from cache if relevant.
   *
   * @param \Drupal\election\Entity\ElectionEntityInterface $entity
   * @param $refresh
   *
   * @return [type]
   */
  public function get(ElectionEntityInterface $entity, $refresh = FALSE) {
    // Get cache ID to uniquely identify:
    $cid = $this->getCacheId($entity);

    $ballotIds = [];

    // Load if getting from cache:
    if (!$refresh && static::USE_CACHE && $cache = \Drupal::cache('election_statistics')->get($cid)) {
      $ballotIds = $this->getNewBallotsSinceLastUpdate($entity);
      if (count($ballotIds) == 0) {
        $result = $cache->data;
        return $result;
      }
    }

    // Generate if not:
    $result = $this->generate($entity, $ballotIds);
    $tags = $this->getCacheTags($entity);
    \Drupal::cache('election_statistics')->set($cid, $result, Cache::PERMANENT, $tags);
    return $result;
  }

  /**
   *
   */
  public static function getFilename($uid) {
    return 'public://election_stats/user_profile_' . $uid . '.json';
  }

  /**
   *
   */
  public static function setJsonFile($uid, $expiry, $data) {
    if (!file_exists('public://election_stats')) {
      \Drupal::service('file_system')->mkdir('public://election_stats', 0777);
    }

    $data['expiry'] = $expiry;

    \Drupal::service('file_system')->saveData(
      json_encode($data),
      'public://election_stats/user_rpf' . $uid . '.json',
      FileSystemInterface::EXISTS_REPLACE,
    );
  }

  /**
   *
   */
  public static function getJsonUrl($uid) {
    return \Drupal::service('file_url_generator')->generateAbsoluteString(static::getFilename($uid));
  }

  /**
   *
   */
  public static function getJsonFile($uid) {
    if (!file_exists(static::getFilename($uid))) {
      return NULL;
    }

    $path = static::getJsonUrl($uid);

    try {
      $arrContextOptions = [
        "ssl" => [
          "verify_peer" => FALSE,
          "verify_peer_name" => FALSE,
        ],
      ];

      $str = file_get_contents($path, FALSE, stream_context_create($arrContextOptions));
    }
    catch (\Exception $e) {
      return NULL;
    }
    $json = json_decode($str, TRUE);
    return $json;
  }

  /**
   *
   */
  public function getUserProfile($uid) {
    $json = static::getJsonFile($uid);
    if ($json && $json['expiry'] > strtotime('now')) {
      $profile = $json;
    }
    else {
      // $cid = '1election_statistics:user:' . $uid;
      // if ($cache = \Drupal::cache('election_statistics')->get($cid)) {
      //  $profile = $cache->data;
      // } else {
      $pluginManager = \Drupal::service('plugin.manager.election_statistics');
      $profile = $pluginManager->generateUserProfile(User::load($uid));
      // }
    }
    if ($profile && isset($profile[$this->getPluginId()])) {
      return $profile[$this->getPluginId()];
    }
    return [];
  }

  /**
   *
   */
  public function getCacheTagsUserProfile(AccountInterface $account) {
    return [];
  }

  /**
   *
   */
  public function generateStatProfile() {
    return [];
  }

  /**
   *
   */
  public function generateUserProfile(AccountInterface $account) {
    return [];
  }

  /**
   *
   */
  public function getStatProfile() {
    $cid = 'election_statistics:statistics';
    if (static::USE_CACHE && $cache = \Drupal::cache('election_statistics')->get($cid)) {
      $profile = $cache->data;
    }
    else {
      $pluginManager = \Drupal::service('plugin.manager.election_statistics');
      $profile = $pluginManager->generateStatProfile();
    }
    if ($profile && isset($profile[$this->getPluginId()])) {
      return $profile[$this->getPluginId()];
    }
    return [];
  }

  /**
   *
   */
  public function getCacheTagsStatProfile() {
    return [];
  }

  /**
   * Generate the statistics.
   *
   * Do not cache here - that is handled in get(). This should always return live information.
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    return [];
  }

  /**
   * Create a render array for the statistics. This should call ->get() to get the data.
   */
  public function build(ElectionEntityInterface $entity) {
    return [];
  }

  /**
   * Return the rendered statistics.
   */
  public function render(ElectionEntityInterface $entity) {
    $render = $this->build($entity);
    return \Drupal::service('renderer')->render($render);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // $form['example_setting'] = [
    //   '#type' => 'number',
    //   '#title' => 'Example setting',
    //   '#min' => 1,
    //   '#default_value' => $this->configuration['example_setting'],
    // ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    // $this->configuration['example_setting'] = $form_state->getValue('example_setting');
  }

  /**
   *
   */
  public function getElection(ElectionEntityInterface $entity) {
    if ($entity->getEntityTypeId() == 'election') {
      return $entity;
    }
    else {
      return $entity->getElection();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(ElectionEntityInterface $entity) {
    $election = $this->getElection($entity);
    return [
      'election:' . $election->id() . ':votes:count',
      'election:' . $election->id() . ':posts:count',
      'election:' . $election->id() . ':candidates:count',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayLabel() {
    return $this->pluginDefinition['display_label'];
  }

  /**
   *
   */
  public function isQuick() {
    if (isset($plugin_definition['quick'])) {
      return $plugin_definition['quick'];
    }

    return FALSE;
  }

  /**
   *
   */
  public function isSensitive() {
    if (isset($plugin_definition['sensitive'])) {
      return $plugin_definition['sensitive'];
    }

    return FALSE;
  }

  /**
   * Get a list of the most popular attribute values for voters in this election.
   *
   * @param object $election
   *   The election entity.
   * @param string $group_by
   *   The {election_ballot} table column to group results by.
   * @param int $limit
   *   The maximum number of items to select.
   *
   * @return array
   *   An array whose elements are associative arrays containing a string 'agent'
   *   and an integer 'voters'.
   */
  public static function getTopBallotFieldValues(ElectionInterface $election, string $group_by, $limit = 10) {
    $query = \Drupal::database()->select('election_ballot', 'eb')
      ->condition('election', $election->id())
      ->condition('confirmed', 1)
      ->fields('eb', [$group_by]);
    $query->addExpression('COUNT(DISTINCT IF(user_id = 0, session_hash, user_id))', 'voters');
    $query->orderBy('voters', 'DESC');
    $query->groupBy($group_by);
    if ($limit > -1) {
      $query->range(0, $limit);
    }
    return $query->execute()->fetchAllAssoc($group_by);
  }

}
