<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\election\Entity\ElectionEntityInterface;

/**
 * @ElectionStatistic(
 *  id = "votes_over_time",
 *  label = @Translation("Votes over time"),
 *  quick = TRUE,
 * )
 */
class VotesOverTime extends OverTimeBase {

  /**
   * {@inheritdoc}
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []) {
    return $this->generateFromDatabase($entity, 'election_ballot', 'Votes', 'Voters');
  }

}
