<?php

namespace Drupal\election_statistics\Plugin\ElectionStatistic;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\election\Entity\ElectionEntityInterface;

/**
 *
 */
interface ElectionStatisticInterface extends ConfigurableInterface, DependentPluginInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * @param \Drupal\election\Entity\ElectionEntityInterface $entity
   *
   * @return array
   *   Render array
   */
  public function generate(ElectionEntityInterface $entity, array $ballotIds = []);

  /**
   * @param \Drupal\election\Entity\ElectionEntityInterface $entity
   *
   * @return string
   */
  public function render(ElectionEntityInterface $entity);

}
