<?php

namespace Drupal\election_test\Plugin\DevelGenerate;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election\Entity\ElectionPostType;
use Drupal\election\Entity\ElectionType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a VocabularyDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "election_post",
 *   label = @Translation("election posts"),
 *   description = @Translation("Generate a given number of election posts."),
 *   url = "election_posts",
 *   permission = "administer election",
 *   settings = {
 *     "num" = 50,
 *     "candidates" = TRUE,
 *     "kill" = TRUE,
 *   }
 * )
 */
class ElectionPostDevelGenerate extends DevelGenerateBase implements ContainerFactoryPluginInterface {

  /**
   * The post storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $electionPostStorage;

  /**
   * Constructs a new VocabularyDevelGenerate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The vocabulary storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $entity_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->electionPostStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('election_post')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['election_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Election'),
      '#target_type' => 'election',
      '#default_value' => $this->getSetting('election_id'),
      '#required' => TRUE,
    ];
    $form['num'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of election posts'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    ];
    $form['candidates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate candidates as well?'),
      '#default_value' => $this->getSetting('candidates'),
    ];
    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete existing election posts in this election before generating new ones.'),
      '#default_value' => $this->getSetting('kill'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function generateElements(array $values) {
    $election = Election::load($values['election_id']);
    if (!$election) {
      return;
    }

    if ($values['kill']) {
      $this->deletePosts($election);
      $this->setMessage($this->t('Deleted existing election posts.'));
    }

    if ($this->isBatch($values['num'])) {
      $this->generateBatchPosts($election, $values['num'], $values['candidates']);
    }
    else {
      $new_vocs = $this->generatePosts($election, $values['num'], $values['candidates']);
      if (!empty($new_vocs)) {
        $this->setMessage($this->t('Created the following new election_posts: @vocs', ['@vocs' => implode(', ', $new_vocs)]));
      }
    }
  }

  /**
   * Deletes all vocabularies.
   */
  protected function deletePosts($election) {
    $post_ids = $election->getPostIds(FALSE);
    $posts = ElectionPost::loadMultiple($post_ids);
    foreach ($posts as $post) {
      $candidates = $post->getCandidates(NULL, FALSE);
      foreach ($candidates as $candidate) {
        $candidate->delete();
      }
      $post->delete();
    }
  }

  /**
   *
   */
  public function randomBooleanWithChance(float $chanceOfTrue = 0.5) {
    $true = random_int(1, 100) <= ($chanceOfTrue * 100);
    return $true;
  }

  /**
   *
   */
  public function randomNumberWithChance(int $min, int $max, float $chanceOfOverZero = 0.5) {
    $int = random_int($min, $max);
    $overZero = random_int(1, 100) <= ($chanceOfOverZero * 100);
    return $overZero ? $int : 0;
  }

  /**
   * Generates posts.
   *
   * @param int $records
   *   Number of posts to create.
   *
   * @return array
   *   Array containing the generated vocabularies id.
   */
  protected function generatePosts(ElectionInterface $election, $records, $generateCandidates = TRUE) {
    $posts = [];

    // Insert new data:
    for ($i = 1; $i <= $records; $i++) {
      $name = str_replace('.', '', $this->getRandom()->sentences(random_int(2, 4), TRUE));

      $election_type = ElectionType::load($election->bundle());
      $types = $election_type->getAllowedPostTypes();
      $type = $types[array_rand($types)];

      $post_data = [
        'name' => $name,
        'election' => $election->id(),
        'type' => $type->id(),
        'langcode' => Language::LANGCODE_NOT_SPECIFIED,
        'status' => $this->randomBooleanWithChance(0.8),
        'vacancies' => 1 + $this->randomNumberWithChance(0, 4, 0.25),
        'skip_allowed' => $this->randomBooleanWithChance(0.8),
        'abstentions_allowed' => $this->randomBooleanWithChance(0.8),
        'include_reopen_nominations' => $this->randomBooleanWithChance(0.95),
        'exclusive' => $this->randomBooleanWithChance(0.8),
        'publish_candidates_automatically' => $this->randomBooleanWithChance(0.8),
        'voting_method_inherit' => $this->randomBooleanWithChance(0.7),
        'conditions_inherit_election' => 'inherit',
      ];
      $post = $this->electionPostStorage->create($post_data);

      // Populate all fields with sample values.
      $this->populateFields($post);

      // Candidate type.
      $election_post_type = ElectionPostType::load($post->bundle());
      $candidate_types = $election_post_type->getAllowedCandidateTypes();
      $post->candidate_type->entity = $candidate_types[array_rand($candidate_types)];

      // Conditions.
      $post = $this->generateStatuses($post);
      $post = $this->generateConditions($post);

      if ($generateCandidates) {
        // Use another DevelGenerate plugin to generate candidates as well:
        $this->generateCandidates($post);
      }
      $post->save();

      $posts[] = $post->id();
      unset($post);
    }

    return $posts;
  }

  /**
   *
   */
  public function generateStatuses(ElectionPostInterface $post) {
    // Only do anything to customise election posts statuses a quarter of the time.
    if (random_int(0, 3) != 2) {
      return $post;
    }

    // Customise statuses...
    $post->save();
    return $post;
  }

  /**
   *
   */
  public function generateConditions(ElectionPostInterface $post) {
    // Only do anything to customise election posts conditions a proportion of the time.
    if (random_int(0, 3) == 0) {
      return $post;
    }

    // Get a list of available conditions.
    /** @var \Drupal\complex_conditions\ConditionManagerInterface $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.complex_conditions');
    $definitions = $plugin_manager->getFilteredDefinitions(['election']);

    unset($definitions['complex_conditions_or_operator']);
    unset($definitions['complex_conditions_and_operator']);

    // Randomly pick a number of conditions to add.
    // Half the time, just one.
    $count = random_int(0, 1) == 1 ? 1 : random_int(2, 3);
    if (count($definitions) > 0 && $count > 0) {
      $plugins = array_rand($definitions, $count);
      if ($count == 1) {
        $plugins = [0 => $plugins];
      }
    }
    else {
      $plugins = [];
    }

    // Populate each condition.
    $conditions = [];
    foreach ($plugins as $plugin_id) {
      $plugin = $plugin_manager->createInstance($plugin_id, []);
      $config = $plugin->generateRandomConditionConfig();
      if ($config) {
        $conditions[] = $config;
      }
    }

    $post->set('conditions_voting', $conditions);

    return $post;
  }

  /**
   *
   */
  public function generateCandidates(ElectionPostInterface $post) {
    $candidatesToGenerate = $this->randomNumberWithChance(1, 5, 0.8);
    $pluginManager = \Drupal::service('plugin.manager.develgenerate');
    $plugin = $pluginManager->createInstance('election_candidates', []);
    $candidates = $plugin->generateCandidates($post, $candidatesToGenerate);
    return $candidates;
  }

  /**
   * {@inheritdoc}
   */
  public function validateDrushParams(array $args, array $options = []) {
    $values = [
      'num' => array_shift($args),
      'election_id' => $options['election_id'],
      'kill' => $options['kill'],
      'candidates' => $options['candidates'],
    ];

    if ($this->isNumber($values['num']) == FALSE) {
      throw new \Exception(dt('Invalid number of vocabularies: @num.', ['@num' => $values['num']]));
    }
    if ($this->isNumber($values['election_id']) == FALSE || !Election::load($values['election_id'])) {
      throw new \Exception(dt('Invalid election_id: @num.', ['@num' => $values['election_id']]));
    }

    return $values;
  }

  /**
   * Generate content in batch mode.
   *
   * This method is used when the number of elements is 50 or more.
   */
  private function generateBatchPosts(ElectionInterface $election, int $records, $generateCandidates = TRUE) {
    // If it is drushBatch then this operation is already run in the
    // self::validateDrushParams().
    // Add the kill operation.
    // if ($values['kill']) {
    //   $operations[] = [
    //     'devel_generate_operation',
    //     [$this, 'batchContentKill', $values],
    //   ];
    // }.
    // Add the operations to create the nodes.
    for ($num = 0; $num < $records; $num++) {
      $operations[] = [
        'devel_generate_operation',
        [$this, 'batchPostCreate', [
          'election' => $election,
          'records' => 1,
          'generateCandidates' => $generateCandidates,
        ],
        ],
      ];
    }

    // Set the batch.
    $batch = [
      'title' => $this->t('Generating posts...'),
      'operations' => $operations,
      'finished' => 'devel_generate_batch_finished',
      'file' => \Drupal::service('extension.list.module')->getPath('devel_generate') . '/devel_generate.batch.inc',
    ];

    batch_set($batch);
    // If ($this->drushBatch) {
    //   drush_backend_batch_process();
    // }
  }

  /**
   * Batch wrapper for calling ContentAddNode.
   */
  public function batchPostCreate($values, &$context) {
    $election = $values['election'];
    $records = $values['records'];
    $generateCandidates = $values['generateCandidates'];
    $this->generatePosts($election, $records, $generateCandidates);
    if (!isset($context['results']['num'])) {
      $context['results']['num'] = 0;
    }
    $context['results']['num'] += $records;
  }

  /**
   * Determines if the content should be generated in batch mode.
   */
  protected function isBatch($post_count) {
    return $post_count >= 50;
  }

}
