<?php

namespace Drupal\election_test\Plugin\DevelGenerate;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election\Entity\ElectionPostType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a ElectionCandidateDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "election_candidates",
 *   label = @Translation("election candidates"),
 *   description = @Translation("Generate a given number of election candidates."),
 *   url = "election_candidates",
 *   permission = "administer election",
 *   settings = {
 *     "num" = 50,
 *     "kill" = FALSE
 *   }
 * )
 */
class ElectionCandidateDevelGenerate extends DevelGenerateBase implements ContainerFactoryPluginInterface {

  /**
   * The post storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $electionCandidateStorage;

  /**
   * Constructs a new VocabularyDevelGenerate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The vocabulary storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $entity_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->electionCandidateStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('election_candidate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['election_post'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Election post'),
      '#target_type' => 'election_post',
      '#default_value' => $this->getSetting('election_post'),
      '#required' => TRUE,
    ];

    $form['num'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of election candidates'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete existing election candidates in this election post before generating new ones.'),
      '#default_value' => $this->getSetting('kill'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function generateElements(array $values) {
    $election_post = ElectionPost::load($values['election_post']);
    if (!$election_post) {
      return;
    }
    if ($values['kill']) {
      $this->deleteCandidates($election_post);
      $this->setMessage($this->t('Deleted existing election candidates.'));
    }

    $new_vocs = $this->generateCandidates($election_post, $values['num']);
    if (!empty($new_vocs)) {
      $this->setMessage($this->t('Created the following new candidates: @vocs', ['@vocs' => implode(', ', $new_vocs)]));
    }
  }

  /**
   * Deletes all vocabularies.
   */
  protected function deleteCandidates($election_post) {
    $candidates = $election_post->getCandidates(FALSE);
    foreach ($candidates as $candidate) {
      $candidate->delete();
    }
  }

  /**
   * Generates posts.
   *
   * @param int $records
   *   Number of posts to create.
   *
   * @return array
   *   Array containing the generated vocabularies id.
   */
  public function generateCandidates(ElectionPostInterface $election_post, $records) {
    $candidates = [];

    // Insert new data:
    for ($i = 1; $i <= $records; $i++) {
      $name = $this->getRandom()->sentences(2, TRUE);

      $candidate_statuses = ElectionCandidate::getPossibleStatuses();
      $candidate_status = random_int(0, 3) != 2 ? 'hopeful' : $candidate_statuses[array_rand($candidate_statuses)];

      $election_post_type = ElectionPostType::load($election_post->bundle());
      $types = $election_post_type->getAllowedCandidateTypes();
      $type = $types[array_rand($types)];

      $candidate_data = [
        // 'user_id' => 1,
        'name' => $name,
        'type' => $type->id(),
        'status' => random_int(0, 3) == 1 ? FALSE : TRUE,
        'langcode' => Language::LANGCODE_NOT_SPECIFIED,
        'election_post' => $election_post->id(),
        'candidate_status' => $candidate_status,
      ];

      $candidate = $this->electionCandidateStorage->create($candidate_data);

      // Populate all fields with sample values.
      $this->populateFields($candidate);
      $candidate->save();

      $candidates[] = $candidate->id();
      unset($candidate);
    }

    return $candidates;
  }

  /**
   * {@inheritdoc}
   */
  public function validateDrushParams(array $args, array $options = []) {
    $values = [
      'num' => array_shift($args),
      'election_post' => $options['election_post'],
      'kill' => $options['kill'],
    ];

    if ($this->isNumber($values['num']) == FALSE) {
      throw new \Exception(dt('Invalid number of vocabularies: @num.', ['@num' => $values['num']]));
    }
    if ($this->isNumber($values['election_post']) == FALSE || !Election::load($values['election_post'])) {
      throw new \Exception(dt('Invalid election_post: @num.', ['@num' => $values['election_post']]));
    }

    return $values;
  }

}
