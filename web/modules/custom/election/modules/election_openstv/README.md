
PENDING ISSUES

- Saving Post Type settings from the Voting Method plugin - https://www.adamfranco.com/2018/11/13/creating-a-drupal-plugin-system/

TO INSTALL

- Copy the contents of open_stv_report_plugin to libraries/openstv/ReportPlugins
- Update /admin/election/config/openstv with the path to your python command. This needs to be Python 2.
