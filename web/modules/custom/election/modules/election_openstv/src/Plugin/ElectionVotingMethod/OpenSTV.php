<?php

namespace Drupal\election_openstv\Plugin\ElectionVotingMethod;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Plugin\ElectionVotingMethodBase;

/**
 * Single transferable vote.
 *
 * @ElectionVotingMethod(
 *   id = "open_stv",
 *   label = @Translation("Single transferable vote"),
 * )
 */
class OpenSTV extends ElectionVotingMethodBase {

  /**
   * The STV method.
   *
   * @var string
   */
  protected $detailed_method;

  /**
   *
   */
  public function checkCanRun($entity) {
    $config = \Drupal::config('election_openstv.openstvsettings');
    $command = $config->get('openstv_command');
    if (!$command) {
      return [
        'can' => FALSE,
        'message' => t('Cannot use OpenSTV - you need to set the Python command at <a href="@settings">the OpenSTV configuration page</a>.', [
          '@settings' => Url::fromRoute('election_openstv.open_stv_settings_form')->toString(),
        ]),
      ];
    }
    return [
      'can' => TRUE,
      'message' => t('Voting method is set up correctly.'),
    ];
  }

  /**
   * Set our instance value for STV method.
   *
   * @param string $method
   *   The method of STV voting.
   */
  public function setDetailedMethod($detailed_method) {
    $this->detailed_method = $detailed_method;
  }

  /**
   * Answer our instance value for STV method.
   *
   * @return string
   *   The method of STV voting.
   */
  public function getDetailedMethod() {
    return $this->configuration['detailed_method'] ?? '';
  }

  /**
   *
   */
  public function getAllowsEqualRanking() {
    return $this->configuration['allow_equal_ranking'] ?? FALSE;
  }

  /**
   *
   */
  public function allowsEqualRanking() {
    return $this->getAllowsEqualRanking() ? TRUE : FALSE;
  }

  /**
   * Answer our instance value for STV method.
   *
   * @return string
   *   The method of STV voting.
   */
  public function getDetailedMethodLabel() {
    if (empty($this->getDetailedMethod())) {
      return '';
    }
    else {
      return _election_openstv_get_methods()[$this->getDetailedMethod()] ?: $this->getDetailedMethod();
    }
  }

  /**
   *
   */
  public function runOpenStvCount($detailed_method, $export_filename, $format, &$error) {
    // Build the OpenSTV command.
    $config = \Drupal::config('election_openstv.openstvsettings');
    $command = $config->get('openstv_command');
    $cmd = escapeshellcmd($command);
    $cmd .= ' -r ' . escapeshellarg($format);
    $cmd .= ' ' . escapeshellarg($detailed_method);
    $cmd .= ' ' . escapeshellarg($export_filename);

    $descriptorspec = [
      0 => ['pipe', 'r'],
      1 => ['pipe', 'w'],
      2 => ['pipe', 'w'],
    ];

    // Run the OpenSTV command, capturing the result and any errors.
    $process = proc_open($cmd, $descriptorspec, $pipes);
    if ($process) {
      $result = stream_get_contents($pipes[1]);
      $error = stream_get_contents($pipes[2]) . ' [cmd: ' . $cmd . ']';
      fclose($pipes[1]);
      fclose($pipes[2]);
      proc_close($process);
    }
    else {
      $error = t('Failed to run the OpenSTV command: @cmd', ['@cmd' => $cmd]);
      return;
    }
    return $result;
  }

  /**
   *
   */
  public function exportPost(ElectionPost $electionPost, array $candidates = NULL, array $options = []) {
    return \Drupal::service('election_ballot_export.export_service')->exportPost($electionPost, $candidates, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function countPost(ElectionPost $electionPost, array $candidates, array $options = []) {
    $error = NULL;

    // Export post and get the absolute system path to the file.
    $export_filename = $this->exportPost($electionPost, $candidates, $options);

    // Settings for count.
    $detailed_method = $options['detailed_method'] ?? 'ERS97STV';
    $format = 'ResultsArrayWithHtml';

    // Get result.
    $raw_result = $this->runOpenStvCount($detailed_method, $export_filename, $format, $error);
    $result = (array) json_decode(utf8_encode($raw_result));

    if (!empty($error)) {
      \Drupal::logger('election_openstv')->error($error);
    }

    // Delete the temporary export file.
    // unlink($export_filename);
    $output = [
      'count_timestamp' => \Drupal::time()->getRequestTime(),
      'count_method' => _election_openstv_get_methods()[$detailed_method] ?: $detailed_method,
    ];

    if (empty($result)) {
      return array_merge([
        'error' => $error,
      ], $output);
    }

    // Create results array for the ElectionPost class to interpret.
    $candidates = \Drupal::service('election_ballot_export.export_service')->getCandidates($electionPost);

    $ronWon = FALSE;

    $elected = [];
    foreach ($result['winners'] as $candidateCountId) {
      $id = $this->getCandidateIdFromCountResult($candidateCountId, $result['names']);
      if ($id == 'ron') {
        $elected[] = 'ron';
        $ronWon = TRUE;
      }
      else {
        $elected[] = ElectionCandidate::load($id);
      }
    }

    $defeated = [];
    foreach ($result['losers'] as $candidateCountId) {
      $id = $this->getCandidateIdFromCountResult($candidateCountId, $result['names']);
      if ($id == 'ron') {
        $defeated[] = 'ron';
      }
      else {
        $defeated[] = ElectionCandidate::load($id);
      }
    }

    // @todo something with "continuing"?
    // $result['candidates'] = self.cleanB.numCandidates
    $output = array_merge(
      $output,
      [
        'count_results_text' => $result['text'],
        'count_results_html' => $result['html'],
        'count_total_ballots' => $result['ballots'],
        'count_total_votes' => $result['votes_valid'],
        'count_total_votes_invalid' => $result['votes_invalid'],
        'count_total_vacancies' => $result['vacancies'],
    // @todo
        'count_total_abstentions' => '',
        'count_candidates_all' => $electionPost->getCandidates(NULL, FALSE),
        'count_candidates_published' => $electionPost->getCandidates(NULL, TRUE),
        'count_candidates_included' => array_merge($elected, $defeated),
        'count_candidates_elected' => $elected,
        'count_candidates_defeated' => $defeated,
        'count_has_random_element' => stristr($result['html'], 'chosen by breaking the tie') ? 1 : 0,
        'count_ron_won' => $ronWon ? 1 : 0,
      ],
    );

    return $output;
  }

  /**
   *
   */
  public function getCandidateIdFromCountResult($countResultId, $names) {
    $name = $names[$countResultId];
    if ($name == 'RON (Re-open Nominations)') {
      return 'ron';
    }
    preg_match_all("/\[(.*?)\]/", $name, $ids);
    return reset(end($ids));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'detailed_method' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    parent::buildConfigurationForm($form, $form_state);

    $form['detailed_method'] = [
      '#type' => 'select',
      '#title' => t('STV method'),
      '#default_value' => $this->getDetailedMethod(),
      '#options' => array_merge(
        ['' => '--- select STV method ---'],
        _election_openstv_get_methods()
      ),
      '#required' => TRUE,
    ];

    $form['allow_equal_ranking'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow equal ranking of options on ballot paper'),
      '#default_value' => $this->getAllowsEqualRanking() ?: FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (empty($form_state->getValue('detailed_method'))) {
      $form_state->setErrorByName('detailed_method', t('Need to provide an STV method.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

}
