"Plugin module for generating a JSON results array with HTML and plain text full counts."

## Copyright (C) 2003-2010 Jeffrey O'Neill
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

__revision__ = "$Id: report.py 570 2022-02-10 17:46:56Z maxwell.keeble $"

import os
import json
import inspect
import textwrap
import math

from openstv.plugins import ReportPlugin
from openstv.version import v as OpenSTV_version

##################################################################

class ResultsArrayWithHtml(ReportPlugin):
  "Return a JSON encoded array that PHP will decode to use elsewhere."

  status = 1
  reportName = "json_with_formatted"

  def __init__(self, e, outputFile=None, test=False):
    ReportPlugin.__init__(self, e, outputFile, test)

    self.maxWidth = 79
    # "full", "table", "round"
    self.style = "full"
    self.maxColWidth = 0
    self.prtf = None

    if self.e.methodName == "Condorcet":
      raise RuntimeError, "JSON report not available for Condorcet elections."

# JSON

  def __repr__(self):
    from pprint import pformat
    return "<" + type(self).__name__ + "> " + pformat(vars(self), indent=4, width=1)

  def generateHeader(self):
    self.output("")

  def generateReportNonIterative(self):
    "Return a results sheet in a JSON array."

    out(json.dumps(self.e))

  def generateReportIterative(self):
    "Return a results sheet in a JSON array."

    self.generateHeader()

    out = self.output

    data = {}
    data['names'] = list(self.cleanB.names)
    data['winners'] = list(self.e.winners)
    data['losers'] = list(self.e.losers)
    data['continuing'] = list(self.e.continuing)

    data['candidates'] = self.cleanB.numCandidates
    data['vacancies'] = self.e.numSeats
    data['ballots'] = self.dirtyB.numBallots
    data['votes_valid'] = self.cleanB.numBallots
    data['votes_invalid'] = self.dirtyB.numBallots - self.cleanB.numBallots

    data['html'] = self.htmlgenerateReportIterative(self) # if self.cleanB.numCandidates > 3 else self.htmlgenerateReportIterativeTabular(self)
    data['text'] = self.textgenerateReportIterative(self)

    out(json.dumps(data, ensure_ascii=False, encoding="utf-8"))

# HTML

  def htmlprintTableRound(self, values):
    """Print a single table for a round"""
    html = '<table class="election__single_round_table">'

    i = 1
    for c in range(self.cleanB.numCandidates):
      html += ("<tr><th>%s</th><td>%s</td></tr>\n" % (self.cleanB.names[c], values[i] if values[i] else "0.00"))
      i += 1
    html += ("<tr><th>%s</th><td>%s</td></tr>\n" % ("Exhausted", values[i] if values[i] else "0.00"))
    i += 1
    if self.e.threshMethod:
      html += ("<tr><th>%s</th><td>%s</td></tr>\n" % ("Surplus",  values[i] if values[i] else "0.00"))
      i += 1
      html += ("<tr><th>%s</th><td>%s</td></tr>\n" % ("Threshold",  values[i] if values[i] else "0.00"))
      i += 1

    html += ("</table>\n\n")
    return html

  def htmlprintTableRow(self, values):
    """Print a single table line"""
    html = ''
    html += ("<tr>\n")
    html += ("<td class='round' rowspan='2'>%s</td>\n" % (values.pop(0)))
    for value in values:
      html += ("<td>%s</td>\n" % value)
    html += ("</tr>\n\n")
    return html

  def htmlgenerateHeader(self):
    from datetime import date
    today = date.today().strftime("%d %b %Y")
    header = """\
<div class="election__result_header election__result_header__info">Count information</div>
<table class="election__info" >
<tr><th>Date count run</th><td>%s</td></tr>
<tr><th>Election rules</th><td>%s</td></tr>
<tr><th>Candidates running</th><td>%s</td></tr>
<tr><th>Available position%s</th><td>%s</td></tr>
<tr><th>Total ballots</th><td>%s</td></tr>
<tr><th>Valid votes</th><td>%s</td></tr>
<tr><th>Invalid votes</th><td>%s</td></tr>
</table>
"""  % (today,
        self.e.longMethodName,
        self.cleanB.numCandidates,
        "s" if self.e.numSeats > 1 else "", self.e.numSeats,
        self.dirtyB.numBallots,
        self.cleanB.numBallots,
        self.dirtyB.numBallots - self.cleanB.numBallots
        )

    return header

  def htmlgenerateReportNonIterative(self):
    "Pretty print results in html format."

    html = self.htmlgenerateHeader()

    nCol = self.cleanB.numCandidates
    nCol += 1 # Exhausted
    nCol += 1 # Not sure

    html += """\
<table class="election__rounds">
<tr>
<th>Round</th>
"""

    for c in range(self.cleanB.numCandidates):
      html += ("<th>%s</th>\n" % self.cleanB.names[c])
    html += ("<th>%s</th>\n" % "Exhausted")
    html += ("</tr>\n\n")

    html += ("<tr>\n")
    html += ("<td class='round' rowspan='2'>1</td>\n")
    for c in range(self.cleanB.numCandidates):
      html += ("<td>%s</td>\n" % self.e.displayValue(self.e.count[c]))
    html += ("<td>%s</td>\n" % self.e.displayValue(self.e.exhausted))
    html += ("</tr>\n\n")

    html += ("<tr><td colspan='%d' class='comment'>%s</td></tr>\n\n" % (nCol, self.e.msg))

    html += ("</table>\n\n")

    # Winners
    winTxt = self.getWinnerText(self.e.winners)
    winTxt = winTxt.replace("\n", "<br>\n")
    html += ("<p class='winner'>%s</p>\n\n" % winTxt)

    html += ("</body>\n</html>\n")
    return html

  def htmlgenerateReportIterative(self, other):
    "Pretty print results in html format."

    html = self.htmlgenerateHeader()

    nCol = self.cleanB.numCandidates
    nCol += 1 # Exhausted
    if self.e.threshMethod:
      nCol += 2 # Surplus and Thresh

    for R in range(self.e.numRounds):
      html += ('<div class="election__round">')
      html += ('<div class="election__round_header">Round %d</div>' % (R+1))
      html += self.htmlprintTableRound(self.getValuesForRound(R))
      html += ("<div class='election__round_comment'>%s</div>" % (self.e.msg[R]))
      html += ('</div>')

    html += ("\n\n")

    # Winners
    winTxt = self.getWinnerText(self.e.winners)
    html += ("<p class='winner'>%s</p>\n\n" % winTxt)
    return html

  def htmlgenerateReportIterativeTabular(self, other):
    "Pretty print results in html format."

    html = self.htmlgenerateHeader()

    nCol = self.cleanB.numCandidates
    nCol += 1 # Exhausted
    if self.e.threshMethod:
      nCol += 2 # Surplus and Thresh

    html += "<table class='election__rounds'><tr><th>Round</th>"

    for c in range(self.cleanB.numCandidates):
      html += ("<th>%s</th>\n" % self.cleanB.names[c])
    html += ("<th>%s</th>\n" % "Exhausted")
    if self.e.threshMethod:
      html += ("<th>%s</th>\n" % "Surplus")
      html += ("<th>%s</th>\n" % "Threshold")
    html += ("</tr>\n\n")

    for R in range(self.e.numRounds):
      html += self.htmlprintTableRow(self.getValuesForRound(R))
      html += ("<tr><td colspan='%d' class='comment'>%s</td></tr>\n\n" % (nCol, self.e.msg[R]))

    html += ("</table>\n\n")

    # Winners
    winTxt = self.getWinnerText(self.e.winners)
    html += ("<p class='winner'>%s</p>\n\n" % winTxt)
    return html

# TEXT
  def textformat(self, value):
    return self.pipify( self.e.displayValue( value ) )

  def textpipify(self, value):
    """Surround text with pipe"""
    return ("|" + self.prtf) % value

  def textprintTableRow(self, values,width, nSubCol):
    """Print one 'line' of results--might occupy more than one line of text."""
    # Separator line
    text = ( "=" * width + "\n" )
    line = values.pop(0)
    for (index,value) in enumerate(values):
      if ((index % nSubCol == 0) and index > 0):
        line += "\n  "
      line += ("|" + self.prtf) % value
    line += "\n"
    text += (line)
    return text

  def textgenerateWithdrawnText(self):

    # Create description of withdrawn candidates
    if len(self.dirtyB.withdrawn) == 0:
      withdrawnText = "No candidates have withdrawn."
    elif len(self.dirtyB.withdrawn) == 1:
      withdrawnText = "Removed withdrawn candidate %s from the ballots."\
                          % self.dirtyB.withdrawn[0]
    else:
      self.dirtyB.withdrawn.sort()
      withdrawnText = "Removed withdrawn candidates %s from the ballots."\
                          % self.dirtyB.joinList(self.dirtyB.withdrawn)
    withdrawnText = textwrap.fill(withdrawnText, width=self.maxWidth)

    return withdrawnText

  def textgenerateHeader(self):

    header = "OpenSTV version %s (http://www.OpenSTV.org/)\n\n" % \
           ("" if self.test else OpenSTV_version)

    # Don't want to modify all of the ref files for this as I'll probably
    # be reworking testing after this release.
    if not self.test:
      header += """\
Suggested donation for using OpenSTV for an election is $50.  Please go to
http://www.OpenSTV.org/donate to donate via PayPal, Google Checkout, or
Amazon Payments.

Certified election reports are also available.  Please go to
http://www.openstv.org/certified-reports for more information.

"""

    if self.dirtyB.getFileName() is not None:
      header += "Loading ballots from file %s.\n" % \
             os.path.basename(self.dirtyB.getFileName())
    header += """\
Ballot file contains %d candidates and %d ballots.
%s
Ballot file contains %d non-empty ballots.

Counting votes for %s using %s.
%d candidates running for %d seat%s.
""" % (self.dirtyB.numCandidates, self.dirtyB.numBallots,
       self.textgenerateWithdrawnText(),
       self.cleanB.numBallots,
       self.e.title, self.e.longMethodName,
       self.cleanB.numCandidates, self.e.numSeats,
       "s" if self.e.numSeats > 1 else ""
       )
    if self.e.optionsMsg != "":
      header += ( textwrap.fill(self.e.optionsMsg, width=self.maxWidth) + "\n" )
    header += ( "\n" )
    return header

  def textsetMinColWidth(self, minColWidth=4):

    # Find the largest value to appear in the report
    mv = 0
    if self.e.methodName == "Condorcet":
      for i in range(self.cleanB.numCandidates):
        m = max(self.e.pMat[i])
        mv = max(m, mv)

    elif self.e.methodName == "Borda":
      m = max(self.e.count)
      mv = max(m, mv)

    elif self.e.iterative:
      if "exhausted" in dir(self.e):
        m = max(self.e.exhausted)
        mv = max(m, mv)
      if "thresh" in dir(self.e):
        m = max(self.e.thresh)
        mv = max(m, mv)
      if "surplus" in dir(self.e):
        m = max(self.e.surplus)
        mv = max(m, mv)
      for i in range(len(self.e.count)):
        m = max(self.e.count[i])
        mv = max(m, mv)

    else:
      if "exhausted" in dir(self.e):
        mv = max(self.e.exhausted, mv)
      if "thresh" in dir(self.e):
        mv = max(self.e.thresh, mv)
      if "surplus" in dir(self.e):
        mv = max(self.e.surplus, mv)
      m = max(self.e.count)
      mv = max(m, mv)

    # From the max value, compute the minimum column width needed
    mv /= self.e.p
    mv = max(mv, minColWidth)
    self.maxColWidth = int(math.floor(math.log10(mv))) + 1
    if self.e.prec > 0:
      self.maxColWidth += self.e.prec + 1
    self.maxColWidth = max(self.maxColWidth, minColWidth)

  def textsetPrintField(self, cw):
    self.prtf = "%" + str(cw) + "." + str(cw) + "s"  # %_._s

  def textgenerateMatrix(self, matrix):
    "Return a matrix in text format."

    nCol = self.cleanB.numCandidates

    txt = self.prtf % ""

    # Candidate names
    for c in range(self.cleanB.numCandidates):
      txt += self.pipify( self.cleanB.names[c] )
    txt += "\n"

    # Separator line
    txt += "-"* self.maxColWidth + ("+" + "-" * self.maxColWidth )*nCol + "\n"

    # For each row, candidate name and matrix values
    for c in range(self.cleanB.numCandidates):
      txt += self.prtf % self.cleanB.names[c]
      for d in range(self.cleanB.numCandidates):
        txt += self.format( matrix[c][d] )
      txt += "\n"

    return txt

  def textgenerateReportCondorcet(self):
    "Generate a text report for an election using Condorcet."

    self.textsetMinColWidth(5)
    self.textsetPrintField(self.maxColWidth)
    text = self.textgenerateHeader()
    report = """\
Pairwise Comparison Matrix:

%s
Smith Set: %s

""" % (self.generateMatrix(self.e.pMat),
       self.cleanB.joinList(self.e.smithSet),
       )
    text += ( report )

    if len(self.e.smithSet) == 1:
      text += ("No completion necessary since the Smith set "\
             "has just one candidate.\n")

    elif self.e.completion == "Schwartz Sequential Dropping":
      completionMsg = """\
Using Schwartz sequential dropping to choose the winner.
Matrix of beatpath magnitudes:

%s%s

""" % (self.generateMatrix(self.e.dMat), self.e.SSDinfo)
      text += (completionMsg)
    elif self.e.completion == "IRV on Smith Set":
      text += ("Using IRV to choose the winner from the Smith set.\n\n")
      R = TextReport(self.e.e, self.maxWidth, "table", outputFile = self.outputFile)
      R.generateReport()
      text += ("\n")
    elif self.e.completion == "Borda on Smith Set":
      text += ("Using the Borda count to choose the winner "\
            "from the Smith set.\n\n")
      R = TextReport(self.e.e, self.maxWidth, "table", outputFile = self.outputFile)
      R.generateReport()
      text += ("\n")
    text += (self.getWinnerText(self.e.winners) + "\n")
    return text

  def textgenerateTextRoundResults(self, R, width, nSubCol):
    self.textprintTableRow( self.getValuesForRound( R ), width, nSubCol)
    ## Print message.
    text = ( "  |" + "-" * (width-3) + "\n")
    line = textwrap.fill(self.e.msg[R], initial_indent="  | ",
                         subsequent_indent="  | ", width=width)
    text += ( line + "\n" )
    return text

  def textgenerateReportNonIterative(self):
    "Pretty print results in text format."

    self.textsetMinColWidth(5)
    self.textsetPrintField(self.maxColWidth)
    text = ''

    # Include summary information for full results.
    if self.style == "full":
      text += self.textgenerateHeader()

    # Find length of longest candidate name
    maxNameLen = 9 # 9 letters in "Exhausted"
    for c in range(self.cleanB.numCandidates):
      maxNameLen = max(maxNameLen, len(self.cleanB.names[c]))

    # Print format strings
    fmt1 = "%" + str(maxNameLen) + "." + str(maxNameLen) + "s"  # %_._s
    fmt2 = "%" + str(self.maxColWidth) + "." + str(self.maxColWidth) + "s"  # %_._s

    # Header
    text += ( (fmt1 + " | " + fmt2 + "\n") % ("Candidate", "Count") )
    text += ("=" * (maxNameLen + 3 + self.maxColWidth) + "\n")

    # Candidate vote totals for the round
    for c in range(self.cleanB.numCandidates):
      text += ( (fmt1 + " | " + fmt2 + "\n") %\
             (self.cleanB.names[c],
              self.e.displayValue(self.e.count[c])))

    # Exhausted ballots
    text += ((fmt1 + " | " + fmt2 + "\n") % ("Exhausted", self.e.exhausted) )

    # Messages
    text += ("\n")
    line = textwrap.fill(self.e.msg, width=self.maxWidth)
    line += "\n\n"
    text += (line)

    # Include winners for full results
    if self.style == "full":
      text += ( self.getWinnerText(self.e.winners, self.maxWidth))

    return text

  def textgenerateReportIterative(self, R=None):
    "Pretty print results in text format."

    self.textsetMinColWidth(5)
    text = ''

    # The following determines the actual column width to use that
    # makes the table ouput compact and evenly distributed

    # nCol is the total number of columns
    nCol = self.cleanB.numCandidates
    nCol += 1 # Exhausted
    if self.e.threshMethod:
      nCol += 1 # Surplus
      nCol += 1 # Thresh
    # maxnSubCol is the maximum number of columns that can fit in a
    # single row.  This is used to determine how many rows we need.
    maxnSubCol = (self.maxWidth-2)/(self.maxColWidth+1)
    # nRow is the number of rows needed to display all of the columns
    (nRow, r) = divmod(nCol, maxnSubCol)
    if r > 0:
      nRow += 1
    # nSubCol is the number of columns per row (distrubted evenly across rows)
    (nSubCol, r) = divmod(nCol, nRow)
    if r > 0:
      nSubCol += 1
    # colWidth is the width of a column in characters
    colWidth = (self.maxWidth-2)/nSubCol - 1
    # width is the actual width of the table
    width = 2 + nSubCol*(colWidth+1)

    # Find length of longest string in the table header
    maxNameLen = 9 # 9 letters in "Exhausted"
    for c in range(self.cleanB.numCandidates):
      maxNameLen = max(maxNameLen, len(self.cleanB.names[c]))

    # Pad strings for table header to a multiple of colWidth
    maxNameLen += colWidth - (maxNameLen % colWidth)
    header = []
    for c in range(self.cleanB.numCandidates):
      header.append(self.cleanB.names[c].ljust(maxNameLen))
    header.append("Exhausted".ljust(maxNameLen))
    if self.e.threshMethod:
      header.append("Surplus".ljust(maxNameLen))
      header.append("Threshold".ljust(maxNameLen))

    # nSubRow is the number of rows needed to display the full candidate names
    (nSubRow, r) = divmod(maxNameLen, colWidth)
    if r > 0:
      nSubRow += 1

    self.textsetPrintField(colWidth)

    # Include summary information for full results.
    if self.style == "full":
      self.textgenerateHeader()

    if self.style in ["full", "table"]:
      # Table header
      for r in range(nRow):
        for sr in range(nSubRow):
          line = ""
          if r == 0 and sr == 0:
            line += " R"
          else:
            line += "  "
          b = sr*colWidth
          e = b + colWidth

          for sc in range(nSubCol):
            h = r*nSubCol + sc
            if h == len(header):
              break
            line += ( "|" + header[h][b:e] )
          text += (line + "\n")

        if r < nRow-1:
          text += ("  |" + ("-" * colWidth + "+" ) *(nSubCol-1) + "-" *colWidth + "\n")

      # Rounds
      for R in range(self.e.numRounds):
        self.textgenerateTextRoundResults(R, width, nSubCol)
      text += ("\n")

    # Include winners for full results
    if self.style == "full":
      text += ( self.getWinnerText(self.e.winners, width) )

    # Generate results for only the specified round
    if self.style == "round":
      self.textgenerateTextRoundResults(R, width, nSubCol)

    # Add optional post-count information
    if len(self.e.msg) > self.e.numRounds:
      text += ("\n\n")
      text += (self.e.msg[self.e.numRounds])

    return text

