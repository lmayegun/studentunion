<?php

namespace Drupal\election_fptp\Plugin\ElectionVotingMethod;

use Drupal\Core\Form\FormStateInterface;

/**
 * Single transferable vote.
 *
 * @ElectionVotingMethod(
 *   id = "block",
 *   label = @Translation("Block voting (multiple non-transferable)"),
 * )
 */
class BlockVoting extends FirstPastThePost {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['maximum_candidates'] = [
      '#type' => 'number',
      '#title' => 'Maximum candidates that can be selected',
      '#min' => 1,
      '#default_value' => $this->configuration['maximum_candidates'],
    ];

    return $form;
  }

}
