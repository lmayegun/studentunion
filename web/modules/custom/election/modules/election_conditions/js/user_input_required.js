/**
 * @file
 * Condition UI behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Provides the summary information for the condition vertical tabs.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the condition summaries.
   */
  Drupal.behaviors.userInputRequired = {
    attach: function () {
      $('#edit-confirm-checkbox').on('change', function () {
        var checked = $('#edit-confirm-checkbox').is(':checked');

        var nominationForm = $('form[id^=election-candidate-]').length > 0;
        var ballotForm = !nominationForm;

        var elements = $('.ballot_intro, #edit-rankings');

        if (ballotForm) {
          if (checked) {
            elements.slideDown('slow');
            $('#edit-confirm').delay(300).slideUp('slow');
            $('#submit-vote').show().prop('disabled', false);
            $('#submit-abstain').show().prop('disabled', false);
          } else {
            elements.hide();
            $('#submit-vote').hide().prop('disabled', true);
            $('#submit-abstain').hide().prop('disabled', true);
          }
        } else if (nominationForm) {
          var elements = $('form[id^=election-candidate-] > div, form[id^=election-candidate-] > details, form[id^=election-candidate-] > fieldset:not(.user_input_required)');
          if (checked) {
            elements.slideDown('slow');
            $('#edit-confirm').delay(300).slideUp('slow');
            $('#edit-actions').show();
            $('#edit-submit').show();
          } else {
            elements.hide();
            $('#edit-confirm').show();
            $('#edit-actions').show();
            $('#edit-submit').hide();
          }
        }
      });
      $('#edit-confirm-checkbox').trigger('change');
    }
  };
}(jQuery, Drupal));
