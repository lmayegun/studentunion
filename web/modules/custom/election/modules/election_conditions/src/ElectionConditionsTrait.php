<?php

namespace Drupal\election_conditions;

use Drupal\complex_conditions\ComplexConditionsEntityTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\election\Entity\Election;

/**
 *
 */
trait ElectionConditionsTrait {

  use ComplexConditionsEntityTrait;

  /**
   *
   */
  public static function addElectionConditionsFields(&$fields, string $entity_type) {
    /**
     * You can have conditions for each of the election phases.
     * You can also choose to share conditions between phases.
     * You can also inherit election conditions for election posts - or not.
     */

    if ($entity_type == 'election') {
      $typeNotice = t('The election conditions can be shared or overridden with any individual posts within the election.');
    }
    else {
      $typeNotice = t('The post will also automatically inherit any conditions set for the election, unless you disable that using the "Inherit election conditions" option.');

      $fields['conditions_inherit_election'] = BaseFieldDefinition::create('list_string')
        ->setLabel(t('Inherit conditions from election'))
        ->setSettings([
          'allowed_values' => [
            'inherit' => 'Inherit election conditions - both election and post conditions will apply',
            'ignore' => 'Ignore election conditions - only post conditions will apply',
          ],
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setRequired(TRUE)
        ->setDefaultValue('inherit')
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }

    $fields['conditions_shared'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Same conditions across all phases'))
      ->setSettings([
        'allowed_values' => [
          'shared' => 'One set of conditions across voting, nominations, etc.',
          'unique' => 'Custom conditions per phase',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('shared')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    foreach (Election::getPhases() as $phase_id => $phase) {
      $name = $phase->label();

      $defaultValue = 'none';
      $allowedValues = [
        'none' => $entity_type == 'election_post' ? 'No post-specific conditions for ' . $name : 'No conditions for ' . $name,
        $phase_id => 'Unique set of ' . ($entity_type == 'election_post' ? 'post ' : '') . ' conditions for ' . $name,
      ];
      foreach (Election::getPhases() as $subphase_id => $subphase) {
        if ($subphase_id == 'nominations') {
          $defaultValue = 'voting';
        }
        $defaultValue = !$defaultValue ? $subphase_id : $defaultValue;
        if ($phase_id != $subphase_id) {
          $allowedValues[$subphase_id] = 'Same as ' . $subphase->label();
        }
      };

      // Conditions will be collected shared automatically across whatever type is selected.
      $fields['conditions_' . $phase_id . '_same_as'] = BaseFieldDefinition::create('list_string')
        ->setLabel((t('@type conditions', ['@type' => $name])))
        ->setDescription($typeNotice)
        ->setSettings([
          'allowed_values' => $allowedValues,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setRequired(TRUE)
        ->setDefaultValue($defaultValue)
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['conditions_' . $phase_id] = BaseFieldDefinition::create('conditions_plugin_item:complex_conditions')
        ->setLabel(t('@name conditions', [
          '@name' => $name,
        ]))
        ->setDescription($typeNotice)
        ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
        ->setDisplayOptions('form', [
          'type' => 'complex_conditions_conditions_table_election',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }
  }

  /**
   *
   */
  public static function addConditionStatesToForm(&$form) {
    foreach (Election::getPhases() as $phase_id => $phase) {
      if ($phase_id == 'voting') {
        // Voting conditions field is visible when either:
        // - conditions_$phase_same_as is set to the phase ID
        // - conditions_shared is set to shared.
        // Basically, if we're not differentiating by phase,
        // We just use the voting conditions
        // Though the user doesn't see any distinction
        // Yes the boolean needs to be like this (repetitive) for States API to work it out.
        $form['conditions_' . $phase_id]['#states'] = [
          'visible' => [
            [
              ':input[name="status_' . $phase_id . '"]' => ['!value' => 'disabled'],
              'and',
              ':input[name="conditions_' . $phase_id . '_same_as"]' => ['value' => $phase_id],
            ],
            'or',
            [
              ':input[name="status_' . $phase_id . '"]' => ['!value' => 'disabled'],
              'and',
              ':input[name="conditions_shared"]' => ['value' => 'shared'],
            ],
          ],
        ];
      }
      else {
        // Actual conditions field for this phase_id is visible when both:
        // - conditions_$phase_id_same_as is set to the phase_id ID
        // - conditions_shared is set to unique.
        $form['conditions_' . $phase_id]['#states'] = [
          'visible' => [
            ':input[name="status_' . $phase_id . '"]' => ['!value' => 'disabled'],
            'and',
            ':input[name="conditions_' . $phase_id . '_same_as"]' => ['value' => $phase_id],
          ],
        ];
      }

      $form['conditions_' . $phase_id . '_same_as']['#states'] = [
        'visible' => [
          ':input[name="conditions_shared"]' => ['value' => 'unique'],
        ],
      ];
    }
  }

}
