<?php

namespace Drupal\election_conditions\Plugin;

use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionBase;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\NominationsPhase;
use Drupal\election\Phase\PhaseInterface;
use Drupal\election\Phase\VotingPhase;

/**
 * Base class for Election post condition plugin plugins.
 */
abstract class ElectionConditionBase extends ComplexConditionBase implements ComplexConditionInterface {

  /**
   * {@inheritdoc}
   */
  public function requiredParameters(): array {
    return [
      'phase' => 'PhaseInterface',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->assertParameters($parameters);
    $requirements = [];

    // This is where you would generate and evaluate your requirements.
    return $requirements;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContextsForEligibility(ElectionPost $post, AccountInterface $account) {
  }

  /**
   * @return [type]
   */
  public function getElectionPostFromContext() {
    $election_post = election_get_election_post_from_context();

    if (!$election_post) {
      return NULL;
    }
    return $election_post;
  }

  /**
   *
   */
  public function alterCandidateForm(array &$form, FormStateInterface $form_state) {
    $this->alterFormForUserInput($form, $form_state, new NominationsPhase());
  }

  /**
   *
   */
  public function alterBallotForm(array &$form, FormStateInterface $form_state) {
    $this->alterFormForUserInput($form, $form_state, new VotingPhase());
  }

  /**
   *
   */
  public function alterFormForUserInput(array &$form, FormStateInterface $form_state, PhaseInterface $phase = NULL) {
  }

  /**
   *
   */
  public function ajaxUserInputRequiredConfirmationChange(array &$form, FormStateInterface $form_state) {
  }

}
