<?php

namespace Drupal\election_conditions\Plugin\ComplexConditions\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionInterface;
use Drupal\election\Entity\Election;
use Drupal\election\Phase\PhaseInterface;

/**
 * Require an action before voting.
 *
 * In this case, a text is shown and checkbox, but could e.g. be a self-definition question.
 *
 * @ComplexCondition(
 *   id = "election_user_input_required",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("User input required"),
 *   display_label = @Translation("User input required"),
 *   category = @Translation("Actions"),
 *   weight = 0,
 * )
 */
class UserInputRequired extends ElectionConditionBase implements ComplexConditionInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'text' => [
        'value' => '',
        'format' => '',
      ],
      'section_header' => 'Confirmation required',
      'checkbox_text' => 'I agree',
      'text_interest' => '',
      'text_nominations' => '',
      'text_voting' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    foreach (Election::getPhases() as $phase_id => $phase) {
      $form['text_' . $phase_id] = [
        '#type' => 'text_format',
        '#title' => $this->t('Text to show above checkbox for ' . $phase->label()),
        '#default_value' => $this->configuration['text_' . $phase_id]['value'] ?? '',
        '#required' => FALSE,
      ];
    }
    $form['checkbox_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text for checkbox'),
      '#default_value' => $this->configuration['checkbox_text'] ?? 'I agree',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['text'] = $values['text'] ?? NULL;
    $this->configuration['checkbox_text'] = $values['checkbox_text'] ?? NULL;
  }

  /**
   *
   */
  public function getPrefixText(PhaseInterface $phase) {
    return $this->configuration['text_' . $phase->id()];
  }

  /**
   *
   */
  public function getCheckboxText(PhaseInterface $phase) {
    return $this->configuration['checkbox_text'];
  }

  /**
   *
   */
  public function alterFormForUserInput(array &$form, FormStateInterface $form_state, PhaseInterface $phase = NULL) {
    $election_post = $this->getElectionPostFromContext();
    if (!$election_post) {
      return;
    }
    $pass = $this->evaluate($election_post, \Drupal::currentUser());
    if (!$pass) {
      $form['#attached']['library'][] = 'election_conditions/user_input_required';

      $form['confirm'] = [
        '#type' => 'fieldset',
        '#attributes' => ['class' => ['user_input_required user_input_required_' . $phase->id() . ' user_input_required_' . $this->getPluginId()]],
        '#weight' => -50,
      ];

      $form['confirm']['confirm_checkbox'] = [
        '#prefix' => $this->getPrefixText($phase),
        '#type' => 'checkbox',
        '#title' => $this->getCheckboxText($phase),
        '#required' => TRUE,
        '#ajax' => [
          // don't forget :: when calling a class method.
          'callback' => '::ajaxUserInputRequiredConfirmationChange',
          // Or TRUE to prevent re-focusing on the triggering element.
          'disable-refocus' => FALSE,
          'event' => 'change',
          // This element is updated with this AJAX callback.
          'wrapper' => 'edit-output',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Loading...'),
          ],
        ],
      ];

      if (isset($form['actions']['skip'])) {
        $form['actions']['skip']['#value'] = t('Skip @name', ['@name' => $election_post->getElectionPostType()->getNaming()]);
      }
    }
  }

}
