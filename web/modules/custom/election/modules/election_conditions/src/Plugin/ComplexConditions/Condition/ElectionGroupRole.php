<?php

namespace Drupal\election_conditions\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\ConditionRequirement;
use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election_conditions\Plugin\ElectionConditionBase;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;

/**
 * Condition.
 *
 * @ComplexCondition(
 *   id = "election_group_role",
 *   condition_types = {
 *     "election",
 *   },
 *   label = @Translation("User has role(s) in group(s)"),
 *   display_label = @Translation("User has role(s) in group(s)"),
 *   category = @Translation("Group memberships"),
 *   weight = 0,
 * )
 */
class ElectionGroupRole extends ElectionConditionBase implements ComplexConditionInterface {
  const TARGET_BUNDLES = NULL;

  public $entity = NULL;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'groups' => [],
      'groups_any_or_all' => 'all',
      'group_roles' => [],
      'group_roles_any_or_all' => 'any',
    ] + parent::defaultConfiguration();
  }

  /**
   *
   */
  public function getGroupRoles($group_types = NULL) {
    $query = \Drupal::entityQuery('group_role');
    $query = $query->condition('internal', 0, '=');
    if ($group_types && count($group_types) > 0) {
      $query = $query->condition('group_type', $group_types, 'IN');
    }
    $ids = $query->execute();
    return \Drupal::entityTypeManager()->getStorage('group_role')->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $groupOptions = [];
    $form['groups'] = [
      '#title' => $this->t('Groups'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'group',
      '#tags' => TRUE,
      '#chosen' => TRUE,
      '#selection_handler' => 'default',
      '#default_value' => $this->getGroupIds() ? Group::loadMultiple($this->getGroupIds()) : NULL,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    if (static::TARGET_BUNDLES) {
      $form['groups']['#selection_settings'] = [
        'target_bundles' => static::TARGET_BUNDLES,
      ];
    }

    $form['groups_any_or_all'] = [
      '#type' => 'select',
      '#title' => $this->t('Require any or all selected groups'),
      '#description' => $this->t('If multiple have been selected.'),
      '#options' => [
        'any' => 'Any',
        'all' => 'All',
      ],
      '#default_value' => $this->configuration['groups_any_or_all'],
    ];

    $roles = $this->getGroupRoles(static::TARGET_BUNDLES ?? NULL);
    $optionsRoles = [];
    foreach ($roles as $role) {
      $optionsRoles[$role->id()] = $role->label();
    }

    $form['group_roles'] = [
      '#title' => $this->t('Group roles'),
      '#type' => 'select',
      '#options' => $optionsRoles,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['group_roles'] ? $this->configuration['group_roles'] : NULL,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    $form['group_roles_any_or_all'] = [
      '#type' => 'select',
      '#title' => $this->t('Require any or all selected roles'),
      '#description' => $this->t('If multiple have been selected.'),
      '#options' => [
        'any' => 'Any',
        'all' => 'All',
      ],
      '#default_value' => $this->configuration['group_roles_any_or_all'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['groups'] = $values['groups'];
    $this->configuration['groups_any_or_all'] = $values['groups_any_or_all'];
    $this->configuration['group_roles'] = $values['group_roles'];
    $this->configuration['group_roles_any_or_all'] = $values['group_roles_any_or_all'];
  }

  /**
   *
   */
  public function getMemberships($account) {
    // Get user's group memberships.
    $membershipService = \Drupal::service('group.membership_loader');
    $memberships = $membershipService->loadByUser($account);
    $groupIds = $this->getGroupIds();
    $final_memberships = [];
    foreach ($memberships as $membership) {

      $group = $membership->getGroup();

      if (!in_array($group->id(), $groupIds)) {
        continue;
      }
      $final_memberships[] = $membership;
    }
    return $final_memberships;
  }

  /**
   *
   */
  public function getGroupIds() {
    return array_column($this->configuration['groups'], 'target_id');
  }

  /**
   * Return array of ConditionRequirement.
   *
   * @return array
   */
  public function evaluateRequirements(EntityInterface $entity, AccountInterface $account, $parameters = []) {
    $this->entity = $entity;

    $this->assertParameters($parameters);
    $requirements = [];

    $groupsToCheck = $this->getGroupIds();

    $memberships = $this->getMemberships($account);

    $rolesInGroups = [];
    foreach ($memberships as $membership) {
      $group = $membership->getGroup();
      $roles = $membership->getRoles();

      if (!isset($rolesInGroups[$group->id()])) {
        $rolesInGroups[$group->id()] = [];
      }
      foreach ($roles as $role) {
        $rolesInGroups[$group->id()][] = $role->id();
      }
    }

    if (!$this->configuration['group_roles_any_or_all']) {
      $this->configuration['group_roles_any_or_all'] = 'all';
    }

    if (!$this->configuration['groups_any_or_all']) {
      $this->configuration['groups_any_or_all'] = 'any';
    }

    $groupsWithRequiredRoles = [];
    foreach ($rolesInGroups as $group => $roles) {
      $configRoles = array_column($this->configuration['group_roles'], 'target_id');
      $intersect = count(array_intersect($roles, $configRoles));
      $any = $intersect > 0;
      $all = $intersect == count($roles);
      $anyMatch = $this->configuration['group_roles_any_or_all'] == 'any' && $any;
      $allMatch = $this->configuration['group_roles_any_or_all'] == 'all' && $all;
      if ($anyMatch || $allMatch) {
        $groupsWithRequiredRoles[] = $group;
      }
    }

    $matches = [];
    $anyPass = FALSE;
    $pass = FALSE;
    foreach (array_keys($rolesInGroups) as $group) {
      $groupMatches = in_array($group, $groupsToCheck);
      if ($this->configuration['groups_any_or_all'] == 'any' && $groupMatches) {
        $pass = TRUE;
        $anyPass = TRUE;
        break;
      }
      if ($this->configuration['groups_any_or_all'] == 'all' && $groupMatches) {
        $matches[] = $group;
      }
    }

    if (!$anyPass) {
      $pass = count(array_intersect($groupsToCheck, $matches)) == count($groupsToCheck);
    }

    // Requirement:
    $requirements = $this->buildRequirements($pass);

    return $requirements;
  }

  /**
   *
   */
  public function getMessage($params) {
    $message = 'User must have ';
    if ($params['@roles_count'] > 1) {
      $message .= $this->configuration['group_roles_any_or_all'] . ' of the required roles (@roles)';
    }
    else {
      $message .= 'the role "@roles"';
    }
    if ($params['@groups_count'] > 1) {
      $message .= ' in ' . $this->configuration['groups_any_or_all'] . ' of these groups: @groups';
    }
    else {
      $message .= ' in the group "@groups"';
    }
    return $message;
  }

  /**
   *
   */
  public function getParams() {
    $groupsToCheck = $this->getGroupIds();
    $groupsNames = [];
    foreach ($groupsToCheck as $groupId) {
      $groupsNames[$groupId] = Group::load($groupId)->label();
    }
    asort($groupsNames);

    $rolesToCheck = $this->configuration['group_roles'] ? array_column($this->configuration['group_roles'], 'target_id') : [];
    $rolesNames = [];
    foreach ($rolesToCheck as $roleId) {
      $role = GroupRole::load($roleId);
      if ($role) {
        $rolesNames[$roleId] = $role->label();
      }
    }
    asort($rolesNames);

    return [
      '@groups' => implode(', ', $groupsNames),
      '@groups_count' => count($groupsNames),
      '@roles' => implode(', ', $rolesNames),
      '@roles_count' => count($rolesNames),
    ];
  }

  /**
   *
   */
  public function buildRequirements($pass) {
    $id = 'group_match_' . uniqid();
    $params = $this->getParams();
    $message = $this->getMessage($params);
    $label = t($message, $params);
    $requirements[$id] = new ConditionRequirement([
      'id' => $id,
      'label' => $label,
      'pass' => $pass,
    ]);
    return $requirements;
  }

}
