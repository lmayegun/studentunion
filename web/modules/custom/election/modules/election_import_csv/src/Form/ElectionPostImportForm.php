<?php

namespace Drupal\election_import_csv\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidateType;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostType;

/**
 * Implement Class ElectionPostImportForm for import form.
 */
class ElectionPostImportForm extends BatchImportCSVForm {

  /**
   * @var null
   */
  public $election = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'election_post_import_form';
  }

  /**
   *
   */
  public static function getMapping(): array {
    $fields = [];
    $fields['Name'] = [
      'description' => 'Post name',
      'required' => TRUE,
    ];
    $fields['Published'] = [
      'description' => 'Yes or No',
      'required' => TRUE,
    ];
    $fields['Vacancies'] = [
      'description' => 'Number of vacancies',
    ];
    $fields['Include RON'] = [
      'description' => 'Include "re-open nominations" as a candidate. Yes or No',
    ];
    $fields['Exclusive'] = [
      'description' => 'Yes or No',
    ];
    $fields['Abstentions'] = [
      'description' => 'Yes or No',
    ];

    $postTypes = ElectionPostType::loadMultiple();
    $postTypeNames = [];
    foreach ($postTypes as $type) {
      $postTypeNames[] = $type->id();
    }
    $fields['Post type'] = [
      'description' => 'One of ' . implode(', ', $postTypeNames),
      'required' => TRUE,
    ];

    $candidateTypes = ElectionCandidateType::loadMultiple();
    $candidateTypeNames = [];
    foreach ($candidateTypes as $type) {
      $candidateTypeNames[] = $type->id();
    }
    $fields['Candidate type'] = [
      'description' => 'One of ' . implode(', ', $candidateTypeNames),
      'required' => TRUE,
    ];

    $fields['Action'] = [
      'description' => t('leave this column off to create, put in "update" to try to update an existing post by name, put in "create with duplicate name" if you get a duplicate name error but want to create the new post anyway.'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {
    $form_state->set('election_id', $election ? $election->id() : NULL);
    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function addExtraColumns($array, $form, $form_state) {
    foreach ($array as $key => $row) {
      $array[$key]['election_id'] = $form_state->get('election_id');
    }
    return $array;
  }

  /**
   *
   */
  public static function validateRow(&$record, array &$context) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function processRow(array $row, array &$context) {
    $election = Election::load($row['election_id']);

    $action = $row['Action'] ?? 'create';

    $required = static::getRequiredFields();
    foreach ($required as $field => $data) {
      if (!isset($row[$field])) {
        static::addError($row, $context, 'Required field not provided: ' . $field);
        return FALSE;
      }
    }

    $validated = static::validateRow($row, $context);
    if (!$validated) {
      return FALSE;
    }

    $existingPost = NULL;
    $existingPostIds = Drupal::entityQuery('election_post')
      ->condition('election', $election->id())
      ->condition('name', $row['Name'])
      ->execute();
    $existingPosts = ElectionPost::loadMultiple($existingPostIds);

    if (count($existingPosts) > 0) {
      if ($action == 'create') {
        static::addError($row, $context, 'Post already exists by that name. Change the Action column value to "create with duplicate name" for this row to override and import anyway.');
        return FALSE;
      }

      if ($action == 'update' && count($existingPosts) > 1) {
        static::addError($row, $context, 'Multiple existing identical posts with that name (' . count($existingPosts) . '), not sure which to update.');
        return FALSE;
      }
      $existingPost = reset($existingPosts);
    }

    if (!$existingPost && $action == 'update') {
      static::addError($row, $context, 'Cannot find existing post to update.');
      return FALSE;
    }

    if (!isset($existingPost) || !$existingPost) {
      $post = static::createPost($election, $row);
      $post = static::afterCreatePost($row, $post);
    }

    return TRUE;
  }

  /**
   *
   */
  public static function createPost($election, $record) {
    $data = [
      'election' => $election->id(),
      'type' => $record['Post type'],
      'name' => $record['Name'],
      'status' => $record['Published'] == 'Yes',
      'vacancies' => isset($record['Vacancies']) ? intval($record['Vacancies']) : 1,
      'include_reopen_nominations' => isset($record['Include RON']) ? $record['Include RON'] == 'Yes' : FALSE,
      'exclusive' => isset($record['Exclusive']) ? $record['Exclusive'] == 'Yes' : FALSE,
      'abstentions_allowed' => isset($record['Abstentions']) ? $record['Abstentions'] == 'Yes' : TRUE,
      'type' => isset($record['Post type']) ? $record['Post type'] : 'default',
      'candidate_type' => isset($record['Candidate type']) ? $record['Candidate type'] : 'default',
    ];

    $post = ElectionPost::create($data);
    $post->save();

    return $post;
  }

  /**
   *
   */
  public static function afterCreatePost($record, $post) {
    return $post;
  }

}
