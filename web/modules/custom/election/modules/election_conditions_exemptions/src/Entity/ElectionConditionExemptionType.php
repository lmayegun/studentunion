<?php

namespace Drupal\election_conditions_exemptions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Election Condition Exemption type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "election_condition_exemp_type",
 *   label = @Translation("Election Condition Exemption type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\election_conditions_exemptions\Form\ElectionConditionExemptionTypeForm",
 *       "edit" = "Drupal\election_conditions_exemptions\Form\ElectionConditionExemptionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\election_conditions_exemptions\ElectionConditionExemptionTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer election condition exemption types",
 *   bundle_of = "election_condition_exemption",
 *   config_prefix = "election_condition_exemp_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/election/exemptions/types/add",
 *     "edit-form" = "/admin/election/exemptions/types/manage/{election_condition_exemp_type}",
 *     "delete-form" = "/admin/election/exemptions/types/manage/{election_condition_exemp_type}/delete",
 *     "collection" = "/admin/election/exemptions/types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class ElectionConditionExemptionType extends ConfigEntityBundleBase {

  /**
   * The machine name of this election condition exemption type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the election condition exemption type.
   *
   * @var string
   */
  protected $label;

}
