<?php

namespace Drupal\election_conditions_exemptions;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of election condition exemption type entities.
 *
 * @see \Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType
 */
class ElectionConditionExemptionTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No election condition exemption types available. <a href=":link">Add election condition exemption type</a>.',
      [':link' => Url::fromRoute('entity.election_condition_exemp_type.add_form')->toString()]
    );

    return $build;
  }

}
