<?php

namespace Drupal\election_conditions_exemptions\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchForm;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemption;
use Drupal\election_conditions_exemptions\Entity\ElectionConditionExemptionType;

/**
 * Class ExemptionDisableForm.
 */
class ExemptionDisableForm extends BatchForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disable_exemptions';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['status'] = [
      '#title' => 'Status to set',
      '#type' => 'select',
      '#options' => [
        '0' => 'Disable',
        '1' => 'Enable',
      ],
      '#default_value' => $_SESSION[$this->getFormId() . '-status'] ?? 0,
      '#weight' => 5,
    ];

    $exemption_types = [];
    $types = ElectionConditionExemptionType::loadMultiple();
    foreach ($types as $type) {
      $exemption_types[$type->id()] = $type->label();
    }
    $form['exemption_type_ids'] = [
      '#title' => 'Exemption types to process',
      '#type' => 'checkboxes',
      '#options' => $exemption_types,
      '#description' => t('Select none to include all exemption types.'),
      '#default_value' => $_SESSION[$this->getFormId() . '-exemption_type_ids'] ?? array_keys($exemption_types),
      '#weight' => 5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $_SESSION[$this->getFormId() . '-status'] = $form_state->getValue('status');
    $_SESSION[$this->getFormId() . '-exemption_type_ids'] = $form_state->getValue('exemption_type_ids');

    $exemption_ids = $this->getExemptions($form, $form_state, $form_state->getValue('exemption_type_ids'));

    $function = 'processOperation';
    $chunk_size = 100;

    // Chunk the array.
    $chunks = array_chunk($exemption_ids, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, $function], [
        [
          'exemption_ids' => $chunk,
          'exemption_type_ids' => count($form_state->getValue('exemption_type_ids')) > 0 ? $form_state->getValue('exemption_type_ids') : [],
          'status' => $form_state->getValue('status'),
        ],
      ]);
    }
  }

  /**
   *
   */
  public function getExemptions($form, $form_state) {
    $query = \Drupal::entityQuery('election_condition_exemption');
    if ($form_state->getValue('exemption_type_ids') && count($form_state->getValue('exemption_type_ids')) > 0) {
      // dd($form_state->getValue('exemption_type_ids'));.
      $query->condition('bundle', $form_state->getValue('exemption_type_ids'), 'IN');
    }
    return $query->execute();
  }

  /**
   *
   */
  public static function processOperation(array $data, array &$context) {
    $exemption_ids = $data['exemption_ids'];
    $exemptions = ElectionConditionExemption::loadMultiple($exemption_ids);
    foreach ($exemptions as $exemption) {
      $exemption->set('status', $data['status'] == '1' ? TRUE : FALSE);
      $exemption->save();
    }
  }

}
