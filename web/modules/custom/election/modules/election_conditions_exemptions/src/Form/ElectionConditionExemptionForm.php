<?php

namespace Drupal\election_conditions_exemptions\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the election condition exemption entity edit forms.
 */
class ElectionConditionExemptionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New election condition exemption %label has been created.', $message_arguments));
      $this->logger('election_conditions_exemptions')->notice('Created new election condition exemption %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The election condition exemption %label has been updated.', $message_arguments));
      $this->logger('election_conditions_exemptions')->notice('Updated new election condition exemption %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.election_condition_exemption.canonical', ['election_condition_exemption' => $entity->id()]);
  }

}
