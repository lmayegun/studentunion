<?php

namespace Drupal\election_conditions_exemptions;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an election condition exemption entity type.
 */
interface ElectionConditionExemptionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the election condition exemption creation timestamp.
   *
   * @return int
   *   Creation timestamp of the election condition exemption.
   */
  public function getCreatedTime();

  /**
   * Sets the election condition exemption creation timestamp.
   *
   * @param int $timestamp
   *   The election condition exemption creation timestamp.
   *
   * @return \Drupal\election_conditions_exemptions\ElectionConditionExemptionInterface
   *   The called election condition exemption entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the election condition exemption status.
   *
   * @return bool
   *   TRUE if the election condition exemption is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the election condition exemption status.
   *
   * @param bool $status
   *   TRUE to enable this election condition exemption, FALSE to disable.
   *
   * @return \Drupal\election_conditions_exemptions\ElectionConditionExemptionInterface
   *   The called election condition exemption entity.
   */
  public function setStatus($status);

}
