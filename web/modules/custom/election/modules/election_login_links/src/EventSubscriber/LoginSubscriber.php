<?php

namespace Drupal\election_login_links\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 *
 */
class LoginSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

  /**
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   */
  public function checkForRedirection(RequestEvent $event) {
    // can't access eligibility.
    return $event;

    // If not logged in by election only:
    if (!isset($_SESSION['election_only']) || !$_SESSION['election_only']) {
      return $event;
    }

    $user = \Drupal::currentUser();

    // Ignore for anonymous and administrator.
    $user = User::load($user->id());
    if ($user->isAnonymous() || $user->id() === '1') {
      return $event;
    }

    // If we're in an election context, we can stay.
    $context_provider = \Drupal::service('election.election_route_context');
    $context_ids = [
      'election',
      'election_post',
    ];
    $contexts = $context_provider->getRuntimeContexts($context_ids);
    foreach ($context_ids as $context_id) {
      if (isset($contexts[$context_id])) {
        if (isset($contexts[$context_id]) && $contexts[$context_id] && $contexts[$context_id]->getContextValue()) {
          return $event;
        }
      }
    }

    $entities = [];
    foreach (\Drupal::routeMatch()->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $entities[$param->getEntityTypeId()] = $param;
        if (in_array($param->getEntityTypeId(), ['election', 'election_post'])) {
          return $event;
        }
      }
    }

    $current_path = \Drupal::service('path.current')->getPath();
    if ($paths = \Drupal::config('election_login_links')->get('additional_paths')) {
      $paths = explode("\r", $paths);
      foreach ($paths as $path) {
        if (\Drupal::service('path.matcher')->matchPath($current_path, $path)) {
          unset($_SESSION['election_only']);
          return $event;
        }
      }
    }

    // Otherwise, log out.
    user_logout();

    // Redirect to login.
    $destination = \Drupal::request()->getRequestUri();

    $loginlink = Url::fromRoute('user.login', [], [
      'absolute' => TRUE,
      'query' => ['destination' => $destination],
    ])->toString();

    \Drupal::moduleHandler()->alter('election_login_links_login_url', $loginlink, $destination, $election);

    $event->setResponse(new TrustedRedirectResponse($loginlink));

    return $event;
  }

}
