<?php

namespace Drupal\election_login_links\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\election\Entity\Election;
use Drupal\election\Phase\VotingPhase;
use Drupal\election_login_links\Entity\ElectionLoginHash;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for login.
 */
class LoginController extends ControllerBase implements ContainerInjectionInterface {

  /**
   *
   */
  public function login($election, string $hash_string) {
    // See if there's a hash to use:
    $hashes = \Drupal::entityQuery('election_login_hash')
      ->condition('hash', $hash_string)
      ->condition('election_id', $election)
      ->sort('expiry', 'ASC')
      ->accessCheck(FALSE)
      ->execute();

    foreach ($hashes as $hash) {
      $hash = ElectionLoginHash::load($hash);

      if (1 == 2 && $hash->used->value == 1) {
        // If it's already been used...
        return $this->redirectUsed($hash);
      }
      elseif ($hash->expiry->value < strtotime('now')) {
        // If it's expired:
        return $this->redirectExpired($hash);
      }
      else {
        $hash->used->value = 1;
        $hash->save();

        if (\Drupal::currentUser()->isAnonymous()) {
          $this->logUserIn($hash);
        }
        else {
          // Already logged in.
          $message = \Drupal::config('election_login_links.settings')->get('message_already_logged_in');
          if ($message != '' && !$message) {
            $message = 'Hi @username. You were already logged in - the one-time login link used has been disabled.';
          }
          if ($message != '') {
            \Drupal::messenger()->addWarning(new FormattableMarkup($message, ['@username' => \Drupal::currentUser()->getDisplayName()]));
          }
        }
        return $this->redirectHash($hash);
      }
    }

    // If no hash, ask user to login before redirecting.
    if ($election) {
      $election = Election::load($election);
      $url_string = Url::fromRoute($this->getDestinationRoute($election), [
        'election' => $election->id(),
        'election_id' => $election->id(),
      ])->toString();
      return $this->redirectToLogin($url_string, $election);
    }

    // No hash and no election? Just go to elections list...
    return $this->redirect('entity.election.collection');
  }

  /**
   *
   */
  public function redirectToLogin($destination, $election) {
    $url = Url::fromRoute('user.login', [], [
      'absolute' => TRUE,
      'query' => [
        'destination' => $destination,
      ],
    ]);
    $url_string = $url->toString();

    \Drupal::moduleHandler()->alter('election_login_links_login_url', $url_string, $destination, $election);

    // $url->toString counts as a render so TrustedRedirectResponse is throwing a cache metadata leak error.
    // https://drupal.stackexchange.com/questions/187086/trustedresponseredirect-failing-how-to-prevent-cache-metadata
    // @todo fix
    // return new TrustedRedirectResponse($url_string);
    $response = new Response('', 302, []);
    $response->targetUrl = $url_string;
    $response->setContent(sprintf('<!DOCTYPE html>
      <html>
          <head>
              <meta charset="UTF-8" />
              <meta http-equiv="refresh" content="0;url=%1$s" />

              <title>Redirecting to %1$s</title>
          </head>
          <body>
              Redirecting to <a href="%1$s">%1$s</a>.
          </body>
      </html>', htmlspecialchars($url_string, ENT_QUOTES, 'UTF-8')));
    $response->headers->set('Location', $url_string);
    return $response;
  }

  /**
   *
   */
  public function redirectHash($hash) {
    $loggedIn = \Drupal::currentUser()->isAuthenticated();

    if ($loggedIn) {
      $postID = $hash->getElection()->getNextPostId(\Drupal::currentUser(), NULL, [], new VotingPhase(), TRUE, NULL, TRUE);
      if ($postID) {
        return $this->redirect('entity.election_post.voting', [
          'election_post' => $postID,
        ]);
      }

      return $this->redirect('entity.election.canonical', [
        // Return $this->redirect($this->getDestinationRoute($hash->getElection()), [.
        'election' => $hash->getElectionId(),
        'election_id' => $hash->getElectionId(),
      ]);
    }

    $hashDestination = Url::fromRoute($this->getDestinationRoute($hash->getElection()), [
      'election' => $hash->getElectionId(),
      'election_id' => $hash->getElectionId(),
    ])->toString();

    return $this->redirectToLogin($hashDestination, $hash->getElection());
  }

  /**
   *
   */
  public function logUserIn($hash) {
    user_login_finalize($hash->getOwner());
    $_SESSION['election_only'] = $hash->getElectionId();

    $message = \Drupal::config('election_login_links.settings')->get('message_logged_in_success');
    if (!$message) {
      $message = 'Hi @username! You\'ve been logged in with a one-time login link for this election. For security reasons, if you go to other pages on the website, you may need to log in again.';
    }
    if ($message != '') {
      \Drupal::messenger()->addMessage(new FormattableMarkup($message, ['@username' => \Drupal::currentUser()->getDisplayName()]));
    }
  }

  /**
   *
   */
  public function redirectUsed($hash) {
    $message = \Drupal::config('election_login_links.settings')->get('message_already_used');
    if ($message != '' && !$message) {
      $message = 'This one-time login link has already been used.';
    }
    if ($message != '') {
      \Drupal::messenger()->addError(new FormattableMarkup($message, ['@username' => \Drupal::currentUser()->getDisplayName()]));
    }
    return $this->redirectHash($hash);
  }

  /**
   *
   */
  public function redirectExpired($hash) {
    $message = \Drupal::config('election_login_links.settings')->get('message_expired');
    if ($message != '' && $message != '' && !$message) {
      $message = 'This one-time login link has expired.';
    }
    if ($message != '') {
      \Drupal::messenger()->addError(new FormattableMarkup($message, ['@username' => \Drupal::currentUser()->getDisplayName()]));
    }
    return $this->redirectHash($hash);
  }

  /**
   * @todo make configurable.
   *
   * @return string
   */
  public function getDestinationRoute($election) {
    if ($election->isOpenOrPartiallyOpen(new VotingPhase())) {
      return 'entity.election.voting';
    }
    else {
      return 'entity.election.canonical';
    }
  }

}
