<?php

namespace Drupal\election_login_links\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election_eligibility_export\Form\ExportEligibleUsers;
use Drupal\election_login_links\Entity\ElectionLoginHash;

/**
 * Class GenerateLinksForm.
 */
class GenerateLinksForm extends ExportEligibleUsers {

  public $expiry = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'generate_links_form';
  }

  /**
   *
   */
  public function getBatchTitle($form, $form_state) {
    if ($form_state->get('type') == 'delete') {
      return $this->t('Deleting links...');
    }
    return $this->t('Generating links...');
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadTitle($form, $form_state) {
    $name = '';
    if ($form_state->get('election_id')) {
      $election = Election::load($form_state->get('election_id'));
      $name = $election->label() . ' (' . $election->id() . ')';
    }
    $date = \Drupal::service('date.formatter')->format(strtotime('now'), 'short');
    return "Election login links " . ($name ? ' - ' . $name : '') . ' - ' . $date;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {

    $form = parent::buildForm($form, $form_state, $election);

    $form_state->set('election_id', $election->id());

    // How many links currently available at what expiry times - markup.
    $hashes = \Drupal::entityQuery('election_login_hash')
      ->condition('election_id', $election->id())
      ->condition('used', 0)
      ->condition('expiry', strtotime('now'), '>')
      ->execute();
    $count = count($hashes);

    $form['markup'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#markup' => $this->t('You can export a list of all users matching your specified criteria alongside a link that they can use to log in once. The link takes them to "start voting" if voting is open, or to the election page if not. For security, this link only allows them access to this one election - if they navigate elsewhere in the site they will be logged out. Note this does not filter by eligibility.'),
      '#suffix' => '</p>',
      '#weight' => -50,
    ];
    $form['settings'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#markup' => $this->t('You can configure messages and allowed paths at the <a href="@settings_link">Configuration page</a> for this module.', [
        '@settings_link' => Url::fromRoute('election_login_links.settings')->toString(),
      ]),
      '#suffix' => '</p>',
      '#weight' => -50,
    ];
    $form['info_existing'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#markup' => $this->t('Current one-time login links generated and available for this election: <b>@count</b>', ['@count' => $count]),
      '#suffix' => '</p>',
      '#weight' => -49,
    ];
    if ($count > 0) {
      $form['info_existing_warning'] = [
        '#type' => 'markup',
        '#prefix' => '<p>',
        '#markup' => $this->t('<b>Generating new links will not cancel existing ones. Clear all links below before re-generating if you want to invalidate previous links.</b> Any cancelled links will still point to the election, they will just require manual login.', ['@count' => $count]),
        '#suffix' => '</p>',
        '#weight' => -48,
      ];
    }

    $form['expiry'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Expiry'),
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime('+1 month')),
      '#weight' => -48,
    ];

    $form['phase']['#access'] = FALSE;
    $form['category']['#access'] = FALSE;

    // Actions.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Download list of one-time login links',
      '#button_type' => 'primary',
      '#submit' => [
        '::getEligible',
      ],
    ];

    if (count($hashes) > 0) {
      $form['actions']['submit_delete'] = [
        '#type' => 'submit',
        '#value' => 'Cancel all existing login links',
        '#button_type' => 'danger',
        '#submit' => [
          '::deleteAll',
        ],
      ];
    }

    return $form;
  }

  /**
   *
   */
  public function getEligible(array &$form, FormStateInterface $form_state) {
    $form_state->set('type', 'eligible');
    parent::submitForm($form, $form_state);
  }

  /**
   *
   */
  public function deleteAll(array &$form, FormStateInterface $form_state) {
    $form_state->set('type', 'delete');
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $form_state->setValue('phase', 'voting');
    $this->expiry = $form_state->getValue('expiry');

    if ($form_state->get('type') == 'delete') {
      $data = \Drupal::entityQuery('election_login_hash')
        ->condition('election_id', $form_state->get('election_id'))
        ->condition('used', 0)
        ->execute();

      $function = 'processDeletions';
      $chunk_size = 10;
      // Chunk the array.
      $chunks = array_chunk($data, $chunk_size, TRUE);
      foreach ($chunks as $chunk) {
        $this->addOperation($batch_builder, [$this, $function], [$chunk]);
      }
    }
    else {
      parent::setOperations($batch_builder, $form, $form_state);
    }
  }

  /**
   *
   */
  public function processDeletions($rows, array &$context) {
    foreach ($rows as $row) {
      $hash = ElectionLoginHash::load($row);
      $hash->delete();
    }
  }

  /**
   *
   */
  public function generateLine($account, $election, array $phases, $category = NULL, $post_type_ids = NULL) {
    // Never create for admin or anonymous:
    if ($account->id() < 2 || $account->hasRole('administrator')) {
      return NULL;
    }

    // $line = parent::generateLine($account, $election, Election::getPhases());
    $line = [
      'User ID' => $account->id(),
      'User name' => $account->getDisplayName(),
      'User e-mail' => $account->getEmail(),
      'include' => TRUE,
    ];

    $expiry = $this->expiry->getTimestamp();
    $line['One-time login link'] = $this->generateLink($account, $election, $expiry);
    $line['Link expiry'] = \Drupal::service('date.formatter')->format($expiry, 'custom', 'Y-m-d H:i:s');
    if (isset($line['One-time login link']) && $line['One-time login link']) {
      return $line;
    }
    return NULL;
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\election\Entity\ElectionInterface $election
   * @param int $timestamp
   *
   * @return [type]
   */
  public function generateLink(AccountInterface $account, ElectionInterface $election, int $timestamp) {
    $hash = ElectionLoginHash::create([
      'expiry' => $timestamp,
      'used' => 0,
    ]);
    $hash->setOwner($account);
    $hash->setElection($election);
    $hash->setHash();
    $hash->save();
    return $hash->getLink();
  }

}
