<?php

namespace Drupal\election_login_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ElectionSettingsForm.
 *
 * @ingroup election
 */
class ElectionSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'election_login_links.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'election_login_links_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getvalues();

    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('additional_paths', $values['additional_paths'])
      ->set('message_logged_in_success', $values['message_logged_in_success'])
      ->set('message_already_logged_in', $values['message_already_logged_in'])
      ->set('message_already_used', $values['message_already_used'])
      ->set('message_leaves_election', $values['message_leaves_election'])
      ->set('message_expired', $values['message_expired'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Election entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['message_logged_in_success'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user on successful login.'),
      '#description' => $this->t('Use @username to include the user display name.'),
      '#default_value' => $config->get('message_logged_in_success') ?? 'Hi @username! You\'ve been logged in with a one-time login link for this election. For security reasons, if you go to other pages on the website, you may need to log in again.',
    ];

    $form['message_already_used'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user when link has already been used.'),
      '#default_value' => $config->get('message_already_used') ?? 'This one-time login link has already been used.',
    ];

    $form['message_already_logged_in'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user when already logged in when trying to use link.'),
      '#description' => $this->t('Use @username to include the user display name.'),
      '#default_value' => $config->get('message_already_logged_in') ?? 'Hi @username. You were already logged in - the one-time login link used has been disabled.',
    ];

    $form['message_expired'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user when the link is already expired.'),
      '#default_value' => $config->get('message_expired') ?? 'This one-time login link has expired.',
    ];

    $form['message_leaves_election'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user when they leave the election area and get logged out.'),
      '#default_value' => $config->get('message_leaves_election') ?? 'You were logged in via an election one-time login link, but have visited a non-election page. Please log in again to access other pages.',
    ];

    $form['additional_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional paths where user can remain logged in (one to each line)'),
      '#description' => $this->t('By default, users will need to log in manually if they access a page outside the election\'s context. Enter additional paths here that the user can remain logged in on (e.g. help pages etc). You can use wildcards (e.g. articles/election-*).'),
      '#default_value' => $config->get('additional_paths'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
