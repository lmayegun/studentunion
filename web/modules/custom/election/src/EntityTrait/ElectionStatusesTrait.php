<?php

namespace Drupal\election\EntityTrait;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\election\Entity\Election;
use Drupal\election\Phase\PhaseInterface;
use Drupal\election\Service\ElectionPostEligibilityChecker;
use Drupal\views\Views;

/**
 *
 */
trait ElectionStatusesTrait {

  /**
   * @todo validate scheduling
   */
  public static function addElectionStatusesFields(&$fields, string $entity_type) {
    // Create nominations and voting status and opening and closing times.
    $weightCounter = 0;
    foreach (Election::getPhases() as $phase_id => $phase) {

      $weightCounter++;

      $defaultValue = 'closed';

      $allowedValues = [
        'closed' => 'Closed',
        'open' => 'Open',
        'scheduled' => 'Scheduled',
        'disabled' => 'Disabled for this post',
      ];

      if ($entity_type == 'election_post') {
        $allowedValues = [
          'inherit' => 'Inherit from election',
        ] + $allowedValues;
        $defaultValue = 'inherit';
      }
      else {
        if ($phase == 'interest') {
          $defaultValue = 'disabled';
        }
      }

      if ($phase_id == 'voting') {
        unset($allowedValues['disabled']);
      }

      $fields['status_' . $phase_id] = BaseFieldDefinition::create('list_string')
        ->setLabel(t($phase->label() . ' status'))
        ->setDescription(t('Whether @name is open, closed or scheduled.', ['@name' => $phase->label()]))
        ->setSettings([
          'allowed_values' => $allowedValues,
        ])
        ->setDefaultValue($defaultValue)
        ->setDisplayOptions('view', [
          'region' => 'hidden',
          'label' => 'inline',
          'type' => 'string',
        ])
        ->setRequired(TRUE)
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => $weightCounter,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $scheduling_statuses = Election::SCHEDULING_STATES;
      foreach ($scheduling_statuses as $scheduling_status) {
        $fields['status_' . $phase_id . '_' . $scheduling_status] = BaseFieldDefinition::create('datetime')
          ->setLabel(t($phase->label() . ' ' . $scheduling_status . ' time'))
          ->setDescription(t(
            'Date and time to @status @name (if scheduled)',
            [
              '@status' => $scheduling_status,
              '@name' => strtolower($phase->label()),
            ]
          ))
          ->setSettings([
            'datetime_type' => 'datetime',
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
            'region' => 'hidden',
            'label' => 'inline',
            'type' => 'datetime_default',
            'settings' => [
              'format_type' => 'medium',
            ],
          ])
          ->setDisplayOptions('form', [
            'type' => 'datetime_default',
            'weight' => $weightCounter + 1,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);
      }
    }
  }

  /**
   *
   */
  public static function addStatusesStatesToForm(&$form) {
    $scheduling_statuses = Election::SCHEDULING_STATES;
    foreach (Election::getPhases() as $phase_id => $phase) {
      foreach ($scheduling_statuses as $scheduling_status) {
        $form['status_' . $phase_id . '_' . $scheduling_status]['#states'] = [
          'visible' => [
            ':input[name="status_' . $phase_id . '"]' => ['value' => 'scheduled'],
          ],
        ];
      }
    }
  }

  /**
   *
   */
  public function checkStatusForPhase(PhaseInterface $phase, $checkStatus = 'open') {
    $status = $this->get('status_' . $phase->id())->value;

    if ($status == 'inherit') {
      $status = $this->getElection()->get('status_' . $phase->id())->value;
    }

    if ($status == $checkStatus) {
      return TRUE;
    }
    elseif ($status != $checkStatus) {
      return FALSE;
    }
    elseif ($checkStatus == 'open' && $status == 'scheduled') {
      $start = $this->get('status_' . $phase->id() . '_open')->value;
      $end = $this->get('status_' . $phase->id() . '_close')->value;
      return $this->checkNowIsBetweenDates($start, $end);
    }
  }

  /**
   * @param PhaseInterface $phase
   *
   * @return string
   *   One of: open, scheduled, over
   */
  public function checkScheduledState(PhaseInterface $phase) {
    $status = $this->get('status_' . $phase->id())->value;

    if ($status == 'inherit') {
      $status = $this->getElection()->get('status_' . $phase->id())->value;
    }

    if ($status != 'scheduled') {
      return NULL;
    }

    $start = $this->get('status_' . $phase->id() . '_open')->date->getTimestamp();
    $end = $this->get('status_' . $phase->id() . '_close')->date->getTimestamp();

    $now = strtotime('now');

    if ($this->checkNowIsBetweenDates($start, $end)) {
      return 'open';
    }

    if ($end < $now) {
      return 'over';
    }

    if ($start >= $now) {
      return 'scheduled';
    }

    return 'scheduled';
  }

  /**
   *
   */
  public function checkNowIsBetweenDates($datetimeStart, $datetimeEnd) {
    return strtotime('now') >= $datetimeStart && strtotime('now') <= $datetimeEnd;
  }

  /**
   * @return array
   */
  public function getEnabledPhases() {
    $election = $this->getEntityTypeId() == 'election_post' ? $this->getElection() : $this;
    $finalPhases = [];
    foreach (Election::getPhases() as $phase_id => $phase) {
      $field = 'status_' . $phase_id;
      if ($election->$field->value != 'disabled') {
        $finalPhases[$phase_id] = $phase;
      }
    }
    return $finalPhases;
  }

  /**
   *
   */
  public function checkIfStatusMeansOpen($status) {
    return $status == 'open' || $status == 'open with scheduled close' || $status == 'open as scheduled' || $status == 'closed but posts open';
  }

  /**
   *
   */
  public function isOpenOrPartiallyOpen(PhaseInterface $phase = NULL, $inherit = TRUE) {
    $statuses = $this->getPhaseStatuses($inherit);
    if ($phase) {
      return $this->checkIfStatusMeansOpen($statuses[$phase->id()]);
    }
    else {
      foreach ($statuses as $phase_id => $status) {
        if ($this->checkIfStatusMeansOpen($status)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  public function getPhaseStatus($phase, $inherit = TRUE) {
    $statuses = $this->getPhaseStatuses($inherit);
    return $statuses[$phase] ?? NULL;
  }

  /**
   *
   */
  public function getOpenPhases($inherit = TRUE) {
    $statuses = $this->getPhaseStatuses($inherit);
    $open = [];
    foreach ($statuses as $phase => $text) {
      if ($this->checkIfStatusMeansOpen($text)) {
        $open[] = $phase;
      }
    }
    return $open;
  }

  /**
   *
   * @param  $inherit
   *
   * @return array
   *   Array for each phase with one of:
   *     - open
   *     - closed
   *     - scheduled
   *     - open as scheduled
   *     - open with scheduled close
   *     - over
   */
  public function getPhaseStatuses($inherit = TRUE) {
    $results = [];

    $phases = $this->getEnabledPhases();

    foreach ($phases as $phase_id => $phase) {
      if ($inherit && $this->getEntityTypeId() == 'election_post' && $this->get('status_' . $phase_id)->value == 'inherit') {
        $electionResults = $this->getElection()->getPhaseStatuses();
        $results[$phase_id] = $electionResults[$phase_id];
      }
      else {
        $status = $this->get('status_' . $phase_id)->value;
        $text = $status;

        switch ($status) {
          case 'open':
            $text = 'open';
            break;

          case 'closed':
            $text = 'closed';
            break;

          case 'scheduled':
            $dateStatus = $this->checkScheduledState($phase);

            if ($dateStatus == 'open') {
              $end = $this->get('status_' . $phase_id . '_close')->date->getTimestamp();
              if ($end > strtotime('now')) {
                $text = 'open with scheduled close';
              } 
              else {
                $text = 'open as scheduled';
              }
            } 
            elseif ($dateStatus == 'scheduled') {
              $text = 'scheduled';
            } 
            else {
              $text = 'over';
            }
            break;
        }

        $results[$phase_id] = $text;
      }
    }

    return $results;
  }

  /**
   *
   */
  public function getVotingLink() {
    return Url::fromRoute(
      'entity.' . $this->getEntityTypeId() . '.voting',
      [$this->getEntityTypeId() == 'election' ? 'election_id' : $this->getEntityTypeId() => $this->id()]
    )->toString();
  }

  /**
   *
   */
  public function getVotingLinkViaLogin() {
    $destination = $this->getVotingLink();
    return Url::fromRoute('user.login', [], [
      'absolute' => TRUE,
      'query' => ['destination' => $destination],
    ])->toString();
  }

  /**
   *
   */
  public function getUserEligibilityInformation(AccountInterface $account, array $phases = NULL, $refresh = FALSE) {
    // Sort out caching.
    $cid_components = [
      'getUserEligibilityInformation',
      $this->getEntityTypeId(),
      $this->id(),
      $account->id(),
      $phases ? substr(hash('md5', implode("", array_keys($phases))), 0, 10) : '',
    ];
    $cid = implode(':', $cid_components);

    // Return cache data if we have it and we're not refreshing:
    if (!$refresh && $cache = \Drupal::cache('election_eligibility')->get($cid)) {
      $result = $cache->data;
      return $result;
    }

    $result = [];

    $debug = FALSE;

    if ($this->getEntityTypeId() == 'election') {
      $election = $this;
    }
    else {
      $election = $this->getElection();
    }

    $phaseStatuses = $this->getPhaseStatuses();
    $electionPhases = $election->getEnabledPhases();

    if ($phases) {
      $phase_ids = array_intersect(array_keys($electionPhases), array_keys($phases));
    }
    else {
      $phase_ids = array_keys($electionPhases);
    }

    $phases = Election::getPhases($phase_ids);

    $electionPhaseStatuses = [];
    if ($this->getEntityTypeId() == 'election_post') {
      $electionPhaseStatuses = $this->getElection()->getPhaseStatuses();
    }

    foreach ($phases as $phase_id => $phase) {

      $result[$phase_id] = [
        'status' => $phaseStatuses[$phase_id],
        'name' => $phase->label(),
        'eligible' => FALSE,
        'already_' . $phase_id => FALSE,
        'ineligibility_reasons' => [],
        'user_input_required' => FALSE,
        'eligibility_link' => NULL,
        'eligibility_label' => NULL,
      ];

      if ($phaseStatuses[$phase_id] == 'inherit') {
        $phaseStatuses[$phase_id] = $electionPhaseStatuses[$phase_id];
      }

      $open = FALSE;
      switch ($phaseStatuses[$phase_id]) {
        case 'open':
          $open = TRUE;
          $status_full = '@phase open';
          break;

        case 'open with scheduled close':
          $open = TRUE;
          $time = $this->get('status_' . $phase_id . '_close')->value;
          $time = strtotime($time);
          $howLong = '';
          if ($time) {
            $howLong = \Drupal::service('date.formatter')->format($time, 'medium');
          }
          $status_full = '@phase open (scheduled to close ' . $howLong . ')';
          break;

        case 'closed':
          $status_full = '@phase closed';
          break;

        case 'closed but posts open':
          $status_full = '@phase closed for election overall but some @posts are open';
          break;

        case 'scheduled':
        case 'scheduled but posts open':
          $time = $this->get('status_' . $phase_id . '_open')->value;
          $time = strtotime($time);
          $howLong = '';
          if ($time) {
            $howLong = \Drupal::service('date.formatter')->format($time, 'medium');
          }
          $status_full = '@phase scheduled to open ' . $howLong . '';
          break;

        case 'over':
        case 'over but posts open':
          $time = $this->get('status_' . $phase_id . '_close')->value;
          $time = strtotime($time);
          $howLong = '';
          if ($time) {
            $howLong = \Drupal::service('date.formatter')->format($time, 'medium');
          }
          $status_full = '@phase closed ' . $howLong . '';
          break;

        case 'disabled':
          $status_full = '@phase disabled for this election';
          break;

        default:
          $status_full = '@phase not clear - ' . $phaseStatuses[$phase_id];
          break;
      }

      if ($this->getEntityTypeId() == 'election') {
        // $this->getPosts(TRUE);
        $posts = [];
      }
      else {
        $posts = [$this];
      }

      $result[$phase_id]['open'] = $open;

      // Links if eligible.
      if ($phase_id == 'voting') {
        if (\Drupal::currentUser()->isAnonymous()) {
          $result[$phase_id]['eligibility_link'] = $this->getVotingLinkViaLogin();
          $result[$phase_id]['eligibility_label'] = t('log in to vote now');
        }
        else {
          $result[$phase_id]['eligibility_link'] = $this->getVotingLink();
          $result[$phase_id]['eligibility_label'] = t('vote now');
        }
      }

      if ($this->getEntityTypeId() == 'election') {
        $result[$phase_id]['eligible'] = $open;
      }
      elseif (count($posts) == 0) {
        $result[$phase_id]['eligible'] = FALSE;
        $status_full = 'No published posts.';
      }
      else {
        foreach ($posts as $post) {
          $eligibilityChecker = new ElectionPostEligibilityChecker($account, $post, $phase);
          $eligibilityChecker->setRefresh($debug);
          $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
          $requirements = $eligibilityChecker->evaluateEligibilityRequirements(FALSE);

          if (!$eligibilityChecker->checkRequirementsForEligibility($requirements)) {
            $result[$phase_id]['eligible'] = FALSE;
            $result[$phase_id]['eligibility_label'] = t('Not eligible to @action', [
              '@action' => $phase->getAction(FALSE, TRUE),
            ]);

            $notAlreadyCompleted = $requirements['not_already_' . $phase_id] ?? FALSE;
            $result[$phase_id]['already_' . $phase_id] = isset($requirements['not_already_' . $phase_id]) && $notAlreadyCompleted->isFailed();

            $formattedFailedRequirements = $post->formatEligibilityRequirements($requirements, TRUE);
            $result[$phase_id]['ineligibility_reasons'] = array_column($formattedFailedRequirements, 'title');
          }
          else {
            $result[$phase_id]['eligible'] = TRUE;

            if (!$eligibilityChecker->checkRequirementsForEligibility($requirements, FALSE)) {
              $result[$phase_id]['user_input_required'] = TRUE;
            }

            if ($this->getEntityTypeId() == 'election_post') {
              $candidateTypeId = $this->getCandidateTypeId();
              if ($candidateTypeId) {
                $result[$phase_id]['eligibility_link'] = Url::fromRoute('entity.election_post.' . $phase_id, [
                  'election_post' => $this->id(),
                  'election_candidate_type' => $candidateTypeId,
                ])->toString();
                $result[$phase_id]['eligibility_label'] = t('@action', ['@action' => $phase->getAction(FALSE, TRUE)]);
              }
            }
          }
        }
      }

      // Custom outputs:
      $eligibleText = '';
      if (isset($result[$phase_id]['already_' . $phase_id]) && $result[$phase_id]['already_' . $phase_id]) {
        $status_full = 'You have already ' . $phase->getAction(TRUE, TRUE);
        $result[$phase_id]['ineligibility_reasons'] = [];
      }
      elseif ($phaseStatuses[$phase_id] == 'open' || $phaseStatuses[$phase_id] == 'open with scheduled close') {
        if ($result[$phase_id]['eligible']) {
          if ($phaseStatuses[$phase_id] == 'open with scheduled close') {
            $status_full = '@phase open';
          }

          $eligibleText = $result[$phase_id]['user_input_required'] ? ' pending your confirmation on ballot' : ' and you are eligible';
        }
        else {
          $eligibleText = ' but you are not eligible';
        }
      }
      else {
        // $eligibleText = isset($result[$phase]['eligible']) && $result[$phase]['eligible'] ? ' though you are eligible' : ' and you are not eligible';
        $eligibleText = '';
      }

      $result[$phase_id]['status_full'] = t($status_full . '@eligible', [
        '@phase' => $phase->label(),
        '@eligible' => $eligibleText,
        '@posts' => 'posts',
      ]);
    }

    $tags = $this->getEligibilityCacheTags($account, $phase);

    \Drupal::cache('election_eligibility')->set($cid, $result, Cache::PERMANENT, $tags);

    return $result;
  }

  /**
   *
   */
  public function getUserEligibilityCacheTag(AccountInterface $account = NULL, PhaseInterface $phase = NULL) {
    return implode(':', [
      'election',
      'eligibility',
      $this->getEntityTypeId(),
      $this->id(),
      $account ? $account->id() : '',
      $phase->id,
    ]);
  }

  /**
   *
   */
  public function getEligibilityCacheTagsMultiplePhases(AccountInterface $account, array $phases) {
    $tags = [];
    foreach ($phases as $phase) {
      $tags = array_merge($tags, $this->getEligibilityCacheTags($account, $phase));
    }
    return $tags;
  }

  /**
   *
   */
  public function getEligibilityCacheTags(AccountInterface $account, PhaseInterface $phase) {
    $eligibilityCacheTags = [
      $this->getUserEligibilityCacheTag(NULL, $phase),
    ];

    $eligibilityCacheTag = array_merge($this->getCacheTags(), $eligibilityCacheTags);

    // Future plan is to have one tag, and manually invalidate it
    // By detecting changes to the election, post, candidates etc that might affect it
    // But otherwise not invalidating it
    // Which is excessive but will alow for much fewer eligibility checks
    // On large elections.
    // Entity tags.
    $otherTags = [];

    if ($this->getEntityTypeId() == 'election_post') {
      $otherTags = $this->getElection()->getEligibilityCacheTags($account, $phase);

      if ($this->candidate_type->entity) {
        // @todo target this on entity change
        $otherTags[] = 'election_candidate_list:' . $this->candidate_type->entity->id();
      }

      // Get conditions and add relevant tags from there.
      $conditions = $this->getConditions($phase);
      foreach ($conditions as $condition) {
        $otherTags = array_merge($otherTags, $condition->getCacheTags($this, $account));
      }
    }

    return Cache::mergeTags($eligibilityCacheTag, $otherTags);
  }

  /**
   * Produces a nicely formatted string explaining the user's eligibility.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return [type]
   */
  public function getUserEligibilityFormatted(AccountInterface $account, array $phases = NULL, bool $refresh = FALSE, $separator = ', ', $format = 'simple', $includeLinks = FALSE) {
    if (count($phases) == 0) {
      $phases = NULL;
    }

    $eligibility = $this->getUserEligibilityInformation($account, $phases, $refresh);

    if ($format == 'links_only') {
      $includeLinks = TRUE;
    }

    $groupedPhases = [];

    $orderOfPhases = ['open', 'open with scheduled close', 'open as scheduled', 'scheduled', 'closed'];

    $results = [];
    foreach ($eligibility as $phase => $data) {
      if ($phases && !in_array($phase, $phases)) {
        continue;
      }
      if (!isset($groupedPhases[$data['status']])) {
        $groupedPhases[$data['status']] = [];
      }
      $groupedPhases[$data['status']][$phase] = $data;
    }

    // For simple, only show the primary phase you care about:
    if ($format == 'simple') {
      foreach ($orderOfPhases as $phase) {
        if (!isset($groupedPhases[$phase])) {
          continue;
        }

        if (count($groupedPhases[$phase]) > 0) {
          $groupedPhases = [$phase => $groupedPhases[$phase]];
        }
      }
    }

    foreach ($orderOfPhases as $phase) {
      if (!isset($groupedPhases[$phase])) {
        continue;
      }

      foreach ($groupedPhases[$phase] as $groupedPhase) {
        $string = '';
        if ($format != 'links_only') {
          $string = $groupedPhase['status_full'];
        }
        if ($includeLinks && $groupedPhase['eligible'] && $groupedPhase['status'] == 'open' && isset($groupedPhase['eligibility_link'])) {
          $string .= ' <a class="btn button" href="' . $groupedPhase['eligibility_link'] . '">' . $groupedPhase['eligibility_label'] . '</a>';
        }
        $results[] = $string;
      }
    }
    $results = array_filter($results);
    return implode($separator, $results);
  }

  /**
   *
   */
  public function validateScheduling(&$form, &$form_state) {
    foreach (Election::getPhases() as $phase_id => $phase) {
      if (isset($form['status_' . $phase_id]) && $form['status_' . $phase_id] == 'scheduled') {
        $phaseName = $phase->label();

        $phase_id_prior = $phase->dependentPhase;

        $opens = $form_state->getValue('status_' . $phase_id . '_open');
        $closes = $form_state->getValue('status_' . $phase_id . '_close');

        if (!$opens || !$closes) {
          $form_state->setErrorByName(
            'status_' . $phase_id,
            $this->t(
              'You must set the opening and closing times when the status is Scheduled for @phaseName.',
              [
                '@phaseName' => $phaseName,
              ]
            )
          );
        }
        elseif ($closes <= $opens) {
          $form_state->setErrorByName(
            'status_' . $phase_id . '_close',
            $this->t(
              'You must set the closing time to be after the opening time when the status is Scheduled for @phaseName.',
              [
                '@phaseName' => $phaseName,
              ]
            )
                  );
        }
        elseif ($form_state->getValue('status_' . $phase_id_prior) == 'scheduled') {
          $priorCloses = $form_state->getValue('status_' . $phase_id . '_close');
          if ($priorCloses > $opens) {

            $form_state->setErrorByName(
              'status_' . $phase_id_prior . '_close',
              $this->t(
                '@priorPhaseName can only be scheduled so that they close before the start of @phaseName.',
                [
                  '@phaseName' => $phaseName,
                  '@priorPhaseName' => $phase_id_prior,
                ]
              )
            );
          }
        }
      }
    }
  }

  /**
   *
   */
  public function resetEligibility(AccountInterface $account = NULL, $phases = [], $booleanOnly = FALSE) {
    if (!$phases || count($phases) == 0) {
      $phases = Election::getPhases();
    }

    if (!$account) {
      return;
    }

    if ($this->getEntityTypeId() == 'election_post') {
      foreach ($phases as $phase) {
        // AccountInterface $account, ElectionPostInterface $election_post, string $phase, $includePhaseStatus = FALSE, $refresh = FALSE, $ignoreRequireUserInput = FALSE, $booleanOnly = FALSE) {.
        $eligibilityChecker = new ElectionPostEligibilityChecker($account, $this, $phase);
        $eligibilityChecker->setIncludePhaseStatus(TRUE);
        $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
        $eligible = $eligibilityChecker->isEligible();

        if (!$booleanOnly) {
          $requirements = $eligibilityChecker->evaluateEligibilityRequirements();
        }

        unset($eligible);
        unset($requirements);
      }
    }
  }

  /**
   * @param PhaseInterface $phase
   */
  public function onPhaseOpenOrClose(PhaseInterface $phase) {
    // Reset any eligibility for this phase:
    Cache::invalidateTags([
      $this->getUserEligibilityCacheTag(NULL, $phase),
      'election:' . $this->getElection()->id() . ':votes',
      'election:' . $this->getElection()->id() . ':votes:count',
      'election:' . $this->getElection()->id() . ':posts',
      'election:' . $this->getElection()->id() . ':posts:count',
      'election:' . $this->getElection()->id() . ':candidates',
      'election:' . $this->getElection()->id() . ':candidates:count',
      'election:' . $this->getElection()->id() . ':ballots',
      'election:' . $this->getElection()->id() . ':ballots:count',
    ]);

    // Election posts view:
    $view = Views::getView('election_posts_for_election_page');
    $view->storage->invalidateCaches();
  }

  /**
   * Show the published checkbox if you're editing.
   *
   * @todo work out how to do this normally - no idea how.
   *
   * @param mixed $form
   *
   * @return [type]
   */
  public static function showPublishedCheckboxIfPermission(&$form) {
    if (!$form['status']['#access']) {
      $form['status']['#access'] = TRUE;
      // $form['status']['#disabled'] = TRUE;
    }
  }

  /**
   * Return number of voters.
   *
   * @param  $confirmedOnly
   *
   * @return [type]
   */
  public function countVoters($confirmedOnly = TRUE) {
    $query = \Drupal::database()->select('election_ballot', 'eb');
    $query->condition('confirmed', 1);
    $query->condition($this->getEntityTypeId(), $this->id());
    $query->groupBy('user_id');
    return $query->countQuery()->execute()->fetchField();
  }
  
}
