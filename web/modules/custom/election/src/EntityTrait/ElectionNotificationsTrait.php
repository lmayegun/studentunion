<?php

namespace Drupal\election\EntityTrait;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Render\Markup;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionCandidateInterface;
use Drupal\election\Phase\PhaseInterface;

/**
 *
 */
trait ElectionNotificationsTrait {

  /**
   * @todo validate scheduling
   */
  public static function addElectionNotificationsFields(&$fields, string $entity_type) {
    // Create nominations and voting status and opening and closing times.
    $tokens = [
      'user name' => '[current-user:name]',
      'election name' => '[election:name]',
      'election URL' => '[election:url:absolute]',
      'post name' => '[election_post:name]',
      'post URL' => '[election_post:url:absolute]',
      'candidate name (interest/nominations only)' => '[election_candidate:name]',
      'candidate URL (interest/nominations only)' => '[election_candidate:url:absolute]',
    ];
    $tokensList = 'You can use these tokens for substitution: ';
    foreach ($tokens as $name => $token) {
      $tokensList .= $name . ' ' . $token . ' • ';
    }

    foreach (Election::getPhases() as $phase_id => $phase) {
      $defaultValue = 'closed';

      if ($entity_type == 'election_post') {
      }
      else {
      }

      $fields['notification_' . $phase_id . '_message'] = BaseFieldDefinition::create('text_long')
        ->setLabel(
          t('Message shown to user after they have @pastTense', [
            '@pastTense' => $phase->getAction(TRUE, TRUE),
          ])
        )
        ->setDescription(t(
          'Leave blank to not show. @tokensList',
          ['@tokensList' => $tokensList]
        ))
        ->setDisplayOptions('form', [
          'type' => 'text_textarea',
          'weight' => 0,
          'rows' => 6,
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);

      $fields['notification_' . $phase_id . '_email_subject'] = BaseFieldDefinition::create('text')
        ->setLabel(
          t('Subject for e-mail user after they have @pastTense.', [
            '@pastTense' => $phase->getAction(TRUE, TRUE),
          ])
        )
        ->setDescription(t(
          'Leave blank to not send. @tokensList',
          ['@tokensList' => $tokensList]
        ))
        ->setDisplayOptions('form', [
          'type' => 'text',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);

      $fields['notification_' . $phase_id . '_email'] = BaseFieldDefinition::create('text_long')
        ->setLabel(
          t('Message e-mailed to user after @pastTense.', [
            '@pastTense' => $phase->getAction(TRUE, TRUE),
          ])
        )
        ->setDescription(t(
          'Leave blank to not show. @tokensList',
          ['@tokensList' => $tokensList]
        ))
        ->setDisplayOptions('form', [
          'type' => 'text_textarea',
          'weight' => 0,
          'rows' => 6,
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);

      $fields['notification_' . $phase_id . '_url'] = BaseFieldDefinition::create('string')
        ->setLabel(
          t('Page to redirect to after @pastTense', [
            '@pastTense' => $phase->getAction(TRUE, TRUE),
          ])
        )
        ->setDescription(t('Leave blank to not redirect. This will override the message above.'))
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setDefaultValue('')
        ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'string',
          'weight' => -50,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -50,
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
    }
  }

  /**
   *
   */
  public static function addNotificationsStatesToForm(&$form) {
    $scheduling_statuses = Election::SCHEDULING_STATES;
    $fields = ['message', 'url', 'email'];
    foreach (Election::getPhases() as $phase_id => $phase) {
      foreach ($fields as $field) {
        $form['notification_' . $phase_id . '_' . $field]['#states'] = [
          'visible' => [
            ':input[name="status_' . $phase_id . '"]' => ['!value' => 'disabled'],
          ],
        ];
      }
    }
  }

  /**
   *
   */
  public function getMessageData(PhaseInterface $phase) {
    $field = 'notification_' . $phase->id . '_message';
    $message = $this->hasField($field) ? $this->get($field)->getValue()[0]['value'] : NULL;

    if (!$message && $this->getEntityTypeId() == 'election_post') {
      $election = $this->getElection();
      $message = $election->getMessageData($phase);
    }

    if (!$message) {
      return NULL;
    }

    return $message;
  }

  /**
   *
   */
  public function getMessageMarkup(PhaseInterface $phase, ElectionCandidateInterface $election_candidate = NULL) {
    $message = $this->getMessageData($phase);
    $message = $this->applyTokens($message, $election_candidate);
    $message = Markup::create($message);
    return $message;
  }

  /**
   *
   */
  public function getEmailData(PhaseInterface $phase) {
    $subjectField = 'notification_' . $phase->id() . '_email_subject';
    $subject = $this->hasField($subjectField) ? $this->get($subjectField)->getValue()[0]['value'] : NULL;

    $bodyField = 'notification_' . $phase->id() . '_email';
    $body = $this->hasField($bodyField) ? $this->get($bodyField)->getValue()[0]['value'] : NULL;

    if (!$subject || !$body) {
      return NULL;
    }

    return [
      'subject' => $subject,
      'body' => $body,
    ];
  }

  /**
   *
   */
  public function applyTokens($string, ElectionCandidateInterface $election_candidate = NULL) {
    $token_service = \Drupal::token();
    $tokenParams = [
      'election_candidate' => $election_candidate,
      'election' => $this->getElection(),
    ];
    if ($this->getEntityTypeId() == 'election_post') {
      $tokenParams['election_post'] = $this;
    }
    return $token_service->replace($string, $tokenParams);
  }

  /**
   *
   */
  public function applyTokensToEmail(array $message, ElectionCandidateInterface $election_candidate = NULL) {
    $message = [
      'subject' => $this->applyTokens($message['subject'], $election_candidate),
      'body' => $this->applyTokens($message['body'], $election_candidate),
    ];
    return $message;
  }

  /**
   *
   */
  public function sendEmail(string $to, PhaseInterface $phase, ElectionCandidateInterface $election_candidate = NULL) {
    if ($this->getEntityTypeId() == 'election') {
      return;
    }

    if (!$to) {
      return;
    }

    $message = $this->getEmailData($phase);

    if (!$message && $this->getEntityTypeId() == 'election_post') {
      $election = $this->getElection();
      $message = $election->getEmailData($phase);
    }

    if (!$message) {
      return;
    }

    $message = $this->applyTokensToEmail($message, $election_candidate);

    // Send a message.
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

    // @todo customise FROM address
    $mailManager = \Drupal::service('plugin.manager.mail');
    $mailManager->mail('election', 'election.notify_user', $to, $lang, $message, NULL, TRUE);

    return;
  }

  /**
   *
   */
  public function getPostRedirect(PhaseInterface $phase) {
    $field = 'notification_' . $phase->id() . '_url';
    $url = $this->hasField($field) ? $this->get($field)->getValue()[0]['value'] : NULL;
    return $url;
  }

}
