<?php

namespace Drupal\election\Entity;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\election\EntityTrait\ElectionNotificationsTrait;
use Drupal\election_conditions\ElectionConditionsTrait;
use Drupal\election\EntityTrait\ElectionStatusesTrait;
use Drupal\election\Phase\InterestPhase;
use Drupal\election\Phase\NominationsPhase;
use Drupal\election\Phase\PhaseInterface;
use Drupal\election\Phase\VotingPhase;
use Drupal\election\Service\ElectionPostEligibilityChecker;
use Drupal\user\UserInterface;

/**
 * Defines the Election entity.
 *
 * @ingroup election
 *
 * @ContentEntityType(
 *   id = "election",
 *   label = @Translation("Election"),
 *   label_collection = @Translation("Elections"),
 *   label_singular = @Translation("election"),
 *   label_plural = @Translation("elections"),
 *   label_count = @PluralTranslation(
 *     singular = "@count election",
 *     plural = "@count elections"
 *   ),
 *   bundle_label = @Translation("Election type"),
 *   handlers = {
 *     "storage" = "Drupal\election\Storage\ElectionStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\election\EntityList\ElectionListBuilder",
 *     "views_data" = "Drupal\election\Entity\ElectionViewsData",
 *     "translation" = "Drupal\election\Translation\ElectionTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\election\Form\ElectionForm",
 *       "add" = "Drupal\election\Form\ElectionForm",
 *       "edit" = "Drupal\election\Form\ElectionForm",
 *       "delete" = "Drupal\election\Form\ElectionDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\election\Routing\ElectionHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\election\Access\ElectionAccessControlHandler",
 *   },
 *   base_table = "election",
 *   bundle_entity_type = "election_type",
 *   data_table = "election_field_data",
 *   revision_table = "election_revision",
 *   revision_data_table = "election_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer elections",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/election/{election}",
 *     "add-page" = "/election/add",
 *     "add-form" = "/election/add/{election_type}",
 *     "edit-form" = "/election/{election}/edit",
 *     "delete-form" = "/election/{election}/delete",
 *     "version-history" = "/election/{election}/revisions",
 *     "revision" = "/election/{election}/revisions/{election_revision}/view",
 *     "revision_revert" = "/election/{election}/revisions/{election_revision}/revert",
 *     "revision_delete" = "/election/{election}/revisions/{election_revision}/delete",
 *     "translation_revert" = "/election/{election}/revisions/{election_revision}/revert/{langcode}",
 *     "collection" = "/admin/election/list",
 *   },
 *   field_ui_base_route = "entity.election_type.edit_form",
 *   permission_granularity = "bundle",
 * )
 */
class Election extends EditorialContentEntityBase implements ElectionInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  use ElectionStatusesTrait;
  use ElectionConditionsTrait;
  use ElectionNotificationsTrait;

  /**
   * The phases an election post goes through.
   */
  const ELECTION_PHASES = [
    'interest',
    'nominations',
    'voting',
  ];

  /**
   *
   */
  public static function getPhases($ids = NULL) {
    $phases = [
      'interest' => new InterestPhase(),
      'nominations' => new NominationsPhase(),
      'voting' => new VotingPhase(),
    ];
    if (!$ids) {
      return $phases;
    }

    $return = [];
    foreach ($phases as $id => $phase) {
      if (in_array($id, $ids)) {
        $return[$id] = $phase;
      }
    }
    return $return;
  }

  /**
   *
   */
  public static function getPhaseClass(string $phase) {
    return static::getPhases()[$phase];
  }

  const SCHEDULING_STATES = [
    'open',
    'close',
  ];

  /**
   *
   */
  public static function getStatusName($status) {
    switch ($status) {
      case 'open':
        return t('Open');

      case 'close':
        return t('Closed');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the election owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * Get user-friendly name for type.
   *
   * @param bool $capital
   *   Start with a capital letter.
   * @param bool $plural
   *   PLuralise.
   *
   * @return string
   *   The user-friendly name.
   */
  public function getTypeNaming($capital = FALSE, $plural = FALSE) {
    return ElectionType::load($this->bundle())->getNaming($capital, $plural);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Election entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 50,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Election entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -50,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description of election'))
      ->setDescription(t('Full text information about the election.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -45,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -49,
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']
      ->setDescription(t('A checkbox indicating whether the election is published. If the election is unpublished no voting or nomination functionality will be available, regardless of nomination or voting status for the election or any posts.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -48,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    static::addElectionStatusesFields($fields, 'election');

    $fields['ballot_behaviour'] = BaseFieldDefinition::create('list_string')
      ->setLabel('Ballot behaviour')
      ->setDescription(t('What happens after voting for an individual position.'))
      ->setSettings([
        'allowed_values' => [
          'one_by_one' => 'Return to position list',
          'next_eligible' => 'Jump to next eligible position in election',
        ],
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('next_eligible')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @todo make view respect this
    $fields['ballot_candidate_sort'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Ballot candidate sort'))
      ->setSettings([
        'allowed_values' => [
          'random' => 'Random',
          'alphabetical' => 'Alphabetical by name',
        ],
      ])
      ->setDefaultValue('random')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ballot_confirmation'] = BaseFieldDefinition::create('list_string')
      ->setLabel('Ballot confirmation')
      ->setDescription(t(''))
      ->setSettings([
        'allowed_values' => [
          'none' => 'None (ballot is confirmed by submitting)',
          'after_post' => 'Users must confirm when submitting ballot',
          'after_election' => 'Users must confirm all votes at end of voting process for them to count',
        ],
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('none')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ballot_show_candidates_below'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show candidates list at end of ballot'))
      ->setDescription(t('Displayed in the "Ballot - full" display mode for the election post.'))
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    static::addElectionConditionsFields($fields, 'election');

    static::addElectionNotificationsFields($fields, 'election');

    $fields['voting_method'] = BaseFieldDefinition::create('plugin_reference')
      ->setLabel(t('Voting method'))
      ->setDescription(t('This can be overridden per post.'))
      ->setSettings([
        'target_type' => 'election_voting_method',
      ])
      ->setDisplayOptions('form', [
        'type' => 'plugin_reference_select',
        'configuration_form' => 'full',
        'provider_grouping' => FALSE,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['allow_candidate_editing'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Allow candidates to edit their nomination'))
      ->setDescription(t('The candidate user must have the "edit own" permission for the candidate type, regardless of this setting. This can be overridden per post.'))
      ->setSettings([
        'allowed_values' => [
          'false' => t('Never'),
          'true' => t('Always'),
          'nominations' => t('Only while nominations are open'),
          'no_votes' => t('Only while no votes have been cast'),
          'custom' => t('Custom code (hook_alter)'),
        ],
      ])
      ->setDefaultValue('false')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['allow_candidate_withdrawal'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Allow candidates to withdraw their nomination'))
      ->setDescription(t('Note this does not delete the candidacy, it just unpublishes it and sets its status to "Withdrawn". This setting can be overridden per post.'))
      ->setSettings([
        'allowed_values' => [
          'false' => t('Never'),
          'true' => t('Always'),
          'nominations' => t('Only while nominations are open'),
          'no_votes' => t('Only while no votes have been cast'),
        ],
      ])
      ->setDefaultValue('false')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ballot_intro'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Ballot introduction text'))
      ->setDescription(t('Shown at top of ballot. Can be overridden per post.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -45,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -49,
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * @param  $only_published
   * @param string $sort
   *   name, random, or.
   *
   * @return [type]
   */
  public function getPostIds($only_published = TRUE, $sort = NULL, array $post_type_ids = NULL) {
    $query = \Drupal::entityQuery('election_post');
    $query->condition('election', $this->id());

    if ($only_published) {
      $query->condition('status', 1);
    }

    if ($post_type_ids) {
      $query->condition('type', $post_type_ids, 'IN');
    }

    if ($sort) {
      if ($sort == 'name') {
        $query->sort('name', 'ASC');
      }
      else {
        $query->sort('weight', 'ASC');

        $pseudorandom_within_weight = TRUE;
        if ($pseudorandom_within_weight) {
          // Pseudorandom sorting within the weight.
          // @todo work out another way in entity query to have a secondary random sort.
          $sort = rand(0, 1) ? 'ASC' : 'DESC';
          $fields = ['name', 'user_id', 'id', 'created', 'changed'];
          $field = isset($fields[rand(0, count($fields) - 1)]) ? $fields[rand(0, count($fields) - 1)] : 'name';
          $query->sort($field, $sort);
        }
        else {
          $query->sort('name', 'ASC');
        }
      }
    }

    $ids = $query->execute();

    if ($sort && $sort == 'random') {
      shuffle($ids);
    }

    return $ids;
  }

  /**
   * @return ElectionType
   */
  public function getElectionType() {
    $type = $this->bundle();
    return ElectionType::load($type);
  }

  /**
   *
   */
  public function getPostTypesAsLabel($capital = TRUE, $plural = TRUE) {
    $types = [];

    // Disabling because it shatters performance in large elections:
    // $post_ids = $this->getPostIds();
    // $posts = ElectionPost::loadMultiple($post_ids);
    // if (count($posts) > 0) {
    //   foreach ($posts as $post) {
    //     if (!isset($types[$post->bundle()])) {
    //       $types[$post->bundle()] = ElectionPostType::load($post->bundle())->getNaming($capital, $plural);
    //     }
    //   }
    // } else {.
    $election_type = ElectionType::load($this->bundle());
    foreach ($election_type->getAllowedPostTypes() as $post_type) {
      $types[$post_type->id()] = $post_type->getNaming($capital, $plural);
    }

    // }
    $types = array_unique($types);
    $result = implode('/', $types);
    return $result;
  }

  /**
   *
   */
  public static function getEligibilityOrder() {
    // @todo get this from overall settings.
    return [
      'Voting open and you are eligible',
      'Voting open pending your confirmation on ballot',
      'Nominations open and you are eligible',
      'Nominations open pending your confirmation on ballot',
      'You have already voted or abstained',
    ];
  }

  /**
   *
   */
  public function getAllEligiblePostIds(AccountInterface $account = NULL) {
    $postIds = $this->getPostIds(TRUE);

    if ($account) {
      $eligiblePostIds = [];
      foreach ($postIds as $postId) {
        $eligibilityChecker = new ElectionPostEligibilityChecker($account, ElectionPost::load($postId), new VotingPhase());
        $eligibilityChecker->setIncludePhaseStatus(TRUE);
        $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
        if ($eligibilityChecker->isEligible()) {
          $eligiblePostIds[] = $postId;
        }
      }

      $postIds = $eligiblePostIds;
    }

    return $postIds;
  }

  /**
   * {@inheritdoc}
   */
  public function getNextPostId(AccountInterface $account = NULL, ElectionPostInterface $current = NULL, array $alreadyDoneOrSkippedIds = NULL, PhaseInterface $phase = NULL, $includePhaseStatus = TRUE, array $post_type_ids = NULL, bool $refresh = FALSE) {
    $postIds = $this->getPostIds(TRUE, 'weight', $post_type_ids);

    if (!$phase) {
      return NULL;
    }

    $currentId = $current ? [$current->id()] : [];

    if (!$alreadyDoneOrSkippedIds) {
      $alreadyDoneOrSkippedIds = array_merge($_SESSION[$this->id() . '_done'] ?? [], $_SESSION[$this->id() . '_skipped'] ?? []);
    }

    if (!$postIds) {
      return NULL;
    }

    $postIds = array_diff($postIds, $alreadyDoneOrSkippedIds, $currentId);
    if (!$postIds || count($postIds) == 0) {
      return NULL;
    }

    foreach ($postIds as $postId) {
      $eligibilityChecker = new ElectionPostEligibilityChecker($account, ElectionPost::load($postId), $phase);
      $eligibilityChecker->setRefresh($refresh);
      $eligibilityChecker->setIncludePhaseStatus($includePhaseStatus);
      $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
      $eligible = $eligibilityChecker->isEligible();
      if ($eligible) {
        // dd($postId);
        return $postId;
      }
    }

    return NULL;
  }

  /**
   *
   */
  public function can(PhaseInterface $phase, AccountInterface $account) {
    return $this->getNextPostId($account, NULL, NULL, $phase) ? TRUE : FALSE;
  }

  /**
   *
   */
  public function canExpressInterest(AccountInterface $account) {
    return $this->can(new InterestPhase(), $account);
  }

  /**
   *
   */
  public function canNominate(AccountInterface $account) {
    return $this->can(new NominationsPhase(), $account);
  }

  /**
   *
   */
  public function canVote(AccountInterface $account) {
    return $this->can(new VotingPhase(), $account);
  }

  /**
   *
   */
  public function getActionLinks(AccountInterface $account) {
    $actions = [];

    // Link to login if logged out.
    if (\Drupal::currentUser()->isAnonymous()) {

      $votingOpen = $this->isOpenOrPartiallyOpen(new VotingPhase());

      $nominationsOpen = $this->isOpenOrPartiallyOpen(new NominationsPhase());

      if ($votingOpen || $nominationsOpen) {
        $title = $votingOpen ? t('Log in to vote') : t('Log in to nominate');
        // Go straight to start voting if logging in to vote; go back to this page otherwise.
        if ($votingOpen) {
          $destination = Url::fromRoute(
            'entity.election.voting',
            ['election_id' => $this->id()]
          )->toString();
        }
        else {
          $destination = \Drupal::request()->getRequestUri();
        }

        $url = Url::fromRoute('user.login', [], [
          'absolute' => TRUE,
          'query' => ['destination' => $destination],
        ])->toString();

        $actions[] = [
          'title' => $title,
          'link' => $url,
          'button_type' => 'primary',
        ];
      }
    }
    else {
      $startVoting = $this->canVote($account);

      if ($startVoting) {
        $actions[] = [
          'title' => t('Start voting'),
          'link' => Url::fromRoute('entity.election.voting', ['election_id' => $this->id()])->toString(),
          'button_type' => 'primary',
        ];
      }

      if ($account->hasPermission('add election posts')) {
        foreach ($this->getElectionType()->getAllowedPostTypes() as $election_post_type) {
          $url = Url::fromRoute('entity.election_post.add_to_election', [
            'election' => $this->id(),
            'election_post_type' => $election_post_type->id(),
          ]);
          if ($url) {
            $actions[] = [
              'title' => t(
                '@label',
                [
                  '@label' => $election_post_type->getActionNaming(),
                ]
              ),
              'link' => $url->toString(),
              'button_type' => 'primary',
            ];
          }
        }
      }
    }

    return $actions;
  }

  /**
   *
   */
  public function getRedirectToNextPost($postID = NULL, $refresh = FALSE) {
    $account = \Drupal::currentUser();
    $alreadyDoneOrSkippedIds = array_merge($_SESSION[$this->id() . '_done'] ?? [], $_SESSION[$this->id() . '_skipped'] ?? []);

    if (!$postID) {
      $postID = $this->getNextPostId($account, NULL, $alreadyDoneOrSkippedIds, new VotingPhase(), TRUE, NULL, $refresh);
    }

    if ($postID) {
      return [
        'route_name' => 'entity.election_post.voting',
        'route_parameters' => [
          'election_post' => $postID,
        ],
        'message' => NULL,
        'post_id' => $postID,
      ];
    }
    else {
      if (\Drupal::currentUser()->isAnonymous()) {
        $message = t(
          'You need to log in to vote.',
        );
      }
      else {
        $message = t(
          'No more %positions available to vote for.@votingOpen',
          [
            '%positions' => 'positions',
            '@votingOpen' => (!$this->isOpenOrPartiallyOpen(new VotingPhase) ? new FormattableMarkup(' Voting is currently closed - come back when it opens!', []) : ''),
          ],
              );
      }

      return [
        'route_name' => 'entity.election.canonical',
        'route_parameters' => [
          'election' => $this->id(),
        ],
        'post_id' => NULL,

        // @todo more informative, i.e. say how many voted for, how many ineligible for, how many closed
        'message' => $message,
      ];
    }
  }

  /**
   *
   */
  public function countCandidatesForVoting($post_ids = NULL) {
    if (!$post_ids) {
      $post_ids = $this->getPostIds(FALSE);
    }
    $query = \Drupal::entityQuery('election_candidate');
    $query->condition('status', 1);
    $query->condition('candidate_status', 'hopeful')
      ->condition('election_post', $post_ids, 'IN');
    return $query->count()->execute();
  }

  /**
   *
   */
  public function countBallots($confirmedOnly = FALSE) {
    $query = \Drupal::database()->select('election_ballot', 'eb');
    if ($confirmedOnly) {
      $query->condition('confirmed', 1);
    }
    $query->condition($this->getEntityTypeId(), $this->id());
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   *
   */
  public function getVoterIds($confirmedOnly = TRUE) {
    $query = \Drupal::database()->select('election_ballot', 'eb');
    $query->fields('eb', ['user_id']);
    if ($confirmedOnly) {
      $query->condition('confirmed', 1);
    }
    $query->condition($this->getEntityTypeId(), $this->id());
    $uids = $query->execute()->fetchCol();
    return $uids;
  }

  /**
   *
   */
  public function getVotingMethod() {
    return $this->voting_method->referencedPlugins()[0];
  }

  /**
   *
   */
  public static function getPublishedElections() {
    $query = \Drupal::entityQuery('election');
    $query->condition('status', 1);
    $ids = $query->execute();
    $elections = Election::loadMultiple($ids);
    return $elections;
  }

  /**
   *
   */
  public static function getOpenElections() {
    $query = \Drupal::entityQuery('election');
    $query->condition('status', 1);
    $ids = $query->execute();
    $elections = [];
    foreach ($ids as $id) {
      $election = Election::load($id);
      if ($election->isOpenOrPartiallyOpen()) {
        $elections[] = $election;
      }
    }
    return $elections;
  }

  /**
   *
   */
  public function getElection() {
    return $this;
  }

  /**
   *
   */
  public function getCandidateIds(array $statuses = [], $only_published = TRUE, AccountInterface $account = NULL) {
    $query = \Drupal::entityQuery('election_post')
      ->condition('election', $this->id());
    $post_ids = $query->execute();

    $query = \Drupal::entityQuery('election_candidate');
    $query->condition('election_post', $post_ids, 'IN');

    if ($statuses && count($statuses) > 0) {
      $query->condition('candidate_status', $statuses, 'IN');
    }

    if ($only_published) {
      $query->condition('status', 1);
    }

    if ($account) {
      $query->condition('user_id', $account->id());
    }

    return $query->execute();
  }

  /**
   *
   */
  public function userHasCompletedPhase(AccountInterface $account, PhaseInterface $phase) {
    $already = NULL;
    switch ($phase->id()) {
      case 'interest':
        $already = $this->interestExists($account);
        break;

      case 'nominations':
        $already = $this->nominationExists($account);
        break;

      case 'voting':
        $already = $this->ballotExists($account);
        break;
    }
    return $already;
  }

  /**
   *
   */
  public function ballotExists($account) {
    $ballots = ElectionBallot::loadIdsByUserAndElection($account, $this);
    return count($ballots) > 0;
  }

  /**
   *
   */
  public function nominationExists($account) {
    $nominations = ElectionCandidate::loadIdsByUserAndElection($account, $this, ['hopeful']);
    return count($nominations) > 0;
  }

  /**
   *
   */
  public function interestExists($account) {
    $nominations = ElectionCandidate::loadIdsByUserAndElection($account, $this, ['interest']);
    return count($nominations) > 0;
  }

}
