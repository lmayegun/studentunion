<?php

namespace Drupal\election\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\election\EntityTrait\ElectionNotificationsTrait;
use Drupal\election_conditions\ElectionConditionsTrait;
use Drupal\election\EntityTrait\ElectionStatusesTrait;
use Drupal\election\Phase\NominationsPhase;
use Drupal\election\Phase\PhaseInterface;
use Drupal\election\Phase\VotingPhase;
use Drupal\user\UserInterface;
use Drupal\election\Service\ElectionPostEligibilityChecker;

/**
 * Defines the Election post entity.
 *
 * @ingroup election
 *
 * @ContentEntityType(
 *   id = "election_post",
 *   label = @Translation("Election post"),
 *   label_collection = @Translation("Election posts"),
 *   label_singular = @Translation("election post"),
 *   label_plural = @Translation("election posts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count election post",
 *     plural = "@count election posts"
 *   ),
 *   bundle_label = @Translation("Election post type"),
 *   handlers = {
 *     "storage" = "Drupal\election\Storage\ElectionPostStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\election\EntityList\ElectionPostListBuilder",
 *     "views_data" = "Drupal\election\Entity\ElectionPostViewsData",
 *     "translation" = "Drupal\election\Translation\ElectionPostTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\election\Form\ElectionPostForm",
 *       "add" = "Drupal\election\Form\ElectionPostForm",
 *       "edit" = "Drupal\election\Form\ElectionPostForm",
 *       "delete" = "Drupal\election\Form\ElectionPostDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\election\Routing\ElectionPostHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\election\Access\ElectionPostAccessControlHandler",
 *   },
 *   base_table = "election_post",
 *   bundle_entity_type = "election_post_type",
 *   data_table = "election_post_field_data",
 *   revision_table = "election_post_revision",
 *   revision_data_table = "election_post_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   translatable = TRUE,
 *   admin_permission = "administer election posts",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/election/post/{election_post}",
 *     "add-page" = "/election/post/add",
 *     "add-form" = "/election/post/add/{election_post_type}",
 *     "edit-form" = "/election/post/{election_post}/edit",
 *     "delete-form" = "/election/post/{election_post}/delete",
 *     "version-history" = "/election/post/{election_post}/revisions",
 *     "revision" = "/election/post/{election_post}/revisions/{election_post_revision}/view",
 *     "revision_revert" = "/election/post/{election_post}/revisions/{election_post_revision}/revert",
 *     "revision_delete" = "/election/post/{election_post}/revisions/{election_post_revision}/delete",
 *     "translation_revert" = "/election/post/{election_post}/revisions/{election_post_revision}/revert/{langcode}",
 *     "collection" = "/election/posts",
 *   },
 *   field_ui_base_route = "entity.election_post_type.edit_form"
 * )
 */
class ElectionPost extends EditorialContentEntityBase implements ElectionPostInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use ElectionStatusesTrait;
  use ElectionConditionsTrait;
  use ElectionNotificationsTrait;

  const COUNT_TOTALS = ['ballots', 'votes', 'votes_invalid', 'abstentions', 'vacancies'];
  const COUNT_CANDIDATE_GROUPS = ['all', 'published', 'included', 'elected', 'defeated'];
  const COUNT_CANDIDATE_GROUP_NAMES = [
    'all' => 'All candidates at time of count',
    'published' => 'Published candidates at time of count',
    'included' => 'Candidates included in count',
    'elected' => 'Winner(s)',
    'defeated' => 'Defeated',
  ];

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the election_post owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getElection() {
    if (!$this->get('election')->first()) {
      return NULL;
    }

    $id = $this->get('election')->first()->getValue()['target_id'];
    return Election::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    // Custom fields.
    $fields['election'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Election'))
      ->setSetting('target_type', 'election')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Full text information about the post.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    static::addElectionConditionsFields($fields, 'election_post');

    $fields['candidate_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Election candidate type'))
      ->setSetting('target_type', 'election_candidate_type')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['limit_to_one_nomination_per_user'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Limit candidates to one nomination per user')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDefaultValue(TRUE);

    $fields['vacancies'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Vacancies'))
      ->setDescription(t('Number of available vacancies in the post.'))
      ->setDefaultValue(1)
      ->setSetting('min', 1)
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Sorting weight'))
      ->setDescription(t('Lower weights appear earlier in lists, higher weights sink to the bottom. Note this is normally set programatically so setting it manually may be overridden.'))
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['skip_allowed'] = BaseFieldDefinition::create('boolean')
      ->setLabel((t('Skipping permitted (can return to vote later)')))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['abstentions_allowed'] = BaseFieldDefinition::create('boolean')
      ->setLabel((t('Abstentions permitted')))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['include_reopen_nominations'] = BaseFieldDefinition::create('boolean')
      ->setLabel((t('Include "Reopen nominations" (RON) as a candidate')))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['exclusive'] = BaseFieldDefinition::create('boolean')
      ->setLabel((t('Exclusive nominations')))
      ->setDescription(t('A candidate can only stand for one exclusive position per election.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDefaultValue(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['publish_candidates_automatically'] = BaseFieldDefinition::create('boolean')
      ->setLabel((t('Publish nominations when created')))
      ->setDescription(t('If checked, newly submitted nominations will be published automatically. Otherwise, the administrator will need to publish them. Note there is an additional permission "view published election candidate entities when voting closed" required for any users who may need to view published candidates generally when voting is not open.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDefaultValue(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Count.
    $fields = array_merge($fields, static::getCountFieldDefinitions($fields));

    $fields['allow_candidate_editing'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Allow candidates to edit their nomination'))
      ->setDescription(t('The candidate user must have the "edit own" permission for the candidate type, regardless of this setting.'))
      ->setSettings([
        'allowed_values' => [
          'inherit' => t('Same as election'),
          'false' => t('Never'),
          'true' => t('Always'),
          'nominations' => t('Only while nominations are open'),
          'no_votes' => t('Only while no votes have been cast'),
          'custom' => t('Custom code (hook_alter)'),
        ],
      ])
      ->setDefaultValue('inherit')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['allow_candidate_withdrawal'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Allow candidates to withdraw their nomination'))
      ->setDescription(t('Note this does not delete the candidacy, it just unpublishes it and sets its status to "Withdrawn".'))
      ->setSettings([
        'allowed_values' => [
          'inherit' => t('Same as election'),
          'false' => t('Never'),
          'true' => t('Always'),
          'nominations' => t('Only while nominations are open'),
          'no_votes' => t('Only while no votes have been cast'),
        ],
      ])
      ->setDefaultValue('false')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['voting_method_inherit'] = BaseFieldDefinition::create('boolean')
      ->setLabel((t('Use election\'s default voting method')))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['voting_method'] = BaseFieldDefinition::create('plugin_reference')
      ->setLabel(t('Voting method'))
      ->setSettings([
        'target_type' => 'election_voting_method',
      ])
      ->setDisplayOptions('form', [
        'type' => 'plugin_reference_select',
        'configuration_form' => 'full',
        'provider_grouping' => FALSE,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    // ->setRequired(TRUE);
    $fields['ballot_intro'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Ballot introduction text'))
      ->setDescription(t('Shown at top of ballot. If provided this overrides the election-wide one.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -45,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -49,
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    static::addElectionStatusesFields($fields, 'election_post');

    return $fields;
  }

  /**
   *
   */
  public function getElectionPostType() {
    return ElectionPostType::load($this->bundle());
  }

  /**
   *
   */
  public function getCandidateQuery(array $statuses = NULL, $only_published = TRUE, AccountInterface $account = NULL) {
    $query = \Drupal::entityQuery('election_candidate');
    $query->condition('election_post', $this->id());

    if ($statuses) {
      $query->condition('candidate_status', $statuses, 'IN');
    }

    if ($only_published) {
      $query->condition('status', 1);
    }

    if ($account) {
      $query->condition('user_id', $account->id());
    }

    return $query;
  }

  /**
   *
   */
  public function getCandidatesIds(array $statuses = NULL, $only_published = TRUE, AccountInterface $account = NULL) {
    $query = $this->getCandidateQuery($statuses, $only_published, $account);
    return $query->execute();
  }

  /**
   *
   */
  public function countCandidates(array $statuses = NULL, $only_published = TRUE, AccountInterface $account = NULL) {
    $query = $this->getCandidateQuery($statuses, $only_published, $account);
    return $query->count()->execute();
  }

  /**
   *
   */
  public function countCandidatesForVoting($ballotCandidateSort = 'random') {
    $candidates = $this->countCandidates(['hopeful'], TRUE);
    return $candidates;
  }

  /**
   *
   */
  public function getCandidates(array $statuses = NULL, $only_published = TRUE, AccountInterface $account = NULL) {
    $ids = $this->getCandidatesIds($statuses, $only_published, $account);
    return ElectionCandidate::loadMultiple($ids);
  }

  /**
   *
   */
  public function getCandidatesForVoting($ballotCandidateSort = 'random') {
    $candidates = $this->getCandidates(['hopeful'], TRUE);

    if ($ballotCandidateSort == 'random') {
      shuffle($candidates);
    }
    elseif ($ballotCandidateSort == 'alhpabetical') {
      // @todo
    }
    return $candidates;
  }

  /**
   * Get user-friendly name for type.
   *
   * @param bool $capital
   *   Start with a capital letter.
   * @param bool $plural
   *   PLuralise.
   *
   * @return string
   *   The user-friendly name.
   */
  public function getTypeNaming($capital = FALSE, $plural = FALSE) {
    return ElectionPostType::load($this->bundle())->getNaming($capital, $plural);
  }

  /**
   *
   */
  public function getCandidateType() {
    return $this->candidate_type->entity ? $this->candidate_type->entity : NULL;
  }

  /**
   *
   */
  public function getCandidateTypeId() {
    return $this->getCandidateType() ? $this->getCandidateType()->id() : NULL;
  }

  /**
   */
  public function getCandidateTypesAsLabel() {
    $candidate_type = $this->getCandidateType();
    if ($candidate_type) {
      return $candidate_type->getNaming(TRUE, TRUE);
    }
    else {
      return t('Candidates');
    }
  }

  /**
   *
   */
  public function getActionLinks(AccountInterface $account = NULL, $phasesToInclude = []) {
    $actions = [];
    if (\Drupal::currentUser()->isAnonymous()) {

      $votingOpen = $this->isOpenOrPartiallyOpen(new VotingPhase());

      $nominationsOpen = $this->isOpenOrPartiallyOpen(new NominationsPhase());

      if ($votingOpen || $nominationsOpen) {
        $title = $votingOpen ? t('Log in to vote') : t('Log in to nominate');
        // Go straight to start voting if logging in to vote; go back to this page otherwise.
        if ($votingOpen) {
          $destination = Url::fromRoute(
            'entity.election_post.voting',
            ['election_post' => $this->id()]
          )->toString();
        }
        else {
          $destination = \Drupal::request()->getRequestUri();
        }

        $url = Url::fromRoute('user.login', [], [
          'absolute' => TRUE,
          'query' => ['destination' => $destination],
        ])->toString();

        $actions[] = [
          'title' => $title,
          'link' => $url,
          'button_type' => 'primary',
        ];
      }
    }
    else {
      $election = $this->getElection();
      $phases = $election->getEnabledPhases();
      if (isset($phasesToInclude) && count($phasesToInclude) > 0) {
        $phase_ids = array_intersect(array_keys($phases), array_keys($phasesToInclude));
        $phases = Election::getPhases($phase_ids);
      }
      foreach ($phases as $phase) {
        $eligibilityChecker = new ElectionPostEligibilityChecker($account, $this, $phase);
        $eligibilityChecker->setIncludePhaseStatus(TRUE);
        $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
        $eligibleForPhase = $eligibilityChecker->isEligible();

        $candidateTypeId = $this->getCandidateTypeId();
        if ($eligibleForPhase && $candidateTypeId) {
          $url = Url::fromRoute('entity.election_post.' . $phase->id(), [
            'election_post' => $this->id(),
            'election_candidate_type' => $candidateTypeId,
          ]);

          if ($url) {
            $actions[] = [
              'title' => t('@label', ['@label' => $phase->getAction()]),
              'link' => $url->toString(),
              'button_type' => 'primary',
            ];
          }
        }

        if (!$eligibleForPhase && $phase->id() == 'voting' && $election->isOpenOrPartiallyOpen(new VotingPhase)) {
          $url = Url::fromRoute('entity.election.voting', [
            'election_id' => $election->id(),
          ]);

          if ($url) {
            $actions[] = [
              'title' => t('@label for other posts in this election', ['@label' => $phase->getAction()]),
              'link' => $url->toString(),
              'button_type' => 'primary',
            ];
          }
        }

        if ($phase->id() == 'nominations' && $account->hasPermission('add election candidate entities without eligibility')) {
          $election_candidate_type = $this->getCandidateType();
          if ($election_candidate_type) {
            $url = Url::fromRoute('entity.election_candidate.add_to_election_post', [
              'election_post' => $this->id(),
              'election_candidate_type' => $election_candidate_type->id(),
            ]);
            if ($url) {
              $label = 'Create ' . $election_candidate_type->getNaming();
              $actions[] = [
                'title' => t(
                  '@label',
                  [
                    '@label' => $label,
                  ]
                ),
                'link' => $url->toString(),
                'button_type' => 'primary',
              ];
            }
          }
        }
      }

      if ($candidates = $this->getCandidates(['hopeful'], FALSE, $account)) {
        foreach ($candidates as $candidate) {
          if ($candidate->access('update', $account)) {
            // Edit button:
            $url = Url::fromRoute('entity.election_candidate.edit_form', [
              'election_candidate' => $candidate->id(),
            ]);
            $actions['election_candidate_edit_' . $candidate->id()] = [
              'title' => count($candidates) > 1 ? t('Edit nomination "@label"', ['@label' => $candidate->label()]) : t('Edit nomination'),
              'link' => $url->toString(),
              'button_type' => 'primary',
            ];
          }
        }

        if ($candidate->access('withdraw', $account)) {
          // Edit button:
          $url = Url::fromRoute('entity.election_candidate.withdraw', [
            'election_candidate' => $candidate->id(),
          ]);
          $actions['election_candidate_withdraw'] = [
            'title' => t('Withdraw nomination'),
            'link' => $url->toString(),
            'button_type' => 'danger',
          ];
        }
      }
    }

    if ($this->count_results_published->value && $this->count_timestamp->value && $account->hasPermission('view published count results')) {
      $uri = Url::fromRoute(
        'entity.election_post.canonical',
        [
          'election_post' => $this->id(),
        ],
        [
          'query' => [
            'results' => TRUE,
          ],
        ]
      )->toString();
      $actions['results'] = [
        'title' => t('View results'),
        'link' => $uri,
      ];
    }
    else {
      // Eligibility button:
      // @todo work out if we can flag eligibility depending on election phase
      $eligible = TRUE;
      $url = Url::fromRoute('entity.election_post.eligibility', [
        'election_post' => $this->id(),
      ]);
      $actions['eligibility'] = [
        'title' => $eligible ? t('Your eligibility') : t('Why not eligible?'),
        'link' => $url->toString(),
        'button_type' => $eligible ? 'primary' : 'danger',
      ];
    }

    return $actions;
  }

  /**
   *
   */
  public function getConditions(PhaseInterface $phase) {
    if (!\Drupal::moduleHandler()->moduleExists('election_conditions')) {
      return [];
    }

    $phase_id = $phase->id();

    $postConditions = [];
    $post_phase_id = 'none';
    $election_phase_id = 'none';

    // If conditions are shared across all phases, we only ever want to load the voting phase for the post.
    // However, we may still need to inherit election conditions, so we only set the phase to get for the post.
    if ($this->conditions_shared->value == 'shared') {
      $phase_id = 'voting';
      $post_phase_id = 'voting';
    }

    // Get election conditions if we're inheriting.
    if ($this->conditions_inherit_election->value == 'inherit') {
      $election = $this->getElection();
      if ($election) {
        if ($election_phase_id == 'none') {
          if ($election->conditions_shared->value == 'shared') {
            $election_phase_id = 'voting';
          }
          else {
            $election_phase_id = $election->get('conditions_' . $phase_id . '_same_as')->value;
          }
        }

        if ($election_phase_id != 'none') {
          $postConditions = $election->getConditionsForField('conditions_' . $election_phase_id);
        }
      }
    }

    // Get post conditions based on whatever phase we're using / inheriting from.
    if ($post_phase_id == 'none') {
      $post_phase_id = $this->get('conditions_' . $phase_id . '_same_as')->value;
    }

    if ($post_phase_id != 'none') {
      $postConditions = array_merge($postConditions, $this->getConditionsForField('conditions_' . $post_phase_id));
    }

    return $postConditions;
  }

  /**
   *
   */
  public function formatEligibilityRequirements(array $requirements, $onlyFailedRequirements = FALSE) {
    $formattedRequirements = [];

    foreach ($requirements as $requirement) {
      if (!$onlyFailedRequirements || $requirement->isFailed()) {
        $formattedRequirements[] = $requirement;
      }
    }

    // Sort by pass.
    usort($formattedRequirements, function ($a, $b) {
      return ($a->isPassed() ? 1 : -1) - ($b->isPassed() ? 1 : -1);
    });

    return $formattedRequirements;
  }

  /**
   *
   */
  public function formatEligibilityRequirementsTable($election_post, array $requirements, PhaseInterface $phase) {
    $formattedRequirements = $this->formatEligibilityRequirements($requirements);
    $render = \Drupal::service('complex_conditions.conditions_renderer')->requirementsTable($requirements);

    $render = [
      '#type' => 'details',
      '#title' => $phase->label(),
      '#open' => $election_post->isOpenOrPartiallyOpen($phase),
      'requirements_table_' . $phase->id() => $render,
    ];

    return $render;
  }

  /**
   *
   */
  public function getBallotIds($confirmedOnly = FALSE) {
    $query = \Drupal::entityQuery('election_ballot');
    $query->condition('election_post', $this->id());

    if ($confirmedOnly) {
      $query->condition('confirmed', TRUE);
    }

    $ids = $query->execute();

    return $ids;
  }

  /**
   *
   */
  public function getBallots($confirmedOnly = FALSE) {
    return ElectionBallot::loadMultiple($this->getBallotIds($confirmedOnly));
  }

  /**
   *
   */
  public function countBallots($confirmedOnly = FALSE) {
    $ballots = $this->getBallots($confirmedOnly);
    return $ballots ? count($ballots) : 0;
  }

  /**
   *
   */
  public static function getCountFieldDefinitions() {
    $fields = [];
    $fields['count_results_published'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Count results are published'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['count_timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Time when count run'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['count_method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Count method'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['count_results_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Count results in text format'))
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'text_default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 40,
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['count_log'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Count log'))
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'text_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['count_results_html'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Count results'))
      // ->setDisplayOptions('view', [
      //   'label' => 'visible',
      //   'type' => 'processed_text',
      // ])
      ->setDisplayOptions('form', [
        'type' => 'text_format',
        'weight' => 40,
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['count_log'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Count log'))
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'text_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    foreach (static::COUNT_TOTALS as $total) {
      $fields['count_total_' . $total] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Count: total ' . $total))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }

    // We want a historical record of these categories of candidate
    // Because as part of a count we may remove some
    // And users could run counts with different combinations of candidates for fun or profit
    // As posts are revisionable, this history will always be available.
    foreach (static::COUNT_CANDIDATE_GROUPS as $candidate_group) {
      $fields['count_candidates_' . $candidate_group] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t(static::COUNT_CANDIDATE_GROUP_NAMES[$candidate_group] ?? 'Count: candidates - ' . $candidate_group))
        ->setSetting('target_type', 'election_candidate')
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }

    $fields['count_ron_won'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Re-open nominations is a winner'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['count_has_random_element'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Count has random element (e.g. coinflip)'))
      ->setDescription(t('Prevents rerunning the count without confirmation'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   *
   */
  public function hasCount() {
    return $this->count_results_html->value != NULL;
  }

  /**
   *
   */
  public function hasRandomCount() {
    return $this->hasCount() && $this->count_has_random_element->value ? TRUE : FALSE;
  }

  /**
   *
   */
  public function validateCountable($count_settings, &$errors) {
    // Currently voting:
    if ($this->isOpenOrPartiallyOpen(new VotingPhase())) {
      $errors[] = 'Voting is still open';
    }

    // Rerunning count - settings:
    $rerunExisting = isset($count_settings['rerun_posts_with_existing_counts']) && $count_settings['rerun_posts_with_existing_counts'];
    $rerunRandom = isset($count_settings['rerun_posts_with_random_counts']) && $count_settings['rerun_posts_with_random_counts'];
    if (!$rerunExisting && $this->hasCount()) {
      $errors[] = 'Already has had a count run, and count settings do not allow re-running';
    }
    if (!$rerunRandom && $this->hasRandomCount()) {
      $errors[] = 'Already has had a count run which had a randomised result, and count settings do not allow re-running';
    }

    // No candidates:
    $candidates = $this->getCandidatesForCount($count_settings);
    if ($candidates == 0) {
      $errors[] = 'No candidates published and/or hopeful';
    }

    // No votes:
    $votes = $this->getBallots(TRUE);
    if (count($votes) == 0) {
      $errors[] = 'No confirmed ballots';
    }

    // No vacancies:
    if ($this->vacancies->value == 0) {
      $errors[] = 'No vacancies number set';
    }

    return count($errors) == 0;
  }

  /**
   *
   */
  public function getCandidatesForCount($count_settings) {
    $overrideSettings = isset($count_settings['overrides']) && $count_settings['overrides'];

    if ($overrideSettings && isset($count_settings['candidates'])) {
      $candidates = ElectionCandidate::loadMultiple($count_settings['candidates']);
      return $candidates;
    }

    $rerunExisting = isset($count_settings['rerun_posts_with_existing_counts']) && $count_settings['rerun_posts_with_existing_counts'];
    $rerunRandom = isset($count_settings['rerun_posts_with_random_counts']) && $count_settings['rerun_posts_with_random_counts'];
    if ($rerunExisting) {
      $candidates = $this->getCandidates([
        'hopeful',
        'defeated',
        'elected',
      ]);
    }
    else {
      $candidates = $this->getCandidates([
        'hopeful',
      ]);
    }
    return $candidates;
  }

  /**
   *
   */
  public function runCount($count_settings) {
    $pluginManager = \Drupal::service('plugin.manager.election_voting_method');
    $votingMethodPlugin = $this->getVotingMethod();

    $updateCandidates = isset($count_settings['save_to_candidate']) && $count_settings['save_to_candidate'];
    $updatePost = isset($count_settings['save_to_post']) && $count_settings['save_to_post'];
    $publishResults = isset($count_settings['publish']) && $count_settings['publish'];
    $rerun = isset($count_settings['rerun_posts_with_existing_counts']) && $count_settings['rerun_posts_with_existing_counts'];
    $rerunRandom = isset($count_settings['rerun_posts_with_random_counts']) && $count_settings['rerun_posts_with_random_counts'];

    $overrideSettings = isset($count_settings['overrides']) && $count_settings['overrides'];

    $log_params = [
      '@updateCandidates' => ($updateCandidates ? '' : 'not ') . 'updating candidates with results',
      '@updatePost' => ($updatePost ? '' : 'not ') . 'updating post with results',
      '@rerun' => ($rerun ? '' : 'not ') . 'rerunning counts for posts already counted',
      '@rerunRandom' => ($rerunRandom ? '' : 'not ') . 'rerunning counts for posts already counted with randomised results',
      '@publishResults' => ($publishResults ? '' : 'not ') . 'settings results as published',
    ];

    // Log if logging:
    if (\Drupal::moduleHandler()->moduleExists('election_log')) {
      $config = \Drupal::config('election_log.settings');
      if ($config->get('log_count')) {
        \Drupal::logger('election')->notice(
          t('Count run for @entity_type "@entity_name" (@entity_id) by @user_name (@user_id). Settings for count: @updateCandidates, @updatePost, @publishResults'),
          array_merge(
            election_log_get_log_context($this, 'run count'),
            $log_params,
          ),
        );
      }
    }

    // Work out candidates.
    $candidates = $this->getCandidatesForCount($count_settings);

    // Run count.
    $options = [
      'vacancies' => $overrideSettings && isset($count_settings['vacancies']) ? $count_settings['vacancies'] : NULL,
    ];
    $result = $votingMethodPlugin->countPost($this, $candidates, $options);

    // Update post.
    if ($updatePost) {
      // Save as new revision.
      $this->setNewRevision(TRUE);
      $this->revision_log = t('Ran count - triggered at @time by @user - @updateCandidates, @updatePost', array_merge([
        '@time' => \Drupal::time()->getRequestTime(),
        '@user' => \Drupal::currentUser()->getDisplayName() . ' (' . \Drupal::currentUser()->id() . ')',
      ], $log_params));
      $this->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $this->setRevisionUserId(\Drupal::currentUser()->id());

      // Run count and set values.
      foreach ($result as $key => $value) {
        if ($this->hasField($key)) {
          $this->set($key, $value);
        }
      }

      $this->count_results_published->value = $publishResults;
      $this->save();
    }

    if ($updateCandidates) {
      if (isset($result['count_candidates_elected'])) {
        foreach ($result['count_candidates_elected'] as $candidate) {
          if ($candidate && $candidate != 'ron') {
            $candidate->setCandidateStatus('elected')->save();
          }
        }
      }

      if (isset($result['count_candidates_defeated'])) {
        foreach ($result['count_candidates_defeated'] as $candidate) {
          if ($candidate && $candidate != 'ron') {
            $candidate->setCandidateStatus('defeated')->save();
          }
        }
      }
    }

    return $result;
  }

  /**
   *
   */
  public function getVotingMethod() {
    if ($this->voting_method_inherit) {
      return $this->getElection()->getVotingMethod();
    }
    else {
      return $this->voting_method->referencedPlugins()[0];
    }
  }

  /**
   *
   */
  public function getCandidateEditingRule() {
    $postEditingRule = $this->allow_candidate_editing->value;
    if ($postEditingRule == 'inherit' || !$postEditingRule) {
      return $this->getElection()->allow_candidate_editing->value;
    }
    return $postEditingRule;
  }

  /**
   *
   */
  public function getCandidateWithdrawalRule() {
    $rule = $this->allow_candidate_withdrawal->value;
    if ($rule == 'inherit' || !$rule) {
      return $this->getElection()->allow_candidate_withdrawal->value;
    }
    return $rule;
  }

  /**
   *
   */
  public function getBallotIntroduction() {
    if ($this->ballot_intro->value != '') {
      return $this->ballot_intro->view();
    }
    else {
      return $this->getElection()->ballot_intro->view();
    }
  }

  /**
   *
   */
  public function userHasCompletedPhase(AccountInterface $account, PhaseInterface $phase) {
    $already = NULL;
    switch ($phase->id()) {
      case 'interest':
        if ($this->get('limit_to_one_nomination_per_user')->value) {
          $already = $this->interestExists($account);
        }
        break;

      case 'nominations':
        if ($this->get('limit_to_one_nomination_per_user')->value) {
          $already = $this->nominationExists($account);
        }
        break;

      case 'voting':
        $already = $this->ballotExists($account);
        break;
    }
    return $already;
  }

  /**
   *
   */
  public function ballotExists($account) {
    $ballots = ElectionBallot::loadIdsByUserAndPost($account, $this);
    return count($ballots) > 0;
  }

  /**
   *
   */
  public function nominationExists($account) {
    $nominations = ElectionCandidate::loadIdsByUserAndPost($account, $this, ['hopeful']);
    return count($nominations) > 0;
  }

  /**
   *
   */
  public function interestExists($account) {
    $nominations = ElectionCandidate::loadIdsByUserAndPost($account, $this, ['interest']);
    return count($nominations) > 0;
  }

  /**
   *
   */
  public function getPostTypesAsLabel($capital = TRUE, $plural = TRUE) {
    $types = [];
    $post_type = $this->getElectionPostType();
    $types[$post_type->id()] = $post_type->getNaming($capital, $plural);
    return $types;
  }

  /**
   *
   */
  public function getVoterIds($confirmedOnly = TRUE) {
    $query = \Drupal::database()->select('election_ballot', 'eb');
    $query->fields('eb', ['user_id']);
    if ($confirmedOnly) {
      $query->condition('confirmed', 1);
    }
    $query->condition($this->getEntityTypeId(), $this->id());
    $uids = $query->execute()->fetchCol();
    return $uids;
  }

}
