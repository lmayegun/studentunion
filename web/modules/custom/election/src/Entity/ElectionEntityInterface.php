<?php

namespace Drupal\election\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Election entities (election posts and elections).
 *
 * @ingroup election
 */
interface ElectionEntityInterface extends ContentEntityInterface {

  /**
   *
   */
  public function getElection();

  /**
   *
   */
  public function ballotExists($account);

  /**
   *
   */
  public function nominationExists($account);

  /**
   *
   */
  public function interestExists($account);

}
