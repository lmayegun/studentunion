<?php

namespace Drupal\election\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Defines the Election candidate entity.
 *
 * @ingroup election
 *
 * @ContentEntityType(
 *   id = "election_candidate",
 *   label = @Translation("Election candidate"),
 *   bundle_label = @Translation("Election candidate type"),
 *   handlers = {
 *     "storage" = "Drupal\election\Storage\ElectionCandidateStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\election\EntityList\ElectionCandidateListBuilder",
 *     "views_data" = "Drupal\election\Entity\ElectionCandidateViewsData",
 *     "translation" = "Drupal\election\Translation\ElectionCandidateTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\election\Form\ElectionCandidateForm",
 *       "add" = "Drupal\election\Form\ElectionCandidateForm",
 *       "edit" = "Drupal\election\Form\ElectionCandidateForm",
 *       "delete" = "Drupal\election\Form\ElectionCandidateDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\election\Routing\ElectionCandidateHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\election\Access\ElectionCandidateAccessControlHandler",
 *   },
 *   base_table = "election_candidate",
 *   bundle_entity_type = "election_candidate_type",
 *   data_table = "election_candidate_field_data",
 *   revision_table = "election_candidate_revision",
 *   revision_data_table = "election_candidate_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   translatable = TRUE,
 *   admin_permission = "administer election candidate entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/election/candidate/{election_candidate}",
 *     "add-page" = "/election/candidate/add",
 *     "add-form" = "/election/candidate/add/{election_candidate_type}",
 *     "edit-form" = "/election/candidate/{election_candidate}/edit",
 *     "delete-form" = "/election/candidate/{election_candidate}/delete",
 *     "version-history" = "/election/candidate/{election_candidate}/revisions",
 *     "revision" = "/election/candidate/{election_candidate}/revisions/{election_candidate_revision}/view",
 *     "revision_revert" = "/election/candidate/{election_candidate}/revisions/{election_candidate_revision}/revert",
 *     "revision_delete" = "/election/candidate/{election_candidate}/revisions/{election_candidate_revision}/delete",
 *     "translation_revert" = "/election/candidate/{election_candidate}/revisions/{election_candidate_revision}/revert/{langcode}",
 *     "collection" = "/election/candidates",
 *   },
 *   field_ui_base_route = "entity.election_candidate_type.edit_form"
 * )
 */
class ElectionCandidate extends EditorialContentEntityBase implements ElectionCandidateInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the election_candidate owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   *
   */
  public function setCandidateStatus($status) {
    $this->set('candidate_status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getElectionPost() {
    return $this->election_post->entity ?? NULL;
  }

  /**
   *
   */
  public static function getPossibleStatuses() {
    return [
      'interest' => t('Expression of interest'),
      'hopeful' => t('Hopeful'),
      'withdrawn' => t('Withdrawn'),
      'rejected' => t('Rejected'),
      'defeated' => t('Defeated'),
      'elected' => t('Elected'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getElection() {
    $electionPost = $this->getElectionPost();
    $id = $electionPost->get('election')->first()->getValue()['target_id'];
    return \Drupal::entityTypeManager()->getStorage('election')->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // $fields['type'] = BaseFieldDefinition::create('entity_reference')
    //   ->setLabel(t('Type'))
    //   ->setDescription(t('The election candidate type.'))
    //   ->setSetting('target_type', 'election_candidate_type')
    //   ->setReadOnly(TRUE);
    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Proposer / nominator'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      // ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name for nomination'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    // Custom fields.
    $fields['election_post'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Election post'))
      ->setSetting('target_type', 'election_post')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['candidate_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setSettings([
        'allowed_values' => static::getPossibleStatuses(),
      ])
      ->setDefaultValue('hopeful')
      ->setDisplayOptions('view', [
        'region' => 'hidden',
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   *
   */
  public function getElectionCandidateType() {
    return ElectionCandidateType::load($this->bundle());
  }

  /**
   *
   */
  public static function loadIdsByUserAndElection(AccountInterface $account, ElectionInterface $election, $statuses = []) {
    $postIds = $election->getPostIds(FALSE);

    $query = \Drupal::entityQuery('election_candidate')
      ->condition('user_id', $account->id())
      ->condition('election_post', $postIds, 'IN');

    if (count($statuses) > 0) {
      $query->condition('candidate_status', $statuses, 'IN');
    }

    $ids = $query->execute();
    return $ids;
  }

  /**
   *
   */
  public static function loadIdsByUserAndPost(AccountInterface $account, ElectionPostInterface $election_post, $statuses = []) {
    $query = \Drupal::entityQuery('election_candidate')
      ->condition('user_id', $account->id())
      ->condition('election_post', $election_post->id());

    if (count($statuses) > 0) {
      $query->condition('candidate_status', $statuses, 'IN');
    }

    $ids = $query->execute();
    return $ids;
  }

  /**
   *
   */
  public function getBallotVotes($confirmedOnly = FALSE) {
    $votes_query = \Drupal::database()->select('election_ballot_vote', 'ev');
    $votes_query->join('election_ballot', 'eb', 'eb.id = ev.ballot_id');
    $votes_query->fields('ev', ['id'])
      ->condition('ev.ranking', 0, '>')
      ->condition('ev.candidate_id', $this->id())
      ->orderBy('ev.ballot_id')
      ->orderBy('ev.ranking');

    if ($confirmedOnly) {
      $votes_query->condition('eb.confirmed', 1);
    }

    $votes = $votes_query->execute()->fetchCol();

    return ElectionBallotVote::loadMultiple($votes);
  }

  /**
   *
   */
  public function countBallotVotes($confirmedOnly = FALSE) {
    return count($this->getBallotVotes($confirmedOnly));
  }

  /**
   *
   */
  public function getCandidateTypeName($plural = FALSE) {
    $type = ElectionCandidateType::load($this->bundle());
    if ($plural) {
      return $type->get('naming_candidate_plural') ?? t('Candidates');
    }
    return $type->get('naming_candidate_singular') ?? t('Candidates');
  }

  /**
   *
   */
  public function getActionLinks(AccountInterface $account = NULL, $phasesToInclude = []) {
    $actions = [];

    if ($this->access('view', $account)) {
      $url = Url::fromRoute('entity.election_candidate.canonical', [
        'election_candidate' => $this->id(),
      ]);
      $actions['election_candidate_view'] = [
        'title' => t('View nomination'),
        'link' => $url->toString(),
        'button_type' => 'primary',
      ];
    }

    $actions = array_merge($actions, $this->getElectionPost()->getActionLinks($account, $phasesToInclude));

    foreach ($actions as $key => $action) {
      if (strstr($key, 'election_candidate_edit_') && $key != 'election_candidate_edit_' . $this->id()) {
        unset($actions[$key]);
        $actions['election_candidate_edit_' . $this->id()]['title'] = t('Edit nomination');
      }
    }

    return $actions;
  }

  /**
   *
   */
  public function ballotExists($account) {
    // @todo filter by candidate as well.
    $ballots = ElectionBallot::loadIdsByUserAndPost($account, $this->getElectionPost());
    return count($ballots) > 0;
  }

  /**
   *
   */
  public function nominationExists($account) {
    return $this->candidate_status->value = 'hopeful' && $this->getOwnerId() == $account->id();
  }

  /**
   *
   */
  public function interestExists($account) {
    // @todo ...
    return $this->candidate_status->value = 'hopeful' && $this->getOwnerId() == $account->id();
  }

}
