<?php

namespace Drupal\election\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\election\Entity\ElectionEntityInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\VotingPhase;
use Drupal\election\Plugin\ElectionVotingMethodInterface;

/**
 * Class CountForm.
 */
abstract class CountFormBase extends BatchDownloadCSVForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'count_form';
  }

  /**
   *
   */
  public function getBatchTitle($form, $form_state) {
    return $this->t('Running count...');
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, array &$form, FormStateInterface $form_state) {
    $data = [ElectionPost::load($form_state->get('election_post_id'))];

    // Chunk the array.
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 10;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [
        [
          'posts' => $chunk,
          'save_to_post' => $form_state->getValue('save_to_post'),
          'save_to_candidate' => $form_state->getValue('save_to_candidate'),
          'publish' => $form_state->getValue('publish'),
          'rerun_posts_with_existing_counts' => $form_state->getValue('rerun_posts_with_existing_counts'),
          'rerun_posts_with_random_counts' => $form_state->getValue('rerun_posts_with_random_counts'),
          'overrides' => $form_state->getValue('overrides'),
          'candidates' => $form_state->getValue('candidates'),
          'vacancies' => $form_state->getValue('vacancies'),
        ],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch(array $data, array &$context) {
    $election_posts = $data['posts'];
    foreach ($election_posts as $election_post) {
      $errors = [];
      $countable = $election_post->validateCountable($data, $errors);
      if ($countable) {
        $resultsSummary = $election_post->runCount($data);
        $line = $this->convertResultsSummary($election_post, $resultsSummary);
      }
      else {
        $resultsSummary = [
          'error' => implode('; ', $errors),
          'count_timestamp' => \Drupal::time()->getRequestTime(),
          'count_method' => '',
        ];

        // Log if logging:
        if (\Drupal::moduleHandler()->moduleExists('election_log')) {
          $config = \Drupal::config('election_log.settings');
          if ($config->get('log_count')) {
            \Drupal::logger('election')->notice(
              t('Cannot run count for @entity_type "@entity_name" (@entity_id) by @user_name (@user_id). Errors: @errors'),
              array_merge(
                election_log_get_log_context($election_post, 'run count'),
                [
                  '@errors' => implode('; ', $errors),
                ],
              ),
            );
          }
        }
      }
      $line = $this->convertResultsSummary($election_post, $resultsSummary);
      $this->addLine($line, $context);
    }
  }

  /**
   *
   */
  public function checkCountCanBeRun(array &$form, ElectionEntityInterface $entity, ElectionVotingMethodInterface $voting_method) {
    $canRun = $voting_method->checkCanRun($entity);
    if (!$canRun['can']) {
      \Drupal::messenger()->addError($canRun['message']);
      return FALSE;
    }

    if ($entity->isOpenOrPartiallyOpen(new VotingPhase)) {
      \Drupal::messenger()->addError(t('Voting is currently open. Counts cannot be run while voting is open.'));
      return FALSE;
    }

    if ($entity->countBallots(TRUE) == 0) {
      \Drupal::messenger()->addError(t('No votes cast yet.'));
      return FALSE;
    }
    return TRUE;
  }

  /**
   *
   */
  public function addCountSettings(array &$form) {
    $form['save_to_post'] = [
      '#title' => t('Save results to the post'),
      '#description' => t('Updates the post with the results.'),
      '#type' => 'checkbox',
      '#default_value' => TRUE,
      '#weight' => 4,
    ];

    $form['publish'] = [
      '#title' => t('Publish results on the post'),
      '#description' => t('Allows visitors to view the results for published posts. If unchecked, you will need to manually update this checkbox by editing the post at a later time. Note this does not publish unpublished posts.'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#weight' => 4,
      '#states' => [
        'visible' => [
          ':input[name="save_to_post"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['save_to_candidate'] = [
      '#title' => t('Save results to each candidate (e.g. defeated, elected)'),
      '#description' => t('Updates the candidate status. If you don\'t want results to be public yet, unpublish the candidate before running the count. If you need to update this after the count, you need to edit each individual candidate and change its status.'),
      '#type' => 'checkbox',
      '#default_value' => TRUE,
      '#weight' => 4,
    ];

    $form['rerun_posts_with_existing_counts'] = [
      '#title' => t('Re-run counts for posts with existing counts already saved to post'),
      '#description' => t('Leave unchecked by default. This will re-run the count and, if "save to post" is enabled, replace any existing count information with new information. (A revision will be created on the post to allow tracking count history).'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#weight' => 4,
    ];

    $form['rerun_posts_with_random_counts'] = [
      '#title' => t('Re-run counts for posts with existing counts already saved to post that depended on a random element'),
      '#description' => t('Leave unchecked by default. This will re-run the count even where a random element (e.g. a tie breaking "coin flip") determined the result. This could mean the randomly determined democratic outcome will be overridden. (A revision will be created on the post to allow tracking count history).'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#weight' => 4,
    ];

    $form['actions']['submit']['#value'] = 'Run count';
  }

  /**
   *
   */
  public function convertResultsSummary($election_post, $resultsSummary) {
    $electedNames = [];

    if (isset($resultsSummary['count_candidates_elected'])) {
      foreach ($resultsSummary['count_candidates_elected'] as $candidate) {
        if ($candidate == 'ron') {
          $electedNames[] = 'RON (Re-open Nominations)';
        }
        elseif ($candidate) {
          $electedNames[] = $candidate->label();
        }
        else {
          $electedNames[] = 'Cannot find candidate';
        }
      }
    }

    $defeatedNames = [];
    if (isset($resultsSummary['count_candidates_defeated'])) {
      foreach ($resultsSummary['count_candidates_defeated'] as $candidate) {
        if ($candidate == 'ron') {
          $defeatedNames[] = 'RON (Re-open Nominations)';
        }
        elseif ($candidate) {
          $defeatedNames[] = $candidate->label();
        }
        else {
          $electedNames[] = 'Cannot find candidate';
        }
      }
    }

    $line = [
      'Post ID' => $election_post->id(),
      'Post name' => $election_post->label(),
      'Error' => $resultsSummary['error'] ?? '',
      'Time run' => \Drupal::service('date.formatter')->format($resultsSummary['count_timestamp'], 'long'),
      'Count method' => $resultsSummary['count_method'] ?? '',
      'Vacancies' => $resultsSummary['count_total_vacancies'] ?? '',
      'Ballots' => $resultsSummary['count_total_ballots'] ?? '',
      'Votes - valid' => $resultsSummary['count_total_votes'] ?? '',
      'Votes - invalid' => $resultsSummary['count_total_votes_invalid'] ?? '',
      'Candidates counted' => count($resultsSummary['count_candidates_included'] ?? []),
      'Elected count' => count($resultsSummary['count_candidates_elected'] ?? []),
      'Elected candidates' => implode(', ', $electedNames),
      'Defeated count' => count($resultsSummary['count_candidates_defeated'] ?? []),
      'Defeated candidates' => implode(', ', $defeatedNames),
    ];

    return $line;
  }

  /**
   *
   */
  public function getCountFormTitle() {
    return $this->t('Run count');
  }

}
