<?php

namespace Drupal\election\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\NominationsPhase;
use Drupal\election\Service\ElectionPostEligibilityChecker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Election candidate edit forms.
 *
 * @ingroup election
 */
class ElectionCandidateForm extends ContentEntityForm {

  use UserInputRequiredFormTrait;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  public $logged = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   *
   */
  public function getPhase() {
    // @todo make sure interest works
    return new NominationsPhase();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $election_post = NULL) {
    /** @var \Drupal\election\Entity\ElectionCandidate $entity */
    $form = parent::buildForm($form, $form_state);

    $election_candidate = $this->entity;
    $election_candidate_type = $election_candidate->getElectionCandidateType();
    if ($election_candidate_type) {
      if ($this->entity->isNew()) {
        $form['name']['widget'][0]['value']['#default_value'] = $election_candidate_type->getDefaultName();
      }

      $form['actions']['submit']['#value'] = t('@naming', [
        '@naming' => static::getActionNameForForm(
          $election_post,
          $election_candidate_type,
          $this->entity->isNew()
        ),
      ]);
    }

    if (is_null($election_post)) {
      $election_post = $election_candidate->getElectionPost();
    }

    $this->logFormOpen($election_post, $election_candidate);

    if (!is_null($election_post)) {
      $election = $election_post->getElection();
      $form['#title'] = $election_candidate_type->getActionNaming($election_post);

      if (isset($form['election_post'])) {
        if ($election->access('update')) {
          $form['election_post']['widget'][0]['target_id']['#default_value'] = $election_post;
          $form['election_post']['#disabled'] = TRUE;
        }
      }
      else {
        $form['election_post'] = [
          '#type' => 'hidden',
          '#value' => $election_post->id(),
        ];
      }
    }

    // Determine whether to show expression of interest form display mode or candidate form display mode.
    // @todo
    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    if ($election_post) {
      if ($this->entity->isNew() && !\Drupal::currentUser()->hasPermission('add election candidate entities without eligibility')) {
        $this->alterForConditions($election_post, new NominationsPhase(), $form, $form_state);
      }

      $form['actions']['back_to_post'] = [
        '#type' => 'link',
        '#title' => t('Cancel and go back to @post', [
          '@post' => $election_post->label(),
        ]),
        '#attributes' => [
          'class' => [
            'button',
            'button--danger',
          ],
        ],
        '#button_type' => "secondary",
        '#weight' => 10,
        '#limit_validation_errors' => [],
        '#url' => $election_post->toUrl(),
      ];

      $form['actions']['back_to_election'] = [
        '#type' => 'link',
        '#title' => t('Cancel and go back to @post', [
          '@post' => $election->label(),
        ]),
        '#attributes' => [
          'class' => [
            'button',
            'button--danger',
          ],
        ],
        '#button_type' => "secondary",
        '#weight' => 10,
        '#limit_validation_errors' => [],
        '#url' => $election_post->getElection()->toUrl(),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $election_post = $entity->getElectionPost();

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:

        $message = $election_post->getMessageMarkup($this->getPhase(), $entity);
        if ($message) {
          $this->messenger()->addMessage($message);
        }
        else {
          $this->messenger()->addMessage($this->t('Created %label.', [
            '%label' => $entity->label(),
          ]));
        }

        Cache::invalidateTags([
          'election:' . $election_post->getElection()->id() . ':candidates',
          'election:' . $election_post->getElection()->id() . ':candidates:count',
        ]);

        if ($election_post) {
          $entity->set('status', $election_post->publish_candidates_automatically->value);
          $entity->save();
        }

        $election_post->sendEmail(\Drupal::currentUser()->getEmail(), $this->getPhase(), $entity);

        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Election candidate.', [
          '%label' => $entity->label(),
        ]));
        Cache::invalidateTags([
          'election:' . $election_post->getElection()->id() . ':candidates',
        ]);
    }

    if ($url = $election_post->getPostRedirect($this->getPhase())) {
      $form_state->setRedirectUrl($url);
    }
    elseif ($election_post) {
      $form_state->setRedirect('entity.election_post.canonical', ['election_post' => $election_post->id()]);
    }
    else {
      $form_state->setRedirect('entity.election_candidate.canonical', ['election_candidate' => $entity->id()]);
    }
  }

  /**
   *
   */
  public static function getActionNameForForm($election_post = NULL, $election_candidate_type, $new = FALSE) {
    if (!$new) {
      return 'Save changes to ' . $election_candidate_type->getNaming();
    }
    else {
      $nominating = !\Drupal::currentUser()->hasPermission('add election candidate entities without eligibility');
      if ($election_candidate_type) {
        if ($nominating) {
          return $election_candidate_type->getActionNaming();
        }
        else {
          return 'Create ' . $election_candidate_type->getNaming();
        }
      }
      return 'Create candidate';
    }
  }

  /**
   *
   */
  public static function getCreationTitle($election_post, $election_candidate_type) {
    // Nominating as opposed to just editing a candidate.
    $nominating = !\Drupal::currentUser()->hasPermission('add election candidate entities without eligibility');
    if ($election_candidate_type) {
      if ($nominating) {
        return t('@naming: @post_name', [
          '@naming' => $election_candidate_type->getActionNaming(),
          '@post_name' => $election_post->label(),
        ]);
      }
      else {
        return t('Create @naming for @post_name', [
          '@naming' => $election_candidate_type->getNaming(),
          '@post_name' => $election_post->label(),
        ]);
      }
    }
    return t('Create candidate');
  }

  /**
   *
   */
  public function logFormOpen($election_post = NULL, $election_candidate = NULL) {
    if (!\Drupal::moduleHandler()->moduleExists('election_log')) {
      return NULL;
    }

    if ($this->logged) {
      return NULL;
    }

    $config = \Drupal::config('election_log.settings');
    if (!$config->get('log_forms')) {
      return;
    }

    if ($election_post || $election_candidate) {
      \Drupal::logger('election')->notice(
        t('Nomination form opened for @entity_type "@entity_name" (@entity_id) by @user_name (@user_id).'),
        election_log_get_log_context($election_candidate ? $election_candidate : $election_post, 'open ' . $this->getPhase()->id() . ' form'),
      );

      $this->logged = TRUE;
    }
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (\Drupal::currentUser()->hasPermission('add election candidate entities without eligibility')) {
      return;
    }
    $election_post = ElectionPost::load($form_state->getValue('election_post'));
    if (!$election_post) {
      return;
    }

    $eligibilityChecker = new ElectionPostEligibilityChecker(\Drupal::currentUser(), $election_post, $this->getPhase());
    $eligibilityChecker->setRefresh(TRUE);
    $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
    if (strstr($form['#id'], 'edit-form')) {
      $eligibilityChecker->setIgnoreAlreadyCompletedPhase(TRUE);
      $eligibilityChecker->setIgnorePostPublished(TRUE);
    }
    else {
      $eligibilityChecker->setIncludePhaseStatus(TRUE);
    }
    $eligible = $eligibilityChecker->isEligible();

    if (!$eligible) {
      $form_state->setErrorByName('name', $this->t('Since opening the form, you have become ineligible for this nomination. It may be nominations have closed, or you have already nominated separately.'));
    }
  }

}
