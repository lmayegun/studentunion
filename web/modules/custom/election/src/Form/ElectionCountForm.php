<?php

namespace Drupal\election\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionType;

/**
 * Class CountForm.
 */
class ElectionCountForm extends CountFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'election_count_form';
  }

  /**
   *
   */
  public function getBatchTitle($form, $form_state) {
    $election = Election::load($form_state->get('election_id'));
    return $this->t('Running count for @election', ['@election' => $election->label()]);
  }

  /**
   *
   */
  public function getDownloadTitle($form, $form_state) {
    $election = Election::load($form_state->get('election_id'));
    return $this->t('Count results and summary for @election', ['@election' => $election->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionInterface $election = NULL) {
    $form = parent::buildForm($form, $form_state, $election);
    $this->addCountSettings($form);
    $method = $election->getVotingMethod();
    if (method_exists($method, 'getDetailedMethodLabel')) {
      $detail = $method->getDetailedMethodLabel();
    }

    $canRun = $this->checkCountCanBeRun($form, $election, $method);
    if (!$canRun) {
      $form['actions']['submit']['#disabled'] = TRUE;
    }

    $form['info'] = [
      '#markup' => t('This will run a count for for all positions in this election based on the election voting method (@method - @detail), or each post\'s voting method if they override.', [
        '@method' => $method->label(),
        '@detail' => $detail,
      ]),
      '#weight' => 1,
    ];

    $form['info'] = [
      '#markup' => t('<p>This will run a count for all positions in this election.</p>'),
      '#weight' => 1,
    ];

    $form['info_download'] = [
      '#markup' => t('<p>After the count is run a spreadsheet will be downloaded with a summary of the count. <b>Please note any errors in the "Error" column.</b></p>'),
      '#weight' => 2,
    ];

    $post_types = [];
    $election_type = ElectionType::load($election->bundle());
    $types = $election_type->getAllowedPostTypes();
    foreach ($types as $type) {
      $post_types[$type->id()] = $type->label();
    }
    $form['post_type_ids'] = [
      '#title' => 'Position types to run for count',
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $post_types,
      // '#description' => t('Select none to include all post types.'),
      '#default_value' => $_SESSION[$this->getFormId() . '-post_types'] ?? array_keys($post_types),
      '#weight' => 5,
    ];

    $form['post_ids_exclude'] = [
      '#title' => 'Positions to exclude',
      '#type' => 'entity_autocomplete',
      '#multiple' => TRUE,
      '#tags' => TRUE,
      '#target_type' => 'election_post',
      '#selection_handler' => 'default',
      '#default_value' => $_SESSION[$this->getFormId() . '-post_ids_exclude'] ?? [],
      '#weight' => 5,
    ];

    $form_state->set('election_id', $election->id());
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $election = Election::load($form_state->get('election_id'));
    $post_type_ids = count($form_state->getValue('post_type_ids')) > 0 ? $form_state->getValue('post_type_ids') : NULL;
    $post_ids = $election->getPostIds(TRUE, NULL, $post_type_ids);

    $exclude = $form_state->getValue('post_ids_exclude') && count($form_state->getValue('post_ids_exclude')) > 0 ? array_column($form_state->getValue('post_ids_exclude'), 'target_id') : NULL;
    if ($exclude) {
      $post_ids = array_diff($post_ids, $exclude);
    }

    $data = ElectionPost::loadMultiple($post_ids);

    // Chunk the array.
    $chunk_size = $form_state->getValue('batch_chunk_size') ?? 10;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processBatch'], [
        [
          'posts' => $chunk,
          'save_to_post' => $form_state->getValue('save_to_post'),
          'save_to_candidate' => $form_state->getValue('save_to_candidate'),
        ],
      ]);
    }
  }

}
