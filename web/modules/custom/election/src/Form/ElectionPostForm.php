<?php

namespace Drupal\election\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionPostType;
use Drupal\election\EntityTrait\ElectionStatusesTrait;
use Drupal\election_conditions\ElectionConditionsTrait;

/**
 * Form controller for Election post edit forms.
 *
 * @ingroup election
 */
class ElectionPostForm extends ContentEntityForm {

  use ElectionStatusesTrait;
  use ElectionConditionsTrait;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $election = NULL) {
    /** @var \Drupal\election\Entity\ElectionPost $entity */
    $form = parent::buildForm($form, $form_state);

    $form['#attached']['library'][] = 'election_conditions/conditions';

    // Hide and show fields:
    static::addStatusesStatesToForm($form);
    static::addConditionStatesToForm($form);
    $form['voting_method']['#states'] = [
      'required' => [
        ':input[name="voting_method_inherit[value]"]' => ['checked' => FALSE],
      ],
      'visible' => [
        ':input[name="voting_method_inherit[value]"]' => ['checked' => FALSE],
      ],
    ];

    static::showPublishedCheckboxIfPermission($form);

    $election_post = $this->entity;
    if ($election_post) {
      $form['#title'] = $election_post->getElectionPostType()->getActionNaming();

      // Restrict candidate types.
      $allowed_candidate_types = [];
      $election_post_type = ElectionPostType::load($election_post->bundle());
      foreach ($election_post_type->getAllowedCandidateTypes() as $type) {
        $allowed_candidate_types[$type->id()] = $type->label();
      }
      $form['candidate_type']['widget']['#options'] = array_merge(
        ['_none' => t('- Select a value -')],
        $allowed_candidate_types,
      );
    }

    $election = $election_post && !$election ? $election_post->getElection() : $election;

    if (!is_null($election)) {
      if (isset($form['election'])) {
        $form['election']['widget'][0]['target_id']['#default_value'] = $election;
        if (!$election->access('update')) {
          $form['election']['#disabled'] = TRUE;
        }
      }

      $phasesToShow = $election->getEnabledPhases();
      if (count($phasesToShow) != count(Election::getPhases())) {
        foreach (Election::getPhases() as $phase_id => $phase) {
          if (!in_array($phase_id, array_keys($phasesToShow))) {
            $form['conditions_' . $phase_id . '_same_as']['#access'] = FALSE;
            $form['conditions_' . $phase_id]['#access'] = FALSE;
            foreach (Election::getPhases() as $phase_to_disable) {
              unset($form['conditions_' . $phase_to_disable->id() . '_same_as']['widget']['#options'][$phase_id]);
            }
          }
        }
      }

      $method = $election->getVotingMethod() ? $election->getVotingMethod()->label() : '';
      if ($method) {
        $form['voting_method_inherit']['#prefix'] = t('Current voting method for election is: <b>@voting_method</b>', ['@voting_method' => $method]);
      }
    }

    if ($this->entity->isNew()) {
      unset($form['count_results_text']);
      unset($form['count_results_html']);
    }
    else {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Election post.', [
          '%label' => $entity->label(),
        ]));
        Cache::invalidateTags([
          'election:' . $entity->getElection()->id() . ':posts',
          'election:' . $entity->getElection()->id() . ':posts:count',
        ]);
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Election post.', [
          '%label' => $entity->label(),
        ]));
        Cache::invalidateTags([
          'election:' . $entity->getElection()->id() . ':posts',
        ]);
    }
    $form_state->setRedirect('entity.election_post.canonical', [
      'election' => $entity->getElection()->id(),
      'election_post' => $entity->id(),
    ]);
  }

}
