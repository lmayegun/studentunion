<?php

namespace Drupal\election\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_import_export\Form\BatchForm;
use Drupal\election\Entity\ElectionBallot;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;

/**
 * Class ElectionPostCloneForm.
 */
class ElectionPostCloneForm extends BatchForm {

  public $originalPostId = NULL;
  public $newPostId = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'election_post_clone_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionPostInterface $election_post = NULL) {
    $form_state->set('election_post_id', $election_post->id());

    $form['clone_candidates'] = [
      '#type' => 'checkbox',
      '#title' => 'Clone existing candidates?',
      '#default_value' => FALSE,
    ];

    $form['clone_ballots_and_votes'] = [
      '#type' => 'checkbox',
      '#title' => 'Clone existing ballots and votes?',
      '#default_value' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="clone_candidates"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Clone @name', ['@name' => $election_post->label()]),
      '#button_type' => 'primary',
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(&$batch_builder, &$form, $form_state) {
    $candidate_map = [];
    $ballots = [];

    $cloneCandidates = $form_state->getValue('clone_candidates');
    $cloneBallots = $form_state->getValue('clone_ballots_and_votes');

    $original = ElectionPost::load($form_state->get('election_post_id'));

    $newPost = $original->createDuplicate();
    $newPost->name->value .= ' (cloned)';
    $newPost->save();

    if ($cloneCandidates) {
      $candidates = $original->getCandidates(NULL, FALSE);
      foreach ($candidates as $candidate) {
        $newCandidate = $candidate->createDuplicate();
        $newCandidate->election_post->entity = $newPost;
        $newCandidate->save();

        $candidate_map[$candidate->id()] = $newCandidate->id();
      }
    }

    $data = [];
    if (count($candidate_map) > 0 && $cloneBallots) {
      $ballot_ids = $original->getBallotIds(FALSE);
      $this->ballotsCount = count($ballot_ids);
      foreach ($ballot_ids as $ballot_id) {
        $data[] = [
          'ballot_id' => $ballot_id,
          'original_post_id' => $original->id(),
          'new_post_id' => $newPost->id(),
          'candidate_map' => $candidate_map,
        ];
      }
    }

    $this->originalPostId = $original->id();
    $this->newPostId = $newPost->id();

    // Chunk the array.
    $chunk_size = 15;
    $chunks = array_chunk($data, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $this->addOperation($batch_builder, [$this, 'processOperation'], [$chunk]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function processOperation(array $data, array &$context) {
    foreach ($data as $row) {
      $newPost = ElectionPost::load($row['new_post_id']);
      $context['results']['original_post_id'] = $row['original_post_id'];
      $context['results']['new_post_id'] = $row['new_post_id'];

      $ballot = ElectionBallot::load($row['ballot_id']);
      $candidate_map = $row['candidate_map'];

      $newBallot = $ballot->createDuplicate();
      $newBallot->election_post->entity = $newPost;
      $newBallot->election->entity = $newPost->getElection();
      $newBallot->save();

      // Clone votes:
      $votes = $ballot->getVotes();
      foreach ($votes as $vote) {
        $newVote = $vote->createDuplicate();
        if ($vote->candidate_id->entity) {
          $newVote->candidate_id->entity = $candidate_map[$vote->candidate_id->entity->id()];
        }
        $newVote->ballot_id->entity = $newBallot;
        $newVote->save();
      }
    }
  }

  /**
   *
   */
  public function getFormTitle($election_post = NULL) {
    return $this->t('Clone @election', [
      '@election_post' => $election_post ? $election_post->label() : '',
    ]);
  }

  /**
   *
   */
  public function batchFinishedRenderArray(array $results) {
    $original = ElectionPost::load($results['original_post_id'] ?? $this->originalPostId);
    $newPost = ElectionPost::load($results['new_post_id'] ?? $this->newPostId);

    if ($newPost->countBallots(FALSE) != $original->countBallots(FALSE)) {
      return ['#markup' => t('Error - the new post ballot count does not match the original. An error may have occured during cloning.')];
    }

    $array = [
      '(original)' => $original,
      '(cloned version)' => $newPost,
    ];

    $build = [];
    foreach ($array as $subtitle => $election_post) {
      $build[] = [
        'link' => [
          '#type' => 'link',
          '#title' => t('Go to @post @subtitle', [
            '@post' => $election_post->label(),
            '@subtitle' => $subtitle,
          ]),
          '#attributes' => [
            'class' => [
              'button',
              'button--primary',
            ],
          ],
          '#button_type' => "primary",
          '#weight' => 10,
          '#limit_validation_errors' => [],
          '#url' => $election_post->toUrl(),
        ],
      ];
    }
    return $build;
  }

}
