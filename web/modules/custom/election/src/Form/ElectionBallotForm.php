<?php

namespace Drupal\election\Form;

use Drupal\Core\Site\Settings;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\election\Entity\ElectionBallotInterface;
use Drupal\election\Entity\ElectionBallotVote;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\VotingPhase;
use Drupal\election\Service\ElectionPostEligibilityChecker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Election ballot edit forms.
 *
 * @ingroup election
 */
class ElectionBallotForm extends ContentEntityForm {

  use UserInputRequiredFormTrait;

  public $logged = FALSE;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $election_post = NULL) {
    $form = parent::buildForm($form, $form_state);

    if (is_numeric($election_post)) {
      $election_post = \Drupal::entityTypeManager()->getStorage('election_post')->load($election_post);
      $form_state->set('election_post', $election_post->id());
    }

    if (!$election_post) {
      $form['#title'] = 'No position found';
      $form['info']['#markup'] = 'No election post found.';
      return $form;
    }

    $election = $election_post->getElection();

    $eligibilityChecker = new ElectionPostEligibilityChecker(\Drupal::currentUser(), $election_post, new VotingPhase());
    $eligibilityChecker->setRefresh(TRUE);
    $eligibilityChecker->setIncludePhaseStatus(TRUE);
    $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
    $eligible = $eligibilityChecker->isEligible();

    if (!$eligible) {
      \Drupal::messenger()->addMessage(t('Not eligible to vote for %position.', [
        '%position' => $election_post->label(),
      ]));
      $url = $election->toUrl()->toString();
      return new TrustedRedirectResponse($url);
    }

    $this->logFormOpen($election_post);

    $form['info']['#markup'] = $election_post->description->value;

    $form['#attached']['library'][] = 'election/ballot';

    $intro = $election_post->getBallotIntroduction();
    if (count(array_keys($intro)) > 1) {
      $form['ballot_intro']['post_intro'] = [
        '#type' => 'markup',
        '#markup' => $intro,
      ];
    }

    $form['election_post']['widget'][0]['target_id']['#default_value'] = $election_post;
    $form['election_post']['#disabled'] = TRUE;

    $voting_method = $election_post->getVotingMethod();
    $allow_equal_ranking = method_exists($voting_method, 'allowsEqualRanking') && $voting_method->allowsEqualRanking();
    if ($allow_equal_ranking) {
      $form['#attributes']['class'][] = 'allow-equal';
    }

    $form['#title'] = t('Vote for @post', ['@post' => $election_post->label()]);

    // Allow voting methods to modify.
    // Allow plugins to modify.
    // @todo .
    $voteLabel = 'Ranking';

    // List voting inputs
    // $votingMethod = $election->get('voting_method')->value;.
    $ballotDisplay = 'table';
    $ballotCandidateSort = $election->get('ballot_candidate_sort')->value;
    $candidates = $election_post->getCandidatesForVoting($ballotCandidateSort);

    $candidate = reset($candidates);
    $candidateLabel = $candidate->getCandidateTypeName() ?? 'Option';
    $candidateLabelPlural = $candidate->getCandidateTypeName(TRUE) ?? 'Options';

    // @todo load these
    $rankedVoting = TRUE;
    $form_state->set('ranked_voting', $rankedVoting);

    $exhaustive = FALSE;
    $form_state->set('exhaustive_voting', $exhaustive);
    $maximumChoices = 1;

    $include_reopen_nominations = $election_post->include_reopen_nominations->value == TRUE;

    $count = count($candidates);

    $rankingOptions = [];
    if ($rankedVoting) {
      // Add a 'No preference' option which means the candidate would not be ranked.
      if (!$exhaustive) {
        $rankingOptions['NONE'] = t('No preference');
      }
      $pref_limit = $count;
      if ($include_reopen_nominations) {
        $pref_limit++;
      }
      for ($i = 1; $i <= $pref_limit; $i++) {
        $ordinal = static::getOrdinal($i);
        $rankingOptions[$i] = t($ordinal);
      }
    }
    else {
      $rankingOptions['NONE'] = t('Not selected');
      $rankingOptions['1'] = t('Selected');
    }

    // A container for anything that should appear before the meat of the ballot.
    // This can be hidden as a block as well, e.g. for "user input required" conditions.
    $form['ballot_intro'] = [
      '#tree' => TRUE,
      '#prefix' => '<div class="ballot_intro">',
      '#suffix' => '</div>',
      '#weight' => -50,
    ];

    $form['ballot_intro']['ballot_explanation'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#markup' => t('This is the ballot paper for the @post_type @name in @election. @vacancies The @candidate_type are listed below@randomSort.', [
        '@name' => $election_post->toLink()->toString(),
        '@election' => $election->toLink()->toString(),
        '@vacancies' => \Drupal::translation()->formatPlural(
          $election_post->vacancies->value,
          'There is 1 vacancy.',
          'There are @count vacancies.',
        ),
        '@election_type' => $election->getElectionType()->label(),
        '@post_type' => $election_post->getTypeNaming(FALSE, FALSE),
        '@candidate_type' => strtolower($candidateLabelPlural),
        '@randomSort' => $ballotCandidateSort == 'random' ? ' in a random order' : '',
      ]),
      '#suffix' => '</p>',
      '#weight' => -2,
    ];

    if ($ballotDisplay == 'table') {
      $rows = [];

      $form['ballot_intro']['rankings_explanation'] = [
        '#type' => 'markup',
        '#prefix' => '<p>',
        '#markup' => t('Please rank the candidates according to your preference.', []),
        '#suffix' => '</p>',
        '#weight' => -1,
      ];

      $form['rankings'] =
        [
          '#type' => 'table',
          '#header' => [
            t($candidateLabelPlural),
            t($voteLabel),
          ],
          '#rows' => $rows,
          '#empty' => t('No candidates.'),
          '#weight' => 0,
        ];

      if ($include_reopen_nominations) {
        $candidates[] = [
          'id' => 'ron',
          'name' => t('Re-open nominations'),
        ];
      }

      $candidateViewBuilder = \Drupal::entityTypeManager()->getViewBuilder('election_candidate');

      $i = 0;
      foreach ($candidates as $candidate) {
        if (is_array($candidate) && isset($candidate['name'])) {
          $form['rankings'][$i]['candidate'] = ['#markup' => $candidate['name']];
          $id = $candidate['id'];
        }
        else {
          $pre_render = $candidateViewBuilder->view($candidate, 'ballot_short');
          $candidate_html = \Drupal::service('renderer')->render($pre_render);
          // @todo make this work better... setting? override within display mode?
          $link_to_candidate_below = TRUE;
          if ($link_to_candidate_below) {
            $candidate_html = '<a href="#candidate-' . $candidate->id() . '">' . $candidate_html . '</a>';
          }
          $form['rankings'][$i]['candidate'] = ['#markup' => $candidate_html];
          $id = $candidate->id();
        }

        $form['rankings'][$i][$id] = [
          '#type' => 'select',
          '#title' => 'Ranking',
          '#title_display' => 'invisible',
          '#options' => $rankingOptions,
          '#chosen' => FALSE,
          '#attributes' => [
            'class' => ['election-candidate-preference-select'],
          ],
        ];

        $i++;
      }
    }

    // Buttons.
    $form['actions'] = [
      '#weight' => 20,
      '#type' => 'actions',
    ];

    $form['actions']['edit'] = [
      '#type' => 'submit',
      '#value' => t('Submit vote'),
      '#weight' => 20,
      '#button_type' => 'primary',
      '#submit' => ["::save"],
      '#id' => 'submit-vote',
    ];

    if ($election_post->abstentions_allowed->value == 1) {
      $form['actions']['abstain'] = [
        '#type' => 'submit',
        '#value' => t('Abstain from voting'),
        '#weight' => 21,
        '#button_type' => 'danger',
        '#submit' => ["::save"],
        '#id' => 'submit-abstain',
      ];
    }

    if ($election_post->skip_allowed->value == 1) {
      $form['actions']['skip'] = [
        '#type' => 'submit',
        '#value' => t('Skip @name for now', ['@name' => $election_post->getElectionPostType()->getNaming()]),
        '#weight' => 22,
        '#button_type' => 'secondary',
        '#submit' => ["::save"],
        '#id' => 'submit-skip',
        '#limit_validation_errors' => [],
      ];
    }

    if ($election_post->getElection()->ballot_show_candidates_below->value) {
      // Can't use the view because it won't be in the same order as the candidates in the table if they have a different sort order...
      // $candidatesList = views_embed_view('election_candidates_for_post', 'embed_ballot_full');
      // $html = \Drupal::service('renderer')->render($candidatesList);
      // So we build it manually.
      $html = '';
      foreach ($candidates as $candidate) {
        if (gettype($candidate) == 'object') {
          $pre_render = $candidateViewBuilder->view($candidate, 'ballot_full');
          $html .= '<a name="candidate-' . $candidate->id() . '"></a>' . \Drupal::service('renderer')->render($pre_render);
        }
      }

      if ($html) {
        // @todo make it a proper themable template
        $form['candidates_full'] = [
          '#weight' => 30,
          '#prefix' => '<div class="candidates_list"><h2>' . $election_post->getCandidateTypesAsLabel() . '</h2>',
          '#markup' => $html,
          '#suffix' => '</div>',
        ];
      }
    }

    $this->alterForConditions($election_post, new VotingPhase(), $form, $form_state);

    return $form;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $triggering_element = $form_state->getTriggeringElement();
    $action = $triggering_element['#id'];

    $election_post = ElectionPost::load($form_state->get('election_post'));
    $voting_method = $election_post->getVotingMethod();

    if ($election_post->ballotExists(\Drupal::currentUser())) {
      $election = $election_post->getElection();
      $redirect = $election->getRedirectToNextPost();
      $url = Url::fromRoute($redirect['route_name'], $redirect['route_parameters']);
      $form_state->setErrorByName('rankings', $this->t('You appear to have already voted for this position since opening the ballot form. <a href="@link">Click here to go to the next ballot you are eligible for</a>.', ['@link' => $url->toString()]));
      return;
    }

    $rankings = $form_state->getValue('rankings');
    $rankingsUsed = [];
    foreach ($rankings as $index => $pair) {
      foreach ($pair as $candidate_id => $ranking) {
        // Ensure no equal ranking if not allowed:
        if (
          $ranking != 'NONE' &&
          $ranking != '' &&
          in_array($ranking, $rankingsUsed) &&
          !$voting_method->allowsEqualRanking()
        ) {
          $form_state->setErrorByName('rankings', $this->t('This post does not allow equal rankings.'));
        }

        $rankingsUsed[] = $ranking;
      }
    }

    $voting = $action == 'submit-vote';
    $abstaining = $action == 'submit-abstain';
    $skipping = $action == 'submit-skip';
    if ($voting) {
      // Ensure exhaustive ranking if required:
      if ($form_state->get('exhaustive_voting') && in_array('NONE', $rankingsUsed)) {
        $form_state->setErrorByName('rankings', $this->t('Must rank all options.'));
      }
      else {
        $uniqueRankings = array_unique($rankingsUsed);
        if (count($uniqueRankings) == 1 && $uniqueRankings[0] == 'NONE') {
          $form_state->setErrorByName('rankings', $this->t('Must rank at least one position to vote.'));
        }
      }
    }

    if (count($rankingsUsed) > 0 && $abstaining && !in_array('NONE', $rankingsUsed)) {
      $form_state->setErrorByName('rankings', $this->t('You have abstained but there are preferences placed. Please remove preferences.'));
    }
  }

  /**
   *
   */
  public static function submitFormVote(ElectionBallotInterface $ballot, array $form, FormStateInterface $form_state) {
    $rankings = $form_state->getValue('rankings');

    // Save votes as election_ballot_vote entities.
    foreach ($rankings as $index => $pair) {
      foreach ($pair as $candidate_id => $ranking) {
        $vote = ElectionBallotVote::create([
          'ballot_id' => $ballot->id(),
          'candidate_id' => $candidate_id == 'ron' ? NULL : $candidate_id,
          'ron' => $candidate_id == 'ron' ? 1 : 0,
          'ranking' => $ranking,
        ]);
        $vote->save();
      }
    }
  }

  /**
   *
   */
  public static function invalidateVoteCache($ballot) {
    Cache::invalidateTags([
      'election:' . $ballot->getElection()->id() . ':votes',
      'election:' . $ballot->getElection()->id() . ':votes:count',
    ]);
  }

  /**
   * This is the default save for the entity form.
   *
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $action = $triggering_element['#id'];

    $ballot = $this->entity;
    $account = \Drupal::currentUser();

    $election_post = ElectionPost::load($form_state->get('election_post'));
    $this->entity->election_post->entity = $election_post;
    $this->entity->election->entity = $election_post->getElection();

    $this->entity->ip->value = $this->getClientIp();
    $this->entity->user_agent->value = substr($_SERVER['HTTP_USER_AGENT'], 0, 254);
    $this->entity->session_hash->value = md5(session_id() . Settings::getHashSalt());

    if ($election_post->getElection()->ballot_confirmation->value == 'none') {
      $this->entity->confirmed->value = TRUE;
    }

    $messageParams = [
      '%position_label' => $election_post->label(),
    ];

    Cache::invalidateTags(
      [
        'election:' . $this->entity->getElection()->id() . ':ballots',
        'election:' . $this->entity->getElection()->id() . ':ballots:count',
      ]
    );

    // Check again if already voted.
    if ($election_post->ballotExists(\Drupal::currentUser())) {
      \Drupal::messenger()->addError(t('You appear to have already voted for %position_label since opening the ballot form.'), $messageParams);
      return $this->goNext($election_post, $form_state);
    }

    switch ($action) {
      case 'submit-skip':
        $this->messenger()->addMessage(
          $this->t(
            'Skipped position %position_label - you can return to vote as long as voting is still open.',
            $messageParams
          )
        );
        static::addSkippedPost($election_post);
        break;

      case 'submit-abstain':

        $message = $election_post->getMessageMarkup(new VotingPhase(), NULL);
        if ($message) {
          $this->messenger()->addMessage($message);
        }

        // @todo custom message for abstentions, in the meantime always clarify.
        $this->messenger()->addMessage(
          $this->t(
            'Abstention recorded for %position_label.',
            $messageParams
          )
        );

        $this->entity->set('abstained', TRUE);
        $status = parent::save($form, $form_state);
        static::addDonePost($election_post);

        Cache::invalidateTags([$election_post->getUserEligibilityCacheTag($account, new VotingPhase())]);
        static::invalidateVoteCache($ballot);

        // @todo send custom e-mail on abstention
        if ($status == SAVED_NEW) {
          $election_post->sendEmail(\Drupal::currentUser()->getEmail(), new VotingPhase(), NULL);
        }
        break;

      case 'submit-vote':
        $message = $election_post->getMessageMarkup(new VotingPhase(), NULL);
        if ($message) {
          $this->messenger()->addMessage($message);
        }
        else {
          $this->messenger()->addMessage(
            $this->t(
              'Vote recorded for %position_label.',
              $messageParams
            )
                  );
        }

        $status = parent::save($form, $form_state);
        static::addDonePost($election_post);

        static::submitFormVote($this->entity, $form, $form_state);

        Cache::invalidateTags([$election_post->getUserEligibilityCacheTag($account, new VotingPhase())]);
        static::invalidateVoteCache($ballot);

        if ($status == SAVED_NEW) {
          $election_post->sendEmail(\Drupal::currentUser()->getEmail(), new VotingPhase(), NULL);
        }
        break;
    }

    // @todo deal with confirmation if required for post
    return $this->goNext($election_post, $form_state);
  }

  /**
   *
   */
  public function goNext($election_post, &$form_state) {
    if ($url = $election_post->getPostRedirect(new VotingPhase())) {
      $form_state->setRedirectUrl($url);
      return;
    }

    $election = $election_post->getElection();
    $ballotBehaviour = $election->ballot_behaviour->value;
    if ($ballotBehaviour == 'one_by_one') {
      $form_state->setRedirect('entity.election_post.canonical', ['election_post' => $election_post->id()]);
    }
    else {
      $redirect = $election->getRedirectToNextPost();
      if ($redirect) {
        if ($redirect['message']) {
          \Drupal::messenger()->addMessage($redirect['message']);
        }

        if ($redirect['route_name'] == 'entity.election_post.voting') {
          $config = \Drupal::config('election.settings');
          if ($config->get('message_entering_loop') && !isset($_SESSION['message_entering_loop_shown'])) {
            \Drupal::messenger()->addMessage(new FormattableMarkup($config->get('message_entering_loop'), []));
            $_SESSION['message_entering_loop_shown'] = TRUE;
          }
        }

        $form_state->setRedirect($redirect['route_name'], $redirect['route_parameters']);
      }
    }
  }

  /**
   *
   */
  public static function getDoneOrSkippedPosts($election) {
    return array_merge(
      $_SESSION[$election->id() . '_done'],
      $_SESSION[$election->id() . '_skipped']
    );
  }

  /**
   *
   */
  public static function addDonePost($election_post) {
    $election = $election_post->getElection();
    $key = $election->id() . '_done';
    if (!isset($_SESSION[$key])) {
      $_SESSION[$key] = [];
    }
    $_SESSION[$key][] = $election_post->id();
  }

  /**
   *
   */
  public static function addSkippedPost($election_post) {
    $election = $election_post->getElection();
    $key = $election->id() . '_skipped';
    if (!isset($_SESSION[$key])) {
      $_SESSION[$key] = [];
    }
    $_SESSION[$key][] = $election_post->id();
  }

  /**
   * Find the ordinal of a number.
   */
  public static function getOrdinal($num) {
    $ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
    if ($num % 100 >= 11 && $num % 100 <= 13) {
      $ord = $num . 'th';
    }
    else {
      $ord = $num . $ends[$num % 10];
    }
    return $ord;
  }

  /**
   *
   */
  public function logFormOpen($election_post) {
    if (!\Drupal::moduleHandler()->moduleExists('election_log')) {
      return NULL;
    }

    if ($this->logged) {
      return NULL;
    }

    $config = \Drupal::config('election_log.settings');
    if (!$config->get('log_ballots')) {
      return;
    }

    if ($election_post) {
      \Drupal::logger('election')->notice(
        t('Ballot form opened for @entity_type "@entity_name" (@entity_id) by @user_name (@user_id).'),
        election_log_get_log_context($election_post, 'open ballot form'),
      );
      $this->logged = TRUE;
    }
  }

  /**
   *
   */
  public static function getFormTitle($election_post) {
    return t('Vote for @post_name', [
      '@post_name' => $election_post->label(),
    ]);
  }

  /**
   *
   */
  public function getClientIp() {
    if (array_key_exists('HTTP_CF_CONNECTING_IP', $_SERVER)) {
      return $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
      return $_SERVER["HTTP_X_FORWARDED_FOR"];
    }
    elseif (array_key_exists('REMOTE_ADDR', $_SERVER)) {
      return $_SERVER["REMOTE_ADDR"];
    }
    elseif (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
      return $_SERVER["HTTP_CLIENT_IP"];
    }

    return '';
  }

}
