<?php

namespace Drupal\election\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election\Phase\PhaseInterface;
use Drupal\election\Phase\VotingPhase;

/**
 *
 */
trait UserInputRequiredFormTrait {

  /**
   *
   */
  public function alterForConditions(ElectionPostInterface $election_post, PhaseInterface $phase, array &$form, FormStateInterface $form_state) {
    $conditions = $election_post->getConditions($phase);
    foreach ($conditions as $condition) {
      if ($phase->id() == 'voting' && method_exists($condition, 'alterBallotForm')) {
        $condition->alterBallotForm($form, $form_state, $phase);
      }
      elseif ($phase->id == 'nominations' && method_exists($condition, 'alterCandidateForm')) {
        $condition->alterCandidateForm($form, $form_state, $phase);
      }
    }
  }

  /**
   *
   */
  public function ajaxUserInputRequiredConfirmationChange(array &$form, FormStateInterface $form_state) {
    $ballot = $form_state->getFormObject()->getEntity();
    $election_post = $ballot->getElectionPost();
    if (!$election_post) {
      $context_provider = \Drupal::service('election.election_route_context');
      $contexts = $context_provider->getRuntimeContexts(['election_post']);
      $election_post = $contexts['election_post']->getContextValue();
      if (!$election_post) {
        return NULL;
      }
      if (is_string($election_post)) {
        $election_post = ElectionPost::load($election_post);
      }
    }

    $conditions = $election_post->getConditions(new VotingPhase());
    foreach ($conditions as $condition) {
      if (method_exists($condition, 'ajaxUserInputRequiredConfirmationChange')) {
        $condition->ajaxUserInputRequiredConfirmationChange($form, $form_state, $election_post);
      }
    }

    return $form;
  }

}
