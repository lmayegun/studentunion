<?php

namespace Drupal\election\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\ElectionCandidateInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class ElectionCandidateWithdrawForm extends ConfirmFormBase {

  /**
   * Election candidate.
   *
   * @var Drupal\election\Entity\ElectionCandidate
   */
  protected $electionCandidate;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionCandidateInterface $election_candidate = NULL) {
    $this->electionCandidate = $election_candidate;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "confirm_candidate_withdrawal";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->electionCandidate->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to withdraw the candidacy "%candidate" for <i>%post</i>?', [
      '%candidate' => $this->electionCandidate->label(),
      '%post' => $this->electionCandidate->getElectionPost()->label(),
    ]);
  }

}
