<?php

namespace Drupal\election\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Entity\ElectionPostInterface;

/**
 * Class CountForm.
 */
class ElectionPostCountForm extends CountFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'election_post_count_form';
  }

  /**
   *
   */
  public function getBatchTitle($form, $form_state) {
    $election_post = ElectionPost::load($form_state->get('election_post_id'));
    return $this->t('Running count for @election_post', ['@election_post' => $election_post->label()]);
  }

  /**
   *
   */
  public function getDownloadTitle($form, $form_state) {
    $election_post = ElectionPost::load($form_state->get('election_post_id'));
    return $this->t('Count results and summary for @election_post', ['@election_post' => $election_post->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ElectionPostInterface $election_post = NULL) {
    $form = parent::buildForm($form, $form_state, $election_post);

    $this->addCountSettings($form);

    $form_state->set('election_post_id', $election_post->id());

    $method = $election_post->getVotingMethod();
    if (method_exists($method, 'getDetailedMethodLabel')) {
      $detail = $method->getDetailedMethodLabel();
    }
    $form['info'] = [
      '#markup' => t('This will run a count for this position based on the post\'s voting method (@method - @detail).', [
        '@method' => $method->label(),
        '@detail' => $detail,
      ]),
      '#weight' => 1,
    ];

    $form['info_download'] = [
      '#markup' => t('<p>After the count is run a spreadsheet will be downloaded with a summary of the count. <b>Please note any errors in the "Error" column.</b></p>'),
      '#weight' => 2,
    ];

    $allowedOverrides = $method->allowedCountOverrides();
    if (count($allowedOverrides) > 0) {
      $form['overrides'] = [
        '#weight' => 5,
        '#type' => 'checkbox',
        '#title' => t('Override post settings'),
        '#default_value' => $_SESSION[$this->getFormId() . 'overrides'] ?? FALSE,
      ];

      $candidates = $election_post->getCandidates(['hopeful', 'elected', 'defeated']);
      $options_candidates = [];
      $options_hopeful_candidates = [];
      foreach ($candidates as $candidate) {
        $label = $candidate->label() . ' [' . $candidate->candidate_status->value . ']';
        $options_candidates[$candidate->id()] = $label;
        if ($candidate->candidate_status->value == 'hopeful') {
          $options_hopeful_candidates[$candidate->id()] = $candidate->id();
        }
      }

      if (in_array('candidates', $allowedOverrides)) {
        $form['candidates'] = [
          '#title' => t('Candidates to include'),
          '#type' => 'checkboxes',
          '#multiple' => TRUE,
          '#chosen' => TRUE,
          '#options' => $options_candidates,
          '#default_value' => $_SESSION[$this->getFormId() . 'candidates'] ?? $options_hopeful_candidates,
          '#states' => [
            'visible' => [
              ':input[name="overrides"][value="1"]' => ['checked' => TRUE],
            ],
          ],
          '#weight' => 6,
        ];
      }

      if (in_array('candidates', $allowedOverrides)) {
        $form['vacancies'] = [
          '#type' => 'number',
          '#title' => t('Vacancies'),
          '#default_value' => $_SESSION[$this->getFormId() . 'vacancies'] ?? $election_post->vacancies->value,
          '#states' => [
            'visible' => [
              ':input[name="overrides"][value="1"]' => ['checked' => TRUE],
            ],
          ],
          '#weight' => 6,
        ];
      }
    }

    $canRun = $this->checkCountCanBeRun($form, $election_post, $method);
    if (!$canRun) {
      $form['actions']['submit']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   *
   */
  public function batchFinishedRenderArray(array $results) {
    $election_post = ElectionPost::load($results[0]['Post ID']);
    if (count($results) > 0 && $election_post) {
      return [
        'link' => [
          '#type' => 'link',
          '#title' => t('Go back to @post', [
            '@post' => $election_post->label(),
          ]),
          '#attributes' => [
            'class' => [
              'button',
              'button--primary',
            ],
          ],
          '#button_type' => "primary",
          '#weight' => 10,
          '#limit_validation_errors' => [],
          '#url' => $election_post->toUrl(),
        ],
        'table' => [
          '#type' => 'table',
          '#header' => array_keys($results[0]),
          '#rows' => $results,
        ],
      ];
    }

    return [];
  }

}
