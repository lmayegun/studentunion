<?php

namespace Drupal\election\Translation;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for election_post.
 */
class ElectionPostTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
