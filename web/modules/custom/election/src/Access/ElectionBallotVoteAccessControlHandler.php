<?php

namespace Drupal\election\Access;

/**
 * Access controller for the Election ballot vote entity.
 *
 * @see \Drupal\election\Entity\ElectionBallotVote.
 */
class ElectionBallotVoteAccessControlHandler extends ElectionBallotAccessControlHandler {

  // We just use the ballot's access.
}
