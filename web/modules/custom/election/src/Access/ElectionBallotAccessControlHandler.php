<?php

namespace Drupal\election\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\VotingPhase;
use Drupal\election\Service\ElectionPostEligibilityChecker;

/**
 * Access controller for the Election ballot entity.
 *
 * @see \Drupal\election\Entity\ElectionBallot.
 */
class ElectionBallotAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\election\Entity\ElectionBallotInterface $entity */

    switch ($operation) {

      case 'view':
        // We don't want a "view" permission for ballots...
        return AccessResult::forbidden();

      case 'update':
        return AccessResult::forbidden();

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete election ballots');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $context_provider = \Drupal::service('election.election_route_context');

    $contexts = $context_provider->getRuntimeContexts(['election_post']);

    if (!isset($contexts['election_post']) || !$contexts['election_post']) {
      return AccessResult::neutral();
    }
    $election_post = ElectionPost::load($contexts['election_post']->getContextValue());

    $eligibilityChecker = new ElectionPostEligibilityChecker($account, $election_post, new VotingPhase());
    $eligibilityChecker->setIncludePhaseStatus(TRUE);
    $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
    $eligibilityChecker->setRefresh(TRUE);

    $eligible = $eligibilityChecker->isEligible();

    return AccessResult::allowedIf($eligible);
  }

}
