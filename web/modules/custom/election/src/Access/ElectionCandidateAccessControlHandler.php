<?php

namespace Drupal\election\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\InterestPhase;
use Drupal\election\Phase\NominationsPhase;
use Drupal\election\Phase\VotingPhase;
use Drupal\election\Service\ElectionPostEligibilityChecker;

/**
 * Access controller for the Election candidate entity.
 *
 * @see \Drupal\election\Entity\ElectionCandidate.
 */
class ElectionCandidateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\election\Entity\ElectionCandidateInterface $entity */

    $election_post = $entity->getElectionPost();
    if (!$election_post) {
      $editPermission = $account->hasPermission('edit election candidate entities');
      if ($editPermission) {
        return AccessResult::allowed();
      }
      else {
        return AccessResult::forbidden();
      }
    }

    switch ($operation) {

      case 'view':
        if (!$entity->isPublished()) {
          $isOwner = $entity->getOwnerId() == $account->id();
          $ownerAndCanView = $isOwner && $account->hasPermission('view own unpublished election candidates');
          $canViewAll = $account->hasPermission('view unpublished election candidate entities');
          return AccessResult::allowedIf($ownerAndCanView || $canViewAll);
        }

        // Published post.
        $publishedPost = $election_post->isPublished();

        if ($publishedPost) {
          $votingOpen = $election_post->isOpenOrPartiallyOpen(new VotingPhase());
          $allTheTime = $account->hasPermission('view published election candidates for published posts');
          if ($votingOpen) {
            $whileVotingOpen = $account->hasPermission('view published election candidate entities when voting open');
            return AccessResult::allowedIf($allTheTime || $whileVotingOpen);
          }
          else {
            return AccessResult::allowedIf($allTheTime);
          }
        }
        else {
          return AccessResult::allowedIfHasPermission($account, 'view published election candidates for unpublished posts');
        }

      case 'update':
        $editPermission = $account->hasPermission('edit election candidate entities');
        if ($editPermission) {
          return AccessResult::allowed();
        }

        $isOwner = $entity->getOwnerId() == $account->id();
        if (!$isOwner) {
          return AccessResult::forbidden();
        }

        $postEditingRule = $election_post->getCandidateEditingRule();

        // @todo deal with interest
        $phase = new NominationsPhase();

        $eligibilityChecker = new ElectionPostEligibilityChecker($account, $election_post, $phase);
        $eligibilityChecker->setRefresh(TRUE);
        $eligibilityChecker->setIgnoreAlreadyCompletedPhase(TRUE);
        $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
        $eligibilityChecker->setIncludePhaseStatus(FALSE);
        $eligible = $eligibilityChecker->isEligible();

        switch ($postEditingRule) {
          case 'true':
            return AccessResult::allowedIf($eligible);

          case 'false':
            return AccessResult::forbidden();

          case 'nominations':
          case 'interest':
            // @todo deal with interest...
            $open = $election_post->isOpenOrPartiallyOpen($phase);
            return AccessResult::allowedIf($open && $eligible);

          case 'no_votes':
            $canEdit = $entity->countBallotVotes(TRUE) == 0;
            return AccessResult::allowedIf($canEdit);

          case 'custom':
            $result = AccessResult::forbidden();
            \Drupal::moduleHandler()->alter('election_candidate_custom_update_access', $result, $entity);
            return $result;
        }

      case 'withdraw':
        $isOwner = $entity->getOwnerId() == $account->id();
        if (!$isOwner) {
          return AccessResult::forbidden();
        }

        $editPermission = $account->hasPermission('withdraw own nomination');
        if (!$editPermission) {
          return AccessResult::forbidden();
        }

        $withdrawalRule = $election_post->getCandidateWithdrawalRule();
        switch ($withdrawalRule) {
          case 'true':
            return AccessResult::allowed();

          case 'false':
            // \Drupal::messenger()->addWarning(t('Cannot withdraw nomination. Contact the election administrators.'));
            return AccessResult::forbidden();

          case 'nominations':
            // @todo deal with interest...
            $canEdit = $election_post->isOpenOrPartiallyOpen(new NominationsPhase());
            if (!$canEdit) {
              // \Drupal::messenger()->addWarning(t('Cannot withdraw if nominations are closed.'));
            }
            return AccessResult::allowedIf($canEdit);

          case 'no_votes':
            $canEdit = $entity->countBallotVotes(TRUE) == 0;
            if (!$canEdit) {
              // \Drupal::messenger()->addWarning(t('Cannot withdraw once votes are cast.'));
            }
            return AccessResult::allowedIf($canEdit);
        }

      case 'delete':
        $votesCount = $entity->countBallotVotes(TRUE);
        if ($votesCount > 0) {
          if (!\Drupal::currentUser()->hasPermission('delete candidates with votes')) {
            // \Drupal::messenger()->addWarning(t('Cannot delete this candidate because votes have already been cast.'));
          }
          return AccessResult::allowedIfHasPermission($account, 'delete candidates with votes');
        }
        else {
          return AccessResult::allowedIfHasPermission($account, 'delete candidates without votes');
        }
    }

    // Unknown operation, no opinion.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $createPermission = $account->hasPermission('add election candidate entities without eligibility');
    if ($createPermission) {
      return AccessResult::allowed();
    }

    $context_provider = \Drupal::service('election.election_route_context');
    $contexts = $context_provider->getRuntimeContexts(['election_post']);
    if (!isset($contexts['election_post']) || !$contexts['election_post']) {
      return AccessResult::neutral();
    }
    $election_post = ElectionPost::load($contexts['election_post']->getContextValue());

    if (!$election_post) {
      return AccessResult::neutral();
    }

    if ($election_post->isOpenOrPartiallyOpen(new InterestPhase)) {
      $canInterest = $account->hasPermission('express interest in posts');

      $eligibleInterest = FALSE;
      if ($canInterest) {
        $eligibilityChecker = new ElectionPostEligibilityChecker($account, $election_post, new InterestPhase());
        $eligibilityChecker->setIncludePhaseStatus(TRUE);
        $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
        $eligibilityChecker->setRefresh(TRUE);
        $eligibleInterest = $eligibilityChecker->isEligible();

        return AccessResult::allowedIf($canInterest && $eligibleInterest);
      }
    }

    if ($election_post->isOpenOrPartiallyOpen(new NominationsPhase)) {
      $canNominate = $account->hasPermission('nominate for posts');

      $eligibleNominate = FALSE;
      if ($canNominate) {
        $eligibilityChecker = new ElectionPostEligibilityChecker($account, $election_post, new NominationsPhase());
        $eligibilityChecker->setIncludePhaseStatus(TRUE);
        $eligibilityChecker->setIgnoreRequireUserInput(TRUE);
        $eligibilityChecker->setRefresh(TRUE);
        $eligibleNominate = $eligibilityChecker->isEligible();

        return AccessResult::allowedIf($canNominate && $eligibleNominate);
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    switch ($field_definition->getName()) {
      case 'status':
        return AccessResult::allowedIfHasPermission($account, 'edit election candidate entities');
    }

    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
