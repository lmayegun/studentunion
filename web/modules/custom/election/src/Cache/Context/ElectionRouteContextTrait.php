<?php

namespace Drupal\election\Cache\Context;

use Drupal\election\Entity\ElectionInterface;
use Drupal\election\Entity\ElectionPostInterface;

/**
 * Trait to get the election entity from the current route.
 *
 * Using this trait will add the getElectionFromRoute() method to the class.
 *
 * If the class is capable of injecting services from the container, it should
 * inject the 'current_route_match' and 'entity_type.manager' services and
 * assign them to the currentRouteMatch and entityTypeManager properties.
 */
trait ElectionRouteContextTrait {

  /**
   * The current route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Gets the current route match object.
   *
   * @return \Drupal\Core\Routing\RouteMatchInterface
   *   The current route match object.
   */
  protected function getCurrentRouteMatch() {
    if (!$this->currentRouteMatch) {
      $this->currentRouteMatch = \Drupal::service('current_route_match');
    }
    return $this->currentRouteMatch;
  }

  /**
   * Gets the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  protected function getEntityTypeManager() {
    if (!$this->entityTypeManager) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * Retrieves the election entity from the current route.
   *
   * This will try to load the election entity from the route if present. If we are
   * on the election add form, it will return a new election entity with the election
   * type set.
   *
   * @return \Drupal\election\Entity\ElectionInterface|null
   *   A election entity if one could be found or created, NULL otherwise.
   */
  public function getElectionFromRoute() {
    $route_match = $this->getCurrentRouteMatch();

    // See if the route has a election parameter and try to retrieve it.
    if (($election = $route_match->getParameter('election')) && $election instanceof ElectionInterface) {
      return $election;
    }
    // Create a new election to use as context if on the election add form.
    elseif ($route_match->getRouteName() == 'entity.election.add_form') {
      $election_type = $route_match->getParameter('election_type');
      return $this->getEntityTypeManager()->getStorage('election')->create(['type' => $election_type->id()]);
    }

    return NULL;
  }

  /**
   * Retrieves the election entity from the current route.
   *
   * This will try to load the election entity from the route if present. If we are
   * on the election add form, it will return a new election entity with the election
   * type set.
   *
   * @return \Drupal\election\Entity\ElectionInterface|null
   *   A election entity if one could be found or created, NULL otherwise.
   */
  public function getElectionPostFromRoute() {
    $route_match = $this->getCurrentRouteMatch();

    // See if the route has a election parameter and try to retrieve it.
    if (($election_post = $route_match->getParameter('election_post')) && $election_post instanceof ElectionPostInterface) {
      return $election_post;
    }
    // Create a new election to use as context if on the election add form.
    elseif ($route_match->getRouteName() == 'entity.election_post.add_form') {
      $election_post_type = $route_match->getParameter('election_post_type');
      return $this->getEntityTypeManager()->getStorage('election_post')->create(['type' => $election_post_type->id()]);
    }

    return NULL;
  }

}
