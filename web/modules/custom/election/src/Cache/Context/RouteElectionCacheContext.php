<?php

namespace Drupal\election\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines a cache context for "per election from route" caching.
 *
 * Please note: This cache context uses the election from the current route as the
 * context value to work with. This context is therefore only to be used with
 * data that was based on the election from the route. A good example being the
 * 'entity:election' context provided by the 'election.election_route_context' service.
 *
 * Cache context ID: 'route.election'.
 */
class RouteElectionCacheContext implements CacheContextInterface {

  use ElectionRouteContextTrait;

  /**
   * Constructs a new RouteElectionCacheContext.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $current_route_match, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentRouteMatch = $current_route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Election from route');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($election = $this->getElectionFromRoute()) {
      // If a election was found on the route, we return its ID as the context.
      if ($id = $election->id()) {
        return $id;
      }
      // If there was no ID, but we still have a election, we are on the election add
      // form and we return the election type instead. This allows the context to
      // distinguish between unsaved elections, based on their type.
      return $election->bundle();
    }

    // If no election was found on the route, we return a string that cannot be
    // mistaken for a election ID or election type.
    return 'election.none';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
