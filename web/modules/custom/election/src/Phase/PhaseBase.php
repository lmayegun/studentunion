<?php

namespace Drupal\election\Phase;

/**
 * Class PhaseBase.
 */
abstract class PhaseBase implements PhaseInterface {

  public $id = '';

  /**
   *
   */
  public function id() {
    return $this->id;
  }

}
