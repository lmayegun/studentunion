<?php

namespace Drupal\election\Phase;

/**
 * Class Nominations.
 */
class NominationsPhase extends PhaseBase implements PhaseInterface {

  public $id = 'nominations';

  /**
   * As a general rule the prior phase must be complete
   * before the next phase can continue
   * (if defined here).
   *
   * Subsequent phase => prior phase.
   */
  public $dependentPhase = 'interest';

  /**
   *
   */
  public function label($plural = TRUE) {
    return $plural ? t('Nominations') : t('Nomination');
  }

  /**
   *
   */
  public function getAction($past_tense = FALSE, $lowercase = FALSE) {
    if ($past_tense) {
      return $lowercase ? t('nominated') : t('Nominated');
    }
    else {
      return $lowercase ? t('nominate') : t('Nominate');
    }
  }

}
