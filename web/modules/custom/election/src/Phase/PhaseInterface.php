<?php

namespace Drupal\election\Phase;

/**
 * Class PhaseInterface.
 */
interface PhaseInterface {

  /**
   *
   */
  public function id();

  /**
   *
   */
  public function label($plural = FALSE);

  /**
   *
   */
  public function getAction($past_tense = FALSE, $lowercase = FALSE);

}
