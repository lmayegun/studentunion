<?php

namespace Drupal\election\Phase;

/**
 * Class Interest.
 */
class InterestPhase extends PhaseBase implements PhaseInterface {

  public $id = 'interest';

  /**
   * As a general rule the prior phase must be complete
   * before the next phase can continue
   * (if defined here).
   *
   * Subsequent phase => prior phase.
   */
  public $dependentPhase = NULL;

  /**
   *
   */
  public function label($plural = FALSE) {
    return $plural ? t('Expressions of interest') : t('Expression of interest');
  }

  /**
   *
   */
  public function getAction($past_tense = FALSE, $lowercase = FALSE) {
    if ($past_tense) {
      return $lowercase ? t('expressed interest') : t('Expressed interest');
    }
    else {
      return $lowercase ? t('express interest') : t('Express interest');
    }
  }

}
