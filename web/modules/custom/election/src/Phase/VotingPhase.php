<?php

namespace Drupal\election\Phase;

/**
 * Class VotingPhase.
 */
class VotingPhase extends PhaseBase implements PhaseInterface {

  public $id = 'voting';

  /**
   * As a general rule the prior phase must be complete
   * before the next phase can continue
   * (if defined here).
   *
   * Subsequent phase => prior phase.
   */
  public $dependentPhase = 'nominations';

  /**
   *
   */
  public function label($plural = FALSE) {
    return t('Voting');
  }

  /**
   *
   */
  public function getAction($past_tense = FALSE, $lowercase = FALSE) {
    if ($past_tense) {
      return $lowercase ? t('voted or abstained') : t('Voted or abstained');
    }
    else {
      return $lowercase ? t('vote') : t('Vote');
    }
  }

}
