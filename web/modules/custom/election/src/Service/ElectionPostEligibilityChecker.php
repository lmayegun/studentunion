<?php

namespace Drupal\election\Service;

use Drupal\complex_conditions\ConditionRequirement;
use Drupal\complex_conditions\ConditionsEvaluator;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\election\Phase\PhaseInterface;
use Drupal\user\Entity\User;

/**
 * Class ElectionPostEligibilityChecker.
 */
class ElectionPostEligibilityChecker {

  /**
   * User account to check eligibility for.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  public $account;

  /**
   * Election post to check eligibility for.
   *
   * @var \Drupal\election\Entity\ElectionPostInterface
   */
  public $election_post;

  /**
   * Election post to check eligibility for.
   *
   * @var \Drupal\election\Phase\PhaseInterface
   */
  public $phase;

  public $refresh = FALSE;
  public $includePhaseStatus = FALSE;
  public $ignoreRequireUserInput = FALSE;

  public $ignoreAlreadyCompletedPhase = FALSE;
  public $ignoreElectionPublished = FALSE;
  public $ignorePostPublished = FALSE;

  /**
   *
   */
  public function __construct(AccountInterface $account = NULL, ElectionPostInterface $election_post = NULL, PhaseInterface $phase = NULL) {
    $this->account = User::load($account->id());
    $this->election_post = $election_post;
    $this->phase = $phase;
  }

  /**
   *
   */
  public function setRefresh(bool $refresh) {
    $this->refresh = $refresh;
    return $this;
  }

  /**
   *
   */
  public function setIncludePhaseStatus(bool $includePhaseStatus) {
    $this->includePhaseStatus = $includePhaseStatus;
    return $this;
  }

  /**
   *
   */
  public function setIgnoreRequireUserInput(bool $ignoreRequireUserInput) {
    $this->ignoreRequireUserInput = $ignoreRequireUserInput;
    return $this;
  }

  /**
   *
   */
  public function setIgnoreAlreadyCompletedPhase(bool $ignoreAlreadyCompletedPhase) {
    $this->ignoreAlreadyCompletedPhase = $ignoreAlreadyCompletedPhase;
    return $this;
  }

  /**
   *
   */
  public function setIgnorePostPublished(bool $ignorePostPublished) {
    $this->ignorePostPublished = $ignorePostPublished;
    return $this;
  }

  /**
   *
   */
  public function setIgnoreElectionPublished(bool $ignoreElectionPublished) {
    $this->ignoreElectionPublished = $ignoreElectionPublished;
    return $this;
  }

  /**
   * Returns TRUE or FALSE as soon as any requirement is found to be failed.
   *
   * The plan evaluateEligibility runs through all requirements to help explain to the user,
   * or act on which requirement is failed.
   *
   * For performance use this as much as possible.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\election\Entity\ElectionPostInterface $election_post
   * @param string $phase
   * @param $includePhaseStatus
   * @param $refresh
   *
   * @return [type]
   */
  public function isEligible() {
    return $this->evaluateEligibilityRequirements(TRUE);
  }

  /**
   *
   */
  public static function checkRequirementsForEligibility($requirements, $ignoreRequireUserInput = TRUE) {
    return ConditionRequirement::allPassed($requirements, $ignoreRequireUserInput);
  }

  /**
   *
   */
  public function getCacheId($booleanOnly = FALSE) {
    $cid_components = [
      'evaluateEligibilityRequirements',
      $this->election_post->id(),
      $this->account->id(),
      $this->phase->id(),
      ($this->includePhaseStatus ? 'ps' : ''),
      ($this->ignoreRequireUserInput ? 'iru' : ''),
      ($this->ignoreAlreadyCompletedPhase ? 'ipn' : ''),
      ($booleanOnly ? 'b' : ''),
    ];
    $cid = implode(':', $cid_components);
    return $cid;
  }

  /**
   * Return either TRUE, or FALSE or an array of reasons why a user cannot vote.
   *
   * Uses drupal cache and only generates these.
   *
   * @return bool|array
   */
  public function evaluateEligibilityRequirements($booleanOnly = FALSE) {
    $cid = $this->getCacheId($booleanOnly);

    // Return cache data if we have it and we're not refreshing:
    if (!$this->refresh && $cache = \Drupal::cache('election_eligibility')->get($cid)) {
      $requirements = $cache->data;
    }
    else {
      $requirements = $this->evaluateEligibilityRequirementsGetValue($booleanOnly);

      // @todo $tags = get cache tags for conditions
      // @todo get specific tags so we don't have to rely on entity changes entirely
      // For now this. We want this to be more intelligent.
      $tags = $this->election_post->getEligibilityCacheTags($this->account, $this->phase);

      if ($booleanOnly && $requirements) {
        $pass = ConditionRequirement::allPassed($requirements, TRUE);
        \Drupal::cache('election_eligibility')->set($cid, $pass, Cache::PERMANENT, $tags);
        return $pass;
      }
      else {
        \Drupal::cache('election_eligibility')->set($cid, $requirements, Cache::PERMANENT, $tags);
      }
    }
    return $requirements;
  }

  /**
   *
   */
  public function evaluateEligibilityRequirementsGetValue($booleanOnly = FALSE) {
    $election = $this->election_post->getElection();

    $titleParams = [
      '%positionName' => $this->election_post->label(),
      '%positionTypeName' => $this->election_post->getTypeNaming(),
      '%electionName' => $election->label(),
    ];

    $titleParams['@phaseName'] = $this->phase->label();
    $titleParams['@phaseAction'] = $this->phase->getAction(FALSE, TRUE);
    $titleParams['@phasePastTense'] = $this->phase->getAction(TRUE, TRUE);

    $requirements = [];

    // Publishing.
    if (!$this->ignoreElectionPublished) {
      $election_published = $election->isPublished();
      if ($booleanOnly && !$election_published) {
        return FALSE;
      }
      $requirements['election_published'] = new ConditionRequirement([
        'id' => 'election_published',
        'label' => t('%electionName election published', $titleParams),
        'pass' => $election_published,
      ]);
    }

    if ($election_published && !$this->ignorePostPublished) {
      $election_post_published = $this->election_post->isPublished();
      if ($booleanOnly && !$election_post_published) {
        return FALSE;
      }
      $requirements['election_post_published'] = new ConditionRequirement([
        'id' => 'election_post_published',
        'label' => t('%positionName %positionTypeName published', $titleParams),
        'pass' => $election_post_published,
      ]);
    }

    // Phase enabled for election.
    $electionPhases = $election->getEnabledPhases();
    if ($this->phase->id() != 'voting') {
      $phase_enabled = in_array($this->phase->id(), array_keys($electionPhases));
      if ($booleanOnly && !$phase_enabled) {
        return FALSE;
      }
      $requirements[$this->phase->id() . '_enabled'] = new ConditionRequirement([
        'id' => $this->phase->id() . '_enabled',
        'label' => t('@phaseName enabled for the %electionName election', $titleParams),
        'pass' => $phase_enabled,
      ]);
    }

    // Phase open.
    if ($this->includePhaseStatus) {
      $postStatus = $this->election_post->getPhaseStatuses(TRUE);
      $phase_open = isset($postStatus[$this->phase->id()]) && $this->election_post->checkIfStatusMeansOpen($postStatus[$this->phase->id()]);
      if ($booleanOnly && !$phase_open) {
        return FALSE;
      }
      $requirements[$this->phase->id() . '_open_election_post'] = new ConditionRequirement([
        'id' => $this->phase->id() . '_open_election_post',
        'label' => t('@phaseName must be open for %positionTypeName %positionName', $titleParams),
        'pass' => $phase_open,
      ]);
    }

    // Check if logged in:
    // @todo is there a use case for anonymous users voting?
    $logged_in = !\Drupal::currentUser()->isAnonymous();
    if ($booleanOnly && !$logged_in) {
      return FALSE;
    }
    $requirements['logged_in'] = new ConditionRequirement([
      'id' => 'logged_in',
      'label' => t('Logged in', $titleParams),
      'pass' => $logged_in,
    ]);

    if ($logged_in) {
      // Check user permissions.
      $permissions = [
        'interest' => 'express interest in posts',
        'nominations' => 'nominate for posts',
        'voting' => 'vote',
      ];

      $permission_phase = $this->account->hasPermission($permissions[$this->phase->id()]);
      if ($booleanOnly && !$permission_phase) {
        return FALSE;
      }
      $requirements['permission_' . $this->phase->id()] = new ConditionRequirement([
        'id' => 'permission_' . $this->phase->id(),
        'label' => t('User has permission to @phaseAction in elections on this website', $titleParams),
        'pass' => $permission_phase,
      ]);

      // Check if already completed phase (e.g. voted)
      if (!$this->ignoreAlreadyCompletedPhase) {
        $already = $this->checkIfUserAlreadyCompletedPhase();
        if (!is_null($already)) {
          if ($booleanOnly && $already) {
            return FALSE;
          }
          $requirements['not_already_' . $this->phase->id()] = new ConditionRequirement([
            'id' => 'not_already_' . $this->phase->id(),
            'label' => t('Must not already have @phasePastTense', $titleParams),
            'pass' => !$already,
          ]);
        }
      }
    }

    if ($this->phase->id() == 'voting') {
      // @todo check number of candidates
      $candidates = $this->election_post->countCandidatesForVoting();
      $enough_candidates = $candidates > 0;

      if ($booleanOnly && !$enough_candidates) {
        return FALSE;
      }

      $requirements['enough_candidates'] = new ConditionRequirement([
        'id' => 'enough_candidates',
        'label' => t('There must be enough hopeful candidates', $titleParams),
        'pass' => $enough_candidates,
      ]);
    }

    if (\Drupal::moduleHandler()->moduleExists('election_conditions')) {
      $conditions = $this->election_post->getConditions($this->phase);

      // Check all  conditions:
      if (count($conditions) > 0) {
        $conditionEvaluator = new ConditionsEvaluator($this->election_post, $this->account, ['phase' => $this->phase]);
        $conditionRequirements = $conditionEvaluator->executeRequirements($conditions, 'AND');
        if ($booleanOnly && ConditionRequirement::anyFailed($conditionRequirements, $this->ignoreRequireUserInput)) {
          return FALSE;
        }
        $requirements = array_merge($requirements, $conditionRequirements);
      }
    }
    return $requirements;
  }

  /**
   *
   */
  public function checkIfUserAlreadyCompletedPhase() {
    return $this->election_post->userHasCompletedPhase($this->account, $this->phase);
  }

}
