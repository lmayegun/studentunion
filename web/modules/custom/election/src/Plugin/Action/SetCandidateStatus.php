<?php

namespace Drupal\election\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Content moderation publish node.
 *
 * @Action(
 *   id = "election_candidate_set_status",
 *   label = @Translation("Set candidate status"),
 *   type = "election_candidate",
 *   confirm = TRUE
 * )
 */
class SetCandidateStatus extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['candidate_status'] = [
      '#title' => t('Status to set to'),
      '#type' => 'select',
      '#options' => ElectionCandidate::getPossibleStatuses(),
      '#default_value' => $form_state->getValue('candidate_status'),
    ];

    $form['published_status'] = [
      '#title' => t('Whether to change published status as well'),
      '#type' => 'select',
      '#options' => [
        '' => t('Leave published status as-is'),
        'publish' => t('Publish'),
        'unpublish' => t('Unpublish'),
      ],
      '#default_value' => $form_state->getValue('published_status'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity = NULL) {
    // Candidate status.
    $status = $this->configuration['candidate_status'];
    $entity->set('candidate_status', $status);

    // Published status.
    if ($this->configuration['published_status'] == 'publish') {
      $entity->set('status', 1);
    }
    elseif ($this->configuration['published_status'] == 'unpublish') {
      $entity->set('status', 0);
    }

    $entity->save();

    return $this->t(
      ':title status set to @status',
      [
        ':title' => $entity->label(),
        '@status' => ElectionCandidate::getPossibleStatuses()[$status],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'edit election candidate entities');
  }

}
