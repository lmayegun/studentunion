<?php

namespace Drupal\election\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Content moderation publish node.
 *
 * @Action(
 *   id = "election_post_publish_results",
 *   label = @Translation("Publish results"),
 *   type = "election_post",
 *   confirm = TRUE
 * )
 */
class PublishResultsForPost extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity = NULL) {
    if (!$entity->count_timestamp->value) {
      return $this->t(
        ':title  - no count has been run',
        [
          ':title' => $entity->label(),
        ]
      );
    }

    $entity->set('count_results_published', TRUE)->save();

    return $this->t(
      ':title results published',
      [
        ':title' => $entity->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'run election counts');
  }

}
