<?php

namespace Drupal\election\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Election voting method plugin plugins.
 */
interface ElectionVotingMethodInterface extends PluginInspectionInterface {

  /**
   * @return array
   *   array like [
   *   'can' => TRUE,
   *   'message' => t('Voting method has no settings and does not need setup.')
   *   ];
   */
  public function checkCanRun($entity);

}
