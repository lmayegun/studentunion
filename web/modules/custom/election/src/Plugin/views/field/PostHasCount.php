<?php

namespace Drupal\election\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Election post candidate count.
 *
 * @ViewsField("election_post_has_count")
 */
class PostHasCount extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   *
   */
  public function getCacheTags($values) {
    /** @var \Drupal\election\Entity\ElectionPostInterface $election_post */
    $election_post = $this->getEntity($values);
    return $election_post->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    /** @var \Drupal\election\Entity\ElectionPostInterface $election_post */
    $election_post = $this->getEntity($values);

    if ($election_post && $election_post->access('view')) {
      $value = $election_post->count_timestamp->value > 0;

      // Return count.
      $alias = $this->field_alias;
      $values->$alias = $value ? 'Yes' : 'No';
    }

    return parent::render($values);
  }

}
