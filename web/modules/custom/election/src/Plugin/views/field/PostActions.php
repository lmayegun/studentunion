<?php

namespace Drupal\election\Plugin\views\field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\Election;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Get whether the user has a record for a grouip.
 *
 * @ViewsField("election_post_actions")
 */
class PostActions extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['phases_to_show'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $phases = [];
    foreach (Election::getPhases() as $phase) {
      $phases[$phase->id()] = $phase->label();
    }
    $form['phases_to_show'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Election phases to include'),
      '#description' => $this->t('Selecting none shows all.'),
      '#options' => $phases,
      '#default_value' => $this->options['phases_to_show'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_entity;

    $phase_ids = [];
    foreach ($this->options['phases_to_show'] as $key => $show) {
      if ($show) {
        $phase_ids[] = $key;
      }
    }
    $phases = Election::getPhases($phase_ids);

    if (method_exists($entity, 'getElectionPost')) {
      $election_post = $entity->getElectionPost();
      $actions = $entity->getActionLinks(\Drupal::currentUser(), $phases);
    }
    else {
      $election_post = $entity;
      $actions = $election_post->getActionLinks(\Drupal::currentUser(), $phases);
    }

    $summary = [
      '#theme' => 'election_post_actions',
      '#actions' => $actions,
    ];

    $render = \Drupal::service('renderer')->render($summary);
    return new FormattableMarkup($render, []);
  }

}
