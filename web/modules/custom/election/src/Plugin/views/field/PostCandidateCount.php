<?php

namespace Drupal\election\Plugin\views\field;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\election\Entity\ElectionCandidate;
use Drupal\election\Entity\ElectionPostInterface;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;

/**
 * Election post candidate count.
 *
 * @ViewsField("election_post_candidate_count")
 */
class PostCandidateCount extends NumericField {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['statuses'] = ['default' => ['hopeful']];
    $options['publishing_state'] = ['default' => 'published'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['publishing_state'] = [
      '#type' => 'select',
      '#title' => $this->t('Publishing state'),
      '#options' => [
        'any' => t('Any'),
        'published' => t('Published candidates only'),
        // 'unpublished' => t('Unpublished candidates only'),
      ],
      '#default_value' => $this->options['publishing_state'],
    ];

    $form['statuses'] = [
      '#type' => 'select',
      '#title' => $this->t('Candidate status(es)'),
      '#multiple' => TRUE,
      '#options' => ElectionCandidate::getPossibleStatuses(),
      '#default_value' => $this->options['statuses'],
    ];
  }

  /**
   *
   */
  public function getCacheTags($values) {
    /** @var \Drupal\election\Entity\ElectionPostInterface $election_post */
    $election_post = $this->getEntity($values);
    $tags = [];
    if ($election_post) {
      $tags[] = 'election:' . $election_post->getElection()->id() . ':candidates';
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    /** @var \Drupal\election\Entity\ElectionPostInterface $election_post */
    $election_post = $this->getEntity($values);

    if ($election_post && $election_post->access('view')) {
      // Cache this value to avoid recalculating unecessarily for big views.
      $cid = __METHOD__ . $election_post->id();
      $options = '';
      foreach ($this->options as $key => $value) {
        if (is_array($value)) {
          $options .= print_r($value, TRUE);
        }
        else {
          $options .= $value;
        }
      }
      $cid .= hash('sha256', $options);

      if ($cache = \Drupal::cache()->get($cid)) {
        $count = $cache->data;
      }
      else {
        $count = $this->countCandidates($election_post, $values);

        \Drupal::cache()->set($cid, $count, Cache::PERMANENT, $this->getCacheTags($values));
      }

      // Return count.
      $alias = $this->field_alias;
      $values->$alias = $count;
    }

    return parent::render($values);
  }

  /**
   *
   */
  public function countCandidates(ElectionPostInterface $election_post, $values) {
    $candidates = $election_post->countCandidates($this->options['statuses'], $this->options['publishing_state'] == 'published');
    return $candidates;
  }

}
