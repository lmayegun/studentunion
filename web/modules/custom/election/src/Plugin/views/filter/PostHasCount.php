<?php

namespace Drupal\election\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 * Filter handler for the current user.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("election_post_has_count_filter")
 */
class PostHasCount extends BooleanOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->value_value = $this->t('Post has had count run and has result');
  }

  /**
   *
   */
  public function query() {
    $this->ensureMyTable();

    $field = $this->tableAlias . '.' . $this->realField . ' ';
    $or = $this->view->query->getConnection()->condition('OR');

    if (empty($this->value)) {
      $or->isNull($field);
    }
    else {
      $or->isNotNull($field);
    }
    $this->query->addWhere($this->options['group'], $or);
  }

}
