<?php

namespace Drupal\election\EventSubscriber;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\election\Entity\Election;
use Drupal\election\Entity\ElectionPost;
use Drupal\election\Phase\VotingPhase;
use Drupal\election\Service\ElectionPostEligibilityChecker;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 *
 */
class RedirectOn403Subscriber extends HttpExceptionSubscriberBase {

  protected $currentUser;

  /**
   *
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   *
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   *
   */
  public function on403(ExceptionEvent $event) {
    $request = $event->getRequest();
    $route_name = $request->attributes->get('_route');

    // Redirect to election post with eligibility explanation.
    $routesToControl = [
      'entity.election_ballot.add_form',
      'entity.election_candidate.add_form',
      'entity.election_candidate.edit_form',
    ];
    $phases = Election::getPhases();
    foreach ($phases as $phase) {
      $routesToControl[] = "entity.election_post." . $phase->id();
    }

    $election_route = in_array($route_name, $routesToControl);
    if (!$election_route) {
      return;
    }

    $uri = FALSE;

    if ($this->currentUser->isAnonymous()) {
      $query = $request->query->all();
      $query['destination'] = Url::fromRoute('<current>')->toString();
      $uri = Url::fromRoute('user.login', [], ['query' => $query])->toString();
    }
    else {
      $routeMatch = RouteMatch::createFromRequest($request);
      $election_post = $routeMatch->getParameters()->get('election_post');
      if (is_string($election_post)) {
        $election_post = ElectionPost::load($election_post);
      }
      if (!$election_post) {
        $election_candidate = $routeMatch->getParameters()->get('election_candidate');
        if (!$election_candidate) {
          return;
        }
        $election_post = $election_candidate->getElectionPost();
        if (!$election_post) {
          return;
        }
      }

      if ($route_name == 'entity.election_ballot.add_form') {
        $phase_id = 'voting';
      }
      elseif (strstr($route_name, 'entity.election_candidate')) {
        // @todo deal with interest...
        $phase_id = 'nominations';
      }
      else {
        $phase_id = substr($route_name, strrpos($route_name, '.') + 1);
      }

      if (!$phase_id) {
        return;
      }
      $phase = Election::getPhaseClass($phase_id);

      $eligibilityChecker = new ElectionPostEligibilityChecker($this->currentUser, $election_post, $phase);
      $eligibilityChecker->setIncludePhaseStatus(TRUE);
      $eligibilityChecker->setRefresh(TRUE);
      $requirements = $eligibilityChecker->evaluateEligibilityRequirements();

      if (!$eligibilityChecker->checkRequirementsForEligibility($requirements, FALSE)) {
        $formattedFailedRequirements = $election_post->formatEligibilityRequirements($requirements, TRUE);
        $requirementsList = [];
        foreach ($formattedFailedRequirements as $requirement) {
          $requirementsList[] = $requirement->getLabel();
        }

        $eligibilityUrl = Url::fromRoute('entity.election_post.eligibility', [
          'election_post' => $election_post->id(),
        ])->toString();

        if ($phase->id() == 'voting' && !$election_post->isOpenOrPartiallyOpen(new VotingPhase)) {
          $message = t('Voting is not currently open. You may be able to vote when it is - <a href="@eligibilityUrl">check your eligibility here</a>.', [
            '@eligibilityUrl' => $eligibilityUrl,
          ]);
        }
        elseif ($election_post->userHasCompletedPhase($this->currentUser, $phase)) {
          $vote = '';
          if ($phase->id() == 'voting') {
            $redirect = $election_post->getElection()->getRedirectToNextPost();
            $url = Url::fromRoute($redirect['route_name'], $redirect['route_parameters']);
            $vote = t(' <a href="@link">Click here to go to the next ballot you are eligible for</a>.', ['@link' => $url->toString()]);
          }
          $message = t('You have already @phasePast for @postName.@vote', [
            '@eligibilityUrl' => $eligibilityUrl,
            '@vote' => new FormattableMarkup($vote, []),
            '@phasePast' => $phase->getAction(TRUE, TRUE),
            '@postName' => $election_post->label(),
          ]);
        }
        else {
          if (count($requirementsList) == 1) {
            $message = t('@requirements', [
              '@requirements' => new FormattableMarkup(implode(', ', $requirementsList), []),
            ]);
          }
          else {
            $message = t('You cannot currently access this post due to not being eligible, based on the <a href="@eligibilityUrl">following requirements</a>: <ul><li>@requirements</li></ul>', [
              '@requirements' => new FormattableMarkup(implode('</li><li>', $requirementsList), []),
              '@eligibilityUrl' => $eligibilityUrl,
            ]);
          }
        }
        \Drupal::messenger()->addError(['#markup' => $message]);
        $uri = Url::fromRoute(
          'entity.election_post.canonical',
          [
            'election_post' => $election_post->id(),
          ],
          [
            'query' => [
              'from_access_denied' => TRUE,
            ],
          ]
        )->toString();
      }
    }

    if ($uri) {
      $returnResponse = new TrustedRedirectResponse($uri);
      $event->setResponse($returnResponse);
    }
  }

}
