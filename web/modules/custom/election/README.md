# INTRODUCTION

This is a Drupal 9 version of https://www.drupal.org/project/election

It is largely functional with some outstanding issues, namely:
- First past the post elections
- Scheduling phases - phases do not step over

## Terminology

An "election" is made up of "posts", which have "candidates" that people vote on. So for example:

Election: Leadership Race

Posts:

Activities & Engagement Officer
Arts Officer

Candidates (per post):

Activities & Engagement Officer
- James Stewart
- Maria Marie

Arts Officer
- Dorothy Flax

However, these can have different names depending on the election type, so e.g. a "referendum" could be made up of "questions" which have "options" that people can vote on. The same general logic applies even with these different names.

## Voting systems

Bundled with the module are election_fptp (First Past The Post) and election_openstv (Single Transferable Vote using the OpenSTV )

# INSTALLATION
## ELIGIBILITY CACHING

If you have complex eligibility requirements for posts (e.g. conditions) and a lot of users,
it is recommended to increase the cache size for eligibility by adding this
to settings.php or settings.local.php :

$settings['database_cache_max_rows']['bins']['election_eligibility'] = 150000;

## STATISTICS CACHING

You can also improve statistics performance via the same approach:

$settings['database_cache_max_rows']['bins']['election_statistics'] = 100000;
