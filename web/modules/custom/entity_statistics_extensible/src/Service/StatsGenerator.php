<?php

namespace Drupal\entity_statistics_extensible\Service;

use Drupal\entity_statistics_extensible\Entity\EntityStatisticsReport;
use Drupal\user\Entity\User;

class StatsGenerator {

  public function checkAccessForPlugin($plugin_definition, $account) {
    if (isset($plugin_definition['permissions']) && $plugin_definition['permissions']) {
      $access = FALSE;
      foreach ($plugin_definition['permissions'] as $permission) {
        if ($account->hasPermission($permission)) {
          $access = TRUE;
          break;
        }
      }
    } else {
      $access = $account->hasPermission('access entity statistics');
    }
    return $access;
  }

  public function getStatsForEntities($entity_ids, $plugins_to_include) {
    $denied = [];
    $account = User::load(\Drupal::currentUser()->id());

    $categories = [];
    $pluginsDenied = [];

    // Load all available stats:
    $pluginManager = \Drupal::service('plugin.manager.entity_statistics_extensible.stats_generator');
    $plugins = $pluginManager->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $plugin = $pluginManager->createInstance($plugin_id);
      $definition = $plugin->getPluginDefinition();
      if ($plugins_to_include && count($plugins_to_include) > 0 && !in_array($plugin_id, $plugins_to_include)) {
        continue;
      }

      // Check access
      $access = $this->checkAccessForPlugin($definition, $account);
      if (!$access) {
        $denied[] = ($definition['label'] ?? $plugin_id);
        // \Drupal::logger('entity_statistics_extensible')->debug($plugin_id . ' no access');
        continue;
      }

      // Categorise it
      $category = isset($definition['category']) ? $definition['category']->__toString() : 'Uncategorised';
      if (!isset($categories[$category])) {
        $categories[$category] = [];
      }

      // Set basic values for this plugin:
      $pluginValues = [
        'title' => ($definition['label'] ?? $plugin_id),
        'description' => $plugin->getDescription(),
        'multivalue' => $plugin->multipleValues,
        'weight' => $definition['weight'] ?? 0,
      ];

      // Get the data for these entities:
      $data = $plugin->getDataForEntities($entity_ids);

      // Update plugin values:
      $pluginValues['values'] = $data['statsData'];
      $pluginValues['excludedCount'] = count($data['excludedEntities']);
      $pluginValues['excludedEntitiesReasons'] = array_unique(array_values($data['excludedEntities']));
      $pluginValues['total'] = $data['total'];

      // Only include plugins with some data:
      if ($data['total'] > 0) {
        $categories[$category][] = $pluginValues;
      } else {
        // \Drupal::logger('entity_statistics_extensible')->debug($plugin_id . ' no results');
        $pluginsDenied[] = $pluginValues['title'];
      }
    }

    // @todo sort each category's plugins by weight

    // @todo tell user which plugins they were denied due to insufficient access
    // $pluginsDenied

    // Remove empty categories:
    foreach ($categories as $category => $values) {
      if (count($values) == 0) {
        unset($categories[$category]);
      }
    }

    return $categories;
  }
}
