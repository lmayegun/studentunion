<?php

/**
 * @file
 * Contains \Drupal\disc\Controller\DiscBatchController.
 */

namespace Drupal\entity_statistics_extensible\Controller;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * [Description EntityStatisticsController]
 */
class EntityStatisticsController {

  public function getChunkSize() {
    return 50;
  }

  public function batch($entity_statistics_set) {
    $entities = [];
    $tempstore = \Drupal::service('tempstore.private')->get('entity_statistics_extensible');

    // Load relevant bits from tempstore:
    $data = $tempstore->get($entity_statistics_set);

    $entities = $data['entity_ids'];
    $return_url = $data['url'];
    $plugins_to_include = $data['plugins_to_include'];

    $operations = [];

    $chunks = array_chunk($entities, $this->getChunkSize());
    foreach ($chunks as $chunk) {
      $operations[] = [
        '\Drupal\entity_statistics_extensible\Controller\EntityStatisticsController::generateStats',
        [
          $entity_statistics_set,
          $chunk,
          $plugins_to_include,
        ],
      ];
    }

    $batch = [
      'title' => t('Calculating statistics...'),
      'operations' => $operations,
      'finished' => '\Drupal\entity_statistics_extensible\Controller\EntityStatisticsController::finished',
    ];

    batch_set($batch);

    return batch_process($return_url);
  }

  public static function finished($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if (!isset($results['entities']) || !$results['entities']) {
      $message = t('No records / entities found.');
    } else if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results['entities']),
        'One processed.',
        '@count processed.'
      );

      $tempstore = \Drupal::service('tempstore.private')->get('entity_statistics_extensible');
      $set_id = $results['set'];

      $options = [];
      $tempstore->set($set_id . '_build', static::build($options, $results['entities'], $results['plugins_to_include']));
    } else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

  public static function generateStats($entity_statistics_set, $entities, $plugins_to_include, &$context) {

    if (!isset($context['results']['entities'])) {
      $context['results']['entities'] = [];
    }

    $plugins = [];
    $pluginManager = \Drupal::service('plugin.manager.entity_statistics_extensible.stats_generator');
    $pluginsLoaded = $pluginManager->getDefinitions();
    foreach ($pluginsLoaded as $plugin_id => $plugin) {
      $plugin = $pluginManager->createInstance($plugin_id);
      $definition = $plugin->getPluginDefinition();
      if ($plugins_to_include && count($plugins_to_include) > 0 && !in_array($plugin_id, $plugins_to_include)) {
        continue;
      }
      $plugins[] = $plugin;
    }

    foreach ($entities as $entity) {
      $entity_type_id = $entity[0];
      $entity_id = $entity[1];

      // Generate data for entity and cache it
      foreach ($plugins as $plugin) {
        $plugin->getCachedEntityData($entity_type_id, $entity_id);
        // \Drupal::logger('entity_statistics_extensible')->debug($plugin->getPluginId() . ' - ' . $entity_type_id . ' - ' . $entity_id);
      }

      $context['results']['entities'][] = [
        $entity_type_id,
        $entity_id,
      ];
    }

    if (!isset($context['results']['set'])) {
      $context['results']['set'] = $entity_statistics_set;
      $context['results']['plugins_to_include'] = $plugins_to_include;
    }

    if (!isset($context['sandbox']['total_processed'])) {
      $context['sandbox']['total_processed'] = 0;
    }
    $context['sandbox']['total_processed'] += count($entities);
    $context['message'] = t('Processed @total_processed', [
      '@total_processed' => $context['sandbox']['total_processed']
    ]);
  }

  public static function build($options, $entity_ids, $plugins_to_include) {
    $build = [];

    // Causing rendering before render issue:
    $statsGenerator = \Drupal::service('entity_statistics_extensible.generator');
    $categories = $statsGenerator->getStatsForEntities($entity_ids, $plugins_to_include);

    // Save as report
    // EntityStatisticsReport::createFromGeneratedStats($this->view, $categories, $this->displayHandler->getOption('entity_type'));

    $build[] = [
      '#theme' => 'views_entity_statistics_extensible',
      '#categories' => $categories,
      '#charts_twig_enabled' => \Drupal::moduleHandler()->moduleExists('charts_twig'),
    ];

    return $build;
  }
}
