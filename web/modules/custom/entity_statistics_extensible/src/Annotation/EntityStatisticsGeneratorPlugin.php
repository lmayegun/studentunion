<?php

namespace Drupal\entity_statistics_extensible\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Note that the "@ Annotation" line below is required and should be the last
 * line in the docblock. It's used for discovery of Annotation definitions.
 *
 * @Annotation
 */
class EntityStatisticsGeneratorPlugin extends Plugin
{
  
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the formatter type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;
  
  /**
   * A short description of the formatter type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * Category
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $category;

  /**
   * Weight
   *
   * @ingroup plugin_translatable
   *
   * @var int
   */
  public $weight;

  /**
   * Permissions
   *
   * @var array
   */
  public $permissions;

}
