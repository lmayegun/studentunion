<?php

namespace Drupal\entity_statistics_extensible\Plugin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_statistics_extensible\Plugin\EntityStatisticsGeneratorPluginInterface;

/**
 * A base class to help developers implement their own plugins.
 */
abstract class EntityStatisticsGeneratorPluginBase extends PluginBase implements EntityStatisticsGeneratorPluginInterface, ContainerFactoryPluginInterface {
  public $multipleValues = false;
  public $entityTypeId = [];
  public $sort = false;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  public function getValidEntityTypes() {
    if (is_array($this->entityTypeId)) {
      return $this->entityTypeId;
    }
    return [$this->entityTypeId];
  }

  public function checkEntityApplicability(string $entity_type_id, int $entity_id) {
    if ($this->entityTypeId) {
      if (in_array($entity_type_id, $this->getValidEntityTypes())) {
        return [
          'applicable' => TRUE,
        ];
      }

      \Drupal::logger('entity_statistics_extensible')->debug($entity_type_id . ' - ' . $entity_id . ' - ' . 'not applicable' . print_r($this->getValidEntityTypes(), TRUE));
      return [
        'applicable' => FALSE,
        'reason' => 'Statistic not applicable to entity type'
      ];
    }
  }

  public function getPossibleDataValues() {
    return [];
  }

  public function getDescription() {
    return $this->pluginDefinition['description'] ?? '';
  }

  public function getPopulationData() {
    $values = array_fill_keys($this->getPossibleDataValues(), 0);
    return $values;
  }

  public function getDataForEntities(array $entities) {
    $statsData = array_fill_keys($this->getPossibleDataValues(), 0);

    $entityData = [];
    $excludedEntities = [];
    $total = 0;
    foreach ($entities as $entity) {
      $entity_type_id = $entity[0];
      $entity_id = $entity[1];

      $applicability = $this->checkEntityApplicability($entity_type_id, $entity_id);
      if (!$applicability['applicable']) {
        $excludedEntities[$entity_id] = $applicability['reason'];
        // \Drupal::logger('entity_statistics_extensible')->debug($this->getPluginId() . ' not applicable ' . $entity_type_id . ' - ' . print_r($this->getValidEntityTypes(), TRUE));
        continue;
      }

      $valueArray = $this->getCachedEntityData($entity_type_id, $entity_id);
      if (!$this->multipleValues || !is_array($valueArray)) {
        $valueArray = [$valueArray];
      }

      foreach ($valueArray as $value) {
        if (!isset($statsData[$value])) {
          $statsData[$value] = 0;
          // OR @todo error or something about value not in possible values
        }
        $statsData[$value]++;
      }

      $entityData[$entity_id] = $valueArray;

      $total++;
    }

    $this->sort($statsData);

    return [
      'statsData' => $statsData,
      'entityData' => $entityData,
      'excludedEntities' => $excludedEntities,
      'total' => $total,
    ];
  }

  public function sort(&$statsData) {
    if ($this->sort == 'desc') {
      arsort($statsData);
    } elseif ($this->sort == 'asc') {
      rsort($statsData);
    }
  }

  public function getCachedEntityData($entity_type_id, $entity_id) {
    $cid = $this->getCacheId(__METHOD__ . '-' . $entity_type_id . '-' . $entity_id);
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);

    if (!$entity) {
      return NULL;
    }

    $startTime = \Drupal::time()->getCurrentMicroTime();
    $data = $this->generateEntityData($entity);

    $diff = \Drupal::time()->getCurrentMicroTime() - $startTime;
    if ($diff > 0.1) {
      \Drupal::logger('statistics')->debug(get_class($this) . ' - ' . $diff);
    }

    \Drupal::cache()->set($cid, $data, Cache::PERMANENT, $entity->getCacheTags());
    return $data;
  }

  public function generateEntityData(EntityInterface $entity) {
    return $entity->id();
  }

  public function getCacheId($functionName) {
    return 'EntityStatisticsGeneratorPlugin:' . $this->getPluginId() . ':' . $functionName;
  }
}
