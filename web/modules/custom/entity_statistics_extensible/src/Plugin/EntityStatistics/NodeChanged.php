<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\NodeFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "node_changed",
 *   label = @Translation("Content last updated"),
 *   description = @Translation("Content (node) edited within the last..."),
 *   category = @Translation("Content"),
 *   weight = 10,
 * )
 **/
class NodeChanged extends NodeFieldBase {

  public $field = 'changed';

  public function getPossibleDataValues() {
    return [
      'last day',
      'last week',
      'last month',
      'last 3 months',
      'last year',
      'last 2 years',
      'more than two years ago',
      'never'
    ];
  }

  public function generateEntityData(EntityInterface $entity) {
    $timestamp = $entity->get($this->field)->value;
    if ($timestamp > strtotime('-1 day')) {
      return t('last day');
    }
    if ($timestamp > strtotime('-1 week')) {
      return t('last week');
    }
    if ($timestamp > strtotime('-1 month')) {
      return t('last month');
    }
    if ($timestamp > strtotime('-3 months')) {
      return t('last 3 months');
    }
    if ($timestamp > strtotime('-1 year')) {
      return t('last year');
    }
    if ($timestamp > strtotime('-2 years')) {
      return t('last 2 years');
    }
    if ($timestamp <= strtotime('-2 years')) {
      return t('more than two years ago');
    }
    if ($timestamp == 0 || !$timestamp) {
      return t('never');
    }
  }
}
