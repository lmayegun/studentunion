<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases;

use Drupal\group\Entity\GroupRole;
use Drupal\entity_statistics_extensible\Plugin\EntityStatisticsGeneratorPluginBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

abstract class GroupMembershipRecordBase extends GroupRoleBase {
    public $enabledRecord = true;
    public $currentRecord = true;
}