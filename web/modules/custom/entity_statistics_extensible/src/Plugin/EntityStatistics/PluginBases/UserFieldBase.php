<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases;

use Drupal\entity_statistics_extensible\Plugin\EntityStatisticsGeneratorPluginBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

abstract class UserFieldBase extends EntityFieldBase {
  public $entityTypeId = ['user'];
  public $entityClass = "Drupal\user\Entity\User";
}
