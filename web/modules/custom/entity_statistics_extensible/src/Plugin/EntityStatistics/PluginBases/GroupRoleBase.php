<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases;

use Drupal\Core\Cache\Cache;
use Drupal\group\Entity\GroupRole;
use Drupal\entity_statistics_extensible\Plugin\EntityStatisticsGeneratorPluginBase;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

abstract class GroupRoleBase extends EntityStatisticsGeneratorPluginBase {

  public $entityTypeId = 'user';

  public $groupRoleId;
  public $groupTypeId;
  public $valueHasRole = 'has role';
  public $valueNoRole = 'does not have role';

  public function getPossibleDataValues() {
    return [$this->valueHasRole, $this->valueNoRole];
  }

  public function generateEntityData($entity) {
    $groupIds = $this->getGroupIds();
    return $this->checkRole($entity, $groupIds) ? $this->valueHasRole : $this->valueNoRole;
  }

  public function checkRole($user, $groupIds) {
    if (!is_array($this->groupRoleId)) {
      $this->groupRoleId = [$this->groupRoleId];
    }

    $manager = \Drupal::service('group_membership_record.manager');

    foreach ($this->groupRoleId as $roleId) {
      if ($manager->hasRoleFromIds([$user->id()], $this->groupTypeId, $groupIds, [$roleId])) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function getGroupIds() {
    $groups = [];
    if ($this->groupTypeId) {
      $gids = \Drupal::entityQuery('group')->condition('type', $this->groupTypeId)->execute();
      $groups = $gids;
    } else {
      $groups = [];
    }

    return $groups;
  }
}
