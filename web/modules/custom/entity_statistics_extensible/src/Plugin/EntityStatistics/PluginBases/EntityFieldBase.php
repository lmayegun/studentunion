<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_statistics_extensible\Plugin\EntityStatisticsGeneratorPluginBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

abstract class EntityFieldBase extends EntityStatisticsGeneratorPluginBase {
  // Required
  public $field;
  public $useField = true;
  public $entityTypeId;
  public $entityClass;

  // not required
  public $entityBundleId;

  /**
   * Some inheriting classes may want to check an entity other than the one being passed
   *
   * e.g. a profile for a user
   */
  public function getEntityToUse(EntityInterface $entity) {
    return $entity;
  }

  public function checkEntityApplicability(string $entity_type_id, int $entity_id) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    if (!$entity) {
      return [
        'applicable' => FALSE,
        'reason' => 'Cannot find entity.'
      ];
    }

    $entity = $this->getEntityToUse($entity);

    if (!$entity) {
      return [
        'applicable' => FALSE,
        'reason' => 'Related entity not found of type ' . $this->entityTypeId,
      ];
    }

    if ($this->useField && !$entity->hasField($this->field)) {
      return [
        'applicable' => FALSE,
        'reason' => 'Entity does not have field ' . $this->field,
      ];
    }

    if (in_array($entity_type_id, $this->getValidEntityTypes())) {
      if ($this->entityBundleId && $entity->bundle() != $this->entityBundleId) {
        return [
          'applicable' => FALSE,
          'reason' => 'Entity not of bundle type ' . $entity->entityBundleId,
        ];
      }

      return [
        'applicable' => TRUE,
      ];
    }

    return [
      'applicable' => FALSE,
      'reason' => 'Statistic not applicable to entity type'
    ];
  }

  /**
   */
  public function getPossibleDataValues() {
    $cid = $this->getCacheId(__METHOD__);
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $values = [];

    // @todo we need to do all entities so we don't miss possible values.
    // For entities OUTSIDE the population we're testing.
    // However this is far too expensive...
    //
    // $entities = $this->entityClass::loadMultiple();
    // foreach ($entities as $entity) {
    //   $values[] = $this->generateEntityData($entity);
    // }
    // array_filter($values);

    \Drupal::cache()->set($cid, $values, Cache::PERMANENT, [$this->entityTypeId . '_list']);

    return $values;
  }

  public function generateEntityData(EntityInterface $entity) {
    $cid = $this->getCacheId(implode(':', [
      __METHOD__,
      $entity->id(),
      $entity->getEntityTypeId(),
    ]));
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    if (!$entity) {
      return NULL;
    }

    $entity = $this->getEntityToUse($entity);
    if (!$entity || !$entity->hasField($this->field)) {
      // \Drupal::logger('entity_statistics_extensible')->debug($entity->getEntityTypeId() . ' does not have field ' . $this->field);
      return NULL;
    }
    $value = $this->getEntityFieldValue($entity, $this->field);

    // \Drupal::logger('entity_statistics_extensible')->debug($entity->getEntityTypeId() . ' - ' . $value);

    \Drupal::cache()->set($cid, $value, Cache::PERMANENT, $entity->getCacheTags());

    return $value;
  }

  public function getEntityFieldValue($entity, $field) {
    return $entity->get($this->field)->value;
  }
}
