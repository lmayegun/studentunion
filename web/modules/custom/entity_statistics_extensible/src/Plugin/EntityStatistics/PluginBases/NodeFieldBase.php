<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases;

abstract class NodeFieldBase extends EntityFieldBase
{
    public $entityTypeId = 'node';
    public $entityClass = "Drupal\node\Entity\Node";
}
