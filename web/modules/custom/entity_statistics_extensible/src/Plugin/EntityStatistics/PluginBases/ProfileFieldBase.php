<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases;

use Drupal\profile\Entity\Profile;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\UserFieldBase;

abstract class ProfileFieldBase extends UserFieldBase {

  public $entityTypeId = 'profile';
  public $entityClass = "Drupal\profile\Entity\Profile";

  // Profile type
  public $entityBundleId = NULL;

  public function getEntityToUse($entity) {
    if ($entity && $entity->getEntityTypeId() == 'user') {
      return \Drupal::entityTypeManager()->getStorage($this->entityTypeId)->loadByUser($entity, $this->entityBundleId);
    }
    return NULL;
  }

  public function getValidEntityTypes() {
    return [$this->entityTypeId, 'user'];
  }
}
