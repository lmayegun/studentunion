<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics;

use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\UserFieldBase;
use Drupal\user\Entity\Role;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "user_last_login",
 *   label = @Translation("User last login"),
 *   description = @Translation("User logged in to the website within the last..."),
 *   category = @Translation("User site activity"),
 *   weight = 10,
 * )
 **/
class UserLastLogin extends UserFieldBase {

  public $field = 'login';

  public function getPossibleDataValues() {
    return [
      'last day',
      'last week',
      'last month',
      'last 3 months',
      'last year',
      'last 2 years',
      'more than two years ago',
      'never',
    ];
  }

  public function generateEntityData($entity) {
    $timestamp = $entity->get($this->field)->value;
    if ($timestamp > strtotime('-1 day')) {
      return 'last day';
    }
    if ($timestamp > strtotime('-1 week')) {
      return 'last week';
    }
    if ($timestamp > strtotime('-1 month')) {
      return 'last month';
    }
    if ($timestamp > strtotime('-3 months')) {
      return 'last 3 months';
    }
    if ($timestamp > strtotime('-1 year')) {
      return 'last year';
    }
    if ($timestamp > strtotime('-2 years')) {
      return 'last 2 years';
    }
    if ($timestamp <= strtotime('-2 years')) {
      return 'more than two years ago';
    }
    if ($timestamp == 0 || !$timestamp) {
      return 'never';
    }
  }
}
