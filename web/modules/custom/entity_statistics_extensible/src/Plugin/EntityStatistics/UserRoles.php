<?php

namespace Drupal\entity_statistics_extensible\Plugin\EntityStatistics;

use Drupal\Core\Cache\Cache;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\UserFieldBase;
use Drupal\user\Entity\Role;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "user_roles",
 *   label = @Translation("User roles"),
 *   description = @Translation("Roles the user has on this website, each of which will grant certain sets of permissions."),
 *   category = @Translation("User site activity"),
 *   permissions = {
 *     "access sensitive entity statistics",
 *   },
 *   weight = 10,
 * )
 **/
class UserRoles extends UserFieldBase {
  public $useField = false;
  public $multipleValues = TRUE;
  public $sort = 'desc';

  public function getPossibleDataValues() {
    $cid = $this->getCacheId(__METHOD__);
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $values = [];
    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      if (!in_array($role->id(), ['anonymous', 'authenticated'])) {
        $values[] = $role->label();
      }
    }
    array_filter($values);

    \Drupal::cache()->set($cid, $values, Cache::PERMANENT, ['user_role_list']);

    return $values;
  }

  public function generateEntityData($entity) {
    $array = [];
    $roles = $entity->getRoles();
    foreach ($roles as $role_id) {
      $role = Role::load($role_id);
      $array[] = $role ? $role->label() : $role_id;
    }
    return $array;
  }
}
