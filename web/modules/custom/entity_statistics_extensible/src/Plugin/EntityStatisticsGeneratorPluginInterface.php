<?php

namespace Drupal\entity_statistics_extensible\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

interface EntityStatisticsGeneratorPluginInterface extends PluginInspectionInterface {

  /**
   * Get the possible values that an entity could have for this data point
   */
  public function getPossibleDataValues();

  /**
   * Return an array keyed by the entity ID
   */
  public function getDataForEntities(array $entities);

  /**
   * Check if this statistic is relevant for this entity,
   * return an array keyed with a boolean 'applicable' and a string 'reason'
   *
   *  return [
   *       'applicable' => false,
   *       'reason' => 'Statistic not applicable to entity type'
   *  ];
   */
  public function checkEntityApplicability(string $entity_type_id, int $entity_id);

  /**
   * Returns a unique cache identifier for the plugin with a function name appended
   */
  public function getCacheId($functionName);
}
