<?php

namespace Drupal\entity_statistics_extensible\Plugin\views\display;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\display\PathPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Route;

/**
 * Display plugin to attach multiple chart configurations to the same chart.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "entity_statistics_extensible",
 *   title = @Translation("Entity statistics"),
 *   help = @Translation("Display that produces statistical info."),
 *   theme = "views_entity_statistics_extensible",
 *   uses_route = TRUE,
 *   returns_response = FALSE
 * )
 */
class EntityStatisticsDisplay extends PathPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesMore = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $showAdminLinks = FALSE;

  public function preExecute() {
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $minToCalculate = $this->getOption('min_records');
    $entity_type = $this->getOption('entity_type');

    // We need to load all results to set the batch up to process them.
    $this->view->setItemsPerPage(0);

    // However for performance reasons for large views, we do NOT want to load ALL entities.
    // $this->view->build();
    // $viewQuery = $this->view->getQuery();
    // $viewQuery->build($this->view);
    // $query = $viewQuery->query();
    // dd($query->execute());

    // $raw_ids = [];

    // $entity_ids = [];
    // foreach ($raw_ids as $id) {
    //   $entity_ids[] = [$entity_type, $id];
    // }

    $this->view->execute();

    $entities = $this->getEntitiesForStats();
    // dd($entities);

    $entity_ids = [];
    foreach ($entities as $entity) {
      $entity_ids[] = [$entity->getEntityTypeId(), $entity->id()];
    }

    $render = [];

    // Fail out if too few:
    if ($minToCalculate > 0 && count($entity_ids) < $minToCalculate) {
      return ['#markup' => Markup::create($this->t('Cannot generate statistics: too few records returned (@count, needed at least @min)', [
        '@count' => count($entity_ids),
        '@min' => $minToCalculate,
      ])->__toString())];
    }

    // Restrict plugins if the view display is set to:
    $plugins_to_include = NULL;
    if ($this->getOption('plugins_to_include')) {
      $plugins_to_include = array_values($this->getOption('plugins_to_include'));
      $plugins_to_include = array_filter($plugins_to_include);
    }

    $unique_id = ($this->view->id() ?? 'no_id') . serialize($entity_ids);
    $set_id = md5($unique_id);

    $tempstore = \Drupal::service('tempstore.private')->get('entity_statistics_extensible');

    if (!$tempstore->get($set_id . '_build')) {
      $tempstore->set($set_id, [
        'entity_ids' => $entity_ids,
        'plugins_to_include' => $plugins_to_include,
        'url' => \Drupal::request()->getRequestUri(),
      ]);

      $url = Url::fromRoute('entity_statistics_extensible.batch', ['entity_statistics_set' => $set_id])->toString();
      $response = new RedirectResponse($url);
      $response->send();
      return [
        '#markup' => 'Loading...',
        '#cache' => [
          // 'contexts' => ['url'],
          'max-age' => 0,
        ],
      ];
    }

    $build = $tempstore->get($set_id . '_build');
    $render = \Drupal::service('renderer')->render($build);
    return [
      '#markup' => $render,
      '#cache' => [
        // 'contexts' => ['url'],
        'max-age' => 0,
      ],
    ];
  }

  public function getEntitiesForStats() {
    $entities = [];

    $entity_type = $this->getOption('entity_type');

    if (!$entity_type) {
      $entity_type = 'base';
    }

    foreach ($this->view->result as $result) {
      if ($entity_type == 'base') {
        $entities[] = $result->_entity;
      } else {
        $entities[] = $result->_relationship_entities[$entity_type];
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['entity_type'] = ['default' => ''];
    $options['min_records'] = ['default' => 10];
    $options['plugins_to_include'] = ['default' => []];

    // Set the default style plugin to 'entity_statistics_extensible'.
    $options['style']['contains']['type']['default'] = 'entity_statistics_extensible';
    $options['defaults']['default']['style'] = FALSE;

    // We don't want to use pager as it doesn't make any sense. But it cannot
    // just be removed from a view as it is core functionality. These values
    // will be controlled by custom configuration.
    $options['pager']['contains'] = [
      'type' => ['default' => 'none'],
      'options' => ['default' => ['offset' => 0]],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    $categories['page']['title'] = $this->t('Statistics page settings');

    $options['entity_types'] = [
      'category' => 'entity_type',
      'title' => $this->t('Entity type'),
      'value' => $this->getOption('entity_type'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);


    switch ($form_state->get('section')) {
      case 'path':
        $entity_types = $this->getPossibleEntities();
        $form['entity_type'] = [
          '#type' => 'select',
          '#title' => $this->t('Select entity type to run statistics for'),
          '#options' => $entity_types,
          '#default_value' => $this->options['entity_type'] ?? 'base',
        ];

        $form['min_records'] = [
          '#type' => 'number',
          '#title' => $this->t('Minimum number of records for calculation'),
          '#description' => $this->t('0 for unlimited. For data protection purposes, it is important to limit this to a number that will prevent individuals being identified.'),
          '#default_value' => $this->options['min_records'] ?? 10,
        ];

        $plugin_types = [];
        $pluginManager = \Drupal::service('plugin.manager.entity_statistics_extensible.stats_generator');
        $plugins = $pluginManager->getDefinitions();
        foreach ($plugins as $id => $plugin) {
          $plugin_types[$id] = $plugin['label'];
        }
        $form['plugins_to_include'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Statistics plugins to display'),
          '#description' => $this->t('Select none to include all. Note not all plugins apply to all entity types.'),
          '#options' => $plugin_types,
          '#default_value' => $this->options['plugins_to_include'] ?? [],
        ];
        break;
    }
    // @todo allow choosing which plugins to show for the chosen entity

    // @todo button on attached display titled the view title
  }

  public function getPossibleEntities() {
    $relationships = $this->view->display_handler->getOption('relationships');
    $relationship_options = [
      'base' => $this->view->getBaseEntityType()->getLabel() . ' [base entity]'
    ];

    foreach ($relationships as $relationship) {
      $relationship_handler = Views::handlerManager('relationship')->getHandler($relationship);
      // ignore invalid/broken relationships.
      if (empty($relationship_handler)) {
        continue;
      }

      // If this relationship is valid for this type, add it to the list.
      $data = Views::viewsData()->get($relationship['table']);
      if (isset($data[$relationship['field']]['relationship']['base']) && $base = $data[$relationship['field']]['relationship']['base']) {
        $relationship_handler->init($this->view, $this->view->display_handler, $relationship);
        $relationship_options[$relationship['id']] = $relationship_handler->adminLabel();
      }
    }
    return $relationship_options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);
    $this->setOption('entity_type', $form_state->getValue('entity_type'));
    $this->setOption('min_records', $form_state->getValue('min_records'));
    $this->setOption('plugins_to_include', $form_state->getValue('plugins_to_include'));
  }

  /**
   * {@inheritdoc}
   */
  public static function buildBasicRenderable($view_id, $display_id, array $args = [], Route $route = NULL) {
    $build = parent::buildBasicRenderable($view_id, $display_id, $args);

    if ($route) {
      $build['#view_id'] = $route->getDefault('view_id');
      $build['#view_display_plugin_id'] = $route->getOption('_view_display_plugin_id');
      $build['#view_display_show_admin_links'] = $route->getOption('_view_display_show_admin_links');
    } else {
      throw new \BadFunctionCallException('Missing route parameters.');
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function attachTo(ViewExecutable $clone, $display_id, array &$build) {
    $displays = $this->getOption('displays');
    if (empty($displays[$display_id])) {
      return;
    }

    // Defer to the feed style; it may put in meta information, and/or
    // attach a feed icon.
    $clone->setArguments($this->view->args);
    $clone->setDisplay($this->display['id']);
    $clone->buildTitle();
    if ($plugin = $clone->display_handler->getPlugin('style')) {
      if (method_exists($plugin, 'attachTo')) {
        $plugin->attachTo($build, $display_id, $clone->getUrl(), $clone->getTitle());
        foreach ($clone->feedIcons as $feed_icon) {
          $this->view->feedIcons[] = $feed_icon;
        }
      }
    }

    // Clean up.
    $clone->destroy();
    unset($clone);
  }

  /**
   * {@inheritdoc}
   */
  public function usesLinkDisplay() {
    return TRUE;
  }
}
