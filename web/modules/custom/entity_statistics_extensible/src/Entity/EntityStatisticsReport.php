<?php

namespace Drupal\entity_statistics_extensible\Entity;

use DateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\views\Entity\View;
use Drupal\views\ViewExecutable;

/**
 * Defines the Group membership record entity.
 *
 * @ingroup entity_statistics_extensible
 *
 * @ContentEntityType(
 *   id = "entity_statistics_report",
 *   label = @Translation("Entity statistics report"),
 *   label_singular = @Translation("entity statistics report"),
 *   label_plural = @Translation("entity statistics reports"),
 *   label_collection = @Translation("Entity statistics reports"),
 *   label_count = @PluralTranslation(
 *     singular = "@count statistics report",
 *     plural = "@count statistics reports"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_statistics_extensible\EntityStatisticsReportListBuilder",
 *
 *     "form" = {
 *       "default" = "Drupal\group_membership_record\Form\GroupMembershipRecordForm",
 *       "delete" = "Drupal\group_membership_record\Form\GroupMembershipRecordDeleteForm",
 *     },
 *   },
 *   base_table = "entity_statistics_report",
 *   translatable = FALSE,
 *   admin_permission = "administer entity statistics reports entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/entity_statistics/report/{group_membership_record}",
 *     "delete-form" = "/admin/entity_statistics/report/{group_membership_record}/delete",
 *     "collection" = "/admin/entity_statistics/report",
 *   },
 * )
 */
class EntityStatisticsReport extends ContentEntityBase
{
    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
    {
        parent::preCreate($storage_controller, $values);
        $values += [
            'user_id' => \Drupal::currentUser()->id(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Created by'))
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', FALSE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        // Unique identifiers:

        $fields['dataset_id'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Dataset ID'))
            ->setDisplayConfigurable('view', FALSE)
            ->setDisplayConfigurable('form', FALSE);

        $fields['view_id'] = BaseFieldDefinition::create('string')
            ->setLabel(t('View ID'))
            ->setDisplayConfigurable('view', FALSE)
            ->setDisplayConfigurable('form', FALSE);

        $fields['view_display'] = BaseFieldDefinition::create('string')
            ->setLabel(t('View ID'))
            ->setDisplayConfigurable('view', FALSE)
            ->setDisplayConfigurable('form', FALSE);


        $fields['entity_type'] = BaseFieldDefinition::create('string')
            ->setLabel(t('View ID'))
            ->setDisplayConfigurable('view', FALSE)
            ->setDisplayConfigurable('form', FALSE);

        // Other info

        $fields['path'] = BaseFieldDefinition::create('uri')
            ->setLabel(t('Path'))
            ->setDisplayConfigurable('view', FALSE)
            ->setDisplayConfigurable('form', FALSE);

        $fields['data'] = BaseFieldDefinition::create('map')
            ->setLabel(t('Data'))
            ->setDisplayConfigurable('view', FALSE)
            ->setDisplayConfigurable('form', FALSE);

        return $fields;
    }

    public static function getDatasetID(ViewExecutable $view)
    {
        $id = $view->id();
        $display = $view->display_handler->display['id'];
        $filters = implode('-', $view->getExposedInput());
        $contextual_filters = '';
        return hash('sha256', implode('-', [$id, $display, $filters, $contextual_filters]));
    }

    public static function createFromGeneratedStats(ViewExecutable $view, array $categories, string $entity_type)
    {
        $report = static::create([
            'dataset_id' => static::getDatasetID($view),
            'view_id' => $view->id(),
            'view_display' => $view->display_handler->display['id'],
            'entity_type' => $entity_type,
            'path' => '',
            'data' => $categories
        ]);
        $report->save();
        return $report;
    }
}
