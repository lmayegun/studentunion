<?php

namespace Drupal\entity_statistics_extensible;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\user\Entity\User;

/**
 * Defines a class to build a listing of entity_statistics_extensible report entities.
 *
 * @ingroup entity_statistics_extensible
 */
class EntityStatisticsReportListBuilder extends EntityListBuilder
{

    /**
     * {@inheritdoc}
     */
    public function buildHeader()
    {
        $header['id'] = $this->t('Report ID');
        $header['view_id'] = $this->t('View ID');
        $header['created'] = $this->t('Created');
        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity)
    {
        /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
        $row['id'] = $entity->id();
        $row['view_id'] = $entity->view_id->value;

        $created = new DrupalDateTime($entity->get('created')->value);
        \Drupal::service('date.formatter')->format($created->getTimestamp(), 'short');

        return $row + parent::buildRow($entity);
    }
}
