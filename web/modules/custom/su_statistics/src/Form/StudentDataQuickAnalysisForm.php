<?php

namespace Drupal\su_statistics\Form;

use DateTime;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\csv_import_export\Form\BatchDownloadCSVForm;
use Drupal\csv_import_export\Form\BatchImportCSVForm;
use Drupal\su_students\Form\StudentDataExportForm;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Allow user to upload a CSV and be shown an analysis of the students
 */
class StudentDataQuickAnalysisForm extends StudentDataExportForm {

  const minRowsForAnonymous = 15;
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['description'] = [
      '#markup' => '<p>Upload a CSV file with at least one identifying column including a UCL user ID, UCL e-mail, or UPI. The website will then match that ID with any currently enrolled students and return you a page of statistical charts and tables for e.g. student gender, study mode, fee status etc.</p>'
    ];
    $form['all_students'] = [
      '#markup' => '<a href="/statistics/union/our-membership" class="btn button">Analysis of all Union members</a>',
      '#weight' => -25,
    ];
    unset($form['export_type']);
    unset($form['data']['share']);
    unset($form['data']['delete']);
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $validators = ['file_validate_extensions' => ['csv']];
    $file = file_save_upload('file_upload', $validators, FALSE, NULL, FileSystemInterface::EXISTS_REPLACE)[0];

    $importForm = new BatchImportCSVForm();
    $array = $importForm->convertCSVtoArray($file->getFileUri());

    if (!$array) {
      return FALSE;
    }

    if (count($array) < static::minRowsForAnonymous) {
      \Drupal::messenger()->addError(t('To avoid the risk of the data being de-anonymised and being used to identify individual students, you cannot run an anonymised export with fewer than @min rows - you provided a CSV containing @count rows.', [
        '@min' => static::minRowsForAnonymous,
        '@count'      => count($array),
      ]));
      return FALSE;
    }

    foreach ($array as $row) {
      $entity_ids[] = ['user', $this->getUserFromRow($row)];
    }
    $entity_ids = array_filter($entity_ids);

    $plugins_to_include = NULL;

    $tempstore = \Drupal::service('tempstore.private')->get('entity_statistics_extensible');
    $unique_id = 'quick' . \Drupal::currentUser()->id() . serialize($entity_ids);
    $set_id = md5($unique_id);

    $tempstore->set($set_id, [
      'entity_ids' => $entity_ids,
      'plugins_to_include' => $plugins_to_include,
      'url' => Url::fromRoute('su_statistics.student_quick_analysis_result', ['set_id' => $set_id])->toString(),
    ]);

    $url = Url::fromRoute('entity_statistics_extensible.batch', ['entity_statistics_set' => $set_id])->toString();
    $response = new RedirectResponse($url);
    $response->send();
    return NULL;
  }
}
