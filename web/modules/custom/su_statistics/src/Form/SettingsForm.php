<?php

namespace Drupal\su_statistics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'su_statistics.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('su_statistics.settings');
    $form['start_of_academic_year'] = [
      '#type' => 'date',
      '#title' => $this->t('Start of academic year for statistics purposes'),
      '#description' => $this->t('DO NOT CHANGE ONCE SNAPSHOTS HAVE STARTED BEING GENERATED. You would need to regenerate all historical stats (not always possible). Otherwise the academic years will not be comparable. Note the year here is ignored, just the day and month is used.'),
      '#default_value' => $config->get('start_of_academic_year'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    $this->config('su_statistics.settings')
      ->set('start_of_academic_year', $form_state->getValue('start_of_academic_year'))
      ->save();
  }
}
