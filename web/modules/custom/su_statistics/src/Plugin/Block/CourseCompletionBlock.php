<?php

namespace Drupal\su_statistics\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;

/**
 * Provides a course completion block, ranking user completion against an EntityStatistics result.
 *
 *
 *
 * @Block(
 *   id = "su_statistics_course_completion",
 *   admin_label = @Translation("Course completion by stat"),
 *   category = @Translation("Custom")
 * )
 */
class CourseCompletionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'plugin' => '',
      'courses' => [],
      'items_to_show' => 5,
      'sort_by' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $plugin_options = [];
    $pluginManager = \Drupal::service('plugin.manager.entity_statistics_extensible.stats_generator');
    $plugins = $pluginManager->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $pluginInstance = $pluginManager->createInstance($plugin_id);
      if (in_array('user', $pluginInstance->getValidEntityTypes())) {
        $plugin_options[$plugin_id] = $plugin['label'];
      }
    }
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Statistic plugin to show'),
      '#options' => $plugin_options,
      '#default_value' => $this->configuration['plugin'],
    ];

    $form['courses'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Course(s)'),
      '#default_value' => $this->configuration['courses'] ? Course::loadMultiple(array_column($this->configuration['courses'], 'target_id')) : [],
      '#target_type' => 'course',
      '#multiple' => TRUE,
      '#tags' => TRUE,
      '#maxlength' => 1024,
    ];

    $form['items_to_show'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of items to show'),
      '#description' => $this->t('Enter 0 to show all'),
      '#default_value' => $this->configuration['items_to_show'],
    ];

    $form['sort_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort by'),
      '#options' => [
        'alpha' => 'Alphabetical',
        'total_desc' => 'Total (high to low)',
        'total_asc' => 'Total (low to high)',
        'turnout_desc' => 'Turnout (high to low)',
        'turnout_asc' => 'Turnout (low to high)',
      ],
      '#default_value' => $this->configuration['sort_by'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['plugin'] = $form_state->getValue('plugin');
    $this->configuration['courses'] = $form_state->getValue('courses');
    $this->configuration['items_to_show'] = $form_state->getValue('items_to_show');
    $this->configuration['sort_by'] = $form_state->getValue('sort_by');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get users who have completed course(s)
    $query = \Drupal::database()->select('course_enrollment', 'ce');
    $query->condition('cid', array_column($this->configuration['courses'], 'target_id'), 'IN');
    $query->condition('complete', 1);
    $query->fields('ce', ['uid']);
    $uids = $query->distinct()->execute()->fetchCol();

    $entity_ids = [];
    foreach ($uids as $uid) {
      $entity_ids[] = ['user', $uid];
    }

    // Hand over to relevant entity_statistics plugin

    $pluginManager = \Drupal::service('plugin.manager.entity_statistics_extensible.stats_generator');
    $plugin = $pluginManager->createInstance($this->configuration['plugin']);
    $data = $plugin->getDataForEntities($entity_ids);
    $population = $plugin->getPopulationData();

    // Get table output and limit to items_to_show
    $results = [];
    $total = $data['total'];

    // Calculate "turnout" by getting total in each faculty etc...
    $results = $data['statsData'];

    if ($this->configuration['sort_by'] == 'total_desc') {
      arsort($results);
    } else if ($this->configuration['sort_by'] == 'total_asc') {
      asort($results);
    } else {
      ksort($results);
    }

    if ($population) {
      foreach ($results as $name => $value) {
        $results[$name] = round(100 * ($value / $population[$name]), 2) . '% (' . $value . ' out of ' . $population[$name] . ')';
      }

      if ($this->configuration['sort_by'] == 'turnout_desc') {
        arsort($results);
      } else if ($this->configuration['sort_by'] == 'turnout_asc') {
        asort($results);
      }
    }

    if ($this->configuration['items_to_show'] > 0) {
      $results = array_slice($results, 0, $this->configuration['items_to_show'], TRUE);
    }

    $rows = [];
    foreach ($results as $name => $result) {
      $rows[] = [$name, $result];
    }

    // Render table to show in block
    $build['content'] = [
      '#type' => 'table',
      '#header' => [
        t('Name'),
        t('Course completions'),
      ],
      '#rows' => $rows,
    ];

    return $build;
  }

  public function getCacheMaxAge() {
    return 60 * 60 * 24;
  }
}
