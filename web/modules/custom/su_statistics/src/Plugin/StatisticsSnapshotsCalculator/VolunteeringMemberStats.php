<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use DateTime;
use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\GroupMembershipRecordQuery;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;
use Exception;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "volunteering",
 *   label = "Volunteering engagement stats"
 * )
 */
class VolunteeringMemberStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  public array $orgTypes = [
    'Community' => 'Community organisations',
    'SLP' => 'Student-Led Projects',
    'External' => 'External organisations',
  ];

  public array $roles = [
    'volunteering_opp-interest',
    'volunteering_opp-placement',
  ];

  /**
   * @return array
   */
  public function getFields(): array {
    $fields = [];

    foreach ($this->orgTypes as $type => $orgTypeLabel) {
      $orgTypeLabel = ($orgTypeLabel ? $orgTypeLabel : 'All');

      foreach ($this->roles as $role_id) {
        $role = GroupRole::load($role_id);

        $fields[$role_id . $type . 'current'] = BaseFieldDefinition::create('integer')
          ->setLabel(t($orgTypeLabel . ' - ' . $role->label() . ' - current'))
          ->setSetting('group', $orgTypeLabel ?: 'General')
          ->setSetting('period', 'snapshot');

        $fields[$role_id . $type . 'current_people'] = BaseFieldDefinition::create('integer')
          ->setLabel(t($orgTypeLabel . ' - unique volunteers with ' . $role->label() . ' - current'))
          ->setSetting('group', $orgTypeLabel ?: 'General')
          ->setSetting('period', 'snapshot');

        $fields[$role_id . $type . 'academic_year'] = BaseFieldDefinition::create('integer')
          ->setLabel(t($orgTypeLabel . ' - ' . $role->label() . ' - this academic year'))
          ->setSetting('group', $orgTypeLabel ?: 'General')
          ->setSetting('period', 'academic_year');

        $fields[$role_id . $type . 'academic_year_people'] = BaseFieldDefinition::create('integer')
          ->setLabel(t($orgTypeLabel . ' - unique volunteers with ' . $role->label() . ' - this academic year'))
          ->setSetting('group', $orgTypeLabel ?: 'General')
          ->setSetting('period', 'academic_year');
      }
    }

    return $fields;
  }

  /**
   * @throws \Exception
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $date = $end;
    $academicYear = $this->getAcademicYearForSnapshot($start, $end);
    if (!$academicYear) {
      throw new Exception("Volunteering stats: can't find year.");
    }

    $values = [];

    foreach ($this->orgTypes as $type => $label) {
      if ($type == '') {
        continue;
      }

      $gids = Drupal::entityQuery('group')
        ->condition('type', 'volunteering_org')
        ->condition('field_vol_organisation_type', $type)
        ->execute();
      $orgs = Group::loadMultiple($gids);

      $current_uids = [];
      $year_uids = [];
      $inYear = 0;

      foreach ($this->roles as $role_id) {
        $memberRole = GroupRole::load($role_id);

        // Get current ones
        $current = 0;
        foreach ($orgs as $org) {
          $opps = su_volunteering_get_opps($org);
          if (!$opps) {
            continue;
          }
          
          foreach ($opps as $opp) {
            // Current:
            $queryCurrent = (new GroupMembershipRecordQuery())
              ->setGroup($opp)
              ->setRole($memberRole)
              ->setDates(new DateTime('@' . $date));
            $current += $queryCurrent->getCount();
            $current_uids += $queryCurrent->executeForUids();

            // Academic year:
            $queryYear = (new GroupMembershipRecordQuery())
              ->setGroup($opp)
              ->setRole($memberRole)
              ->setDates($academicYear->start, $academicYear->end);
            $inYear += $queryYear->getCount();
            $year_uids += $queryYear->executeForUids();
          }
        }

        $values[$role_id . $type . 'current'] = $current;
        $values[$role_id . $type . 'current_people'] = count(array_unique($current_uids));

        $values[$role_id . $type . 'academic_year'] = $inYear;
        $values[$role_id . $type . 'academic_year_people'] = count(array_unique($year_uids));
      }
    }

    return $values;
  }

}
