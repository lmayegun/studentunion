<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "engagement",
 *   label = "Student member engagement"
 * )
 */
class EngagementStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  public function getFields(): array {
    $fields = [];

    // Entity type counts:
    $fields['students'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Student members'))
      ->setSetting('period', 'snapshot');

    foreach ($this->getStudentTypes() as $type => $label) {
      $fields['students_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type total', ['@type' => $label]))
        ->setSetting('group', $label)
        ->setSetting('period', 'snapshot');

      $fields['any_engagement_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type engaged with Union in any way (apart from logged in)', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'month');

      $fields['logged_on_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type logged on to website', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'month');

      $fields['voted_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type voted', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'month');

      $fields['tickets_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type who have bought an event ticket', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'month');

      $fields['club_member_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type - club/society members this academic year', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'snapshot');

      // NOT WHAT WE NEED - NEEDS TO BE USERS WHO DIDN'T HAVE MEMBERSHIP BEFORE:
      //      $fields['club_member_joined_' . $type] = BaseFieldDefinition::create('integer')
      //        ->setLabel(t('@type - new club/society members in month', ['@type' => $label]))
      //        ->setSetting('proportion_of', 'students_' . $type)
      //        ->setSetting('proportion_of_label', '% of ' . $label)
      //        ->setSetting('group', $label)
      //        ->setSetting('period', 'month');

      $fields['vol_interest_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type - users with volunteering expressions of interest this academic year', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'academic_year');

      // NOT WHAT WE NEED - NEEDS TO BE USERS WHO DIDN'T HAVE INTEREST BEFORE:
      //      $fields['vol_interest_joined_' . $type] = BaseFieldDefinition::create('integer')
      //        ->setLabel(t('@type - new volunteering expressions of interest in month', ['@type' => $label]))
      //        ->setSetting('proportion_of', 'students_' . $type)
      //        ->setSetting('group', $label)
      //        ->setSetting('period', 'month');

      $fields['vol_placement_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type - users with volunteer placements this academic year', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'academic_year');

      // NOT WHAT WE NEED - NEEDS TO BE USERS WHO DIDN'T HAVE PLACEMENT BEFORE:
      //      $fields['vol_placement_joined_' . $type] = BaseFieldDefinition::create('integer')
      //        ->setLabel(t('@type - new volunteer placements in month', ['@type' => $label]))
      //        ->setSetting('proportion_of', 'students_' . $type)
      //        ->setSetting('proportion_of_label', '% of ' . $label)
      //        ->setSetting('group', $label)
      //        ->setSetting('period', 'month');

      $fields['academic_rep_' . $type] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type - academic representatives', ['@type' => $label]))
        ->setSetting('proportion_of', 'students_' . $type)
        ->setSetting('proportion_of_label', '% of ' . $label)
        ->setSetting('group', $label)
        ->setSetting('period', 'snapshot');
    }

    return $fields;
  }

  public function getStudentTypes() {
    $types = [
      'all' => 'All students',
      'pg' => 'Postgraduate students',
      'ug' => 'Undergraduate students',
    ];
    return $types;
  }

  /**
   * @throws \Exception
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $startOfMonth = $this->calculateStartFromPeriod('students', $start, $end, 'month');
    $endOfMonth = $this->calculateEndFromPeriod('students', $start, $end, 'month');

    $values = [];

    foreach ($this->getStudentTypes() as $type => $label) {
      $values['students_' . $type] = count($this->getStudents($type, $startOfMonth, $endOfMonth));

      $loggedOnUids = $this->getLoggedIn($type, $startOfMonth, $endOfMonth);
      $values['logged_on_' . $type] = count($loggedOnUids);

      $votedUids = $this->getVoted($type, $startOfMonth, $endOfMonth);
      $values['voted_' . $type] = count($votedUids);

      $ticketUids = $this->getTicketPurchasers($type, $startOfMonth, $endOfMonth);
      $values['tickets_' . $type] = count($ticketUids);

      $clubSocMembersUids = $this->getPeriodGroupMembershipRecord($type, $start, $end, ['club_society-member'], 'academic_year');
      $values['club_member_' . $type] = count($clubSocMembersUids);

      // SEE COMMENT ON FIELDS:
      //      $clubSocJoinedUids = $this->getNewGroupMembershipRecord($type, $startOfMonth, $endOfMonth, ['club_society-member']);
      //      $values['club_member_joined_' . $type] = count($clubSocJoinedUids);

      $volInterestUids = $this->getPeriodGroupMembershipRecord($type, $start, $end, ['volunteering_opp-interest'], 'academic_year');
      $values['vol_interest_' . $type] = count($volInterestUids);

      // SEE COMMENT ON FIELDS:
      //      $volInterestJoinedUids = $this->getNewGroupMembershipRecord($type, $startOfMonth, $endOfMonth, ['volunteering_opp-interest']);
      //      $values['vol_interest_joined_' . $type] = count($volInterestJoinedUids);

      $volPlacementsUids = $this->getPeriodGroupMembershipRecord($type, $start, $end, ['volunteering_opp-placement'], 'academic_year');
      $values['vol_placement_' . $type] = count($volPlacementsUids);

      // SEE COMMENT ON FIELDS:
      //      $volPlacementsJoinedUids = $this->getNewGroupMembershipRecord($type, $startOfMonth, $endOfMonth, ['volunteering_opp-placement']);
      //      $values['vol_placement_joined_' . $type] = count($volPlacementsJoinedUids);

      $academicRepUids = $this->getCurrentGroupMembershipRecord($type, $startOfMonth, $endOfMonth, [
        'academic_group-faculty_represent',
        'academic_group-lead_department_r',
        'academic_group-course_representa',
        'academic_group-research_student_',
      ]);
      $values['academic_rep_' . $type] = count($academicRepUids);

      // Get any Uids from any of them:
      $uids = array_unique(array_merge(
        $votedUids,
        $ticketUids,
        $clubSocMembersUids,
        //        $clubSocJoinedUids,
        $volInterestUids,
        //        $volInterestJoinedUids,
        $volPlacementsUids,
        //        $volPlacementsJoinedUids,
        $academicRepUids,
      ));
      $values['any_engagement_' . $type] = count($uids);

    }

    return $values;
  }

  private function getStudents(string $type, $startOfMonth, $endOfMonth) {
    $query = Drupal::database()->select('users', 'u');
    $query->fields('u', ['uid']);
    $this->addProfileToQuery($query, 'u.uid', $type);
    return $query->distinct()->execute()->fetchCol('uid');
  }

  public static function addProfileToQuery(&$query, $sourceTableUid, $type) {
    $query->join('profile', 'prf', 'prf.uid = ' . $sourceTableUid);

    $query->condition('prf.type', 'student')
      ->condition('prf.status', 1);

    if ($type == 'pg' || $type == 'ug') {
      if ($type == 'pg') {
        $termIds = [
          su_drupal_helper_get_taxonomy_term_by_name('Postgraduate (Taught)', 'students_study_modes'),
          su_drupal_helper_get_taxonomy_term_by_name('Postgraduate (Research)', 'students_study_modes'),
        ];
      }
      else {
        $termIds = [su_drupal_helper_get_taxonomy_term_by_name('Undergraduate', 'students_study_modes')];
      }
      $query->join('profile__field_student_study_mode', 'prfm', 'prfm.entity_id = prf.profile_id');
      $query->condition('prfm.field_student_study_mode_target_id', $termIds, 'IN');
    }
  }

  private function getLoggedIn(string $type, ?int $startOfMonth, ?int $endOfMonth) {
    $query = Drupal::database()->select('users_field_data', 'u');
    $query->fields('u', ['uid']);
    $query->condition('u.access', $startOfMonth, '>=');
    $query->condition('u.access', $endOfMonth, '<=');
    $this->addProfileToQuery($query, 'u.uid', $type);
    return $query->distinct()->execute()->fetchCol('uid');
  }

  private function getVoted(string $type, $startOfMonth, $endOfMonth) {
    $query = Drupal::database()->select('election_ballot', 'eb');
    $query->condition('confirmed', 1);
    $query->fields('eb', ['user_id']);
    $this->addProfileToQuery($query, 'eb.user_id', $type);
    return $query->distinct()->execute()->fetchCol('user_id');
  }

  private function getTicketPurchasers($type, $startOfMonth, $endOfMonth) {
    $query = Drupal::database()
      ->select('commerce_product_variation', 'cpv');
    $query->condition('cpv.type', 'event');
    $query->join('commerce_order_item', 'oi', 'oi.purchased_entity = cpv.variation_id');
    $query->join('commerce_order', 'co', 'oi.order_id = co.order_id');
    $query->fields('co', ['uid']);
    $this->addProfileToQuery($query, 'co.uid', $type);
    $query->condition('co.state', 'draft', '<>')
      ->condition('co.state', 'cancelled', '<>')
      ->condition('co.placed', $startOfMonth, '>=')
      ->condition('co.placed', $endOfMonth, '<=');
    return $query->distinct()->execute()->fetchCol('uid');
  }

  /**
   * @param string $type
   * @param int $start
   * @param int $end
   * @param array $groupRoleIds
   * @param string $period
   *
   * @return mixed
   * @throws \Exception
   */
  private function getPeriodGroupMembershipRecord(string $type, int $start, int $end, array $groupRoleIds, string $period) {
    $query = $this->getGroupMembershipRecordQuery($type, $groupRoleIds);

    $start = $this->calculateStartFromPeriod($type, $start, $end, $period);
    $end = $this->calculateEndFromPeriod($type, $start, $end, $period);

    return $this->getGroupMembershipRecordsBetweenDates($type, $start, $end, $groupRoleIds);
  }

  /**
   * @param $type
   * @param $groupRoleIds
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  private function getGroupMembershipRecordQuery(string $type, array $groupRoleIds): Drupal\Core\Database\Query\SelectInterface {
    $query = Drupal::database()->select('group_membership_record', 'gmr');
    $query->fields('gmr', ['user_id']);
    $query->condition('gmr.status', 1);
    $query->condition('gmr.group_role_id', $groupRoleIds, 'IN');
    $this->addProfileToQuery($query, 'gmr.user_id', $type);
    return $query;
  }

  private function getGroupMembershipRecordsBetweenDates(string $type, $start, $end, $groupRoleIds) {
    $query = $this->getGroupMembershipRecordQuery($type, $groupRoleIds);

    $start = new DrupalDateTime($start);
    $start = $start->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $end = new DrupalDateTime($end);
    $end = $end->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $inBetween = $query->orConditionGroup()
      ->condition('gmr.date_range__value', $start, '<=')
      ->condition('gmr.date_range__value', $end, '<=')
      ->condition('gmr.date_range__end_value', $end, '>=');
    $noEnd = $query->andConditionGroup()
      ->condition('gmr.date_range__value', $end, '<=')
      ->isNull('gmr.date_range__end_value');
    $current = $query->orConditionGroup()
      ->condition($inBetween)
      ->condition($noEnd);
    $query->condition($current);

    return $query->distinct()->execute()->fetchCol('user_id');
  }

  /**
   * @param string $type
   * @param int|null $startOfMonth
   * @param int|null $endOfMonth
   * @param array $groupRoleIds
   *
   * @return array
   */
  private function getCurrentGroupMembershipRecord(string $type, ?int $startOfMonth, ?int $endOfMonth, array $groupRoleIds): array {
    $query = $this->getGroupMembershipRecordQuery($type, $groupRoleIds);

    $now = new DrupalDateTime('now');
    $now = $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $inBetween = $query->orConditionGroup()
      ->condition('gmr.date_range__value', $now, '<=')
      ->condition('gmr.date_range__end_value', $now, '>=');
    $noEnd = $query->andConditionGroup()
      ->condition('gmr.date_range__value', $now, '<=')
      ->isNull('gmr.date_range__end_value');
    $current = $query->orConditionGroup()
      ->condition($inBetween)
      ->condition($noEnd);
    $query->condition($current);

    return $query->distinct()->execute()->fetchCol('user_id');
  }

  /**
   * @param string $type
   * @param int|null $startOfMonth
   * @param int|null $endOfMonth
   * @param array $groupRoleIds
   *
   * @return array
   */
  private function getNewGroupMembershipRecord(string $type, ?int $startOfMonth, ?int $endOfMonth, array $groupRoleIds): array {
    $query = $this->getGroupMembershipRecordQuery($type, $groupRoleIds);

    $start = new DrupalDateTime($startOfMonth);
    $start = $start->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $end = new DrupalDateTime($endOfMonth);
    $end = $end->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $query->condition('gmr.date_range__value', $start, '>=')
      ->condition('gmr.date_range__value', $end, '<=');

    return $query->distinct()->execute()->fetchCol('user_id');
  }

}
