<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "club_soc",
 *   label = "Club/Society members"
 * )
 */
class ClubSocMemberStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  public const SPLITS_INTO_QUEUES = TRUE;

  public const SPLIT_CHUNK_SIZE = 2000;

  public function getFields(): array {
    $fields = [];

    $fields['members'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total Club/Society members'))
      ->setSetting('period', 'snapshot')
      ->setSetting('group', 'Club/Society members summary');

    $terms = $this->getClubSocCategories();
    foreach ($terms as $term) {
      $fields['members_' . $term->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Club/Society members in category: ' . $term->label()))
        ->setSetting('period', 'snapshot')
        ->setSetting('group', 'Club/Society members in categories')
        ->setSetting('proportion_of', 'members')
        ->setSetting('proportion_of_label', '% of members')
        ->setSetting('short_label', $term->label());
    }

    $fields['members_societies'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Society members'))
      ->setSetting('period', 'snapshot')
      ->setSetting('proportion_of', 'members')
      ->setSetting('proportion_of_label', '% of members')
      ->setSetting('group', 'Club/Society members summary');

    $fields['members_clubs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Club members'))
      ->setSetting('period', 'snapshot')
      ->setSetting('proportion_of', 'members')
      ->setSetting('proportion_of_label', '% of members')
      ->setSetting('group', 'Club/Society members summary');

    return $fields;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $values = [];

    /** @var \Drupal\su_clubs_societies\Service\ClubSocietyMembershipsService $clubSocMembershipsService */
    $clubSocMembershipsService = Drupal::service('su_clubs_societies.memberships');

    //    $uids = $clubSocMembershipsService->getMemberUidsForGroupRecordsInCategories();
    //    foreach ($uids as $uid) {
    //      echo $uid . '<br>';
    //    }
    //    die;

    $values['members'] = count($clubSocMembershipsService->getMemberUidsForGroupRecordsInCategories());

    $members_societies = [];
    $members_clubs = [];

    $terms = $this->getClubSocCategories();
    foreach ($terms as $term) {

      $members_in_category = $clubSocMembershipsService->getMemberUidsForGroupRecordsInCategories([$term->id()]);
      $values['members_' . $term->id()] = count(array_unique($members_in_category));

      if ($term->label() == 'Sport' || $term->label() == 'Sports') {
        $members_clubs = $members_in_category;
      }
      else {
        $members_societies = array_merge($members_societies, $members_in_category);
      }
    }

    $values['members_societies'] = count(array_unique($members_societies));
    $values['members_clubs'] = count(array_unique($members_clubs));
    return $values;
  }

}
