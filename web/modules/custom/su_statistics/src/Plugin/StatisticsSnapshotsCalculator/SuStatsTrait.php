<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use DateTime;
use Drupal;
use Drupal\taxonomy\Entity\Term;

trait SuStatsTrait {

  /**
   * {@inheritdoc}
   */
  public function calculateEndFromPeriod(string $unique_id, int $start = NULL, int $end = NULL, string $period = 'snapshot'): ?int {
    if ($period == 'academic_year') {
      $academicYear = static::getAcademicYearForSnapshot($start, $end);
      return $academicYear->end->getTimestamp();
    }
    else {
      return parent::calculateEndFromPeriod($unique_id, $start, $end, $period);
    }

  }

  /**
   * Convert two timestamps to a year object.
   */
  public static function getAcademicYearForSnapshot($start, $end) {
    /** @var \Drupal\date_year_filter\Service\YearService $yearService */
    $yearService = Drupal::service('date_year_filter.year');
    $date = DateTime::createFromFormat('U', $end);
    $statsStart = Drupal::config('su_statistics.settings')
      ->get('start_of_academic_year') ?: '2021-09-01';
    $year = $yearService->getYearForDate($date, $statsStart);
    return $year;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateStartFromPeriod(string $unique_id, int $start = NULL, int $end = NULL, string $period = 'snapshot'): ?int {
    if ($period == 'academic_year') {
      $academicYear = static::getAcademicYearForSnapshot($start, $end);
      return $academicYear->start->getTimestamp();
    }
    else {
      return parent::calculateStartFromPeriod($unique_id, $start, $end, $period);
    }

  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClubSocCategories(): array {
    $vid = 'club_and_society_categories';
    $term_objects = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($vid);

    $ids = array_map(function ($e) {
      return is_object($e) ? $e->tid : NULL;
    }, $term_objects);

    return Term::loadMultiple($ids);
  }

}
