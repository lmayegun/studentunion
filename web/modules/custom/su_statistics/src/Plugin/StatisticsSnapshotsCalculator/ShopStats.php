<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "shop",
 *   label = "Online shop"
 * )
 */
class ShopStats extends StatisticsSnapshotsCalculatorBase {

  public const SPLITS_INTO_QUEUES = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];
    $fields['orders'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Completed orders'))
      ->setSetting('period', 'month');

    $fields['online_income'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Total online income'))
      ->setSetting('period', 'month');

    $fields['items_sold'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Items sold'))
      ->setSetting('period', 'month');

    $productVariationTypes = ProductVariationType::loadMultiple();
    foreach ($productVariationTypes as $productVariationType) {
      $group = 'Product variation type: ' . $productVariationType->label();

      $fields['items_available_' . $productVariationType->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Products available for product variation type ' . $productVariationType->label()))
        ->setSetting('period', 'snapshot')
        ->setSetting('group', $group);

      $fields['online_income_' . $productVariationType->id()] = BaseFieldDefinition::create('float')
        ->setLabel(t('Income for product variation type ' . $productVariationType->label()))
        ->setSetting('period', 'month')
        ->setSetting('group', $group);

      $fields['items_sold_' . $productVariationType->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Items sold for product variation type ' . $productVariationType->label()))
        ->setSetting('period', 'month')
        ->setSetting('group', $group);

      // @todo
      $fields['customers_m_' . $productVariationType->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Unique monthly customers for product variation type ' . $productVariationType->label()))
        ->setSetting('period', 'month')
        ->setSetting('group', $group);
    }

    $productTypes = ProductType::loadMultiple();
    foreach ($productTypes as $productType) {
      $group = 'Product type: ' . $productType->label();

      $fields['items_available_p_' . $productType->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Products available for product type ' . $productType->label()))
        ->setSetting('period', 'snapshot')
        ->setSetting('group', $group);

      $fields['online_income_p_' . $productType->id()] = BaseFieldDefinition::create('float')
        ->setLabel(t('Income for product type ' . $productType->label()))
        ->setSetting('period', 'month')
        ->setSetting('group', $group);

      $fields['items_sold_p_' . $productType->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Items sold for product type ' . $productType->label()))
        ->setSetting('period', 'month')
        ->setSetting('group', $group);

      // @todo
      $fields['customers_m_p_' . $productType->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Unique monthly customers for product type ' . $productType->label()))
        ->setSetting('period', 'month')
        ->setSetting('group', $group);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    if (!$segment_data) {
      $values = [];
      $startOfMonth = $this->calculateStartFromPeriod('shop', $start, $end, 'month');
      $endOfMonth = $this->calculateEndFromPeriod('shop', $start, $end, 'month');

      // Set available totals once.
      foreach (ProductVariationType::loadMultiple() as $productVariationType) {
        $query = Drupal::entityQuery('commerce_product_variation')
          ->condition('type', $productVariationType->id())
          ->condition('status', 1);

        if (in_array($productVariationType->id(), ['event', 'meeting'])) {
          $query = $query->condition('field_date_range.value', strtotime('-1 hour'), '>=');
        }
        $values['items_available_' . $productVariationType->id()] = $query->count()
          ->execute();

        $values['customers_m_' . $productVariationType->id()] = 0;
      }

      foreach (ProductType::loadMultiple() as $productType) {
        $query = Drupal::entityQuery('commerce_product')
          ->condition('type', $productType->id())
          ->condition('status', 1);

        $count = 0;
        // Only include future events:
        if ($productType->id() == 'event') {
          $products = $query->execute();
          foreach ($products as $product) {
            $product = Product::load($product);
            if (count(su_events_get_published_future_variations($product)) > 0) {
              $count++;
            }
          }
        }
        else {
          $count = $query->count()->execute();
        }

        $values['items_available_p_' . $productType->id()] = $count;

        // Get unique customers:
        $query = Drupal::database()
          ->select('commerce_product_variation', 'cpv');
        $query->addExpression('COUNT([uid])', 'uid');
        $query->condition('cpv.type', $productType->id());
        $query->addExpression('COUNT([uid])', 'uid');
        $query->join('commerce_order_item', 'oi', 'oi.purchased_entity = cpv.variation_id');
        $query->join('commerce_order', 'co', 'oi.order_id = co.order_id');
        $query->condition('co.state', 'draft', '<>')
          ->condition('co.state', 'cancelled', '<>')
          ->condition('co.placed', $startOfMonth, '>=')
          ->condition('co.placed', $endOfMonth, '<=');
        $countUids = (int) $query->execute()->fetchField();
        $values['customers_m_p_' . $productType->id()] = $countUids;
      }

      // Save to snapshot.
      foreach ($values as $key => $value) {
        $repository = Drupal::service('statistics_snapshots.repository');
        $repository->setStatValue($this, $key, $start, $end, $value);
      }

      // Then load orders to get details and split into multiple queue items.
      $query = Drupal::entityQuery('commerce_order')
        ->condition('placed', $startOfMonth, '>=')
        ->condition('placed', $endOfMonth, '<=')
        ->condition('state', 'draft', '<>')
        ->condition('state', 'cancelled', '<>')
        ->accessCheck(FALSE);
      $result = $query->execute();
      if (count($result) > 0) {
        $this->splitIntoQueues($result, $start, $end);
      }
      return [];
    }

    $orders = Order::loadMultiple($segment_data);
    $values = [
      'orders' => 0,
      'online_income' => 0.00,
      'items_sold' => 0,
    ];
    foreach ($orders as $order) {
      $values['orders']++;
      $items = $order->getItems();
      foreach ($items as $item) {
        if (!$item->getPurchasedEntity()) {
          continue;
        }

        $productType = $item->bundle();
        $productVariationType = $item->getPurchasedEntity()->bundle();

        // Items sold
        $values['items_sold'] += (float) $item->getQuantity();

        // Variations
        if (!isset($values['items_sold_' . $productVariationType])) {
          $values['items_sold_' . $productVariationType] = 0;
        }
        $values['items_sold_' . $productVariationType] += (float) $item->getQuantity();

        // Products
        if (!isset($values['items_sold_p_' . $productType])) {
          $values['items_sold_p_' . $productType] = 0;
        }
        $values['items_sold_p_' . $productType] += (float) $item->getQuantity();

        // Income
        $total = $item->getAdjustedTotalPrice();
        $values['online_income'] += $total ? $total->getNumber() : 0;

        if (!isset($values['online_income_' . $productVariationType])) {
          $values['online_income_' . $productVariationType] = 0;
        }
        $values['online_income_' . $productVariationType] += (float) $item->getAdjustedTotalPrice()
          ->getNumber();

        if (!isset($values['online_income_p_' . $productType])) {
          $values['online_income_p_' . $productType] = 0;
        }
        $values['online_income_p_' . $productType] += (float) $item->getAdjustedTotalPrice()
          ->getNumber();
      }
    }

    return $values + ['statistics_snapshots_increment' => TRUE];
  }

}
