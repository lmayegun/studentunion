<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use DateTime;
use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "academic_reps",
 *   label = "Academic representatives"
 * )
 */
class AcademicRepStats extends StatisticsSnapshotsCalculatorBase {

  public function getFields() {
    $fields = [];

    // This is a moment in time as they get reset each academic year anyway:
    $fields['academic_reps'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total academic representatives'))
      ->setSetting('period', 'snapshot');

    // @todo per faculty and department maybe...

    return $fields;
  }

  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');
    $date = $end;

    $repType = GroupMembershipRecordType::load('academic_representative');
    $records = $gmrRepositoryService->count(NULL, NULL, NULL, $repType, new DateTime('@' . $date));

    $values = [
      'academic_reps' => $records,
    ];

    return $values;
  }

}
