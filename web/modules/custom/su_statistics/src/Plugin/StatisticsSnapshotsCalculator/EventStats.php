<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "events",
 *   label = "Events"
 * )
 */
class EventStats extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];

    // This plugin loads based on either a tag/taxonomy term reference,
    // Or a field on the commerce product or variatione entity.

    foreach ($this->getEventSubtypes() as $id => $data) {
      $label = $data['label'];

      $fields['events_' . $id] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Events - ' . $label . ' - events starting in month'))
        ->setSetting('period', 'month')
        ->setSetting('group', $label);

      $fields['events_tickets_' . $id] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Events - ' . $label . ' - tickets sold'))
        ->setSetting('period', 'month')
        ->setSetting('group', $label);

      $fields['events_customers_' . $id] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Events - ' . $label . ' - unique customers'))
        ->setSetting('period', 'month')
        ->setSetting('group', $label);
    }

    return $fields;
  }

  /**
   * @return array|array[]
   */
  public function getEventSubtypes(): array {
    try {
      $categories = Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree('events_categories');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      $categories = [];
    }
    foreach ($categories as $category) {
      $categoryTypes[] = [
        'label' => 'Category "' . $category->name . '"',
        'type' => 'tag',
        'vid' => 'events_categories',
        'field' => 'field_event_category',
        'tag_names' => [$category->name],
        'entity_type' => 'commerce_product',
        'entity_type_table_alias' => 'cp',
      ];
    }

    $others = [
      'project_active' => [
        'label' => 'Project Active',
        'type' => 'tag',
        'vid' => 'tags',
        'tag_names' => ['Project Active'],
        'field' => 'field_event_tags',
        'entity_type' => 'commerce_product',
        'entity_type_table_alias' => 'cp',
      ],
      'pg' => [
        'label' => 'Postgraduate',
        'type' => 'field',
        'entity_type' => 'commerce_product',
        'entity_type_table_alias' => 'cp',
        'field' => 'field_access_pg',
        'value' => 1,
      ],
    ];

    return $categoryTypes + $others;
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $values = [];

    if (!$start) {
      $start = strtotime('now');
    }
    if (!$end) {
      $end = $start;
    }

    foreach ($this->getEventSubtypes() as $id => $data) {
      $label = $data['label'];
      $type = $data['type'];

      // Load order items:
      $query = Drupal::database()->select('commerce_order_item', 'i');
      $query->fields('i', ['order_item_id', 'quantity'])
        ->condition('i.quantity', 0, ">");

      // Order to make sure it's completed and get unique customer:
      $query->join('commerce_order', 'o', 'i.order_id = o.order_id');
      $query->fields('o', ['uid'])
        ->condition('o.state', 'completed');

      // Variation to load ID and link to other fields:
      $query->join('commerce_product_variation_field_data', 'cpv', 'i.purchased_entity = cpv.variation_id');
      $query->fields('cpv', ['variation_id']);

      // Product to filter by type:
      $query->join('commerce_product_field_data', 'cp', 'cp.product_id = cpv.product_id');
      $query->condition('cp.type', 'event');

      // Filter by dates:
      // Override start and end with current month:
      $startToUse = $this->calculateStartFromPeriod('events_' . $id, $start, $end, 'month');
      $endToUse = $this->calculateEndFromPeriod('events_' . $id, $start, $end, 'month');
      $query->join('commerce_product_variation__field_date_range', 'cpd', 'cpd.entity_id = cpv.variation_id');

      $query->condition('cpd.field_date_range_value', $startToUse, '>=');
      $query->condition('cpd.field_date_range_value', $endToUse, '<=');

      // Load specific fields based on type:
      if ($type == 'field') {
        $alias = 'f' . $data['field'];
        $query->join($data['entity_type'] . '__' . $data['field'], $alias, $alias . '.entity_id = ' . $data['entity_type_table_alias'] . '.product_id');
        $query->condition($alias . '.' . $data['field'] . '_value', $data['value']);
      }
      elseif ($type == 'tag') {
        $alias = 'tag' . $data['field'];
        $query->join($data['entity_type'] . '__' . $data['field'], $alias, $alias . '.entity_id = ' . $data['entity_type_table_alias'] . '.product_id');

        $termIds = [];
        foreach ($data['tag_names'] as $tag_name) {
          $termIds[] = su_drupal_helper_get_taxonomy_term_by_name($tag_name, $data['vid'] ?: 'tags');
        }
        $query->condition($alias . '.' . $data['field'] . '_target_id', $termIds, 'IN');
      }

      $query->distinct();

      $data = $query->execute()->fetchAllAssoc('order_item_id');

      $values['events_' . $id] = count(array_unique(array_column($data, 'variation_id')));
      $values['events_tickets_' . $id] = array_sum(array_column($data, 'quantity'));
      $values['events_customers_' . $id] = count(array_unique(array_column($data, 'uid')));
    }

    return $values;
  }

}
