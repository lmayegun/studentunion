<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\group\Entity\GroupType;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "groups",
 *   label = "Group numbers"
 * )
 */
class GroupNumberStats extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];

    $fields['total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total groups (published)'))
      ->setSetting('period', 'snapshot');

    $groupTypes = GroupType::loadMultiple();
    foreach ($groupTypes as $type) {
      $fields['type_' . $type->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('@type groups (published)', [
          '@type' => $type->label(),
        ]))
        ->setSetting('period', 'snapshot');
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    try {
      $groups = Drupal::entityTypeManager()
        ->getStorage('group')
        ->loadByProperties(['status' => 1]);
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      $groups = [];
    }
    $types = [];

    foreach ($groups as $group) {
      if (!isset($types[$group->bundle()])) {
        $types[$group->bundle()] = 0;
      }
      $types[$group->bundle()]++;
    }

    $values = [];

    // Set values
    $values['total'] = count($groups);
    foreach ($types as $type => $count) {
      $values['type_' . $type] = $count;
    }

    return $values;
  }

}
