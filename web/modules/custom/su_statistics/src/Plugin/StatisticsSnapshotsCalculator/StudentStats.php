<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;
use Drupal\taxonomy\Entity\Term;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "students",
 *   label = "Student information fields"
 * )
 */
class StudentStats extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $values = [];

    $uids = Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', ['member', 'associate_visiting'], 'IN')
      ->execute();

    $profileFields = $this->getProfileFields();
    foreach ($profileFields as $fieldName => $fieldConfig) {
      $query = Drupal::database()->select('profile__' . $fieldName, 'pf');

      $query->join('profile', 'p', 'pf.entity_id = p.profile_id');
      $query->condition('p.type', 'student');
      $query->condition('p.status', 1);

      // Limit to roles:
      $query->join('users', 'u', 'p.uid = u.uid');
      $query->join('user__roles', 'ur', 'u.uid = ur.entity_id');
      $query->condition('ur.roles_target_id', [
        'member',
        'associate_visiting',
      ], 'IN');

      // Count and group by the various values:
      $query->fields('pf', [$fieldName . '_target_id']);
      $query->addExpression('COUNT(pf.' . $fieldName . '_target_id)', 'count_of_values');
      $query->groupBy('pf.' . $fieldName . '_target_id');
      $results = $query->execute()->fetchAllAssoc($fieldName . '_target_id');

      $totalLeftOver = count($uids);
      foreach ($results as $value => $data) {
        $totalLeftOver -= $data->count_of_values;
        $values[$fieldName . ':' . $value] = $data->count_of_values;
      }

      $values[$fieldName . ':null'] = $totalLeftOver;
    }

    return $values;
  }

  /**
   *
   */
  public function getProfileFields() {
    $allFields = Drupal::service('entity_field.manager')
      ->getFieldDefinitions('profile', 'student');
    $finalFields = [];
    foreach ($allFields as $fieldName => $fieldConfig) {
      if ($fieldConfig->getType() != 'entity_reference') {
        continue;
      }
      if ($fieldConfig->getSettings()['target_type'] != 'taxonomy_term') {
        continue;
      }

      $finalFields[$fieldName] = $fieldConfig;
    }
    return $finalFields;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    // We need to load all possible values to work this out.
    // Cache possible values daily to avoid over-calculating...
    $cid = __METHOD__;
    if ($item = Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $data = $this->calculate([], NULL, NULL);

    $profileFields = $this->getProfileFields();

    $termLabels = [];

    foreach ($data as $field_id => $value) {
      $values = explode(':', $field_id);
      $field_id = $values[0];
      $tid = $values[1];

      if (isset($termLabels[$tid])) {
        $label = $termLabels[$tid];
      }
      else {
        if ($term = Term::load($tid)) {
          $termLabels[$tid] = $term->label();
        }
        else {
          if ($tid == 'null') {
            $termLabels[$tid] = 'None';
          }
          else {
            $termLabels[$tid] = 'Unknown';
          }
        }
        $label = $termLabels[$tid];
      }

      $fieldConfig = $profileFields[$field_id];

      $fields[$field_id . ':' . $tid] = BaseFieldDefinition::create('integer')
        ->setLabel(t($fieldConfig->getLabel() . ' - ' . $label))
        ->setDefaultValue(0)
        ->setSetting('period', 'snapshot')
        ->setSetting('proportion', $fieldConfig->getLabel())
        ->setSetting('chart', $fieldConfig->getLabel())
        ->setSetting('chart_type', 'pie')
        ->setSetting('valueLabel', $label);
    }

    Drupal::cache()->set($cid, $fields, strtotime('+1 day'));
    return $fields;
  }

}
