<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use DateTime;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\GroupMembershipRecordQuery;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "club_soc_memberships",
 *   label = "Club/Society memberships"
 * )
 */
class ClubSocMembershipStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  public function getFields() {
    $fields = [];

    $fields['memberships'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total memberships - all (both paid and free)'))
      ->setSetting('group', 'Key statistics')
      ->setSetting('period', 'snapshot');

    $fields['memberships_free'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total memberships - free only'))
      ->setSetting('period', 'snapshot')
      ->setSetting('group', 'Key statistics')
      ->setSetting('proportion_of', 'memberships')
      ->setSetting('proportion_of_label', '% of all memberships');

    $fields['memberships_societies'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Society memberships - all (both paid and free)'))
      ->setSetting('period', 'snapshot')
      ->setSetting('group', 'Key statistics')
      ->setSetting('proportion_of', 'memberships')
      ->setSetting('proportion_of_label', '% of all memberships');

    $fields['memberships_societies_free'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Society memberships - free only'))
      ->setSetting('period', 'snapshot')
      ->setSetting('group', 'Key statistics')
      ->setSetting('proportion_of', 'memberships')
      ->setSetting('proportion_of_label', '% of all memberships');

    $fields['memberships_clubs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Club memberships - all (both paid and free)'))
      ->setSetting('period', 'snapshot')
      ->setSetting('group', 'Key statistics')
      ->setSetting('proportion_of', 'memberships')
      ->setSetting('proportion_of_label', '% of all memberships');

    $fields['memberships_clubs_free'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Club memberships - free only'))
      ->setSetting('period', 'snapshot')
      ->setSetting('group', 'Key statistics')
      ->setSetting('proportion_of', 'memberships')
      ->setSetting('proportion_of_label', '% of all memberships');

    $terms = $this->getClubSocCategories();
    foreach ($terms as $term) {
      $fields['memberships_' . $term->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Memberships in category "' . $term->label() . '" - all (both paid and free)'))
        ->setSetting('period', 'snapshot')
        ->setSetting('group', 'Categories')
        ->setSetting('proportion_of', 'memberships')
        ->setSetting('proportion_of_label', '% of all memberships');

      $fields['memberships_' . $term->id() . '_free'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Memberships in category "' . $term->label() . '" - free only'))
        ->setSetting('period', 'snapshot')
        ->setSetting('group', 'Categories')
        ->setSetting('proportion_of', 'memberships')
        ->setSetting('proportion_of_label', '% of all memberships');
    }

    return $fields;
  }

  /**
   * @throws \Exception
   */
  public function getQuery($start = NULL, $end = NULL, $categoryTermId = NULL, $inCategory = TRUE, $paid = NULL) {
    $query = new GroupMembershipRecordQuery();
    $query->setRole(GroupRole::load('club_society-member'));
    $query->setRequireEnabled(TRUE);
    $query->setDates(new DateTime('@' . $start), new DateTime('@' . $end));
    // $query->setRequireGroupPublished(TRUE);

    if ($categoryTermId) {
      $this->setGroupCategory($query, $categoryTermId, $inCategory);
    }

    if (!is_null($paid)) {
      $this->setMembershipPrice($query, $paid);
    }
    return $query;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {

    // Total members:
    $values['memberships'] = $this->getQuery($start, $end)->getCount();

    $values['memberships_free'] = $values['memberships'] - $this->getQuery($start, $end, NULL, NULL, TRUE)
        ->getCount();

    // Societies:

    $values['memberships_societies'] = $this->getQuery($start, $end, 2758, FALSE)
      ->getCount();

    $values['memberships_societies_free'] = $values['memberships_societies'] - $this->getQuery($start, $end, 2758, FALSE, TRUE)
        ->getCount();

    // Clubs:

    $values['memberships_clubs'] = $this->getQuery($start, $end, 2758, TRUE)
      ->getCount();

    $values['memberships_clubs_free'] = $values['memberships_clubs'] - $this->getQuery($start, $end, 2758, TRUE, TRUE)
        ->getCount();

    // Per category:

    $terms = $this->getClubSocCategories();
    foreach ($terms as $term) {
      $values['memberships_' . $term->id()] = $this->getQuery($start, $end, $term->id(), TRUE)
        ->getCount();

      $values['memberships_' . $term->id() . '_free'] = $values['memberships_' . $term->id()] - $this->getQuery($start, $end, $term->id(), TRUE, TRUE)
          ->getCount();
    }

    return $values;
  }

  /**
   * Note $paid only seems to work in the positive due to the joins,
   * So use total minus that to get to free.
   *
   * @param $query
   * @param bool $paid
   *
   * @return void
   */
  public function setMembershipPrice(&$query, bool $paid) {
    $selectQuery = $query->getQuery();

    // Link to product variation:
    $selectQuery->leftJoin('commerce_product__field_club_society', 'cpcs', 'cpcs.field_club_society_target_id = gmr.group_id');
    $selectQuery->leftJoin('commerce_product_variation_field_data', 'cpv', 'cpv.product_id = cpcs.entity_id');

    // Link to order item:
    $selectQuery->leftJoin('group_membership_record__field_order', 'gmroid', 'gmroid.entity_id = gmr.id');
    $selectQuery->leftJoin('commerce_order_item', 'coi', 'coi.order_id = gmroid.field_order_target_id AND coi.purchased_entity = cpv.variation_id');

    // Make sure order item
    $selectQuery->condition('coi.total_price__number', 0, $paid ? '>' : '=');

    $selectQuery->groupBy('gmr.id');

    $query->setQuery($selectQuery);
  }

  public function setGroupCategory(&$query, int $groupCategoryTermId, $inCategory = TRUE) {
    $selectQuery = $query->getQuery();

    $selectQuery->leftJoin('group__field_club_category', 'clubcat', 'clubcat.entity_id = gmr.group_id');

    $selectQuery->condition('clubcat.field_club_category_target_id', $groupCategoryTermId, $inCategory ? '=' : '<>');

    //    $selectQuery->groupBy('gmr.id');

    $query->setQuery($selectQuery);
  }

}
