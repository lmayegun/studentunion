<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "self_define",
 *   label = "Self-definition fields for Union members"
 * )
 */
class SelfDefinitionStats extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $values = [];

    $uids = Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', ['member', 'associate_visiting'], 'IN')
      ->execute();

    $profileFields = $this->getProfileFields();
    foreach ($profileFields as $fieldName => $fieldConfig) {
      $query = Drupal::database()->select('profile__' . $fieldName, 'pf');

      $query->join('profile', 'p', 'pf.entity_id = p.profile_id');
      $query->condition('p.type', 'self_define');
      $query->condition('p.status', 1);

      // Limit to roles:
      $query->join('users', 'u', 'p.uid = u.uid');
      $query->join('user__roles', 'ur', 'u.uid = ur.entity_id');
      $query->condition('ur.roles_target_id', [
        'member',
        'associate_visiting',
      ], 'IN');

      // Count and group by the various values:
      $query->fields('pf', [$fieldName . '_value']);
      $query->addExpression('COUNT(pf.' . $fieldName . '_value)', 'count_of_values');
      $query->groupBy('pf.' . $fieldName . '_value');
      $results = $query->execute()->fetchAllAssoc($fieldName . '_value');

      $totalLeftOver = count($uids);
      foreach ($results as $value => $data) {
        $totalLeftOver -= $data->count_of_values;
        $values[$fieldName . ':' . $value] = $data->count_of_values;
      }

      $values[$fieldName . ':null'] = $totalLeftOver;
    }

    return $values;
  }

  /**
   *
   */
  public function getProfileFields() {
    $allFields = Drupal::service('entity_field.manager')
      ->getFieldDefinitions('profile', 'self_define');
    $selfDefinitionFields = [];
    foreach ($allFields as $fieldName => $fieldConfig) {
      $fieldStorage = $fieldConfig->getFieldStorageDefinition();
      if (!$fieldStorage->getSettings()) {
        continue;
      }
      $options = $fieldStorage->getSettings()['allowed_values'] ?? NULL;
      if ($options && is_array($options)) {
        $selfDefinitionFields[$fieldName] = $fieldConfig;
      }
    }
    return $selfDefinitionFields;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];

    $profileFields = $this->getProfileFields();
    foreach ($profileFields as $fieldName => $fieldConfig) {
      $fieldStorage = $fieldConfig->getFieldStorageDefinition();
      if (!$fieldStorage->getSettings()) {
        continue;
      }
      $options = $fieldStorage->getSettings()['allowed_values'] ?? NULL;
      if ($options && is_array($options)) {
        foreach ($options as $key => $label) {
          $fields[$fieldName . ':' . $key] = BaseFieldDefinition::create('integer')
            ->setLabel(t($fieldConfig->getLabel() . ' - ' . $label))
            ->setDefaultValue(0)
            ->setSetting('period', 'snapshot')
            ->setSetting('proportion', $fieldConfig->getLabel())
            ->setSetting('chart', $fieldConfig->getLabel())
            ->setSetting('chart_type', 'pie')
            ->setSetting('valueLabel', $key);
        }

        $fields[$fieldName . ':null'] = BaseFieldDefinition::create('integer')
          ->setLabel(t($fieldConfig->getLabel() . ' - ' . 'not provided'))
          ->setDefaultValue(0)
          ->setSetting('period', 'snapshot')
          ->setSetting('proportion', $fieldConfig->getLabel())
          ->setSetting('chart', $fieldConfig->getLabel())
          ->setSetting('chart_type', 'pie')
          ->setSetting('valueLabel', t('Not provided'));
      }
    }

    return $fields;
  }

}
