<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "club_soc_groups",
 *   label = "Club/Society groups"
 * )
 */
class ClubSocStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    $fields = [];

    // Club soc specific
    try {
      $terms = $this->getClubSocCategories();
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      $terms = [];
    }
    foreach ($terms as $term) {
      $fields['type_club_soc_category_' . $term->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Clubs/Societies in category: ' . $term->label()))
        ->setSetting('period', 'snapshot')
        ->setSetting('proportion', 'Clubs/Societies in category');
    }
    $fields['type_club_soc_category_'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Clubs/Societies with no category'))
      ->setSetting('period', 'snapshot')
      ->setSetting('proportion', 'Clubs/Societies in category');

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    try {
      $groups = Drupal::entityTypeManager()
        ->getStorage('group')
        ->loadByProperties([
          'status' => 1,
          'type' => 'club_society',
        ]);
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      $groups = [];
    }

    $clubSocCategories = [];
    foreach ($groups as $group) {
      $termId = $group->field_club_category->entity ? $group->field_club_category->entity->id() : NULL;
      if (!isset($clubSocCategories[$termId])) {
        $clubSocCategories[$termId] = ['groups' => []];
      }
      $clubSocCategories[$termId]['groups'][] = $group->id();
    }

    $values = [];

    foreach ($clubSocCategories as $termId => $count) {
      $values['type_club_soc_category_' . $termId] = count(array_unique($count['groups']));
    }


    try {
      $terms = $this->getClubSocCategories();
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      $terms = [];
    }
    foreach ($terms as $term) {
      if (!isset($values['type_club_soc_category_' . $term->id()])) {
        $values['type_club_soc_category_' . $term->id()] = 0;
      }
    }
    return $values;
  }

}
