<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "elections",
 *   label = "Elections"
 * )
 */
class ElectionStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  public function getFields(): array {
    $fields = [];

    // Entity type counts
    $fields['election'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total elections created in academic year'))
      ->setSetting('period', 'academic_year');
    $fields['election_post'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total election positions created in academic year'))
      ->setSetting('period', 'academic_year');
    $fields['election_candidate'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total nominations in an election in academic year'))
      ->setSetting('period', 'academic_year');
    $fields['election_ballot'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total votes (ballots) cast in academic year'))
      ->setSetting('period', 'academic_year');

    // Other queries
    $fields['election_voters'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total unique users voted in an election in academic year'))
      ->setSetting('period', 'academic_year');
    $fields['election_candidates_unique'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total unique candidates in an election in academic year'))
      ->setSetting('period', 'academic_year');

    return $fields;
  }

  /**
   * @throws \Exception
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $startOfAcademicYearTimestamp = $this->calculateStartFromPeriod('shop', $start, $end, 'academic_year');
    $endOfAcademicYearTimestamp = $this->calculateEndFromPeriod('shop', $start, $end, 'academic_year');

    // Manually managed to avoid distorting things:
    $testElections = [4];

    $values = [];

    $values['election'] = Drupal::entityQuery('election')
      ->condition('id', $testElections, 'NOT IN')
      ->condition('created', $startOfAcademicYearTimestamp, '>')
      ->condition('created', $endOfAcademicYearTimestamp, '<=')
      ->count()
      ->execute();

    $postIds = Drupal::entityQuery('election_post')
      ->condition('election', $testElections, 'NOT IN')
      ->condition('created', $startOfAcademicYearTimestamp, '>=')
      ->condition('created', $endOfAcademicYearTimestamp, '<=')
      ->execute();
    $values['election_post'] = count($postIds);

    $values['election_candidate'] = Drupal::entityQuery('election_candidate')
      ->condition('election_post', $postIds, 'IN')
      ->condition('created', $startOfAcademicYearTimestamp, '>=')
      ->condition('created', $endOfAcademicYearTimestamp, '<=')
      ->count()
      ->execute();

    $values['election_ballot'] = Drupal::entityQuery('election_ballot')
      ->condition('election', $testElections, 'NOT IN')
      ->condition('created', $startOfAcademicYearTimestamp, '>=')
      ->condition('created', $endOfAcademicYearTimestamp, '<=')
      ->count()
      ->execute();

    $query = Drupal::database()->select('election_ballot', 'eb');
    $query->condition('confirmed', 1);
    $query->fields('eb', ['user_id']);
    $election_voters = $query->distinct()
      ->countQuery()
      ->execute()
      ->fetchField();
    $values['election_voters'] = $election_voters;

    $query = Drupal::database()->select('election_candidate_field_data', 'ec');
    $query->fields('ec', ['user_id']);
    $election_candidates_unique = $query->distinct()
      ->countQuery()
      ->execute()
      ->fetchField();
    $values['election_candidates_unique'] = $election_candidates_unique;

    return $values;
  }

}
