<?php

namespace Drupal\su_statistics\Plugin\StatisticsSnapshotsCalculator;

use DateTime;
use Drupal;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\GroupMembershipRecordQuery;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "cris",
 *   label = "CRIS engagement stats"
 * )
 */
class CrisStats extends StatisticsSnapshotsCalculatorBase {

  use SuStatsTrait;

  public array $orgTypes = [
    'Community' => 'Community organisations',
    'External' => 'External organisations',
  ];

  public $roles = [
    'volunteering_cris_idea-cris_inte',
    'volunteering_cris_idea-placement',
  ];

  public function getFields() {
    $fields = [];

    $type = 'cris_idea';
    $label = 'CRIS idea';

    $orgTypeLabel = 'CRIS';

    // Webform signups
    // @todo

    // Expressions of interests, placements in CRIS ideas
    foreach ($this->roles as $role_id) {
      $role = GroupRole::load($role_id);

      $fields[$role_id . 'current'] = BaseFieldDefinition::create('integer')
        ->setLabel(t($role->label() . ' - records - current'))
        ->setSetting('group', 'CRIS interests and placements')
        ->setSetting('period', 'snapshot');

      $fields[$role_id . 'current_people'] = BaseFieldDefinition::create('integer')
        ->setLabel(t($role->label() . ' - unique people - current'))
        ->setSetting('group', 'CRIS interests and placements')
        ->setSetting('period', 'snapshot');

      $fields[$role_id . 'academic_year'] = BaseFieldDefinition::create('integer')
        ->setLabel(t($role->label() . ' - records - this academic year'))
        ->setSetting('group', 'CRIS interests and placements')
        ->setSetting('period', 'academic_year');

      $fields[$role_id . 'academic_year_people'] = BaseFieldDefinition::create('integer')
        ->setLabel(t($role->label() . ' - unique people - this academic year'))
        ->setSetting('group', 'CRIS interests and placements')
        ->setSetting('period', 'academic_year');
    }

    // Orgs working with CRIS
    $fields['cris_working_with_orgs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Organisations working with CRIS - shared (not CRIS-only)'))
      ->setSetting('group', 'CRIS organisations')
      ->setSetting('period', 'snapshot');

    $fields['cris_only_orgs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Organisations working with CRIS - CRIS-only'))
      ->setSetting('group', 'CRIS organisations')
      ->setSetting('period', 'snapshot');

    // Published CRIS ideas
    $fields['cris_ideas_published'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Published CRIS ideas'))
      ->setSetting('group', 'CRIS ideas')
      ->setSetting('period', 'snapshot');

    $states = $this->getCRISWorkflowStates();
    $fields['signups'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Submissions total - current'))
      ->setSetting('group', 'CRIS sign-ups')
      ->setSetting('period', 'snapshot');

    $fields['signups_academic_year'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Submissions total - this academic year '))
      ->setSetting('group', 'CRIS sign-ups')
      ->setSetting('period', 'academic_year');

    foreach ($states as $state_id => $stateLabel) {
      $fields['signups_state_' . $state_id] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Submissions currently at state: @stateLabel', ['@stateLabel' => $stateLabel]))
        ->setSetting('group', 'CRIS sign-ups')
        ->setSetting('period', 'snapshot');
    }
    return $fields;
  }

  /**
   * @throws \Exception
   */
  function calculate(array $segment_data = NULL, int $start = NULL, int $end = NULL): array {
    $date = $end;
    $academicYear = $this->getAcademicYearForSnapshot($start, $end);
    if (!$academicYear) {
      throw new Exception("CRIS stats: can't find year.");
    }

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = Drupal::service('group_membership_record.repository');

    $values = [];
    $gids = Drupal::entityQuery('group')
      ->condition('type', 'volunteering_org')
      ->condition('field_vol_organisation_type', array_keys($this->orgTypes), 'IN')
      ->execute();
    $orgs = Group::loadMultiple($gids);

    $current_uids = [];
    $year_uids = [];
    $inYear = 0;

    foreach ($this->roles as $role_id) {
      $memberRole = GroupRole::load($role_id);

      // Get current ones
      $current = 0;
      foreach ($orgs as $org) {
        $opps = su_volunteering_get_opps($org);
        if (!$opps) {
          continue;
        }
        foreach ($opps as $opp) {
          // Current:
          $queryCurrent = (new GroupMembershipRecordQuery())
            ->setGroup($opp)
            ->setRole($memberRole)
            ->setDates(new DateTime('@' . $date));
          $current += $queryCurrent->getCount();
          $current_uids += $queryCurrent->executeForUids();

          // Academic year:
          $queryYear = (new GroupMembershipRecordQuery())
            ->setGroup($opp)
            ->setRole($memberRole)
            ->setDates($academicYear->start, $academicYear->end);
          $inYear += $queryYear->getCount();
          $year_uids += $queryYear->executeForUids();
        }
      }

      $values[$role_id . 'current'] = $current;
      $values[$role_id . 'current_people'] = count(array_unique($current_uids));

      $values[$role_id . 'academic_year'] = $inYear;
      $values[$role_id . 'academic_year_people'] = count(array_unique($year_uids));
    }

    $values['cris_working_with_orgs'] = Drupal::entityQuery('group')
      ->condition('type', 'volunteering_org')
      ->condition('field_vol_org_work_with_cr', 1)
      ->condition('field_vol_org_cris_only', 0)
      ->count()
      ->execute();

    $values['cris_only_orgs'] = Drupal::entityQuery('group')
      ->condition('type', 'volunteering_org')
      ->condition('field_vol_org_cris_only', 1)
      ->count()
      ->execute();

    $values['cris_ideas_published'] = Drupal::entityQuery('group')
      ->condition('type', 'volunteering_cris_idea')
      ->condition('status', 1)
      ->count()
      ->execute();

    $values['signups'] = count(Drupal::service('webform_query')
      ->setWebform('webform_132555')
      ->execute());

    $startOfAcademicYearTimestamp = $this->calculateStartFromPeriod('shop', $start, $end, 'academic_year');
    $endOfAcademicYearTimestamp = $this->calculateEndFromPeriod('shop', $start, $end, 'academic_year');

    $values['signups_academic_year'] = count(Drupal::service('webform_query')
      ->setWebform('webform_132555')
      ->addCondition('created', $startOfAcademicYearTimestamp, '>=', 'webform_submission')
      ->addCondition('created', $endOfAcademicYearTimestamp, '<', 'webform_submission')
      ->execute());

    $query = Drupal::service('webform_query');
    $query->setWebform('webform_132555');
    $sids = array_column($query->execute(), 'sid');
    $submissions = WebformSubmission::loadMultiple($sids);

    $states = $this->getCRISWorkflowStates();
    foreach ($states as $stateId => $stateLabel) {
      $values['signups_state_' . $stateId] = count(webform_workflows_element_queries_filter_by_state($submissions, 'workflow', $stateId));
    }
    return $values;
  }

  public function getCRISWorkflowStates() {
    $workflow = Drupal\workflows\Entity\Workflow::load('cris_registration');
    $states = $workflow->getTypePlugin()->getStates();
    $result = [];
    foreach ($states as $state) {
      $result[$state->id()] = $state->label();
    }
    return $result;
  }

}
