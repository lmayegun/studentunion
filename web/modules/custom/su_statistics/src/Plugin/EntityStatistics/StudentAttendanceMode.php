<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentTaxonomyFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "student_attendance_mode",
 *   label = @Translation("Attendance mode"),
 *   description = @Translation(""),
 *   category = @Translation("Student demographic information"),
 *   weight = 10,
 * )
 **/
class StudentAttendanceMode extends StudentTaxonomyFieldBase {
  public $field = 'field_student_attendance_mode';
  public $vid = 'students_attendance_modes';
}
