<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use DateTime;
use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "student_age",
 *   label = @Translation("Age range"),
 *   description = @Translation(""),
 *   category = @Translation("Student demographic information"),
 *   weight = 10,
 * )
 **/
class StudentAge extends StudentFieldBase {
  public $field = 'field_student_date_of_birth';
  public function getPossibleDataValues() {
    return [
      '< 18',
      '18 - 21',
      '22 - 25',
      '25 - 29',
      '30 - 34',
      '35 - 39',
      '40 - 44',
      '45 - 49',
      '50 - 54',
      '55 - 59',
      '60 - 69',
      '70 - 79',
      '> 80'
    ];
  }

  public function generateEntityData($entity) {
    $dob = parent::generateEntityData($entity);
    $ranges = [
      '< 18' => [0, 17],
      '18 - 21' => [18, 21],
      '22 - 25' => [22, 25],
      '25 - 29' => [25, 29],
      '30 - 34' => [30, 34],
      '35 - 39' => [35, 39],
      '40 - 44' => [40, 44],
      '45 - 49' => [45, 49],
      '50 - 54' => [50, 54],
      '55 - 59' => [55, 59],
      '60 - 69' => [60, 69],
      '70 - 79' => [70, 79],
      '> 80' => [80, 300],
    ];

    $date = DateTime::createFromFormat('Y-m-d', $dob);
    if (!$date) {
      return 'Unknown';
    }
    $now = new DateTime();
    $interval = $now->diff($date);
    if ($interval) {
      $age = $interval->y;

      foreach ($ranges as $id => $values) {
        if ($age >= $values[0] && $age <= $values[1]) {
          return $id;
        }
      }
    }
    return 'Unknown';
  }
}
