<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\Core\Cache\Cache;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\UserFieldBase;
use Drupal\user\Entity\Role;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "union_member_type",
 *   label = @Translation("Union membership type"),
 *   description = @Translation(""),
 *   category = @Translation("Union information"),
 *   weight = 10,
 * )
 **/
class UnionMemberType extends UserFieldBase {

  public $useField = FALSE;
  public $multipleValues = FALSE;
  public $sort = 'desc';

  public function getPossibleDataValues() {
    return [
      'Union member / current student',
      'Associate/Visiting member',
      'Not a member',
    ];
  }

  public function generateEntityData($entity) {
    if ($entity->hasRole('member')) {
      return 'Union member / current student';
    }
    if ($entity->hasRole('associate_visiting')) {
      return 'Associate/Visiting member';
    }
    return 'Not a member';
  }
}
