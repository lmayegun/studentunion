<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentTaxonomyFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "student_fee_status",
 *   label = @Translation("Fee status"),
 *   description = @Translation(""),
 *   category = @Translation("Student demographic information"),
 *   weight = 10,
 * )
 **/
class StudentFeeStatus extends StudentTaxonomyFieldBase {
  public $field = 'field_student_fee_status';
  public $vid = 'students_fee_statuses';
}
