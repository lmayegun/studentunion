<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\Core\Cache\Cache;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\GroupRoleBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "club_society_member",
 *   label = @Translation("Current club and/or society member"),
 *   description = @Translation(""),
 *   category = @Translation("Clubs/Societies"),
 *   weight = 10,
 * )
 **/
class ClubSocietyMember extends GroupRoleBase {
  public $groupRoleId = ['club_society-member'];
  public $groupTypeId = 'club_society';
  public $valueHasRole = 'Member';
  public $valueNoRole = 'Non-member';

  public function getGroupsByCategory($parentGroups, $categoryName, $matchCategory = TRUE) {
    $cid = $this->getCacheId(__METHOD__);
    if ($item = \Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $groups = [];
    foreach ($parentGroups as $group) {
      if ($categoryField = $group->get('field_club_category')) {
        if (!$categoryField->referencedEntities) {
          continue;
        }
        $category = $categoryField->referencedEntities[0];
        if (!$category) {
          continue;
        }
        if ($category->label() == $categoryName && $matchCategory) {
          $groups[] = $group;
        } elseif ($category->label() != $categoryName && !$matchCategory) {
          $groups[] = $group;
        }
      }
    }

    \Drupal::cache()->set($cid, $groups, Cache::PERMANENT, ['group_list']);

    return $groups;
  }
}
