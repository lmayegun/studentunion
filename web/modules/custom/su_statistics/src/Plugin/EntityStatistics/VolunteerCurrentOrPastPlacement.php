<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\GroupMembershipRecordBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "volunteering_placement_past_or_present",
 *   label = @Translation("Volunteer placement past or present"),
 *   description = @Translation(""),
 *   category = @Translation("Volunteering"),
 *   weight = 10,
 * )
 **/
class VolunteerCurrentOrPastPlacement extends GroupMembershipRecordBase {
  public $groupRoleId = 'volunteering_opp-placement';
  public $groupTypeId = 'volunteering_opp';
  public $valueHasRole = 'Volunteer placement';
  public $valueNoRole = 'No placement';

  public $enabledRecord = false;
  public $currentRecord = false;
}
