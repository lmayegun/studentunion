<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentFieldBase;
use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentTaxonomyFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "student_study_mode",
 *   label = @Translation("Study mode"),
 *   description = @Translation(""),
 *   category = @Translation("Student demographic information"),
 *   weight = 10,
 * )
 **/
class StudentStudyMode extends StudentTaxonomyFieldBase {
  public $field = 'field_student_study_mode';
  public $vid = 'students_study_modes';
}
