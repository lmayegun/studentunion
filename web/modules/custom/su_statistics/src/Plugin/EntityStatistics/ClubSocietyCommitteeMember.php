<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "club_society_committee_member",
 *   label = @Translation("Current club and/or society committee member"),
 *   description = @Translation(""),
 *   category = @Translation("Clubs/Societies"),
 *   weight = 10,
 * )
 **/
class ClubSocietyCommitteeMember extends ClubSocietyMember {
  public $groupRoleId = [
    'club_society-president',
    'club_society-treasurer',
    'club_society-social_secretary',
    'club_society-captain',
    'club_society-producer',
    'club_society-media_officer',
    'club_society-committee_member',
  ];
  public $valueHasRole = 'Committee member';
  public $valueNoRole = 'Not committee member';
}
