<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "student_gender",
 *   label = @Translation("Gender"),
 *   description = @Translation(""),
 *   category = @Translation("Student demographic information"),
 *   weight = 10,
 * )
 **/
class StudentGender extends StudentFieldBase {
  public $field = 'field_student_gender';
  public function getDescription() {
    return t('Note: this gender data is from UCL\'s student data system, which gathers this information under the categories "Male", "Female" or "Other". This does not reflect in detail the full range of gender identities possible, and on this basis the data presented here should not be relied upon for full accuracy, and the person\'s self-identified gender should be preferenced where available.');
  }
}
