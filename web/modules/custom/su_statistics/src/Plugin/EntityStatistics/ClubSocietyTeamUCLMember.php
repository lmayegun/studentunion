<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\GroupMembershipRecordBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "club_society_team_ucl_member",
 *   label = @Translation("Current sports club member (TeamUCL)"),
 *   description = @Translation(""),
 *   category = @Translation("Clubs/Societies"),
 *   weight = 10,
 * )
 **/
class ClubSocietyTeamUCLMember extends ClubSocietyMember {
  public function getGroups() {
    $parentGroups = parent::getGroups();
    return $this->getGroupsByCategory($parentGroups, 'Sports', true);
  }
}
