<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\GroupRoleBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "volunteering_placement",
 *   label = @Translation("Current volunteer placement"),
 *   description = @Translation(""),
 *   category = @Translation("Volunteering"),
 *   weight = 10,
 * )
 **/
class VolunteerCurrentPlacement extends GroupRoleBase {
  public $groupRoleId = 'volunteering_opp-placement';
  public $groupTypeId = 'volunteering_opp';
  public $valueHasRole = 'Volunteer placement';
  public $valueNoRole = 'No placement';
}
