<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics\PluginBases;

use Drupal;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\ProfileFieldBase;

abstract class StudentFieldBase extends ProfileFieldBase {

  public $entityBundleId = 'student';

  public function checkEntityApplicability(string $entity_type_id, int $entity_id) {
    $entity = Drupal::entityTypeManager()
      ->getStorage($entity_type_id)
      ->load($entity_id);
    if (!$entity) {
      return [
        'applicable' => FALSE,
        'reason' => 'Cannot find entity.',
      ];
    }

    $profile = $this->getEntityToUse($entity);
    if (!$profile) {
      // \Drupal::logger('entity_statistics_extensible')->debug('Profile not found' . $entity_type_id . ' - ' . $entity_id);

      return [
        'applicable' => FALSE,
        'reason' => 'User is not a student, so we do not have this data.',
      ];
    }
    return parent::checkEntityApplicability($entity_type_id, $entity_id);
  }
}
