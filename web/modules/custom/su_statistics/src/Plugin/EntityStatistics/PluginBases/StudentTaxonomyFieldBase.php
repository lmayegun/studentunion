<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics\PluginBases;

use Drupal\profile\Entity\Profile;
use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\ProfileFieldBase;
use Drupal\taxonomy\Entity\Term;

abstract class StudentTaxonomyFieldBase extends StudentFieldBase {
  public $field = 'field_student_study_mode';
  public $vid = 'students_study_modes';

  public function getEntityFieldValue($entity, $field) {
    $term = $entity->get($this->field)->entity;
    return $term ? $term->label() : 'Unknown';
  }

  /**
   * @return array
   */
  public function getPossibleDataValues() {
    $term_data = [];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($this->vid);
    foreach ($terms as $term) {
      $term_data[] = $term->name;
    }
    return $term_data;
  }

  public function getPopulationData() {
    $values = array_fill_keys($this->getPossibleDataValues(), 0);

    foreach ($values as $label => $value) {
      $term_id = su_drupal_helper_get_taxonomy_term_by_name($label, $this->vid);
      if (!$term_id) {
        continue;
      }

      $ids = \Drupal::entityQuery('profile')
        ->condition('type', 'student')
        ->condition('status', 1)
        ->condition($this->field . '.entity:taxonomy_term.tid', $term_id)
        ->accessCheck(FALSE)
        ->execute();
      $total = count($ids);
      $values[$label] = $total;
    }

    return $values;
  }
}
