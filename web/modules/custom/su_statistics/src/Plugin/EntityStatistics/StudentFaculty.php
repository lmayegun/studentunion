<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\su_statistics\Plugin\EntityStatistics\PluginBases\StudentTaxonomyFieldBase;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "student_faculty",
 *   label = @Translation("Faculty"),
 *   description = @Translation(""),
 *   category = @Translation("Student demographic information"),
 *   weight = 10,
 * )
 **/
class StudentFaculty extends StudentTaxonomyFieldBase {
  public $field = 'field_student_faculty';
  public $vid = 'ucl_faculties_and_departments';

  /**
   * @return array
   */
  public function getPossibleDataValues() {
    $term_data = [];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($this->vid);
    foreach ($terms as $term) {
      // Faculties don't have parents
      if ($term->parents && $term->parents[0]) {
        continue;
      }
      $term_data[] = $term->name;
    }
    return $term_data;
  }
}
