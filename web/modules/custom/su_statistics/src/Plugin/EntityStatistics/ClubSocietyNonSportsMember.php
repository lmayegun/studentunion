<?php

namespace Drupal\su_statistics\Plugin\EntityStatistics;

use Drupal\entity_statistics_extensible\Plugin\EntityStatistics\PluginBases\GroupMembershipRecordBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @EntityStatisticsGeneratorPlugin(
 *   id = "club_society_society_member",
 *   label = @Translation("Current society (non-sports) member"),
 *   description = @Translation(""),
 *   category = @Translation("Clubs/Societies"),
 *   weight = 10,
 * )
 **/
class ClubSocietyNonSportsMember extends ClubSocietyMember {
  public function getGroups() {
    $parentGroups = parent::getGroups();
    return $this->getGroupsByCategory($parentGroups, 'Sports', FALSE);
  }
}
