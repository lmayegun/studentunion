<?php

namespace Drupal\su_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\profile\Entity\Profile;
use Drupal\taxonomy\Entity\Term;

class StudentDataQuickAnalysisController extends ControllerBase {

  public function render($set_id = NULL) {
    if (!$set_id) {
      return [];
    }

    $tempstore = \Drupal::service('tempstore.private')->get('entity_statistics_extensible');
    $build = $tempstore->get($set_id . '_build');
    $render = \Drupal::service('renderer')->render($build);
    return ['#markup' => $render];
  }
}
