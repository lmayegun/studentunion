<?php

namespace Drupal\su_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\profile\Entity\Profile;
use Drupal\taxonomy\Entity\Term;

class StudentChartsController extends ControllerBase {

  public $uids = [];
  public $profileIds = [];

  public function setUids($uids) {
    $this->uids = $uids;
    $this->profileIds = [];
  }

  public function render($uids = []) {
    $this->setUids($uids);

    $build = [];

    $exampleStudents = \Drupal::entityQuery('profile')
      ->condition('type', 'student')
      ->condition('status', 1)
      ->range(0, 1)
      ->execute();
    $profile = Profile::load(reset($exampleStudents));

    $taxonomyFields = ['field_student_study_mode', 'field_student_fee_status', 'field_student_caring_resp', 'field_student_attendance_mode'];
    $valueFields = ['field_student_gender'];

    $members =  (int) \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', ['member'], 'IN')
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['totals'] = [
      '#markup' => t(
        '<table><tr><th>Total members</th><td>@members</td></tr></table>',
        [
          '@members' => $members,
        ]
      ),
    ];

    foreach ($taxonomyFields as $field) {
      $build[$field] = $this->generatePieChartTaxonomyTerm($field, $profile);
    }
    $build[$field] = $this->generatePieChartAge($field, $profile);

    return $build;
  }

  public function filterProfilesByUids($profileIds) {
    return $profileIds;
  }

  public function generatePieChartAge() {
    $dates = [
      'under 18' =>
      [
        strtotime('-17 years') - 1,
        strtotime('0 years')
      ],
      '18 - 21' =>
      [
        strtotime('-21 years') - 1,
        strtotime('-18 years')
      ],
      '22 - 25' =>
      [
        strtotime('-25 years') - 1,
        strtotime('-21 years')
      ],
      '26 - 29' => [
        strtotime('-29 years') - 1,
        strtotime('-26 years')
      ],
      '30+' => [
        strtotime('-1000 years'),
        strtotime('-30 years')
      ],
    ];
    $total = 0;
    $counts = [];
    foreach ($dates as $label => $timestamps) {
      $start = DrupalDateTime::createFromTimestamp($timestamps[0]);
      $end = DrupalDateTime::createFromTimestamp($timestamps[1]);
      $start->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $end->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $start = $start->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $end = $end->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

      $query = \Drupal::entityQuery('profile')
        ->condition('type', 'student')
        ->condition('status', 1)
        ->condition('field_student_date_of_birth', $start, '>=')
        ->condition('field_student_date_of_birth', $end, '<');

      if (count($this->uids) > 0) {
        $query->condition('uid', $this->uids, 'IN');
      }

      $ids = $query->execute();
      $counts[$label] = count($ids);
      $total += count($ids);
    }

    // Define a series to be used in multiple examples.
    $series = [
      '#type' => 'chart_data',
      '#title' => $this->t('Age'),
      '#data' => $counts,
    ];

    // Define an x-axis to be used in multiple examples.
    $xaxis = [
      '#type' => 'chart_xaxis',
      '#title' => $this->t('Age'),
      '#labels' => array_keys($dates),
    ];

    // Define a y-axis to be used in multiple examples.
    $yaxis = [
      '#type' => 'chart_yaxis',
      '#title' => $this->t('Number of members'),
    ];

    return $this->generatePieChart(t('Age'), $series, $xaxis, $yaxis);
  }

  public function generatePieChartTaxonomyTerm($field, $exampleProfile) {
    $studentDataDefinitions = \Drupal::service('su_students.data_definitions');

    $fieldDefinition = $exampleProfile->get($field)->getFieldDefinition();
    $fieldLabel = $fieldDefinition->getLabel();
    $vid = reset($fieldDefinition->getSetting('handler_settings')['target_bundles']);

    $term_ids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $vid)
      ->execute();
    $terms = Term::loadMultiple($term_ids);
    $counts = [];
    $labels = [];
    foreach ($terms as $term) {
      $labels[$term->id()] = $term->label();
      if ($vid == 'students_fee_statuses') {
        $labels[$term->id()] = $studentDataDefinitions->getMappedNameFeeStatus()[$term->label()];
      }

      if (!isset($counts[$term->id()])) {
        $counts[$term->id()] = 0;

        $query = \Drupal::entityQuery('profile')
          ->condition('type', 'student')
          ->condition('status', 1)
          ->condition($field, $term->id());

        if (count($this->uids) > 0) {
          $query->condition('uid', $this->uids, 'IN');
        }

        $profiles = $query->execute();
        $counts[$term->id()] = count($profiles);
      }
    }

    // Define a series to be used in multiple examples.
    $series = [
      '#type' => 'chart_data',
      '#title' => $this->t($fieldLabel),
      '#data' => $counts,
    ];

    // Define an x-axis to be used in multiple examples.
    $xaxis = [
      '#type' => 'chart_xaxis',
      '#title' => $this->t($fieldLabel),
      '#labels' => $labels,
    ];

    // Define a y-axis to be used in multiple examples.
    $yaxis = [
      '#type' => 'chart_yaxis',
      '#title' => $this->t('Number of members'),
    ];

    return $this->generatePieChart($fieldLabel, $series, $xaxis, $yaxis);
  }

  public function generatePieChart($fieldLabel, $series, $xaxis, $yaxis) {

    return [
      '#type' => 'chart',
      '#chart_type' => 'pie',
      '#title' => $fieldLabel,
      '#polar' => FALSE,
      '#tooltips' => [], //$charts_settings->get('charts_default_settings.display.tooltips'),

      'series' => $series,
      'x_axis' => $xaxis,
      'y_axis' => $yaxis,

      '#raw_options' => [], // e.g. ['chart' => ['backgroundColor' => '#000000']],
      // '#colors' => ['#f26640', '#082244', '#6c3f99', '#2aaa9e', '#fec340'],

      '#title_font_size' => 14,
      '#title_font_weight' => 'bold',

      '#legend' => TRUE,
      '#legend_position' => 'right',

      '#exporting_library' => TRUE,

      '#data_labels' => TRUE,
      '#tooltips' => TRUE,
      '#tooltips_use_html' => TRUE,


      '#cache' => $this->getCacheTags(),
    ];
  }

  public function getCacheTags() {
    return [
      'max-age' => 60 * 60 * 24 * 7,
      'tags' => [
        'profile_list:student',
      ]
    ];
  }
}
