<?php

namespace Drupal\su_statistics\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshot;

/**
 * Controller for statistics snapshots.
 */
class StatsDashboardController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @return array
   */
  public function getDashboard() {
    $build = [];

    $start = strtotime('-24 hours');
    $end = strtotime('now');
    $build['as_of'] = [
      '#markup' => t('<div class="messages messages--info">Current as at <b>@date</b>.</div>', [
        '@date' => Drupal::service('date.formatter')
          ->format($end, 'long'),
      ]),
    ];

    $build['summary'] = [
      '#markup' => $this->getSummary($start, $end),
    ];

    $build['membership'] = [
      '#prefix' => '<h2>Our membership</h2>',
      'mem' => (new StudentChartsController)->render(),
    ];

    return $build;
  }

  /**
   * @param $start
   * @param $end
   * @param $linebreak
   *
   * @return string
   */
  public static function getSummary($start, $end, $linebreak = '<br>') {
    $repository = Drupal::service('statistics_snapshots.repository');
    $pluginManager = Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $userPlugin = $pluginManager->createInstance('user');
    $cscPlugin = $pluginManager->createInstance('club_soc');
    $volunteeringPlugin = $pluginManager->createInstance('volunteering');
    $shopPlugin = $pluginManager->createInstance('shop');
    $nodePlugin = $pluginManager->createInstance('node');
    $repsPlugin = $pluginManager->createInstance('academic_reps');

    $output[] = '<i>Please note these summary statistics may be calculated differently than some departments calculate them. Please contact individual departments for detailed up-to-date numbers.</i>';

    $output[] = $linebreak;

    $pages = $repository->getRefreshedStatValue($nodePlugin, 'nodes_page', $start, $end) + $repository->getRefreshedStatValue($nodePlugin, 'nodes_page_full', $start, $end);
    $output[] = '👥 Active website users (active in last year): ' . number_format($repository->getRefreshedStatValue($userPlugin, 'users_active_in_last_year', $start, $end));
    $output[] = '🔑 Logged in during the period: ' . number_format($repository->getRefreshedStatValue($userPlugin, 'logged_in', $start, $end));
    $output[] = '📄 Pages published on the website: ' . number_format($pages);

    $output[] = $linebreak;

    $output[] = '👥 Union members: ' . number_format($repository->getRefreshedStatValue($userPlugin, 'rolemember'));
    $output[] = '<i>[NOTE this is heavily affected by time of year / other factors, please ask depending on use]</i>';

    $output[] = $linebreak;

    $output[] = '<b>Clubs/Societies: </b>';
    $output[] = '🏃‍♀️ Club/Society members: ' . number_format($repository->getRefreshedStatValue($cscPlugin, 'members', $start, $end));
    $output[] = '🏃‍♀️ Club/Society memberships: ' . number_format($repository->getRefreshedStatValue($cscPlugin, 'memberships', $start, $end));
    $output[] = '🎭 Society members: ' . number_format($repository->getRefreshedStatValue($cscPlugin, 'members_societies', $start, $end));
    $output[] = '🎭 Society memberships: ' . number_format($repository->getRefreshedStatValue($cscPlugin, 'memberships_societies', $start, $end));
    $output[] = '🏐 Team UCL (club) members: ' . number_format($repository->getRefreshedStatValue($cscPlugin, 'members_clubs', $start, $end));
    $output[] = '🏐 Team UCL (club) memberships: ' . number_format($repository->getRefreshedStatValue($cscPlugin, 'memberships_clubs', $start, $end));

    $output[] = $linebreak;

    $output[] = '<b>Academic reps: </b>';
    $output[] = '🧑🏽‍🎓 Total academic reps: ' . number_format($repository->getRefreshedStatValue($repsPlugin, 'academic_reps', $start, $end));

    $output[] = $linebreak;

    $output[] = '<b>Volunteering totals for this academic year: </b>';
    $orgTypes = [
      'Community' => 'Community organisations',
      'SLP' => 'Student-Led Projects',
    ];
    $roles = [
      'volunteering_opp-interest' => 'expressions of interest',
      'volunteering_opp-placement' => 'placements',
    ];

    foreach ($orgTypes as $type => $orgTitle) {
      foreach ($roles as $role => $roleName) {
        $value = $repository->getRefreshedStatValue($volunteeringPlugin, $role . $type . 'academic_year', $start, $end);
        $output[] = '🫂 ' . $orgTitle . ' ' . $roleName . ': ' . number_format($value);
      }
    }

    $output[] = $linebreak;

    $output[] = '<b>Online shop stats: </b>';
    $output[] = '💰 Online income: ' . number_format($repository->getRefreshedStatValue($shopPlugin, 'online_income', $start, $end));
    $output[] = '🛒 Completed orders: ' . number_format($repository->getRefreshedStatValue($shopPlugin, 'orders', $start, $end));
    $output[] = '📦 Items sold: ' . number_format($repository->getRefreshedStatValue($shopPlugin, 'items_sold', $start, $end));

    $output[] = $linebreak;

    $output[] = '<b>Event stats: </b>';
    $tickets = $repository->getRefreshedStatValue($shopPlugin, 'items_sold_event', $start, $end);
    $available = $repository->getRefreshedStatValue($shopPlugin, 'items_available_event', $start, $end);
    $available_products = number_format($repository->getRefreshedStatValue($shopPlugin, 'items_available_p_event', $start, $end));
    $output[] = '🎫 Tickets/bookings: ' . number_format($tickets);
    $output[] = '📅 Events available for sale/booking: ' . $available_products;
    $output[] = '📅 Event variations available for sale/booking: ' . number_format($available);
    $output[] = '🪙 Average sales/bookings per event variation (indicative demand): ' . ($available > 0 ? number_format(round(($tickets / $available))) : '');

    $output[] = $linebreak;

    return implode($linebreak, $output);
  }

  public function getLatestSnapshot() {
    $query = Drupal::entityQuery('statistics_snapshot')
      ->condition('processing_end', 0, '>')
      ->sort('created', 'DESC');
    $ids = $query->execute();
    $id = reset($ids);
    return StatisticsSnapshot::load($id);
  }

}
