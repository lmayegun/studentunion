drush queue:delete statistics_snapshots_queue

drush ev 'statistics_snapshots_start_snapshot(strtotime("-1 day"), strtotime("now"));'

drush queue:run statistics_snapshots_queue -vvv
