<?php

namespace Drupal\su_jobshop\EventSubscriber;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderEventSubscriber implements EventSubscriberInterface
{

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
    $events = [
      'commerce_order.order.paid' => 'onPaid',
    ];
    return $events;
  }

  /**
   * Places the order after it has been fully paid through an off-site gateway.
   *
   * Off-site payments can only be made at checkout.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The event.
   */
  public function onPaid(OrderEvent $event)
  {
    $order = $event->getOrder();
    /**
     * @var OrderItem $orderItem
     */
    $orderItems = $order->getItems();
    foreach ($orderItems as $orderItem) {
      if ($orderItem->get('type')->getString() == 'jobshop_advertisement') {
        $nodes = $orderItem->get('field_jobshop_advertisement')->referencedEntities();
        if (isset($nodes[0])) {
          $node = $nodes[0];
          $node->set('moderation_state', 'paid_awaiting_approval');
          $node->save();
        }
      }
    }
  }
}
