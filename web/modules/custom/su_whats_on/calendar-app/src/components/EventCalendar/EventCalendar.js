import {Calendar, momentLocalizer} from "react-big-calendar";
import moment from "moment";
import ListView from './ListView';
import AgendaEvent from './AgendaEvent';

import {useHistory} from "react-router-dom"

moment.locale("en-GB", {
  week: {
    dow: 1 //Monday is the first day of the week.
  },
  longDateFormat: {
    //time range format on events
    LT: "H:mm",
    LTS: "H:mm:ss",
    //date range format used on agenda toolbar
    L: "DD/MM/YYYY",
    l: "D M YYYY",
    LL: "dddd D MMMM YYYY",
    ll: "D MMM YYYY",
    LLL: "Do MMMM YYYY LT",
    lll: "D MMM YYYY LT",
    LLLL: "dddd, MMMM Do YYYY LT",
    llll: "dddd, MMMM D YYYY LT"
  }
});

const localizer = momentLocalizer(moment);

export default function EventCalendar(props) {
  const history = useHistory();
  let eventsData = props.eventsData;

  // console.log('events data before', props.eventsData);
  const events = [];
  if (eventsData.length > 0) {
    var datesForProducts = {};
    for (let i = 0; i < eventsData.length; i++) {
      let event = eventsData[i];
      let start = new Date(event.field_date_range_value * 1000);
      let end = new Date(event.field_date_range_end_value * 1000);

      if (!datesForProducts[event.product_id]) {
        datesForProducts[event.product_id] = [];
      }

      let dateKey = event.field_date_range_value + '-' + event.field_date_range_end_value;
      if (datesForProducts[event.product_id].includes(dateKey)) {
        // Don't include an event variation if the same product already has one
        // at that date and time
      }
      else {
        datesForProducts[event.product_id].push(dateKey);

        let eventTransformed = {
          title: event.title,
          start: start,
          end: end,
          link: event.view_commerce_product_variation,
          category: event.field_event_category_id,
          category_name: event.field_event_category,
          online: event.field_event_online,
          logo: event.field_logo,
          host: event.field_event_owner_group,
          tags: event.field_event_tags,
          image: event.field_event_image_card || event.field_event_image_banner,
          venue: event.field_event_venue,
          duotone: event.field_duotone,
          body: event.body
        };

        events.push(eventTransformed);
      }
    }
  }

  const handleSelectEvent = (link) => {
    // window.location.href = `${link}`;
    window.open(link);
  };

  function NormalEvent(eventProps) {
    console.log('NormalEvent', eventProps, props.currentView);
    let event = eventProps.event;
    let fullView = props.currentView != 'month';
    return (<>{event.title}
        {fullView && event.host ?
          <div
            class={"rbc-event-content--group"}>{event.host}</div> : <></>}
      </>
    )
  }

  //https://github.com/jquense/react-big-calendar/blob/master/stories/demos/exampleCode/rendering.js
  // https://jquense.github.io/react-big-calendar/examples/index.html?path=/story/examples--example-8

  const handleStyles = (category_id, online, startDate, endDate) => {
    let style = {
      order: (startDate.getTime() / 1000)
    };
    let classes = "event-category-" + category_id;
    let now = new Date();
    if (endDate < now) {
      classes += " past";
    }
    if (online == "On") {
      classes += " online";
    }

    return {
      className: classes,
      style: style,
      // startHour: ("0" + startDate.getHours()).slice(-2),
    };
  };

  var components = {
    event: NormalEvent,
    agenda: {
      event: AgendaEvent
    }
  };

  // return calendar component with default view list if screen is mobile
  return (
    <div className='whats-on-calendar' style={{minHeight: 900}}>
      <Calendar
        localizer={localizer}
        events={events}
        defaultDate={props.dateRange ? props.dateRange[0] : new Date()}
        onRangeChange={(dateArrayOrObject, viewName) => {
          console.log('onRangeChange', dateArrayOrObject, dateArrayOrObject);
          if (
            typeof dateArrayOrObject === 'object' &&
            !Array.isArray(dateArrayOrObject) &&
            dateArrayOrObject !== null
          ) {
            props.setDateRange([dateArrayOrObject.start, dateArrayOrObject.end]);
          }
          else {
            props.setDateRange(dateArrayOrObject);
          }
        }}
        elementProps={{className: 'online'}}
        step={60}
        popup
        view={props.currentView}
        onView={(view) => {
          const params = (new URL(document.location)).searchParams
          params.set("v", view)
          history.push({search: params.toString()})
          props.setCurrentView(view);
        }}
        onSelectEvent={(event) => handleSelectEvent(event.link)}
        eventPropGetter={(event) => handleStyles(event.category, event.online, event.start, event.end)}
        components={components}
        defaultView={props.display > 1200 ? 'week' : 'list'}
        views={{
          week: true,
          month: true,
          agenda: true,
          list: ListView
        }}
        messages={{
          list: 'List'
        }}
      />
    </div>
  );
};

