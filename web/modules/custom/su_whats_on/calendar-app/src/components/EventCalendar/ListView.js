import PropTypes from "prop-types";
import {useRef} from "react";
import addClass from "dom-helpers/addClass";
import removeClass from "dom-helpers/removeClass";
import getWidth from "dom-helpers/width";
import scrollbarSize from "dom-helpers/scrollbarSize";
import Card from "../Card/Card";

import * as dates from "react-big-calendar/lib/utils/dates";
import {navigate} from "react-big-calendar/lib/utils/constants";
import {isSelected} from "react-big-calendar/lib/utils/selection";
import {inRange} from "react-big-calendar/lib/utils/eventLevels";

function ListView({
                    selected,
                    getters,
                    accessors,
                    localizer,
                    components,
                    length,
                    date,
                    events,
                  }) {
  const headerRef = useRef(null);
  const dateColRef = useRef(null);
  const timeColRef = useRef(null);
  const contentRef = useRef(null);
  const tbodyRef = useRef(null);

  // useEffect(() => {
  // _adjustHeader();
  // });

  const renderDay = (day, events, dayKey) => {
    const {event: Event, date: ListViewDate} = components;

    events = events.filter((e) =>
      inRange(e, dates.startOf(day, "day"), dates.endOf(day, "day"),
        accessors, localizer));

    return events.map((event, idx) => {
      let title = accessors.title(event);
      let end = accessors.end(event);
      let start = accessors.start(event);
      let link = event.link;
      let category_name = event.category_name;
      let online = event.online;
      let image = event.image;
      let logo = event.logo;
      let host = event.host;
      let tags = event.tags;
      let venue = event.venue;
      let duotone = event.duotone;
      let body = event.body;

      const userProps = getters.eventProp(
        event,
        start,
        end,
        isSelected(event, selected)
      );

      let dateLabel = idx === 0 && localizer.format(day, "dddd D MMMM YYYY");
      let first =
        idx === 0 ? (
          <div className="day-header">
            {ListViewDate ? (
              <ListViewDate day={day} label={dateLabel}/>
            ) : (
              dateLabel
            )}
          </div>
        ) : false;

      return (
        <tr>
          <td>
            {first}
            <div
              key={dayKey + "_" + idx}
              className={userProps.className + " " + 'card-grid'}
              style={userProps.style}
            >
              <Card
                className={"card-horizontal"}
                event={event}
                date={localizer.format(start, "dddd D MMMM YYYY")}
                times={timeRangeLabel(day, event)}
                title={title}
                category={category_name}
                link={link}
                online={online}
                logo={logo}
                host={host}
                tags={tags}
                image={image}
                venue={venue}
                duotone={duotone}
                body={body}
              />
            </div>
          </td>
        </tr>
      );
    }, []);
  };

  const timeRangeLabel = (day, event) => {
    let labelClass = "",
      TimeComponent = components.time,
      label = localizer.messages.allDay;

    let end = accessors.end(event);
    let start = accessors.start(event);

    if (!accessors.allDay(event)) {
      if (dates.eq(start, end)) {
        label = localizer.format(start, "agendaTimeFormat");
      }
      else if (dates.eq(start, end, "day")) {
        label = localizer.format({start, end}, "agendaTimeRangeFormat");
      }
      else if (dates.eq(day, start, "day")) {
        label = localizer.format(start, "agendaTimeFormat");
      }
      else if (dates.eq(day, end, "day")) {
        label = localizer.format(end, "agendaTimeFormat");
      }
    }

    if (dates.gt(day, start, "day")) {
      labelClass = "rbc-continues-prior";
    }
    if (dates.lt(day, end, "day")) {
      labelClass += " rbc-continues-after";
    }

    return (
      <span className={labelClass.trim()}>
        {TimeComponent ? (
          <TimeComponent event={event} day={day} label={label}/>
        ) : (
          label
        )}
      </span>
    );
  };

  const _adjustHeader = () => {
    if (!tbodyRef.current) {
      return;
    }

    let header = headerRef.current;
    let firstRow = tbodyRef.current.firstChild;

    if (!firstRow) {
      return;
    }

    let isOverflowing =
      contentRef.current.scrollHeight > contentRef.current.clientHeight;

    let _widths = [];
    let widths = _widths;

    _widths = [getWidth(firstRow.children[0]), getWidth(firstRow.children[1])];

    if (widths[0] !== _widths[0] || widths[1] !== _widths[1]) {
      dateColRef.current.style.width = _widths[0] + "px";
      timeColRef.current.style.width = _widths[1] + "px";
    }

    if (isOverflowing) {
      addClass(header, "rbc-header-overflowing");
      header.style.marginRight = scrollbarSize() + "px";
    }
    else {
      removeClass(header, "rbc-header-overflowing");
    }
  };

  let {messages} = localizer;
  let end = dates.add(date, length, "day");

  let range = dates.range(date, end, "day");

  events = events.filter((event) =>
    inRange(
      event,
      dates.startOf(date, "day"),
      dates.endOf(end, "day"),
      accessors,
      localizer
    )
  );

  events.sort((a, b) => +accessors.start(a) - +accessors.start(b));

  return (
    <div className="rbc-list-view">
      {events.length !== 0 ? (
        <>
          <div ref={headerRef}>
            <th ref={dateColRef}></th>
            <th ref={timeColRef}></th>
          </div>
          <div className="rbc-list-content" ref={contentRef}>
            <table className="rbc-list-table">
              <tbody ref={tbodyRef}>
              {range.map((day, idx) => renderDay(day, events, idx))}
              </tbody>
            </table>
          </div>
        </>
      ) : (
        <span className="rbc-agenda-empty">{messages.noCardsInRange}</span>
      )}
    </div>
  );
}

ListView.propTypes = {
  events: PropTypes.array,
  date: PropTypes.instanceOf(Date),
  length: PropTypes.number.isRequired,

  selected: PropTypes.object,

  accessors: PropTypes.object.isRequired,
  components: PropTypes.object.isRequired,
  getters: PropTypes.object.isRequired,
  localizer: PropTypes.object.isRequired,
};

ListView.defaultProps = {
  length: 60,
};

ListView.range = (start, {length = ListView.defaultProps.length}) => {
  let end = dates.add(start, length, "day");
  return {start, end};
};

ListView.navigate = (
  date,
  action,
  {length = ListView.defaultProps.length}
) => {
  switch (action) {
    case navigate.PREVIOUS:
      return dates.add(date, -length, "day");

    case navigate.NEXT:
      return dates.add(date, length, "day");

    default:
      return date;
  }
};

ListView.title = (
  start,
  {length = ListView.defaultProps.length, localizer}
) => {
  let end = dates.add(start, length, "day");
  return localizer.format({start, end}, "agendaHeaderFormat");
};

export default ListView;
