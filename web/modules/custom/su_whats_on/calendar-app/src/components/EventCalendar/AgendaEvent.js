

export default function EventAgenda({ event }) {
    return (
        <span className={'event-category ' + event.category}>
            <a href={event.link}>{event.title}</a>
            {/* <p>{event.desc}</p> */}
        </span>
    )
}
