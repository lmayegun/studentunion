import { LinearProgress } from "@mui/material";

export default function Loader(props) {
    return <LinearProgress />;
    // return (props.error == true ?
    //     <div className="loader-box">{props.message}</div> :
    //     <div className="loader-box" style={{ gridColumn: 2, height: `${props.height}rem` }}>
    //         <div className="union-loader" style={{ margin: 'auto' }} />
    //     </div>);
}
