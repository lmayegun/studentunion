import _ from "lodash";
import HTMLReactParser from "html-react-parser";

export default function Card(props) {
  return (
    <div
      className={`card card-horizontal card-horizontal-sm ${props.image ? '' : 'card-no-image'}`}>
      <a className="card-link" href={props.link}></a>
      {props.image ? (
        <div className="image-fieldset">
          <div className="image-fields">
            {props.logo ? (
                <div className="card-field host_icon"
                     style={{backgroundImage: `url(${props.logo})`}}></div>) :
              <div></div>}
            {props.host ? (
                <div
                  className="card-field host_text">{_.unescape(props.host)}</div>) :
              <div></div>
            }
            <div
              className="card-field image_box">{_.unescape(props.category)}</div>
          </div>
          <div
            className={`card-duotone-field ${_.unescape(props.duotone)}`}
            style={{backgroundImage: `url(${props.image})`}}
          ></div>
        </div>
      ) : (
        <></>
      )}
      <div className="content fieldset">
        <div className="content-fields">
          <div className="fields-text">
            {!props.image ? (
              <div className="host-fields">
                {props.logo ? (
                    <div className="card-field host_icon"
                         style={{backgroundImage: `url(${props.logo})`}}></div>) :
                  <div></div>}
                {props.host ? (
                    <div className="card-field host_text"
                         style={{color: ""}}>{_.unescape(props.host)}</div>) :
                  <div></div>
                }
              </div>
            ) : (
              <div></div>
            )}
            <div
              className="card-field card_title_field">{_.unescape(props.title)}</div>
            <div
              className="card-field card_subheading_1">{props.online == 'On' ? 'Online • ' : ''}{props.date} {props.times}</div>
            {/* {props.host ? <div className="card-field card_subheading_2">{_.unescape(props.host)}</div> : <></>} */}
            {props.body ? <div
              className="card-field card_body_text_preview">{HTMLReactParser(props.body)}</div> : <></>}

          </div>
          <div class='fields-tags'>
            <a class='card-field card_button_text' href={props.link}>Book</a>
            <div class='card-field body_box'>{props.price || props.free}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
