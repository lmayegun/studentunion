import { useState, useRef } from "react";
import TextField from "@mui/material/TextField";
import { Autocomplete } from '@mui/material';

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop - 16);

export default function AutocompleteFilter (props) {

  const timeOut = useRef(null);
  const [inputValue, setInputValue] = useState('');
  const [loading, setLoading] = useState(false);
  const autocompleteRef = useRef(null);

  //console.log("input value", inputValue);
  //console.log("textsearch props", props);
  //console.log("props test on top", props.filters.textSearch);
  return (
    <Autocomplete
      freeSolo
      fullWidth
      inputValue={inputValue}
      options={props.autocompleteOptions}
      //   groupBy={(option) => option.group}
      loading={loading}
      loadingText={"Loading search results below..."}
      onChange={(event, newValue, reason) => {
        // console.log("onChange newValue of autocomplete", newValue, reason);
        if (reason === "clear")
        {
          // props.setSearchFreeText("");

        } else if (reason == 'select-option')
        {
          // Select autocomplete
          // Would need to change to fit searchCategories, possibly

          // Select an event from the dropdown
          if (newValue.url)
          {
            window.open(newValue.url);

            // Select a category from the dropdown
          } else
          {
            setInputValue('');
            props.setSearchCategories(
              [newValue.filterValue]
            );
          }
        } else
        {
          // props.setSearchFreeText(newValue);
        }
      }}
      onInputChange={(event, newInputValue, reason) => {
        // console.log("onInputChange newValue of autocomplete", newInputValue, reason);
        setInputValue(newInputValue);

        if (newInputValue.url)
        {
        } else
        {
          if (reason === "clear" || (reason == "reset"))
          {
            setInputValue("");
            props.setSearchFreeText("");
          } else if (newInputValue.length > 2)
          {
            setLoading(true);
            clearTimeout(timeOut.current);
            timeOut.current = setTimeout(
              () => {
                props.setSearchFreeText(newInputValue);
                setLoading(false);
              },
              600
            );
          }
        }
      }}
      ref={autocompleteRef}
      onFocus={(event) => {
        scrollToRef(autocompleteRef);
      }}
      getOptionLabel={(option) => {
        return option.title ? option.title : option.name ? option.name : option;
      }}
      renderInput={(params) => (
        <TextField
          label="Search"
          // variant="outlined" 
          {...params}
        />
      )}
    />
  );
}