import React from "react";
import Button from "@mui/material/Button";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import HTMLReactParser from "html-react-parser";

import {useTheme} from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

import {useHistory} from "react-router-dom"
import {Box, Chip, MenuItem, OutlinedInput, Select} from "@mui/material";

export default function CategoryFilter(props) {
  const history = useHistory();

  //another function
  const updateLocalAndParentState = (event, checked, category) => {
    // console.log('updateLocalAndParentState', event, checked, category);

    var newChecked = [...checked];
    if (!checked || !checked.includes(category)) {
      newChecked.push(category);
    }
    else {
      newChecked = checked.filter((e) => e !== category);
    }

    props.setSearchCategories(newChecked);

    const params = (new URL(document.location)).searchParams;
    params.set("c", newChecked.join(';'));
    history.push({search: params.toString()});
  };

  const updateLocalAndParentStateSet = (event, newChecked) => {
    // console.log("updateLocalAndParentStateSet", event, newChecked);

    props.setSearchCategories(newChecked);

    const params = (new URL(document.location)).searchParams;
    params.set("c", newChecked.join(';'));
    history.push({search: params.toString()});
  };

  const theme = useTheme();
  const matches = useMediaQuery((theme) => theme.breakpoints.down('lg'));

  // console.log('props.categories', props.categories);
  // var dropdown = matches;
  var dropdown = false;
  if (dropdown) {
    return (<Select
      value={props.searchCategories}
      multiple
      onChange={(event) => {
        // console.log('Select change', event);
        var categories = [event.target.value];
        updateLocalAndParentStateSet(
          event,
          categories
        );
      }}
      input={<OutlinedInput id="select-multiple-chip" label="Chip"/>}
      renderValue={(selected) => {
        // console.log('renderValue', selected, props.categories);

        return (
          <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
            {selected.map((value) => {
              value = parseInt(value);
              var category = props.categories.find(category => category.id == value);
              return (
                <Chip key={value} label={category.name}/>
              );
            })}
          </Box>
        );
      }
      }
    >
      <MenuItem value="">
        <em>All categories</em>
      </MenuItem>
      {
        props.categories && props.categories.map((category, i) => {
          let categoryId = category.id;
          let categoryName = category.name;
          let categoryClassName = 'event-category-' + categoryId;
          const checked =
            props.searchCategories.includes(categoryId) ||
            props.searchCategories.length == 0;
          return (
            <MenuItem key={categoryId}
                      value={categoryId}>{categoryName}</MenuItem>
          );
        })
      }
    </Select>);
  }
  else {
    return (
      <div className="category-filter row">
        {props.categories && props.categories.map((category, i) => {
          let categoryId = category.id;
          let categoryName = category.name;
          let categoryClassName = 'event-category-' + categoryId;
          const checked =
            props.searchCategories.includes(categoryId) ||
            props.searchCategories.length == 0;

          if (categoryName) {

            return (
              <div className="category-btn col-sm">
                <Button
                  fullWidth
                  style={{justifyContent: "flex-start"}}
                  className={categoryClassName + ' ' + (checked ? "checked" : "unchecked")}
                  variant={checked ? "outlined" : "contained"}
                  onClick={(event) => {
                    updateLocalAndParentState(
                      event,
                      props.searchCategories,
                      categoryId
                    );
                  }}
                  // value={checked}
                  startIcon={
                    checked ? <CheckBoxIcon/> : <CheckBoxOutlineBlankIcon/>
                  }
                >
                  {HTMLReactParser(categoryName)}
                </Button>
              </div>
            );
          }
          else {
            return <></>;
          }
        })}
      </div>
    );
  }
}
