import { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";

import { useHistory } from "react-router-dom"

export default function FreeTextFilter(props) {
  const history = useHistory();
  return (
    <div className="text-search">
      <input
        value={props.searchFreeText}
        label="Search"
        className={"form-text"}
        placeholder="Search"
        onChange={(event) => {
          const params = (new URL(document.location)).searchParams;
          params.set("t", event.target.value);
          history.push({ search: params.toString() });

          props.setSearchFreeText(event.target.value);
        }}
      />
    </div>
  );
}
