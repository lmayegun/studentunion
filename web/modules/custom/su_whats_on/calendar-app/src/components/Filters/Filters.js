import 'react-big-calendar/lib/css/react-big-calendar.css';
import FreeTextFilter from './FreeTextFilter';
import CategoryFilter from './CategoryFilter';
import Button from '@mui/material/Button';
import { useHistory } from "react-router-dom"
import { Fragment } from 'react';

export default function Filter(props) {
    const history = useHistory();
    const filteringByTags = props.searchTags && props.searchTags.length > 0;
    return (<div class="filters">
        <div class="free-text-and-reset">
            <FreeTextFilter
                {...props}
            />

            {filteringByTags ? (<p>Currently filtering by tag. Reset filters to see all events.</p>) :
                <></>}

            {/* <AutocompleteFilter
            autocompleteOptions={options}
            setSearchFreeText={setSearchFreeText}
            setSearchCategories={setSearchCategories}
            group={options}
            /> */}

            <Button
                className="reset-btn"
                variant="contained"
                onClick={(event) => props.resetFilters()}
            >Reset search</Button >
        </div>

        {!filteringByTags &&
            props.categories && props.categories.length > 0 ? (
            <CategoryFilter
                {...props}
            />
        ) : (
            <></>
        )
        }
    </div >
    );
}
