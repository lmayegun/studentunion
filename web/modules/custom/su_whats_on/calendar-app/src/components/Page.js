import {useEffect, useState} from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import axios from 'axios';
import EventCalendar from './EventCalendar/EventCalendar';
import Filters from './Filters/Filters';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import {getAllOfKind} from './../functions';
import {getFilteredEvents} from './../search';
import {useCurrentWidth} from 'react-breakpoints-hook'; //#endregion
import Loader from './Loader/Loader';
import moment from "moment";

export default function Page(props) {
  const history = useHistory();

  let {
    queryCategories,
    queryView,
    queryText,
    queryTags,
    queryStart,
    queryEnd
  } = props;

  const {path} = useRouteMatch();
  const {search} = useLocation();
  // const { text, category } = queryString.parse(search);

  const width = useCurrentWidth();

  const [error, setError] = useState([]);
  const [display, setDisplay] = useState(width);
  const [loadedEvents, setLoadedEvents] = useState([]);
  const [filteredEvents, setFilteredEvents] = useState([]);

  // Array of dates, we use the start and end to load AJAX:
  const [dateRange, setDateRange] = useState(
    [queryStart, queryEnd]
  );

  const [searchFreeText, setSearchFreeText] = useState(queryText);
  const [searchCategories, setSearchCategories] = useState(queryCategories ? queryCategories.split(';') : []);
  const [searchTags, setSearchTags] = useState(queryTags ? queryTags.split(';') : []);

  var defaultView = queryView ? queryView : false;
  if (!defaultView) {
    defaultView = searchTags.length > 0 ? 'list' : false;
  }
  if (!defaultView) {
    defaultView = (display > 1200 ? 'week' : 'list');
  }
  const [currentView, setCurrentView] = useState(defaultView);

  // Loaded category options
  const [categories, setCategories] = useState([]);

  const [results, setResults] = useState([]);
  const [total, setTotal] = useState([]);
  const [loading, setLoading] = useState(false);
  const [smallLoading, setSmallLoading] = useState(false);

  const [loadedDateRanges, setLoadedDateRanges] = useState([]);

  useEffect(() => {
    // Start a new array for all the results
    var newResults = [...loadedEvents];
    // console.log('existing results', newResults.length, newResults);
    newResults.push(...results);
    // console.log('new results after merge', newResults.length, newResults);

    //each time we create a new set (data structure with unique objects) from
    // results after update let resultsUnique = [...new
    // Set(newResults.map((result) => JSON.stringify(result))),] .map((string)
    // => JSON.parse(string));

    let resultsUnique = newResults.filter((value, index, self) => {
      return self.findIndex(v => {
        if (!v || !value) {
          return null;
        }
        return v.variation_id === value.variation_id;
      }) === index;
    });
    // console.log('unique results', resultsUnique.length, resultsUnique);

    // setResults(resultsUnique);

    setLoadedEvents(resultsUnique);

    if (resultsUnique.length) {
      // setLoading(false);
    }
  }, [results]);

  useEffect(async () => {
    setLoading(true);

    //set initial date range on load
    if (searchTags.length > 0) {
      var period = currentView == 'week' ? 'week' : 'month';
      var start = (moment().format('YYYY-MM-DD'));
      var end = moment().add(1, 'month').format('YYYY-MM-DD');
    }
    else if (queryStart && queryEnd) {
      var start = moment(queryStart).format('YYYY-MM-DD');
      var end = moment(queryEnd).format('YYYY-MM-DD');
    }
    else if (currentView == 'list') {
      var start = (moment().startOf('week').format('YYYY-MM-DD'));
      var end = moment().endOf('week').add(7, 'days').format('YYYY-MM-DD');
    }
    else {
      var period = currentView == 'week' ? 'week' : 'month';
      var start = (moment().startOf(period).subtract(1, 'days').format('YYYY-MM-DD'));
      var end = moment().endOf(period).add(1, 'days').format('YYYY-MM-DD');
    }
    setDateRange([start, end]);
  }, []);

  var loadJSON = async (start, end, page) => {
    if (loadedDateRanges.indexOf(start + end) > 0) {
      return;
    }

    setLoading(true);

    await axios(`whats-on/json/${start}/${end}/list/${page}`)
      .then((response) => {
        setError(false);

        // console.log('response.data', response.data);
        setResults(response.data);

        if (response.data.length > 0) {
          setTimeout(() => {
            loadJSON(start, end, page + 1);
          }, 50);
        }
        else {
          setLoadedDateRanges([...loadedDateRanges, start + end]);
          setLoading(false);
        }
      })
      // error handling
      .catch((error) => {
        setError(true);
        // console.log(error.request);
        // console.log(error.config);
        // setLoadedEvents([]);
      });
  };

  // On date range change:
  useEffect(async () => {
    if (!dateRange || !dateRange[0]) {
      return;
    }
    const dateRangeStart = moment(Object.values(dateRange)[0]);
    const dateRangeEnd = moment(Object.values(dateRange)[Object.values(dateRange).length - 1]).add(1, 'days');

    const dateRangeStartFormat = dateRangeStart.format('YYYY-MM-DD');
    const dateRangeEndFormat = dateRangeEnd.format('YYYY-MM-DD');

    loadJSON(dateRangeStart.unix(), dateRangeEnd.unix(), 0);

    const params = (new URL(document.location)).searchParams;
    params.set("s", dateRangeStartFormat);
    params.set("e", dateRangeEndFormat);
    history.push({search: params.toString()});
  }, [dateRange]);

  //update setDisplay - display value determines what calendar view will be
  // loaded on current screen size
  useEffect(() => {
    setDisplay(width);
  }, [width]);

  useEffect(() => {
    if (!categories) {
      setSearchCategories([]);
    }
  }, [categories]);

  useEffect(() => {
    if (loadedEvents.length) {
      setCategories(getAllOfKind(loadedEvents, 'field_event_category', true, true));
    }
  }, [loadedEvents]);

  useEffect(() => {
    let filteredEvents = getFilteredEvents(
      loadedEvents,
      searchFreeText,
      searchCategories,
      searchTags
    );
    setFilteredEvents(filteredEvents);
  }, [loadedEvents, searchFreeText, searchCategories]);

  const resetFilters = () => {
    setSearchFreeText('');
    setSearchCategories([]);
    setSearchTags([]);
    setFilteredEvents(loadedEvents);

    const params = new URLSearchParams;
    history.push({search: params.toString()});
  };

  return (<div className="whats-on-container">
    <Filters
      searchFreeText={searchFreeText}
      setSearchFreeText={setSearchFreeText}
      searchCategories={searchCategories}
      setSearchCategories={setSearchCategories}
      searchTags={searchTags}
      setSearchTags={setSearchTags}
      valuesInUse={categories}
      categories={categories}
      resetFilters={resetFilters}
    />
    {loading ? <Loader
      error={error}
      message={'Apologies, there has been an issue loading our events.'}
    /> : <></>}
    <EventCalendar
      eventsData={filteredEvents}
      display={display}
      dateRange={dateRange}
      setDateRange={setDateRange}
      currentView={currentView}
      setCurrentView={setCurrentView}
    />
  </div>);
}
