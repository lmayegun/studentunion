import Fuse from "fuse.js";
import _ from "lodash";

const minTextSearch = 1;

/**
 * Set eventsData to filtered events
 */
const getFilteredEvents = (loadedEvents, searchFreeText, searchCategories, searchTags) => {
  let filteredEvents = loadedEvents;

  // Limit by categories
  if (searchCategories.length > 0) {
    filteredEvents = filterByCategories(filteredEvents, searchCategories);
  }

  // Limit by tags
  if (searchTags.length > 0) {
    filteredEvents = filterByTags(filteredEvents, searchTags);
  }

  // Limit by freetext
  if (searchFreeText && searchFreeText.length >= minTextSearch) {
    filteredEvents = filterByFreeText(filteredEvents, searchFreeText);
  }

  return filteredEvents;
};

const filterByCategories = (events, searchCategories) => {
  let filteredEvents = [];

  for (let i = 0; i < events.length; i++) {
    let event = events[i];
    if (searchCategories.includes(event.field_event_category_id)) {
      filteredEvents.push(event);
    }
  }

  return filteredEvents;
}

const filterByTags = (events, searchTags) => {
  let filteredEvents = [];

  for (let i = 0; i < events.length; i++) {
    let event = events[i];
    if (_.intersection(searchTags, event.field_event_tags).length > 0) {
      filteredEvents.push(event);
    }
  }

  return filteredEvents;
}

const filterByFreeText = (events, searchFreeText) => {
  // console.log('Searching by ', searchFreeText);
  const options = {
    isCaseSensitive: false,
    includeScore: true,
    keys: [
      {name: "title", weight: 1},
      {name: "field_event_owner_group", weight: 3},
      // { name: "field_event_tags", weight: 6 },
      // { name: "field_event_venue", weight: 8 },
      {name: "body", weight: 15},
      // { name: "field_event_category", weight: 1 },
    ]
  };

  const fuse = new Fuse(events, options);
  const fuseResults = fuse.search(searchFreeText);
  // console.log('fuseResults', fuseResults);

  var filteredByScore = fuseResults.filter((a) => {
    return a.score < 0.001;
  });
  if (filteredByScore.length == 0) {
    filteredByScore = fuseResults.filter((a) => {
      return a.score < 0.05;
    });
  }
  if (filteredByScore.length == 0) {
    filteredByScore = fuseResults.filter((a) => {
      return a.score < 0.1;
    });
  }

  // console.log('filteredByScore', filteredByScore);

  let filteredEvents = filteredByScore.map((a) => {
    a.item.refIndex = a.refIndex;
    return a.item;
  });
  // console.log('events', filteredEvents);
  return filteredEvents;

}

export {getFilteredEvents};
