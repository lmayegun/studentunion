import Page from './components/Page';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { createTheme, ThemeProvider, StyledEngineProvider, adaptV4Theme } from '@mui/material/styles';
import {
  BrowserRouter as Router,
  Link,
  useLocation,
  useHistory
} from "react-router-dom";

const theme = createTheme(adaptV4Theme({
  palette: {
    primary: {
      main: '#f26640',
    },
    secondary: {
      main: '#082244',
    },
  },
}));

// A custom hook that builds on useLocation to parse
// the query string for you.
function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export default function App() {
  const history = useHistory();

  //query string for pre-filtered urls
  //filter format ?field_date_range_value=2021-07-22

  let query = useQuery();

  return (
    <div>
      <Router history={history}>
        {/* <Route path="/"> */}
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={theme}>
            <Page
              queryCategories={query.get("c")}
              queryView={query.get("v")}
              queryText={query.get("t")}
              queryTags={query.get("ta")}
              queryStart={query.get("s")}
              queryEnd={query.get("e")}
            ></Page>
          </ThemeProvider>
        </StyledEngineProvider>
      </Router>
    </div>
  );
}
