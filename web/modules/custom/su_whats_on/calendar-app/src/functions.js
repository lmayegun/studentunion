import _ from "lodash";

function getAllOfKind(data, field, unique, withId) {
  let all = [];
  let idsUsed = [];
  if (data.length > 0) {
    for (let i = 0; i < data.length; i++) {
      if (data[i][field] != [] && data[i][field] != '') {

        let value = _.unescape(data[i][field]);
        var final = null;
        var id = data[i][field + '_id'];
        if (withId) {
          final = { id: id, name: value };
        } else {
          final = value;
        }

        if (unique === true) {
          // console.log('final', final);
          // console.log('all', all, !_.includes(all, final));
          if (!idsUsed.includes(id)) {
            if (withId) {
              all.push(final);
            } else {
              all.push(value);
            }
            idsUsed.push(id);
          }
          // console.log(field, 'unique');
        } else {
          if (withId) {
            all.push(final);
          } else {
            all.push(final);
          }
        }
      }
    }
    return all;
  }
}

function delay(t) {
  return new Promise(resolve => setTimeout(resolve, t));
}


export {
  getAllOfKind, delay
};
