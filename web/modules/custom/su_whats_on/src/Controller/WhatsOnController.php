<?php

namespace Drupal\su_whats_on\Controller;

use Drupal;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Mail\MailFormatHelper;
use Symfony\Component\HttpFoundation\JsonResponse;


class WhatsOnController extends ControllerBase {

  public function render() {
    $build = [];
    $build['#markup'] = '<div id="root"></div>';
    $build['#attached']['library'][] = 'su_whats_on/calendar-app';
    return $build;
  }

  public function eventsJSON(int $startDate = NULL, int $endDate = NULL, $dataSet = 'list', $page = 0) {
    $cid = __METHOD__ . implode(':', [$startDate, $endDate, $dataSet, $page]);

    if ($cache = Drupal::cache()->get($cid)) {
      return new JsonResponse($cache->data);
    }

    $query = Drupal::entityQuery('commerce_product_variation')
      ->condition('type', ['event', 'meeting'], 'IN');

    $and = $query->andConditionGroup()
      ->condition('status', 1)
      ->condition('field_date_range.value', $startDate, '>=')
      ->condition('field_date_range.end_value', $endDate, '<=');
    $query->condition($and);

    $query->range($page * 50, (($page + 1) * 50) - 1);

    $eventVariationIds = $query->execute();

    $convertedEvents = [];
    foreach ($eventVariationIds as $eventVariationId) {
      $variation = ProductVariation::load($eventVariationId);
      if ($variation) {
        $convertedEvents[] = $this->eventsJSONConvertEvent($variation, $dataSet);
      }
    }

    Drupal::cache()
      ->set($cid, $convertedEvents, strtotime('+3 minutes'), ['']);
    return new JsonResponse($convertedEvents);
  }

  public function eventsJSONConvertEvent($eventVariation, $dataSet = 'list') {
    $cid = __METHOD__ . $eventVariation->id();

    if ($cache = Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $product = $eventVariation->getProduct();
    if (!$product->isPublished()) {
      return NULL;
    }

    $newEvent = [];
    $newEvent['title'] = $product->label();
    $newEvent['variation_id'] = $eventVariation->id();
    $newEvent['product_id'] = $product->id();
    $newEvent['view_commerce_product_variation'] = $eventVariation->toUrl('canonical')
      ->setAbsolute(TRUE)
      ->toString();

    // Dates
    $newEvent['field_date_range_value'] = $eventVariation->field_date_range->value;
    $newEvent['field_date_range_end_value'] = $eventVariation->field_date_range->end_value;

    $group = $product->field_event_owner_group->entity;
    if ($group && $group->bundle() == 'volunteering_opp') {
      $group = su_volunteering_get_org_for_opp($group);
    }
    $newEvent['field_event_owner_group'] = $group ? $group->label() : '';
    $newEvent['field_event_owner_group_id'] = $group ? $group->id() : '';

    $newEvent['field_event_online'] = $product->field_event_online->value ? 'On' : 'Off';

    if ($dataSet == 'list') {
      $cardValues = su_views_display_convert_entity_to_card_values($product);

      $newEvent['field_duotone'] = isset($cardValues['duotone']) && $cardValues['duotone'] != '' ? $cardValues['duotone'] : su_views_display_get_random_duotone();
      $newEvent['field_event_image_banner'] = $cardValues['imageUrl'];
      $newEvent['field_event_image_card'] = $cardValues['imageUrl'];
      $newEvent['field_logo'] = $cardValues['logo'];

      $body = MailFormatHelper::htmlToText($product->body->value);
      $newEvent['body'] = strlen($body) > 397 ? mb_strcut($body, 0, 400) . '...' : $body;

      $category = $product->get('field_event_category')->referencedEntities();
      if (count($category) > 0) {
        $newEvent['field_event_category'] = $category[0]->getName();
        $newEvent['field_event_category_id'] = $category[0]->id();
      }
      else {
        $newEvent['field_event_category'] = '';
        $newEvent['field_event_category_id'] = '';
      }

      $newEvent['field_event_tags'] = [];
      $tags = $product->get('field_event_tags')->referencedEntities();
      if (count($tags) > 0) {
        foreach ($tags as $tag) {
          $newEvent['field_event_tags'][] = $tag->id();
        }
      }
    }

    $newEvent['field_event_venue'] = $product->field_event_venue->entity ? $product->field_event_venue->entity->id() : '';

    Drupal::cache()
      ->set($cid, $newEvent, Cache::PERMANENT, Cache::mergeTags($product->getCacheTags(), $eventVariation->getCacheTags()));

    return $newEvent;
  }

}
