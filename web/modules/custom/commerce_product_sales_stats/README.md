This module adds a "Sales stats" tab on each commerce product, showing:

- Total sales for product and each variation
- Cumulative sales (as a graph if Charts is enabled, but also as a table)
- Daily sales (as a graph if Charts is enabled, but also as a table)
