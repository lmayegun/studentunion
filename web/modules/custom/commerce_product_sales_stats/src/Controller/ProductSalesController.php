<?php

namespace Drupal\commerce_product_sales_stats\Controller;

use Drupal;
use Drupal\charts\Plugin\chart\Library\ChartBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Controller\ControllerBase;

class ProductSalesController extends ControllerBase {

  var $commerceProduct = NULL;

  const DAYS_TO_GROUP_BY_WEEK = 27;

  const DAYS_TO_GROUP_BY_MONTH = 300;

  public function getDefaultCurrency() {
    $stores = $this->commerceProduct->getStores();
    $store = reset($stores);
    if (!$store) {
      $store = Drupal::service('commerce_store.current_store')
        ->getStore()
        ->id();
    }
    return $store->get('default_currency')->getValue()[0]['target_id'];
  }

  public function renderForProduct(ProductInterface $commerce_product = NULL) {
    if (!$commerce_product) {
      return [];
    }

    $this->commerceProduct = $commerce_product;

    $cumulative_stats = $this->generateFromDatabase($commerce_product, TRUE);
    $noncumulative_stats = $this->generateFromDatabase($commerce_product, FALSE);

    $build = [];

    $build[] = $this->renderSummaryTable($commerce_product, $cumulative_stats);

    if (Drupal::moduleHandler()->moduleExists('charts')) {
      $build[] = $this->buildChart($commerce_product, $cumulative_stats, TRUE, t('Cumulative sales (by quantity sold)'));
      $build[] = $this->buildChart($commerce_product, $noncumulative_stats, FALSE, t('Daily sales (by quantity sold)'));
    }

    $build[] = $this->renderTableStats($commerce_product, $cumulative_stats, t('Cumulative sales table (by quantity sold)'));
    $build[] = $this->renderTableStats($commerce_product, $noncumulative_stats, t('Daily sales table (by quantity sold)'));

    if (count($commerce_product->getVariationIds()) > 1) {
      // @todo quantity sold split by variations:
    }

    return $build;
  }

  public function renderSummaryTable($commerce_product, $cumulative_stats) {
    $currency_formatter = Drupal::service('commerce_price.currency_formatter');

    $total = end($cumulative_stats['overall']);
    $product_row = [
      t('Product total (@label)', ['@label' => $commerce_product->label()]),
      $total['orders'],
      $total['quantity'],
      $currency_formatter->format($total['revenue'], $total['currency']),
    ];

    $variations = $commerce_product->getVariations();
    $variation_rows = [];
    if (count($variations) > 1) {
      foreach ($variations as $variation) {
        $total = end($cumulative_stats[$variation->id()]);
        $price = new Price($total['revenue'], $total['currency']);
        $variation_rows[] = [
          t('@label', ['@label' => $this->getVariationLabel($variation)]),
          $total['orders'],
          $total['quantity'],
          $currency_formatter->format($total['revenue'], $total['currency']),
        ];
      }
    }

    $table = [
      '#type' => 'table',
      '#header' => [
        t('Product / variation'),
        t('Orders'),
        t('Sold quantity'),
        t('Revenue'),
      ],
    ];

    if (count($variation_rows) > 0) {
      $table['#rows'] = $variation_rows;
      $table['#footer'] = [$product_row];
    }
    else {
      $table['#rows'] = [$product_row];
    }
    return $table;
  }

  public function getVariationLabel(ProductVariationInterface $variation) {
    if ($variation->hasField('field_event_variation_title') && $variation->field_event_variation_title->value) {
      $title = $variation->field_event_variation_title->value;
    }
    else {
      $title = $variation->label();
    }

    if ($variation->hasField('field_date_range') && $variation->field_date_range->value) {
      $formatter = Drupal::service('date.formatter');
      $start = $variation->field_date_range->value;
      $end = $variation->field_date_range->end_value;
      $title .= ' [' . $formatter->format($start, 'short') . ' - ' . $formatter->format($end, 'short') . ']';
    }
    return $title;
  }

  public function renderTableStats($commerce_product, $stats, $title) {
    $table_stats = [];
    $keys = [
      t('Date'),
      $commerce_product->label(),
    ];

    $variations = $commerce_product->getVariations();

    if (count($variations) > 1) {
      foreach ($variations as $variation) {
        $keys[] = $this->getVariationLabel($variation);
      }
    }

    $timestamps = array_keys($stats['overall']);
    foreach ($timestamps as $timestamp) {
      $formatted = $this->formatDate($timestamp);

      $row = [
        $formatted,
      ];

      $row[] = t('@value', [
        '@value' => $stats['overall'][$timestamp]['quantity'],
      ]);

      if (count($variations) > 1) {
        foreach ($variations as $variation) {
          $value = isset($stats[$variation->id()][$timestamp]) ? $stats[$variation->id()][$timestamp]['quantity'] : 0;
          $row[] = t('@value', [
            '@value' => $value,
          ]);
        }
      }
      $table_stats[] = $row;
    }

    return [
      '#type' => 'details',
      '#title' => $title,
      'table' => [
        '#type' => 'table',
        '#header' => $keys,
        '#rows' => $table_stats,
      ],
    ];
  }

  public function formatDate($timestamp) {
    $dateTimeService = Drupal::service('date.formatter');
    $date_formats = Drupal::entityTypeManager()->getStorage('date_format');
    if ($date_formats->load('default_short_date_no_time_')) {
      $formatted = $dateTimeService->format($timestamp, 'default_short_date_no_time_');
    }
    else {
      $formatted = date('Y-m-d', $timestamp);
    }
    return $formatted;
  }

  public function getDatesForVariations($variation_ids, $cumulative = TRUE, $minTimestamp = NULL, $maxTimestamp = NULL, $timeGrouping = 'day') {
    $query = Drupal::database()->select('commerce_order_item', 'i');
    $query->join('commerce_order', 'o', 'i.order_id = o.order_id');

    $query->condition('i.purchased_entity', $variation_ids, 'IN')
      ->condition('i.quantity', 0, ">")
      ->condition('o.state', 'completed');

    $query->addExpression("date_format( FROM_UNIXTIME(o.placed), '" . $this->getMysqlDateFormat($timeGrouping) . "' )", $timeGrouping);
    $query->orderBy($timeGrouping, 'ASC');

    $query->addExpression('count(*)', "orders");
    $query->addExpression('sum(i.quantity)', "quantity");
    $query->addExpression('sum(i.total_price__number)', "revenue");
    $query->addExpression('GROUP_CONCAT(DISTINCT i.total_price__currency_code)', "currency");
    $query->groupBy($timeGrouping);

    $datesInUseResult = $query->execute()->fetchAllAssoc($timeGrouping);
    return $this->fillDates(
      $datesInUseResult,
      $cumulative,
      $minTimestamp,
      $maxTimestamp,
      $timeGrouping,
    );
  }

  /**
   * @param ProductInterface $entity
   *
   * @return array
   */
  public function generateFromDatabase(ProductInterface $entity, $cumulative = TRUE, $timeGrouping = 'day') {
    $result = [];

    $variations = $entity->getVariationIds();

    $result['overall'] = $this->getDatesForVariations($variations, $cumulative);

    // @todo make other timegroups work (something to do with the database query probably)
    // if (count($result['overall']) > $this->DAYS_TO_GROUP_BY_MONTH) {
    //   $timeGrouping = 'month';
    // } else if (count($result['overall']) > $this->DAYS_TO_GROUP_BY_WEEK) {
    //   $timeGrouping = 'week';
    // }

    if ($timeGrouping != 'day') {
      $result['overall'] = $this->getDatesForVariations($variations, $cumulative, NULL, NULL, $timeGrouping);
    }

    if (count($variations) > 1) {
      $timestamps = array_keys($result['overall']);

      $minTimestamp = reset($timestamps);
      $maxTimestamp = $timestamps[count($timestamps) - 1];

      foreach ($variations as $variation_id) {
        $result[$variation_id] = $this->getDatesForVariations([$variation_id], $cumulative, $minTimestamp, $maxTimestamp, $timeGrouping);
      }
    }

    return $result;
  }

  /**
   * Add in any empty hours to the dataset so it is complete.
   *
   * @param mixed $datesInUseResult
   *
   * @return array
   */
  public function fillDates($datesInUseResult, $cumulative = TRUE, $minTimestamp = NULL, $maxTimestamp = NULL, $timeGrouping = 'day') {
    $datesInUse = array_column($datesInUseResult, $timeGrouping);
    $timestampsInUse = $datesInUse;
    array_walk($timestampsInUse, function (&$value, $key) use ($timeGrouping) {
      $value = $this->convertDatabaseToTimestamp($timeGrouping, $value);
    });

    $result = [];

    if ($maxTimestamp) {
      $last = $maxTimestamp;
    }
    else {
      $last = $timestampsInUse[count($timestampsInUse) - 1];
    }

    if ($minTimestamp) {
      $first = $minTimestamp;
    }
    else {
      $first = reset($timestampsInUse);
    }

    $values = [
      'orders' => 0,
      'quantity' => 0,
      'revenue' => 0,
      'currency' => '',
    ];

    $current = $first;
    $stepVal = '+1 ' . $timeGrouping;
    while ($current <= $last) {
      $result[$current] = [
        'orders' => $cumulative ? $values['orders'] : 0,
        'quantity' => $cumulative ? $values['quantity'] : 0,
        'revenue' => $cumulative ? $values['revenue'] : 0,
        'currency' => $values['currency'] ?: $this->getDefaultCurrency(),
      ];

      $date = $this->formatTimestamp($timeGrouping, $current);

      if (isset($datesInUseResult[$date])) {
        $data = (array) $datesInUseResult[$date];
        $result[$current]['orders'] += $data['orders'];
        $result[$current]['quantity'] += $data['quantity'];
        $result[$current]['revenue'] += $data['revenue'];

        // @todo currently we're assuming one currency type in use...
        $result[$current]['currency'] = $data['currency'];

        $values = $result[$current];
      }

      $current = strtotime($stepVal, $current);
    }

    return $result;
  }

  /**
   *
   */
  public function buildChart(ProductInterface $entity, $stats, $cumulative = TRUE, $title = 'Sales') {
    $xaxis = [
      '#type' => 'chart_xaxis',
      '#title' => t('Time'),
    ];

    $yaxis = [
      '#type' => 'chart_yaxis',
      '#title' => t('Quantity sold'),
    ];

    $chart = [
      '#type' => 'chart',
      '#chart_type' => $cumulative ? 'spline' : 'column',
      '#title' => $title,
      '#spline' => TRUE,
      '#polar' => FALSE,
      '#tooltips' => [],

      'x_axis' => $xaxis,
      'y_axis' => $yaxis,

      '#data_labels' => FALSE,

      '#raw_options' => [
        'point' => [
          'show' => FALSE,
        ],
        'line' => [
          'point' => FALSE,
          'zerobased' => TRUE,
          'classes' => 'chart-thick-line',
        ],
        'axis' => [
          'x' => [
            'tick' => [
              'show' => FALSE,
              'count' => 15,
              'culling' => [
                'lines' => FALSE,
                'max' => 15,
              ],
            ],
          ],
        ],
      ],

      '#title_font_size' => 14,
      '#title_font_weight' => 'bold',

      '#legend' => TRUE,
      '#legend_position' => 'bottom',

      '#exporting_library' => FALSE,

      '#tooltips' => TRUE,
      '#tooltips_use_html' => TRUE,
    ];

    $colours = ChartBase::getDefaultColors();
    $i = 0;
    foreach ($stats as $key => $data) {
      if (!isset($chart['x_axis']['#labels'])) {
        $dates = array_keys($data);
        array_walk($dates, function (&$value, $key) {
          $value = $this->formatDate($value);
        });
        $chart['x_axis']['#labels'] = $dates;
      }

      if ($key == 'overall') {
        $label = t('All variations');
      }
      else {
        $variation = ProductVariation::load($key);
        $label = $this->getVariationLabel($variation);
      }

      $data = array_diff(array_combine(array_keys($data), array_column($data, 'quantity')), [NULL]);

      $chart[$key] = [
        '#type' => 'chart_data',
        '#title' => $label,
        '#data' => $data,
        '#color' => $colours[$i],
        '#line_width' => 3,
      ];
      $i++;
    }

    return $chart;
  }

  /**
   * @param string $timeGrouping
   *
   * @return string
   *   String for MySQL query.
   */
  public function getMysqlDateFormat(string $timeGrouping) {
    switch ($timeGrouping) {
      case 'hour':
        return '%Y-%m-%d H';
      case 'week':
        return '%Y-%m-%d';
      case 'month':
        return '%Y-%m';
      case 'day':
      default:
        return '%Y-%m-%d';
    }
  }

  /**
   * @param string $timeGrouping
   * @param int $timestamp
   *
   * @return string
   *   Formatted date string.
   */
  public function formatTimestamp(string $timeGrouping, int $timestamp) {
    switch ($timeGrouping) {
      case 'hour':
        return date('Y-m-d H', $timestamp);
      case 'week':
        return date('Y-m-d', $timestamp) . ' - ' . date('Y-m-d', strtotime('+6 days', $timestamp));
      case 'month':
        return date('Y-m', $timestamp);
      case 'day':
      default:
        return date('Y-m-d', $timestamp);
    }
  }

  /**
   * @param string $timeGrouping
   * @param string $value
   *
   * @return int
   *   Timestamp for start of period
   */
  public function convertDatabaseToTimestamp(string $timeGrouping, string $value) {
    switch ($timeGrouping) {
      case 'hour':
        return strtotime($value . ':00');
      case 'week':
        $parts = explode(' - ', $value);
        $start = $parts[0];
        return strtotime($start . ' 12:00');
      case 'month':
        return strtotime($value . '-01 12:00');
      case 'day':
      default:
        return strtotime($value . ' 12:00');
    }
  }

}
