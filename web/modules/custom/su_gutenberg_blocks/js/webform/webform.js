const { blocks, data, element, components, editor, blockEditor } = wp;
const { Fragment } = element;
const _ = lodash;
const {
    PanelBody,
    BaseControl,
    Icon,
    RangeControl,
    IconButton,
    Toolbar,
    SelectControl,
    TextControl,
    Button
} = components;
const {
    InnerBlocks,
    RichText,
    RichTextToolbarButton,
    PanelColorSettings,
    MediaUpload,
    BlockControls,
} = editor;
const {
    InspectorControls
} = blockEditor;
const __ = Drupal.t;

// CARDS BY TAGS
export default {
    title: __('Webform'),
    description: __('Embed a webform in another page'),
    icon: 'feedback',
    attributes: {
        webform_id: {
            type: 'string',
            default: '',
        },
    },

    edit({ className, attributes, setAttributes, isSelected }) {
        const { webform_id } = attributes;

        return (
            <Fragment>
                <div class="cards-by-tag--editor-box">Webform: <b>{webform_id}</b></div>
                <InspectorControls>
                    <PanelBody title={__('Settings')}>
                        <TextControl
                            label="Webform ID"
                            value={webform_id}
                            onChange={(webform_id) => {
                                setAttributes({ webform_id });
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </Fragment >
        );
    },

    save({ }) {
        // This is done dynamically 
        return (
            <div>
            </div >
        );
    },
};