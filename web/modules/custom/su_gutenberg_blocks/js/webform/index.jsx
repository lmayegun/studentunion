/**
 * Internal dependencies
 */
 import webform from './webform';

 export const webformSettings = {
     ...webform
 };