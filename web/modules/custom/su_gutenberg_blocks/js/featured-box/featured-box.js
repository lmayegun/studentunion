import { duotoneOptions, brandColourOptions } from '../shared';

const { element, components, blockEditor } = wp;
const { Fragment } = element;
const { PanelBody, SelectControl } = components;
const { RichText, InspectorControls, MediaUpload, InnerBlocks } = blockEditor;
const __ = Drupal.t;

export default {
  title: __('Featured box'),
  description: __('A page width feature tile.'),
  icon: 'align-center',
  attributes: {
    title: {
      type: 'string',
      default: '',
    },
    text: {
      type: 'string',
      default: '',
    },
    mediaID: {
      type: 'number',
    },
    colourScheme: {
      type: 'string',
      default: 'coral',
    },
    mediaURL: {
      type: 'string',
      default: '',
      // source: 'attribute',
      // selector: 'img',
      // attribute: 'src',
    },
    duotone: {
      type: 'string',
      default: '',
    },
  },
  edit({ attributes, setAttributes }) {
    const {
      text,
      mediaID,
      colourScheme,
      duotone,
      mediaURL,
      title,
    } = attributes;
    return (
      <div
        class={'featured_box front_page_featured_box featured_box-' + colourScheme}
      >
        <div class="slider-elements">
          <div class="row">
            <div class="col-sm featured_box_col_content">
              <div class="top">
                <h2>
                  <RichText
                    identifier="title"
                    tagName="div"
                    className="title"
                    value={title}
                    placeholder={__('Edit title')}
                    onChange={title => {
                      setAttributes({
                        title: title,
                      });
                    }}
                  />
                </h2>
                <InnerBlocks
                  allowedBlocks={[
                    `core/paragraph`,
                    `core/image`,
                    `core/list`,
                    `core/button`,
                  ]}
                />
              </div>
              <div class="bottom">
                {/* {% if term.field_links|length > 0 %}
                            <div class="slider_buttons">
                                {% for button in term.field_links %}
                                <a href="{{button.uri}}" class="button">{{ button.title }}</a>
                                {% endfor %}
                            </div>
                            {% endif %} */}
              </div>
            </div>
            <div
              class={'col-sm order-first order-md-0 featured_box_col_image ' + duotone}
              style={{ backgroundImage: 'url(' + mediaURL + ')' }}
            ></div>
          </div>
        </div>
        <InspectorControls>
          <PanelBody title={__('Settings')}>
            {/* <TextControl
                        label="URL"
                        value={url}
                        onChange={(url) => {
                            setAttributes({ url });
                        }}
                    /> */}
            <MediaUpload
              label="Image"
              onSelect={media => {
                setAttributes({
                  mediaURL: media.url,
                  mediaID: media.id,
                });
              }}
              type="image"
              value={''}
              render={({ open }) => (
                <Fragment>
                  {mediaURL ? (
                    <img src={mediaURL} width="100px" height="100px" />
                  ) : (
                    <div></div>
                  )}
                  <button onClick={open}>Upload image</button>
                </Fragment>
              )}
            />
            <SelectControl
              label="Colour scheme for box"
              value={colourScheme}
              options={brandColourOptions}
              onChange={colourScheme => {
                setAttributes({ colourScheme });
              }}
            />
            <SelectControl
              label="Duotone for image"
              value={duotone}
              options={duotoneOptions}
              onChange={duotone => {
                setAttributes({ duotone });
              }}
            />
          </PanelBody>
        </InspectorControls>
      </div>
    );
  },

  save({ attributes }) {
    const { title, text, colourScheme, duotone, mediaURL, url } = attributes;
    return (
      <div
        class={
          'featured_box front_page_featured_box featured_box-' + colourScheme
        }
      >
        <div class="slider-elements">
          <div class="row">
            <div class="col-sm featured_box_col_text">
              <div class="top">
                <h2>{title}</h2>
                <InnerBlocks.Content />
              </div>
              <div class="bottom"></div>
            </div>
            <div
              class={'col-sm order-first order-md-0 featured_box_col_image ' + duotone}
              style={'background-image: url(' + mediaURL + ')'}
            ></div>
          </div>
        </div>
      </div>
    );
  },
};
