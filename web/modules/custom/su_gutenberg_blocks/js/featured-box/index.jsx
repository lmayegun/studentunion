/**
 * Internal dependencies
 */
 import featuredBox from './featured-box';

 export const featuredBoxSettings = {
     ...featuredBox
 };