/**
 * Internal dependencies
 */

 import linkTile from './link-tile';
 import linkTiles from './link-tiles';
 
 export const linkTileSettings = {
     ...linkTile
 };
 
 export const linkTilesSettings = {
     ...linkTiles
 };