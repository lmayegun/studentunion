const { element, editor, components, blockEditor } = wp;
const { Fragment } = element;
const {
    PanelBody,
    SelectControl,
    TextControl,
} = components;
const {
    RichText, InspectorControls, MediaUpload
} = blockEditor;

const __ = Drupal.t;

import { duotoneOptions } from '../shared';

export default {
    title: __('Link tile'),
    description: __('An image tile that links somewhere.'),
    icon: 'admin-links',
    attributes: {
        title: {
            type: 'string',
            default: ''
        },
        subtitle: {
            type: 'string',
            default: ''
        },
        url: {
            type: 'string',
            default: ''
        },
        mediaID: {
            type: 'number',
        },
        mediaURL: {
            type: 'string',
            default: '',
            // source: 'attribute',
            // selector: 'img',
            // attribute: 'src',
        },
        duotone: {
            type: 'string',
            default: ''
        },
    },
    edit({ attributes, setAttributes }) {
        const { url, subtitle, mediaID, duotone, mediaURL, title } = attributes;
        return (<div class={"link-tile duotone-" + duotone} >
            <div class="link-tile-titles">
                <RichText
                    identifier="title"
                    tagName="div"
                    className="title"
                    value={title}
                    placeholder={__('Edit title')}
                    onChange={(title) => {
                        setAttributes({
                            title: title
                        })
                    }}
                    withoutInteractiveFormatting={true}
                />
                <RichText
                    identifier="subtitle"
                    tagName="div"
                    className="subtitle"
                    value={subtitle}
                    placeholder={__('Edit optional subtitle')}
                    onChange={(subtitle) => {
                        setAttributes({
                            subtitle: subtitle
                        })
                    }}
                    withoutInteractiveFormatting={true}
                /></div>
            <div class={"duotone-image " + duotone} style={{ backgroundImage: "url(" + mediaURL + ");" }}>
            </div>
            <InspectorControls>
                <PanelBody title={__('Settings')}>
                    <TextControl
                        label="URL"
                        value={url} length
                        onChange={(url) => {
                            setAttributes({ url });
                        }}
                    />

                    <MediaUpload
                        label="Image"
                        onSelect={
                            (media) => {
                                setAttributes({
                                    mediaURL: media.url,
                                    mediaID: media.id,
                                });
                            }
                        }
                        type="image"
                        value={""}
                        render={
                            ({ open }) => (
                                <Fragment>
                                    {mediaURL ? <img src={mediaURL} width="100px" height="100px" /> : <div></div>}
                                    <button onClick={open}>
                                        Upload image
                                    </button>
                                </Fragment>
                            )}
                    />

                    <SelectControl
                        label="Duotone"
                        value={duotone}
                        options={duotoneOptions}
                        onChange={(duotone) => {
                            setAttributes({ duotone });
                        }}
                    />
                </PanelBody>
            </InspectorControls>
        </ div>);
    },

    save({ className, attributes }) {
        console.log(className);
        const { title, subtitle, duotone, mediaURL, url } = attributes;
        return (
            <div className={className + " col-sm"}>
                <a href={url} className={"link-tile col-sm duotone-" + duotone}>
                    <div class="link-tile-titles">
                        <div class="title"><span>{title}</span></div>
                        {subtitle && subtitle != '' && subtitle.length > 0 ? <div class="subtitle"><span>{subtitle}</span></div> : <Fragment />}
                    </div>
                    <div class={"duotone-image " + duotone} style={{ backgroundImage: "url(" + mediaURL + ");" }}>
                    </div>
                </a>
            </div>
        );
    },
};