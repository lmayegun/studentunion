const { element, editor, blockEditor } = wp;
const { Fragment } = element;
const {
    InnerBlocks,
} = blockEditor;
const __ = Drupal.t;

export default {
    title: __('Link tiles'),
    icon: 'embed-photo',
    description: __('Add image link tiles.'),
    edit: ({ className, attributes, setAttributes, isSelected }) => {
        return (
            <div className={className + " link-tiles"}>
                <InnerBlocks allowedBlocks={[`su-gutenberg-blocks/link-tile`]} />
            </div>
        );
    },
    save: ({ attributes }) => {
        return (
            <Fragment>
                <div class="container link-tiles">
                    <div class="row gy-3 gx-3">
                        <InnerBlocks.Content />
                    </div>
                </div>
            </Fragment>
        );
    },
};