/**
 * Internal dependencies
 */
 import cardsByTags from './cards-by-tags';

 export const cardsByTagsSettings = {
     ...cardsByTags
 };