const { blocks, data, element, components, editor, blockEditor } = wp;
const { Fragment } = element;
const _ = lodash;
const {
    PanelBody,
    BaseControl,
    Icon,
    RangeControl,
    IconButton,
    Toolbar,
    SelectControl,
    TextControl,
    Button
} = components;
const {
    InnerBlocks,
    RichText,
    RichTextToolbarButton,
    PanelColorSettings,
    MediaUpload,
    BlockControls,
} = editor;
const {
    InspectorControls
} = blockEditor;
const __ = Drupal.t;

// CARDS BY TAGS
export default {
    title: __('Tagged content / events'),
    description: __('Cards - tagged content and/or events'),
    icon: 'screenoptions',
    attributes: {
        title: {
            type: 'string',
            default: '',
        },
        entity_type: {
            type: 'string',
            default: 'event',
        },
        tag: {
            type: 'string',
            default: '',
        },
    },

    edit({ className, attributes, setAttributes, isSelected }) {
        const { title, entity_type, tag } = attributes;

        return (
            <Fragment>
                <div class="cards-by-tag--editor-box">Cards of type <b>{entity_type}</b>, tagged "{tag}", will appear here.</div>
                <InspectorControls>
                    <PanelBody title={__('Settings')}>
                        <TextControl
                            label="Title"
                            value={title}
                            onChange={(title) => {
                                setAttributes({ title });
                            }}
                        />
                        <SelectControl
                            label="Type"
                            value={entity_type}
                            options={[
                                { label: "Events (future)", value: "event" },
                                { label: "News, pages and other content (no events)", value: "content" },
                                { label: "News only", value: "news" },
                                // { label: "Officer", value: "officer" }
                            ]}
                            onChange={(entity_type) => {
                                setAttributes({ entity_type });
                            }}
                        />
                        <TextControl
                            label="Tag"
                            value={tag}
                            onChange={(tag) => {
                                setAttributes({ tag });
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </Fragment >
        );
    },

    save({ className, attributes }) {
        const { title, entity_type, tag } = attributes;

        return (
            <div className={className}>
                <h2>{title}</h2>
                {entity_type}: {tag}
            </div >
        );
    },
};