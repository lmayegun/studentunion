/**
 * Internal dependencies
 */
import poll from './poll';

export const pollSettings = {
  ...poll,
};
