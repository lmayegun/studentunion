const { blocks, data, element, components, editor, blockEditor } = wp;
const { Fragment } = element;
const _ = lodash;
const {
  PanelBody,
  Button,
} = components;
const {
  InnerBlocks,
  RichText,
  RichTextToolbarButton,
  PanelColorSettings,
  MediaUpload,
  BlockControls,
} = editor;
const { InspectorControls } = blockEditor;
const __ = Drupal.t;

// CARDS BY TAGS
export default {
  title: __('Poll'),
  description: __('Embed a poll in another page'),
  icon: 'forms',
  attributes: {
    webform_id: {
      type: 'string',
      default: '',
    },
  },

  edit({ className, attributes, setAttributes, isSelected }) {
    const { poll_id } = attributes;

    return (
      <Fragment>
        <div class="cards-by-tag--editor-box">
          Poll: <b>{poll_id}</b>
        </div>
        <InspectorControls>
          <PanelBody title={__('Settings')}>
            <TextControl
              label="Poll ID"
              value={poll_id}
              onChange={(poll_id) => {
                setAttributes({ poll_id });
              }}
            />
          </PanelBody>
        </InspectorControls>
      </Fragment>
    );
  },

  save({}) {
    // This is done dynamically
    return <div></div>;
  },
};
