import * as accordion from './accordion';
import * as cardsByTags from './cards-by-tags';
import * as cards from './cards';
import * as linkTiles from './link-tiles';
import * as headingBanner from './heading-banner';
import * as webform from './webform';
import * as poll from './poll';
import * as featuredBox from './featured-box';

const { blocks, data } = wp;

const { registerBlockType } = blocks;
const { dispatch, select } = data;

const category = {
  slug: 'su-gutenberg-blocks',
  title: 'Custom Union blocks',
};

const currentCategories = select('core/blocks')
  .getCategories()
  .filter((item) => item.slug !== category.slug);

dispatch('core/blocks').setCategories([category, ...currentCategories]);

// ACCORDION
// https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/nested-blocks-inner-blocks/

registerBlockType(`${category.slug}/ckeditor-accordion-section`, {
  category: category.slug,
  parent: [`${category.slug}/ckeditor-accordion`],
  ...accordion.accordionSectionSettings,
});

registerBlockType(`${category.slug}/ckeditor-accordion`, {
  category: category.slug,
  ...accordion.accordionSettings,
});

// CARDS BY TAGS

registerBlockType(`${category.slug}/cards-by-tags`, {
  category: category.slug,
  ...cardsByTags.cardsByTagsSettings,
});

// CARDS

registerBlockType(`${category.slug}/cards`, {
  category: category.slug,
  ...cards.cardsSettings,
});

registerBlockType(`${category.slug}/card-custom`, {
  category: category.slug,
  parent: [`${category.slug}/cards`],
  ...cards.cardCustomSettings,
});

registerBlockType(`${category.slug}/card-content`, {
  category: category.slug,
  parent: [`${category.slug}/cards`],
  ...cards.cardContentSettings,
});

// Link tiles

registerBlockType(`${category.slug}/link-tile`, {
  category: category.slug,
  parent: [`${category.slug}/link-tiles`],
  ...linkTiles.linkTileSettings,
});

registerBlockType(`${category.slug}/link-tiles`, {
  category: category.slug,
  ...linkTiles.linkTilesSettings,
});

// Heading banner

registerBlockType(`${category.slug}/heading-banner`, {
  category: category.slug,
  ...headingBanner.headingBannerSettings,
});

// Featured box

registerBlockType(`${category.slug}/featured-box`, {
  category: category.slug,
  ...featuredBox.featuredBoxSettings,
});

// Webform

registerBlockType(`${category.slug}/webform`, {
  category: category.slug,
  ...webform.webformSettings,
});

// Poll

registerBlockType(`${category.slug}/poll`, {
  category: category.slug,
  ...poll.pollSettings,
});
