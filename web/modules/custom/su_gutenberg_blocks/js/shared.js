export var duotoneOptions = [
  { value: '', label: 'None' },
  { value: 'green-purple-filter', label: 'Green & Purple' },
  { value: 'yellow-blue-filter', label: 'Yellow & Blue' },
  { value: 'yellow-purple-filter', label: 'Yellow & Purple' },
  {
    value: 'team-ucl-filter',
    label: 'Team UCL filter (light blue and purple)',
  },
  { value: 'yellow-green-filter', label: 'Yellow & Green' },
  { value: 'orange-blue-filter', label: 'Orange & Blue' },
];

export var brandColourOptions = [
  {
    value: 'teal',
    label: 'Teal (CSC Skirting Board)',
  },
  {
    value: 'coral',
    label: 'Coral (Portico Sunset)',
  },
  {
    value: 'purple',
    label: 'Purple (125 Plum)',
  },
  {
    value: 'yellow',
    label: "Yellow (Bentham's Teeth)",
  },
  {
    value: 'navy',
    label: "Navy (Midnight At Mully's)",
  },
  {
    value: 'teamucl-blue',
    label: 'TeamUCL Blue',
  },
  {
    value: 'teamucl-purple',
    label: 'TeamUCL Purple',
  },
];
