const { blocks, data, element, components, editor, blockEditor } = wp;
const { Fragment } = element;
const _ = lodash;
const {
    PanelBody,
    BaseControl,
    Icon,
    RangeControl,
    IconButton,
    Toolbar,
    SelectControl,
    TextControl,
    Button
} = components;
const {
    InnerBlocks,
    RichText,
    RichTextToolbarButton,
    PanelColorSettings,
    MediaUpload,
    BlockControls,
} = editor;
const {
    InspectorControls
} = blockEditor;
const __ = Drupal.t;

// CARDS BY TAGS
export default {
    title: __('Content card'),
    description: __('Embed content as a card'),
    icon: 'welcome-add-page',
    attributes: {
        entity_id: {
            type: 'string',
            default: '',
        },
        entity_type: {
            type: 'string',
            default: 'content',
        },
    },

    edit({ className, attributes, setAttributes, isSelected }) {
        const { entity_type, entity_id } = attributes;

        return (
            <Fragment>
                <div class="cards-by-tag--editor-box">Card for <b>{entity_type}</b> with ID <b>{entity_id}</b></div>
                <InspectorControls>
                    <PanelBody title={__('Settings')}>
                        <SelectControl
                            label="Type"
                            value={entity_type}
                            options={[
                                { label: "Content", value: "content" },
                                { label: "Event", value: "event" },
                                { label: "Product", value: "product" },
                            ]}
                            onChange={(entity_type) => {
                                setAttributes({ entity_type });
                            }}
                        />
                        <TextControl
                            label="ID of entity"
                            value={entity_id}
                            type="number"
                            onChange={(entity_id) => {
                                setAttributes({ entity_id });
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </Fragment >
        );
    },

    save({ }) {
        // This is done dynamically via twig
        return (
            <div>
            </div >
        );
    },
};