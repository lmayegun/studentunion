const { blocks, data, element, components, editor, blockEditor } = wp;
const { Fragment } = element;
const _ = lodash;
const {
    PanelBody,
    BaseControl,
    Icon,
    RangeControl,
    IconButton,
    Toolbar,
    SelectControl,
    TextControl,
    Button
} = components;
const {
    InnerBlocks,
    RichText,
    RichTextToolbarButton,
    PanelColorSettings,
    MediaUpload,
    BlockControls,
} = editor;
const {
    InspectorControls
} = blockEditor;
const __ = Drupal.t;

// CARDS BY TAGS
export default {
    title: __('Custom card'),
    description: __('Provide custom values for a card (not based on existing content)'),
    icon: 'welcome-write-blog',
    attributes: {
        title: {
            type: 'string',
            default: '',
        },
        url: {
            type: 'string',
            default: '',
        },
        card_subheading_1: {
            type: 'string',
            default: '',
        },
        card_subheading_2: {
            type: 'string',
            default: '',
        },
        card_body_text_preview: {
            type: 'string',
            default: '',
        },
        body_box: {
            type: 'string',
            default: '',
        },
        price: {
            type: 'string',
            default: '',
        },
        card_button_text: {
            type: 'string',
            default: '',
        },
        mediaID: {
            type: 'number',
        },
        mediaURL: {
            type: 'string',
            default: '',
            // source: 'attribute',
            // selector: 'img',
            // attribute: 'src',
        },
        duotone: {
            type: 'string',
            default: '',
        },
        host_icon: {
            type: 'string',
            default: '',
        },
        host_text: {
            type: 'string',
            default: '',
        },
        image_box: {
            type: 'string',
            default: '',
        },
    },

    edit({ className, attributes, setAttributes, isSelected }) {
        const { title, url, card_subheading_1, card_subheading_2 } = attributes;

        return (
            <Fragment>
                <div class="cards-by-tag--editor-box">Custom card <b>{title}</b></div>
                <InspectorControls>
                    <PanelBody title={__('Settings')}>
                        <TextControl
                            label="Title"
                            value={title}
                            onChange={(title) => {
                                setAttributes({ title });
                            }}
                        />
                        <TextControl
                            label="URL"
                            value={url}
                            onChange={(url) => {
                                setAttributes({ url });
                            }}
                        />
                        <TextControl
                            label="Subheading 1"
                            value={card_subheading_1}
                            onChange={(card_subheading_1) => {
                                setAttributes({ card_subheading_1 });
                            }}
                        />
                        <TextControl
                            label="Subheading 2"
                            value={card_subheading_2}
                            onChange={(card_subheading_2) => {
                                setAttributes({ card_subheading_2 });
                            }}
                        /> <MediaUpload
                            label="Image"
                            onSelect={
                                (media) => {
                                    setAttributes({
                                        mediaURL: media.url,
                                        mediaID: media.id,
                                    });
                                }
                            }
                            type="image"
                            value={""}
                            render={
                                ({ open }) => (
                                    <Fragment>
                                        {mediaURL ? <img src={mediaURL} width="100px" height="100px" /> : <div></div>}
                                        <button onClick={open}>
                                            Upload image
                                        </button>
                                    </Fragment>
                                )}
                        />
                    </PanelBody>
                </InspectorControls>
            </Fragment >
        );
    },

    save({ }) {
        // This is done dynamically via twig
        return (
            <div>
            </div >
        );
    },
};