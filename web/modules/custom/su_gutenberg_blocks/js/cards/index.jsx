/**
 * Internal dependencies
 */
 import cards from './cards';
 import cardCustom from './card-custom';
 import cardContent from './card-content';

 export const cardCustomSettings = {
     ...cardCustom
 };

 export const cardContentSettings = {
     ...cardContent
 };

 export const cardsSettings = {
     ...cards
 };