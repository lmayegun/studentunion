const { element, editor, blockEditor } = wp;
const { Fragment } = element;
const {
    InnerBlocks,
} = blockEditor;
const __ = Drupal.t;

export default {
    title: __('Cards'),
    description: __('Embed cards in the page'),
    icon: 'admin-page',
    attributes: {
        'rowClasses': {
            type: 'string',
            default: ''
        },
        'responsiveCardSize': {
            type: 'boolean',
            default: false
        },
        'card_mode': {
            type: 'string',
            default: 'card-portrait'
        }
    },
    edit: ({ className, attributes, setAttributes, isSelected }) => {
        return (
            <div className={className + ""}>
                <InnerBlocks orientation="horizontal" allowedBlocks={[`su-gutenberg-blocks/card-custom`, `su-gutenberg-blocks/card-content`]} />
            </div>
        );
    },
    save: ({ attributes }) => {
        const { rowClasses, responsiveCardSize, card_mode } = attributes;
        return (
            <Fragment>
            </Fragment >
        );
    },
};