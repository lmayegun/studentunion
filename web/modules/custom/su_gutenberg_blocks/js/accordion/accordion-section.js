const { element, editor, blockEditor } = wp;
const { Fragment } = element;
const {
    RichText,
} = blockEditor;
const __ = Drupal.t;

export default {
    title: __('Accordion section'),
    description: __('A single accordion section within an accordion'),
    attributes: {
        text: {
            type: 'string',
            default: ''
        },
        title: {
            type: 'string',
            default: ''
        },
    },
    edit({ attributes, setAttributes }) {
        const { text, title } = attributes;
        return (<Fragment>
            <RichText
                identifier="title"
                tagName="dt"
                className="styled"
                value={title}
                placeholder={__('Edit title')}
                onChange={(title) => {
                    setAttributes({
                        title: title
                    })
                }}
            />

            <RichText
                identifier="text"
                tagName="dd"
                value={text}
                placeholder={__('Edit text')}
                onChange={(text) => {
                    setAttributes({
                        text: text
                    })
                }}
                multiline={true}
            />
        </Fragment>);
    },

    save({ attributes }) {
        const { text, title } = attributes;
        return (
            <Fragment>
                <RichText.Content tagName="dt" value={title} />
                <RichText.Content tagName="dd" value={text} />
            </Fragment>
        );
    },
};