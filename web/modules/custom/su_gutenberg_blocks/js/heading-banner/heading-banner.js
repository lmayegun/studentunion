const { element, editor, components, blockEditor } = wp;
const { Fragment } = element;
const {
    PanelBody,
    SelectControl,
    TextControl,
} = components;
const {
    RichText, InspectorControls, MediaUpload
} = blockEditor;
const __ = Drupal.t;

import { duotoneOptions } from '../shared';

export default {
    title: __('Heading banner'),
    description: __('A full width heading banner with an anchor name.'),
    icon: 'align-wide',
    attributes: {
        title: {
            type: 'string',
            default: ''
        },
        subtitle: {
            type: 'string',
            default: ''
        },
        anchorName: {
            type: 'string',
            default: ''
        },
        mediaID: {
            type: 'number',
        },
        mediaURL: {
            type: 'string',
            default: '',
        },
        duotone: {
            type: 'string',
            default: ''
        },
    },
    edit({ attributes, setAttributes }) {
        const { subtitle, anchorName, mediaID, duotone, mediaURL, title } = attributes;
        return (<div class={"alignfull has-parallax duotone-" + duotone} >
            <RichText
                identifier="title"
                tagName="h2"
                className="title"
                value={title}
                placeholder={__('Edit title')}
                onChange={(title) => {
                    setAttributes({
                        title: title
                    })
                }}
            />
            <RichText
                identifier="subtitle"
                tagName="h3"
                className="subtitle"
                value={subtitle}
                placeholder={__('Edit optional subtitle')}
                onChange={(subtitle) => {
                    setAttributes({
                        subtitle: subtitle
                    })
                }}
            />
            <div class={"duotone-image " + duotone} style={{ backgroundImage: "url(" + mediaURL + ");" }}>
            </div>
            <InspectorControls>
                <PanelBody title={__('Settings')}>
                    <MediaUpload
                        label="Image"
                        onSelect={
                            (media) => {
                                setAttributes({
                                    mediaURL: media.url,
                                    mediaID: media.id,
                                });
                            }
                        }
                        type="image"
                        value={""}
                        render={
                            ({ open }) => (
                                <Fragment>
                                    {mediaURL ? <img src={mediaURL} width="100px" height="100px" /> : <div></div>}
                                    <button onClick={open}>
                                        Upload image
                                    </button>
                                </Fragment>
                            )}
                    />
                    <SelectControl
                        label="Duotone"
                        value={duotone}
                        options={duotoneOptions}
                        onChange={(duotone) => {
                            setAttributes({ duotone });
                        }}
                    />
                    <TextControl
                        label="Anchor name"
                        value={anchorName}
                        onChange={(anchorName) => {
                            setAttributes({ anchorName });
                        }}
                    />
                </PanelBody>
            </InspectorControls>
        </ div>);
    },

    save({ attributes }) {
        const { title, anchorName, duotone, mediaURL, subtitle } = attributes;
        return (
            <Fragment>
                {anchorName && anchorName != '' ? <a name={anchorName} /> : <></>}
                <div class={"heading-banner alignfull has-parallax duotone-" + duotone}>
                    <div class="headings container">
                        <h2 class="title"><span class="heading-box-break">{title}</span></h2>
                        {subtitle != '' ? <div class="page-subtitle-container"><h3 class="page-subtitle">{subtitle}</h3></div> : <Fragment />}
                    </div>
                    <div class={"duotone-image " + duotone} style={{ backgroundImage: "url(" + mediaURL + ");" }}>
                    </div>
                </div >
            </Fragment>
        );
    },
};