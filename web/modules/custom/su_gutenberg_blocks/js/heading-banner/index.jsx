/**
 * Internal dependencies
 */

 import headingBanner from './heading-banner';
 
 export const headingBannerSettings = {
     ...headingBanner
 };
 