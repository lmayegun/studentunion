
cd /var/www/d8/web/modules/custom/su_gutenberg_blocks && npm run build && drush cr
# Editing and creating blocks:

- Only edit index.jsx, then compile via 'npm run build' or 'npm start'

- For npm start, make sure you have the inspector open and cache disabled when you refresh to get the latest changes.

# Current blocks

- ckeditor-accordion: recreates the ckeditor-accordion for Gutenberg

- cards-by-tags: shows Union content or events by embedded a view filtered by tags - see index.es6.js, but also su_gutenberg_blocks.module for determining the tag ID, and also templates/gutenberg-block--su-gutenberg-blocks--cards-by-tags.html.twig for how the view is embedded

# Copied from existing documentation:

## Notice

Rename `package.example.json` to `package.json` if you're using this code on your own module.

The Gutenberg editor gives a lot of options out of the box, but to really harness the power Gutenberg gives you, a custom block written in a ReactJS syntax, wrapped with the Gutenberg block API is the way to go.

This does not only let you create the block as standard HTML, CSS and JS, but you can also use the powerful Gutenberg block API to create rich and advanced user interfaces.

The best way to quickly get started to create your own custom blocks is to use the provided Example block sub-module.

You'll need NodeJS (and NPM) installed for Javascript and CSS transpiling tools.

- Copy the gutenberg/modules/example_block folder to your custom modules folder and rename it. In our case, we're using modules/custom/my_custom_blocks
- Rename example*block.*.yml files to my*custom_blocks.*.yml and package.example.json to package.json
- Rename example_block reference to my_custom_blocks on my_custom_blocks.gutenberg.yml and package.json files
- Run npm install on the my_custom_blocks folder - this sometimes take a while...(longer than in a react app)
- Activate the my_custom_blocks Drupal Module (drush en my_custom_blocks)
- in cli navigate to my_custom_blocks folder Run #npm run build to create the new js Code
- Run #npm start on in my_custom_blocks folder in cli to continues watch changes during development
- The new Block is now available in gutenberg editor. Search for the Block Title

This should be the file structure of your newly created module:

Module folder structure [TODO?]

# Resources on custom block development

    Blocks (WordPress/gutenberg.git docs)

    Writing Your First Block Type (WordPress.org docs)

    Your best friends: RichText and InnerBlocks components

    Also you can check how core blocks are implemented. [WHERE?] Find a middle term between reusing core blocks, their style or their functionalities and creating blocks from scratch.
