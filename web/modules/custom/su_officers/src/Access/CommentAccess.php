<?php

namespace Drupal\comment_perm\Access;

use Drupal\comment\CommentManagerInterface;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\group\Entity\Group;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Comment types additional overrides for access checks.
 */
class CommentAccess implements ContainerInjectionInterface
{
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The comment manager.
   *
   * @var \Drupal\comment\CommentManagerInterface
   */
  protected $commentManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * CommentAccess constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\comment\CommentManagerInterface $comment_manager
   *   The comment manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CommentManagerInterface $comment_manager, AccountProxyInterface $current_user)
  {
    $this->entityTypeManager = $entity_type_manager;
    $this->commentManager = $comment_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('comment.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Access check for the reply form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity this comment belongs to.
   * @param string $field_name
   *   The field_name to which the comment belongs.
   * @param int $pid
   *   (optional) Some comments are replies to other comments. In those cases,
   *   $pid is the parent comment's comment ID. Defaults to NULL.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result.
   */
  public function replyFormAccess(EntityInterface $entity, $field_name, $pid = NULL)
  {
    // Check if entity and field exists.
    $fields = $this->commentManager->getFields($entity->getEntityTypeId());
    if (empty($fields[$field_name])) {
      throw new NotFoundHttpException();
    }

    $account = $this->currentUser;
    $comment_type = $entity->get($field_name)->getSettings()['comment_type'];

    if ($comment_type !== 'officer_project_update') {
      return AccessResult::neutral();
    }

    // Check if the user has the proper permissions.
    $access = AccessResult::allowedIf($this->accessPostComment($account, $entity));

    // If commenting is open on the entity.
    $status = $entity->{$field_name}->status;
    $access = $access->andIf(AccessResult::allowedIf($status == CommentItemInterface::OPEN)
      ->addCacheableDependency($entity))
      // And if user has access to the host entity.
      ->andIf(AccessResult::allowedIf($entity->access('view')));

    return $access;
  }

  /**
   * Check if user has access to comments.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to view comments, FALSE otherwise.
   */
  public function accessPostComment(AccountInterface $account, Node $entity)
  {
    // Global (site role) permission
    if ($account->hasPermission("post officer_project_update comments")) {
      return true;
    }

    // Otherwise, check if project node is referenced as a project by a group (officer) for which the user has project update permission
    // i.e. only allow permission if this project is part of the officer's group
    $groupIds = \Drupal::entityQuery('group')
      ->condition('field_officer_projects', $entity->id())
      ->execute();
    $groups = Group::loadMultiple($groupIds);
    foreach ($groups as $group) {
      if ($group->hasPermission('add officer project updates', $account)) {
        return true;
      }
    }

    return FALSE;
  }
}
