<?php

namespace Drupal\su_officers\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber event listener.
 */
class RouteSubscriber extends RouteSubscriberBase
{

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection)
  {
    if ($route = $collection->get('comment.reply')) {
      $route->setRequirement('_custom_access', '\Drupal\su_officers\Access\CommentAccess::replyFormAccess');
    }
  }
}
