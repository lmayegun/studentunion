Module should allow users to select which roles can edit, per workflow

So:
- Third party setting on workflow
- Hook or subscribe into access request to manage access if any roles set
