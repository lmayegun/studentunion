<?php

/**
 * @file
 * Provide views data for group_recommendation.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function group_recommendation_views_data_alter(&$data)
{

    $data['groups']['group_recommendation_similarity'] = [
        'group' => t('Group'),
        'title' => t('Group similarity score'),
        'help' => t('Percentage/count of similarity score.'),
        'field' => [
            'id' => 'group_recommendation_similarity_views_field',
        ],
        'sort' => [
            'id' => 'group_recommendation_similarity_views_sort',
        ],
    ];

    $data['groups']['group_recommendation_similarity_group_id'] = [
        'title' => t('Group ID for similarity comparison'),
        'group' => t('Group'),
        'help' => t('ID of groups(s).'),
        'argument' => [
            'id' => 'group_recommendation_similarity_arg',
            'name field' => 'title',
            'numeric' => TRUE,
            'validate type' => 'id',
        ],
    ];
}
