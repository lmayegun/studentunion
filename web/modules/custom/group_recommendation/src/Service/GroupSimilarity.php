<?php

namespace Drupal\group_recommendation\Service;

use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group\GroupMembershipLoader;

use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Service\GroupMembershipRecordManager;
use Drupal\group_membership_record\Service\GroupMembershipRecordRepository;

/**
 * Class GroupSimilarity.
 */
class GroupSimilarity
{
    /**
     * Get similar groups fro a group
     * Based on how similar their memberships are
     */
    public function getSimilarGroups($group, $groupTypes = [], $count = null)
    {
        $rankings = [];

        // Get this groups members UIDs
        $group_uids = $this->getGroupMemberUids($group);

        // Load eligible groups
        $groups = $this->getGroups();

        foreach ($groups as $group) {
            $test_uids = $this->getGroupMemberUids($group);

            $overlap = count(array_intersect($test_uids, $group_uids));

            // Ranking is the proportion of this group's members in the other's
            $ranking = $overlap / count($group_uids);

            $rankings[$group->id()] = $ranking;
        }

        rsort($rankings);

        if ($count) {
            return array_slice($rankings, 0, $count);
        }

        return $rankings;
    }

    public function getGroups() {
      // @todo allow config to determine which groups to compare
      // by default just groups of the same type
    }

    public function getGroupMemberUids(GroupInterface $group)
    {
        // @todo allow config to determine which members count for similarity (e.g. ignore admin/superuser roles)

        // @todo more performant to just load IDs directly from database
        $uids = [];
        $memberships = $group->getMembers();
        foreach ($memberships as $membership) {
            $uid = $membership->getUser()->id();
            $uids[] = $uid;
        }
        return $uids;
    }

    public function getScoreForGroups($group1, $group2)
    {
    }
}
