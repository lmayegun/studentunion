<?php

namespace Drupal\group_recommendation\Service;

use Drupal\group\Entity\Group;

/**
 * Class GroupUserMatching.
 */
class GroupUserMatching
{
    /**
     * Gets recommended groups based on:
     * - Similar groups to the user's current groups
     * - Groups that have members of the user's current groups, and similar groups 
     */
    public function getRecommendedGroupsForUser($account, $groupTypes = [], $groupsToReturn = 5)
    {
        $similarityService = \Drupal::service('group_recommendation.similarity');

        $similarGroupsToCheck = 10;
        $topUsersToCompare = 30;

        // Get user's groups
        $groups = [];
        $userGroupMemberships = \Drupal::service('group.membership_loader')->loadByUser($account);
        foreach ($userGroupMemberships as $userGroupMembership) {
            $groups[$userGroupMembership->getGroup()->id()] = 1;
        }

        // For each of the user's group, get similar groups
        foreach ($groups as $groupId => $score) {
            $newSimilarGroups = $similarityService->getSimilarGroups(Group::load($groupId), $groupTypes, $similarGroupsToCheck);

            foreach ($newSimilarGroups as $similarGroupId => $similarityScore) {
                if (in_array($similarGroupId, $groups)) {
                    // If it's a user's group, don't change
                    // Remove the user's current groups from similarity
                    if ($groups[$similarGroupId] == 1) {
                        continue;
                    }

                    // Otherwise, keep the highest similarity score
                    if ($$groups[$similarGroupId] > $similarityScore) {
                        continue;
                    }
                }

                $groups[$similarGroupId] = $similarityScore;
            }
        }

        // Get memberships across all groups
        $uidsMembershipCounts = [];
        $groupMembers = [];
        foreach ($groups as $groupId => $similarityScore) {
            $group = Group::load($groupId);

            // Store the full membership list
            $groupMembers[$groupId] = $similarityService->getGroupMemberUids($group);

            foreach ($uids as $uid) {
                // For each user, count how many of the groups they are in
                if ($uid != $account->id()) {
                    if (!isset($uidsMembershipCounts[$uid])) {
                        $uidsMembershipCounts[$uid] = 0;
                    }
                    $uidsMembershipCounts[$uid]++;
                }
            }
        }

        // Identify top similar users that are in the most of these similar groups
        rsort($uidsMembershipCounts);
        $topUids = array_keys(array_slice($uidsMembershipCounts, 0, $topUsersToCompare));

        $groupRankings = [];
        foreach ($groups as $groupId => $groupWeighting) {
            $totalUsers = count($groupMembers[$groupId]);
            $topUsers = count(array_intersect($topUids, $groupMembers[$groupId]));

            // Score groups based on how many of the top users are in them
            $userWeighting = $topUsers / $totalUsers;

            // Create combined score of similarity combined with the top user weightings
            $groupRankings[$groupId] = $groupWeighting * $userWeighting;
        }

        // Sort groups by finalScore desc
        rsort($groupRankings);

        // Return subset 
        return array_slice($groupRankings, 0, $groupsToReturn);
    }
}
