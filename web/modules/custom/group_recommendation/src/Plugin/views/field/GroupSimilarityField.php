<?php

namespace Drupal\group_recommendation\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Shows the similarity of the group.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("group_recommendation_similarity_field")
 */
class GroupSimilarityField extends FieldPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function query()
    {
        $params = [
            'function' => 'count',
        ];
        $this->field_alias = $this->query->addField('group', 'id', NULL, $params);
    }

    /**
     * {@inheritdoc}
     */
    protected function defineOptions()
    {
        $options = parent::defineOptions();
        $options['percent_suffix'] = ['default' => 1];
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state)
    {
        $form['percent_suffix'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Append % when showing percentage'),
            '#default_value' => !empty($this->options['percent_suffix']),
        ];
        parent::buildOptionsForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values)
    {
        $output = round($values->{$this->field_alias} / $this->view->total_similar * 100);
        if (!empty($this->options['percent_suffix'])) {
            $output .= '%';
        }
        return $output;
    }
}
