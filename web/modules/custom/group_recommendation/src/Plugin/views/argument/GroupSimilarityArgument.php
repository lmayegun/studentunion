<?php

namespace Drupal\group_recommendation\Plugin\views\argument;

use Drupal\views\Views;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Database\Query\Condition;

/**
 * Argument handler to accept an entity id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("group_recommendation_similarity_arg")
 */
class GroupSimilarityArgument extends NumericArgument implements ContainerFactoryPluginInterface
{

    /**
     * Database service object.
     *
     * @var \Drupal\Core\Database\Connection
     */
    protected $connection;

    /**
     * Entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    private $entityTypeManager;

    /**
     * Constructs the SimilarReferencesArgument object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Database\Connection $connection
     *   The database connection.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, EntityTypeManagerInterface $entity_type_manager)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->connection = $connection;
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('database'),
            $container->get('entity_type.manager')
        );
    }

    /**
     * Define default values for options.
     */
    protected function defineOptions()
    {
        $options = parent::defineOptions();
        $options['include_args'] = ['default' => FALSE];

        return $options;
    }

    /**
     * Build options settings form.
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state)
    {
        parent::buildOptionsForm($form, $form_state);

        $form['include_args'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Include argument group(s) in results'),
            '#description' => $this->t('If selected, the group(s) passed as the argument will be included in the view results.'),
            '#default_value' => !empty($this->options['include_args']),
        ];
    }

    /**
     * Validate this argument works. By default, all arguments are valid.
     */
    public function validateArgument($arg)
    {

        if (isset($this->argument_validated)) {
            return $this->argument_validated;
        }

        // The argument.
        $this->value = [$arg => $arg];



        // $this->fields = $fields;
        // $this->view->total_similar = $similarCount;

        // if (empty($fields) || !$hasRelationship) {
        //     return FALSE;
        // }

        return TRUE;
    }

    /**
     * Add filter(s).
     */
    public function query($group_by = FALSE)
    {
        $this->ensureMyTable();
        $isReferenced = new Condition('OR');
        // Add relationships and conditions.
        foreach ($this->fields as $field) {
            if (is_array($field) && !empty($field['entity_ids'])) {
                $configuration = [
                    'table' => $field['table_name'],
                    'field' => 'entity_id',
                    'left_table' => 'node_field_data',
                    'left_field' => 'nid',
                    'adjusted' => TRUE,
                    'type' => 'LEFT',
                ];
                $join = Views::pluginManager('join')->createInstance('standard', $configuration);
                $this->query->addRelationship($field['table_name'], $join, 'node_field_data');
                // Add condition.
                $isReferenced->condition((new Condition('OR'))
                        ->condition("{$field['table_name']}.entity_id", $field['entity_ids'], 'IN')
                );
            }
        }
        if ($isReferenced->count()) {
            $this->query->addWhere('AND', $isReferenced);
        }
        // Exclude the current node(s).
        if (empty($this->options['include_args'])) {
            $this->query->addWhere(0, "node.nid", $this->value, 'NOT IN');
        }
        $this->query->addGroupBy('nid');
    }

    /**
     * Get the target_id values of a given entity ID and field name.
     *
     * @param string $field
     *   The field name.
     * @param int $entityId
     *   The entity ID.
     *
     * @return array
     *   An array of entity ids.
     */
    public function getReferenceFieldTargetIds($field, $entityId)
    {
        $table = sprintf('node__%s', $field);
        $col = sprintf('%s_target_id', $field);
        $select = $this->connection->select($table, 'fd');
        $select->fields('fd', ['entity_id', $col]);
        $select->condition('entity_id', $entityId);
        $select->distinct();
        $results = $select->execute()->fetchAll();
        $ids = [];
        foreach ($results as $row) {
            $ids[] = $row->{$col};
        }
        // Don't allow zero as a target_id value.
        if (($key = array_search(0, $ids)) !== FALSE) {
            unset($ids[$key]);
        }

        return $ids;
    }

    /**
     * Get an array of all Entity Reference fields with a specific target type.
     *
     * @param string $targetType
     *   Target entity type.
     *
     * @return array
     *   Array of labels, key'd by the field machine name.
     */
    public function getReferenceFieldsByType($targetType)
    {
        $field_properties = [
            'settings' => [
                'target_type' => $targetType,
            ],
            'entity_type' => 'node',
            'type' => 'entity_reference',
            'deleted' => FALSE,
            'status' => 1,
        ];
        $fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties($field_properties);
        $referenceFields = [];
        /** @var \Drupal\field\FieldStorageConfigInterface $field */
        foreach ($fields as $field) {
            $referenceFields[$field->getName()] = sprintf('%s (%s)', $field->getName(), $targetType);
        }
        return $referenceFields;
    }
}
