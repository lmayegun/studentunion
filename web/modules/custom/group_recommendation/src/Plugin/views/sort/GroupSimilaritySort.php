<?php

namespace Drupal\group_recommendation\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Handler which sort by the similarity.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("group_recommendation_similarity_sort")
 */
class GroupSimilaritySort extends SortPluginBase
{

    /**
     * Define default sorting order.
     */
    protected function defineOptions()
    {
        $options = parent::defineOptions();
        $options['order'] = ['default' => 'DESC'];
        return $options;
    }

    /**
     * Add orderBy.
     */
    public function query()
    {
        $this->ensureMyTable();
        $this->query->addOrderBy($this->tableAlias, 'nid', $this->options['order'], NULL, ['function' => 'count']);
    }
}
