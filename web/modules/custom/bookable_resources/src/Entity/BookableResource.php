- Name
- Description
- Inherit settings from other resource

If not inheriting:

- Default availability (if an availability block is not defined)
- Restriction plugins

Custom fields (not base):
- Type - taxonomy reference

function checkAvailability(DrupalDateTime $start, DrupalDateTime $end) {
$bookable = false;
$issues = [];

// If override permission
$override_permission = false;
if($override_permission) {
$bookable = true;
// We still provide the issues so the admin user doesn't do something insane
}

// Get current availability blocks

// Get current bookings overlapping that time


// Check restrictions

return ['status' => $bookable, 'issues' => $issues];
}
